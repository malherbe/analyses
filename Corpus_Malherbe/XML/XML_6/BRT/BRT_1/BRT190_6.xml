<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT190" modus="cm" lm_max="12" metProfile="6+6">
				<head type="number">XV</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M/mp">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.4">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="1.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.8" punct="vg:12">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="2.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t</w> <w n="2.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.5">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.8" punct="pi:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.7">fr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="3.10" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="vg">e</seg>c</w>,</l>
					<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ps</w> <w n="4.3" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="4.8">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>c</w> <w n="4.9" punct="pi:12">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pi">e</seg>c</w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="Lc">o</seg>rsqu</w>’<w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="5.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="5.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="5.7">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fp">e</seg></w>-<w n="6.4">t</w>-<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.10" punct="pi:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="7.8" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
					<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="8.4" punct="vg:6">d<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="8.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="8.8" punct="pi:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pi">eau</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="9.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">aî</seg>t</w>-<w n="9.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>l</w>, <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.10" punct="vg:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="5.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="10.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="10.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="10.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="10.8" punct="pi:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="11" num="6.1" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">Ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="Fm">e</seg>s</w>-<w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="11.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></w></l>
					<l n="12" num="6.2" lm="12" met="6+6"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="12.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="12.8">r<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="12.9" punct="pi:12">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pi">ain</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="7">
					<l n="13" num="7.1" lm="12" met="6+6"><w n="13.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="13.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="13.10" punct="vg:12">c<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="7.2" lm="12" met="6+6"><w n="14.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3" mp="M">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="14.5">r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="14.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.9" punct="pi:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="8">
					<l n="15" num="8.1" lm="12" met="6+6"><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">m</w>’<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="15.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M/mp">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="4" mp="Lp">ez</seg></w>-<w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="15.9">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg></w> <w n="15.10">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="15.11" punct="vg:12">p<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
					<l n="16" num="8.2" lm="12" met="6+6"><w n="16.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="16.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="16.6" punct="vg:9">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="16.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="16.9" punct="pi:12">j<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pi">eu</seg></w> ?</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Pic, pic, pique, pique, pique-nique, pic, Pic de la Mirandole, pique.</note>
					</closer>
			</div></body></text></TEI>