<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT189" modus="cm" lm_max="10" metProfile="4+6">
				<head type="number">XIV</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="1.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="4" caesura="1">e</seg>r</w><caesura></caesura> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="1.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>f</w> <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="1.9" punct="vg:10">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">Su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fm">e</seg></w>-<w n="2.3" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg" caesura="1">à</seg></w>,<caesura></caesura> <w n="2.4">qu</w>’<w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="2.7">h<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="2.9" punct="pe:10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pe">on</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.4">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.5" punct="pe:10">M<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe ps" mp="F">e</seg></w> ! …</l>
					<l n="4" num="2.2" lm="10" met="4+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M/mp">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.6" punct="pi:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi ps" mp="F">e</seg></w> ? …</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="5" num="3.1" lm="10" met="4+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="Lc">à</seg></w>-<w n="5.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">e</seg>p</w>,<caesura></caesura> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.6" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>,</l>
					<l n="6" num="3.2" lm="10" met="4+6"><w n="6.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="6.7" punct="pt:10">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="7" num="4.1" lm="10" met="4+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">T<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>t</w><caesura></caesura> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="7.5">d</w>’<w n="7.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">É</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="4.2" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="8.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="8.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="8.8" punct="pt:10">L<seg phoneme="ɔ" type="vs" value="1" rule="317" place="10">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="9" num="5.1" lm="10" met="4+6"><w n="9.1">L</w>’<w n="9.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.6" punct="vg:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>rs</w>,</l>
					<l n="10" num="5.2" lm="10" met="4+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.3" punct="vg:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="10.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>s</w> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="10.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.8" punct="pt:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>rs</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="11" num="6.1" lm="10" met="4+6"><w n="11.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t</w> <w n="11.2" punct="vg:4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="11.6" punct="vg:10">Fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="6.2" lm="10" met="4+6"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="12.3" punct="ps:4">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="ps" caesura="1">on</seg>s</w>…<caesura></caesura> <w n="12.4">D<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M/mp">ai</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="7" mp="Lp">ez</seg></w>-<w n="12.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="12.6" punct="pi:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></w> ?</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Aman, Aman, amant, aman, Amand (Saint-).</note>
					</closer>
			</div></body></text></TEI>