<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT284" modus="cm" lm_max="12" metProfile="6+6">
				<head type="number">IX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.4" punct="vg:6">di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.7" punct="pe:12">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="12" punct="pe">o</seg></w> !</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2" punct="vg:3">b<seg phoneme="o" type="vs" value="1" rule="315" place="2" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="3.7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8" punct="pv:12">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>gt</w> <w n="4.2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rts</w><caesura></caesura> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="4.10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="493" place="2" mp="M">y</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>x</w><caesura></caesura> <w n="5.5">s</w>’<w n="5.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</w>, <w n="5.7">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="9" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="5.9">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4" punct="vg:6">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.7" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="7.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="7.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7" punct="vg:8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></w>, <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.9">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="7.11">l</w>’<w n="7.12"><seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="8.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>t</w> <w n="8.6">qu</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.8" punct="pt:12">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="vg:2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>d</w>, <w n="9.3">d</w>’<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="9.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="9.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="9.10" punct="vg:12">fl<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="10.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="10.4" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="10.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="10.8" punct="dp:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="dp">en</seg>ts</w> :</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">N<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>c</w> <w n="11.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="11.3" punct="pe:4"><seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>ltr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg></w> ! » <w n="11.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="C">i</seg></w> <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</w> <w n="11.8" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.3">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="12.8" punct="pe:12">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</w> !</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="13.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="13.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>nt</w> <w n="13.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</w> !</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="14.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="14.8" punct="pe:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></w> ! …</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Sémiramis.</note>
					</closer>
			</div></body></text></TEI>