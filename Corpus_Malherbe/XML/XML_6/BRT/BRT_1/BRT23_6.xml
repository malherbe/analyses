<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT23" modus="cp" lm_max="12" metProfile="6+6, (4)">
				<head type="number">XXIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6">« <w n="1.1" punct="pe:2">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe" mp="F">e</seg></w> ! <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ls</w><caesura></caesura> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rmi<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="1.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="1.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.10" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="2.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ds</w><caesura></caesura> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="2.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="2.10" punct="pe:12">v<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pe">en</seg></w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g</w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="3.5">qu</w>’<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.8">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="3.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="3.10">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>s</w> <w n="3.11">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.12"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="3.13">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="4.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="4.9" punct="pe:12">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pe">en</seg></w> !</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="5.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="5.6">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.10">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="5.11" punct="pv:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</w> ;</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3" punct="vg:3">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>ds</w>, <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.6" punct="vg:6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="6.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="6.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.9">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="6.10" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="7.2">c</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4" punct="vg:3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3" punct="vg">en</seg></w>, <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.6" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="7.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.11">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="8" num="1.8" lm="4"><space unit="char" quantity="16"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3" punct="pt:4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>r</w>. »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Bon sang ne peut mentir.</note>
					</closer>
			</div></body></text></TEI>