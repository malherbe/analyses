<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT56" modus="cp" lm_max="12" metProfile="6, 6+6">
				<head type="number">LVI</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="1.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="1.7">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8" mp="M">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.10" punct="pi:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps" mp="F">e</seg></w> ? …</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">oû</seg>t</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">n</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="2.6">n<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="2.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="12"></space><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="3.3">d</w>’<w n="3.4" punct="dp:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="dp">on</seg></w> :</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="4.7">qu</w>’<w n="4.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.9">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.11">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t</w> <w n="4.12" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="5.4" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.6" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</w>, <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="12"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l</w>’<w n="6.3" punct="ps:6"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="ps">on</seg></w>…</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1" punct="vg:2">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2" punct="vg">ô</seg>t</w>, <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="7.7">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="10">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.8" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="8.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="C">i</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.9" punct="pv:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="9" num="1.9" lm="6" met="6"><space unit="char" quantity="12"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="10.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="10.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="10.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.8" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="11.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="12" num="1.12" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ts</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="12.5" punct="ps:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ps">a</seg>l</w>…</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="13.4">gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="13.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="13.7">qu</w>’<w n="13.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="13.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="13.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="1.14" lm="6" met="6"><space unit="char" quantity="12"></space><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.3">h<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w></l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="15.3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="15.6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.7" punct="ps:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
					<l n="16" num="1.16" lm="6" met="6"><space unit="char" quantity="12"></space><w n="16.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="16.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="16.3" punct="pt:6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pt">on</seg></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ L’eau va toujours à la rivière.</note>
					</closer>
			</div></body></text></TEI>