<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT18" modus="cp" lm_max="12" metProfile="8, 6+6">
				<head type="number">XVIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6">« <w n="1.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="1.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="1.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="1.7">d</w>’<w n="1.8">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="1.9" punct="pi:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps" mp="F">e</seg></w> ? …</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2" punct="vg:2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2" punct="vg">a</seg>y<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="2.5" punct="pe:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe" caesura="1">er</seg></w> !<caesura></caesura> <w n="2.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</w>, <w n="2.7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M/mp">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M/mp">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="11" mp="Lp">ez</seg></w>-<w n="2.8" punct="dp:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="12" punct="dp">en</seg></w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">î</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3">ch<seg phoneme="e" type="vs" value="1" rule="347" place="4" mp="P">ez</seg></w> <w n="3.4" punct="vg:6">V<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>f<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="3.6">l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="3.8">th<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="4.3" punct="vg:3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd</w>, <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="4.5" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">tr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.10" punct="pe:12">f<seg phoneme="œ" type="vs" value="1" rule="304" place="11" mp="M">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pe">an</seg></w> ! »</l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="5.4">P<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>l</w> <w n="5.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="6.3">sp<seg phoneme="i" type="vs" value="1" rule="125" place="3">ee</seg>ch</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.6" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg>s</w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3" punct="vg:4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="7.6">d</w>’<w n="7.7" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
					<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">D</w>’<w n="9.2" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="9.5" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="10.3">gr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.5" punct="vg:6">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="10.6" punct="ps:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="ps">i</seg>t</w>…</l>
					<l n="11" num="1.11" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">l</w>’<w n="11.3" punct="vg:4"><seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.4">s</w>’<w n="12.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Qui paye ses dettes s’enrichit.</note>
					</closer>
			</div></body></text></TEI>