<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT79" modus="cp" lm_max="12" metProfile="6, 6+6">
				<head type="number">LXXIX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3" punct="pe:3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe">a</seg></w> ! <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="1.5" punct="vg:6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>c</w>,<caesura></caesura> <w n="1.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="1.7" punct="vg:9">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>r</w>, <w n="1.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="1.9">m</w>’<w n="1.10" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="2.5" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3">m</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.5" punct="vg:6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="3.9">p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.10" punct="pe:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="4.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="4.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.6" punct="pt:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1" punct="vg:3">V<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="5.2" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="5.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="5.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="12"></space><w n="6.1" punct="vg:2">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>p</w>, <w n="6.2" punct="vg:3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="6.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="6.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>r</w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="7.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="7.5" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="7.7">t<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="7.9" punct="pv:12">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">n</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="8.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="8.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l</w> <w n="8.6" punct="dp:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp" caesura="1">i</seg></w> :<caesura></caesura> <w n="8.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="8.8">cr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="8.10" punct="pe:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pe ps">o</seg>rt</w> ! …</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="9.4">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="9.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="9.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.8" punct="pt:12">dr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="10" num="1.10" lm="6" met="6"><space unit="char" quantity="12"></space><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.4" punct="dp:6"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="dp">e</seg>rts</w> :</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="11.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.6" punct="vg:6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="11.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.9">l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">P<seg phoneme="y" type="vs" value="1" rule="453" place="1" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>ts</w> <w n="12.8" punct="pe:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pe ps">e</seg>rts</w> ! …</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="13.3">m</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="13.5" punct="ps:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="ps" caesura="1">i</seg>s</w>…<caesura></caesura> <w n="13.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="13.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="13.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="13.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.10" punct="dp:12">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="14" num="1.14" lm="6" met="6"><space unit="char" quantity="12"></space><w n="14.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="14.4" punct="pe:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" punct="pe">un</seg></w> !</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="15.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="15.8">m</w>’<w n="15.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="15.10" punct="dp:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="16.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="16.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="P">a</seg>r</w> <w n="16.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="16.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="16.7">n</w>’<w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="16.9"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="16.10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="16.11">qu</w>’<w n="16.12" punct="pe:12"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="12" punct="pe">un</seg></w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Promettre et tenir sont deux.</note>
					</closer>
			</div></body></text></TEI>