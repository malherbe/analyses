<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT210" modus="cm" lm_max="12" metProfile="6+6">
				<head type="number">XXXV</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="1.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="1.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="1.7" punct="dp:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="dp" caesura="1">ou</seg>r</w> :<caesura></caesura> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="1.10" punct="pt:12">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.4" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.8">s</w>’<w n="2.9"><seg phoneme="i" type="vs" value="1" rule="497" place="9">y</seg></w> <w n="2.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rd</w> <w n="2.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.12" punct="pt:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg>t</w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.5"><seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="3.6">n</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="3.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="3.10" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="10" mp="M">en</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462" place="11" mp="M">u</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2" punct="vg:4">G<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="4.4" punct="vg:6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg>s</w>,<caesura></caesura> <w n="4.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="4.9" punct="pt:12">bru<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg>t</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu</w>’<w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="5.6" punct="dp:6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="dp" caesura="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> :<caesura></caesura> <w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.8">s</w>’<w n="5.9"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="5.10">f<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.11">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11">en</seg></w> <w n="5.12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="6.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="Lc">i</seg></w>-<w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="6.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="6.10" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="7.7" punct="dp:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp" caesura="1">a</seg>rd</w> :<caesura></caesura> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs</w> <w n="7.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="7.10"><seg phoneme="i" type="vs" value="1" rule="497" place="10" mp="C">y</seg></w> <w n="7.11" punct="vg:12">gr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>t</w><caesura></caesura> <w n="8.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="8.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="8.9" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.5" punct="dp:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="dp" caesura="1">er</seg></w> :<caesura></caesura> <w n="9.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="9.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.9" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="2" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="10.3">s<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.6" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pt">e</seg>l</w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">h<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="11.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.5">c</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.9" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.9" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pt">e</seg>l</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="13.4">v<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="13.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="13.7" punct="dp:6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="dp" caesura="1">ai</seg>r</w> :<caesura></caesura> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="13.9">s</w>’<w n="13.10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.11"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="13.12">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">aim</seg></w> <w n="14.4">f<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.6">l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="14.7" punct="pv:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg></w> ;</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3" mp="C">y</seg></w> <w n="15.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="15.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="15.6">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="15.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="15.9">j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="15.10">d</w>’<w n="15.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="16.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="16.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>t</w><caesura></caesura> <w n="16.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="16.8">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="16.10" punct="dp:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg></w> :</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">Qu</w>’<w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="Lc">en</seg>tr</w>’<w n="17.5"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="17.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="17.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="17.10" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="18.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">qu</w>’<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="18.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>t</w>,<caesura></caesura> <w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="18.7">s</w>’<w n="18.8" punct="ps:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="ps">a</seg></w>…</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="19.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="19.4">cr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="19.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="19.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="19.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="19.10" punct="vg:12">t<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="20.2" punct="vg:3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="20.4" punct="vg:6">b<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>x</w>,<caesura></caesura> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="20.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="20.8" punct="pi:12">c<seg phoneme="e" type="vs" value="1" rule="272" place="10" mp="M">æ</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="21.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="21.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="21.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="21.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="21.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="21.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="21.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="21.11">cl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="22" num="6.2" lm="12" met="6+6"><w n="22.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="22.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="22.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ç<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="22.5">v<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>ts</w> <w n="22.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ds</w> <w n="22.7"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rts</w></l>
					<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1" punct="ps:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="ps">E</seg>t</w>… <w n="23.2" punct="pe:2">cl<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>c</w> ! <w n="23.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="23.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="23.5" punct="pe:6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="ps pe" caesura="1">eu</seg>t</w>…<caesura></caesura> ! <w n="23.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="23.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="23.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="23.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="23.10">c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="24" num="6.4" lm="12" met="6+6"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="24.4">l<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="24.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="24.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="24.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="24.8" punct="pe:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pe">e</seg>rs</w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Bale, bal, balle, balle, balle, balle.</note>
					</closer>
			</div></body></text></TEI>