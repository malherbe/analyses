<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT248" modus="cm" lm_max="12" metProfile="6+6">
				<head type="number">XXIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="1.3">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>tf<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="1.6" punct="pt:12">c<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="2.8" punct="vg:9">Pl<seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>s</w>, <w n="2.9">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="2.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.11" punct="pt:12">T<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="3.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" mp="C">e</seg>t</w> <w n="3.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="3.7">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="4.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.10">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="4.11" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t</w> <w n="5.2">f<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="5.3" punct="vg:6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>pt<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="5.6" punct="vg:9">j<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>rs</w>, <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.8">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="6.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="6.4" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.8" punct="ps:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="ps">on</seg></w>…</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="7.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.8" punct="vg:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.6">D<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="8.9">d</w>’<w n="8.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="8.11" punct="pe:12">Ph<seg phoneme="a" type="vs" value="1" rule="343" place="10" mp="M">a</seg><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="9.3" punct="vg:6">t<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="9.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="Lc">e</seg>lqu</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="9.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.9" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.4" punct="vg:6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="10.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M/mp">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.8" punct="pt:12">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="11.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.8">d<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="11.10">p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="12.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rf<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="12.7" punct="pe:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>rs</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1" punct="vg:2">C<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>s</w>, <w n="13.2" punct="vg:4">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">O</seg>rph<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.5">f<seg phoneme="u" type="vs" value="1" rule="428" place="8" mp="M">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="14.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="14.3">t<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>lt<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="14.6">h<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="14.7" punct="vg:12">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</w>,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="15.2">s</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="15.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="15.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="15.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="3" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="16.6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="M">eu</seg>pl<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="16.8" punct="pt:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>ts</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="17.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="17.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="17.4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="17.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>p</w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="17.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="17.10">H<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="18.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w> <w n="18.8" punct="pv:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2" punct="vg:3">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rb<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rg</w>, <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="19.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="19.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="19.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="19.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="19.8">n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="19.9" punct="vg:12">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="20.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="20.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="20.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="20.7">ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="20.9" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ GIBET<lb></lb>▪ ICARE<lb></lb>▪ BADEN<lb></lb>▪ ÉRÈBE<lb></lb>▪ TENEZ</note>
					</closer>
			</div></body></text></TEI>