<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC39" modus="sm" lm_max="8" metProfile="8">
					<head type="number">V</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="1.7" punct="pi:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="2.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="2.5" punct="vg:8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">É</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6" punct="pt:8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="5.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="6.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="7.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="8.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="8.6" punct="pe:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pe">e</seg>l</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="9.5" punct="vg:8">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.5">l</w>’<w n="10.6" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>r</w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7" punct="vg:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="12.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="12.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="12.8" punct="pt:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pt">œu</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="13.5" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" punct="vg">en</seg></w>, <w n="13.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.7" punct="vg:8">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4" punct="vg:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bd<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="15.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="15.4">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4" punct="pe:8">fr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">L</w>’<w n="17.2" punct="vg:4">H<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="17.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.5" punct="vg:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Dr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="18.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="18.4" punct="ps:8">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="ps">an</seg>t</w>…</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="19.2">l</w>’<w n="19.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>r</w>, <w n="19.4">cl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l</w> <w n="20.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="20.5" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="21.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.6" punct="pv:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="22.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6" punct="vg:8"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="23.6" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="24.7" punct="pe:8">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="25.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="25.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="25.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="26.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="26.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="26.6" punct="pv:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pv">en</seg>ts</w> ;</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="27.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.3">N<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="28.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="28.4" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>ts</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4" punct="pv:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pv">i</seg>s</w> ; <w n="29.5" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="29.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="29.8" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1" punct="vg:2">Fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="30.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="30.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="30.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="30.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w></l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="31.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="31.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="31.5" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="32.2" punct="vg:4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="3">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="32.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="32.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="32.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="32.6" punct="pe:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>x</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1" lm="8" met="8"><w n="33.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="33.2">l</w>’<w n="33.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="33.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="33.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="33.7" punct="vg:8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="34" num="9.2" lm="8" met="8"><w n="34.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="34.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="34.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="34.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="34.6" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
						<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr</w>’<w n="35.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="35.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="35.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="35.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="35.6">d</w>’<w n="35.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="36" num="9.4" lm="8" met="8"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="36.2">s</w>’<w n="36.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="36.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="36.5">l</w>’<w n="36.6" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">5 mai 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>