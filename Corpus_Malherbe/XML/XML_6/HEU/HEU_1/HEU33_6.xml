<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU33" modus="cm" lm_max="10" metProfile="5+5">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Les Chiennes d’Enfer</head>
						<lg n="1">
							<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="1.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.5">l</w>’<w n="1.6" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="vg">en</seg>t</w>,</l>
							<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="2.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="2.7" punct="vg:10">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="3.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>t</w><caesura></caesura> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
							<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="4.4">di<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.6">cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.7" punct="ps:10">pu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="ps">an</seg>t</w>…</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4" punct="vg:5">m<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>r</w> <w n="5.7">d</w>’<w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="5.9" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
							<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>r</w><caesura></caesura> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</w> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w></l>
							<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="7.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" caesura="1">oin</seg></w><caesura></caesura> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" mp="P">e</seg>rs</w> <w n="7.5">l</w>’<w n="7.6" punct="pe:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe ps">é</seg></w> !…</l>
							<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1" punct="vg:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="8.2" punct="vg:3">m<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="8.4" punct="vg:5">Z<seg phoneme="ø" type="vs" value="1" rule="403" place="5" punct="vg" caesura="1">eu</seg>s</w>,<caesura></caesura> <w n="8.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="8.6">M<seg phoneme="ø" type="vs" value="1" rule="399" place="7">eu</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="9.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>r</w><caesura></caesura> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="9.6" punct="ps:10">fl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="ps">ai</seg>r</w>…</l>
							<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">chi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="10.3">d</w>’<w n="10.4">H<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" caesura="1">è</seg>s</w><caesura></caesura> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="10.8" punct="ps:10">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></w>…</l>
							<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>ts</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="11.4" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg" caesura="1">o</seg>rds</w>,<caesura></caesura> <w n="11.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="11.6" punct="vg:10">T<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" caesura="1">an</seg>s</w><caesura></caesura> <w n="12.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="12.6" punct="pe:10">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="pe">ai</seg>r</w> !</l>
						</lg>
					</div></body></text></TEI>