<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU29" modus="sm" lm_max="8" metProfile="8">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Niobé</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4" punct="vg:8">N<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="ps:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.7" punct="vg:8">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.5" punct="pt:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1">V<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="5.5" punct="ps:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>t</w> !</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="7.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="8.6" punct="pe:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>t</w> !</l>
						</lg>
					</div></body></text></TEI>