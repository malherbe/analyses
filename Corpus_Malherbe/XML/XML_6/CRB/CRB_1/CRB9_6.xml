<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÇA</head><head type="main_subpart">PARIS</head><div type="poem" key="CRB9" modus="sm" lm_max="8" metProfile="8">
						<lg n="1">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>c</w>, <hi rend="ital"><w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">tr<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w></hi> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="1.5" punct="dp:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">c</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>q</w>-<w n="3.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w>-<w n="3.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>lli<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4" punct="vg:8">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="4.2">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>c</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>t</w> <w n="4.6" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="dp:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="dp">a</seg>s</w> : <w n="5.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="5.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="vg:8">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="6.2" punct="vg:3">v<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="6.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <hi rend="ital"><w n="6.4">M<seg phoneme="œ" type="vs" value="1" rule="151" place="5">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>r</w> <w n="6.5">V<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w></hi></l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="7.6">f<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1" punct="vg:1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>s</w>, <w n="8.2" punct="pi:3">tr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pi ps">é</seg></w> ?… <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="8.4" punct="tc:5">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="5" punct="ti">oi</seg></w> — <w n="8.5">P<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7" punct="pe:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe ps">ou</seg>r</w> !…</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1">F<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2" punct="pe:3">b<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe ps ti">a</seg>l</w> !… — <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5" punct="pe:8">c<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ti">e</seg></w> ! —</l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.4" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="vg:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="11.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">p<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg></w> <w n="11.5" punct="vg:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>c</w>,</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="12.6" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="13" num="4.2" lm="8" met="8"><w n="13.1">B<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>c</w>-<w n="13.2" punct="vg:3">j<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="13.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6" punct="pe:8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe ps">an</seg>c</w> !…</l>
							<l n="14" num="4.3" lm="8" met="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="14.3">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="14.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>