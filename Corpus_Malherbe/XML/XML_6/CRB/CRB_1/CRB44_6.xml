<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB44" modus="sm" lm_max="7" metProfile="7">
					<head type="main">VENDETTA</head>
					<lg n="1">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.7" punct="dp:7">br<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="dp">a</seg>s</w> :</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1" punct="vg:2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="pe:7">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>yer<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe ps">a</seg>s</w> !…</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2" punct="tc:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="ti">e</seg></w> — <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="4.5" punct="pe:7">f<seg phoneme="a" type="vs" value="1" rule="193" place="7">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe ti">e</seg></w> ! —</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.7" punct="dp:7">p<seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="dp">eau</seg></w> :</l>
						<l n="6" num="2.2" lm="7" met="7"><w n="6.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.4" punct="pt:7">j<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="7" num="2.3" lm="7" met="7"><w n="7.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="7.2" punct="pe:3">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ps">e</seg></w> !… <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="8" num="2.4" lm="7" met="7"><w n="8.1">J<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="8.4" punct="vg:7">cr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="7" punct="vg">au</seg>d</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="7" met="7"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5" punct="vg:7">p<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.3">j</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.6" punct="vg:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>,</l>
						<l n="11" num="3.3" lm="7" met="7"><w n="11.1" punct="ps:2">P<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="ps">e</seg></w> … <w n="11.2" punct="tc:3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg ti">ai</seg>s</w>, — <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5" punct="tc:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg ti">e</seg></w>, —</l>
						<l n="12" num="3.4" lm="7" met="7"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3" punct="vg:6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="12.4" punct="pe:7">T<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pe">oi</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="7" met="7">— <w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg">o</seg>r</w>, <w n="13.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w>-<w n="13.6" punct="vg:7">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="7" met="7"><w n="14.1">S<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.4">S<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w></l>
						<l n="15" num="4.3" lm="7" met="7"><w n="15.1" punct="vg:1">Fr<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="vg">oi</seg>d</w>, <w n="15.2" punct="vg:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="15.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w></l>
						<l n="16" num="4.4" lm="7" met="7"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="16.3">p<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w>’<w n="16.6" punct="ps:7">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg></w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="7" met="7"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="17.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="17.5" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="17.6" punct="vg:7">P<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="7" met="7"><w n="18.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p</w> <w n="18.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="18.3">qu</w>’<w n="18.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="18.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6" punct="ps:7">cr<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="ps">oi</seg></w>…</l>
						<l n="19" num="5.3" lm="7" met="7"><w n="19.1">V<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w>-<w n="19.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.5" punct="pi:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi ps">e</seg></w> ?…</l>
						<l n="20" num="5.4" lm="7" met="7"><w n="20.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">v<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w>-<w n="20.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="20.5" punct="pe:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pe ps">oi</seg></w> !…</l>
					</lg>
				</div></body></text></TEI>