<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC28" modus="cm" lm_max="10" metProfile="4+6">
				<head type="main">Irma</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="1.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="1.7" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="415" place="7" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</w>, <w n="1.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="1.9" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="2.4">l</w>’<w n="2.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.8" punct="pt:10">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="pt">e</seg>il</w>.</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="3.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="3.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.9" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.7" punct="pe:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="pe">e</seg>il</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="5.4">c</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.8" punct="vg:10">tr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="6.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w> <w n="6.6" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg></w> !</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="7.4">c</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="7.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="7.9" punct="vg:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="8.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="8.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>x</w><caesura></caesura> <w n="8.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="8.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="8.8">d</w>’<w n="8.9" punct="pe:10"><seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="9.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7"><seg phoneme="u" type="vs" value="1" rule="426" place="8">où</seg></w> <w n="9.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="9.9" punct="vg:10">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">am</seg>p</w><caesura></caesura> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="10.7" punct="vg:10">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>rs</w>,</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="11.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="11.8" punct="ps:10">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg>s</w>…</l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="12.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="12.8" punct="pe:10">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>rs</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M/mp">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="Lp">ou</seg>rs</w>-<w n="13.3" punct="vg:4">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="4" punct="vg" caesura="1">e</seg></w>,<caesura></caesura> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.7" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="14.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="14.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" caesura="1">oin</seg>t</w><caesura></caesura> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="6" mp="M">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="14.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="14.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.11" punct="vg:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>r</w>,</l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="15.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4" caesura="1">um</seg>s</w><caesura></caesura> <w n="15.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ds</w> <w n="15.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="15.8" punct="ps:10">pr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg>s</w>…</l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="16.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fm">e</seg></w>-<w n="16.3" punct="pe:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pe" caesura="1">oi</seg></w> !<caesura></caesura> <w n="16.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="16.5">t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="16.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="16.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="16.8" punct="pe:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>r</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="10" met="4+6"><w n="17.1" punct="pe:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="pe">o</seg>rs</w> ! <w n="17.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="17.4" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pe" caesura="1">ai</seg>x</w> !<caesura></caesura> <w n="17.5" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>s</w>, <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="17.7">f<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.8" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
					<l n="18" num="5.2" lm="10" met="4+6"><w n="18.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe">oi</seg></w> ! <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="18.4">n</w>’<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5" mp="Lp">e</seg>st</w>-<w n="18.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="18.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="18.9" punct="pi:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pi">eu</seg>r</w> ?</l>
					<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M/mp">É</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fm">e</seg></w>-<w n="19.2" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="19.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="19.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="19.5" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="pe" mp="F">e</seg></w> ! <w n="19.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="19.7">t</w>’<w n="19.8" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
					<l n="20" num="5.4" lm="10" met="4+6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="20.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="20.6">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l</w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="20.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="20.9" punct="pe:10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="pe">œu</seg>r</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1853">1853</date>
					</dateline>
				</closer>
			</div></body></text></TEI>