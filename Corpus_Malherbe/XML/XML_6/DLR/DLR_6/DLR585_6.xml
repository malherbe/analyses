<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHEVAUX DE LA MER</head><div type="poem" key="DLR585" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="main">COLÈRE MARINE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g</w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rds</w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="1.9">l</w>’<w n="1.10" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>x<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.6" punct="vg:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">r<seg phoneme="y" type="vs" value="1" rule="457" place="3">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.5">s</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t</w> <w n="3.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="3.11">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">c<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="5.3">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.5" punct="ps:8">b<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="ps">u</seg>t</w>… <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">A</seg></w> <w n="5.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.9" punct="pe:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pe">e</seg>r</w> !</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="6.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="6.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.8" punct="vg:12">br<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="7.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="7.8">d</w>’<w n="7.9" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">j</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="9.8">l</w>’<w n="9.9">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="9.11">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>s</w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.8">br<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.9" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="11.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="11.9" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12" met="6+6">‒ <w n="13.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1" mp="Lp">eu</seg>x</w>-<w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4" punct="vg:5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="13.5" punct="pe:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pe" caesura="1">e</seg>r</w> !<caesura></caesura> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="13.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="13.9" punct="pi:12">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="14.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.7" punct="vg:12">v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="15.3" punct="vg:4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="15.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="15.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="15.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tru<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="15.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="15.9" punct="vg:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.3">l</w>’<w n="16.4" punct="pe:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>