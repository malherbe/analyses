<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE A CHEVAL</head><div type="poem" key="DLR559" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">FÉERIE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2" punct="vg:3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>l</w>, <w n="1.3">j</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.10">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="2.7" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" mp="P">è</seg>s</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="Lc">ou</seg>s</w>-<w n="3.9">b<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="3.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.11" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="4.2">fr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="4.5" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="5.2" punct="vg:3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>l</w>, <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="5.4" punct="vg:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>l</w>,<caesura></caesura> <w n="5.5">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="Fm">e</seg>s</w>-<w n="5.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="5.7">ch<seg phoneme="e" type="vs" value="1" rule="347" place="10" mp="P">ez</seg></w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.9" punct="pi:12">f<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</w> ?</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>pt</w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.9" punct="pt:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="7.4" punct="vg:6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="7.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.7" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>c<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>nt</w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="8.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="8.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="8.9" punct="pt:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="9.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="9.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">d</w>’<w n="9.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="9.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>g</w> <w n="9.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.11" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">h<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="4" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.6" punct="ps:12">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="11" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="ps">on</seg>s</w>…</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="vg">oi</seg>s</w>, <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="11.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="P">a</seg>r</w> <w n="11.9" punct="dp:12">m<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg>s</w> :</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="12.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5" punct="pe:6">r<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe" caesura="1">ou</seg>x</w> !<caesura></caesura> <w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">I</seg>l</w> <w n="12.7">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="12.8" punct="pe:9">r<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="pe">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="12.9"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="C">I</seg>l</w> <w n="12.10">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>t</w> <w n="12.11">j<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="13.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="13.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7" mp="M">ein</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="13.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.7" punct="pe:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pe">o</seg>ts</w> !</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="14.2" punct="vg:3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="14.4" punct="ps:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ps" caesura="1">a</seg>l</w>…<caesura></caesura> <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">Y</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="Lp">a</seg></w>-<w n="14.7">t</w>-<w n="14.8"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="14.9">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="14.10" punct="pi:12">ch<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="15.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="15.4" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="15.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="15.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="15.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="15.10" punct="vg:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.3">s</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="M">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="16.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="16.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="16.9" punct="pi:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>