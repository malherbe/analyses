<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR635" modus="sm" lm_max="8" metProfile="8">
					<head type="main">PRINTEMPS</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.5" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">F<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>il</w> <w n="2.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="3.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="5.4">d</w>’<w n="5.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="5.7">l</w>’<w n="5.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.9" punct="vg:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7" punct="pv:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="8.5">d</w>’<w n="8.6" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="9.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="9.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="10.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="10.4"><seg phoneme="ø" type="vs" value="1" rule="247" place="4">œu</seg>fs</w> <w n="10.5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s</w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="10.8" punct="vg:8">n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>d</w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3">n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="12.6" punct="pt:8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg></w> <w n="13.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="13.7" punct="vg:8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>t</w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="15.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.4" punct="vg:5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="vg">in</seg>s</w>, <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.3">l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="16.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="16.5" punct="pt:8">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="17.3" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="17.4" punct="pe:4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="pe">em</seg>ps</w> ! <w n="17.5">C</w>’<w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="17.7" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="17.8" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="18.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="18.6" punct="pe:8">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>rt</w> !</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w>-<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="19.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="20.5" punct="pi:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">m</w>’<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="21.4" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="21.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ls</w> <w n="21.7" punct="vg:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.5" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="23" num="6.3" lm="8" met="8">« <w n="23.1">Qu</w>’<w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w>-<w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="23.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="23.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="23.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="23.8" punct="pi:8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></w> ?… »</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="24.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="24.4" punct="pt:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8" met="8">‒ <w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="25.2" punct="vg:3">p<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg>s</w>, <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="25.4" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1" punct="vg:2">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="26.2" punct="vg:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg>x</w>, <w n="26.3" punct="vg:5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="26.4" punct="vg:6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="26.5" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="27.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="27.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="27.5" punct="vg:8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">J</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="e" type="vs" value="1" rule="170" place="2">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="28.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="28.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="28.7" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="8" punct="pt">y</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>