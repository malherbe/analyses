<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR638" modus="sm" lm_max="8" metProfile="8">
					<head type="main">ZEPPELINS</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="1.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">l</w>’<w n="1.7" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="2.2" punct="vg:5">s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="2.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w>-<w n="2.4" punct="pi:8">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>s</w> ?</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.5" punct="pi:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>g<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>s</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>rc<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="vg:8">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="6.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.4" punct="vg:8">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="8.3">l</w>’<w n="8.4" punct="pe:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pe:1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="pe">oi</seg>t</w> ! <w n="9.2">N<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="9.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="9.4" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w>-<w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5" punct="pt:8">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>l</w>.</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5" punct="vg:8">t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.6" punct="pt:8">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>l</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="13.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="13.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="13.5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rc</w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="15.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>c</w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">pl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="16.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="16.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">A</seg>rc</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="18.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6">qu</w>’<w n="18.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8" punct="pt">e</seg>st</w>.</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="19.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3" punct="pe:5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="pe">i</seg>t</w> ! <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>l</w> <w n="19.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="19.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>t</w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ts</w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="22.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="22.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.5" punct="pt:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w>-<w n="23.3" punct="vg:3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="23.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="23.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.6" punct="pt:8">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">ez</seg></w> ! <w n="24.2">V<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="24.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4">l</w>’<w n="24.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="24.6" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>