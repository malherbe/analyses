<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE A CHEVAL</head><div type="poem" key="DLR556" modus="cp" lm_max="10" metProfile="6, 5+5">
					<head type="main">AU TROT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2" punct="vg:2">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="1.4" punct="vg:4">tr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" punct="vg">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="1.6" punct="vg:6">tr<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg">o</seg>t</w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5" caesura="1">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="2.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.7" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="3.1">Fu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="3.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2" punct="vg:3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>l</w>, <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="4.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7" punct="vg:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="438" place="10" punct="vg">o</seg>t</w>,</l>
						<l n="5" num="1.5" lm="6" met="6"><space unit="char" quantity="8"></space><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2" punct="vg:2">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="5.4" punct="vg:4">tr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" punct="vg">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.6" punct="pt:6">tr<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="pt">o</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2" punct="vg:2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>t</w>, <w n="6.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.6" punct="pt:6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></w>.</l>
						<l n="7" num="2.2" lm="10" met="5+5"><w n="7.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="7.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="7.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" caesura="1">o</seg>l</w><caesura></caesura> <w n="7.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">fou<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.8" punct="ps:10">j<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></w>…</l>
						<l n="8" num="2.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.5" punct="pe:6">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg></w> !</l>
						<l n="9" num="2.4" lm="10" met="5+5"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="9.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="9.6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="9.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.8" punct="pt:10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="10" num="2.5" lm="6" met="6"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2" punct="vg:2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>t</w>, <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.6" punct="pt:6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="11.1" punct="pe:2">Tr<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg>s</w> ! <w n="11.2" punct="pe:4">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg>s</w> ! <w n="11.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pe">on</seg>s</w> !</l>
						<l n="12" num="3.2" lm="10" met="5+5"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="12.2">br<seg phoneme="y" type="vs" value="1" rule="454" place="2" mp="M">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" caesura="1">è</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="12.6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.7" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="13" num="3.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="13.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.4" punct="vg:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="3.4" lm="10" met="5+5"><w n="14.1">F<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="14.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9">o</seg>p</w> <w n="14.8" punct="vg:10">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>ds</w>,</l>
						<l n="15" num="3.5" lm="6" met="6"><space unit="char" quantity="8"></space><w n="15.1" punct="vg:2">Tr<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="15.2" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="15.3" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pe ps">on</seg>s</w> !…</l>
					</lg>
				</div></body></text></TEI>