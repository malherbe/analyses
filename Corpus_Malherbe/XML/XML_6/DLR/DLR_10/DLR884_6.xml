<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR884" modus="sm" lm_max="8" metProfile="8">
				<head type="main">MUSIQUE NOCTURNE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w>-<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4">d</w>’<w n="1.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="pt:3">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="pt">oi</seg>s</w>. <w n="3.2">P<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ds</w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2" punct="vg:3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>t</w>, <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rds</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="6.3">l</w>’<w n="6.4" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>bi<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="6.5">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.7" punct="pt:8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="7" num="1.7" lm="8" met="8">« <w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="7.3" punct="vg:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="7.7" punct="vg:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>-<w n="8.3" punct="vg:2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w>, <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="8.8" punct="pt:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="9.4" punct="vg:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg">en</seg></w>, <w n="9.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="9.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="9.7" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>. »</l>
					<l n="10" num="1.10" lm="8" met="8">— <w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w>-<w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="10.5">m</w>’<w n="10.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="7">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1" punct="pe:2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">er</seg></w> ! <w n="11.2" punct="pe:3">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="pe">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4">O</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="11.6" punct="pe:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pe">e</seg>il</w> !</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="12.2" punct="pt:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="pt">oi</seg></w>. <w n="12.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.6" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</w>,</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.7" punct="pt:8">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="14.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="14.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="14.7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="15" num="1.15" lm="8" met="8"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="15.5" punct="pt:8">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
					<l n="16" num="1.16" lm="8" met="8"><w n="16.1" punct="pi:2">M<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pi ps">i</seg>r</w> ?… <w n="16.2" punct="pi:4">V<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pi ps">e</seg></w> ?… <w n="16.3">T<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.5" punct="pt:8">n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>