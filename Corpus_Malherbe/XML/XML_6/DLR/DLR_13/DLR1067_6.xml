<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IV</head><head type="main_part">Musique</head><div type="poem" key="DLR1067" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="main">Appel</head>
					<head type="sub_2">PAR VENTS ET MARÉES, Fasquelle, 1910</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="1.2" punct="vg:3">B<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>ch</w>, <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="1.4" punct="vg:6">Sch<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="vg" caesura="1">a</seg>nn</w>,<caesura></caesura> <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="1.6" punct="vg:10">B<seg phoneme="e" type="vs" value="1" rule="15" place="8" mp="M">ee</seg>th<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="DLR1067_1" place="10" punct="vg">e</seg>n</w>, <w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="C">ou</seg>s</w> <w n="1.8" punct="vg:12">Gl<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>ck</w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>ls</w> <w n="2.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>x<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="3.5">d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.8">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="vg:8">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s</w> <w n="5.4">fru<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>ts</w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.8">r<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>gu<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>c</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>vi<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="7.4" punct="pe:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pe ps" caesura="1">oi</seg>r</w> !<caesura></caesura>… <w n="7.5">N<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="7.6">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rs</w> <w n="7.8">s<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="8.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="8.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="8.8" punct="pt:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</w>.</l>
						<l n="9" num="2.3" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="pe:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>s</w> ! <w n="9.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="9.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="9.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="9.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.10">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="Lc">ou</seg>s</w>-<w n="9.11" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="dp in">on</seg>d</w> : « <w n="10.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">t</w>’<w n="10.7" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> ! »</l>
						<l n="11" num="2.5" lm="12" met="6+6"><w n="11.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3" mp="M">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="11.6" punct="vg:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
						<l n="12" num="2.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3">br<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.6" punct="pe:8">m<seg phoneme="ø" type="vs" value="1" rule="402" place="8">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ts</w> <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="13.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.7" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>s</w>,</l>
						<l n="14" num="3.2" lm="12" met="6+6"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.4">l</w>’<w n="14.5" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>r</w>, <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="14.7">d</w>’<w n="14.8" punct="vg:12">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="15" num="3.3" lm="12" met="6+6"><w n="15.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="15.3" punct="dp:4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="dp">ou</seg>s</w> : <w n="15.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="15.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.7" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
						<l n="16" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1" punct="vg:2">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="16.3" punct="vg:5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="16.5" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
						<l n="17" num="3.5" lm="12" met="6+6"><w n="17.1" punct="vg:1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" punct="vg">en</seg>ds</w>, <w n="17.2" punct="vg:3">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="17.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rds</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="17.5">l<seg phoneme="i" type="vs" value="1" rule="493" place="6" caesura="1">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="17.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="C">ou</seg>s</w> <w n="17.9" punct="pt:12">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
						<l n="18" num="3.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="18.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="18.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="18.7" punct="pe:8">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>