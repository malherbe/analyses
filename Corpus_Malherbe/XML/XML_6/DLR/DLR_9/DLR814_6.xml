<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">DE LA VIE A LA MORT</head><div type="poem" key="DLR814" modus="cm" lm_max="10" metProfile="5+5">
					<head type="main">HYPOTHÈSE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="1.5" punct="dp:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="dp in" caesura="1">ou</seg>r</w> :<caesura></caesura> « <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">I</seg>l</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.8" punct="pt:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></w>. »</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" mp="P">e</seg>rs</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.5" punct="vg:5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="2.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="2.9" punct="pt:10">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="3.2" punct="vg:3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>l</w>, <w n="3.3" punct="vg:5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="5" punct="vg" caesura="1">ue</seg>il</w>,<caesura></caesura> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.5" punct="pe:10">t<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="4.8">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t</w> <w n="4.9" punct="pt:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="Lc">au</seg></w>-<w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mc">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="5.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.10">l</w>’<w n="5.11" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg></w><caesura></caesura> <w n="6.5">qu</w>’<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="6.9">qu</w>’<w n="6.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="6.11" punct="vg:10">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="vg">ain</seg>t</w>,</l>
						<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="Lc">au</seg></w>-<w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mc">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.5" punct="vg:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg" caesura="1">o</seg>rts</w>,<caesura></caesura> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.8" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pt">ain</seg></w>.</l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M/mp">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="Lp">on</seg>s</w>-<w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="8.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="8.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.8" punct="pi:10">pl<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="Lc">au</seg></w>-<w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mc">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.5" punct="vg:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg" caesura="1">o</seg>rts</w>,<caesura></caesura> <w n="9.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8" punct="vg:10">f<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>l</w>,</l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x</w> <w n="10.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5" caesura="1">e</seg>ds</w><caesura></caesura> <w n="10.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="10.8">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="10.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="10.11" punct="pt:10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="11.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="11.8">m<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="12.5">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="4" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="12.6">l</w>’<w n="12.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.9" punct="pi:10">m<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pi">a</seg>l</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1" mp="C">e</seg>t</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>l</w><caesura></caesura> <w n="13.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="13.6">s</w>’<w n="13.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="14.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="14.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" mp="M">aî</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="14.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="14.6" punct="vg:10">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>r</w>,</l>
						<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">N</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="15.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="15.5" punct="vg:5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="15.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.8">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l</w> <w n="15.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="15.10" punct="vg:10">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="16" num="4.4" lm="10" met="5+5"><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">qu</w>’<w n="16.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="16.4">n<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="16.5" punct="vg:5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="16.7" punct="pi:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pi">eu</seg>r</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10" met="5+5"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="17.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>s</w><caesura></caesura> <w n="17.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" mp="P">e</seg>rs</w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="18" num="5.2" lm="10" met="5+5"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="18.4" punct="vg:5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="18.5">l</w>’<w n="18.6" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg></w>, <w n="18.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="18.8" punct="vg:9">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="18.9">l</w>’<w n="18.10" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>r</w>,</l>
						<l n="19" num="5.3" lm="10" met="5+5"><w n="19.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="dc-1" place="4" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg></w><caesura></caesura> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="19.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="20" num="5.4" lm="10" met="5+5"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="20.3">l</w>’<w n="20.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="20.6">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="20.7">l</w>’<w n="20.8" punct="pt:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="pt">ai</seg>r</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="10" met="5+5"><w n="21.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="21.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" mp="C">e</seg>t</w> <w n="21.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">In</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg></w><caesura></caesura> <w n="21.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C">u</seg></w> <w n="21.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="21.7">gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="22" num="6.2" lm="10" met="5+5"><w n="22.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="22.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="22.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rv<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="22.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="22.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="22.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="22.8" punct="vg:10">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10" punct="vg">en</seg></w>,</l>
						<l n="23" num="6.3" lm="10" met="5+5"><w n="23.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="23.2">c</w>’<w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="23.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="23.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="23.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w></l>
						<l n="24" num="6.4" lm="10" met="5+5"><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="24.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="24.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="24.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="24.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="24.8" punct="pi:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="10" met="5+5"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="25.2">c</w>’<w n="25.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="25.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="25.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="25.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="25.7">li<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w></l>
						<l n="26" num="7.2" lm="10" met="5+5"><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="26.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="26.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="26.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="26.7" punct="pi:10"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg>s</w> ?</l>
						<l n="27" num="7.3" lm="10" met="5+5">‒ <w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="e" type="vs" value="1" rule="354" place="3" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="27.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="27.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="27.7" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="28" num="7.4" lm="10" met="5+5"><w n="28.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="28.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="28.3" punct="ps:5">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="ps" caesura="1">o</seg>l</w>…<caesura></caesura> <w n="28.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="28.6" punct="pe:10">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg></w> !</l>
					</lg>
				</div></body></text></TEI>