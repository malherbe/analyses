<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">SPECTRES</head><div type="poem" key="DLR837" modus="sm" lm_max="8" metProfile="8">
					<head type="main">DEUX BARQUES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="1.2" punct="vg:3">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.6">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="3.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.8" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.6" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w>-<w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="7.6">l</w>’<w n="7.7" punct="vg:8">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.5" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3" punct="vg:4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.4" punct="pe:5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg>h</w> ! <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="9.7" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="10.4" punct="vg:5">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="10.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>rs</w> <w n="10.6" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ls</w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.7"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.9">P<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="12.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.4" punct="pe:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>ls</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="13.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="13.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="14.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.7" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>r</w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="15.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="16.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="16.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pe">e</seg>r</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="17.7">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="18.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="18.5" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="19.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="19.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="20.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="20.5" punct="pt:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="21.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">d</w>’<w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="21.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="22.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="22.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="22.6" punct="vg:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rts</w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="23.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="23.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="23.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1" punct="vg:1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="24.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="24.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="24.5" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rs</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8" met="8">— <w n="25.1" punct="vg:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="25.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="25.3" punct="vg:5">v<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg">eu</seg>fs</w>, <w n="25.4">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="25.5" punct="vg:8">v<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1" punct="vg:3">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="26.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ts</w> <w n="26.3" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="dp">é</seg>s</w> :</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="27.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="27.4" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="28.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>nt</w>, <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="28.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="28.5" punct="pe:8">n<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>