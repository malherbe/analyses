<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">CI-GÎT</head><div type="poem" key="DLR822" modus="cp" lm_max="12" metProfile="8, 6, 6+6, 5+5">
					<head type="main">LA SOMME</head>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">d</w>’<w n="1.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.7" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="6" caesura="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg>t</w> <w n="2.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="3.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>s</w>, <w n="3.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt</w></l>
							<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg" mp="F">e</seg>s</w>, <w n="5.2" punct="vg:3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>cs</w>, <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="5.7" punct="vg:12">r<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="6.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.6">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
							<l n="7" num="2.3" lm="12" met="6+6">‒ <w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="7.9">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>rs</w> <w n="7.10" punct="vg:12">tr<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
							<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="8.4" punct="pe:6">f<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg>s</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1" mp="C">e</seg>t</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">fr<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="9.7">br<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="9.9" punct="vg:12">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="vg">oin</seg></w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.3">t<seg phoneme="y" type="vs" value="1" rule="463" place="5" mp="M">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.7" punct="vg:12">S<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="11.4" punct="dp:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="dp in" caesura="1">e</seg>l</w> :<caesura></caesura> « <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="11.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="11.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>s</w> <w n="11.8">t<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12">oin</seg></w></l>
							<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l</w>’<w n="12.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.5">h<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="476" place="5">ï</seg></w> <w n="12.6">l</w>’<w n="12.7" punct="pe:6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> ! »</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>s</w><caesura></caesura> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="13.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
							<l n="14" num="1.2" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="2" mp="M">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="14.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="14.5" punct="vg:8">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="6" mp="M">um</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>, <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="14.7" punct="pe:10">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg></w> !</l>
							<l n="15" num="1.3" lm="12" met="6+6"><w n="15.1" punct="vg:2">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>ts</w>, <w n="15.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="15.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="15.6">d<seg phoneme="e" type="vs" value="1" rule="353" place="10" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
							<l n="16" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.8" punct="pt:8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="17" num="2.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="17.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w> <w n="17.3">tr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>ts</w><caesura></caesura> <w n="17.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="M">eu</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
							<l n="18" num="2.2" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="18.1">B<seg phoneme="u" type="vs" value="1" rule="d-2" place="1" mp="M">ou</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="18.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="18.3">r<seg phoneme="u" type="vs" value="1" rule="428" place="4" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="18.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="18.6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="18.7" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>s</w>,</l>
							<l n="19" num="2.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="19.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="19.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
							<l n="20" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">H<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="20.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.4" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="21.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="21.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="22" num="3.2" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="22.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="22.3">dr<seg phoneme="e" type="vs" value="1" rule="353" place="4" mp="M">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="22.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="22.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="22.6" punct="pt:10">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>x</w>.</l>
							<l n="23" num="3.3" lm="12" met="6+6"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="23.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="23.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="23.5">d</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="23.7" punct="vg:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>, <w n="23.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</w>,</l>
							<l n="24" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="24.4" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1" lm="10" met="5+5"><space unit="char" quantity="4"></space>‒ <w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="25.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="25.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="5" caesura="1">y</seg>rs</w><caesura></caesura> <w n="25.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="25.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9" mp="P">e</seg>rs</w> <w n="25.6" punct="vg:10">m<seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="vg">oi</seg></w>,</l>
							<l n="26" num="4.2" lm="10" met="5+5"><space unit="char" quantity="4"></space><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="26.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>rs</w><caesura></caesura> <w n="26.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C">u</seg></w> <w n="26.5">cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="26.6" punct="vg:10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="27" num="4.3" lm="12" met="6+6"><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="27.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="27.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="27.4" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="27.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="27.6">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="27.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="27.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
							<l n="28" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="dp in">er</seg></w> : « <w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">E</seg>st</w>-<w n="28.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="28.5" punct="pe:8">p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> ! »</l>
						</lg>
					</div>
				</div></body></text></TEI>