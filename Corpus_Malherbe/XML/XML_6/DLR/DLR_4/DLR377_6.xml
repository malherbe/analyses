<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BARBARESQUES</head><div type="poem" key="DLR377" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">SIRÈNE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="1.5">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">cl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.5">c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>x</w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M/mp">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="Lp">a</seg></w>-<w n="3.2">t</w>-<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</w> <w n="3.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.8">n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="4.9" punct="pi:12"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pi">eau</seg>x</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="5.4" punct="pe:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>s</w> ! <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.9">f<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="5.11">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="5.12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="6.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="6.6">qu</w>’<w n="6.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="6.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="6.9">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="9" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="6.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.11" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="7.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="7.5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="7.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="7.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="7.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>l</w> <w n="7.10">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r</w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="8.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6" punct="vg:12">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">gr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>r</w>, <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.5" punct="vg:6">gl<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="9.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w> <w n="9.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t</w> <w n="9.10">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>gs</w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.7" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.5" punct="vg:6">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rd</w>,<caesura></caesura> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="11.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2">d</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="12.4">n<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="12.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="12.9" punct="pt:12">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>ds</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="13.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="13.3">ch<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="13.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="13.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="13.8">m</w>’<w n="13.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="14.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="Lc">u</seg>squ</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="14.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="14.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.8">gl<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="14.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="14.10">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rps</w> <w n="14.11" punct="vg:12">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>d</w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1" punct="vg:3">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>sl<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="15.7">qu</w>’<w n="15.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.9" punct="pt:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="16.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="16.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="16.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="16.8" punct="pt:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pt">oi</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="Lc">o</seg>rsqu</w>’<w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="17.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="17.7">cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="17.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="17.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="17.10"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">m</w>’<w n="18.3" punct="tc:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3" punct="vg ti">ein</seg>t</w>, — <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="18.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="18.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="18.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="18.9">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="18.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="18.11" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>rs</w>.</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1" punct="vg:2">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg></w>, <w n="19.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="19.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="19.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="19.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>c</w><caesura></caesura> <w n="19.6">d</w>’<w n="19.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="19.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="19.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="19.10" punct="pt:12">ste<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>rs</w>.</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="20.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="20.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="20.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="20.7">pl<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="20.8" punct="ps:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg>s</w>…</l>
					</lg>
				</div></body></text></TEI>