<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ORANAIS ET KABYLES</head><div type="poem" key="DLR443" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">VISAGES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ts</w> <w n="2.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="2.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">O</seg>r<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3">fr<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.6">s</w>’<w n="4.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="4">y</seg>s</w> <w n="5.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="5.9">l</w>’<w n="5.10" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1" punct="vg:1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">d</w>’<w n="6.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="6.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.8" punct="pt:12">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.7">s</w>’<w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3" punct="vg:3">S<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>d</w>, <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="8.5">l</w>’<w n="8.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="8.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="8.9" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w> <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="9.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="9.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="10" num="5.2" lm="12" met="6+6"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.3">B<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>d<seg phoneme="u" type="vs" value="1" rule="dc-2" place="4" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">â</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ffl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.7" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>