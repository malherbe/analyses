<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ORANAIS ET KABYLES</head><div type="poem" key="DLR444" modus="cm" lm_max="12" metProfile="6÷6">
					<head type="main">A TLEMCEN</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" mp6="C" met="6−6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2" punct="vg:3">Tl<seg phoneme="e" type="vs" value="1" rule="DLR444_1" place="2" mp="M">e</seg>mc<seg phoneme="e" type="vs" value="1" rule="DLR444_2" place="3" punct="vg">e</seg>n</w>, <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="1.5" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</w>, <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.10">ch<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>x</w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="2.2">j</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="2.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">A</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="2.10">m<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.4">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="3.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="3.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="3.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="3.10">ch<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>d</w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="4.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="4.4" punct="vg:6">tr<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.9" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>squ<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.6">l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
						<l n="6" num="2.2" lm="12" mp6="M" mp4="Mem" mp8="M" met="12"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="6.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="6.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="7.2">l</w>’<w n="7.3" punct="vg:3"><seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.5" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="7.6">l</w>’<w n="7.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pt">en</seg>t</w>.</l>
						<l n="8" num="2.4" lm="12" mp6="M" met="4+4+4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">j</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8" caesura="2">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w><caesura></caesura> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.7" punct="pt:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="o" type="vs" value="1" rule="435" place="1" mp="M/mp">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M/mp">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="9.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="9.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="12">en</seg></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="10.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="10.5">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.7">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="11.8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="12.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="12.10" punct="pi:12">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pi">en</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>