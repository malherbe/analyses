<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR357" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">SOUDANAIS</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">im</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.7">n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.9" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>ls</w><caesura></caesura> <w n="3.5" punct="vg:7">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="3.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r</w> <w n="3.9">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t</w></l>
						<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1" punct="vg:1">Gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.9">p<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="4.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.11" punct="pt:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>rs</w> <w n="5.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.7">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="5.8">s<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="6.3" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="6.5">r<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.8" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="7.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t</w><caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.8" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>rs<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>s</w>,</l>
						<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.6" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="9.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="9.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="9.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="9.9">n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.10" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="10" num="5.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">Ai</seg></w>-<w n="10.2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="10.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>s</w><caesura></caesura> <w n="10.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.8">qu</w>’<w n="10.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="10.10">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="10.11">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.12">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.13" punct="pi:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</w> ?</l>
					</lg>
				</div></body></text></TEI>