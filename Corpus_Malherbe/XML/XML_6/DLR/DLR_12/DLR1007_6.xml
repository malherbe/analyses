<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
				<change when="2023-06-13" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DLR1007" modus="cp" lm_max="12" metProfile="4, 8, 6+6, (3)">
				<head type="main">FUGUE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="1.5">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.10">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>f</w></l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="2.1">S</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w>-<w n="2.3" punct="pi:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi ps">e</seg>s</w> ?…</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w>-<w n="4.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="4.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="4.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="4.11" punct="pi:12">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="5.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rps</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="5.7" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="6" num="2.2" lm="3"><space unit="char" quantity="16"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="ps:3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="ps">e</seg></w>…</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="7.5">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.10" punct="vg:12">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>f</w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.8" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w>-<w n="9.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="9.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="9.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="9.11" punct="pi:12">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></w> ?</l>
					<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.3" punct="vg:4">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="11.8">t</w>’<w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>s</w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.7" punct="pi:8">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="315" place="1">Eau</seg></w> <w n="13.2" punct="vg:2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.3" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe">a</seg>h</w> ! <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="13.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.7" punct="pe:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="13.8" punct="pe:9"><seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="pe">A</seg>h</w> ! <w n="13.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="13.10">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="13.11" punct="pe:12">pu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pe ps">i</seg>s</w> !…</l>
					<l n="14" num="4.2" lm="8" met="8">— <w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="14.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="15.5">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="15.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="15.10">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>f</w></l>
					<l n="16" num="4.4" lm="4" met="4"><space unit="char" quantity="16"></space><w n="16.1">S</w>’<w n="16.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w>-<w n="16.3" punct="pi:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi ps">e</seg>s</w> ?…</l>
				</lg>
			</div></body></text></TEI>