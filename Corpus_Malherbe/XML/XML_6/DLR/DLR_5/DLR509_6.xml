<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR509" modus="sm" lm_max="8" metProfile="8">
					<head type="main">VIA APPIA</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5" punct="vg:8">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="8">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3" punct="vg:5">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.6" punct="pt:8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="8" met="8"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="3.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5" punct="pi:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></w> ?…</l>
						<l n="4" num="2.2" lm="8" met="8"><w n="4.1" punct="vg:4">St<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="4.2" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="4.3" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="8" met="8"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.7" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="6" num="3.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rts</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="6.7">l</w>’<w n="6.8" punct="pt:8">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="8" met="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="7.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>d</w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="7.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="7.6" punct="vg:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="4.2" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.8" punct="pt:8">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="8" met="8"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2" punct="vg:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="25" place="7">e</seg>n</w> <w n="9.5">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">im</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="10" num="5.2" lm="8" met="8"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg>ts</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6">l</w>’<w n="10.7" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">O</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8">ym</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="11.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="12" num="6.2" lm="8" met="8"><w n="12.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rts</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.7" punct="pt:8">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1" lm="8" met="8">— <w n="13.1">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="1">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2" punct="vg:3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" punct="vg">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="13.4">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="5">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="7.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="14.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.6" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1" lm="8" met="8"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457" place="3">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="8.2" lm="8" met="8"><w n="16.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="16.3" punct="pi:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></w> ?…</l>
					</lg>
				</div></body></text></TEI>