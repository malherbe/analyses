<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DES BÊTES</head><div type="poem" key="DLR286" modus="cp" lm_max="12" metProfile="5÷6">
					<head type="main">HEURES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="11" met="5+6"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="1.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="1.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="1.8" punct="vg:11">b<seg phoneme="a" type="vs" value="1" rule="307" place="10" mp="M">â</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="11" punct="vg">er</seg></w>,</l>
						<l n="2" num="1.2" lm="11" met="6+5" mp5="M"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.3" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="2.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.7" punct="vg:11">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="11" met="5+6"><w n="3.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.3" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="3.5" punct="vg:7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="7" punct="vg">en</seg>s</w>, <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="3.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="3.8" punct="vg:11">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="4" num="1.4" lm="11" met="5−6" mp5="C" mp6="M"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C" caesura="1">au</seg></w><caesura></caesura> <w n="4.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>il</w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="4.7" punct="pt:11">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="307" place="10" mp="M">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="11" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10" met="10" mp4="C" mp6="F"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2" punct="ps:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="ps" mp="F">e</seg>s</w>… <w n="5.3">L<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>r</w> <w n="5.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="5.5">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.7" punct="vg:10">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="2.2" lm="11" met="5+6"><w n="6.1">N<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="6.3" punct="pt:3">j<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="pt">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">E</seg>t</w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg></w><caesura></caesura> <w n="6.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="6.8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="6.10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="6.11">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rt</w></l>
						<l n="7" num="2.3" lm="11" met="5+6"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="7.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.3">c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ls</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.8">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rd</w> <w n="7.9">d</w>’<w n="7.10"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.11" punct="pt:11"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></w>.</l>
						<l n="8" num="2.4" lm="11" met="6+5" mp5="M"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.5" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.7">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="Lc">en</seg>tr</w>’<w n="8.9" punct="pt:11"><seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M/mc">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" punct="pt">e</seg>rt</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="11" mp6="F" met="5−6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>q</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="9.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>lb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w><caesura></caesura> <w n="9.6">p<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="9.8">d<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="9.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg>s</w></l>
						<l n="10" num="3.2" lm="11" met="5+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="10.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="10.6">qu<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.8" punct="vg:11">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" punct="vg">on</seg>d</w>,</l>
						<l n="11" num="3.3" lm="11" met="6+5" mp5="Mem"><w n="11.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ts</w><caesura></caesura> <w n="11.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg>s</w></l>
						<l n="12" num="3.4" lm="11" met="5+6"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2" punct="vg:3">D<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="12.3" punct="vg:5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="12.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="12.7" punct="pt:11">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" mp="M">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="11" met="6+5" mp5="M"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="13.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="13.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="13.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9" punct="pt:11">p<seg phoneme="a" type="vs" value="1" rule="307" place="11">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></w>.</l>
						<l n="14" num="4.2" lm="11" met="5+6"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="14.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>c</w><caesura></caesura> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="14.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="7">œ</seg>il</w> <w n="14.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8" punct="vg:11">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11" punct="vg">e</seg>c</w>,</l>
						<l n="15" num="4.3" lm="11" mp5="F" mp6="P" met="11"><w n="15.1">Ch<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="15.3">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="15.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="15.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>p</w> <w n="15.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="15.9">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w></l>
						<l n="16" num="4.4" lm="11" met="6+5" mp5="Pem"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="16.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="16.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="16.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.8" punct="ps:11">v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="307" place="11">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="ps" mp="F">e</seg></w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="11" met="6+5" mp5="C"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="17.2" punct="vg:2">cr<seg phoneme="i" type="vs" value="1" rule="469" place="2" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="17.4" punct="vg:4">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>d</w>, <w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="17.6" punct="vg:6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="17.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="17.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="17.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="17.10" punct="vg:11">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" punct="vg">ou</seg>ps</w>,</l>
						<l n="18" num="5.2" lm="11" met="5+6"><w n="18.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="18.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="18.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="18.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" caesura="1">aim</seg></w><caesura></caesura> <w n="18.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="18.6" punct="vg:11"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="11">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></w>,</l>
						<l n="19" num="5.3" lm="11" met="6+5" mp5="M"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="19.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="19.7">p<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">em</seg>pt<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="19.9" punct="pv:11">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" punct="pv">ou</seg>x</w> ;</l>
						<l n="20" num="5.4" lm="12" met="12" mp6="P"><w n="20.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="20.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="20.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="20.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="20.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="20.6" punct="ps:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="ps">i</seg>r</w>… <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">E</seg>t</w> <w n="20.8">c</w>’<w n="20.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="20.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="20.11" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>