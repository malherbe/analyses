<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR254" modus="cm" lm_max="12" metProfile="6=6">
					<head type="main">LES CHALANDS</head>
					<opener>
						<salute>A Ch. Th. Féret.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12" mp6="Pem" met="6−6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="1.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6" punct="vg:9">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="1.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ds</w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="C">eu</seg>rs</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="2.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>ds</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="2.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>x</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="2.8">l</w>’<w n="2.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="3.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.4" punct="vg:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.7" punct="vg:11">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="vg" mp="F">e</seg></w>, <w n="3.8" punct="vg:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>ts</w>,</l>
						<l n="4" num="1.4" lm="12" mp6="C" met="6−6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2">p<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="4.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="4.7">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.8" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="5.6" punct="vg:9">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg>s</w>, <w n="5.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="5.8" punct="pt:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</w>.</l>
						<l n="6" num="2.2" lm="12" mp6="C" met="6−6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C" caesura="1">eu</seg>r</w><caesura></caesura> <w n="6.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="6.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="6.8"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="C">i</seg>ls</w> <w n="6.9">s</w>’<w n="6.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>nt</w>,</l>
						<l n="7" num="2.3" lm="12" mp6="M" mp4="F" met="8+4"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="7.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8" caesura="1">er</seg></w><caesura></caesura> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="7.9">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="8.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="8.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.7" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.4">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="9.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="9.9" punct="vg:12">gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="10.6">m</w>’<w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="10.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="11">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.10" punct="vg:12"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6">— <w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="11.8">R<seg phoneme="u" type="vs" value="1" rule="dc-2" place="8" mp="M">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.11" punct="tc:12">H<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg ti" mp="F">e</seg></w>, —</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>st<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="12.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>p</w> <w n="12.7" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>ls</w> <w n="13.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="13.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="Lc">u</seg>squ</w>’<w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="13.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.9">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.10" punct="vg:12">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="14.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="14.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>rs</w> <w n="14.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rds</w><caesura></caesura> <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>s</w> <w n="14.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>s</w> <w n="14.9">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r</w></l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2" punct="vg:2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2" punct="vg">en</seg>s</w>, <w n="15.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="15.4">qu</w>’<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="15.7" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="15.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="15.10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.11">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="16.2" punct="ps:3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="ps">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">E</seg>t</w> <w n="16.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="16.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="16.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="16.10" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>