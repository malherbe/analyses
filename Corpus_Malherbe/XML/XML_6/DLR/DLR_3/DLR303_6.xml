<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">△</head><div type="poem" rhyme="none" key="DLR303" modus="cp" lm_max="12" metProfile="8, 4−6, 3=6, 5=6, 6=6">
					<head type="main">VERTIGE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4−6" mp4="C"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="1.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="1.4">F<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="1.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
						<l n="2" num="1.2" lm="9" mp4="F" met="6+3"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="2.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w></l>
						<l n="3" num="1.3" lm="11" met="5+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="3.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.4" punct="vg:11">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="4.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rds</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.9" punct="pt:12">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="2">
						<l rhyme="none" n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="pe:1">Sph<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" punct="pe">in</seg>x</w> ! <w n="5.2" punct="pe:2">Sph<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="pe">in</seg>x</w> ! <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="5.6" punct="pv:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv" caesura="1">ou</seg>s</w> ;<caesura></caesura> <w n="5.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="5.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.10" punct="pv:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l rhyme="none" n="6" num="2.2" lm="10" met="4+6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3" punct="vg:4">h<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">n</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg>s</w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="6.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="6.9" punct="pv:10">gr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="7.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.10" punct="ps:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="ps">i</seg>t</w>…</l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="8.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="Lp">en</seg>t</w>-<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="8.8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="C">i</seg></w> <w n="8.9">nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w></l>
						<l n="9" num="2.5" lm="12" met="6+6"><w n="9.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="P">o</seg>rs</w> <w n="9.2">d</w>’<w n="9.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="9.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="P">o</seg>rs</w> <w n="9.5">d</w>’<w n="9.6" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.9">v<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.10"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.11">l</w>’<w n="9.12" punct="pi:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pi">i</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1" lm="9" met="3+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3" caesura="1">eu</seg>x</w>-<w n="10.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg>s</w></l>
						<l n="11" num="3.2" lm="8" met="8"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5" punct="vg:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>,</l>
						<l n="12" num="3.3" lm="8" met="8"><w n="12.1">M<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="12.4">c<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w></l>
						<l n="13" num="3.4" lm="11" met="6+5" mp5="Mem"><w n="13.1">Dr<seg phoneme="e" type="vs" value="1" rule="353" place="1" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>s</w><caesura></caesura> <w n="13.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="13.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="13.7"><seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>f</w> <w n="13.8" punct="pi:11">f<seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pi" mp="F">e</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l rhyme="none" n="14" num="4.1" lm="12" met="6+6"><w n="14.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="14.6" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rt</w>,<caesura></caesura> <w n="14.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="14.9"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="14.10">ch<seg phoneme="e" type="vs" value="1" rule="347" place="11" mp="P">ez</seg></w> <w n="14.11" punct="dp:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="dp">oi</seg></w> :</l>
						<l rhyme="none" n="15" num="4.2" lm="10" met="4+6"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="15.2">D<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4" caesura="1">e</seg>st</w><caesura></caesura> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="15.7" punct="pe:10">gr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</w> !</l>
						<l n="16" num="4.3" lm="12" met="6+6"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="16.2">D<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="16.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="16.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="16.8" punct="pe:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</w> !</l>
						<l n="17" num="4.4" lm="12" met="6+6"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="17.2">D<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ls</w> <w n="17.8" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</w> !</l>
						<l n="18" num="4.5" lm="12" mp6="Fc" met="4+4+4"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="18.2" punct="vg:4">c<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="18.4" punct="vg:9">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="8" caesura="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>,<caesura></caesura> <w n="18.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="18.7" punct="ps:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="12" punct="ps">en</seg>s</w>…</l>
					</lg>
					<lg n="5">
						<l n="19" num="5.1" lm="12" mp6="C" met="6−6">— <w n="19.1" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>g<seg phoneme="i" type="vs" value="1" rule="493" place="2" punct="vg">y</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="19.3" punct="pe:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pe">oi</seg></w> ! <w n="19.4">J</w>’<w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="19.7">D<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="19.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="19.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="19.10" punct="pe:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>g</w> !</l>
					</lg>
				</div></body></text></TEI>