<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR135" modus="cm" lm_max="12" metProfile="6−6">
				<head type="main">DÉDICACE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1" mp="C">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.7" punct="vg:12">ch<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">Â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="2.3">t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t</w> <w n="2.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.7" punct="vg:10">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="2.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="2.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>x</w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="3.5" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>, <w n="3.6">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="3.8">r<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="4.7">l<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rds</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="4.9" punct="pt:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="5.2" punct="vg:2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg>t</w>, <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="5.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="5.9">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="6.8" punct="vg:12">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">î</seg>t</w> <w n="8.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="8.7" punct="pt:12">n<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="pt">au</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="9.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="9.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="10.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="10.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="10.8" punct="vg:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">V<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.6" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="11.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.9">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="11.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="11.11" punct="vg:12">m<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rt</w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="y" type="vs" value="1" rule="391" place="1">Eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" mp="P">e</seg>rs</w> <w n="12.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="12.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w> <w n="12.7" punct="ps:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg>s</w>…</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="13.2" punct="pe:3">P<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pe">an</seg>t</w> ! <w n="13.3">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="13.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="13.6">t</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="13.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></w></l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="14.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>s</w> <w n="14.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.9">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="14.10" punct="vg:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="15" num="4.3" lm="12" mp6="C" met="6−6"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="15.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="15.5">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="15.7" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="11" mp="M">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>ls</w> <w n="16.2" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>ts</w>, <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="16.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="16.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="16.8">tr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="16.10" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="17.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="17.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="17.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="18.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="18.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="18.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="18.10">j<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r</w> <w n="18.11" punct="vg:12">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="19.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>ssi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="19.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="19.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="19.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="19.8">s</w>’<w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="11" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="20.2" punct="vg:3">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="20.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="20.6">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="20.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="20.9" punct="pe:12">Ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>