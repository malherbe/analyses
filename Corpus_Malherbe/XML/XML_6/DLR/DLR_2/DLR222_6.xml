<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HEURES INTIMES</head><div type="poem" key="DLR222" modus="cm" lm_max="12" metProfile="6=6">
					<head type="main">A LA VITRE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6" punct="pt:9"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="pt" mp="F">e</seg>s</w>. <w n="1.7">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.8">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></w></l>
						<l n="2" num="1.2" lm="12" mp6="C" met="6−6"><w n="2.1">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd</w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="2.5">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C" caesura="1">on</seg></w><caesura></caesura> <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="3.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.6" punct="vg:10">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.8" punct="ps:12">plu<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M/mc">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="Lc">ou</seg>rd</w>’<w n="4.2" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="4.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.6">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.9" punct="pt:12">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.9">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">s</w>’<w n="6.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="6.9" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12" mp6="C" met="6−6"><w n="7.1" punct="vg:4">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" mp="P">e</seg>rs</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C" caesura="1">un</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="Lc">à</seg></w>-<w n="7.7" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="Lp">i</seg></w>-<w n="8.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="8.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="8.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="11">en</seg></w> <w n="8.9" punct="pv:12">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="9.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="9.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="9.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">N</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="10.4">s</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="10.9" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
						<l n="11" num="3.3" lm="12" mp7="F" met="4+4+4"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="11.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" caesura="2">a</seg>s</w><caesura></caesura> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="12.7" punct="pt:12">f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>