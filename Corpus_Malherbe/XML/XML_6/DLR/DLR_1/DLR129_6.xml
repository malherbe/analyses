<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR129" modus="cm" lm_max="12" metProfile="6−6">
					<head type="main">BEAU JOUR</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="8" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="1.8">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="1.9">j<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t</w> <w n="2.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="2.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="2.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="2.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="2.10">d</w>’<w n="2.11"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3" punct="vg:3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="3.9" punct="vg:12">j<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>nt</w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>nt</w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="6.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.9" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="7.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="7.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="7.7" punct="vg:9">j<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>r</w>, <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="8.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>x</w><caesura></caesura> <w n="8.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="8.9">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="10">om</seg></w> <w n="8.10">qu</w>’<w n="8.11"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l</w> <w n="8.12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="9.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.4" punct="pv:5"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pv">i</seg></w> ; <w n="9.5">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.6" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1" lm="12" mp6="C" met="6−6">… <w n="11.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="11.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="11.7" punct="vg:9">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="7" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="11.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="11.9">l</w>’<w n="11.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="12" num="2.2" lm="12" met="6+6"><w n="12.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" mp="P">e</seg>rs</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>t</w><caesura></caesura> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.8">s</w>’<w n="12.9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="13" num="2.3" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="13.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.9">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</w></l>
						<l n="14" num="2.4" lm="12" met="6+6"><w n="14.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="14.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="14.5">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="14.8">tr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="14.10" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>