<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES II</head><div type="poem" key="DLR66" modus="cp" lm_max="12" metProfile="8, 4, 5÷6, (5), (3)">
					<head type="main">REQUIEM</head>
					<lg n="1">
						<l n="1" num="1.1" lm="11" met="6+5" mp5="P"><w n="1.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>qu<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="1.3" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.6" punct="pt:11">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></w>.</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w></l>
						<l n="3" num="1.3" lm="4" met="4"><space unit="char" quantity="16"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w></l>
						<l n="4" num="1.4" lm="11" met="6+5" mp5="Mem"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="4.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.7">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></w></l>
						<l n="5" num="1.5" lm="11" met="5+6"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="5.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5" caesura="1">œu</seg>r</w><caesura></caesura> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>pt</w> <w n="5.8" punct="pt:11">d<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" punct="pt">eu</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="11" met="5+6"><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="6.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>x</w>, <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5" caesura="1">ô</seg></w><caesura></caesura> <w n="6.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>lh<seg phoneme="u" type="vs" value="1" rule="d-2" place="10" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></w></l>
						<l n="7" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">M<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>ls</w> <w n="7.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ds</w> <w n="7.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
						<l n="8" num="2.3" lm="4" met="4"><space unit="char" quantity="16"></space><w n="8.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w></l>
						<l n="9" num="2.4" lm="11" met="6+5" mp5="M"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">Ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rts</w> <w n="9.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="9.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="9.7">fl<seg phoneme="y" type="vs" value="1" rule="454" place="10" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></w></l>
						<l n="10" num="2.5" lm="11" met="5+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="10.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="10.7" punct="pt:11">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" mp="M">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="11" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="11" met="5−6" mp5="C" mp6="M"><w n="11.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w> ,<w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">om</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="11.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9">en</seg></w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="11.7" punct="vg:11">ch<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="12" num="3.2" lm="11" met="6+5" mp5="M"><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.7">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="12.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.9" punct="vg:11">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="11" punct="vg">eu</seg></w>,</l>
						<l n="13" num="3.3" lm="4" met="4"><space unit="char" quantity="16"></space><w n="13.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.3" punct="vg:4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>,</l>
						<l n="14" num="3.4" lm="11" met="6+5" mp5="M"><w n="14.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="14.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.8" punct="vg:11">chl<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="15" num="3.5" lm="11" met="5+6"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="5" caesura="1">à</seg></w><caesura></caesura> <w n="15.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="15.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="15.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="15.8" punct="vg:11">f<seg phoneme="ø" type="vs" value="1" rule="398" place="11" punct="vg">eu</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1" lm="11" met="5+6"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">Ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="16.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="16.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>ds</w><caesura></caesura> <w n="16.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="16.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg>s</w></l>
						<l n="17" num="4.2" lm="11" met="5+6"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="17.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="17.5" punct="vg:6">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="17.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="17.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="17.8">d</w>’<w n="17.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.10"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="17.11">d</w>’<w n="17.12"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>r</w></l>
						<l n="18" num="4.3" lm="5"><space unit="char" quantity="14"></space><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="18.3">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="18.4" punct="pe:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pe">o</seg>r</w> !</l>
						<l n="19" num="4.4" lm="11" met="5+6"><w n="19.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="19.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="19.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="19.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">oû</seg>t</w><caesura></caesura> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="19.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="19.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg>s</w></l>
						<l n="20" num="4.5" lm="11" met="6+5" mp5="M"><w n="20.1" punct="vg:3">G<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="20.3" punct="vg:6">n<seg phoneme="a" type="vs" value="1" rule="343" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="6" punct="vg" caesura="1">ï</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>,<caesura></caesura> <w n="20.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="7">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="20.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="20.6" punct="pt:11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11" punct="pt">o</seg>r</w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1" lm="11" met="5−6" mp5="C" mp6="M"><w n="21.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="21.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="21.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="21.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="21.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="21.8">g<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></w></l>
						<l n="22" num="5.2" lm="11" met="5+6"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="22.2" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5" caesura="1">ô</seg></w><caesura></caesura> <w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="22.5" punct="pe:7">p<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pe">eu</seg>rs</w> ! <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="22.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="22.8" punct="vg:11">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="11" punct="vg">eu</seg>r</w>,</l>
						<l n="23" num="5.3" lm="4" met="4"><space unit="char" quantity="16"></space><w n="23.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>,</l>
						<l n="24" num="5.4" lm="11" met="6+5" mp5="F"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="24.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="24.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="24.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="24.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-27" place="10" mp="F">e</seg></w> <w n="24.8" punct="vg:11">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="11">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></w>,</l>
						<l n="25" num="5.5" lm="11" mp6="Fc" met="5−6"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="25.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="25.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w><caesura></caesura> <w n="25.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="25.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="25.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="25.7" punct="ps:11">c<seg phoneme="œ" type="vs" value="1" rule="249" place="11" punct="ps">œu</seg>r</w>…</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1" lm="11" met="6+5" mp5="P"><w n="26.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>qu<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="26.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="26.3" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="26.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="26.5">m<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></w></l>
						<l n="27" num="6.2" lm="11" met="6+5" mp5="Lc"><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">j</w>’<w n="27.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>rd</w>’<w n="27.5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="27.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="27.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="27.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="27.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w></l>
						<l n="28" num="6.3" lm="3"><space unit="char" quantity="20"></space><w n="28.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg></w> <w n="28.2" punct="pe:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="e" type="vs" value="1" rule="DLR66_1" place="3" punct="pe">e</seg></w> !</l>
						<l n="29" num="6.4" lm="11" met="5−6" mp5="C" mp6="M"><w n="29.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M/mp">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w>-<w n="29.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3" mp="Fm">e</seg></w> <w n="29.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="29.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="29.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="29.6" punct="vg:11">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></w>,</l>
						<l n="30" num="6.5" lm="12" met="12" mp6="C"><w n="30.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="30.2" punct="vg:2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="30.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="30.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="30.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="30.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="30.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="30.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>