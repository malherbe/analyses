<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR109" modus="cp" lm_max="12" metProfile="8, 3÷6, 6+6, 5=6, 5÷5">
					<head type="main">DES YEUX</head>
					<opener>
						<salute>A Ma Sœur Marguerite.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="9" mp3="M" mp6="C" met="9"><space unit="char" quantity="6"></space><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="1.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="1.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="1.6">b<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg>s</w></l>
						<l rhyme="none" n="2" num="1.2" lm="9" mp3="M" met="6+3"><space unit="char" quantity="6"></space><w n="2.1">S</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w><caesura></caesura> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.7" punct="pt:9">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9" punct="pt">i</seg>t</w>.</l>
						<l rhyme="none" n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:3">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="3.2">c</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="4" num="1.4" lm="9" mp3="M" mp6="C" met="9"><space unit="char" quantity="6"></space><w n="4.1">L</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="4.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="4.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="4.6" punct="pt:9">b<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="9" mp3="M" mp6="C" met="9"><space unit="char" quantity="6"></space><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rts</w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="5.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="5.8" punct="vg:9"><seg phoneme="u" type="vs" value="1" rule="426" place="9" punct="vg">où</seg></w>,</l>
						<l n="6" num="2.2" lm="11" met="6+5" mp5="M"><space unit="char" quantity="2"></space><w n="6.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>ds</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="6.3" punct="vg:4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="6.4">cl<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6" punct="vg:11">f<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="11">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></w>,</l>
						<l n="7" num="2.3" lm="9" met="3+6"><space unit="char" quantity="6"></space><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">Ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="2">u</seg>s</w> <w n="7.2">d</w>’<w n="7.3" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="7.6" punct="pv:9">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7" mp="M">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="8" num="2.4" lm="10" mp5="Mem" met="10"><space unit="char" quantity="4"></space><w n="8.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ds</w> <w n="8.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="8.6">h<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>b<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w></l>
						<l n="9" num="2.5" lm="9" met="3+6"><space unit="char" quantity="6"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="9.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" caesura="1">a</seg>t</w><caesura></caesura> <w n="9.4">m<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="9.5">d</w>’<w n="9.6">h<seg phoneme="i" type="vs" value="1" rule="493" place="7" mp="M">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg></w></l>
						<l n="10" num="2.6" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="10.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3" mp="M">o</seg>mn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">am</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="487" place="8" mp="M">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="10.8">f<seg phoneme="u" type="vs" value="1" rule="426" place="12">ou</seg></w></l>
						<l n="11" num="2.7" lm="9" met="3−6" mp3="C" mp6="F"><space unit="char" quantity="6"></space><w n="11.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="11.3">fr<seg phoneme="o" type="vs" value="1" rule="415" place="4" mp="M">ô</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg></w></l>
						<l n="12" num="2.8" lm="11" met="5+6"><space unit="char" quantity="2"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="12.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="Lc">i</seg></w>-<w n="12.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.10" punct="vg:11">nu<seg phoneme="i" type="vs" value="1" rule="491" place="11" punct="vg">i</seg>t</w>,</l>
						<l n="13" num="2.9" lm="10" met="5+5"><space unit="char" quantity="4"></space>‒ <w n="13.1">G<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="13.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="13.5">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="13.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="13.8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> ‒</l>
						<l n="14" num="2.10" lm="12" met="6+6"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="14.2">n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="14.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="15" num="2.11" lm="12" met="6+6"><w n="15.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="15.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="15.6">squ<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>mm<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="15.8" punct="vg:12">cr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="vg">au</seg>ds</w>,</l>
						<l n="16" num="2.12" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ds</w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="16.6" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="17" num="2.13" lm="12" met="6+6"><w n="17.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="17.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="17.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="17.5">d</w>’<w n="17.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="17.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="17.8">l</w>’<w n="17.9">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="17.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="17.11">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>rs</w> <w n="17.12" punct="pt:12">p<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="18" num="3.1" lm="12" met="6+6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>h</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="18.3" punct="pe:3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pe">eu</seg>x</w> ! <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="18.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="18.6">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="18.8">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="18.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.10"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="18.11"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="19" num="3.2" lm="12" met="6+6"><w n="19.1">D<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" mp="P">e</seg>rs</w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="19.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="19.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="19.8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>t</w></l>
						<l n="20" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="20.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="20.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w></l>
						<l n="21" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="21.1">L</w>’<w n="21.2"><seg phoneme="œ" type="vs" value="1" rule="286" place="1">œ</seg>il</w> <w n="21.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="21.7" punct="pe:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>