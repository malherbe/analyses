<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR984" modus="sm" lm_max="8" metProfile="8">
				<head type="main">BALLADE DE MES POUPÉES</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.6">j<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.8" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="2.2" punct="vg:3">j<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="2.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rts</w>, <w n="2.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6" punct="vg:8">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="8" punct="vg">eu</seg>s</w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="3.2" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg>s</w>, <w n="3.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="3.4" punct="vg:8">s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ts</w> <w n="4.5" punct="pt:8">di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="5.4" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.8">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="6.4" punct="pt:8">h<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="5">en</seg>s</w> <w n="7.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="6">en</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462" place="7">u</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.5" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5">f<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="10.4" punct="vg:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.6">n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ts</w> <w n="12.4" punct="pt:8">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="13.3" punct="vg:5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="13.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="13.6" punct="vg:8">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w>-<w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.5" punct="pt:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.5">cr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="15.6" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="16.5" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="17.2" punct="vg:2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg></w>, <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="17.4" punct="vg:5">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>, <w n="17.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="vg:8">fi<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.4">d</w>’<w n="18.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="19.3" punct="vg:4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="4" punct="vg">a</seg>il</w>, <w n="19.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.4" punct="pt:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="21.3" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>, <w n="21.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="21.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.6" punct="pt:8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="22.4">d</w>’<w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="22.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="23.2">qu</w>’<w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="23.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="23.5">l</w>’<w n="23.6" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</w>, <w n="23.7" punct="vg:8"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="24.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.5" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">n</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="25.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.5" punct="vg:4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4" punct="vg">o</seg>p</w>, <w n="25.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1">C<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t</w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="26.6" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="27.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="27.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="27.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="27.7" punct="vg:8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="28.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="28.5" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>