<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR982" modus="sm" lm_max="8" metProfile="8">
				<head type="main">BALLADE DU CŒUR</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>t</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="1.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>sc<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">d</w>’<w n="2.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="3.5" punct="vg:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="4.6">l</w>’<w n="4.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.4" punct="vg:8">ph<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>si<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pt:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rv<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="7.4">l</w>’<w n="7.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="8.6">n</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="8.10" punct="pt:8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8" punct="pt">e</seg>d</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="9.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>r</w>, <w n="9.3">c</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="9.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</w>,</l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="10.7" punct="vg:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.8" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</w>.</l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="12.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="12.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5" punct="vg:8">J<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="14.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="14.8" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5" punct="vg:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nh<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="16.6">n</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.8">qu</w>’<w n="16.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="16.10" punct="pt:8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8" punct="pt">e</seg>d</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1">J<seg phoneme="u" type="vs" value="1" rule="d-2" place="1">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">r<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w></l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="18.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1">S</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="19.3" punct="vg:3">st<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="19.4">c</w>’<w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="19.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="19.8" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</w>.</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="20.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>str<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="21.5">l</w>’<w n="21.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lm<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3" punct="vg:5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5" punct="vg">en</seg>t</w>, <w n="22.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="22.6" punct="vg:8">si<seg phoneme="e" type="vs" value="1" rule="241" place="8" punct="vg">e</seg>d</w>,</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="23.2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="23.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="23.6" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="24.6">n</w>’<w n="24.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="24.8">qu</w>’<w n="24.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="24.10" punct="pt:8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8" punct="pt">e</seg>d</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="25.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="25.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="25.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="25.7">m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="26.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="26.5" punct="vg:8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1" punct="pe:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg></w> ! <w n="27.2">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="27.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.5" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="28.6">n</w> <w n="28.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="28.8">qu</w>’<w n="28.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="28.10" punct="pt:8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8" punct="pt">e</seg>d</w>.</l>
				</lg>
			</div></body></text></TEI>