<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV2" modus="cm" lm_max="10" metProfile="5−5">
				<head type="main">Platonisme</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="1.5" punct="vg:5">F<seg phoneme="a" type="vs" value="1" rule="193" place="5" punct="vg" caesura="1">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="2.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>gts</w> <w n="2.3">p<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>ll<seg phoneme="y" type="vs" value="1" rule="dc-3" place="4" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>ts</w><caesura></caesura> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="2.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="Lp">on</seg>t</w>-<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="2.7" punct="pi:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pi">er</seg></w> ?</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="3.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4">D<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.5">n</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w></l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="4.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="4.7" punct="pt:10">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>pt<seg phoneme="i" type="vs" value="1" rule="493" place="10">y</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="5.5">F<seg phoneme="a" type="vs" value="1" rule="193" place="5" caesura="1">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="5.9">C<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">s</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="6.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="6.8" punct="vg:10">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="7.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg></w><caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="7.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.9">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w></l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>cl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="8.3">J<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" caesura="1">in</seg></w><caesura></caesura> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="8.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="8.7" punct="pt:10">M<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="9.4" punct="vg:5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" punct="vg" caesura="1">ain</seg>s</w>,<caesura></caesura> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="9.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>s</w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="10.2">d</w>’<w n="10.3">H<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="10.7" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>s</w>,</l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.5">v<seg phoneme="o" type="vs" value="1" rule="318" place="5" caesura="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.9" punct="pe:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="10" met="5−5" mp5="C"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="12.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="12.5">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="12.7">B<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="12.8" punct="vg:10">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</w>,</l>
					<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="13.3">t</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="13.7">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>x</w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.9" punct="vg:10">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="14.3">d</w>’<w n="14.4" punct="vg:5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="14.5" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="pt">en</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>