<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SECOND</head><div type="poem" key="FLO41" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="number">FABLE XIX</head>
					<head type="main">Myson</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">M<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="1.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="pv:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:2">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="3.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.3" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="?" type="va" value="1" rule="162" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.5" punct="vg:8">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg>s</w>, <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="3.7" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="4.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.5" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="4.6" punct="vg:7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>l</w>, <w n="4.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="4.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="4.9" punct="vg:12">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="5.3">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="5.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>ts</w>.</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="6.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.4">Gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>cs</w> <w n="6.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="6.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.7" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="7.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="7.3" punct="vg:4">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="7.4" punct="vg:6">M<seg phoneme="i" type="vs" value="1" rule="493" place="5" mp="M">y</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="7.6">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="7.8" punct="dp:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="dp">i</seg>s</w> :</l>
						<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="8.3" punct="pv:3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pv">eu</seg>l</w> ; <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="8.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w>-<w n="8.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="pi:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1" punct="vg:2">Vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2" punct="vg">en</seg>t</w>, <w n="9.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M/mp">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M/mp">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="Lp">i</seg>t</w>-<w n="9.3" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>l</w>,<caesura></caesura> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="9.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="10">oi</seg></w> <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.7" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>