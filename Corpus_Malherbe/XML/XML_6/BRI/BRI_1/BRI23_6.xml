<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI23" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">À LA MÉMOIRE DE GEORGES FARCY</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Il adorait la France, la Poésie <lb></lb>
								et la Philosophie. Que la Patrie <lb></lb>
								conserve son nom !
							</quote>
							<bibl>
								<name>VICTOR COUSIN</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="pe">i</seg></w> ! <w n="1.2">T<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="1.3">j</w>’<w n="1.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="1.5" punct="vg:8">F<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rc<seg phoneme="i" type="vs" value="1" rule="493" place="8" punct="vg">y</seg></w>, <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.8" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="2.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="2.7">c<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="2.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="2.10" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="œ" type="vs" value="1" rule="249" place="1">œu</seg>r</w> <w n="3.2" punct="vg:3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.6" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>,<caesura></caesura> <w n="3.7" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</w> ! <w n="3.8">T</w>’<w n="3.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>s</w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>f</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="4.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="4.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="4.9" punct="pe:12">g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>s</w> !</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="5.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>r</w>, <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.7">N<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.11" punct="vg:12">gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="6.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.8" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="y" type="vs" value="1" rule="391" place="3">eu</seg>t</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.9" punct="pv:12">Pl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg></w> ;</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="8.3" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>,<caesura></caesura> <w n="8.4" punct="vg:8">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="8.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10">an</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="8.7" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="9.3">br<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5" punct="vg:6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.10" punct="vg:12">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1" punct="vg:2">F<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.2" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3" mp="M">im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>f</w> <w n="10.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.6" punct="pe:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="11.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="11.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="12.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462" place="8" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="12.8" punct="pt:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="pi:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pi" mp="F">e</seg></w> ? <w n="13.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>l</w><caesura></caesura> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="Lc">ou</seg>rd</w>’<w n="13.6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="13.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="13.8" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="14.5" punct="vg:4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="14.6">c</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="14.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.10">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="14.11">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="14.12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="14.13" punct="pt:12">L<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>x</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="15.3" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="15.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="15.5" punct="pe:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="pe" caesura="1">œu</seg>r</w> !<caesura></caesura> <w n="15.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="15.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="15.8">f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="15.9">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="11" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="16.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="16.5" punct="vg:6">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="16.7">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" mp="Lc">oi</seg></w>-<w n="16.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.9" punct="pv:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="Lc">à</seg></w>-<w n="17.2" punct="vg:2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>t</w>, <w n="17.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M/mp">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="4" mp="M/mp">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="Lp">i</seg>s</w>-<w n="17.4" punct="pe:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe" caesura="1">oi</seg></w> !<caesura></caesura> <w n="17.5">Pl<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="17.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="18.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="18.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="18.5" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>r</w>,<caesura></caesura> <w n="18.6" punct="vg:10">P<seg phoneme="i" type="vs" value="1" rule="493" place="7" mp="M">y</seg>th<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="18.8" punct="pt:12">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1830">Août 1830</date>.
						</dateline>
				</closer>
			</div></body></text></TEI>