<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI110" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">Camée</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">AI</seg>SS<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">AN</seg>T</w> <w n="1.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.4" punct="vg:6">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">f<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="1.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.10" punct="vg:12">gr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="2.3" punct="vg:6">n<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="2.7">d</w>’<w n="2.8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">O</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="3.5" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>ts</w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8" punct="vg:9">s<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>r</w>, <w n="3.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="3.10" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1" mp="M">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.4" punct="vg:6">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="u" type="vs" value="1" rule="426" place="6" punct="vg" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.7">f<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>ts</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="4.9" punct="pt:12">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="5.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="y" type="vs" value="1" rule="251" place="3">eû</seg>t</w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.6" punct="pt:6">r<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt" caesura="1">i</seg>r</w>.<caesura></caesura> <w n="5.7">S<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.9">br<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="5.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.11">M<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5" mp="M">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="6.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.8" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2" punct="vg:3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>t</w>, <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>ds</w><caesura></caesura> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="8" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="7.9" punct="vg:12">p<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="8.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rps</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="8.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="8.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="8.10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="8.11" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</w>.</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="9.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="9.9" punct="pt:12">V<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="10.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.7">f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>t</w> <w n="10.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="11.6" punct="dp:6">N<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp" caesura="1">a</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> :<caesura></caesura> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg></w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.10">gu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg></w></l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="12.7">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rd<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></w>.</l>
					</lg>
				</div></body></text></TEI>