<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ERATO</head><div type="poem" key="FRT8" modus="cm" lm_max="12" metProfile="6−6">
					<head type="main">A la Fleur de Lis</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">P<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="1.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5" punct="vg:6">c<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="1.7">N<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="1.9" punct="vg:12">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S</w>’<w n="2.2" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="2" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.8">R<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.9" punct="pt:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="3.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="3.5">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="3.7">t<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="3.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="3.10" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="4.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>rs</w><caesura></caesura> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="4.9" punct="pt:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="5.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>fr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" mp="M">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7">b<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2" punct="vg:3">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">d<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.7">r<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="6.10">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>fr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="7.5" punct="vg:9">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="8.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="8.9" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" mp6="C" met="6−6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="9.3">st<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gm<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C" caesura="1">ou</seg>s</w><caesura></caesura> <w n="9.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">A</seg>phr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="10.4">l<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="10.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">t</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="10.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="11.4">l<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="Lp">ai</seg>s</w>-<w n="11.9" punct="pi:12">t<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pi">u</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="12.4" punct="vg:6">bl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="12.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="12.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="12.10" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">S<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="13.2">d</w>’<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6" caesura="1">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="13.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="14.6">r<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="14.8">d</w>’<w n="14.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>