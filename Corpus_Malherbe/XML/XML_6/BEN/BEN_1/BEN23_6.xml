<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN23" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">À Monsieur Esprit, premier Médecin de Monsieur.</head>
					<head type="form">SONNET.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>SPR<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">I</seg>T</w>, <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="1.6">r<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.8" punct="pt:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">gu<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="2.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8" punct="vg:12"><seg phoneme="œ" type="vs" value="1" rule="286" place="11" mp="M">œ</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="445" place="10" mp="M">û</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1" mp="C">e</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt</w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="4.4">sç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="4.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="4.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="4.10">d</w>’<w n="4.11"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="4.12" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.3">gu<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.4" punct="vg:6">L<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rs<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="6.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="6.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.7" punct="pv:12">fi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="7.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t</w> <w n="7.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="7.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oî</seg>t</w> <w n="7.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="8.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="8.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.7" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="11" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="9.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.7" punct="pe:12">c<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.10">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="10.11">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="Lc">Au</seg></w>-<w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mc">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="11.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="11.8" punct="pt:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">ez</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.3">gu<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="12.4">n</w>’<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="12.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>s</w> <w n="12.8" punct="vg:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="13.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg></w> <w n="13.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="13.8" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1" mp="Lc">eu</seg>t</w>-<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M/mp">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="14.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.6">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="14.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="14.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="14.10" punct="pt:12">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>