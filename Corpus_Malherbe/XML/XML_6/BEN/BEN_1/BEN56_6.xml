<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN56" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="main">À Mademoiselle de Guerchy, <lb></lb>luy envoyant la copie d’une Joüissance.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>LL<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">E</seg></w> <w n="1.2" punct="vg:4">Gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="i" type="vs" value="1" rule="493" place="4" punct="vg">y</seg></w>, <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6" punct="vg:8">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="2.6" punct="pv:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>t</w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="3.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="3.6" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="3.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="3.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="3.9">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="3.10" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8" punct="pi:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pi">an</seg>t</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="5.2" punct="vg:3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2" punct="vg:3">scr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w>-<w n="6.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="6.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="7.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="7.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="7.8">v<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.9" punct="vg:12">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="8.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5" mp="M">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="8.6">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c</w> <w n="8.7">qu</w>’<w n="8.8"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="8.9">n</w>’<w n="8.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="8.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="8.12" punct="pt:12">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pt">en</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.4" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="11.8" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>lh<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.7" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="13.4" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="13.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="13.7" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">S</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.5" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cqu<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="dp">ê</seg>ts</w> :</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="15.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem/mp">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="9" mp="Lp">ez</seg></w>-<w n="15.9">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="15.10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11">en</seg></w> <w n="15.11"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="Lc">e</seg>lqu</w>’<w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="16.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="16.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>t</w> <w n="16.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="16.8" punct="pi:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>cqu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pi">e</seg>ts</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg>s</w> <w n="17.3">d</w>’<w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tru<seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="17.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="17.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="17.8" punct="vg:8">v<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="18.6" punct="vg:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</w>,</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">m</w>’<w n="19.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="19.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="19.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="19.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="19.11">d</w>’<w n="19.12"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="20.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="20.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="20.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="20.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="20.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="20.9" punct="pt:12">v<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="21.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="21.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="21.6">v<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.7">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="22" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="22.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5" punct="pi:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pi">on</seg></w> ?</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="23.2">n</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="23.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="M">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="23.5">qu</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="23.7">f<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="23.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="23.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="24" num="6.4" lm="12" met="6+6"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="24.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">oû</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="24.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt</w> <w n="24.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="24.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="24.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="24.9" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					</lg>
				</div></body></text></TEI>