<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN24" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">Sur la ville de Paris.</head>
					<head type="form">SONNET.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">RI<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">EN</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4" punct="pv:6">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv" caesura="1">i</seg>s</w> ;<caesura></caesura> <w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7" punct="vg:9">bl<seg phoneme="a" type="vs" value="1" rule="341" place="9" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.10" punct="pv:12">l<seg phoneme="u" type="vs" value="1" rule="BEN__2" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">ë</seg></w> ;</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="2.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="2.6" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="2.10" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pv">e</seg>st</w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l</w> <w n="3.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="3.3" punct="vg:3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3" punct="vg">en</seg></w>, <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="3.5">s</w>’<w n="3.6"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="3.7" punct="vg:6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="3.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.10">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="3.11">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="C">i</seg>l</w> <w n="3.13" punct="dp:12"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="12" punct="dp">e</seg>st</w> :</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="4.3" punct="vg:3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="4.6" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="463" place="6" punct="vg" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">ë</seg></w>,<caesura></caesura> <w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="4.8"><seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="C">y</seg></w> <w n="4.9" punct="vg:9">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="vg">en</seg>d</w>, <w n="4.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="4.11"><seg phoneme="i" type="vs" value="1" rule="497" place="11" mp="C">y</seg></w> <w n="4.12" punct="pt:12">r<seg phoneme="u" type="vs" value="1" rule="BEN__2" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">ë</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="5.4" punct="vg:3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="5.6">s</w>’<w n="5.7"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="5.8" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="5.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.10"><seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="C">y</seg></w> <w n="5.11" punct="vg:9">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="5.13"><seg phoneme="i" type="vs" value="1" rule="497" place="11" mp="C">y</seg></w> <w n="5.14" punct="pv:12">j<seg phoneme="u" type="vs" value="1" rule="BEN__2" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">ë</seg></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="6.3" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>, <w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="6.6" punct="vg:6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.8"><seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="C">y</seg></w> <w n="6.9" punct="vg:9">m<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="vg">eu</seg>rt</w>, <w n="6.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="6.11"><seg phoneme="i" type="vs" value="1" rule="497" place="11" mp="C">y</seg></w> <w n="6.12" punct="dp:12">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="dp">ai</seg>st</w> :</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="7.5" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t</w> <w n="7.7" punct="vg:12">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>st</w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="8.4">t<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="8.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.9" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="BEN__2" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">ë</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.5" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>ts</w>,<caesura></caesura> <w n="9.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="C">i</seg>l</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="10.3" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg></w>,<caesura></caesura> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.5">r<seg phoneme="wa" type="vs" value="1" rule="424" place="8">oy</seg></w> <w n="10.6">n</w>’<w n="10.7"><seg phoneme="i" type="vs" value="1" rule="497" place="9">y</seg></w> <w n="10.8">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="10.9" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>s</w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="445" place="4" mp="M">û</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l</w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.5">s</w>’<w n="11.6"><seg phoneme="i" type="vs" value="1" rule="497" place="9">y</seg></w> <w n="11.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>t</w> <w n="11.8" punct="dp:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="12.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="12.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>t</w><caesura></caesura> <w n="12.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="12.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>g</w> <w n="12.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.11" punct="vg:12">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12" punct="vg">ain</seg>ts</w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.5">p<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="13.8" punct="pt:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="14.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="Lc">on</seg>g</w>-<w n="14.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="14.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="14.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="14.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.9" punct="pt:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>