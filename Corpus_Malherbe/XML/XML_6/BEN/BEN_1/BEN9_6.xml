<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN9" modus="sm" lm_max="8" metProfile="8">
					<head type="main">Sur la Laideur.</head>
					<head type="form">SONNET VIII.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">EIN</seg>T</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="1.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="1.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.6">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="2.7" punct="vg:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g</w> <w n="3.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="4.4">s</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.8" punct="pv:8">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="5.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.9">p<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="pv:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="7.2" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="7.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="7.4" punct="dp:8">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="dp">u</seg></w> :</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>p</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">gr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="8.6" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="9.2">gr<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5" punct="pt:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</w>.</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">p<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s</w> <w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.6" punct="pv:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>rs</w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.4">j<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="12.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="12.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="12.7" punct="pv:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>r</w> ;</l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="13.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="14.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="14.4" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>