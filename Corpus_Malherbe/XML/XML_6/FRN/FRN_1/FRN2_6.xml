<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN2" modus="cp" lm_max="18" metProfile="12, 16, 15, 17, (13)">
		<head type="main">LA MAÎTRESSE QUE JE PRENDRAI</head>
			<lg n="1">
				<l n="1" num="1.1" lm="12" mp6="C"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="1.8" punct="dp:12">b<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
				<l n="2" num="1.2" lm="13"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="2.3">d</w>’<w n="2.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t</w>, <w n="2.5">ç<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="2.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="2.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="2.8">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9">o</seg>p</w> <w n="2.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l</w> <w n="2.10"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="2.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="12" mp="C">a</seg></w> <w n="2.12" punct="pt:13">t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="13">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="pt" mp="F">e</seg></w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1" lm="18"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.8">S<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="12" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="13" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="14">é</seg></w> <w n="3.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="15" mp="C">e</seg>s</w> <w n="3.10">G<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="16">en</seg>s</w> <w n="3.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="17" mp="Pem">e</seg></w> <w n="3.12" punct="vg:18">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="18">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="19" punct="vg" mp="F">e</seg>s</w>,</l>
				<l n="4" num="2.2" lm="16"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.7" punct="vg:14"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12" mp="M">u</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="13">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14" punct="vg" mp="F">e</seg></w>, <w n="4.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="15" mp="Lc">eu</seg>t</w>-<w n="4.9" punct="pt:16"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="16">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt" mp="F">e</seg></w>.</l>
			</lg>
			<lg n="3">
				<l n="5" num="3.1" lm="15"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="5.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.5">m</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s</w> <w n="5.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" mp="C">on</seg></w> <w n="5.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="13">e</seg>r</w> <w n="5.10" punct="vg:15">p<seg phoneme="o" type="vs" value="1" rule="444" place="14" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="15">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="16" punct="vg" mp="F">e</seg></w>,</l>
				<l n="6" num="3.2" lm="17"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>p<seg phoneme="i" type="vs" value="1" rule="482" place="7" mp="M">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="11">e</seg>rs</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="342" place="12" mp="P">à</seg></w> <w n="6.9">l</w>’<w n="6.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="13">en</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14" mp="F">e</seg></w> <w n="6.11" punct="pt:17">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="15" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="16" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="17">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="18" punct="pt" mp="F">e</seg></w>.</l>
			</lg>
			<lg n="4">
				<l n="7" num="4.1" lm="17"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg">ai</seg></w>, <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.8" punct="vg:13">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="13" punct="vg" mp="F">e</seg>s</w>, <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="342" place="14" mp="P">à</seg></w> <w n="7.10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="15" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="16" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="17">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="18" mp="F">e</seg></w></l>
				<l n="8" num="4.2" lm="16"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4" punct="vg:8">t<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>, <w n="8.5" punct="pe:9"><seg phoneme="o" type="vs" value="1" rule="444" place="9" punct="pe">o</seg>h</w> ! <w n="8.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="8.7" punct="vg:14">t<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="13" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="14" punct="vg">en</seg>t</w>, <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="15" mp="Pem">e</seg></w> <w n="8.9" punct="pt:16">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="16">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt" mp="F">e</seg>s</w>.</l>
			</lg>
			<lg n="5">
				<l n="9" num="5.1" lm="16"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="9.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="9.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="9.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.8">d<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="12" mp="C">a</seg></w> <w n="9.10">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="13" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="14">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="15" mp="F">e</seg></w> <w n="9.11" punct="vg:16">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="16">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="vg" mp="F">e</seg></w>,</l>
				<l n="10" num="5.2" lm="15"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3">d</w>’<w n="10.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>rs</w>, <w n="10.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="10.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="vg">u</seg></w>, <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.8">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="13" mp="F">e</seg>s</w> <w n="10.9" punct="pt:15">h<seg phoneme="o" type="vs" value="1" rule="435" place="14" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411" place="15">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="16" punct="pt" mp="F">e</seg>s</w>.</l>
			</lg>
			<lg n="6">
				<l n="11" num="6.1" lm="16"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="11.7" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10" mp="M">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</w>, <w n="11.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="13">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14" mp="F">e</seg></w> <w n="11.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="15" mp="C">e</seg>s</w> <w n="11.10" punct="dp:16">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="16">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="dp" mp="F">e</seg>s</w> :</l>
				<l n="12" num="6.2" lm="12" mp6="C"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2" punct="vg:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>s</w>, <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="12.4" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>t</w>, <w n="12.5">ç<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="12.6">n</w>’<w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="12.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="12.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="12.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="12.11" punct="pt:12">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
			</lg>
	</div></body></text></TEI>