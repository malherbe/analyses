<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA BOHÊME ET MON CŒUR</head><div type="poem" key="CRC51" rhyme="none" modus="sm" lm_max="8" metProfile="8">
			<head type="main"><hi rend="ital">Montmartre</hi></head>
			<lg n="1">
				<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tm<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6" punct="vg:8">j<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
				<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="2.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>x</w>, <w n="2.4">d</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="2.7" punct="pt:8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">n</w>’<w n="3.4" punct="dp:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp">e</seg></w> : <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">j<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
				<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="4.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.5">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7" punct="vg:8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				<l n="6" num="2.2" lm="8" met="8"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
				<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="7.3">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg><seg phoneme="i" type="vs" value="1" rule="492" place="5">i</seg>t</w> <w n="7.4" punct="vg:8">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>,</l>
				<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L</w>’<w n="8.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="8.6">l</w>’<w n="8.7" punct="pt:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="9.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="9.7" punct="dp:8">l<seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="dp">à</seg></w> :</l>
				<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="10.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="10.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="10.10" punct="pt:8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
				<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="11.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">r<seg phoneme="i" type="vs" value="1" rule="480" place="6">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
				<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.6">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>t</w>.</l>
			</lg>
			<lg n="4">
				<l n="13" num="4.1" lm="8" met="8"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2" punct="vg:4">M<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="13.3">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.5" punct="pv:8">G<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w>;</l>
				<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="14.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="14.7" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>ts</w>.</l>
				<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w></l>
				<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.5" punct="pt:8">bl<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
			</lg>
			<lg n="5">
				<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="17.3">b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="17.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="17.6" punct="vg:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</w>,</l>
				<l n="18" num="5.2" lm="8" met="8"><w n="18.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				<l n="19" num="5.3" lm="8" met="8"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
				<l n="20" num="5.4" lm="8" met="8"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="20.3">cr<seg phoneme="y" type="vs" value="1" rule="454" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="20.6" punct="pi:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pi">eu</seg>r</w> ?</l>
			</lg>
			<lg n="6">
				<l n="21" num="6.1" lm="8" met="8"><w n="21.1">T<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="21.2" punct="dp:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="dp">oi</seg></w> : <w n="21.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</w> <w n="21.5">bl<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
				<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="22.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="22.4" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				<l n="23" num="6.3" lm="8" met="8"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">t</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="23.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="23.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.7" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				<l n="24" num="6.4" lm="8" met="8"><w n="24.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="24.2">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="24.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="24.4">j</w>’<w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="24.6"><seg phoneme="y" type="vs" value="1" rule="391" place="6">eu</seg>s</w> <w n="24.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">ez</seg></w>.</l>
			</lg>
			<lg n="7">
				<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="25.2" punct="vg:3">M<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="25.3" punct="pe:5">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" punct="pe">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="25.4" punct="pe:8">H<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				<l n="26" num="7.2" lm="8" met="8"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2" punct="vg:3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="26.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="26.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="26.5" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
				<l n="27" num="7.3" lm="8" met="8"><w n="27.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="27.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>t</w>,</l>
				<l n="28" num="7.4" lm="8" met="8">(<w n="28.1" punct="vg:2">J<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="28.2" punct="vg:3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="28.3" punct="vg:5">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="28.4" punct="pf:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w>)</l>
				<l n="29" num="7.5" lm="8" met="8"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="29.4">f<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="29.6" punct="ps:8">p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
			</lg>
		</div></body></text></TEI>