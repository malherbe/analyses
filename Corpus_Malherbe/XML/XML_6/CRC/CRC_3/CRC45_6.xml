<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA BOHÊME ET MON CŒUR</head><div type="poem" key="CRC45" modus="cp" lm_max="10" metProfile="8, 4, 5+5, (5)">
			<head type="main"><hi rend="ital">Compagnons</hi></head>
			<div n="1" type="section">
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3" punct="vg:4">plu<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w>, <w n="1.4" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</w> ! <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">br<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="3">î</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.4" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w></l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="5.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="5.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="5.5">d</w>’<w n="5.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="6.4">l</w>’<w n="6.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6" punct="pv:8">m<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="7.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="8.2">cr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.4" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="9.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="4"></space><w n="10.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>t</w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w></l>
					<l n="11" num="3.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="11.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="11.2" punct="vg:4">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w></l>
					<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="12.2" punct="vg:3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6" punct="pt:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
			</div>
			<div n="2" type="section">
				<head type="number">II</head>
				<lg n="1">
					<l n="13" num="1.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="pi:3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="pi ps ti">i</seg>s</w> ? … — <w n="13.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="13.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="7">e</seg>s</w>-<w n="13.8" punct="pi:8">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pi ps">u</seg></w> ? …</l>
					<l n="14" num="1.2" lm="4" met="4"><space unit="char" quantity="12"></space><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3" punct="pt:4">f<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					<l n="15" num="1.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="15.6">s</w>’<w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="15.8" punct="pt:8">t<seg phoneme="y" type="vs" value="1" rule="445" place="8" punct="pt">û</seg></w>.</l>
					<l n="16" num="1.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="16.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2" punct="pe:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe">eu</seg>r</w> ! <w n="16.3">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="17" num="2.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="17.1">D</w>’<w n="17.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.4" punct="pi:3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="pi ti">en</seg>s</w>? — <w n="17.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="17.6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="17.7">d</w>’<w n="17.8"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="17.9">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>s</w>-<w n="17.10" punct="pi:8">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pi">u</seg></w>?</l>
					<l n="18" num="2.2" lm="4" met="4"><space unit="char" quantity="12"></space><w n="18.1">L</w>’<w n="18.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3" punct="pt:4">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					<l n="19" num="2.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="19.5" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>,</l>
					<l n="20" num="2.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="20.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="20.5" punct="ps:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1" lm="8" met="8"><space unit="char" quantity="4"></space><w n="21.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3" punct="pi:3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pi ti">ai</seg>s</w> ? — <w n="21.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="21.6">t</w>’<w n="21.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="21.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w>-<w n="21.9" punct="pi:8">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pi">u</seg></w> ?</l>
					<l n="22" num="3.2" lm="4" met="4"><space unit="char" quantity="12"></space><w n="22.1">B<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="22.3" punct="pt:4">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</w>.</l>
					<l n="23" num="3.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="23.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="23.5" punct="vg:5">l<seg phoneme="y" type="vs" value="1" rule="453" place="5" punct="vg">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="23.7">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w></l>
					<l n="24" num="3.4" lm="8" met="8"><space unit="char" quantity="4"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>fr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="24.4" punct="pi:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</w>?</l>
				</lg>
			</div>
			<div n="3" type="section">
				<head type="number">III</head>
				<lg n="1">
					<l n="25" num="1.1" lm="10" met="5+5"><w n="25.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="25.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="25.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="25.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="25.6">d<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="25.8" punct="ps:10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="ps">ain</seg></w>…</l>
					<l n="26" num="1.2" lm="5"><space unit="char" quantity="10"></space><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.4" punct="ps:5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="ps">e</seg></w>…</l>
					<l n="27" num="1.3" lm="10" met="5+5"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="27.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="27.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="27.6">m<seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="27.7" punct="ps:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="ps">in</seg></w>…</l>
				</lg>
				<lg n="2">
					<l n="28" num="2.1" lm="10" met="5+5"><w n="28.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="28.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="28.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="28.4" punct="ps:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="ps" caesura="1">a</seg>s</w>…<caesura></caesura> <w n="28.5">T<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C">u</seg></w> <w n="28.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="28.7">s<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="28.8" punct="ps:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="ps">a</seg>s</w>…</l>
					<l n="29" num="2.2" lm="5"><space unit="char" quantity="10"></space><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.4" punct="ps:5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="ps">e</seg></w>…</l>
					<l n="30" num="2.3" lm="10" met="5+5"><w n="30.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="30.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="30.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="30.5">s</w>’<w n="30.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="30.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="30.9" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</w>.</l>
				</lg>
			</div>
		</div></body></text></TEI>