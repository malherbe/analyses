<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC17" modus="sp" lm_max="8" metProfile="8, 6">
		<head type="main">Villon, qu’on chercherait…</head>
			<opener>
				<salute><hi rend="smallcap">À MAX JACOB</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="1.5" punct="vg:8">c<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</w>,</l>
			<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">N</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="2.4" punct="vg:3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="2.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="2.6" punct="vg:6">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
			<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.6" punct="pt:8">p<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
		</lg>
		<lg n="2">
			<l n="4" num="2.1" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.5" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
			<l n="5" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="5.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w></l>
			<l n="6" num="2.3" lm="8" met="8"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.4">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="6.5" punct="pt:8">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		</lg>
		<lg n="3">
			<l n="7" num="3.1" lm="8" met="8"><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="7.4" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
			<l n="8" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="8.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="497" place="4">Y</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
			<l n="9" num="3.3" lm="8" met="8"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="9.5" punct="pt:8">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
		</lg>
		<lg n="4">
			<l n="10" num="4.1" lm="8" met="8"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="10.2">T<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.4" punct="pt:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gd<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
			<l n="11" num="4.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1" punct="vg:2">B<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="11.3">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>h<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w></l>
			<l n="12" num="4.3" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">M<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>rs</w>-<w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w>-<w n="12.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rs</w>-<w n="12.5">qu</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w>-<w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w>-<w n="12.8" punct="vg:8">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
		</lg>
		<lg n="5">
			<l n="13" num="5.1" lm="8" met="8"><w n="13.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="13.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">B<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>x<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.6">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
			<l n="14" num="5.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1">B<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w>-<w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w>-<w n="14.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
			<l n="15" num="5.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> « <w n="15.4">d<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> » <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.7" punct="pt:8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg></w>.</l>
		</lg>
		<lg n="6">
			<l n="16" num="6.1" lm="8" met="8">… <w n="16.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="16.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="16.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.6" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		</lg>
	</div></body></text></TEI>