<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC15" rhyme="none" modus="sm" lm_max="7" metProfile="7">
		<head type="main">Madrigal</head>
			<opener>
				<salute><hi rend="smallcap">À RENÉ BIZET</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1" lm="7" met="7"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="2" num="1.2" lm="7" met="7"><w n="2.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w></l>
			<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w>-<w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="4.5" punct="pt:7">d<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></w>.</l>
		</lg>
		<lg n="2">
			<l n="5" num="2.1" lm="7" met="7"><w n="5.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="5.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="6" num="2.2" lm="7" met="7"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="6.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w></l>
			<l n="7" num="2.3" lm="7" met="7"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="7.5">qu</w>’<w n="7.6" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></w>.</l>
			<l n="8" num="2.4" lm="7" met="7"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2" punct="pe:3">h<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe">a</seg>s</w> ! <w n="8.3">l</w>’<w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
			<l n="9" num="2.5" lm="7" met="7"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w></l>
			<l n="10" num="2.6" lm="7" met="7"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.7" punct="pe:7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
		</lg>
	</div></body></text></TEI>