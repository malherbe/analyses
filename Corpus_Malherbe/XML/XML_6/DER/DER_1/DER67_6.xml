<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER67" modus="cm" lm_max="12" metProfile="6+6">
				<head type="number">LXVII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="1.2" punct="vg:3">C<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="vg">o</seg></w>, <w n="1.3" punct="vg:6">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="1.4">V<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="1.7">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="11" mp="Lc">ean</seg></w>-<w n="1.8">M<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rc</w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd</w>, <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="2.4">f<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.6">p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="2.9">l</w>’<w n="2.10"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rc</w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.7">t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.10" punct="vg:12">str<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="4.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>c</w><caesura></caesura> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="M">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="5.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>l</w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="6.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="6.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="6.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l</w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="7.3">p<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.8">pl<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.9">d</w>’<w n="7.10" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>tr<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="8.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="8.9">cr<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="9.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="9.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>ds</w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>s</w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="10.4">c<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="10.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>s</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.8">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>s</w></l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="11.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="C">eu</seg>r</w> <w n="11.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="11.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.10">r<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg>s</w> <w n="12.3">j<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5" punct="vg:6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>rs</w>,<caesura></caesura> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.7">b<seg phoneme="ø" type="vs" value="1" rule="247" place="8">œu</seg>fs</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.10" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2">ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="2">ai</seg>s</w> <w n="13.3">b<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s</w><caesura></caesura> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="13.8">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="13.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="14.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>cs</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="14.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>z</w><caesura></caesura> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="14.8">ru<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="14.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.11" punct="pi:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pi">eu</seg>rs</w> ?</l>
				</lg>
			</div></body></text></TEI>