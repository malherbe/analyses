<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER19" modus="sm" lm_max="8" metProfile="8">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="1.7">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6" punct="pt:8">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7" punct="pi:8">br<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>s</w> ?</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7" punct="pi:8">n<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2" punct="po:3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-27" place="3">e</seg></w> (<hi rend="ital"><w n="5.3" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>c</w>, <w n="5.4" punct="vg:5">h<seg phoneme="e" type="vs" value="1" rule="250" place="5" punct="vg">œ</seg>c</w>, <w n="5.5" punct="pv:6">h<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pv">o</seg>c</w> ; <w n="5.6" punct="vg:8">h<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>,</hi></l>
					<l n="6" num="2.2" lm="8" met="8"><hi rend="ital"><w n="6.1" punct="vg:2">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="6.2">h<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</w></hi>), <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="6.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="6.5">d</w>’<w n="6.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">j</w>’<w n="8.6" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="391" place="8" punct="pt">eu</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.6" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>rs</w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rs</w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ils</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5">fru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ts</w> <w n="11.6">m<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>rs</w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5" punct="pt:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Cu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2" punct="po:5"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-27" place="5">e</seg></w> (<hi rend="ital"><w n="13.3" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>c</w>, <w n="13.4" punct="vg:7">h<seg phoneme="e" type="vs" value="1" rule="250" place="7" punct="vg">œ</seg>c</w>, <w n="13.5">h<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>c</w></hi>),</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="14.4">t</w>-<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>sc</w>, <w n="14.8">l<seg phoneme="ə" type="ef" value="1" rule="DER19_1" place="9">e</seg></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1" punct="pi:4">C<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ps<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pi">i</seg>s</w> ? <w n="15.2">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w>-<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>scl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="16.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="16.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="16.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.6" punct="pi:8">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pi">o</seg>q</w> ?</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rsi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4">s</w>’<w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="18.6" punct="pt:8">l<seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="pt">à</seg></w>.</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="19.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">d<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <hi rend="ital"><w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w></hi></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="20.4" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="vg:2">Cu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.3">t</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t</w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="22.6">cr<seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.7">ch<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lc<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="24.5">di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="24.6">m</w>’<w n="24.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.4">l</w>’<w n="25.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</w> <w n="25.6">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="26.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="26.6">j</w>’<w n="26.7" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>s</w> ;</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">r<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="27.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">j</w>’<w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="28.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="28.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w>-<w n="28.8" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w>-<w n="29.4">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="29.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="30.3">li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="30.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="30.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="30.6" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="31.2">l</w>’<w n="31.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="31.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="31.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="31.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="31.7">m</w>’<w n="31.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="32.4">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="32.6" punct="pe:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>x</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="33.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="33.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="33.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="33.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="33.7" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2" punct="vg:2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>rs</w>, <w n="34.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="34.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="34.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="34.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="34.7" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="35.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="35.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="35.5" punct="pt:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>ts</w>.</l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="36.2">l</w>’<w n="36.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="36.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="36.6" punct="pt:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1" lm="8" met="8"><w n="37.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="37.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="37.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="37.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="37.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
					<l n="38" num="10.2" lm="8" met="8"><w n="38.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="38.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="38.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="38.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="38.5">n</w>’<w n="38.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="38.7" punct="vg:8">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="39" num="10.3" lm="8" met="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="39.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="39.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="39.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="39.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="39.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="40" num="10.4" lm="8" met="8"><w n="40.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="40.2">l</w>’<w n="40.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="40.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="40.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="40.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="40.7" punct="pt:8">n<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">ez</seg></w>.</l>
				</lg>
			</div></body></text></TEI>