<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN474" modus="cm" lm_max="10" metProfile="4+6">
					<head type="main">Dizain au lecteur</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="1.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fm">e</seg></w>-<w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="1.5">l</w>’<w n="1.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>cc<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="2.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="2.6" punct="vg:7">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>, <w n="2.7">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="2.8" punct="pt:10">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">V<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.6">B<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="4.3" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="4.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ç<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5" punct="tc:7">mi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="6">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg ti" mp="F">e</seg></w>, — <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.7" punct="pt:10">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></w>.</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="6.7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="6.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.10" punct="vg:10">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="7.5" punct="tc:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="ti" mp="F">e</seg></w> — <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="7.7">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="8.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="8.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="8.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="8.6" punct="pe:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>g</w> !</l>
						<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="pe:2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="pe">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" caesura="1">e</seg>c</w><caesura></caesura> <w n="9.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.5" punct="vg:7">r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.7">l</w>’<w n="9.8" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="10.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="10.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="10.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="10.8" punct="pt:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>g</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1873">Juin 1873.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>