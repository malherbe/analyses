<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN168" modus="sm" lm_max="8" metProfile="8">
				<head type="main">LES FORGERONS</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Rh<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>thm<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="1.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.3">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="3.5">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="3.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="3.7">l</w>’<w n="3.8" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="4.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="2">e</seg>r</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.7" punct="pt:8">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">Jean et Jacques.</head>
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="6.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ts</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.5">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6" punct="vg:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.6">br<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="8.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<head type="main"></head>
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">n<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="175" place="4">ë</seg>l</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="10.3" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="10.4">c</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.8" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">Jacques.</head>
					<l part="I" n="11" num="4.1" lm="8" met="8"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3" punct="vg:4">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="4" punct="vg">ean</seg></w>, </l>
				</lg>
				<lg n="5">
					<head type="main">Jean.</head>
					<l part="F" n="11" lm="8" met="8"><w n="11.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6" punct="vg:8">J<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
				</lg>
				<lg n="6">
					<head type="main">Jacques.</head>
					<l part="I" n="12" num="6.1" lm="8" met="8"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ffl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3" punct="pe:4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe">eu</seg></w> ! </l>
				</lg>
				<lg n="7">
					<head type="main">Jean.</head>
					<l part="F" n="12" lm="8" met="8"><w n="12.4">B<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6" punct="pe:8">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pe">e</seg>r</w> !</l>
				</lg>
				<lg n="8">
					<head type="main">Jacques.</head>
					<l n="13" num="8.1" lm="8" met="8"><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>r</w> <w n="13.2">gr<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="14" num="8.2" lm="8" met="8"><w n="14.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="14.6" punct="vg:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
					<l n="15" num="8.3" lm="8" met="8"><w n="15.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="8.4" lm="8" met="8"><w n="16.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="16.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="16.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></w>.</l>
				</lg>
				<lg n="9">
					<head type="main">Jean.</head>
					<l n="17" num="9.1" lm="8" met="8"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="17.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="17.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.4" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="18" num="9.2" lm="8" met="8"><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="18.3" punct="vg:4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="18.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="18.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</w>,</l>
					<l n="19" num="9.3" lm="8" met="8"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="19.6" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="20" num="9.4" lm="8" met="8"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="20.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.6">d</w>’<w n="20.7" punct="pe:8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</w> !</l>
				</lg>
				<lg n="10">
					<head type="main">Jacques.</head>
					<l n="21" num="10.1" lm="8" met="8"><w n="21.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="21.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.6" punct="pe:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="22" num="10.2" lm="8" met="8"><w n="22.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="22.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w></l>
					<l n="23" num="10.3" lm="8" met="8"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3" punct="vg:5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="24" num="10.4" lm="8" met="8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">ch<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
				</lg>
				<lg n="11">
					<head type="main">Jean.</head>
					<l n="25" num="11.1" lm="8" met="8"><w n="25.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="25.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="25.6" punct="vg:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="26" num="11.2" lm="8" met="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="26.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="26.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="26.6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
					<l n="27" num="11.3" lm="8" met="8"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="27.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="27.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>cs</w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="28" num="11.4" lm="8" met="8"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rb<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="28.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.6" punct="pt:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</w>.</l>
				</lg>
				<lg n="12">
					<head type="main">Jacques.</head>
					<l n="29" num="12.1" lm="8" met="8"><w n="29.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="29.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="29.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="29.6" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="30" num="12.2" lm="8" met="8"><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="30.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rb<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.4">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="30.5" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="445" place="8" punct="vg">û</seg>r</w>,</l>
					<l n="31" num="12.3" lm="8" met="8"><w n="31.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="31.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="31.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="31.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="31.5">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="32" num="12.4" lm="8" met="8"><w n="32.1">L</w>’<w n="32.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="32.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="32.6">d</w>’<w n="32.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>r</w>.</l>
				</lg>
				<lg n="13">
					<head type="main">Jean.</head>
					<l n="33" num="13.1" lm="8" met="8"><w n="33.1" punct="vg:3">L<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="33.2">d</w>’<w n="33.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="34" num="13.2" lm="8" met="8"><w n="34.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="34.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="34.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="34.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="34.5" punct="pv:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pv">e</seg>il</w> ;</l>
					<l n="35" num="13.3" lm="8" met="8"><w n="35.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="35.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="35.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="35.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="35.6">l</w>’<w n="35.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="36" num="13.4" lm="8" met="8"><w n="36.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="36.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="36.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="36.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g</w> <w n="36.6" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</w>.</l>
				</lg>
				<lg n="14">
					<head type="main">Jacques.</head>
					<l n="37" num="14.1" lm="8" met="8"><w n="37.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="37.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="37.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="37.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="37.5">s</w>’<w n="37.6" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="38" num="14.2" lm="8" met="8"><w n="38.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="38.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="38.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="38.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="38.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="38.6" punct="vg:8">cl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="39" num="14.3" lm="8" met="8"><w n="39.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="39.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="39.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="39.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="39.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="39.6" punct="vg:8">gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="40" num="14.4" lm="8" met="8"><w n="40.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="40.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="40.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="40.4" punct="pe:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
				</lg>
				<lg n="15">
					<head type="main">Jean.</head>
					<l n="41" num="15.1" lm="8" met="8"><w n="41.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="41.2">fr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="41.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="41.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="41.5" punct="pe:8">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="16">
					<head type="main">Jacques.</head>
					<l n="42" num="16.1" lm="8" met="8"><w n="42.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="42.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="42.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="42.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
					<l n="43" num="16.2" lm="8" met="8"><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="43.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg></w> <w n="43.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="43.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="43.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="43.6" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="17">
					<head type="main">Jean.</head>
					<l part="I" n="44" num="17.1" lm="8" met="8"><w n="44.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="44.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="44.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w></l>
				</lg>
				<lg n="18">
					<head type="main">Jacques.</head>
					<l part="F" n="44" lm="8" met="8"><w n="44.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">E</seg>t</w> <w n="44.5" punct="pe:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1859">octobre 1859</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>