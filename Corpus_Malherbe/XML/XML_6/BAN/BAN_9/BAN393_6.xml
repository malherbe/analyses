<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN393" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">Pour mademoiselle ***</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								22. Car la fille d’Hérodiade y étant entrée <lb></lb>
								et ayant dansé devant le roi, elle lui plut <lb></lb>
								tellement, et à ceux qui étaient à table avec <lb></lb>
								lui, qu’il lui dit : Demandez-moi ce que vous <lb></lb>
								voudrez, et je vous le donnerai. <lb></lb>
								23. Et il ajouta avec serment : Oui, je vous <lb></lb>
								donnerai tout ce que vous me demanderez, <lb></lb>
								quand ce serait la moitié de mon royaume. <lb></lb>
								24. Elle, étant sortie, dit à sa mère : Que <lb></lb>
								demanderai-je ? Sa mère lui répondit : La tête <lb></lb>
								de Jean-Baptiste.
							</quote>
							<bibl>
								<hi rend="ital">Évangile selon saint Marc.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="Lc">a</seg>s</w>-<w n="1.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem/mc">e</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>fs</w>,<caesura></caesura> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="1.6">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.8" punct="vg:12">B<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>cch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">I</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5" punct="vg:6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="2.7">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="2.8">d</w>’<w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="2.10" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ts</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.6" punct="vg:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="4.5">cr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.6">d</w>’<w n="4.7" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pe">ain</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="5.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="5.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="5.9" punct="vg:12">P<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">B<seg phoneme="ɛ" type="vs" value="1" rule="339" place="1" mp="M">a</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="6.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="6.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>rs</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="6.8" punct="vg:12">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="7.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.9">l</w>’<w n="7.10" punct="pe:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">In</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="8.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="8.9" punct="pe:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pe:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>lssl<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">er</seg></w> ! <w n="9.2" punct="pe:6">T<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>gl<seg phoneme="i" type="vs" value="1" rule="dc-1" place="4" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe" caesura="1">i</seg></w> !<caesura></caesura> <w n="9.3" punct="pe:9">C<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rl<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="pe">a</seg></w> ! <w n="9.4">s<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>rs</w> <w n="9.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rs<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.4" punct="vg:6">gu<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="10.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rds</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.8" punct="vg:12">h<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2" punct="vg:3">f<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="11.8" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.7" punct="pe:12">C<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="13.2">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="13.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>t</w>,<caesura></caesura> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.7">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="13.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="13.9" punct="pe:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</w> !</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="14.3">H<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="14.5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.6">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="9">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="14.8" punct="vg:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</w>,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="15.7" punct="vg:6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="15.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</w>, <w n="15.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="15.10">v<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="15.11" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</w> !</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2" mp="M">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="16.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="16.3" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rds</w>,<caesura></caesura> <w n="16.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="16.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="16.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="16.9">l</w>’<w n="16.10" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Février 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>