<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN223" modus="sp" lm_max="7" metProfile="7, 5">
				<head type="number">XXXI</head>
				<head type="main">Darcier</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="1">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="6">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w></l>
					<l n="2" num="1.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.3" punct="vg:5">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rd</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rd</w>’<w n="3.5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w></l>
					<l n="4" num="1.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pt">an</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w></l>
					<l n="6" num="2.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3" punct="vg:5">D<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rci<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>,</l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="7.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w></l>
					<l n="8" num="2.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="pt:5">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lb<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="9.2" punct="pe:3">D<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rci<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="pe">er</seg></w> ! <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="pe:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pe">oi</seg>s</w> !</l>
					<l n="10" num="3.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">rh<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w></l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">d<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w></l>
					<l n="12" num="3.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="12.3" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="13.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5" punct="vg:7">di<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg></w>,</l>
					<l n="14" num="4.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="14.1" punct="vg:1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4" punct="vg:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>r</w>,</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="15.3">br<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="15.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w></l>
					<l n="16" num="4.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="16.1">D</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg>r</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1" punct="vg:2">P<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="17.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="17.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rb<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w></l>
					<l n="18" num="5.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="18.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="18.3" punct="vg:5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg">eu</seg>rs</w>,</l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="19.2"><seg phoneme="y" type="vs" value="1" rule="251" place="2">eû</seg>t</w> <w n="19.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="19.4">qu</w>’<w n="19.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="19.7">b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w></l>
					<l n="20" num="5.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="20.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3" punct="vg:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg">eu</seg>rs</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="7" met="7"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="21.5"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="21.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7">en</seg>t</w></l>
					<l n="22" num="6.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="22.1" punct="pe:5">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pe">e</seg>l</w> !</l>
					<l n="23" num="6.3" lm="7" met="7"><w n="23.1">Pr<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="23.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7">en</seg>t</w></l>
					<l n="24" num="6.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="24.1">L</w>’<w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4" punct="pt:5">fi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pt">e</seg>l</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="7" met="7"><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="25.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="25.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w></l>
					<l n="26" num="7.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="26.4">d</w>’<w n="26.5" punct="vg:5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="5" punct="vg">ue</seg>il</w>,</l>
					<l n="27" num="7.3" lm="7" met="7"><w n="27.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>ts</w></l>
					<l n="28" num="7.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="28.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="28.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="28.4" punct="vg:5">d<seg phoneme="œ" type="vs" value="1" rule="406" place="5" punct="vg">eu</seg>il</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="7" met="7"><w n="29.1" punct="vg:2">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="29.2">h<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="29.4" punct="vg:7">r<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>x</w>,</l>
					<l n="30" num="8.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="30.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="30.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="30.3" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg>s</w>,</l>
					<l n="31" num="8.3" lm="7" met="7"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="31.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w></l>
					<l n="32" num="8.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="32.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="32.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="32.4" punct="pt:5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="pt">u</seg>s</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="7" met="7"><w n="33.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="33.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="33.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="33.4" punct="vg:7">h<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>ts</w>,</l>
					<l n="34" num="9.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="34.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="34.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="34.3" punct="vg:3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="34.4">l</w>’<w n="34.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w></l>
					<l n="35" num="9.3" lm="7" met="7"><w n="35.1">D</w>’<w n="35.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="35.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="35.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="35.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>cs</w> <w n="35.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w></l>
					<l n="36" num="9.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="36.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="36.2" punct="pv:5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pv">on</seg>t</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1" lm="7" met="7"><w n="37.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="37.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="37.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="37.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="37.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w></l>
					<l n="38" num="10.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="38.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="38.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="38.3" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>,</l>
					<l n="39" num="10.3" lm="7" met="7"><w n="39.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="39.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="39.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="39.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="39.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w></l>
					<l n="40" num="10.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="40.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="40.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="40.3" punct="pt:5">p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pt">é</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1" lm="7" met="7"><w n="41.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="41.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="41.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="41.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="41.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w></l>
					<l n="42" num="11.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="42.1">Qu</w>’<w n="42.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="42.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="42.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="42.5" punct="vg:5">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>s</w>,</l>
					<l n="43" num="11.3" lm="7" met="7"><w n="43.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="43.2" punct="vg:5">M<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rs<seg phoneme="e" type="vs" value="1" rule="383" place="3">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="43.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w></l>
					<l n="44" num="11.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="44.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="44.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ts</w> <w n="44.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="44.4" punct="vg:5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>s</w>,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1" lm="7" met="7"><w n="45.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="45.2">cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="45.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="45.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="45.5">V<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w></l>
					<l n="46" num="12.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="46.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="46.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l</w></l>
					<l n="47" num="12.3" lm="7" met="7"><w n="47.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="47.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="47.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="47.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w></l>
					<l n="48" num="12.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="48.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="48.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="48.3" punct="pt:5"><seg phoneme="i" type="vs" value="1" rule="468" place="3">I</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt">a</seg>l</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1" lm="7" met="7"><w n="49.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="49.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="49.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="49.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w>-<w n="49.5" punct="vg:7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>l</w>,</l>
					<l n="50" num="13.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="50.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="50.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l</w> <w n="50.3">n</w>’<w n="50.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="50.5" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>t</w> : <w n="50.6" punct="vg:5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>,</l>
					<l n="51" num="13.3" lm="7" met="7"><w n="51.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="51.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="51.3" punct="vg:5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="5" punct="vg">e</seg>r</w>, <w n="51.4" punct="vg:7">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>l</w>,</l>
					<l n="52" num="13.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="52.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="52.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="52.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="52.4" punct="vg:5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="5" punct="vg">om</seg></w>,</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1" lm="7" met="7"><w n="53.1" punct="vg:2">D<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="53.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="53.3" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</w>,</l>
					<l n="54" num="14.2" lm="5" met="5"><space quantity="4" unit="char"></space><w n="54.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="54.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="54.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>,</l>
					<l n="55" num="14.3" lm="7" met="7"><w n="55.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="55.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="55.3">d</w>’<w n="55.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="55.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w></l>
					<l n="56" num="14.4" lm="5" met="5"><space quantity="4" unit="char"></space><w n="56.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="56.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="56.3" punct="pe:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe">er</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">28 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>