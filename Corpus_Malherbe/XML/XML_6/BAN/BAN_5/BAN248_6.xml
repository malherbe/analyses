<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN248" modus="sm" lm_max="7" metProfile="7">
				<head type="number">LVI</head>
				<head type="main">La Mercière</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">D</w>’<w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="1.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="1.4" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pi">ou</seg>s</w> ? <w n="1.5">D<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.6" punct="pi:7">L<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pi">on</seg></w> ?</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="2.2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.6" punct="pt:7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="3.6" punct="vg:7">B<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1" punct="vg:2">M<seg phoneme="œ" type="vs" value="1" rule="151" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>r</w>, <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w>-<w n="4.4" punct="pt:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="5.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">Gu<seg phoneme="i" type="vs" value="1" rule="485" place="5">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w></l>
					<l n="6" num="2.2" lm="7" met="7"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="6.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="6.5" punct="pi:7">ch<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ff<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></w> ?</l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="7.2" punct="pt:2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" punct="pt">e</seg></w>. <w n="7.3">C</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.6" punct="vg:7">S<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="i" type="vs" value="1" rule="493" place="7" punct="vg">y</seg></w>,</l>
					<l n="8" num="2.4" lm="7" met="7"><w n="8.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>ts</w>, <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w></l>
					<l n="10" num="3.2" lm="7" met="7"><w n="10.1">F<seg phoneme="œ" type="vs" value="1" rule="304" place="1">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.6" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="11.6" punct="pt:7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pt">oi</seg>r</w>.</l>
					<l n="12" num="3.4" lm="7" met="7"><w n="12.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="12.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="12.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w>-<w n="12.5" punct="vg:7">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="8" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">J<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.7">R<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w></l>
					<l n="14" num="4.2" lm="7" met="7"><w n="14.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="14.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4" punct="pe:7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.4" punct="pi:7">P<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pi">i</seg>s</w> ?</l>
					<l n="16" num="4.4" lm="7" met="7"><w n="16.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.4" punct="vg:4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>s</w>, <w n="16.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w>-<w n="16.7" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.4" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="17.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="17.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>,</l>
					<l n="18" num="5.2" lm="7" met="7"><w n="18.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="18.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5" punct="vg:7">r<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">m</w>’<w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="19.5">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="19.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w></l>
					<l n="20" num="5.4" lm="7" met="7"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3" punct="pt:7">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">1er février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>