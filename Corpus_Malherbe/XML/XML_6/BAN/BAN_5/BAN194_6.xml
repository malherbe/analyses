<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN194" modus="sm" lm_max="8" metProfile="8">
				<head type="number">II</head>
				<head type="main">Lili</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4">L<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.5">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="4.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="4.7" punct="pt:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">fr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="6.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="7.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="7.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="7">œ</seg>il</w> <w n="7.7" punct="vg:8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.6">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.8" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="9.2">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.3" punct="vg:4">P<seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>l</w>, <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.5" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Hi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>r</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="10.4">Z<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.5" punct="vg:6">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">H<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="12.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.4" punct="pt:8">p<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="13.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="13.6" punct="pv:8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></w> ;</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">c</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.4">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.8" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="15.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">bl<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="16.6" punct="pt:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="17.6" punct="pv:8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></w> ;</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">c</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="18.4">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="18.5">qu</w>’<w n="18.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="18.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.8" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="19.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="19.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="19.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.3">bl<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="20.6" punct="pt:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">1er décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>