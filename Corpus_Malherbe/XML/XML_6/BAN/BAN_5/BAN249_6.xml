<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN249" modus="sm" lm_max="8" metProfile="8">
				<head type="number">LVII</head>
				<head type="main">Païva</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="1.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="7">y</seg>s</w> <w n="1.6" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="2.5" punct="vg:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="343" place="6">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="7">ï</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="4.3" punct="tc:4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">î</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg ti">ai</seg>t</w>, — <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6" punct="pt:8">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2">l</w>’<w n="5.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="5.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.6" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.4" punct="vg:8">br<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>lp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="pt:8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">gr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rcl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="10.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>p<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3">l</w>’<w n="11.4" punct="dp:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp">i</seg>t</w> : <w n="11.5" punct="vg:8">G<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t</w>-<w n="12.2" punct="vg:3">V<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>r</w>, <w n="12.3" punct="vg:6">G<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="12.4" punct="pv:8">H<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="BAN249_1" place="8">ay</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">D</w>’<w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="13.3" punct="dp:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="dp">o</seg>r</w> : <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ss<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.4">c<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="16.6" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="17.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5" punct="vg:8">B<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="493" place="8" punct="vg">y</seg></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="19.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Pl<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="20.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="20.5">d</w>’<w n="20.6" punct="dp:8">H<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">Z<seg phoneme="ø" type="vs" value="1" rule="403" place="1">eu</seg>s</w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="21.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="21.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5">d</w>’<w n="21.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="487" place="7">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="22.3">pr<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="22.4" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="23.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.3" punct="vg:5">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="vg">in</seg></w>, <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="25.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="25.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="25.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="25.5">fr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cs</w> <w n="25.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>ts</w>,</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="26.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="26.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>th<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="27.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="27.5">br<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="27.6">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t</w> <w n="28.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="28.3">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="28.4" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="29.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="29.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="29.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rds</w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="30.4" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="31.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="31.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="31.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="31.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="31.6" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rds</w>.</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="32.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="32.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="32.4">s<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="32.5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="32.6" punct="vg:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1" punct="vg:2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="33.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="33.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="33.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="33.5" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</w>,</l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="34.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="34.3">c<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="34.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="34.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="34.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="35.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w> <w n="35.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="35.5">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">J<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="36.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="36.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="36.6" punct="pv:8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1" lm="8" met="8"><w n="37.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="37.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="37.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="37.4" punct="vg:5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="37.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="37.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>s</w></l>
					<l n="38" num="10.2" lm="8" met="8"><w n="38.1">D</w>’<w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="38.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="38.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="39" num="10.3" lm="8" met="8"><w n="39.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="39.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="39.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="39.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>ll<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</w></l>
					<l n="40" num="10.4" lm="8" met="8"><w n="40.1">D</w>’<w n="40.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="40.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="40.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="40.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="40.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="40.7" punct="pv:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1" lm="8" met="8"><w n="41.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="41.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="41.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="41.4" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>,</l>
					<l n="42" num="11.2" lm="8" met="8"><w n="42.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="42.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="42.3" punct="vg:5">d<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="42.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="42.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="42.6">tr<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="43" num="11.3" lm="8" met="8"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="43.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="43.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="43.4" punct="vg:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>,</l>
					<l n="44" num="11.4" lm="8" met="8"><w n="44.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="44.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="44.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="44.4" punct="pt:8">d<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">6 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>