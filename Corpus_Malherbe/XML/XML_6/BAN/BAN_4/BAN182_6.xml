<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN182" modus="sm" lm_max="8" metProfile="8">
				<head type="main">Caprice</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6" punct="vg:8">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt</w> <w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.4" punct="vg:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="4.5" punct="vg:8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.6" punct="vg:8">br<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">t</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.8" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>s</w> : <w n="7.5">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g</w> <w n="7.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">M</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.3" punct="vg:3">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>ps</w> <w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.7" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pe">en</seg></w> !</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ds</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.5" punct="dp:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.4" punct="pe:4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4" punct="pe">e</seg>ds</w> ! <w n="10.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="10.8" punct="vg:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></w>,</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">c</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="11.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w></l>
					<l n="12" num="2.6" lm="8" met="8"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Février 1861.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>