<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN717" modus="cp" lm_max="10" metProfile="5, 5+5">
				<head type="main">Au laurier</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.7"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w></l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="2.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="2.6">v<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="2.7" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4" punct="vg:5">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="3.5">c</w>’<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="3.7" punct="vg:7">t<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="3.8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="3.9" punct="vg:10">L<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="pt:5">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="M">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.3" punct="vg:5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="353" place="6" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.6" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>ts</w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4" punct="vg:5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7" punct="vg:10">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="7.3" punct="vg:5">r<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="7.5">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="7.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w></l>
					<l n="8" num="2.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4" punct="pt:5">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="9.3" punct="vg:5">r<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>s</w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ds</w> <w n="9.7">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="9.8" punct="vg:10">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>rs</w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="10.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="10.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">É</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg>x</w> <w n="11.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>rs</w></l>
					<l n="12" num="3.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="12.2">s<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.5" punct="pt:5">L<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="13.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cs</w> <w n="13.8" punct="vg:10">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>s</w>,</l>
					<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" caesura="1">o</seg>r</w><caesura></caesura> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="14.8" punct="vg:10">h<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>l</w><caesura></caesura> <w n="15.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="15.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w></l>
					<l n="16" num="4.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="16.4" punct="pt:5">H<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="10" met="5+5"><w n="17.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="17.2" punct="vg:3">Qu<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="17.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="17.6" punct="vg:10">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
					<l n="18" num="5.2" lm="10" met="5+5"><w n="18.1">Br<seg phoneme="y" type="vs" value="1" rule="445" place="1" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>r</w><caesura></caesura> <w n="18.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="18.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="19" num="5.3" lm="10" met="5+5"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="M">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="19.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="19.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="19.7" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
					<l n="20" num="5.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="20.1">C<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="20.3" punct="pt:5">m<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="10" met="5+5"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="21.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="21.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w></l>
					<l n="22" num="6.2" lm="10" met="5+5"><w n="22.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="22.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>r</w><caesura></caesura> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="22.5" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="23" num="6.3" lm="10" met="5+5"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="23.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="23.3" punct="vg:5">L<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="o" type="vs" value="1" rule="438" place="5" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="23.4" punct="vg:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</w>, <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="9" mp="M">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w></l>
					<l n="24" num="6.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="24.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="24.3" punct="pt:5">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="10" met="5+5"><w n="25.1" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>sch<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="25.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="25.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ds</w> <w n="25.6" punct="vg:10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="vg">œu</seg>rs</w>,</l>
					<l n="26" num="7.2" lm="10" met="5+5"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="26.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="26.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>ts</w><caesura></caesura> <w n="26.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="26.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="26.7" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="27" num="7.3" lm="10" met="5+5"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="27.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="27.3">rh<seg phoneme="i" type="vs" value="1" rule="493" place="4" mp="M">y</seg>thm<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="27.4">l</w>’<w n="27.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="27.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="27.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="27.8" punct="vg:10">ch<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="vg">œu</seg>rs</w>,</l>
					<l n="28" num="7.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="28.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="28.3" punct="pt:5">M<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="10" met="5+5"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="29.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">A</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="29.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="29.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="29.7" punct="vg:10">m<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</w>,</l>
					<l n="30" num="8.2" lm="10" met="5+5"><w n="30.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="30.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="30.5" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="31" num="8.3" lm="10" met="5+5"><w n="31.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="31.3" punct="vg:5">c<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg" caesura="1">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="31.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t</w> <w n="31.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="31.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w></l>
					<l n="32" num="8.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="32.1" punct="vg:2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="32.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.3" punct="pt:5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">O</seg>rph<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>