<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN771" modus="sp" lm_max="6" metProfile="6, 2">
				<head type="main">Nuit</head>
				<lg n="1">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1" punct="vg:1">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>ch</w>, <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4" punct="vg:6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ct<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.3">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w></l>
					<l n="3" num="1.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="3.1" punct="vg:2">T<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="4.5" punct="pt:6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="5.3" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w></l>
					<l n="7" num="2.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</w></l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>s</w> <w n="8.2">d</w>’<w n="8.3" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pt">en</seg>ts</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="9.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w></l>
					<l n="11" num="3.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="11.1">S</w>’<w n="11.2" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="12.2" punct="vg:4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="12.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w>-<w n="12.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>l</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4" punct="pe:6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w></l>
					<l n="15" num="4.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="15.1" punct="pe:2">Sh<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ksp<seg phoneme="i" type="vs" value="1" rule="BAN771_1" place="2">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></w> !</l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="16.5" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="6" met="6"><w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="17.4" punct="pe:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="18.5">b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w></l>
					<l n="19" num="5.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="19.1">C<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></w></l>
					<l n="20" num="5.4" lm="6" met="6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="20.5" punct="pt:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="6" met="6"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="21.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="21.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
					<l n="22" num="6.2" lm="6" met="6"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="22.4" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>,</l>
					<l n="23" num="6.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="23.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</w></l>
					<l n="24" num="6.4" lm="6" met="6"><w n="24.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="2">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="24.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="24.3" punct="pt:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="6" met="6"><w n="25.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="25.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="25.4" punct="vg:6">g<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
					<l n="26" num="7.2" lm="6" met="6"><w n="26.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="26.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="26.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w></l>
					<l n="27" num="7.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="27.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="27.2" punct="vg:2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg>s</w>,</l>
					<l n="28" num="7.4" lm="6" met="6"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="28.2">P<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="28.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="28.5" punct="pt:6">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pt">e</seg>cq</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="6" met="6"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="29.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.5" punct="vg:6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="30" num="8.2" lm="6" met="6"><w n="30.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="30.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="30.4">F<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">aim</seg></w></l>
					<l n="31" num="8.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="31.1" punct="vg:2">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="32" num="8.4" lm="6" met="6"><w n="32.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="32.3" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="6" met="6"><w n="33.1">D</w>’<w n="33.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="33.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="33.4">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="33.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="34" num="9.2" lm="6" met="6"><w n="34.1">J</w>’<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="34.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="34.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="34.5">l</w>’<w n="34.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="34.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w></l>
					<l n="35" num="9.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="35.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="35.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></w></l>
					<l n="36" num="9.4" lm="6" met="6"><w n="36.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="36.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="36.3" punct="pt:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="pt">e</seg>t</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1" lm="6" met="6"><w n="37.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="37.2">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="37.3">d</w>’<w n="37.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
					<l n="38" num="10.2" lm="6" met="6"><w n="38.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="38.2">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="38.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="38.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w></l>
					<l n="39" num="10.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="39.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="39.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</w></l>
					<l n="40" num="10.4" lm="6" met="6"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="40.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="40.3" punct="pt:6"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1" lm="6" met="6"><w n="41.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="41.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="41.3" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>l</w>, <w n="41.4"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="41.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="41.6" punct="pe:6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
					<l n="42" num="11.2" lm="6" met="6"><w n="42.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="42.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="42.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="42.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w></l>
					<l n="43" num="11.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="43.1">S</w>’<w n="43.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></w></l>
					<l n="44" num="11.4" lm="6" met="6"><w n="44.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="44.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="44.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg>r</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1" lm="6" met="6"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="45.2">d</w>’<w n="45.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="45.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="45.5">d</w>’<w n="45.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</w></l>
					<l n="46" num="12.2" lm="6" met="6"><w n="46.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="46.2">l</w>’<w n="46.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="46.4" punct="vg:4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="46.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="46.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w></l>
					<l n="47" num="12.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="47.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="47.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</w></l>
					<l n="48" num="12.4" lm="6" met="6"><w n="48.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="48.2">j</w>’<w n="48.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="48.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="48.5" punct="pt:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>x</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1" lm="6" met="6"><w n="49.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="49.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="49.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="49.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="49.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</w></l>
					<l n="50" num="13.2" lm="6" met="6"><w n="50.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="50.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="50.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="50.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rts</w></l>
					<l n="51" num="13.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="51.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="51.2">tr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg>s</w></l>
					<l n="52" num="13.4" lm="6" met="6"><w n="52.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="52.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="52.3" punct="pi:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi">a</seg>rds</w> ?</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1" lm="6" met="6"><w n="53.1" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pe">i</seg>s</w> ! <w n="53.2">L<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="53.3" punct="vg:6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="54" num="14.2" lm="6" met="6"><w n="54.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="54.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="54.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w></l>
					<l n="55" num="14.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="55.1" punct="vg:2">M<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="56" num="14.4" lm="6" met="6"><w n="56.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="56.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="56.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="56.4" punct="pt:6">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1" lm="6" met="6"><w n="57.1" punct="vg:2">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="57.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="57.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="57.4">V<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="58" num="15.2" lm="6" met="6"><w n="58.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="58.2">d</w>’<w n="58.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="58.4" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>ts</w>,</l>
					<l n="59" num="15.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="59.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></w></l>
					<l n="60" num="15.4" lm="6" met="6"><w n="60.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="60.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="60.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="60.4" punct="pv:5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pv">i</seg>r</w> ; <w n="60.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w></l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1" lm="6" met="6"><w n="61.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="61.2">n</w>’<w n="61.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="61.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="61.5" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="62" num="16.2" lm="6" met="6"><w n="62.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="62.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="62.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="62.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="62.5">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w></l>
					<l n="63" num="16.3" lm="2" met="2"><space quantity="8" unit="char"></space><w n="63.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="63.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></w></l>
					<l n="64" num="16.4" lm="6" met="6"><w n="64.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="64.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="64.3" punct="pt:6">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pt">i</seg>t</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1891"> 3 mars 1891.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>