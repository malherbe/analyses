<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN45" modus="sm" lm_max="5" metProfile="5">
					<lg n="1">
						<l n="1" num="1.1" lm="5" met="5"><w n="1.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="vg">oi</seg></w>, <w n="1.2" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="5" met="5"><w n="2.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>r</w>,</l>
						<l n="3" num="1.3" lm="5" met="5"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="3.3" punct="vg:5">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ph<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="5" met="5"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ph<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
						<l n="5" num="2.2" lm="5" met="5"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w></l>
						<l n="6" num="2.3" lm="5" met="5"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="6.5" punct="vg:5">f<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="5" met="5"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">d</w>’<w n="7.4" punct="pi:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pi">ou</seg>r</w> ?</l>
					</lg>
					<lg n="4">
						<l n="8" num="4.1" lm="5" met="5">—<w n="8.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="vg">oi</seg></w>, <w n="8.2">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="8.3" punct="vg:5">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="5">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
						<l n="9" num="4.2" lm="5" met="5"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w></l>
						<l n="10" num="4.3" lm="5" met="5"><w n="10.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="10.3" punct="vg:5">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="11" num="5.1" lm="5" met="5"><w n="11.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="11.3" punct="vg:5">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
						<l n="12" num="5.2" lm="5" met="5"><w n="12.1">L<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>s</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w></l>
						<l n="13" num="5.3" lm="5" met="5"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					</lg>
					<lg n="6">
						<l n="14" num="6.1" lm="5" met="5"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.4" punct="pi:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pi">an</seg>t</w> ?</l>
					</lg>
					<lg n="7">
						<l n="15" num="7.1" lm="5" met="5">—<w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
						<l n="16" num="7.2" lm="5" met="5"><w n="16.1">Br<seg phoneme="y" type="vs" value="1" rule="445" place="1">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="16.3" punct="dp:5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" punct="dp">un</seg></w> :</l>
						<l n="17" num="7.3" lm="5" met="5"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="17.2">s<seg phoneme="u" type="vs" value="1" rule="428" place="2">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.4" punct="pi:5">fl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="8">
						<l n="18" num="8.1" lm="5" met="5">—<w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2">s<seg phoneme="u" type="vs" value="1" rule="428" place="2">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4" punct="pe:5">fl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
						<l n="19" num="8.2" lm="5" met="5"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="19.4" punct="dp:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="5" punct="dp">um</seg></w> :</l>
						<l n="20" num="8.3" lm="5" met="5"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.4">l</w>’<w n="20.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="9">
						<l n="21" num="9.1" lm="5" met="5"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="21.2" punct="vg:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>t</w>, <w n="21.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="21.4" punct="pe:5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" punct="pe">un</seg></w> !</l>
					</lg>
					<lg n="10">
						<l n="22" num="10.1" lm="5" met="5">—<w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2">h<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
						<l n="23" num="10.2" lm="5" met="5"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="23.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="24" num="10.3" lm="5" met="5"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3" punct="pt:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="11">
						<l n="25" num="11.1" lm="5" met="5">—<w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
						<l n="26" num="11.2" lm="5" met="5"><w n="26.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="26.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="27" num="11.3" lm="5" met="5"><w n="27.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4" punct="vg:5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="12">
						<l n="28" num="12.1" lm="5" met="5"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="28.3" punct="pv:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pv">i</seg>r</w> ;</l>
					</lg>
					<lg n="13">
						<l n="29" num="13.1" lm="5" met="5"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="29.3">l</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="29.5" punct="vg:5">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
						<l n="30" num="13.2" lm="5" met="5"><w n="30.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="30.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="30.3">l</w>’<w n="30.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w></l>
						<l n="31" num="13.3" lm="5" met="5"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="31.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="31.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="31.4" punct="pt:5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="14">
						<l n="32" num="14.1" lm="5" met="5"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>h</w> <w n="32.2" punct="pe:2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg></w> ! <w n="32.3">N<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
						<l n="33" num="14.2" lm="5" met="5"><w n="33.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="33.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w></l>
						<l n="34" num="14.3" lm="5" met="5"><w n="34.1">L</w>’<w n="34.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="34.4" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="15">
						<l n="35" num="15.1" lm="5" met="5"><w n="35.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3" punct="pe:5">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe">er</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1841">février 1841</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>