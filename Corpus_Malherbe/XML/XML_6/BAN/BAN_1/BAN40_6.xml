<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN40" modus="cp" lm_max="12" metProfile="8, 6+6, (4+6)">
					<head type="main">AMOUR ANGÉLIQUE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Oh ! l’amour ! dit-elle, — et sa <lb></lb>
									voix tremblait et son œil rayonnait, <lb></lb>
									-c’est être deux et n’être qu’un. <lb></lb>
									Un homme et une femme qui se <lb></lb>
									fondent en un ange,c’est le ciel.</quote>
								<bibl>
									<name>VICTOR HUGO</name>, <hi rend="ital">Notre-Dame de Paris</hi> <lb></lb>, liv. II, chap. VII.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="1.4">qu</w>’<w n="1.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="Lc">i</seg></w>-<w n="1.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="1.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.8">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.11">pr<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>gs</w> <w n="2.8" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6" met_alone="True"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4" punct="vg:4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="3.9" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">d<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.8" punct="pt:12">M<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="œ" type="vs" value="1" rule="345" place="5">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="6.6" punct="vg:8">v<seg phoneme="ø" type="vs" value="1" rule="248" place="8" punct="vg">œu</seg>x</w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="9" num="2.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="9.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="9.5" punct="pe:8">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="8" punct="pe">eu</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">É</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="10.5">c</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.8">s<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="10.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="11" num="3.2" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="11.7">m<seg phoneme="i" type="vs" value="1" rule="493" place="8" mp="M">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="11.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</w>,</l>
						<l n="12" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.5">ph<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="13" num="3.4" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">s<seg phoneme="u" type="vs" value="1" rule="428" place="2">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="13.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>ds</w> <w n="13.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="13.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="13.9" punct="pt:12">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="14" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="14.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="14.5" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</w> :</l>
						<l n="15" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="15.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</w>,</l>
						<l n="16" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>rs</w> <w n="16.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="16.6" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="17" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="18" num="4.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="18.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="18.6" punct="pe:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>rs</w> !</l>
					</lg>
					<lg n="5">
						<l n="19" num="5.1" lm="12" met="6+6"><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="19.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="19.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="19.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="19.7">d</w>’<w n="19.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="19.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="19.10" punct="vg:12">j<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="20" num="5.2" lm="12" met="6+6"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="20.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="20.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6" punct="vg" caesura="1">um</seg>s</w>,<caesura></caesura> <w n="20.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="20.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="20.8" punct="vg:12">mi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="vg">e</seg>l</w>,</l>
						<l n="21" num="5.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="21.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="21.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="22" num="5.4" lm="12" met="6+6"><w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="22.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="22.4">ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="22.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="22.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="22.7">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="22.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="22.9" punct="pt:12">s<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="23" num="6.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="23.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t</w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="23.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.8" punct="dp:8">v<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="24" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="24.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="24.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="24.8">fi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w></l>
						<l n="25" num="6.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="25.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="25.6">n<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="26" num="6.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="27" num="6.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="27.3">s</w>’<w n="27.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="27.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="27.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.7" punct="pe:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pe">e</seg>l</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">juin 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>