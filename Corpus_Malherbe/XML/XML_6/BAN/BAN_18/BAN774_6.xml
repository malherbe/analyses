<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
				<title type="medium">Une édition électronique</title>
				<title type="part">Extrait (deux poèmes)</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>60 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BAN_18</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Banville_-_Petit_Trait%C3%A9_de_po%C3%A9sie_fran%C3%A7aise,_1881.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
								<author>Théodore de Banville</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER, ÉDITEUR</publisher>
									<date when="1881">1881</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1881">1881</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraits : poèmes de l’auteur.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">XI. Conclusion</head><div type="poem" key="BAN774" modus="cm" lm_max="9" metProfile="5+4">
					<head type="main">VERS DE NEUF SYLLABES, <lb></lb>AVEC UNE SEULE CÉSURE PLACÉE <lb></lb>APRÈS LA CINQUIÈME SYLLABE</head>
					<lg n="1">
						<head type="main">Le Poète.</head>
						<l n="1" num="1.1" lm="9" met="5+4"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="1.4">l</w>’<w n="1.5" punct="tc:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="ti" caesura="1">e</seg>r</w> —<caesura></caesura> <w n="1.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.8" punct="vg:9">f<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="vg">eu</seg>r</w>,</l>
						<l n="2" num="1.2" lm="9" met="5+4"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="2.4" punct="tc:5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="ti" caesura="1">ai</seg>s</w> —<caesura></caesura> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="2.6" punct="vg:9">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="9" met="5+4"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.3" punct="tc:5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="ti" caesura="1">oi</seg>t</w> —<caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="3.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w></l>
						<l n="4" num="1.4" lm="9" met="5+4"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" mp="P">e</seg>rs</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.5" punct="tc:5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="ti" caesura="1">i</seg>t</w> —<caesura></caesura> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="4.7" punct="pt:9"><seg phoneme="ø" type="vs" value="1" rule="405" place="7" mp="M">Eu</seg>r<seg phoneme="i" type="vs" value="1" rule="493" place="8" mp="M">y</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="9" met="5+4"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="5.3" punct="tc:5"><seg phoneme="e" type="vs" value="1" rule="354" place="3" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="ti" caesura="1">é</seg></w> —<caesura></caesura> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>s</w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="7">œ</seg>il</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.8" punct="pt:9">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="9" punct="pt">eu</seg>x</w>.</l>
						<l n="6" num="2.2" lm="9" met="5+4"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="6.3" punct="tc:5">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="ti" caesura="1">on</seg>s</w> —<caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="6.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg></w></l>
						<l n="7" num="2.3" lm="9" met="5+4"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="7.3" punct="tc:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="ti" caesura="1">an</seg>t</w> —<caesura></caesura> <w n="7.4" punct="vg:9">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="9" punct="vg">eu</seg>x</w>,</l>
						<l n="8" num="2.4" lm="9" met="5+4"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>fs</w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4" punct="tc:5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="ti" caesura="1">u</seg>s</w> —<caesura></caesura> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="8.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.7" punct="pt:9">L<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="9" met="5+4"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3" punct="tc:5">f<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="ti" caesura="1">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> —<caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="9.5" punct="vg:9">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg></w>,</l>
						<l n="10" num="3.2" lm="9" met="5+4"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">c</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="10.5" punct="tc:5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="vg ti" caesura="1">ain</seg></w>,<caesura></caesura> — <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="10.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t</w> <w n="10.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.9">l</w>’<w n="10.10">H<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg></w></l>
						<l n="11" num="3.3" lm="9" met="5+4"><w n="11.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.4" punct="tc:5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="5" punct="ti" caesura="1">o</seg>ts</w> —<caesura></caesura> <w n="11.5" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rt</w>, <w n="11.6" punct="vg:9">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg></w>,</l>
						<l n="12" num="3.4" lm="9" met="5+4"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="12.4" punct="tc:5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="5" punct="ti" caesura="1">om</seg></w> —<caesura></caesura> <w n="12.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.6" punct="pt:9">c<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="9" met="5+4"><w n="13.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="13.2" punct="tc:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="ti" caesura="1">é</seg></w> —<caesura></caesura> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="13.5" punct="vg:9">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="vg">eu</seg>r</w>,</l>
						<l n="14" num="4.2" lm="9" met="5+4"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="14.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="14.3" punct="tc:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="ti" caesura="1">i</seg></w> —<caesura></caesura> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="14.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="14.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="14.7" punct="vg:9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="15" num="4.3" lm="9" met="5+4"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="15.4" punct="tc:5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg ti" caesura="1">oi</seg>s</w>,<caesura></caesura> — <w n="15.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" mp="C">e</seg>t</w> <w n="15.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w></l>
						<l n="16" num="4.4" lm="9" met="5+4"><w n="16.1">P<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="16.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="16.4" punct="tc:5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="ti" caesura="1">an</seg>cs</w> —<caesura></caesura> <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="16.7" punct="pt:9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="9" met="5+4"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="17.4" punct="tc:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="ti" caesura="1">ou</seg>rs</w> —<caesura></caesura> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="17.6">fr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w></l>
						<l n="18" num="5.2" lm="9" met="5+4"><w n="18.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="18.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="18.4" punct="tc:5">m<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="ti" caesura="1">eu</seg>rt</w> —<caesura></caesura> <w n="18.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></w>,</l>
						<l n="19" num="5.3" lm="9" met="5+4"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rts</w> <w n="19.4" punct="tc:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="ti" caesura="1">eau</seg>x</w> —<caesura></caesura> <w n="19.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="19.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="19.7" punct="vg:9">g<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>r</w>,</l>
						<l n="20" num="5.4" lm="9" met="5+4"><w n="20.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="20.2">qu</w>’<w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.4" punct="tc:5">r<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="ti" caesura="1">i</seg></w> —<caesura></caesura> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="20.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g</w> <w n="20.7">d</w>’<w n="20.8" punct="pe:9"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">O</seg>rph<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pe" mp="F">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>