<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN312" modus="sm" lm_max="8" metProfile="8">
				<head type="main">A Jules de Prémaray</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">L<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="1.2">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pt</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="1.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5" punct="vg:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">l</w>’<w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="3.5">mi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="3.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>th<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.3">r<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="5.4" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.5" punct="pe:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6">ym</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>th<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="7.4">c</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="7.7" punct="pe:8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</w> !</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x</w> <w n="8.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg>s</w>, <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.5" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="10.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="10.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="10.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>ts</w></l>
					<l n="12" num="2.6" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="13.2" punct="pe:4">P<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="pe">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ! <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.4" punct="pe:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>l</w> !</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>d</w> <w n="14.7">br<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w></l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="15.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="15.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">É</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="6">en</seg>s</w> <w n="15.5" punct="vg:8">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1" punct="vg:1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.6" punct="vg:8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</w>,</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rts</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="17.6">s</w>’<w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w></l>
					<l n="18" num="3.6" lm="8" met="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="18.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="18.5" punct="pt:8">st<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2" punct="vg:3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="19.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="19.5" punct="pe:8">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8" punct="pe">ym</seg></w> !</l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="20.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w></l>
					<l n="21" num="4.3" lm="8" met="8"><w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="21.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.6" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="22.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="2">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>rs</w> !</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="23.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>ls</w> <w n="23.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="23.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="23.7" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</w>,</l>
					<l n="24" num="4.6" lm="8" met="8"><w n="24.1" punct="vg:2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">em</seg>ps</w>, <w n="24.2" punct="vg:4">l<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg>s</w>, <w n="24.3" punct="vg:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</w>, <w n="24.4" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>