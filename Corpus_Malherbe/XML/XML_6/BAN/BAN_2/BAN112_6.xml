<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRIOLETS</head><div type="poem" key="BAN112" modus="sm" lm_max="8" metProfile="8">
					<head type="main">ÉLÈVE DE VOLTAIRE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="1.3">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4" punct="pi:6">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="pi">e</seg></w> ? <w n="1.5">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.6" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</w> ;</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:2">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>s</w>, <w n="2.2" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.6" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="3.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="3.6" punct="dp:8">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="dp">a</seg>s</w> :</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.3">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.4" punct="pi:6">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="pi">e</seg></w> ? <w n="4.5">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2" punct="dp:3">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>t</w> : <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">T</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.4" punct="pi:6">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="pi">e</seg></w> ? <w n="7.5">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:2">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>s</w>, <w n="8.2" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.6" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1859">janvier 1859</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>