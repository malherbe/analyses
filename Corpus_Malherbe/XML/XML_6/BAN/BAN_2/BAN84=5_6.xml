<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" insert="5" modus="xx" key="BAN84">
								<head type="main">CHANSON.</head>
								<sp n="42">
									<speaker>Le Lutin.</speaker>
									<lg n="1">
										<l n="358" n_ins="1" num_ins="1.1" lm="7" met="7"><space quantity="10" unit="char"></space><w n="358.1">C</w>’<w n="358.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="358.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="358.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="358.5">l</w>’<w n="358.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="358.7"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
										<l n="359" n_ins="2" num_ins="1.2" lm="7" met="7"><space quantity="10" unit="char"></space><w n="359.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="359.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="359.3" punct="dp:7">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></w> :</l>
										<l n="360" n_ins="3" num_ins="1.3" lm="7" met="7"><space quantity="10" unit="char"></space><w n="360.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="360.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="360.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="360.4" punct="vg:7">f<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
										<l n="361" n_ins="4" num_ins="1.4" lm="7" met="7"><space quantity="10" unit="char"></space><w n="361.1">C</w>’<w n="361.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="361.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="361.4">qu</w>’<w n="361.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="361.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="361.7" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="pe">o</seg>r</w> !</l>
										<l n="362" n_ins="5" num_ins="1.5" lm="7" met="7"><space quantity="10" unit="char"></space><w n="362.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="2">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="362.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="362.3" punct="vg:7">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
										<l n="363" n_ins="6" num_ins="1.6" lm="7" met="7"><space quantity="10" unit="char"></space><w n="363.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="363.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="363.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="363.4">fr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
										<l n="364" n_ins="7" num_ins="1.7" lm="7" met="7"><space quantity="10" unit="char"></space><w n="364.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="364.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="364.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="364.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
										<l n="365" n_ins="8" num_ins="1.8" lm="7" met="7"><space quantity="10" unit="char"></space><w n="365.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="365.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="365.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="365.4">d</w>’<w n="365.5" punct="pe:7"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="pe">o</seg>r</w> !</l>
									</lg>
								</sp>
								<sp n="43">
									<speaker>Le Comédien Bouffon.</speaker>
									<lg n="1">
										<l n="366" n_ins="9" num_ins="1.1" lm="7" met="7"><space quantity="10" unit="char"></space><w n="366.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="366.2">S<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="366.3" punct="pe:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pe">ou</seg>s</w> ! <w n="366.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="366.5">cu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
										<l n="367" n_ins="10" num_ins="1.2" lm="7" met="7"><space quantity="10" unit="char"></space><w n="367.1">N</w>’<w n="367.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="367.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="367.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="367.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
										<l n="368" n_ins="11" num_ins="1.3" lm="7" met="7"><space quantity="10" unit="char"></space><w n="368.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="368.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="368.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="368.4">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="368.5" punct="pe:7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
										<l n="369" n_ins="12" num_ins="1.4" lm="7" met="7"><space quantity="10" unit="char"></space><w n="369.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="369.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="369.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="369.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="369.5" punct="pv:7">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pv">an</seg>ts</w> ;</l>
										<l n="370" n_ins="13" num_ins="1.5" lm="7" met="7"><space quantity="10" unit="char"></space><w n="370.1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="370.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="370.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="370.4" punct="vg:7">c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
										<l n="371" n_ins="14" num_ins="1.6" lm="7" met="7"><space quantity="10" unit="char"></space><w n="371.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="371.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="371.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="371.4" punct="vg:7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
										<l n="372" n_ins="15" num_ins="1.7" lm="7" met="7"><space quantity="10" unit="char"></space><w n="372.1">R<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="372.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="372.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="372.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="372.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="372.6" punct="vg:7">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
										<l n="373" n_ins="16" num_ins="1.8" lm="7" met="7"><space quantity="10" unit="char"></space><w n="373.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="373.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="373.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="373.4">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="373.5" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="pe">an</seg>s</w> !</l>
									</lg>
									<stage>Tous les personnages et funambules forment des groupes autour desquels court une danse ivre de joie. La farce est jouée. </stage>
								</sp>
							</div></body></text></TEI>