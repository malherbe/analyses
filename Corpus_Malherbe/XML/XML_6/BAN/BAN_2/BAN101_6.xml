<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDEAUX</head><div type="poem" key="BAN101" modus="cp" lm_max="10" metProfile="1, 4+6">
					<head type="main">ADIEU, PANIERS</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">d</w>’<w n="1.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="1.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fm">e</seg></w>-<w n="1.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="1.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="8">o</seg>p</w> <w n="1.7" punct="vg:10">pr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="2.4">n</w>’<w n="2.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="2.6">qu</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="2.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.9" punct="vg:10">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="4" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="3.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="3.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="3.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg></w>.</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="4.2">v<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="4.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="4.4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="4.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="4.9">f<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="5.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="5.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" caesura="1">oin</seg></w><caesura></caesura> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.8" punct="pt:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="10" met="4+6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2" punct="vg:2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2" punct="vg">ain</seg></w>, <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="6.4" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="493" place="4" punct="vg" caesura="1">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="6.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="6.9" punct="pv:10">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="7" num="2.2" lm="10" met="4+6"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="7.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.5">f<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="8" num="2.3" lm="10" met="4+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">s</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.6">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="8.9">j<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w></l>
						<l n="9" num="2.4" lm="1" met="1"><space quantity="18" unit="char"></space><w n="9.1">L</w>’<w n="9.2" punct="pt:1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1" lm="10" met="4+6"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="10.4" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="10.5" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</w> ! <w n="10.6">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.7" punct="pt:10">c<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="11" num="3.2" lm="10" met="4+6"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3" mp="M">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="12" num="3.3" lm="10" met="4+6"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>scr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="12.6">l</w>’<w n="12.7" punct="pt:10">h<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg></w>.</l>
						<l n="13" num="3.4" lm="10" met="4+6"><w n="13.1" punct="vg:2">C<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="13.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="vg" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="13.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="13.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="13.8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w></l>
						<l n="14" num="3.5" lm="10" met="4+6"><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" caesura="1">ê</seg>ts</w><caesura></caesura> <w n="14.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="14.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="15" num="3.6" lm="1" met="1"><space quantity="18" unit="char"></space><w n="15.1" punct="pt:1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2" punct="pt">e</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1845">novembre 1845</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>