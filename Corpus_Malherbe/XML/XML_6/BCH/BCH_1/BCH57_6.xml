<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH57" modus="cm" lm_max="12" metProfile="6+6">
					<head type="number">VI</head>
					<head type="main">LE SOUVENIR</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="1.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.8" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.7">j<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">î</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>ts</w> <w n="3.4">s</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.7">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">F<seg phoneme="œ" type="vs" value="1" rule="304" place="1" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="4.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="4.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.7" punct="pt:12">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="5.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="5.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="5.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.9" punct="pv:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pv">e</seg>r</w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.6">n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="6.9" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:1">L<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>rds</w>, <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="7.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="8.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="9.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="9.5" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="354" place="10" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt</w> <w n="10.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.7">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.8"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w> <w n="10.9" punct="vg:12"><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Fr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="11.9">fl<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>ts</w> <w n="11.10" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pt">e</seg>rts</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="12.7">m<seg phoneme="u" type="vs" value="1" rule="428" place="9" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="12.9" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="13.6">c<seg phoneme="œ" type="vs" value="1" rule="345" place="7" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="13.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg>s</w> <w n="13.9" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="14.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="14.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="14.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="14.8" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>