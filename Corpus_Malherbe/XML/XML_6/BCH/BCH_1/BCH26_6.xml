<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH26" modus="sm" lm_max="8" metProfile="8">
					<head type="number">XXV</head>
					<head type="main">TES CHEVEUX</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.3" punct="vg:5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.7">v<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5">s<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>d<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>t</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.5" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="4.4">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6" punct="vg:8">n<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="5.5">n<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="6.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.4">d<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="6.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="6">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="8" met="8"><w n="7.1">N</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="7.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.7" punct="pv:8">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="8" num="2.2" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="8.4">t</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.8" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="9" num="2.3" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">s</w>’<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.5" punct="vg:4">n<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="133" place="5">e</seg>h</w> <w n="9.7" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" punct="vg">en</seg></w>, <w n="9.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="9.9" punct="pe:8">p<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>s</w> !</l>
						<l n="10" num="2.4" lm="8" met="8"><w n="10.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-27" place="7">e</seg></w> <w n="10.5">h<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="11" num="2.5" lm="8" met="8"><w n="11.1">Qu</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.3">s</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="12" num="2.6" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">ru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="12.5" punct="pt:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.2" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="13.3">st<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="14" num="3.2" lm="8" met="8"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.4" punct="dp:8">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="15" num="3.3" lm="8" met="8"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>s</w>-<w n="15.4" punct="vg:3">j<seg phoneme="ə" type="ee" value="0" rule="e-15">e</seg></w>, <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>rs</w>,</l>
						<l n="16" num="3.4" lm="8" met="8"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="16.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="17" num="3.5" lm="8" met="8"><w n="17.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rb<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="18" num="3.6" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.6" punct="pe:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="19.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6" punct="vg:8">j<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="20" num="4.2" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="21" num="4.3" lm="8" met="8"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="21.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="4">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="21.5" punct="pe:8">gr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg>s</w> !</l>
						<l n="22" num="4.4" lm="8" met="8"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="22.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="22.4" punct="vg:3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg>s</w>, <w n="22.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="22.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="22.7" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="23" num="4.5" lm="8" met="8"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="23.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.6">l</w>’<w n="23.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="24" num="4.6" lm="8" met="8"><w n="24.1">F<seg phoneme="ɥi" type="vs" value="1" rule="462" place="1">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="24.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="24.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.5" punct="pe:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg>s</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1" lm="8" met="8"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>gts</w> <w n="25.4">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="25.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="25.6" punct="vg:8">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="26" num="5.2" lm="8" met="8"><w n="26.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="26.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="26.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="26.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ts</w> <w n="26.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="26.7" punct="pe:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="27" num="5.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2" punct="vg:2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4">d</w>’<w n="27.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>, <w n="27.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="27.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
						<l n="28" num="5.4" lm="8" met="8"><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="28.5">s</w>’<w n="28.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="29" num="5.5" lm="8" met="8"><w n="29.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="29.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="29.3">m</w>’<w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="29.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="29.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="29.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="30" num="5.6" lm="8" met="8"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="30.6" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>