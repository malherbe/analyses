<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH92" modus="cm" lm_max="10" metProfile="5+5">
					<head type="number">XXXVIII</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.9">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M/mp">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="Lp">em</seg>ps</w>-<w n="2.7" punct="pv:10">c<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pv">i</seg></w> ;</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="4.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" caesura="1">em</seg>ps</w><caesura></caesura> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="4.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="7" mp="M">œ</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</w> <w n="4.7" punct="pt:10"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4" punct="vg:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="5.6">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="5.8" punct="vg:10">m<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>s</w><caesura></caesura> <w n="6.6" punct="vg:7">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>r</w>, <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="6.8">c<seg phoneme="œ" type="vs" value="1" rule="345" place="9" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w></l>
						<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="7.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>r</w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.8" punct="pv:10">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="8.8" punct="pt:10">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="9" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="9.2">j<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="9.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>x</w><caesura></caesura> <w n="9.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2" punct="vg:2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg>s</w>, <w n="10.3">l</w>’<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="10.5" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="10.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9" mp="M">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="11.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="11.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="11.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="11.8" punct="vg:10">f<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1" punct="pe:1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>s</w> ! <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="12.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="12.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w> <w n="12.7">l</w>’<w n="12.8" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9" mp="M">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="Lp">ai</seg>s</w>-<w n="13.5" punct="pi:5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="pi" caesura="1">u</seg></w> ?<caesura></caesura> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="13.9" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="14.3">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="14.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" caesura="1">e</seg>il</w><caesura></caesura> <w n="14.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="14.6">d</w>’<w n="14.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.8" punct="pv:10">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="pv">ai</seg>s</w> ;</l>
						<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="15.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="15.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="15.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="15.9">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
						<l n="16" num="4.4" lm="10" met="5+5"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="16.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt</w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="16.7" punct="pt:10">j<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="pt">ai</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>