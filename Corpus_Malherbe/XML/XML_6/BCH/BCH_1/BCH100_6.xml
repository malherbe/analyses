<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH100" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="number">II</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="1.4" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12">o</seg>ts</w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" mp="P">e</seg>rs</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>gs</w> <w n="2.9" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="3.4" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.7" punct="pv:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>ts</w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="4.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="4.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.8" punct="vg:10">fr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.10">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12">o</seg>ts</w></l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="6.3">cr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="6.8" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</w>,</l>
						<l n="7" num="2.2" lm="12" met="6+6"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.4">ju<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.7">fr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.10" punct="pv:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="8" num="2.3" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="8.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="8.7" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</w>,</l>
						<l n="9" num="2.4" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">cr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>il</w></l>
						<l n="10" num="2.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="2">I</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.7" punct="pt:8">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="Lp">on</seg>t</w>-<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="11.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="11.7" punct="pi:12">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pi">o</seg>ts</w> ?</l>
						<l n="12" num="3.2" lm="12" met="6+6"><w n="12.1" punct="pe:1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1" punct="pe">en</seg></w> ! <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.9" punct="pv:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="13" num="3.3" lm="12" met="6+6"><w n="13.1" punct="vg:3">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="M">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="13.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>r</w> <w n="13.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t</w><caesura></caesura> <w n="13.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="13.7" punct="pt:12">D<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>s</w>.</l>
						<l n="14" num="3.4" lm="12" met="6+6"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">Î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="14.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="14.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.8" punct="vg:9">tr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="14.9">c</w>’<w n="14.10" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10" punct="vg">e</seg>st</w>, <w n="14.11"><seg phoneme="o" type="vs" value="1" rule="415" place="11">ô</seg></w> <w n="14.12" punct="pe:12">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pe">o</seg>ts</w> !</l>
						<l n="15" num="3.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">L</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="15.5" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="8">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1" lm="12" met="6+6"><w n="16.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="Fm">e</seg>s</w>-<w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w> <w n="16.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="16.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="16.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w> <w n="16.7">d</w>’<w n="16.8"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="17" num="4.2" lm="12" met="6+6"><w n="17.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="17.2">t<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>q<seg phoneme="wa" type="vs" value="1" rule="256" place="5" mp="M">ua</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="17.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.7">l</w>’<w n="17.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="18" num="4.3" lm="12" met="6+6"><w n="18.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M/mp">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w> <w n="18.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="18.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="18.8">l</w>’<w n="18.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="19" num="4.4" lm="12" met="6+6"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="19.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="19.6" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="19.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="19.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="19.9">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w> <w n="19.10">d</w>’<w n="19.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="20" num="4.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.5" punct="pt:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>