<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF58" modus="cp" lm_max="12" metProfile="8, (4+6), 6+6">
					<head type="main">IMITÉ DE L’ANTHOLOGIE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6" met_alone="True"><space unit="char" quantity="4"></space><w n="1.1">V<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="1.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.7" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="dp">an</seg>t</w> :</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.5">cu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="3.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="6" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.9" punct="pt:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg>t</w>, <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.4" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="5.2" punct="vg:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="5.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l</w> <w n="5.6" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="5.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.9" punct="pt:12">m<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w>-<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="6.5" punct="vg:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.4" punct="vg:8">t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="8.2">D<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.5">bl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.7" punct="pe:12">s<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pe">œu</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>