<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF14" modus="cm" lm_max="10" metProfile="4+6">
					<head type="main">L’AVARE ÉBORGNÉ</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="1.2" punct="vg:4">H<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rp<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="1.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="1.6" punct="vg:10">h<seg phoneme="i" type="vs" value="1" rule="493" place="7" mp="M">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="2.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.5">m<seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6" punct="pt:10">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>st<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6">« <w n="3.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.6">gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M/mp">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="Lp">ai</seg>t</w>-<w n="4.3" punct="pv:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pv" caesura="1">on</seg></w> ;<caesura></caesura> <w n="4.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="4.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="Lc">e</seg>lqu</w>’<w n="4.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M/mc">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9" mp="M/mc">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="5.2" punct="pt:4">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>j<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="4" punct="pt ti" caesura="1">ean</seg></w>.<caesura></caesura> — <w n="5.3" punct="vg:5">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>, <w n="5.4" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rbl<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg></w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="5.7" punct="vg:10">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="6.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="6.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="6.8" punct="pv:10">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" punct="pv">e</seg>r</w> ;</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="vg:4">gu<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="7.8" punct="pt:10">fr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" punct="pt">e</seg>r</w>. »</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">fr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="8.3" punct="vg:4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="8.6" punct="vg:10">c<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2" punct="vg:4">b<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>st<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="9.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w></l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="10.2">cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">l</w>’<w n="10.4" punct="vg:4"><seg phoneme="œ" type="vs" value="1" rule="286" place="4" punct="vg" caesura="1">œ</seg>il</w>,<caesura></caesura> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="10.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" mp="Lc">è</seg>s</w>-<w n="10.7" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M/mc">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M/mc">ai</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem/mc">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="pt">en</seg>t</w>.</l>
						<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rp<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.2" punct="pv:4">cr<seg phoneme="i" type="vs" value="1" rule="469" place="4" punct="pv" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ;<caesura></caesura> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">E</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="12.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="12.7">l</w>’<w n="12.8" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
						<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1" mp="Lc">è</seg>s</w>-<w n="13.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M/mc">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M/mc">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="13.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>lg<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rt</w><caesura></caesura> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="14.5">cl<seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="14.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="14.7" punct="pt:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="15" num="1.15" lm="10" met="4+6">« <w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="15.2" punct="pe:2"><seg phoneme="œ" type="vs" value="1" rule="286" place="2" punct="pe">œ</seg>il</w> ! <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="15.4" punct="pe:4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="pe" caesura="1">e</seg>l</w> !<caesura></caesura> <w n="15.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> ! <w n="15.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="15.7" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" mp="M">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></w> !</l>
						<l n="16" num="1.16" lm="10" met="4+6"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="16.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="16.4" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="16.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.6"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="16.7" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="17" num="1.17" lm="10" met="4+6"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M/mp">ou</seg>rv<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M/mp">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="17.4" punct="pv:10">r<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg></w> ;</l>
						<l n="18" num="1.18" lm="10" met="4+6"><w n="18.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="18.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="18.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>s</w><caesura></caesura> <w n="18.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="18.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="18.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="18.7" punct="vg:10">j<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="19" num="1.19" lm="10" met="4+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="19.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="19.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="19.6" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg></w>. »</l>
						<l n="20" num="1.20" lm="10" met="4+6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg" caesura="1">o</seg>rs</w>,<caesura></caesura> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="20.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="20.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="20.8" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>b<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="21" num="1.21" lm="10" met="4+6">« <w n="21.1" punct="vg:2">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="21.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="Lp">i</seg>t</w>-<w n="21.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>l</w>,<caesura></caesura> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="21.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="21.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-27" place="9" mp="F">e</seg></w> <w n="21.7" punct="pv:10">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="22" num="1.22" lm="10" met="4+6"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="22.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="22.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" mp="Lc">è</seg>s</w>-<w n="22.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="22.5">qu</w>’<w n="22.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="22.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="22.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="22.9" punct="pv:10">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></w> ;</l>
						<l n="23" num="1.23" lm="10" met="4+6"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="23.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">oû</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="23.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.8" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></w> :</l>
						<l n="24" num="1.24" lm="10" met="4+6"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="a" type="vs" value="1" rule="307" place="3" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="24.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="24.6">n</w>’<w n="24.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="24.9" punct="pt:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></w>. »</l>
					</lg>
				</div></body></text></TEI>