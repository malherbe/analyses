<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER60" modus="cp" lm_max="9" metProfile="8, 9ir, (7), (9)">
				<head type="main">LA DOUBLE CHASSE</head>
				<head type="tune">AIR : Tonton, tontaine, tonton</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="1.2" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="1.5" punct="pv:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="2.3">n</w>’<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ds</w>-<w n="2.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.8" punct="pi:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pi">on</seg></w> ?</l>
					<l n="3" num="1.3" lm="9"><w n="3.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="3.2" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="3.3" punct="vg:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="3.4" punct="pt:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt">on</seg></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="4.1" punct="vg:1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>rs</w>, <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="2"></space><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="5.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="6" num="1.6" lm="7"><space unit="char" quantity="4"></space><w n="6.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="6.2" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="6.3" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="7.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.3" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gn<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="8.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pt:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="9" num="2.3" lm="9"><w n="9.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="9.2" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="9.3" punct="vg:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="9.4" punct="pt:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt">on</seg></w>.</l>
					<l n="10" num="2.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">j<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="11" num="2.5" lm="8" met="8"><space unit="char" quantity="2"></space><w n="11.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w>-<w n="11.5" punct="pe:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg></w> !</l>
					<l n="12" num="2.6" lm="7"><space unit="char" quantity="4"></space><w n="12.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="12.2" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="12.3" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rf</w> <w n="13.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="13.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.6">l</w>’<w n="13.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="3.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="14.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5" punct="pt:8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="15" num="3.3" lm="9"><w n="15.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="15.2" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="15.3" punct="vg:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="15.4" punct="pt:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt">on</seg></w>.</l>
					<l n="16" num="3.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.4" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="16.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="16.6" punct="vg:8">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="17" num="3.5" lm="8" met="8"><space unit="char" quantity="2"></space><w n="17.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">gl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="17.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="17.5">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c</w> <w n="17.6" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="18" num="3.6" lm="7"><space unit="char" quantity="4"></space><w n="18.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="18.2" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="18.3" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="19.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="19.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="19.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="19.4">m<seg phoneme="ø" type="vs" value="1" rule="399" place="5">eu</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.5" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="20" num="4.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3" punct="pv:4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="20.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="20.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="20.6" punct="dp:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="dp">on</seg>d</w> :</l>
					<l n="21" num="4.3" lm="9"><w n="21.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="21.2" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="21.3" punct="vg:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="21.4" punct="pt:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt">on</seg></w>.</l>
					<l n="22" num="4.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="22.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="22.2" punct="vg:2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2" punct="vg">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="22.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="22.6" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="23" num="4.5" lm="8" met="8"><space unit="char" quantity="2"></space><w n="23.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rts</w> <w n="23.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="23.5" punct="pt:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="24" num="4.6" lm="7"><space unit="char" quantity="4"></space><w n="24.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="24.2" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="24.3" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="25.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="25.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="25.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="25.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p</w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="25.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="26" num="5.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="26.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="26.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rf</w> <w n="26.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="26.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.7" punct="pt:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="27" num="5.3" lm="9"><w n="27.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="27.2" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="27.3" punct="vg:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="27.4" punct="pt:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt">on</seg></w>.</l>
					<l n="28" num="5.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="28.1">L</w>’<w n="28.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="28.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="28.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="28.5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="28.6">qu</w>’<w n="28.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="28.8" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="29" num="5.5" lm="8" met="8"><space unit="char" quantity="2"></space><w n="29.1"><seg phoneme="y" type="vs" value="1" rule="450" place="1">U</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="29.6" punct="pt:8">f<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="30" num="5.6" lm="7"><space unit="char" quantity="4"></space><w n="30.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="30.2" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="30.3" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1" lm="8" met="8"><space unit="char" quantity="2"></space><w n="31.1" punct="vg:2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="31.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="31.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="31.5" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="32" num="6.2" lm="8" met="8"><space unit="char" quantity="2"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="32.4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="32.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="32.7" punct="pt:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="33" num="6.3" lm="9"><w n="33.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="33.2" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="33.3" punct="vg:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg" mp="F">e</seg></w>, <w n="33.4" punct="pt:9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt">on</seg></w>.</l>
					<l n="34" num="6.4" lm="8" met="8"><space unit="char" quantity="2"></space><w n="34.1">L</w>’<w n="34.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="34.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="34.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="34.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="35" num="6.5" lm="8" met="8"><space unit="char" quantity="2"></space><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rf</w> <w n="35.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="35.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="35.7" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					<l n="36" num="6.6" lm="7"><space unit="char" quantity="4"></space><w n="36.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="36.2" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="36.3" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
				</lg>
			</div></body></text></TEI>