<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER55" modus="sp" lm_max="7" metProfile="7, (3), (5)">
				<head type="main">BON VIN ET FILLETTE</head>
				<head type="tune">AIR : Ma tante Urlurette</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">L</w>’<w n="1.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="1.3">l</w>’<w n="1.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>, <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6" punct="vg:7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4" punct="pv:7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pv">in</seg></w> ;</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">N<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="pe:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					<l n="4" num="1.4" lm="3"><space unit="char" quantity="8"></space><w n="4.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="5" num="1.5" lm="3"><space unit="char" quantity="8"></space><w n="5.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="5"><space unit="char" quantity="4"></space><w n="6.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.4" punct="pe:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.6" punct="dp:7">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="dp">on</seg></w> :</l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
					<l n="9" num="2.3" lm="7" met="7"><w n="9.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="9.5" punct="pt:7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rvi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="10" num="2.4" lm="3"><space unit="char" quantity="8"></space><w n="10.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="11" num="2.5" lm="3"><space unit="char" quantity="8"></space><w n="11.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="12" num="2.6" lm="5"><space unit="char" quantity="4"></space><w n="12.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.4" punct="pe:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="13.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="13.7" punct="vg:7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>ds</w>,</l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w></l>
					<l n="15" num="3.3" lm="7" met="7"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="15.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="15.4" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="16" num="3.4" lm="3"><space unit="char" quantity="8"></space><w n="16.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="17" num="3.5" lm="3"><space unit="char" quantity="8"></space><w n="17.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="18" num="3.6" lm="5"><space unit="char" quantity="4"></space><w n="18.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="18.4" punct="pe:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="19.3">tr<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w>-<w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.6" punct="pi:7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pi">eu</seg>x</w> ?</l>
					<l n="20" num="4.2" lm="7" met="7"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="20.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="20.4">s</w>’<w n="20.5"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="20.6">pl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="20.7" punct="dp:7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="dp">eu</seg>x</w> :</l>
					<l n="21" num="4.3" lm="7" met="7"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="21.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="21.5" punct="pe:7">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					<l n="22" num="4.4" lm="3"><space unit="char" quantity="8"></space><w n="22.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="23" num="4.5" lm="3"><space unit="char" quantity="8"></space><w n="23.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="24" num="4.6" lm="5"><space unit="char" quantity="4"></space><w n="24.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="24.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="24.4" punct="pe:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1" lm="7" met="7"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="25.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="25.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="25.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="25.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w></l>
					<l n="26" num="5.2" lm="7" met="7"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="26.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="26.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="26.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="26.6" punct="vg:7">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>t</w>,</l>
					<l n="27" num="5.3" lm="7" met="7"><w n="27.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="27.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="27.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="27.5" punct="pt:7">t<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="28" num="5.4" lm="3"><space unit="char" quantity="8"></space><w n="28.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="29" num="5.5" lm="3"><space unit="char" quantity="8"></space><w n="29.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="30" num="5.6" lm="5"><space unit="char" quantity="4"></space><w n="30.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="30.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="30.4" punct="pe:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1" lm="7" met="7"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="31.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="31.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pi">i</seg>s</w>-<w n="31.4" punct="pi:3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> ? <w n="31.5" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="31.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="31.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.8" punct="vg:7">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>s</w>,</l>
					<l n="32" num="6.2" lm="7" met="7"><w n="32.1">M<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="32.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t</w> <w n="32.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="32.4" punct="pv:7">b<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pv">a</seg>s</w> ;</l>
					<l n="33" num="6.3" lm="7" met="7"><w n="33.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="33.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="33.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="33.5" punct="pt:7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="34" num="6.4" lm="3"><space unit="char" quantity="8"></space><w n="34.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="35" num="6.5" lm="3"><space unit="char" quantity="8"></space><w n="35.1" punct="vg:3">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
					<l n="36" num="6.6" lm="5"><space unit="char" quantity="4"></space><w n="36.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="36.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="36.4" punct="pe:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>