<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER116" modus="cm" lm_max="12">
				<head type="number">CXVI</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.6">ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg></w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.7">h<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="2.10" punct="pv:12">bu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg>s</w> ;</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="3.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="3.3">l</w>’<w n="3.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="3.8">l<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="3.9" punct="vg:12">d<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="4.3" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="4.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="4.5">d</w>’<w n="4.6" punct="vg:6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg>x</w>, <w n="4.7" punct="vg:9">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="4.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="4.9" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="5.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="5.5">z<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>th</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="5.8">r<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="5.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">cu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12" punct="vg">aim</seg>s</w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="7.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.4" lm="12"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="8.3">j<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">gl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="8.10" punct="pt:12">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12" punct="pt">ein</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>