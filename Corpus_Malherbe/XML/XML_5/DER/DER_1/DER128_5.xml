<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER128" modus="sm" lm_max="8">
				<head type="number">CXXVIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="2.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6" punct="pt:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1" lm="8"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w></l>
					<l n="4" num="2.2" lm="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:4">P<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="4.3">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="5">ean</seg></w> <w n="4.4" punct="vg:8">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1" lm="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>r</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="6" num="3.2" lm="8"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="6.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="6.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1" lm="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="7.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w></l>
					<l n="8" num="4.2" lm="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.5" punct="pv:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg>s</w> ;</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1" lm="8"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6">b<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="10" num="5.2" lm="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">l<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1" lm="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">n<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
					<l n="12" num="6.2" lm="8"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</w></l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1" lm="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="13.4" punct="vg:4">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="493" place="4" punct="vg">y</seg></w>, <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="7.2" lm="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ff<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="14.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1" lm="8"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.8" punct="pt:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</w>.</l>
					<l n="16" num="8.2" lm="8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="16.2">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="16.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.6" punct="pt:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</w>.</l>
				</lg>
				<lg n="9">
					<l n="17" num="9.1" lm="8"><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>t</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="18" num="9.2" lm="8"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="18.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="18.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="18.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="354" place="6">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="10">
					<l n="19" num="10.1" lm="8"><w n="19.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="19.2" punct="pe:4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pe">u</seg>s</w> ! <w n="19.3" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">A</seg>h</w> ! <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="20" num="10.2" lm="8"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="20.4">j</w>’<w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="20.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="20.8" punct="pv:8">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</w> ;</l>
				</lg>
				<lg n="11">
					<l n="21" num="11.1" lm="8"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2" punct="vg:4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="21.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="22" num="11.2" lm="8"><w n="22.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="22.3">p<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="12">
					<l n="23" num="12.1" lm="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w></l>
					<l n="24" num="12.2" lm="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="24.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.6" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>ts</w>.</l>
				</lg>
				<lg n="13">
					<l n="25" num="13.1" lm="8"><w n="25.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="25.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="25.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="25.6" punct="pe:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
					<l n="26" num="13.2" lm="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="26.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="26.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="26.6" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
				</lg>
				<lg n="14">
					<l n="27" num="14.1" lm="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="27.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="27.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="27.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w></l>
					<l n="28" num="14.2" lm="8"><w n="28.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="28.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>g<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w></l>
				</lg>
				<lg n="15">
					<l n="29" num="15.1" lm="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="29.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="29.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="30" num="15.2" lm="8"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="30.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="30.5" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="16">
					<l n="31" num="16.1" lm="8"><w n="31.1">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="1">ean</seg></w> <w n="31.2" punct="vg:4">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="31.3">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="5">ean</seg></w> <w n="31.4" punct="vg:8">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
					<l n="32" num="16.2" lm="8"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.3">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="32.4">j</w>’<w n="32.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="32.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="32.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="32.8" punct="vg:8">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></w>,</l>
				</lg>
				<lg n="17">
					<l n="33" num="17.1" lm="8"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="33.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="33.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="34" num="17.2" lm="8"><w n="34.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="34.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="34.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="34.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="34.6" punct="pt:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="18">
					<l n="35" num="18.1" lm="8"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="35.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="35.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="35.6" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>,</l>
					<l n="36" num="18.2" lm="8"><w n="36.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="36.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="36.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.4">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="36.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="36.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="36.7" punct="vg:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>,</l>
				</lg>
				<lg n="19">
					<l n="37" num="19.1" lm="8"><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="37.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="37.3" punct="vg:4">g<seg phoneme="u" type="vs" value="1" rule="425" place="3">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="37.4">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="38" num="19.2" lm="8"><w n="38.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="38.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="38.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="38.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="38.5" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>