<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO32" modus="cm" lm_max="10">
					<head type="main">AUX CÉLIBATAIRES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="1.2" punct="pe:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe">ou</seg>t</w> ! <w n="1.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.4" punct="vg:10">c<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="10"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>br<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.7" punct="pe:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg>s</w> !</l>
						<l n="3" num="1.3" lm="10"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="3.3">g<seg phoneme="i" type="vs" value="1" rule="468" place="6">î</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="3.4" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ts</w> <w n="4.9" punct="pe:10">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="5.4">b<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="5.6" punct="dp:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
						<l n="6" num="2.2" lm="10"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">s<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="6.5" punct="pv:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pv">eu</seg>x</w> ;</l>
						<l n="7" num="2.3" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="7.4" punct="vg:7">j<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="7.5" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="10"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="8.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="8.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="8.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="8.9" punct="pe:10">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg>x</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7" punct="vg:10"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="10"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">È</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bs<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pv">eu</seg>x</w> ;</l>
						<l n="11" num="3.3" lm="10"><w n="11.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="11.5">l</w>’<w n="11.6" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.4" lm="10"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd</w> <w n="12.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="12.7" punct="pt:10">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="13.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="13.7" punct="pv:10">t<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="14" num="4.2" lm="10"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>gs</w> <w n="14.7" punct="pv:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pv">eu</seg>x</w> ;</l>
						<l n="15" num="4.3" lm="10"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="15.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="16" num="4.4" lm="10"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="16.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="16.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="16.8" punct="pe:10">v<seg phoneme="ø" type="vs" value="1" rule="248" place="10" punct="pe">œu</seg>x</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10"><w n="17.1" punct="vg:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="17.2" punct="pe:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe">ou</seg>t</w> ! <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="17.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c</w> <w n="17.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="18" num="5.2" lm="10"><w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="18.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="3">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="18.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="18.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="18.8" punct="pe:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>rs</w> !</l>
						<l n="19" num="5.3" lm="10"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w>-<w n="19.3" punct="dp:4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="dp in">i</seg></w> : « <w n="19.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.6" punct="vg:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="10"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="20.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="20.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="20.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>rs</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="10">« <w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="21.3" punct="vg:4">cr<seg phoneme="y" type="vs" value="1" rule="454" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="21.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="21.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="21.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="22" num="6.2" lm="10"><w n="22.1">D</w>’<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="22.3">bl<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="22.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="22.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="22.6" punct="pv:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="pv">en</seg>ts</w> ;</l>
						<l n="23" num="6.3" lm="10"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="23.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w>-<w n="23.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.6" punct="pi:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi">e</seg></w> ?</l>
						<l n="24" num="6.4" lm="10"><w n="24.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="24.2" punct="pv:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pv">on</seg></w> ; <w n="24.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rs</w>, <w n="24.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="24.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="24.6" punct="pt:10">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="pt">en</seg>ts</w>. »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="10"><w n="25.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="25.2" punct="pe:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe">ou</seg>s</w> ! <w n="25.3">l</w>’<w n="25.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">É</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="25.5">l</w>’<w n="25.6" punct="pv:10"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="26" num="7.2" lm="10"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="26.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="26.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="26.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="26.8" punct="vg:10">chr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10" punct="vg">en</seg></w>,</l>
						<l n="27" num="7.3" lm="10"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="27.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="27.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.6">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="27.7">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="28" num="7.4" lm="10"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>x</w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="28.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="28.9" punct="pt:10">qu<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>di<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10" punct="pt">en</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="10"><w n="29.1">C</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.4" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="29.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="29.6" punct="vg:10">c<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="30" num="8.2" lm="10"><w n="30.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="30.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="30.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="30.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="30.9">l</w>’<w n="30.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg></w></l>
						<l n="31" num="8.3" lm="10"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="31.2">l</w>’<w n="31.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="31.4">s<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="31.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>s</w> <w n="31.6" punct="pv:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg>s</w> ;</l>
						<l n="32" num="8.4" lm="10"><w n="32.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="32.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="32.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>ts</w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="32.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="32.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="32.7" punct="pe:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>ts</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1883">1er janvier 1883</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>