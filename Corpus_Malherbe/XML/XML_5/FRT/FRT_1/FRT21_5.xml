<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Vers pour <lb></lb>LES SERVANTES</head><div type="poem" key="FRT21" modus="cm" lm_max="12">
					<head type="main">Marteau de porte</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">d<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="1.3" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="1.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6" punct="vg:9">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.8">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg></w> <w n="1.9" punct="vg:12">r<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="2.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="2.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>x</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="3.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">cr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="3.9" punct="vg:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:1">Fr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.3">l</w>’<w n="4.4" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>s</w>, <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="4.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="4.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="10">en</seg>s</w> <w n="4.11" punct="pt:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.9" punct="pt:12">gu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">s</w>’<w n="6.3" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="6.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="6.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.8" punct="vg:12">cu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="8.5">qu</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.8">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>gts</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.10">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.11"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="8.12">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="8.13" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="1">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.5">n</w>’<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="9.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="9.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="9.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="9.10" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>t</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="10.4" punct="vg:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.6" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>sf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>t</w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="11.9" punct="vg:12">b<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ltr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="pv:3">ch<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pv">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ; <w n="13.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="189" place="4" punct="vg">e</seg>t</w>, <w n="13.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="13.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="13.11" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="1">en</seg>s</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="14.8" punct="pt:12">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>