<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP128" modus="cm" lm_max="12">
					<head type="main">ADIEUX AUX EAUX-BONNES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="1.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.8" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="2.8">d</w>’<w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="2.10" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>ts</w> <w n="3.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lti<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg>s</w>, <w n="3.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.4" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>rs</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6" punct="vg:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>, <w n="3.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="3.8">p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c</w> <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.10" punct="vg:12">G<seg phoneme="ɛ" type="vs" value="1" rule="COP128_1" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.8" punct="pt:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.6">m</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>dm<seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="5.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="5.9">d</w>’<w n="5.10" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd</w> <w n="6.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="6.8" punct="vg:12">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="7.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="7.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="7.9" punct="vg:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="2">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">S<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d</w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="8.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.8">l</w>’<w n="8.9" punct="pt:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.8" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="10.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="10.8">l</w>’<w n="10.9" punct="pv:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></w> ;</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="11.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="11.8">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="11.9" punct="pe:12">ch<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">N<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.7">d</w>’<w n="12.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1">N</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="13.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="13.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="13.11"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="14.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="14.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.8" punct="pi:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pi">oi</seg>r</w> ?</l>
					</lg>
					<closer>
						<dateline>
							<date when="1878">Septembre 1878.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>