<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP101" modus="sm" lm_max="8">
					<head type="main">HYMNE À LA PAIX</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>x</w> <w n="1.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="4">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="2.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.6" punct="pv:8">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg>s</w> ;</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="3.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.6" punct="vg:8">j<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="4.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ds</w> <w n="4.7" punct="pt:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
						<l n="5" num="1.5" lm="8"><w n="5.1">H<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="1.6" lm="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="6.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7" punct="pv:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg>x</w> ;</l>
						<l n="7" num="1.7" lm="8"><w n="7.1" punct="vg:2">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>t</w>, <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="7.3" punct="pe:4">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="4" punct="pe">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="7.5" punct="pe:6">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="pe">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="7.7">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="1.8" lm="8"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="8.3" punct="vg:4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="4" punct="vg">a</seg>il</w>, <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="8.6" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pe">o</seg>s</w> !</l>
						<l n="9" num="1.9" lm="8"><w n="9.1" punct="vg:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="vg">en</seg>s</w>, <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3">t</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6">en</seg>s</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.8" punct="pv:8">m<seg phoneme="ø" type="vs" value="1" rule="402" place="8">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="10" num="1.10" lm="8"><w n="10.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="10.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.5">l</w>’<w n="10.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>r</w> ;</l>
						<l n="11" num="1.11" lm="8"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg>ï<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="12" num="1.12" lm="8"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="12.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6" punct="pt:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</w>.</l>
						<l n="13" num="1.13" lm="8"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="13.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="1.14" lm="8"><w n="14.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="2">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>x</w>, <w n="14.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="14.4">t</w>’<w n="14.5" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg>s</w> ;</l>
						<l n="15" num="1.15" lm="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3">r<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>thm<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="15.4">l</w>’<w n="15.5">h<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="1.16" lm="8"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5" punct="pt:8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
						<l n="17" num="1.17" lm="8"><w n="17.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rs</w>, <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="17.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="17.6" punct="pe:8">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
						<l n="18" num="1.18" lm="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ts</w> <w n="18.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="18.5">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w></l>
						<l n="19" num="1.19" lm="8"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="19.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.7" punct="vg:8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="1.20" lm="8"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="20.7" punct="pt:8">n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>ds</w>.</l>
						<l n="21" num="1.21" lm="8"><w n="21.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="21.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="21.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="22" num="1.22" lm="8"><w n="22.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="22.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="22.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="22.5" punct="dp:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="dp">é</seg>s</w> :</l>
						<l n="23" num="1.23" lm="8"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t</w> <w n="23.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="23.6" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="24" num="1.24" lm="8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="24.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="24.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="24.7" punct="pt:8">bl<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
						<l n="25" num="1.25" lm="8"><w n="25.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="25.2">qu</w>’<w n="25.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.6">d</w>’<w n="25.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="26" num="1.26" lm="8"><w n="26.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="26.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</w>,</l>
						<l n="27" num="1.27" lm="8"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="27.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="27.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="28" num="1.28" lm="8"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt</w> <w n="28.2">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="28.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="28.5" punct="pv:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pv">e</seg>il</w> ;</l>
						<l n="29" num="1.29" lm="8"><w n="29.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="29.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.3">l<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="29.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oî</seg>t</w> <w n="29.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="29.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="29.7" punct="vg:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="30" num="1.30" lm="8"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="30.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w>-<w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="30.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="30.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="30.7">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w></l>
						<l n="31" num="1.31" lm="8"><w n="31.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="31.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="31.3">l</w>’<w n="31.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="31.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="31.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.7">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="32" num="1.32" lm="8"><w n="32.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="32.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="32.5" punct="pt:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</w>.</l>
					</lg>
				</div></body></text></TEI>