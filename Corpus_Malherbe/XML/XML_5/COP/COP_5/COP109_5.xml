<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP109" modus="sm" lm_max="8">
					<head type="main">L’ARMURE</head>
					<head type="sub_1">(D’APRÈS LE TABLEAU DE VOLLON)</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="1.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">Dr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4">p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="vg:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.3">h<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="4.4">d</w>’<w n="4.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1" punct="vg:1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" punct="vg">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.7" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="6.2" punct="vg:3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="6.4">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="vg:8">d<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>s</w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>ds</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6" punct="vg:8">cu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="8"><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.2">T<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lb<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>t</w> <w n="8.3"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="8.4" punct="pt:8">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pt">o</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="9.4" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="8"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
						<l n="11" num="3.3" lm="8"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>c</w> <w n="11.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">fl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="12" num="3.4" lm="8"><w n="12.1">F<seg phoneme="œ" type="vs" value="1" rule="304" place="1">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.2">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="12.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t</w> <w n="12.4" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l</w> <w n="13.5">d</w>’<w n="13.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rts</w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6">cu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>r</w></l>
						<l n="15" num="4.3" lm="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.4" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8"><w n="16.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t</w> <w n="16.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="16.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="16.5" punct="pt:8">fu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>r</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8"><w n="17.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="18" num="5.2" lm="8"><w n="18.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="18.2" punct="vg:2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg></w>, <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="18.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="18.6">l</w>’<w n="18.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="18.8" punct="pt:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>t</w>.</l>
						<l n="19" num="5.3" lm="8"><w n="19.1" punct="vg:2">B<seg phoneme="a" type="vs" value="1" rule="327" place="1">a</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd</w>, <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="19.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="19.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ps</w> <w n="19.5">d</w>’<w n="19.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="5.4" lm="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="20.2">p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="20.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.7" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>t</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8"><w n="21.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="21.2" punct="vg:2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="21.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c</w>-<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w>-<w n="21.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c</w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="21.7" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="22" num="6.2" lm="8"><w n="22.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="22.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5" punct="pv:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></w> ;</l>
						<l n="23" num="6.3" lm="8"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.5">r<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="24" num="6.4" lm="8"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="24.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="24.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="24.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.7" punct="vg:8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>t</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8"><w n="25.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="25.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="25.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="25.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="26" num="7.2" lm="8"><w n="26.1">Qu</w>’<w n="26.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="26.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="26.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</w>,</l>
						<l n="27" num="7.3" lm="8"><w n="27.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="27.4">l</w>’<w n="27.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="27.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="28" num="7.4" lm="8"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="28.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="28.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t</w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="28.6" punct="pt:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1875">1875.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>