<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP144" modus="cm" lm_max="12">
					<head type="main">LA MORT DU POÈTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.8" punct="pv:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">l<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="2.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="2.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="4.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="4.10" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4" punct="vg:3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s</w>, <w n="5.5">l</w>’<w n="5.6" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.9">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d</w> <w n="5.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt</w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4">cr<seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="6.6">b<seg phoneme="u" type="vs" value="1" rule="428" place="8">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="6.8" punct="vg:12">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="7.9">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="7.11">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt</w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.6" punct="pt:12">gl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2" punct="vg:3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="9.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>x</w> <w n="9.9" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="10.2" punct="pv:3">cl<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pv">e</seg>s</w> ; <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="10.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="10.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="10.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="11.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="11.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="11.4">d</w>’<w n="11.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>r</w>, <w n="11.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>ts</w> <w n="11.7" punct="vg:9">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9" punct="vg">e</seg>ils</w>, <w n="11.8">c<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>x</w> <w n="11.9" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>rts</w>,</l>
						<l n="12" num="3.4" lm="12"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>rs</w> <w n="12.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="12.8" punct="pt:12">pr<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1" punct="vg:2">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.4" punct="pe:6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe">eu</seg>rs</w> ! <w n="13.5" punct="vg:8">T<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg">e</seg></w>, <hi rend="ital"><w n="13.6" punct="pe:12">D<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="e" type="vs" value="1" rule="COP144_1" place="10">e</seg>s</w> <w n="13.7"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="e" type="vs" value="1" rule="272" place="12" punct="pe">æ</seg></w></hi> !</l>
						<l n="14" num="4.2" lm="12"><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">c</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="14.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.7">d<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w> <w n="14.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.11"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="15" num="4.3" lm="12"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="15.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="15.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ts</w> <w n="15.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="15.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
						<l n="16" num="4.4" lm="12"><w n="16.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">s</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="16.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="16.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="16.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="16.9" punct="pt:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>