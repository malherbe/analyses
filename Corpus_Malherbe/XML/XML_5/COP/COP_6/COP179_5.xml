<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP179" modus="sm" lm_max="8">
				<head type="main">Ballade parlementaire</head>
				<head type="sub_1">Sur l’expulsion de l’abbé Delsor</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="1">en</seg>s</w> <w n="1.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="1.3" punct="vg:3">Bl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>c</w>, <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.5">c<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>ls<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="2.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.7">qu</w>’<w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.9">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> « <w n="4.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>d</w> », <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> « <w n="4.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w> ».</l>
					<l n="5" num="1.5" lm="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2" punct="vg:2">Ju<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>f</w>, <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="5.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6" punct="pv:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></w> ;</l>
					<l n="6" num="1.6" lm="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="421" place="3">oi</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.6" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="7" num="1.7" lm="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.3" punct="pe:3">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" punct="pe ps">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !… <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">On</seg></w> <w n="7.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="7.7" punct="pt:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="8" num="1.8" lm="8"><w n="8.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="pt:8">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="8"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="9.2" punct="vg:3">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3" punct="vg">e</seg></w>, <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="2.2" lm="8"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="1">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="10.4" punct="vg:8">B<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="11" num="2.3" lm="8"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">Gu<seg phoneme="i" type="vs" value="1" rule="485" place="2">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5" punct="vg:8">gr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="12" num="2.4" lm="8"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">b<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="12.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="12.7" punct="vg:8">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="13" num="2.5" lm="8"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="13.5" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="14" num="2.6" lm="8"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>q<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="15" num="2.7" lm="8"><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="15.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="15.5" punct="pe:8">l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></w> !</l>
					<l n="16" num="2.8" lm="8"><w n="16.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6" punct="pt:8">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="8"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.4" punct="pt:8">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="18" num="3.2" lm="8"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="18.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="18.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
					<l n="19" num="3.3" lm="8"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="19.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">l</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="19.6" punct="pv:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="20" num="3.4" lm="8"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="20.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="21" num="3.5" lm="8"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">D<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="22" num="3.6" lm="8"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>r</w>, <w n="22.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7">f<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>t</w>-<w n="22.8">c<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9">e</seg></w></l>
					<l n="23" num="3.7" lm="8"><w n="23.1">Qu</w>’<w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg></w> <w n="23.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="23.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="23.6">l<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="23.7" punct="pt:8">B<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="24" num="3.8" lm="8"><w n="24.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="24.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6" punct="pt:8">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<head type="form">ENVOI</head>
					<l n="25" num="4.1" lm="8"><w n="25.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="25.2" punct="vg:3">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>qb<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="25.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="25.5" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="26" num="4.2" lm="8"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3">bl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg></w>-<w n="26.5" punct="pe:8">r<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="27" num="4.3" lm="8"><w n="27.1">L</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="27.4">b<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="27.7" punct="pt:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="28" num="4.4" lm="8"><w n="28.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="28.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="28.6" punct="pt:8">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>