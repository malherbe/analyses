<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP13" modus="cm" lm_max="12">
					<head type="main">À un ange gardien</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2" punct="vg:3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="1.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="1.7" punct="vg:12">chr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">T</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.5" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="2.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.9" punct="vg:12">g<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rdi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="3.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8" punct="pe:12">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">N</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="5.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="5.4" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="5.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="5.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.8">ph<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.10" punct="vg:12">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="6.6" punct="vg:9">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>rs</w>, <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="6.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t</w> <w n="6.9" punct="vg:12">li<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="7.5">pl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="7.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>s</w> <w n="8.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</w>, <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="9.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</w>,</l>
						<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="10.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="10.9" punct="vg:9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="vg">on</seg>t</w>, <w n="10.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="10.11">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11">oin</seg>s</w> <w n="10.12" punct="vg:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</w>,</l>
						<l n="11" num="1.11" lm="12"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="11.7" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="12" num="1.12" lm="12"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="12.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="12.8" punct="pi:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="13" num="1.13" lm="12"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="13.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="13.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="13.10" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
						<l n="14" num="1.14" lm="12"><w n="14.1">B<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="14.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="14.3">n</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w>-<w n="14.5">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.7">qu</w>’<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="14.10">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.11">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="14.12" punct="pi:12">s<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pi">œu</seg>r</w> ?</l>
						<l n="15" num="1.15" lm="12"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="15.2">c<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.3">t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="15.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="15.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="15.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="16" num="1.16" lm="12"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="16.3">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="3">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="16.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg></w> <w n="16.7">c<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="17" num="1.17" lm="12"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="17.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="17.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="17.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="17.8">n<seg phoneme="a" type="vs" value="1" rule="343" place="9">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="10">ï</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</w>,</l>
						<l n="18" num="1.18" lm="12"><w n="18.1">N</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="18.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="18.5">qu</w>’<w n="18.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="18.7">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="18.8">s</w>’<w n="18.9"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="18.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="18.11">c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ls</w> <w n="18.12">d</w>’<w n="18.13"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</w></l>
						<l n="19" num="1.19" lm="12"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="19.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="19.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="19.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="19.6">c<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="19.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="19.8" punct="vg:12">g<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="1.20" lm="12"><w n="20.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2">t</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="20.5">qu</w>’<w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="20.7">n</w>’<w n="20.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="20.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="20.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="20.11" punct="pi:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
					</lg>
				</div></body></text></TEI>