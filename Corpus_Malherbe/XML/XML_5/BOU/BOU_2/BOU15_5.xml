<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU15" modus="sm" lm_max="7">
				<head type="main">CHANSON D’AMOUR</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="1.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="5">y</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5" punct="vg:7">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="7"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w></l>
					<l n="3" num="1.3" lm="7"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="4" num="1.4" lm="7"><w n="4.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="4.4" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="7"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="5.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="7"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="6.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>lh<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w></l>
					<l n="7" num="2.3" lm="7"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">m<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="8" num="2.4" lm="7"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="8.6" punct="pe:7">c<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg>t</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="7"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="9.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
					<l n="10" num="3.2" lm="7"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.6" punct="vg:7">cr<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></w>,</l>
					<l n="11" num="3.3" lm="7"><w n="11.1">R<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.4">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
					<l n="12" num="3.4" lm="7"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">pl<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="12.5" punct="pe:7">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="7"><w n="13.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="13.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.4">l</w>’<w n="13.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="7"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.6" punct="vg:7">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg>s</w>,</l>
					<l n="15" num="4.3" lm="7"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="œ" type="vs" value="1" rule="345" place="2">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="16" num="4.4" lm="7"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4" punct="pe:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pe">on</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="7">— <w n="17.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="17.5" punct="vg:7">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="18" num="5.2" lm="7"><w n="18.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>-<w n="18.4" punct="vg:5">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w>, <w n="18.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="18.6" punct="vg:7">j<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</w>,</l>
					<l n="19" num="5.3" lm="7"><w n="19.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="19.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="5">om</seg></w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="20" num="5.4" lm="7"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="20.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>r</w> <w n="20.5">d</w>’<w n="20.6" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pe">ou</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>