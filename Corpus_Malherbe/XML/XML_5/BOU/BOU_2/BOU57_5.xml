<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU57" modus="cm" lm_max="12">
				<head type="main">MARS</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.5" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rs</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="1.8">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="1.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.10" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.3">h<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.4" punct="vg:6">f<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="2.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>il</w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="3.5" punct="vg:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d</w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="3.10" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">d<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="4.4">fr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.8" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pt">e</seg>il</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.7">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="5.8" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="6.7" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>t</w> <w n="7.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="8.10" punct="pt:12">fr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="9.3" punct="vg:2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>rt</w>, <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="9.8">l</w>’<w n="9.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c</w> <w n="10.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="10.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2" place="8">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="10.8" punct="vg:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="12" num="3.4" lm="12"><w n="12.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.9" punct="pt:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt pt">eau</seg>x</w>..</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="13.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3" punct="pe:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="pe">e</seg>ts</w> ! <w n="13.4">j</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="13.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="8">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="13.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="13.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="14" num="4.2" lm="12"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="14.3" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="14.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="14.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="14.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="14.9" punct="vg:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
					<l n="15" num="4.3" lm="12"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.4">bl<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="15.5">m<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>rs</w> <w n="15.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="15.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="15.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="15.10" punct="vg:12">gl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="4.4" lm="12"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="9">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>r</w> <w n="16.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="16.7" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>r</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="12"><w n="17.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="17.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rd</w>’<w n="17.4" punct="vg:6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="17.7" punct="vg:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>t</w>, <w n="17.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="17.9">n<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="18" num="5.2" lm="12"><w n="18.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="18.4">d</w>’<w n="18.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="18.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="18.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg>s</w> ;</l>
					<l n="19" num="5.3" lm="12"><w n="19.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="19.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="19.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="20" num="5.4" lm="12"><w n="20.1" punct="vg:3">B<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rge<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="20.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="20.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="20.4" punct="vg:6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="vg">ein</seg></w>, <w n="20.5">l</w>’<w n="20.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="20.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="20.8" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="12"><w n="21.1">L</w>’<w n="21.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="21.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="21.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="21.8" punct="vg:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="22" num="6.2" lm="12"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="22.3">gl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="22.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="22.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="22.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="22.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="22.9" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
					<l n="23" num="6.3" lm="12"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">j</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="23.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="23.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.6" punct="vg:6">fl<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="23.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="23.8">d</w>’<w n="23.9"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="23.10" punct="vg:12">g<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="24" num="6.4" lm="12"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="24.3">fl<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="24.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="24.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="24.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>nt</w> <w n="24.7" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>