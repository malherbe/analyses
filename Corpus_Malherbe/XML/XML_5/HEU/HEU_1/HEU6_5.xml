<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre puéril</head><head type="main_subpart">III</head><head type="main_subpart">L’Inspiration et la Pensée</head><head type="sub_1">… et le rêve se mêle aux pensées qu’il caresse</head><div type="poem" key="HEU6" modus="cm" lm_max="12">
						<opener>
							<salute>A Jacques Roupcinsky.</salute>
						</opener>
						<head type="main">PÉGASE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1" punct="pe:3">H<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pe">i</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="1.3">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="1.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>rs</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8" punct="pe:12"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</w> !</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="2.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="2.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="2.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="2.10">gr<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="3" num="1.3" lm="12"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.6" punct="vg:12">cr<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ds</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="4.7" punct="pe:12">t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>d</w> <w n="5.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.7">h<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="5.8" punct="vg:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="6.3">j<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ts</w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="6.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.10" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="7.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5" punct="vg:6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg">en</seg>ts</w>, <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="7.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.9" punct="vg:12">tr<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="8" num="2.4" lm="12"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.6">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w> <w n="8.7">d</w>’<w n="8.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r</w> <w n="8.9" punct="pe:12">l<seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="9.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rs</w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rs</w> <w n="9.8" punct="vg:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="10.8" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="11.2" punct="vg:2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>rs</w>, <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="11.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="11.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.9" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="vg">en</seg>d</w>,</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="12.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.9">tr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.10">s</w>’<w n="12.11" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>d</w> <w n="13.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="13.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>t</w> <w n="13.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="13.9" punct="vg:12">fr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
							<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.4">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>c</w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">aim</seg></w> <w n="14.7">d</w>’<w n="14.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r</w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="14.11" punct="pe:12">r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>