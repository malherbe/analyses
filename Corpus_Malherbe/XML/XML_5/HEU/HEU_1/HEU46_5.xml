<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">VIII</head><head type="main_subpart">Le Goût de la Mort</head><div type="poem" key="HEU46" modus="sm" lm_max="8">
						<head type="main">LA MORT DE L’HOMME</head>
						<head type="sub">ÉPITAPHE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8"><w n="1.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w></l>
							<l n="2" num="1.2" lm="8"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="4">ue</seg>il</w> <w n="2.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="2.6" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
							<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="3.2" punct="vg:5">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="3.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="3.5" punct="vg:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
							<l n="4" num="1.4" lm="8"><w n="4.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="4.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="4.7" punct="pe:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="8"><w n="5.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w>-<w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="5.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="5.6" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</w>,</l>
							<l n="6" num="2.2" lm="8"><w n="6.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="6.2">L<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">cl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="6.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.6" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
							<l n="7" num="2.3" lm="8"><w n="7.1" punct="vg:2">Gl<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="7.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.3" punct="vg:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">Om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="7.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
							<l n="8" num="2.4" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">t</w>’<w n="8.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="8" punct="pt">eoi</seg>r</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="8"><w n="9.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.2" punct="vg:3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="9.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.6" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
							<l n="10" num="3.2" lm="8"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="10.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.6" punct="vg:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="11" num="3.3" lm="8"><w n="11.1">M<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>s</w> <w n="11.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5" punct="ps:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="ps">é</seg></w>…</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="8"><w n="12.1" punct="pe:1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="pe">oi</seg>s</w> ! <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">n<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.4">t</w>’<w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="13" num="4.2" lm="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
							<l n="14" num="4.3" lm="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="14.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						</lg>
					</div></body></text></TEI>