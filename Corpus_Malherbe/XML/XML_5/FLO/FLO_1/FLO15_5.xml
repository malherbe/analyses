<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO15" modus="cp" lm_max="12">
					<head type="number">FABLE XV</head>
					<head type="main">Le Lierre et le Thym</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><space unit="char" quantity="8"></space><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4" punct="vg:4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" punct="vg">ain</seg>s</w>, <w n="1.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6" punct="pe:8">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="2.7" punct="dp:8">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8" punct="dp">ym</seg></w> :</l>
						<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="3.2" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="3.3">c</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.6" punct="pv:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></w> ;</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="5" num="1.5" lm="12"><w n="5.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="vg:6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="5.8">mi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="9">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="5.10">l</w>’<w n="5.11" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="6.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">ch<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="6.7" punct="vg:12">J<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="7" num="1.7" lm="8"><space unit="char" quantity="8"></space><w n="7.1">S</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="7.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7" punct="pt:8">n<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="8.3" punct="vg:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="vg">ai</seg></w>, <w n="8.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6" punct="vg:6">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6" punct="vg">ym</seg></w>, <w n="8.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.8">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="8.9">m</w>’<w n="8.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="8.11" punct="pv:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="9.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="9.9" punct="dp:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="dp">oi</seg></w> :</l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>s</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="10.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w>-<w n="10.7" punct="pv:8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="11.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="12" num="1.12" lm="8"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="12.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6" punct="pt:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1" lm="12"><w n="13.1" punct="vg:3">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="13.2" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>rs</w>, <w n="13.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="7">ai</seg>s<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.5" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="2.2" lm="12"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6">gr<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="14.7"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></w></l>
						<l n="15" num="2.3" lm="8"><space unit="char" quantity="8"></space><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="15.4" punct="vg:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="16" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="16.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6" punct="pt:8">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8" punct="pt">ym</seg></w>.</l>
					</lg>
				</div></body></text></TEI>