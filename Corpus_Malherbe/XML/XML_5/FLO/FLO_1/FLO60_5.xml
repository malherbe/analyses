<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO60" modus="cp" lm_max="12">
					<head type="number">FABLE XVI</head>
					<head type="main">Le Paon, les deux Oisons <lb></lb>et le Plongeon</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290" place="2">aon</seg></w> <w n="1.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="3">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5" punct="vg:6">r<seg phoneme="u" type="vs" value="1" rule="426" place="6" punct="vg">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="1.9"><seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg>x</w></l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="2.4" punct="pt:8">pl<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="3.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rds</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="3.6">d</w>’<w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="3.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rqu<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pt">au</seg>ts</w>.</l>
						<l n="5" num="1.5" lm="12"><w n="5.1" punct="vg:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="5.3">l</w>’<w n="5.4" punct="vg:6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" punct="vg">un</seg></w>, <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="5.7">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="5.9" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="8"><space unit="char" quantity="8"></space><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="6.5" punct="vg:6">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>ts</w>, <w n="6.6" punct="pt:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
						<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3" punct="vg:3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="7.5">l</w>’<w n="7.6" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="7.9" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="8" num="1.8" lm="8"><space unit="char" quantity="8"></space><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.4">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r</w> <w n="8.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.8" punct="pt:8">ch<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.2">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="9.9" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>t</w>.</l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w>-<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w>-<w n="10.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.5">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.6" punct="dp:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>t</w> :</l>
						<l n="11" num="1.11" lm="12"><w n="11.1" punct="vg:2">M<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>rs</w>, <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w>-<w n="11.4">t</w>-<w n="11.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>l</w>, <w n="11.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="11.7">v<seg phoneme="wa" type="vs" value="1" rule="440" place="8">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="11.10">li<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="12" num="1.12" lm="12"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6" punct="dp:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290" place="6" punct="dp">aon</seg></w> : <w n="12.7">c</w>’<w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="12.9">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="12.10" punct="vg:9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>r</w>, <w n="12.11">j</w>’<w n="12.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="12.13" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12" punct="pv">en</seg>s</w> ;</l>
						<l n="13" num="1.13" lm="12"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="13.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="13.5" punct="vg:6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" punct="vg">e</seg>ds</w>, <w n="13.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="13.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ds</w> <w n="13.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="13.11" punct="vg:12">si<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg>s</w>,</l>
						<l n="14" num="1.14" lm="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="14.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="14.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.7" punct="pt:8">qu<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>