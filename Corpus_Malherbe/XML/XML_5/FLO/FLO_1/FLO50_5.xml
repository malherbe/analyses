<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO50" modus="cp" lm_max="12">
					<head type="number">FABLE VI</head>
					<head type="main">Hercule au ciel</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="1.4">d</w>’<w n="1.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>lcm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="1.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>gs</w> <w n="1.9" punct="vg:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="vg">au</seg>x</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t</w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="2.8">di<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="2.9">s</w>’<w n="2.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>pr<seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>nt</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w>-<w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7">f<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="3.8" punct="pt:12">h<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>s</w>.</l>
						<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>rs</w>, <w n="4.2" punct="vg:4">M<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="4.3" punct="vg:6">V<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="4.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="4.5">l</w>’<w n="4.6" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>nt</w>.</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">J<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="5.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="9">ue</seg>il</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="5.8" punct="pt:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</w>.</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="6.5" punct="vg:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="7.2" punct="vg:3">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s</w>, <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.9" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="8.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="?" type="va" value="1" rule="162" place="6">en</seg>t</w> <w n="8.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="8.9" punct="pt:12">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></w>.</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">h<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="9.8" punct="pt:12">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2" punct="vg:2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>ls</w>, <w n="10.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="10.6" punct="vg:8">J<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
						<l n="11" num="1.11" lm="12"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">t</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="11.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.7" punct="pi:6">di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pi">eu</seg></w> ? <w n="11.8">D</w>’<w n="11.9"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.10">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>t</w> <w n="11.11">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="11.12">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="11.13" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="12" num="1.12" lm="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>ct</w>, <w n="12.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6" punct="pi:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="pi">en</seg>s</w> ?</l>
						<l n="13" num="1.13" lm="8"><space unit="char" quantity="8"></space>— <w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.6" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.8" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="1.14" lm="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rs</w>, <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="15" num="1.15" lm="8"><space unit="char" quantity="8"></space><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="15.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="15.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.8" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>