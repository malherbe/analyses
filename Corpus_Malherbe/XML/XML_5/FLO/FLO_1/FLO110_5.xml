<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><div type="poem" key="FLO110" modus="cp" lm_max="12">
					<head type="number">FABLE XXII</head>
					<head type="main">Le Poisson volant *</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="1.2">p<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.3" punct="vg:6">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="1.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="1.7" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</w>,</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w>’<w n="2.6" punct="dp:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</w>.</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="1.6" lm="8"><space unit="char" quantity="8"></space><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.7" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>rs</w> ;</l>
						<l n="7" num="1.7" lm="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="1.8" lm="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="8.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.8" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>rs</w>.</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.4" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="dp">on</seg>d</w> : <w n="9.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.6" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>t</w>, <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="9.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.9" punct="vg:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="10.8" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
						<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="11.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="11.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="11.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="11.8" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>t</w> <w n="12.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">l</w>’<w n="12.6" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>r</w>, <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.8">v<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.9">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="12.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.11">l</w>’<w n="12.12" punct="pt:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>