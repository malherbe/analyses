<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO11" modus="sm" lm_max="7">
					<head type="number">FABLE XI</head>
					<head type="main">Le Chien et le Chat</head>
					<lg n="1">
						<l n="1" num="1.1" lm="7"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="1.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="2" num="1.2" lm="7"><w n="2.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.3" punct="vg:4">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="vg">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t</w></l>
						<l n="3" num="1.3" lm="7"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2">l<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.6" punct="pt:7">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="4" num="1.4" lm="7"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t</w></l>
						<l n="5" num="1.5" lm="7"><w n="5.1" punct="vg:2">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="5.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="5.6" punct="vg:7">z<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="7"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
						<l n="7" num="1.7" lm="7"><w n="7.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
						<l n="8" num="1.8" lm="7"><w n="8.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4" punct="pt:7">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="9" num="1.9" lm="7"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>t</w>, <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="9.5" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></w>,</l>
						<l n="10" num="1.10" lm="7"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="11" num="1.11" lm="7"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="11.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="dp:7">m<seg phoneme="o" type="vs" value="1" rule="438" place="7" punct="dp">o</seg>t</w> :</l>
						<l n="12" num="1.12" lm="7"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.3" punct="vg:4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>c</w>, <w n="12.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.5" punct="vg:7">s<seg phoneme="o" type="vs" value="1" rule="438" place="7" punct="vg">o</seg>t</w>,</l>
						<l n="13" num="1.13" lm="7"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">c</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="13.9" punct="pe:7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>