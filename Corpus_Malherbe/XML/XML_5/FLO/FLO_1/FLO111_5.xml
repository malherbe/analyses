<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="FLO111" modus="sm" lm_max="8">
				<head type="main">ÉPILOGUE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.4" punct="dp:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="dp">au</seg>x</w> :</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="3.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="3.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="3.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>ts</w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="4.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.6" punct="pv:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="5" num="1.5" lm="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6" punct="pt:8">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					<l n="6" num="1.6" lm="8"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rts</w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="6.5" punct="vg:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
					<l n="7" num="1.7" lm="8"><w n="7.1">L</w>’<w n="7.2" punct="vg:2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="2" punct="vg">ue</seg>il</w>, <w n="7.3">l</w>’<w n="7.4" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5" punct="vg">ê</seg>t</w>, <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="1.8" lm="8"><w n="8.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="8.3">l</w>’<w n="8.4" punct="pv:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pv">e</seg>rs</w> ;</l>
					<l n="9" num="1.9" lm="8"><w n="9.1">V<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="1.10" lm="8"><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="10.3">l</w>’<w n="10.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.6" punct="vg:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</w>,</l>
					<l n="11" num="1.11" lm="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="11.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd</w> <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="11.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.8" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>rs</w>.</l>
					<l n="12" num="1.12" lm="8"><w n="12.1" punct="vg:2">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="13" num="1.13" lm="8"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="13.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="13.4" punct="vg:4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">aî</seg>t</w>, <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="13.7">l</w>’<w n="13.8" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pv">en</seg>d</w> ;</l>
					<l n="14" num="1.14" lm="8"><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="14.2" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="14.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.5" punct="vg:*">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="?" type="va" value="1" rule="162" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="15" num="1.15" lm="8"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.4" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="16" num="1.16" lm="8"><w n="16.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w>-<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7" punct="pi:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pi">eu</seg>r</w> ?</l>
					<l n="17" num="1.17" lm="8"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2" punct="vg:2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>x</w>, <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="17.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>x</w> <w n="17.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="17.7" punct="vg:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</w>,</l>
					<l n="18" num="1.18" lm="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="18.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="18.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="18.7" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="19" num="1.19" lm="8"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>il</w> <w n="19.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
					<l n="20" num="1.20" lm="8"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">fl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="21" num="1.21" lm="8"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="21.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="21.6" punct="vg:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
					<l n="22" num="1.22" lm="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="22.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="22.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>