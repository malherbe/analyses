<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH62" modus="sm" lm_max="8">
					<head type="number">XI</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1" punct="pe:1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>s</w> ! <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.6">d</w>’<w n="1.7" punct="pi:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pi">an</seg></w> ?</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="2.2" punct="vg:2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg></w>, <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.6" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2" punct="dp:3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="dp">e</seg></w> : <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.7" punct="pt:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>c</w>.</l>
						<l n="6" num="2.2" lm="8"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="6.3">l</w>’<w n="6.4" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg">eau</seg></w>, <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.7" punct="vg:8">cr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="7.3" punct="vg:4">l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6" punct="vg:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="8" num="2.4" lm="8"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="pt:8">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.7" punct="pi:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pi ti">e</seg>r</w> ? —</l>
						<l n="10" num="3.2" lm="8"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="10.3" punct="vg:3">t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="10.6">gr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.8">qu</w>’<w n="10.9"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="10.10" punct="vg:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="3.3" lm="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.5" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.8" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>r</w>,</l>
						<l n="12" num="3.4" lm="8"><w n="12.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="12.4">m</w>’<w n="12.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="13.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.6">d</w>’<w n="13.7" punct="pi:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pi">an</seg></w> ?</l>
						<l n="14" num="4.2" lm="8"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="14.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="14.4" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rt</w>, <w n="14.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.7" punct="pv:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="15" num="4.3" lm="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="15.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="15.6" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="16" num="4.4" lm="8"><w n="16.1">M<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rt</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="16.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.8" punct="pt:8">j<seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="17.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>rs</w> <w n="17.6">gl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
						<l n="18" num="5.2" lm="8"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>qu<seg phoneme="i" type="vs" value="1" rule="487" place="5">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="18.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="19" num="5.3" lm="8"><w n="19.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg></w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3" punct="vg:5">h<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">en</seg>ts</w>, <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w></l>
						<l n="20" num="5.4" lm="8"><w n="20.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="20.3">g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.7" punct="pe:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="21.3" punct="pt:3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3" punct="pt">en</seg></w>. <w n="21.4">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.6">l</w>’<w n="21.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
						<l n="22" num="6.2" lm="8"><w n="22.1">L</w>’<w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="1">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.5">c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="22.6">d<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="23" num="6.3" lm="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pt">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="23.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">Où</seg></w> <w n="23.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c</w></l>
						<l n="24" num="6.4" lm="8">—<w n="24.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.7" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>