<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH129" modus="cm" lm_max="12">
					<head type="number">XXXI</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="1.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.8" punct="pt:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ls</w> <w n="2.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>s</w> <w n="2.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.9">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="2.10">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rpr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.5">gl<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.7" punct="vg:12">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">h<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="4.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="4.8">m</w>’<w n="4.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="4.10" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ll<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="5.8" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.9">n</w>’<w n="6.10"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="6.11">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="6.12">d</w>’<w n="6.13" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</w> !</l>
						<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">j</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="7.9" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l</w> <w n="8.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>x</w> <w n="8.6">n</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.8">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="8.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.10" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2" punct="vg:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="9.3">ch<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="9.6" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3" punct="vg:5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="10.4">j</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="10.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="10.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="10.9" punct="dp:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="dp">é</seg></w> :</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.7">d</w>’<w n="11.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.4" lm="12"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2">j</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="12.4">f<seg phoneme="u" type="vs" value="1" rule="428" place="3">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.9">n</w>’<w n="12.10"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="12.11">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg></w> <w n="12.12" punct="pt:12">tr<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="vg:3">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="13.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11">en</seg>t</w> <w n="13.6" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="12"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="14.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="14.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="14.7">br<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="14.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="14.10" punct="vg:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>t</w>,</l>
						<l n="15" num="4.3" lm="12"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="15.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.5" punct="tc:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="ti">oi</seg>x</w> — <w n="15.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="15.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="15.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>x</w> <w n="15.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="15.10"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="16" num="4.4" lm="12"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="16.8">d</w>’<w n="16.9" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">j</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="17.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="17.7">d</w>’<w n="17.8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="17.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="17.10" punct="vg:12">bl<seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="12"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="18.8">l</w>’<w n="18.9" punct="pe:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg></w> !</l>
						<l n="19" num="5.3" lm="12"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">j</w>’<w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="19.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="19.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="19.6" punct="vg:9">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="19.7">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w>-<w n="19.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="19.9" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="445" place="12">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="12"><w n="20.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="20.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.6">d</w>’<w n="20.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="20.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="20.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="20.10" punct="pt:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>t</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="12"><w n="21.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="21.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="21.3" punct="pt:4">p<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>s</w>. <w n="21.4">J</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="21.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="21.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.8" punct="vg:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</w>, <w n="21.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="21.10" punct="vg:10">b<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>s</w>, <w n="21.11">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="21.12" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="22" num="6.2" lm="12"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="22.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="22.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.6" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg">e</seg>r</w>, <w n="22.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="22.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="22.9">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="22.10" punct="dp:12">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="11">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="dp">i</seg>r</w> :</l>
						<l n="23" num="6.3" lm="12"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">n</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="23.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="23.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="23.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.8">d</w>’<w n="23.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="23.10">ch<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="24" num="6.4" lm="12"><w n="24.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="24.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="24.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="24.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="24.9" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>