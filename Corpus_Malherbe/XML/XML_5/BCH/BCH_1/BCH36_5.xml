<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH36" modus="sm" lm_max="8">
					<head type="number">XXXV</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">N</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w>-<w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.6">fr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="1.7" punct="pi:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pi">oi</seg>s</w> ?</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w>-<w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w></l>
						<l n="3" num="1.3" lm="8"><w n="3.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">gl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="3.6" punct="vg:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><w n="4.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="4.2" punct="vg:4">s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="4.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
						<l n="5" num="1.5" lm="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
						<l n="6" num="1.6" lm="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-27" place="6">e</seg></w> <w n="6.4" punct="pi:8">h<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="7.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7" punct="pe:8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>d</w> !</l>
						<l n="8" num="2.2" lm="8"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="8.3">s</w>’<w n="8.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
						<l n="9" num="2.3" lm="8"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6" punct="ps:8">p<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
						<l n="10" num="2.4" lm="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="10.6">qu</w>’<w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.8" punct="pv:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></w> ;</l>
						<l n="11" num="2.5" lm="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w>-<w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="11.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
						<l n="12" num="2.6" lm="8"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="12.5" punct="pi:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="8"><w n="13.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="13.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ts</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
						<l n="14" num="3.2" lm="8"><w n="14.1">Ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="14.5" punct="vg:8">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
						<l n="15" num="3.3" lm="8"><w n="15.1" punct="vg:2">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="15.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="3.4" lm="8"><w n="16.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="16.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="16.6" punct="pv:8">r<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>s</w> ;</l>
						<l n="17" num="3.5" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="17.7">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ts</w></l>
						<l n="18" num="3.6" lm="8"><w n="18.1">S</w>’<w n="18.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4">plu<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="18.5" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1" lm="8"><w n="19.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="19.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="19.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="19.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w></l>
						<l n="20" num="4.2" lm="8"><w n="20.1">S</w>’<w n="20.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.4" punct="pi:6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="pi">e</seg>s</w> ? <w n="20.5" punct="vg:8">Bu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</w>,</l>
						<l n="21" num="4.3" lm="8"><w n="21.1">T<seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="21.2" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="21.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="21.4">h<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="22" num="4.4" lm="8"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.4" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="445" place="6" punct="vg">û</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="22.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w></l>
						<l n="23" num="4.5" lm="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="23.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w></l>
						<l n="24" num="4.6" lm="8"><w n="24.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="24.5" punct="pt:8">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1" lm="8"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="25.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="25.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="25.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w></l>
						<l n="26" num="5.2" lm="8"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="26.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="3">œ</seg>il</w> <w n="26.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="26.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w></l>
						<l n="27" num="5.3" lm="8"><w n="27.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="27.2">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="27.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="27.5" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
						<l n="28" num="5.4" lm="8"><w n="28.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="28.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="28.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="28.4" punct="vg:6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">em</seg>ps</w>, <w n="28.5" punct="pv:8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pv">em</seg>ps</w> ;</l>
						<l n="29" num="5.5" lm="8"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2" punct="vg:2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="29.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">oû</seg>l</w> <w n="29.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.7" punct="vg:8">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">em</seg>ps</w>,</l>
						<l n="30" num="5.6" lm="8"><w n="30.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="30.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="30.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="30.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="30.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.7" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1" lm="8"><w n="31.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="31.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg></w> <w n="31.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="31.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="31.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.6">gu<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w></l>
						<l n="32" num="6.2" lm="8"><w n="32.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="32.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l</w> <w n="32.3">d</w>’<w n="32.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="3">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="32.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="32.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="32.7" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</w>,</l>
						<l n="33" num="6.3" lm="8"><w n="33.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="33.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="33.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="33.5">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="33.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="33.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="34" num="6.4" lm="8"><w n="34.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2" punct="vg:2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="34.3" punct="vg:4">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>t</w>, <w n="34.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="35" num="6.5" lm="8"><w n="35.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="35.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="35.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="35.5">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="35.6" punct="vg:8">n<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="36" num="6.6" lm="8"><w n="36.1">T</w>’<w n="36.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="36.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="36.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="36.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="36.6" punct="pt:8">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>