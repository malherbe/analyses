<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH77" modus="cm" lm_max="12">
					<head type="number">XXIII</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="1.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.8" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="2.9" punct="vg:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="3.3" punct="vg:4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.10" punct="pt:12">l<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="4.8" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="5" num="2.2" lm="12"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="5.6">l</w>’<w n="5.7" punct="vg:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="6" num="2.3" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="6.9" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="12"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="7.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="7.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="7.8" punct="pv:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>r</w> ;</l>
						<l n="8" num="3.2" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="8.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="8.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.8">r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405" place="11">eu</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</w></l>
						<l n="9" num="3.3" lm="12"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="9.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="9.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="9.8" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="12"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="10.3">v<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="10.4" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>l</w>, <w n="10.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="10.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="11">ou</seg><seg phoneme="i" type="vs" value="1" rule="476" place="12" punct="vg">ï</seg></w>,</l>
						<l n="11" num="4.2" lm="12"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="11.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="11.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="11.5">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="8">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="11.8" punct="vg:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>t</w>,</l>
						<l n="12" num="4.3" lm="12"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="12.4">fl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.6" punct="ps:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg>s</w>…</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="12"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.4" punct="tc:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="ti">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> — <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="13.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="13.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="13.8" punct="vg:8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rd</w>, <w n="13.9">r<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="487" place="11">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="14" num="5.2" lm="12"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="14.7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="14.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="15" num="5.3" lm="12"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="15.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="15.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="15.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="15.8" punct="pe:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg>s</w> !</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1" lm="12"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="16.5" punct="vg:9">m<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="16.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="16.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
						<l n="17" num="6.2" lm="12"><w n="17.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="17.4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="wa" type="vs" value="1" rule="440" place="8">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="17.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
						<l n="18" num="6.3" lm="12"><w n="18.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">fou<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="18.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="18.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="18.9" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="7">
						<l n="19" num="7.1" lm="12"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="19.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="19.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="19.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="19.7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="19.8">d</w>’<w n="19.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</w>,</l>
						<l n="20" num="7.2" lm="12"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">n</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="20.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="20.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="20.7">p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="20.8" punct="pv:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>x</w> ;</l>
						<l n="21" num="7.3" lm="12"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="21.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="21.6" punct="vg:9">j<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>s</w>, <w n="21.7" punct="pe:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1" lm="12"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="22.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="22.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="22.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="22.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="22.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="22.8" punct="pv:12">t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
						<l n="23" num="8.2" lm="12"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="23.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="23.5">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="23.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="23.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
						<l n="24" num="8.3" lm="12"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="24.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="24.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="24.6">pl<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="24.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="24.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="24.9" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>