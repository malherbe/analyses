<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH35" modus="cm" lm_max="12">
					<head type="number">XXXIV</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Ils sont morts jusqu’à l’âme, ils sont anéantis.</quote>
								<bibl>
									(<name>SULLY PRUDHOMME</name>.)
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.7" punct="vg:12">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>ds</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="2.7">h<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="2.8" punct="vg:12">c<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="3.7">d<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>gts</w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">gl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.5">b<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="4.6">d</w>’<w n="4.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.4">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.7" punct="pt:12">t<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="12"><w n="6.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="6.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="6.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="6.6" punct="pi:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="11">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></w> ?</l>
						<l n="7" num="2.2" lm="12"><w n="7.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="7.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="7.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="8" num="2.3" lm="12"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="9" num="2.4" lm="12"><w n="9.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="9.4">dr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="9.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>ts</w> <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="9.8">l</w>’<w n="9.9" punct="pv:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="11">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></w> ;</l>
						<l n="10" num="2.5" lm="12"><w n="10.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="10.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.11" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="12"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="11.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="11.10" punct="vg:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</w>,</l>
						<l n="12" num="3.2" lm="12"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="12.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="12.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="13" num="3.3" lm="12"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">s</w>’<w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="4">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="13.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="13.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>ts</w> <w n="13.7" punct="pv:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>rs</w> ;</l>
						<l n="14" num="3.4" lm="12"><w n="14.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="14.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="14.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="14.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ils</w> <w n="14.8" punct="vg:12">f<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</w>,</l>
						<l n="15" num="3.5" lm="12"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="15.6">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="15.7" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1" lm="12"><w n="16.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="16.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w>-<w n="16.3" punct="vg:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="16.6">v<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="16.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>squ</w>’<w n="16.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="16.9" punct="vg:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</w>,</l>
						<l n="17" num="4.2" lm="12"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="17.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="17.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="18" num="4.3" lm="12"><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="18.3">d<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="18.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="18.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="18.7" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>r</w> ;</l>
						<l n="19" num="4.4" lm="12"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="19.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="19.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="19.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="19.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="19.9" punct="vg:12">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="11">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</w>,</l>
						<l n="20" num="4.5" lm="12"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.4" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg">e</seg>il</w>, <w n="20.5">l</w>’<w n="20.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="20.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="20.9" punct="pi:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>