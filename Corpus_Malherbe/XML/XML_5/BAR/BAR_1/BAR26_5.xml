<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR26" modus="cp" lm_max="12">
				<opener>
					<salute>À ***</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><space quantity="12" unit="char"></space><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="1.4" punct="tc:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg ti">a</seg>s</w>, — <w n="1.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.10" punct="pt:8">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="2.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.5" punct="pt:6">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></w>. <w n="2.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">I</seg>l</w> <w n="2.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rt</w>, <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="2.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.11" punct="ps:12">su<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="ps">i</seg>s</w>…</l>
					<l n="3" num="1.3" lm="10"><space quantity="4" unit="char"></space><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="3.3" punct="vg:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="3.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="3.8">f<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="3.9">l</w>’<w n="3.10" punct="vg:10">h<seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8"><space quantity="12" unit="char"></space><w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="4.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">n</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="4.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.9">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="5" num="1.5" lm="8"><space quantity="12" unit="char"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="5.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.6" punct="pt:8">fu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1" lm="8"><space quantity="12" unit="char"></space><w n="6.1" punct="pe:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg></w> ! <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="6.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6">s</w>’<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="7" num="2.2" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="7.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="7.8"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w>-<w n="7.9" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</w>.</l>
					<l n="8" num="2.3" lm="8"><space quantity="12" unit="char"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="8.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">c</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="8.10" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="9" num="2.4" lm="8"><space quantity="12" unit="char"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">f<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>t</w>-<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="9.4" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="9.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="9.7">s</w>’<w n="9.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="2.5" lm="10"><space quantity="4" unit="char"></space><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2">t</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.4" punct="tc:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg ti">oi</seg>r</w>, — <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.8">t</w>’<w n="10.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="10.10">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="10.11" punct="pe:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1" lm="8"><space quantity="12" unit="char"></space><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="11.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="11.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w>-<w n="11.5">t</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.9" punct="pe:8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="12" num="3.2" lm="12"><w n="12.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="12.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>s</w> <w n="12.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="12.8" punct="pe:12">j<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>s</w> !</l>
					<l n="13" num="3.3" lm="8"><space quantity="12" unit="char"></space><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="13.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="13.3">t</w>’<w n="13.4" punct="pt:6"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></w>. <w n="13.5" punct="ps:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					<l n="14" num="3.4" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">m</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="14.8">d<seg phoneme="o" type="vs" value="1" rule="435" place="8">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="14.9">d</w>’<w n="14.10"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="15" num="3.5" lm="8"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="15.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="15.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.5" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pe">e</seg>ts</w> !</l>
				</lg>
			</div></body></text></TEI>