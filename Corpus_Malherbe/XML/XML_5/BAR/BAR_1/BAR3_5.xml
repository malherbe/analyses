<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR3" modus="cm" lm_max="10">
				<opener>
					<salute>À ***.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="10"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.4" punct="vg:5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="vg">œu</seg>r</w>, <w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="1.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="1.8" punct="vg:10">fl<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10"><w n="2.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>ts</w>, <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ts</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="2.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="2.8" punct="pv:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pv">eau</seg></w> ;</l>
					<l n="3" num="1.3" lm="10"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="3.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.5" punct="tc:5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="5" punct="vg ti">en</seg>s</w>, — <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="3.10" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="10"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="4.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="4.7" punct="dp:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ge<seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
					<l n="5" num="1.5" lm="10"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">c</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="5.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="5.9" punct="pe:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pe">eau</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1" lm="10"><w n="6.1" punct="pe:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="pe">i</seg></w> ! <w n="6.2">c</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="6.4" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="6.5" punct="ps:5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="ps">i</seg></w>… <w n="6.6">C</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="6.8" punct="vg:7">t<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="6.9">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.10" punct="vg:10">f<seg phoneme="a" type="vs" value="1" rule="193" place="10">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.2" lm="10"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">m</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="7.8" punct="ps:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="ps">ou</seg>r</w>…</l>
					<l n="8" num="2.3" lm="10"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="8.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="8.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="8.9">d</w>’<w n="8.10" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="9" num="2.4" lm="10"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5" punct="vg:5">mi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5" punct="vg">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="9.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="9.10" punct="pe:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1" lm="10"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="vg:3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="10.3" punct="ps:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="ps">ai</seg>s</w>… <w n="10.4">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="11" num="3.2" lm="10"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>rs</w> <w n="11.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rs</w> <w n="11.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="11.9" punct="ps:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="ps">a</seg>s</w>…</l>
					<l n="12" num="3.3" lm="10"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="12.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="12.7" punct="ps:10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg></w>…</l>
					<l n="13" num="3.4" lm="10"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="13.3">m</w>’<w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr</w>’<w n="13.5"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="13.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="13.10" punct="pe:10">br<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="14" num="4.1" lm="10"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.5" punct="ps:5">br<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="ps">a</seg>s</w>… <w n="14.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.7">br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="14.8">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="14.9">qu</w>’<w n="14.10"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.11" punct="pv:10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
					<l n="15" num="4.2" lm="10"><w n="15.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="15.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="15.4" punct="pe:5">h<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>s</w> ! <w n="15.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt</w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="15.8" punct="dp:10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="dp">er</seg></w> :</l>
					<l n="16" num="4.3" lm="10"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="16.4">s</w>’<w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="16.6" punct="vg:5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>, <w n="16.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="16.9">l</w>’<w n="16.10" punct="ps:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg></w>…</l>
					<l n="17" num="4.4" lm="10"><w n="17.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="17.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>nt</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="17.7" punct="pe:10">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></w> !</l>
				</lg>
			</div></body></text></TEI>