<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV14" modus="sm" lm_max="5">
				<head type="main">Bal décadent</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Vais m’en aller !</quote>
							<bibl>
								<name>TRISTAN CORBIÈRE</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="5"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="2" num="1.2" lm="5"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3" punct="pt:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1" lm="5"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w></l>
					<l n="4" num="2.2" lm="5"><w n="4.1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="a" type="vs" value="1" rule="365" place="2">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="4.2" punct="pt:5">fl<seg phoneme="y" type="vs" value="1" rule="454" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" punct="pt">e</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1" lm="5"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.4">chl<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
					<l n="6" num="3.2" lm="5"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">c</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.5" punct="pt:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1" lm="5"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="7.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>,</l>
					<l n="8" num="4.2" lm="5"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="8.4" punct="pt:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt">a</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1" lm="5"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
					<l n="10" num="5.2" lm="5"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3" punct="pt:5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1" lm="5"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w></l>
					<l n="12" num="6.2" lm="5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>s</w> <w n="12.4" punct="pt:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="pt">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w>.</l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1" lm="5"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">fl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>x</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5" punct="vg:5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="14" num="7.2" lm="5"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4" punct="vg:5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1" lm="5"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="15.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="15.3">c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w></l>
					<l n="16" num="8.2" lm="5"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3" punct="vg:5">v<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg>s</w>,</l>
				</lg>
				<lg n="9">
					<l n="17" num="9.1" lm="5"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>rs</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5" punct="vg:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="18" num="9.2" lm="5"><w n="18.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="315" place="4">Eau</seg></w>-<w n="18.4" punct="pt:5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="19" num="10.1" lm="5"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="19.3" punct="vg:5">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>,</l>
					<l n="20" num="10.2" lm="5"><w n="20.1">G<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="20.2" punct="vg:5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>,</l>
				</lg>
				<lg n="11">
					<l n="21" num="11.1" lm="5"><w n="21.1">L<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="22" num="11.2" lm="5"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="12">
					<l n="23" num="12.1" lm="5"><w n="23.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="23.2">d</w>’<w n="23.3" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>x</w>,</l>
					<l n="24" num="12.2" lm="5"><w n="24.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="24.2">d</w>’<w n="24.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">An</seg>thr<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>x</w>,</l>
				</lg>
				<lg n="13">
					<l n="25" num="13.1" lm="5"><w n="25.1">Bl<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="25.3" punct="pt:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					<l n="26" num="13.2" lm="5"><w n="26.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="26.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="26.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="26.5" punct="vg:5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="14">
					<l n="27" num="14.1" lm="5"><w n="27.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>t</w>,</l>
					<l n="28" num="14.2" lm="5"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">m</w>’<w n="28.3" punct="dp:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="dp">ai</seg>t</w> :</l>
				</lg>
				<lg n="15">
					<l n="29" num="15.1" lm="5">«<w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.2" punct="vg:5">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>s</w>,</l>
					<l n="30" num="15.2" lm="5"><w n="30.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="30.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>z</w></l>
				</lg>
				<lg n="16">
					<l n="31" num="16.1" lm="5"><w n="31.1">D</w>’<w n="31.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="31.4">fl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="32" num="16.2" lm="5"><w n="32.1">M</w>’<w n="32.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="32.3">l</w>’<w n="32.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="17">
					<l n="33" num="17.1" lm="5"><w n="33.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="33.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="33.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="33.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="33.5" punct="vg:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg">e</seg>rts</w>,</l>
					<l n="34" num="17.2" lm="5"><w n="34.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="34.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="34.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="34.4" punct="pt:5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="pt">e</seg>rs</w>.</l>
				</lg>
				<lg n="18">
					<l n="35" num="18.1" lm="5"><w n="35.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="35.2" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="36" num="18.2" lm="5"><w n="36.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="36.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.3" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="19">
					<l n="37" num="19.1" lm="5"><w n="37.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="37.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="37.3">t</w>’<w n="37.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>,</l>
					<l n="38" num="19.2" lm="5"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="38.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="38.3" punct="pe:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe">er</seg></w> !»</l>
				</lg>
				<lg n="20">
					<l n="39" num="20.1" lm="5"><w n="39.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="39.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="39.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="39.4">m</w>’<w n="39.5" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					<l n="40" num="20.2" lm="5"><w n="40.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="40.2" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s</w>, <w n="40.3" punct="pe:5">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="21">
					<l n="41" num="21.1" lm="5"><w n="41.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w> <w n="41.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="41.3">s</w>’<w n="41.4" punct="ps:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="ps">er</seg></w>…</l>
					<l n="42" num="21.2" lm="5"><w n="42.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="42.2">s</w>’<w n="42.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="42.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w></l>
				</lg>
				<lg n="22">
					<l n="43" num="22.1" lm="5"><w n="43.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="43.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="43.3" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="44" num="22.2" lm="5"><w n="44.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="44.2" punct="pe:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe pe">e</seg></w> !!</l>
				</lg>
			</div></body></text></TEI>