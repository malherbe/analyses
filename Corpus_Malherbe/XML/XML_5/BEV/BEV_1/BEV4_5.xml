<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV4" modus="cm" lm_max="10">
				<head type="main">Suavitas</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="2" num="1.2" lm="10"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">n<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">im</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.7">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.8">d</w>’<w n="2.9" punct="pt:10"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pt">o</seg>r</w>.</l>
					<l n="3" num="1.3" lm="10"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">R<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="vg">o</seg>r</w>,</l>
					<l n="4" num="1.4" lm="10"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.4">L<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>s</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.7" punct="pt:10">cr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10"><w n="5.1">F<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.6" punct="pe:8">j<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="pe">e</seg></w> ! <w n="5.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>s</w></l>
					<l n="6" num="2.2" lm="10"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ls<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="6.6">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.3" lm="10"><w n="7.1">H<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="7.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.4">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.5" punct="vg:10">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.4" lm="10"><w n="8.1">N</w>’<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6">ti<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="8.8" punct="pi:10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pi">ain</seg>s</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5" punct="pe:5">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="pe">i</seg>t</w> ! <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="9.7" punct="pe:10">m<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
					<l n="10" num="3.2" lm="10"><w n="10.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="10.6" punct="vg:10">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>ts</w>,</l>
					<l n="11" num="3.3" lm="10"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="11.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="11.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>s</w>,</l>
					<l n="12" num="3.4" lm="10"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="12.8" punct="pe:10">R<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>