<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA14" modus="sm" lm_max="8">
				<head type="number">VIII</head>
				<head type="main">Lesbina</head>
				<opener>
					<epigraph>
						<cit>
							<quote>N’ai-je pas pour toi, belle juive</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">N</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w>-<w n="1.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="1.6" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.8" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="2">e</seg></w> <w n="2.3" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="2.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pi">ez</seg></w> ?</l>
					<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="4.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="4">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="5" num="1.5" lm="8"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="5.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="5.6" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1" lm="8"><w n="6.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="6.3" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="6.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.6" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.2" lm="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ds</w>-<w n="7.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="7.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="7.5" punct="vg:8">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="8" num="2.3" lm="8"><w n="8.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="8.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w>-<w n="8.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.8" punct="pi:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					<l n="9" num="2.4" lm="8">— <w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1">E</seg>s</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="9.3" punct="pi:5">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pi">e</seg></w> ? <w n="9.4">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="2.5" lm="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pi">an</seg>t</w> ?</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1" lm="8"><w n="11.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="11.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="3.2" lm="8"><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="12.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.5" punct="pt:8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></w>.</l>
					<l n="13" num="3.3" lm="8"><w n="13.1" punct="vg:3">C<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="13.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="14" num="3.4" lm="8"><w n="14.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4" punct="vg:5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.6" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="15" num="3.5" lm="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.4" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="15.5" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1" lm="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="16.5" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="16.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7">m</w>’<w n="16.8" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="17" num="4.2" lm="8"><w n="17.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="17.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="17.6" punct="vg:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>c</w>,</l>
					<l n="18" num="4.3" lm="8"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="18.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="18.7" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="19" num="4.4" lm="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="19.4">m</w>’<w n="19.5" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="19.7">qu</w>’<w n="19.8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="19.9">m</w>’<w n="19.10"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="20" num="4.5" lm="8"><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="20.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="20.4" punct="vg:5">br<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="20.6" punct="pe:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1" lm="8"><w n="21.1" punct="pe:2">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe">e</seg></w> ! <w n="21.2" punct="pe:5">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pe">an</seg>t</w> ! <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg></w> <w n="21.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="21.5" punct="pe:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="22" num="5.2" lm="8"><w n="22.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg>s</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
					<l n="23" num="5.3" lm="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="23.3" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="23.4">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.5" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="24" num="5.4" lm="8"><w n="24.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.5">s<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="25" num="5.5" lm="8"><w n="25.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="25.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bh<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1" lm="8"><w n="26.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="26.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="26.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="26.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="26.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="27" num="6.2" lm="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>f<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="27.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="27.4" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="28" num="6.3" lm="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="28.4" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="29" num="6.4" lm="8"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="29.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="29.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="29.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="30" num="6.5" lm="8"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="30.4" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pe">en</seg>ts</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">Novembre 1883.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>