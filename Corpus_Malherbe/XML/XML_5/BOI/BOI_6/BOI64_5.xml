<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI64" modus="cm" lm_max="12">
				<head type="main">Vers en style de Chapelain, pour mettre <lb></lb>à la fin de son poème de « la Pucelle »</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">M<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="1.5" punct="vg:6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>r</w>, <w n="1.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.10">r<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.11" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="2.3" punct="vg:6">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="2.4">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="2.6" punct="pv:12">M<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.4">l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rd</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="3.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="3.9" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="12" punct="vg">en</seg>s</w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="4.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="4.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="4.7">f<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="4.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="4.9" punct="pt:12">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>ts</w>.</l>
				</lg>
			</div></body></text></TEI>