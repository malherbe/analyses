<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI45" modus="cp" lm_max="12">
				<head type="main">ÉPIGRAMME</head>
				<head type="sub">A un médecin</head>
				<opener>
					<dateline>
						<date when="1674">(1674)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="1.8">qu</w>’<w n="1.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.10">c<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">G<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.5">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="9">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>t</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="3.6" punct="pt:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">n</w>’<w n="4.8"><seg phoneme="y" type="vs" value="1" rule="391" place="8">eu</seg>s</w> <w n="4.9">j<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="4.10" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12" punct="vg">ein</seg></w>,</l>
					<l n="5" num="1.5" lm="8"><space unit="char" quantity="8"></space><w n="5.1" punct="pv:2">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="pv">in</seg></w> ; <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="5.6" punct="dp:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">l</w>’<w n="6.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="u" type="vs" value="1" rule="426" place="6" punct="vg">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.7" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
					<l n="7" num="1.7" lm="8"><space unit="char" quantity="8"></space><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="7.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>