<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES PHILOSOPHIQUES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1634 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Philosophiques</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louiseakermanpoesiesphilosophiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<normalization>
					<p>Les balises de pagination ont été supprimées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK34" modus="cm" lm_max="12">
				<head type="number">XII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2" punct="pe:4">N<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="1.3" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="vg">ô</seg>t</w>, <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="9">om</seg></w> <w n="1.7">d</w>’<w n="1.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>str<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="2.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="2.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="2.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="2.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bs<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rb<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="3.4">s</w>’<w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.8" punct="dp:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="4" num="1.4" lm="12">« <w n="4.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe">oi</seg></w> ! <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g</w> <w n="4.5">br<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="4.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="4.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="4.9" punct="pi:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></w> ?</l>
					<l n="5" num="1.5" lm="12"><w n="5.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="5.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="5.6">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.7">l</w>’<w n="5.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>fl<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="6.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="6.5">c</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="6.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="6.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="6.9">l</w>’<w n="6.10" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>s</w>.</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3" punct="vg:4">br<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="7.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.9" punct="pv:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.4" punct="dp:6"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="dp">o</seg>r</w> : <w n="8.5" punct="pe:9">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="pe">ez</seg></w> ! <w n="8.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="8.7" punct="pt:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>s</w>. »</l>
				</lg>
			</div></body></text></TEI>