<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK21" modus="cp" lm_max="12">
				<head type="number">XXI</head>
				<head type="main">Daphné</head>
					<opener>
						<salute>À Éva Callimaki-Catargi</salute>
					</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>, <w n="1.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="o" type="vs" value="1" rule="444" place="7">O</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8">ym</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="2.9" punct="vg:12">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>rs</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2" punct="vg:2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>r</w>, <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4">l<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.6" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg">ain</seg></w>, <w n="3.7">s</w>’<w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="3.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.11">tr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="3">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>rs</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="5.2" punct="vg:3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="5.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="5.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.8" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="6.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="6.8" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>ts</w> ;</l>
					<l n="7" num="2.3" lm="12"><w n="7.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="7.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.5" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="7.8">f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="8.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4">l<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="8.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.7" punct="pt:8">br<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w>-<w n="9.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.8" punct="dp:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">I</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.5">fu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="10.9">m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="10.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.11" punct="pt:12">f<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pt">oi</seg></w>.</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg">oin</seg></w>, <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="12.5" punct="pe:8">s<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="13.3" punct="vg:3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="vg">au</seg>t</w>, <w n="13.4">d</w>’<w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="13.7">qu</w>’<w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="13.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="13.10">n</w>’<w n="13.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="12"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="14.3" punct="vg:6">D<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>phn<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="14.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="14.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="14.8" punct="vg:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></w>,</l>
					<l n="15" num="4.3" lm="12"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="15.6">s</w>’<w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="15.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="15.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="15.10">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="16" num="4.4" lm="8"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="16.4">l<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="16.5" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
				</lg>
			</div></body></text></TEI>