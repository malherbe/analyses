<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV31" modus="cm" lm_max="12">
					<head type="main">JUILLET</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>h</w> <w n="1.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe ps">eu</seg></w> !… <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w>-<w n="1.6">t</w>-<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.9">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="1.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="1.11" punct="pi:12">ju<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pi">e</seg>t</w> ?</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3" punct="pe:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe">i</seg></w> ! <w n="2.4">J<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="2.6" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="2.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="2.9" punct="pe:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.5" punct="pe:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe">ou</seg>rs</w> ! <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">E</seg>t</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="3.8">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.4" punct="dp:4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" punct="dp">ein</seg></w> : <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ps</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="4.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ps</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.10" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="307" place="11">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pe">e</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rd</w>’<w n="5.6" punct="vg:6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="5.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="5.10">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t</w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:4">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4" punct="vg:6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>ts</w>, <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="6.7" punct="ps:12"><seg phoneme="e" type="vs" value="1" rule="353" place="10">e</seg>ffr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="7.2">sp<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3" punct="vg:6">di<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>z<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="7.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="8.7">r<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.8" punct="pe:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pe">e</seg>t</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12">« <w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.5" punct="vg:9">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="9.7" punct="vg:11">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" punct="vg">oin</seg></w>, <w n="9.8" punct="pi:12">s<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps">e</seg></w> ? »…</l>
						<l n="10" num="3.2" lm="12">…<w n="10.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="10.2" punct="pe:2">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe ps">on</seg></w> !… <w n="10.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="10.6">m</w>’<w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="10.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="10.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.10">m<seg phoneme="ø" type="vs" value="1" rule="402" place="12">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="11" num="3.3" lm="12"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="11.7">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>ds</w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="12.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.4" punct="ps:6"><seg phoneme="o" type="vs" value="1" rule="432" place="6" punct="ps">o</seg>s</w>… <w n="12.5">J<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="12.8">pl<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="13" num="4.2" lm="12"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="13.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="13.6">m</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.8" punct="pe:7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pe ps">ai</seg>t</w> !… <w n="13.9">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="13.10">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.11">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>d</w></l>
						<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="14.8" punct="pe:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<closer>
						<note id="" type="footnote">A la lecture, les deux sonnets, juillet, Août doivent s’enchaîner, Juillet,— Tous les diètes sans répit, rouge canaille, La sonorité "é", qui domine, pince le son, le remonte, analogue à l’altération dièze.</note>
					</closer>
				</div></body></text></TEI>