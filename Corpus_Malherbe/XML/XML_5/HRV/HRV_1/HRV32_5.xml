<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV32" modus="cm" lm_max="12">
					<head type="main">AOÛT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="1.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="1.8" punct="ps:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="ps">en</seg>t</w>…</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.8">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.10">d</w>’<w n="2.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.4" punct="vg:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>s</w>, <w n="3.5">f<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.7" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="4.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.7">d</w>’<w n="4.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l</w> <w n="5.8">n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2" punct="ps:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="ps">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>… <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="444" place="5">O</seg></w> <w n="6.4" punct="pe:8">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="pe">e</seg></w> ! <w n="6.5">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>x</w> <w n="6.6" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="7.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>c</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="7.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.9">d</w>’<w n="7.10" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="8" num="2.4" lm="12">…<w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">h<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="8.3">d</w>’<w n="8.4">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="8.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.5" punct="vg:6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="9.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>ts</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.8" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="10.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="10.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="11.2">fr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ts</w> <w n="11.3" punct="pe:5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5" punct="pe">u</seg>s</w> ! <w n="11.4">B<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="11.6" punct="pe:12">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>x</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="12.8">n</w>’<w n="12.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="12.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="12.11">qu</w>’<w n="12.12" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.5" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="13.6">m</w>’<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="13.10" punct="pe:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>x</w> !</l>
						<l n="14" num="4.3" lm="12"><w n="14.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="14.2" punct="pe:2">ou<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pe ps">i</seg></w> !… <w n="14.3" punct="vg:4">Cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="14.4" punct="vg:6">h<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="14.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="14.7" punct="vg:9">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9" punct="vg">en</seg></w>, <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="14.10">l<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					</lg>
					<closer>
						<note id="" type="footnote">Août,‒ Ut majeur‒  chute du demi-ton (indiquée dès le dernier tercet à juillet) et donnant tout d’abord une impression de détente, mais sans satisfaction, plus de poids que de repos, à cause de l’inertie du ton ut majeur, La tonalité est indiquée par la sonorité " an" large, pleine et un peu plate.</note>
					</closer>
				</div></body></text></TEI>