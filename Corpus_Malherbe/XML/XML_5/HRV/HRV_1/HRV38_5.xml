<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV38" modus="cm" lm_max="12">
				<head type="main">FÉVRIER</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="1.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.8">F<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="ps:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="ps">ou</seg>r</w>… <w n="2.4">P<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="2.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="2.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="2.11">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="3.4" punct="ps:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="ps">u</seg>s</w>… <w n="3.5">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="3.7">v<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="3.8">c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="4.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="4.7" punct="pe:12">m<seg phoneme="u" type="vs" value="1" rule="428" place="11">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.3">d</w>’<w n="5.4" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="dc-2" place="3">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>st</w>, <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="5.7" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</w>, <w n="5.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.10">p<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d</w>’<w n="6.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6">en</seg>s</w>-<w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.7">m</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lgu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.7">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.9">t</w>’<w n="7.10" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</w>, <w n="8.4">t</w>’<w n="8.5" punct="ps:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="5">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="ps">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="8.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="8.10" punct="pe:12">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">er</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="9.2">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4" punct="pe:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe">u</seg>s</w> ! <w n="9.5">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="9.10">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="10.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6" punct="ps:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="ps">oi</seg></w>… <w n="10.7" punct="vg:9">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>s</w>, <w n="10.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w>-<w n="10.9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg></w> « <w n="10.10" punct="pe:12">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !»</l>
						<l n="11" num="3.3" lm="12">…<w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.9" punct="vg:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt</w> <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="12.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.9" punct="ps:12">di<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
						<l n="13" num="4.2" lm="12"><w n="13.1" punct="ps:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="ps">i</seg>s</w>… <w n="13.2" punct="ps:2">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="ps">o</seg>l</w>… <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="13.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="13.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="13.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="13.11" punct="ps:12">y<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="ps">eu</seg>x</w>…</l>
						<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="14.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="14.6" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date to="1922" from="1921">Mars 1921 ‒ Février 1922</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>