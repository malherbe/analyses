<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV33" modus="cm" lm_max="12">
					<head type="main">SEPTEMBRE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="pe:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ps">e</seg></w> !… <w n="1.2">V<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.3">S<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.6">pl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="2.7">d</w>’<w n="2.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="2.9" punct="vg:12">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ts</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.6" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> ! <w n="3.7">T<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.9">j<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="4.4">r<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>thm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="4.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="4.8">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>squ</w>’<w n="4.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="4.10" punct="pe:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="5.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>t</w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="6.2">l</w>’<w n="6.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="6.7">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="6.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.9">pl<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="7" num="2.3" lm="12"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="7.3">t</w>’<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.6" punct="ps:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ps">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w>-<w n="7.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.10">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="8.7" punct="pe:12">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>x</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.5" punct="vg:9">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="vg">eu</seg>rs</w>, <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="10.9" punct="vg:12">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="11.3">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="11.9">pl<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="11.10">d</w>’<w n="11.11" punct="pi:12"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pi">o</seg>r</w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="12.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="12.5" punct="ps:6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" punct="ps">e</seg>ds</w>… <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="12.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="12.10" punct="ps:12">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
						<l n="13" num="4.2" lm="12">…<w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.5" punct="vg:6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="13.7">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="13.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9" punct="ps:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
					</lg>
					<closer>
						<note type="footnote" id="">La bémol majeur qui possède comme une sorte de motricité spéciale par ses quatre bémols bien équilibrés dans une large pulsation vitale.</note>
					</closer>
				</div></body></text></TEI>