<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1008" modus="cp" lm_max="12">
				<head type="main">SANGLOT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">R<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="2.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></w> <w n="2.9">d</w>’<w n="2.10" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pt">oi</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="3.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="3.9" punct="vg:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="4.7" punct="pt:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="5.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.4">pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="6.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.7" punct="pv:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>x</w> ;</l>
					<l n="7" num="2.3" lm="12"><w n="7.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="7.3">r<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="7.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="7.8" punct="vg:12">str<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="8" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="8.1">G<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rrh<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">br<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5" punct="pe:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe ps">ou</seg>s</w> !…</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rds</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.8" punct="pt:12">d<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="10.5">tr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="10.6" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
					<l n="12" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="12.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.4" punct="ps:8">d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
				</lg>
			</div></body></text></TEI>