<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BARBARESQUES</head><div type="poem" key="DLR382" modus="cm" lm_max="12">
					<head type="main">A LA LOUANGE DES PORTS DE MER</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.5" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</w>, <w n="1.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rts</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.8" punct="vg:9">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9" punct="vg">e</seg>r</w>, <w n="1.9">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rts</w> <w n="1.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="1.11">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>b<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="3.7" punct="vg:12">qu<seg phoneme="i" type="vs" value="1" rule="485" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="4" num="2.2" lm="12"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4" punct="vg:6">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="4.5">d</w>’<w n="4.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.9" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="5.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="5.8" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</w>,</l>
						<l n="6" num="3.2" lm="12"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3" punct="vg:5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="6.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="6.6" punct="vg:8">qu<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</w>, <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="6.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="4.2" lm="12"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="8.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>ts</w>, <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">c<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="8.10" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.5">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="9.8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="9.9">v<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="9.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>ts</w></l>
						<l n="10" num="5.2" lm="12"><w n="10.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="10.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="10.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w>-<w n="10.6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="10.7" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>ts</w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="12"><w n="11.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="11.2" punct="vg:2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rts</w>, <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="11.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="11.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="11.10">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="12" num="6.2" lm="12"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="12.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w> <w n="12.5"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1" lm="12"><w n="13.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="13.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rts</w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="13.4">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="13.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="13.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.8">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="13.10">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>s</w></l>
						<l n="14" num="7.2" lm="12"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="14.3" punct="vg:4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.8">h<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t</w> <w n="14.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="14.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12" punct="vg">è</seg>s</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1" lm="12"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3">ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="15.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="15.8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="16" num="8.2" lm="12"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="16.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.5">j<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="16.8" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1" lm="12"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">H<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="17.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="17.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="17.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <subst hand="RR" reason="analysis" type="phonemization"><add rend="hidden"><w n="17.8" punct="pt:11"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ch</w></add><del>H</del></subst>. <w n="17.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="444" place="12" punct="pt">O</seg></w>.</l>
						<l n="18" num="9.2" lm="12"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="18.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.5">l</w>’<w n="18.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="18.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="18.9">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="18.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-27" place="11">e</seg></w> <w n="18.11" punct="pt:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="pt">au</seg>t</w>.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1" lm="12"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="19.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="19.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="19.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="19.9" punct="pt:12">br<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="20" num="10.2" lm="12"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="20.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="20.5">d</w>’<w n="20.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="20.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="20.8" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1" lm="12"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.3">j</w>’<w n="21.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="21.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="21.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="21.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></w></l>
						<l n="22" num="11.2" lm="12"><w n="22.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.4" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg">e</seg>r</w>, <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="22.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="22.9" punct="pt:12">b<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1" lm="12"><w n="23.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="23.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="23.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.5">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="23.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="23.7">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="24" num="12.2" lm="12"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>th<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="24.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="24.7" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1" lm="12"><w n="25.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="25.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="25.3" punct="pe:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe">ou</seg>s</w> ! <w n="25.4">J</w>’<w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="25.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="25.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="25.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="25.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="25.10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l</w></l>
						<l n="26" num="13.2" lm="12"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="26.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="26.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="26.8" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>l</w>,</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1" lm="12"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="27.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="27.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="27.4">m</w>’<w n="27.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="5">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="27.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="27.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="27.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="27.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="27.10" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="28" num="14.2" lm="12"><w n="28.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="28.2" punct="vg:2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rts</w>, <w n="28.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="28.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="28.7">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="28.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="28.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="28.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="28.11"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					</lg>
				</div></body></text></TEI>