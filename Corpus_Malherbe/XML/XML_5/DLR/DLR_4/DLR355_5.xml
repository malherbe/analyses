<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR355" modus="cm" lm_max="12">
					<head type="main">CONQUÊTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="1.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="1.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rt</w> <w n="2.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="3.9">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></w></l>
						<l n="4" num="2.2" lm="12"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="4.9">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="4.10" punct="pt:12">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">aim</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">q<seg phoneme="wa" type="vs" value="1" rule="259" place="5">ua</seg>dr<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.7" punct="vg:12">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="3.2" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">s</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">B<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.9" punct="pt:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12">‒ <w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4" punct="vg:6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="6" punct="vg">oi</seg></w>, <w n="7.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.7">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l</w> <w n="7.8" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
						<l n="8" num="4.2" lm="12"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="8.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="8.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="8.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.10" punct="pi:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rmi<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></w> ?</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12"><w n="9.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="vg">oi</seg></w>, <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.4">h<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="5.2" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3" punct="vg:3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>ts</w>, <w n="10.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="10.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.10" punct="vg:12">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="12"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" punct="vg">en</seg>s</w>, <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="11.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g</w> <w n="11.9" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></w>,</l>
						<l n="12" num="6.2" lm="12"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="12.3" punct="vg:2">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="12.6" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>t</w>, <w n="12.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="9">y</seg>s</w> <w n="12.9">m</w>’<w n="12.10" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12" punct="pi">en</seg>t</w> ?</l>
					</lg>
				</div></body></text></TEI>