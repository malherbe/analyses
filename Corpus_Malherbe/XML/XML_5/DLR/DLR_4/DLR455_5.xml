<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AU PORT</head><div type="poem" key="DLR455" modus="cp" lm_max="12">
					<head type="main">DE RETOUR</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">R<seg phoneme="w" type="vs" value="1" rule="DLR455_1" place="5">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="1.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.9">s</w>’<w n="1.10"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2" punct="vg:2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">â</seg>ts</w>, <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.9">p<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg>s</w> <w n="2.10" punct="vg:12">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>ts</w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="3.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="4.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="4.5">d</w>’<w n="4.6" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rdi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.5">pr<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.6" punct="pt:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="6.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="10">um</seg>s</w> <w n="6.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="6.9" punct="pt:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rts</w>.</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">Fr<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="8" num="2.4" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="8.6" punct="pt:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rts</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="9.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.7">ph<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="10.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="10.4">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="10.8">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="11">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
						<l n="11" num="3.3" lm="8"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="12" num="3.4" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="12.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w>-<w n="13.2">t</w>-<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="13.8">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="14" num="4.2" lm="12"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.3" punct="vg:3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="14.4">l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rd</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ts</w> <w n="14.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="14.9" punct="vg:12">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>t</w>,</l>
						<l n="15" num="4.3" lm="8"><w n="15.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="4.4" lm="8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="16.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5" punct="pi:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pi">o</seg>t</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="17.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="17.7" punct="pt:12">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>lg<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="18" num="5.2" lm="12"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg></w> <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="18.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</w> <w n="18.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="18.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w> <w n="18.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="18.8">d</w>’<w n="18.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="18.10" punct="vg:12">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>st</w>,</l>
						<l n="19" num="5.3" lm="8"><w n="19.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.5">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="19.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="20" num="5.4" lm="8"><w n="20.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="20.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="u" type="vs" value="1" rule="292" place="6">aoû</seg>t</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7">l</w>’<w n="20.8" punct="pt:8">Ou<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>st</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="12"><w n="21.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="21.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="21.4" punct="vg:6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rt</w>, <w n="21.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="21.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.8"><seg phoneme="u" type="vs" value="1" rule="426" place="10">où</seg></w> <w n="21.9">l</w>’<w n="21.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="21.11" punct="pt:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="22" num="6.2" lm="12"><w n="22.1" punct="vg:3">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="2">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="22.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="22.3">r<seg phoneme="i" type="vs" value="1" rule="472" place="5">i</seg>i<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="22.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="22.7">ch<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="22.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>s</w></l>
						<l n="23" num="6.3" lm="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.5">r<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="24" num="6.4" lm="8"><w n="24.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="24.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6">y</seg>s</w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="24.5" punct="pt:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>