<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE FRANCE</head><div type="poem" key="DLR413" modus="cm" lm_max="12">
					<head type="main">A PORT-ROYAL</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="1.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="1.5">l</w>’<w n="1.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.10">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt</w>-<w n="1.11" punct="vg:12">R<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="2.6" punct="vg:12">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="10">u</seg><seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2" punct="ps:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="ps">on</seg>s</w>… <w n="3.3">J</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>dr<seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t</w> <w n="3.7" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:6">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg">en</seg>t</w>, <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="4.3" punct="vg:9">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg">e</seg>s</w>, <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="4.5" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">Fr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="5.7" punct="vg:12">J<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>n<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11">i</seg><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">tr<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="6.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="6.7">p<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="6.8" punct="pt:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2" punct="vg:4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.5">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="7.7" punct="vg:12">ch<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.6" punct="pt:12">V<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>ph<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5" punct="vg:8">Ju<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>, <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="9.8" punct="vg:12">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>t</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>t</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="10.6" punct="vg:9">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9" punct="vg">oin</seg>s</w>, <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="10.8" punct="pt:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</w>.</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.5" punct="vg:6">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</w>, <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="11.7" punct="pt:12">pl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="12.4">r<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="12.6" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>