<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR346" modus="cm" lm_max="12">
					<head type="main">ENSEIGNEMENT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="1.2" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="1.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="1.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="2.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="2.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="2.6">qu</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="2.10">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>squ</w>’<w n="2.11"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="2.12" punct="vg:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>r</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.6">S<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ph<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg></w> <w n="3.7" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="4.2" punct="vg:2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="5">î</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="4.9" punct="pt:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="5.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="5.5">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="6.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ts</w> <w n="6.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="6.10" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="7.6">m</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="7.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w> <w n="7.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="8.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="8.8" punct="ps:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
					</lg>
				</div></body></text></TEI>