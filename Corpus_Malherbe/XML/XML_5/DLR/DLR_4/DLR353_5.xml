<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR353" modus="cp" lm_max="12">
					<head type="main">DANS LES JARDINS</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="1.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="8"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.4" punct="pt:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1" lm="12"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="3.5">d</w>’<w n="3.6" punct="vg:9"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="vg">er</seg></w>, <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="3.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
							<l n="4" num="2.2" lm="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="4.4" punct="pt:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>cs</w>.</l>
						</lg>
						<lg n="3">
							<l n="5" num="3.1" lm="12"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ts</w> <w n="5.3" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.7" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="6" num="3.2" lm="8"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="6.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.5" punct="pt:8">pl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="7" num="4.1" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="7.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="7.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="7.9">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</w></l>
							<l n="8" num="4.2" lm="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</w>.</l>
						</lg>
						<lg n="5">
							<l n="9" num="5.1" lm="12"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2" punct="vg:3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="9.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="9.7" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="10" num="5.2" lm="8"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="10.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>ls</w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.6" punct="pt:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="11" num="6.1" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="11.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="11.5" punct="vg:9">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9" punct="vg">en</seg>t</w>, <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="11.7" punct="vg:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
							<l n="12" num="6.2" lm="8"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="12.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="12.6" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">où</seg></w>,</l>
						</lg>
						<lg n="7">
							<l n="13" num="7.1" lm="12"><w n="13.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="13.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>t</w> <w n="13.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="13.9">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="13.10">j<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="14" num="7.2" lm="8"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="14.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6" punct="ps:8">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="15" num="1.1" lm="12"><w n="15.1">J<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">m<seg phoneme="u" type="vs" value="1" rule="428" place="5">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.6" punct="vg:8">ch<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="15.8" punct="ps:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
							<l n="16" num="1.2" lm="12"><w n="16.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="16.5" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg">e</seg>s</w>, <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
							<l n="17" num="1.3" lm="12"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="17.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="17.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="17.8" punct="vg:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</w>,</l>
							<l n="18" num="1.4" lm="12"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="18.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="18.7">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="18.8" punct="pt:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="19" num="2.1" lm="12"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="19.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="19.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="19.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
							<l n="20" num="2.2" lm="12"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w>-<w n="20.4">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w>-<w n="20.6" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="vg">à</seg></w>, <w n="20.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="20.8"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="20.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="20.10" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
							<l n="21" num="2.3" lm="12"><w n="21.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4" punct="vg:6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">em</seg>ps</w>, <w n="21.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="21.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="21.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="21.8" punct="vg:12">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="22" num="2.4" lm="12"><w n="22.1">M</w>’<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="22.3">d<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="22.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="22.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="22.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="23" num="1.1" lm="12"><w n="23.1">L</w>’<w n="23.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="23.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="23.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="23.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="23.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="23.8">br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="23.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="24" num="1.2" lm="8"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="24.4" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rth<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>,</l>
							<l n="25" num="1.3" lm="12"><w n="25.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="25.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="25.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="25.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="25.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="25.9" punct="pt:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>x</w>.</l>
							<l n="26" num="1.4" lm="12"><w n="26.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="26.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="26.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="26.7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="26.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="26.9">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="26.10" punct="vg:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="27" num="1.5" lm="8"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="27.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in">i</seg>t</w> : « <w n="27.4">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="27.6" punct="pe:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>s</w> ! »</l>
						</lg>
						<lg n="2">
							<l n="28" num="2.1" lm="12"><w n="28.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.3" punct="pt:4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="pt">e</seg>r</w>. <w n="28.4">V<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="28.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.6" punct="pt:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</w>. <w n="28.7" punct="pe:10">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="28.8" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
							<l n="29" num="2.2" lm="8"><w n="29.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="29.4">d</w>’<w n="29.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
							<l n="30" num="2.3" lm="12"><w n="30.1">D</w>’<w n="30.2"><seg phoneme="œ" type="vs" value="1" rule="286" place="1">œ</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="30.3" punct="vg:4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="30.4">d</w>’<w n="30.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="30.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="y" type="vs" value="1" rule="d-3" place="8">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="30.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="30.8" punct="vg:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg>s</w>,</l>
							<l n="31" num="2.4" lm="12"><w n="31.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="31.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="31.5" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="31.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>ts</w> <w n="31.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="31.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="31.9">br<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="31.10" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</w>,</l>
							<l n="32" num="2.5" lm="8"><w n="32.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="32.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="32.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="32.6" punct="pt:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>