<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE FRANCE</head><div type="poem" key="DLR419" modus="cm" lm_max="12">
					<head type="main">PRESCIENCE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w>-<w n="1.4" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="1.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.6" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="2.2">br<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="2.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="3.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.7">pl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="3.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="3.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="3.10" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="4.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.9" punct="pi:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="5.4">m</w>’<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>r</w> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.8" punct="pt:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">M</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="6.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="6.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="6.9" punct="pt:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></w>.</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w>-<w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rps</w> <w n="7.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.9">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="8.8" punct="pi:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pi">eau</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3" punct="vg:4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>s</w>, <w n="9.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="9.8" punct="pt:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>rd</w>.</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.7">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.8" punct="dp:12">C<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="11.7">pl<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="12" num="3.4" lm="12"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.7" punct="pt:12">B<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>lth<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>