<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VI</head><head type="main_part">Religion</head><div type="poem" key="DLR1079" modus="cm" lm_max="12">
					<head type="main">Pascal</head>
					<head type="sub_2">HORIZONS, Fasquelle, 1905</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>l</w>, <w n="1.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="1.4">Sh<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>kespe<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="1.6" punct="vg:12">th<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.4">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="2.7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="3.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.8" punct="vg:8">C<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="3.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="3.10">d</w>’<w n="3.11"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="4.4">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">t</w>’<w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="3" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="6.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l</w> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="6.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</w></l>
						<l n="7" num="2.3" lm="12"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="354" place="1">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="7.6" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9" punct="vg">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg></w> <w n="7.8">t</w>’<w n="7.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">C<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="8.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="8.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.4" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="8.5" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>t</w>, <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg></w> <w n="8.7">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg></w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="8.9">l</w>’<w n="8.10" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">T</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="9.3" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="9.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l</w> <w n="9.5" punct="vg:6">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg">ai</seg></w>, <w n="9.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>l</w> <w n="9.7">l<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.8" punct="pe:12">chr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pe">en</seg></w> !</l>
						<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="10.2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">â</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="10.8" punct="vg:12">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="11.2" punct="pe:3">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pe in">an</seg>t</w> !’<w n="11.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">In</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.6" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">ch<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="12.5">ch<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>pt<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="13" num="4.2" lm="12"><w n="13.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>bstr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="13.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="i" type="vs" value="1" rule="493" place="11">y</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="14.10" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></w> !</l>
					</lg>
				</div></body></text></TEI>