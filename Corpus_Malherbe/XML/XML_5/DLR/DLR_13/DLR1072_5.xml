<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE V</head><head type="main_part">Intimité</head><div type="poem" key="DLR1072" modus="cm" lm_max="12">
					<head type="main">Le Bonheur</head>
					<head type="sub_2">HORIZONS, Fasquelle, 1905</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="1.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">br<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="1.8" punct="vg:12">y<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="2.6" punct="pt:12">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="3.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="3.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="3.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.10" punct="dp:12">n<seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="4" num="2.2" lm="12"><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="4.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.8">l</w>’<w n="4.9" punct="pv:12"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.8">d</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>x</w></l>
						<l n="6" num="3.2" lm="12"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="6.7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="6.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="6.9" punct="pv:12">m<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>ts</w> ;</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12"><w n="7.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.3" punct="vg:4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</w>, <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.5" punct="vg:6">fru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>ts</w>, <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="7.10">p<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="8" num="4.2" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w>-<w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="8.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="8.8" punct="pv:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12"><w n="9.1">S</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="9.3">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="9.5">s</w>’<w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="9.8">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>d</w> <w n="9.9"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="9.10" punct="vg:8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</w>, <w n="9.11">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="9.12">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.13">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="10" num="5.2" lm="12"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.8" punct="pv:12">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg></w> ;</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="12"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="11.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="11.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="12" num="6.2" lm="12"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="12.3">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rs</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">d</w>’<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="9">ue</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="12.9" punct="vg:12">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="13.7" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</w>,</l>
						<l n="14" num="7.2" lm="12"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="14.6">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="14.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="14.8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="14.9" punct="vg:12">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>rs</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1" lm="12"><w n="15.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="15.3" punct="vg:6">ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="15.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="15.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="15.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="15.7">d</w>’<w n="15.8" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="16" num="8.2" lm="12"><w n="16.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="16.3">d</w>’<w n="16.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="16.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.7" punct="vg:9"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="8">ou</seg><seg phoneme="i" type="vs" value="1" rule="476" place="9" punct="vg">ï</seg></w>, <w n="16.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="16.9" punct="vg:12">gr<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1" lm="12"><w n="17.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="17.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="17.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="17.5">ru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="17.9">d</w>’<w n="17.10" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</w>,</l>
						<l n="18" num="9.2" lm="12"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2" punct="vg:3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="18.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="18.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="18.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="18.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="18.9" punct="ps:12">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="ps">o</seg>rd</w>…</l>
					</lg>
				</div></body></text></TEI>