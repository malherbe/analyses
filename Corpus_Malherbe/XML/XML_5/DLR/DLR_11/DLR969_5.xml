<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR969" modus="cm" lm_max="10">
				<head type="main">BALLADE DES ROMBIÈRES</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rps</w> <w n="1.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="7">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3" punct="vg:4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>t</w>, <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="2.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="2.6" punct="vg:10">b<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</w>,</l>
					<l n="3" num="1.3" lm="10"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t</w>-<w n="3.4">Li<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="3.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="3.7">l</w>’<w n="3.8">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>st<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="4" num="1.4" lm="10"><w n="4.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">dr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="4.8" punct="vg:10">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</w>,</l>
					<l n="5" num="1.5" lm="10"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>cs</w> <w n="5.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w>-<w n="5.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="5.8" punct="pi:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pi">in</seg>s</w> ?</l>
					<l n="6" num="1.6" lm="10"><w n="6.1" punct="vg:1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rt</w>, <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="6.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="6.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="6.7" punct="vg:10">bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="7" num="1.7" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ss<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="7.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="7.4">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>nt</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.6" punct="pt:10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">ain</seg>s</w>.</l>
					<l n="8" num="1.8" lm="10">— <w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="8.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="8.7" punct="pe:10">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="10"><w n="9.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.7" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="10" num="2.2" lm="10"><w n="10.1">L</w>’<w n="10.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="10.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>t</w>, <w n="10.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="10.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.8" punct="vg:10">fr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10" punct="vg">ein</seg>s</w>,</l>
					<l n="11" num="2.3" lm="10"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="11.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.8" punct="vg:10">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="12" num="2.4" lm="10"><w n="12.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="12.4">m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="12.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rp<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>s</w></l>
					<l n="13" num="2.5" lm="10"><w n="13.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="13.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="13.6" punct="pt:10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pt">in</seg>s</w>.</l>
					<l n="14" num="2.6" lm="10"><w n="14.1" punct="vg:2">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2" punct="vg">ô</seg>t</w>, <w n="14.2" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="14.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</w> <w n="14.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="14.7" punct="vg:10">bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="15" num="2.7" lm="10"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="15.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="15.3">f<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="15.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="15.7" punct="pt:10">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">ain</seg>s</w>.</l>
					<l n="16" num="2.8" lm="10">— <w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="16.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="16.7" punct="pe:10">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="10"><w n="17.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="17.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="17.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="17.6" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>z<seg phoneme="o" type="vs" value="1" rule="DLR969_1" place="9">oo</seg>t<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
					<l n="18" num="3.2" lm="10"><w n="18.1" punct="vg:1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="18.4">s</w>’<w n="18.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>st<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="18.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="18.7" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="vg">ain</seg>s</w>,</l>
					<l n="19" num="3.3" lm="10"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="19.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="20" num="3.4" lm="10"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">si<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="20.6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="20.7" punct="pt:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pt">in</seg>s</w>.</l>
					<l n="21" num="3.5" lm="10"><w n="21.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="21.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="21.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="21.5">qu<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="21.6" punct="vg:10">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</w>,</l>
					<l n="22" num="3.6" lm="10"><w n="22.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="22.3">d</w>’<w n="22.4"><seg phoneme="o" type="vs" value="1" rule="432" place="4">o</seg>s</w> <w n="22.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="22.7">Pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
					<l n="23" num="3.7" lm="10"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="23.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="23.5">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="23.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="23.7" punct="pt:10">r<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10" punct="pt">ein</seg>s</w>.</l>
					<l n="24" num="3.8" lm="10">— <w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="24.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="24.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="24.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="24.7" punct="pe:10">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="10"><w n="25.1">D<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="25.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="25.3" punct="vg:4">fi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="25.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="25.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="25.6" punct="vg:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rbi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="26" num="4.2" lm="10"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="26.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>x</w> <w n="26.4" punct="vg:10">p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</w>,</l>
					<l n="27" num="4.3" lm="10"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="27.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="27.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="27.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ts</w> <w n="27.6" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10" punct="pt">ain</seg>s</w>.</l>
					<l n="28" num="4.4" lm="10">— <w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="28.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="28.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="28.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="28.7" punct="pe:10">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>