<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE A CHEVAL</head><div type="poem" key="DLR561" modus="cm" lm_max="10">
					<head type="main">CHASSE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rf</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="1.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w></l>
						<l n="2" num="1.2" lm="10"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="2.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="10"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.2" punct="vg:5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="vg">en</seg>s</w>, <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10"><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="4.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg>s</w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="4.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="4.8" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="5.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="5.7" punct="vg:10">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="2.2" lm="10"><w n="6.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="6.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="6.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t</w>, <w n="6.5">j</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="6.7">p<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="6.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="6.9" punct="pt:10">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>ts</w>.</l>
						<l n="7" num="2.3" lm="10"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="7.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w></l>
						<l n="8" num="2.4" lm="10"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="8.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="8.6" punct="pi:10">r<seg phoneme="wa" type="vs" value="1" rule="440" place="9">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rf</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="9.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.8" punct="vg:10">h<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
						<l n="10" num="3.2" lm="10"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="10.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g</w> <w n="10.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="10.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.9" punct="pt:10">h<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
						<l n="11" num="3.3" lm="10"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg>s</w> <w n="11.6" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="9">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg></w>.</l>
						<l n="12" num="3.4" lm="10"><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">m<seg phoneme="ø" type="vs" value="1" rule="399" place="2">eu</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3" punct="ps:5">gl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="ps">i</seg>t</w>… <w n="12.4">c</w>’<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="12.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>t</w>-<w n="12.9"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10"><w n="13.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu</w>’<w n="13.2" punct="vg:2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" punct="vg">un</seg></w>, <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="13.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5" punct="vg:5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.7">s<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="13.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="13.9" punct="pt:10">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pt">o</seg>r</w>.</l>
						<l n="14" num="4.2" lm="10"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="14.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.5">v<seg phoneme="y" type="vs" value="1" rule="457" place="5">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="14.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.9" punct="pi:10">c<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi">e</seg></w> ?</l>
						<l n="15" num="4.3" lm="10"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rf</w> <w n="15.3">n</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="15.6" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" punct="vg">à</seg></w>, <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rf</w> <w n="15.9">n</w>’<w n="15.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="15.11">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="15.12" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rt</w>,</l>
						<l n="16" num="4.4" lm="10"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rf</w> <w n="16.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rpr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10"><w n="17.1" punct="pe:2">T<seg phoneme="a" type="vs" value="1" rule="343" place="1">a</seg>ï<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="pe">au</seg>t</w> ! <w n="17.2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="17.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="17.8" punct="vg:8">v<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>, <w n="17.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="17.10" punct="pe:10">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pe">oi</seg>s</w> !</l>
						<l n="18" num="5.2" lm="10"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w>-<w n="18.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="18.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4" punct="pi:5">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" punct="pi">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ? <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w>-<w n="18.6">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="18.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="18.8" punct="pi:10">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi">e</seg></w> ?</l>
						<l n="19" num="5.3" lm="10"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="19.4" punct="vg:5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="vg">en</seg>t</w>, <w n="19.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="19.7" punct="vg:10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="10"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="20.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="20.7" punct="pt:10">b<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="10"><w n="21.1" punct="pe:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe">e</seg></w> ! <w n="21.2">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="21.3" punct="vg:5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>s</w>, <w n="21.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ds</w> <w n="21.6">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg>s</w> <w n="21.7" punct="pe:10">s<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg>s</w> !</l>
						<l n="22" num="6.2" lm="10"><w n="22.1" punct="vg:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="22.3" punct="pe:5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>l</w> ! <w n="22.4">G<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="22.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="22.6" punct="pe:10">lu<seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pe">i</seg></w> !</l>
						<l n="23" num="6.3" lm="10"><w n="23.1" punct="vg:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3" punct="pe:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="pe">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="23.6" punct="vg:10">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="9">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="24" num="6.4" lm="10"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="24.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="24.4" punct="vg:5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>s</w>, <w n="24.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="24.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="24.8" punct="pe:10">nu<seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pe">i</seg>t</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="10"><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rf</w> <w n="25.3" punct="vg:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">aî</seg>t</w>, <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="25.6">s</w>’<w n="25.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="26" num="7.2" lm="10"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="26.4">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="26.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="26.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="26.7" punct="pt:10">v<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>l</w>.</l>
						<l n="27" num="7.3" lm="10"><w n="27.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="27.4">d</w>’<w n="27.5"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="27.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="27.7">s</w>’<w n="27.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="27.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="27.10" punct="vg:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="28" num="7.4" lm="10"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="28.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l</w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="28.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="28.7" punct="pt:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>l</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="10"><w n="29.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="29.2">l</w>’<w n="29.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.4">d</w>’<w n="29.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="29.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="29.8" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="30" num="8.2" lm="10"><w n="30.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="30.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rf</w> <w n="30.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="30.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="30.6" punct="vg:10">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
						<l n="31" num="8.3" lm="10"><w n="31.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="31.2" punct="vg:5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="vg">en</seg>s</w>, <w n="31.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="32" num="8.4" lm="10"><w n="32.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="32.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="32.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="32.5">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg>s</w> <w n="32.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="32.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="32.8" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pi ps">er</seg></w> ?…</l>
					</lg>
				</div></body></text></TEI>