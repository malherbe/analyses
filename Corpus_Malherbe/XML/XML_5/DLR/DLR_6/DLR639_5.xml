<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR639" modus="cp" lm_max="12">
					<head type="main">SAINTES ALLIÉES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">D<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="1.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>l</w> <w n="1.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w>-<w n="2.6" punct="vg:8">d<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>il</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="3.4" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="3.5" punct="vg:9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="3.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="3.8" punct="vg:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">t</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8">d</w>’<w n="4.9" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="12" punct="pt">ue</seg>il</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.3" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="5.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w>-<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.6">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="6.7" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.5">gl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="7.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="7.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="7.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="7.9">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg>s</w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rts</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5" punct="dp:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="dp">an</seg>g</w> : <w n="8.6" punct="vg:7">Re<seg phoneme="i" type="vs" value="1" rule="497" place="7" punct="vg">i</seg>ms</w>, <w n="8.7" punct="vg:10">M<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg">e</seg>s</w>, <w n="8.8" punct="pt:12">L<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">N</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="9.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.7">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="8">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="9.8" punct="pi:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
						<l n="10" num="3.2" lm="8"><space unit="char" quantity="8"></space><w n="10.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>cs</w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>-<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="10.6" punct="pi:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi pi">a</seg>s</w> ? ?</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="11.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="11.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="11.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</w></l>
						<l n="12" num="3.4" lm="12"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="432" place="8">o</seg>s</w> <w n="12.7" punct="pi:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="vg:2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="13.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="13.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w>-<w n="13.10">v<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>s</w></l>
						<l n="14" num="4.2" lm="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="14.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="14.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="14.5" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="15" num="4.3" lm="12"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="15.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="15.8" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="16" num="4.4" lm="12"><w n="16.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="16.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="16.7" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.6" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="17.7">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="17.8" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="8"><space unit="char" quantity="8"></space><w n="18.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="18.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</w>,</l>
						<l n="19" num="5.3" lm="12"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5" punct="vg:6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>t</w>, <w n="19.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="19.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="19.9">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r</w> <w n="19.10" punct="vg:12">t<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="12"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="170" place="4">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="20.6" punct="pt:12">m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="12"><w n="21.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="21.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="21.6" punct="pt:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="22" num="6.2" lm="8"><space unit="char" quantity="8"></space><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="22.4">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="22.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="22.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="22.7" punct="pt:8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="8" punct="pt">om</seg>s</w>.</l>
						<l n="23" num="6.3" lm="12"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="23.2">r<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="23.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="23.5" punct="vg:9"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg">e</seg>s</w>, <w n="23.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="23.7">c<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</w></l>
						<l n="24" num="6.4" lm="12"><w n="24.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="24.2">c<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="24.4">cl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="24.7">gu<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="24.8" punct="pt:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="12">‒ <w n="25.1" punct="vg:3">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="25.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.4" punct="pe:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="pe">e</seg>l</w> ! <w n="25.5" punct="vg:9">S<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="25.6" punct="pe:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>ts</w> !</l>
						<l n="26" num="7.2" lm="8"><space unit="char" quantity="8"></space><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="26.5">d</w>’<w n="26.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="27" num="7.3" lm="12"><w n="27.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="27.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="27.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="27.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ds</w> <w n="27.7">l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>th<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>s</w></l>
						<l n="28" num="7.4" lm="12"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="28.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="28.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="28.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="28.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="28.8" punct="pt:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="12"><w n="29.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="29.2">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="29.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="29.4">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="29.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="29.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="29.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="29.8" punct="pv:12">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pv">e</seg>l</w> ;</l>
						<l n="30" num="8.2" lm="8"><space unit="char" quantity="8"></space><w n="30.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="30.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="30.3">s</w>’<w n="30.4"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="30.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="30.6" punct="pt:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
						<l n="31" num="8.3" lm="12"><w n="31.1" punct="vg:4">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="31.2">c</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="31.4" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="31.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="31.6" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>tr<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="32" num="8.4" lm="12"><w n="32.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="32.4">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="32.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="32.7" punct="pe:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pe">e</seg>l</w> !</l>
					</lg>
				</div></body></text></TEI>