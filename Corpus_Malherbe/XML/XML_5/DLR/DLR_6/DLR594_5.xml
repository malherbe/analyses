<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">ARRIÈRES-SAISONS</head><div type="poem" key="DLR594" modus="cp" lm_max="12">
					<head type="main">J’AI SI GRANDE AMITIÉ…</head>
					<lg n="1">
						<l n="1" num="1.1" lm="6"><space unit="char" quantity="12"></space><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w></l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.3" punct="pe:6">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="pe ps">e</seg>s</w> !… <w n="2.4">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="3" num="1.3" lm="5"><space unit="char" quantity="14"></space><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="3.5" punct="vg:5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</w>,</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2" punct="vg:3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="4.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="5" num="1.5" lm="8"><space unit="char" quantity="8"></space><w n="5.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="5.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="5.4">j</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.7" punct="pt:8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8" punct="pt">e</seg>d</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="6"><space unit="char" quantity="12"></space><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="7" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="7.7" punct="vg:8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</w>,</l>
						<l n="8" num="2.3" lm="5"><space unit="char" quantity="14"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="pt:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pt">o</seg>r</w>.</l>
						<l n="9" num="2.4" lm="12">‒ <w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="9.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3" punct="vg:6">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="9.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.5" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg in">e</seg></w>, ‒</l>
						<l n="10" num="2.5" lm="8"><space unit="char" quantity="8"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="10.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>