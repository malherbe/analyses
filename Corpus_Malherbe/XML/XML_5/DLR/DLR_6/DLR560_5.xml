<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE A CHEVAL</head><div type="poem" key="DLR560" modus="sp" lm_max="8">
					<head type="main">PEUR</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="4"><space unit="char" quantity="8"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="3" num="1.3" lm="4"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.4" punct="pt:4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="8"><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pi:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pi">eu</seg>r</w> ?</l>
						<l n="5" num="2.2" lm="4"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="5.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w></l>
						<l n="6" num="2.3" lm="4"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3" punct="pi:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="8"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="7.4">h<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t</w></l>
						<l n="8" num="3.2" lm="4"><space unit="char" quantity="8"></space><w n="8.1">D</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w></l>
						<l n="9" num="3.3" lm="4"><space unit="char" quantity="8"></space><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3" punct="pi:4">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="8"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="8">um</seg></w></l>
						<l n="11" num="4.2" lm="4"><space unit="char" quantity="8"></space><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="11.3">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w></l>
						<l n="12" num="4.3" lm="4"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3" punct="pi:4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.6">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="13.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu</w>’<w n="13.8" punct="ps:8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" punct="ps">un</seg></w>…</l>
						<l n="14" num="5.2" lm="4"><space unit="char" quantity="8"></space>‒ <w n="14.1" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>n<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" punct="vg">un</seg></w>,</l>
						<l n="15" num="5.3" lm="4"><space unit="char" quantity="8"></space><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">bu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.3" punct="pt:4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1" lm="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="16.3">qu</w>’<w n="16.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.6">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="16.7">l</w>’<w n="16.8" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="17" num="6.2" lm="4"><space unit="char" quantity="8"></space><w n="17.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="18" num="6.3" lm="4"><space unit="char" quantity="8"></space><w n="18.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="18.2" punct="pt:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1" lm="8"><w n="19.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="19.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="19.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
						<l n="20" num="7.2" lm="4"><space unit="char" quantity="8"></space><w n="20.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2" punct="vg:2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>g</w>, <w n="20.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w>-<w n="20.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="21" num="7.3" lm="4"><space unit="char" quantity="8"></space><w n="21.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">m</w>’<w n="21.3" punct="pi:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1" lm="8"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="22.3">qu</w>’<w n="22.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.6">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="22.7">l</w>’<w n="22.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</w>.</l>
						<l n="23" num="8.2" lm="4"><space unit="char" quantity="8"></space><w n="23.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="23.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="23.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">h<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="24" num="8.3" lm="4"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="24.2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rt</w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.4" punct="ps:4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="ps">ou</seg>r</w>…</l>
					</lg>
				</div></body></text></TEI>