<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">ARRIÈRES-SAISONS</head><div type="poem" key="DLR597" modus="cp" lm_max="12">
					<head type="main">TOURMENT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="1.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="1.9" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="428" place="11">ou</seg>ill<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">r<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2" punct="vg:3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="3.8">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.10" punct="pt:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>l</w>.</l>
						<l n="4" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w>-<w n="4.4">t</w>-<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.7" punct="pi:5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pi">oi</seg></w> ? <w n="4.8">Qu</w>’<w n="4.9"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="4.10"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w>-<w n="4.11">t</w>-<w n="4.12" punct="pi:8"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg>l</w> ?</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="5.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="5.10" punct="vg:12">n<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="3.2" lm="8"><space unit="char" quantity="8"></space><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w>-<w n="6.3" punct="pi:5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pi">i</seg>ls</w> ? <w n="6.4">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.5" punct="pi:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.5" punct="vg:6">s<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>rs</w>, <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w>-<w n="7.8" punct="vg:9"><seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>ls</w>, <w n="7.9"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w>-<w n="7.10" punct="pi:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg>s</w> ?</l>
						<l n="8" num="4.2" lm="8"><space unit="char" quantity="8"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4" punct="ps:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="ps">a</seg>s</w>… <w n="8.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="8.8" punct="ps:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ps">a</seg>s</w>…</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="9.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="9.12">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="5.2" lm="8"><space unit="char" quantity="8"></space><w n="10.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.4" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="12"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">s<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="11.8" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="12" num="6.2" lm="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="12.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="12.8" punct="pi:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>l</w> ?</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1" lm="12"><w n="13.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="13.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="14" num="7.2" lm="8"><space unit="char" quantity="8"></space><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="14.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1" lm="12"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="15.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.7">m</w>’<w n="15.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.9"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="15.10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="15.11">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="15.12">l</w>’<w n="15.13"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="15.14">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="15.15">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t</w></l>
						<l n="16" num="8.2" lm="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="16.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="16.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="16.6" punct="pt:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>t</w>.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1" lm="12"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="17.3">m</w>’<w n="17.4"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="17.5" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="17.6">pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="17.7" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="vg">en</seg>t</w>, <w n="17.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg></w>-<w n="17.9" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="9.2" lm="8"><space unit="char" quantity="8"></space><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="18.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.8">m</w>’<w n="18.9" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1" lm="12"><w n="19.1" punct="vg:2">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="19.2" punct="vg:3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3" punct="vg">o</seg>p</w>, <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p</w> <w n="19.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="19.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="19.9">f<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</w></l>
						<l n="20" num="10.2" lm="8"><space unit="char" quantity="8"></space><w n="20.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="20.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1" lm="12"><w n="21.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="21.2" punct="vg:3">P<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="3" punct="vg">oi</seg></w>, <w n="21.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s</w> <w n="21.4" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>rs</w>, <w n="21.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="21.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="21.7" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="22" num="11.2" lm="8"><space unit="char" quantity="8"></space><w n="22.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="22.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="22.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="ps">i</seg>s</w>-<w n="22.4" punct="ps:4">j<seg phoneme="ə" type="ee" value="0" rule="e-15">e</seg></w>… <w n="22.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="22.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="22.7" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>