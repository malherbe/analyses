<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FUMÉES</head><div type="poem" key="DLR191" modus="cp" lm_max="12">
					<head type="main">LE TRAVAIL NOCTURNE…</head>
					<lg n="1">
						<l n="1" num="1.1" lm="9"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>il</w> <w n="1.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w></l>
						<l n="2" num="1.2" lm="9"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.3">r<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="2.5" punct="pt:9">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="9"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w> <w n="3.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">s</w>’<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.8">fu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w></l>
						<l n="4" num="1.4" lm="9"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="4.2">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="4.3">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6" punct="vg:9">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg">e</seg></w>,</l>
						<l n="5" num="1.5" lm="9"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>fl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" punct="vg">en</seg>t</w>, <w n="5.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="5.5">l</w>’<w n="5.6" punct="pt:9"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="9"><w n="6.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg></w> ! <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">plu<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="6.5" punct="pe:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pe">eau</seg>x</w> ! <w n="6.6" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="pe">O</seg></w> ! <w n="6.7">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w></l>
						<l n="7" num="2.2" lm="9"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="7.8" punct="vg:9">r<seg phoneme="y" type="vs" value="1" rule="457" place="9">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.3" lm="9"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">l</w>’<w n="8.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rr<seg phoneme="y" type="vs" value="1" rule="457" place="9">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="10">e</seg></w></l>
						<l n="9" num="2.4" lm="8"><w n="9.1">S</w>’<w n="9.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="9.6" punct="vg:8">bi<seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="6">en</seg>f<seg phoneme="œ" type="vs" value="1" rule="304" place="7">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="10" num="2.5" lm="12"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="10.2" punct="vg:3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="10.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="10.7" punct="pe:12">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="11">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pe ps">an</seg></w> !…</l>
					</lg>
				</div></body></text></TEI>