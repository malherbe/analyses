<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR210" modus="cm" lm_max="12">
					<head type="main">J’AIME SONGER…</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="1.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="1.9">f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">l</w>’<w n="2.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.9" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>, <w n="2.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.11">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="3.3">s</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pt<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="11">ou</seg><seg phoneme="i" type="vs" value="1" rule="476" place="12">ï</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg in">œu</seg>r</w>, ‒ <w n="4.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="4.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rd</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">r<seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="11">ou</seg><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="5.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="5.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="5.9" punct="vg:12">j<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg in">ai</seg>s</w>, ‒</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="6.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.10">j</w>’<w n="6.11" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>s</w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2" punct="vg:3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.9" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l</w>’<w n="8.3" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="8.5" punct="vg:10">m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg">e</seg>s</w>, <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="415" place="11">ô</seg></w> <w n="8.7" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg>s</w> !</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="9.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs</w></l>
						<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="10.2" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="10.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.7" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</w>,</l>
						<l n="11" num="1.11" lm="12"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>p<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="12.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="12.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="12.8" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>