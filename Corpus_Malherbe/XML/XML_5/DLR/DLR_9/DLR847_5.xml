<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">L’ANGOISSE</head><div type="poem" key="DLR847" modus="cp" lm_max="12">
					<head type="main">NON CREDO</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="1.4" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="1.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ts</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7" punct="pt:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="3.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="3.8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="3.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="3.10">y<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="3.11" punct="vg:12">tr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.6">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="5.9">f<seg phoneme="o" type="vs" value="1" rule="434" place="9">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="6.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="6.8" punct="vg:8">n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="7.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="7.8">tr<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="7.9">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
						<l n="8" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="vg:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">f<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.6">v<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="9.7">l</w>’<w n="9.8" punct="vg:9"><seg phoneme="o" type="vs" value="1" rule="318" place="9" punct="vg">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="9.10" punct="vg:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>t</w>,</l>
						<l n="10" num="3.2" lm="8"><space unit="char" quantity="8"></space><w n="10.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w>-<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="11" num="3.3" lm="12"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>p<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="11.7">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="12" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">qu</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4" punct="vg">e</seg>st</w>, <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="12.8" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="13.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="13.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="13.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="13.9" punct="pt:12">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="14" num="4.2" lm="8"><space unit="char" quantity="8"></space><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="14.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.9" punct="pt:8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>t</w>.</l>
						<l n="15" num="4.3" lm="12"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pe">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> ! <w n="15.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">Ou</seg></w> <w n="15.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="15.7">l</w>’<w n="15.8" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>cu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8"><space unit="char" quantity="8"></space><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">d</w>’<w n="16.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.7" punct="pe:8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="17.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="17.5" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="17.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="17.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="17.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg></w>-<w n="17.9" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="8"><space unit="char" quantity="8"></space><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.5">j</w>’<w n="18.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></w>,</l>
						<l n="19" num="5.3" lm="12"><w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="19.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w> <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="19.7" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="8"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="20.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="20.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>