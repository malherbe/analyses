<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE</head><div type="poem" key="DLR786" modus="sm" lm_max="8">
					<head type="main">CONVERSATION</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8">‒ <w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="1.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.6" punct="vg:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>s</w>, <w n="1.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="2.6" punct="pi:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pi">oi</seg>r</w> ?</l>
						<l n="3" num="1.3" lm="8">‒ <w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>s</w> : « <w n="3.3">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="3.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">s</w>’<w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="4.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t</w> <w n="4.8" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8">« <w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t</w> <w n="5.5" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="6" num="2.2" lm="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rds</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="6.7" punct="vg:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="2.4" lm="8"><w n="8.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.3" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8">« <w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="9.7">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="10" num="3.2" lm="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="10.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="10.6" punct="pt:8">tr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
						<l n="11" num="3.3" lm="8"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.5" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="12" num="3.4" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">n</w>’<w n="12.5"><seg phoneme="e" type="vs" value="1" rule="354" place="5">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8">« <w n="13.1">L</w>’<w n="13.2" punct="vg:3"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="13.3">r<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.2">t</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.5" punct="pt:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
						<l n="15" num="4.3" lm="8"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="15.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="15.5">l</w>’<w n="15.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="16.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="17.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="17.5">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.6" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="ital"><w n="18.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w></hi> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <hi rend="ital"><w n="18.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</w></hi>.</l>
						<l n="19" num="5.3" lm="8"><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="19.3">l</w>’<w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="19.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="19.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w></l>
						<l n="20" num="5.4" lm="8"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="dp in">i</seg>t</w> : « <w n="20.4" punct="pe:8">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> ! »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8">« <w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="21.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w></l>
						<l n="22" num="6.2" lm="8"><w n="22.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>-<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ts</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.5" punct="pi:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</w> ?</l>
						<l n="23" num="6.3" lm="8"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="23.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w></l>
						<l n="24" num="6.4" lm="8"><w n="24.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="24.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="24.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.7" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8">« <w n="25.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="25.2">qu</w>’<w n="25.3"><seg phoneme="e" type="vs" value="1" rule="354" place="3">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.4">l</w>’<w n="25.5" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</w>,</l>
						<l n="26" num="7.2" lm="8"><w n="26.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="26.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="26.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="26.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.7" punct="pt:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="27" num="7.3" lm="8"><w n="27.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="27.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="27.4" punct="vg:8">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="28" num="7.4" lm="8"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="28.2" punct="vg:4">j<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="28.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="28.5" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rts</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="8">« <w n="29.1">L</w>’<w n="29.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="29.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="29.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="29.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="29.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="29.7">fl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="30" num="8.2" lm="8"><w n="30.1">S</w>’<w n="30.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="30.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="30.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></w>.</l>
						<l n="31" num="8.3" lm="8"><w n="31.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="31.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w>-<w n="31.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="31.5">P<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="32" num="8.4" lm="8"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">d</w>’<w n="32.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="32.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="32.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="32.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="32.7" punct="pi:8">h<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pi">au</seg>t</w> ? »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1" lm="8">‒ <w n="33.1">J</w>’<w n="33.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="33.3" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>t</w> : « <w n="33.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">Au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="a" type="vs" value="1" rule="194" place="7">e</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="34" num="9.2" lm="8"><w n="34.1">J</w>’<w n="34.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="34.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="34.4">r<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="34.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="34.7" punct="vg:8">l<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>,</l>
						<l n="35" num="9.3" lm="8"><w n="35.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="35.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="35.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="35.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="36" num="9.4" lm="8"><w n="36.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="36.2">l</w>’<w n="36.3" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="36.4" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="36.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="36.6" punct="dp:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="dp in">ai</seg>t</w> : « <w n="36.7" punct="pt:8">M<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></w> ».</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1" lm="8"><w n="37.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="37.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="37.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w>-<w n="37.4">t</w>-<w n="37.5" punct="pi:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="pi">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ? <w n="37.6"><seg phoneme="u" type="vs" value="1" rule="426" place="5">Où</seg></w> <w n="37.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>c</w> <w n="37.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w>-<w n="37.9">t</w>-<w n="37.10" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ? »</l>
					</lg>
				</div></body></text></TEI>