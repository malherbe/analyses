<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">SPECTRES</head><div type="poem" key="DLR828" modus="sp" lm_max="8">
					<head type="main">L’ABSENTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">î</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.5" punct="vg:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="4"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.2" punct="pi:4">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" punct="pi">ain</seg>s</w> ?</l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="4"><space unit="char" quantity="8"></space><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3" punct="pi:4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="pi">in</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="5.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="2.2" lm="4"><space unit="char" quantity="8"></space><w n="6.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.4" punct="vg:4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rps</w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.6" punct="vg:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rts</w>,</l>
						<l n="8" num="2.4" lm="4"><space unit="char" quantity="8"></space><w n="8.1">Fu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="8.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.4" punct="pt:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="9.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w></l>
						<l n="10" num="3.2" lm="4"><space unit="char" quantity="8"></space><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="pt:4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
						<l n="11" num="3.3" lm="8"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2" punct="vg:3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w></l>
						<l n="12" num="3.4" lm="4"><space unit="char" quantity="8"></space><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="12.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="12.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="12.4" punct="pt:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="13.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="13.5" punct="vg:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg">en</seg></w>, <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.7">n</w>’<w n="13.8"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="13.9">pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="13.10" punct="pt:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
						<l n="14" num="4.2" lm="4"><space unit="char" quantity="8"></space><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.4" punct="pt:4">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
						<l n="15" num="4.3" lm="8"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2" punct="vg:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="15.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3" punct="pt:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" punct="pt">en</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="17.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="17.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="18" num="5.2" lm="4"><space unit="char" quantity="8"></space><w n="18.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="18.2" punct="pt:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pt">i</seg>ts</w>.</l>
						<l n="19" num="5.3" lm="8"><w n="19.1" punct="vg:2">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="19.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="19.5">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="20" num="5.4" lm="4"><space unit="char" quantity="8"></space><w n="20.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="20.4" punct="pt:4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pt">i</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>