<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE</head><div type="poem" key="DLR789" modus="cp" lm_max="12">
					<head type="main">SONNERIE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><space unit="char" quantity="8"></space><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="1.3" punct="vg:3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="vg">en</seg>t</w>, <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.8" punct="vg:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-27" place="4">e</seg></w> <w n="2.3">h<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</w>.</l>
						<l n="3" num="1.3" lm="7"><space unit="char" quantity="10"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4" punct="vg:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>r</w>, <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.6" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="5"><space unit="char" quantity="14"></space><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4" punct="pi:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pi">o</seg>r</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.8"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.10" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">tr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.5" punct="vg:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>x</w>,</l>
						<l n="7" num="2.3" lm="8"><space unit="char" quantity="8"></space><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ts</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.8" punct="vg:8">v<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>x</w>,</l>
						<l n="8" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.5">l</w>’<w n="8.6" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.3" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="vg:6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg">e</seg>rf</w>, <w n="9.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="9.7">fu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>r</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.10" punct="vg:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</w>,</l>
						<l n="10" num="3.2" lm="8"><space unit="char" quantity="8"></space><w n="10.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.6" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="3.3" lm="8"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3" punct="vg:3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>r</w>, <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.5">s</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.8" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="12" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">tr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.6" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-27" place="4">e</seg></w> <w n="13.3">h<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6" punct="vg:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</w>,</l>
						<l n="14" num="4.2" lm="8"><space unit="char" quantity="8"></space><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="14.3" punct="vg:3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="vg">en</seg>t</w>, <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.8" punct="vg:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="15" num="4.3" lm="7"><space unit="char" quantity="10"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4" punct="vg:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>r</w>, <w n="15.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.6" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="5"><space unit="char" quantity="14"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="16.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4" punct="pi:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pi">o</seg>r</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="17.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rf</w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="17.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="17.9">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9">en</seg>s</w> <w n="17.10">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ph<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="18" num="5.2" lm="8"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</w>, <w n="18.4">r<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="18.5" punct="pi:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pi">en</seg>ts</w> ?</l>
						<l n="19" num="5.3" lm="8"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="19.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="19.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="19.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="5.4" lm="8"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="20.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="20.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.5">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="20.6" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pi">an</seg>ts</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="12"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="21.4" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg">oin</seg></w>, <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="21.7">n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="21.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="21.9" punct="vg:12">cu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="22" num="6.2" lm="8"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="22.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
						<l n="23" num="6.3" lm="8"><space unit="char" quantity="8"></space><w n="23.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="23.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="23.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="23.6" punct="vg:8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="24" num="6.4" lm="8"><space unit="char" quantity="8"></space><w n="24.1" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="24.3" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8"><space unit="char" quantity="8"></space><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="25.3" punct="vg:3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="vg">en</seg>t</w>, <w n="25.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="25.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="25.8" punct="vg:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="26" num="7.2" lm="8"><space unit="char" quantity="8"></space><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="26.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-27" place="4">e</seg></w> <w n="26.3">h<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="26.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</w>.</l>
						<l n="27" num="7.3" lm="7"><space unit="char" quantity="10"></space><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="27.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="27.4" punct="vg:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>r</w>, <w n="27.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="27.6" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="28" num="7.4" lm="5"><space unit="char" quantity="14"></space><w n="28.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="28.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.4" punct="pi:5">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pi">o</seg>r</w> ?</l>
					</lg>
				</div></body></text></TEI>