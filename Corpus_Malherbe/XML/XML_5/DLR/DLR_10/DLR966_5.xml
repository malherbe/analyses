<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR966" modus="cp" lm_max="10">
					<head type="number">XIV</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>rs</w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.5">l</w>’<w n="1.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="1.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.9">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="1.10" punct="vg:10">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>r</w>,</l>
						<l n="2" num="1.2" lm="5"><space unit="char" quantity="10"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4" punct="pt:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="5"><space unit="char" quantity="10"></space><w n="3.1" punct="pi:1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="pi">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ? <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.4" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.5" punct="pi:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></w> ?</l>
						<l n="4" num="1.4" lm="10"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">cl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="4.8" punct="pt:10">m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1" punct="pi:2">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pi ps">i</seg>r</w> ?… <w n="5.2">C</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="5.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="5.8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w>-<w n="5.9" punct="pt:10">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
						<l n="6" num="2.2" lm="5"><space unit="char" quantity="10"></space><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w></l>
						<l n="7" num="2.3" lm="5"><space unit="char" quantity="10"></space><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w></l>
						<l n="8" num="2.4" lm="5"><space unit="char" quantity="10"></space><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="8.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.4" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="vg">e</seg>il</w>,</l>
						<l n="9" num="2.5" lm="10"><w n="9.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="9.8" punct="pe:10">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1" lm="10"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4" punct="pt:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="pt">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <hi rend="ital"><w n="10.5" punct="pt:9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt">e</seg></w></hi>. <w n="10.8" punct="pi:10">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="10" punct="pi">oi</seg></w> ?</l>
						<l n="11" num="3.2" lm="5"><space unit="char" quantity="10"></space>— <w n="11.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="11.2" punct="pt:2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pt">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="11.3" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="12" num="3.3" lm="5"><space unit="char" quantity="10"></space><w n="12.1" punct="tc:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="ti">a</seg></w> — <w n="12.2" punct="pe:2">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>rs</w> ! <w n="12.3">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.4">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5" punct="pt:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="13" num="3.4" lm="10">— <w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">cl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="13.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="13.9" punct="pt:10">t<seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="pt">oi</seg></w>.</l>
					</lg>
				</div></body></text></TEI>