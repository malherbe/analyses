<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR943" modus="sp" lm_max="8">
				<head type="main">NIRVANA</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">n<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="1.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">n</w>’<w n="2.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">aî</seg>t</w>,</l>
					<l n="3" num="1.3" lm="4"><space unit="char" quantity="10"></space><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w></l>
					<l n="4" num="1.4" lm="3"><space unit="char" quantity="10"></space><w n="4.1" punct="vg:3">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">aî</seg>t</w>,</l>
					<l n="5" num="1.5" lm="8"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>r</w> <w n="6.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="vg:6">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="6.7" punct="vg:8">l<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>,</l>
					<l n="7" num="1.7" lm="3"><space unit="char" quantity="10"></space><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="7.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>,</l>
					<l n="8" num="1.8" lm="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="8.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">d</w>’<w n="8.6">h<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>pn<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="9" num="1.9" lm="2"><space unit="char" quantity="10"></space><w n="9.1" punct="vg:2">M<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="8"><w n="10.1" punct="vg:2">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg>s</w>, <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.5">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w></l>
					<l n="11" num="1.11" lm="2"><space unit="char" quantity="10"></space><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="11.2" punct="pt:2">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2" punct="pt">en</seg></w>.</l>
				</lg>
			</div></body></text></TEI>