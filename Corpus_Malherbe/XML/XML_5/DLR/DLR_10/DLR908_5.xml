<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR908" modus="sm" lm_max="8">
				<head type="main">À LA NUIT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2" punct="vg:4">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.4" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>x</w>, <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="vg:8">f<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:2">P<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="3.2" punct="vg:5">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="5" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w>, <w n="3.3" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l</w> <w n="4.2">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="7">en</seg>s</w> <w n="4.7" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="5.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <subst hand="RR" reason="analysis" type="phonemization"><del>X</del><add rend="hidden"><w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ks</w></add></subst> <w n="5.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="2.2" lm="8"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="6.6" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></w>,</l>
					<l n="7" num="2.3" lm="8"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4" punct="vg:5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="vg:8">pl<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>t</w>,</l>
					<l n="8" num="2.4" lm="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="8.4" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8"><w n="9.1" punct="vg:1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>t</w>, <w n="9.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="9.3">qu<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w></l>
					<l n="10" num="3.2" lm="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="10.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="10.7" punct="vg:8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="11" num="3.3" lm="8"><w n="11.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="11.3">s</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="11.7" punct="vg:8">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></w>,</l>
					<l n="12" num="3.4" lm="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="12.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8"><w n="13.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
					<l n="14" num="4.2" lm="8"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="15" num="4.3" lm="8"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="15.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="4.4" lm="8"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="16.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</w>,</l>
				</lg>	
				<lg n="5">
					<l n="17" num="5.1" lm="8"><w n="17.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="18" num="5.2" lm="8"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="18.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>t</w>,</l>
					<l n="19" num="5.3" lm="8"><w n="19.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="19.2" punct="vg:3">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="19.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>ll<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="19.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="19.6" punct="vg:8">f<seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></w>,</l>
					<l n="20" num="5.4" lm="8"><w n="20.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="20.4" punct="vg:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8"><w n="21.1" punct="vg:1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>t</w>, <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">t</w>’<w n="21.4" punct="pe:4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="21.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="21.8" punct="vg:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>,</l>
					<l n="22" num="6.2" lm="8"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">t</w>’<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="22.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.8" punct="vg:8">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="23" num="6.3" lm="8"><w n="23.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="23.2" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="23.3" punct="vg:4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.4" punct="vg:7"><seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg">e</seg></w>, <w n="23.5" punct="vg:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="24" num="6.4" lm="8"><w n="24.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="24.3">d</w>’<w n="24.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="354" place="4">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="24.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.6" punct="pe:8">l<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></w> !</l>
				</lg>
			</div></body></text></TEI>