<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MORT</head><div type="poem" key="DLR292" modus="cm" lm_max="12">
					<head type="main">A CEUX QUI CRIENT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="1.3">cr<seg phoneme="i" type="vs" value="1" rule="469" place="3">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="1.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="1.9">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="2.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="2.8" punct="vg:12">v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">c<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w>-<w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp">i</seg>s</w> : <w n="3.7">S<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="3.9">v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="4.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="4.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="4.11" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="5.7">n</w>’<w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="5.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="5.10" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</w> !</l>
						<l n="6" num="2.2" lm="12"><w n="6.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="6.2">j</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="6.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.9">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="6.10">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="6.11">l</w>’<w n="6.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="7" num="2.3" lm="12"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3" punct="pv:3">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pv">o</seg>rt</w> ; <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="pt:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pt">e</seg></w>. <w n="8.3">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="8.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="8.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="8.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="9.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="9.6">d</w>’<w n="9.7" punct="dp:9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="dp">e</seg></w> : <w n="9.8">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.9">v<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="12">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="10.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="10.4" punct="vg:6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg">au</seg>x</w>, <w n="10.5">l</w>’<w n="10.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="11" num="3.3" lm="12"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="11.3" punct="vg:3">p<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>rs</w>, <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="11.7">n</w>’<w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.9">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9">en</seg></w> <w n="11.10">qu</w>’<w n="11.11"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w>-<w n="11.12" punct="pi:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="12" num="3.4" lm="12"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3" punct="vg:6">br<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="12.4">n<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ls</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rts</w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.7" punct="pt:12">v<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="12" punct="pt">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="13.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="13.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="13.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="13.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ds</w> <w n="13.10">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="13.11">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="13.12">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="13.13" punct="pt:12">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="14" num="4.2" lm="12"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="14.8" punct="dp:8">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="dp">eu</seg>rt</w> : <w n="14.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="14.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.11" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>s</w>.</l>
						<l n="15" num="4.3" lm="12"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.5" punct="vg:4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.7" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="15.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="15.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="15.10">di<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="15.11">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="15.12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w> <w n="15.13" punct="ps:12">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg>s</w>…</l>
						<l n="16" num="4.4" lm="12"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="16.2" punct="pe:3">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3" punct="pe">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4">O</seg></w> <w n="16.4" punct="pe:6">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> ! <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="444" place="7">O</seg></w> <w n="16.6" punct="pe:9">D<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="pe ps ti">i</seg>r</w> !… — <w n="16.7">N<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="16.8" punct="pe:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg>s</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="17.3">c<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="17.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="17.5">cr<seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="17.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="17.10">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="18" num="5.2" lm="12"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="18.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">s</w>’<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="18.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="18.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="18.8" punct="pe:12">v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></w> !</l>
						<l n="19" num="5.3" lm="12"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="19.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>s</w> : <w n="19.5" punct="vg:6">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="19.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="19.8">v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="20" num="5.4" lm="12"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">c</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.5">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="20.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="20.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="20.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="20.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="20.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="20.12" punct="pe:12">V<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>