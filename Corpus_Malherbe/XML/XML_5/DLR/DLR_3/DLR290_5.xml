<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MORT</head><div type="poem" key="DLR290" modus="cm" lm_max="12">
					<head type="main">CRI</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="ps:1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="ps">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="1.2" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">A</seg>h</w> ! <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">qu</w>’<w n="1.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.11" punct="pe:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="2.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>squ</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="2.8">b<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w>-<w n="3.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="3.10" punct="pe:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps">e</seg></w> !…</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t</w></l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.5">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="5.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="5.10">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="1.6" lm="12"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.3" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="6.5">qu</w>’<w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="6.9" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</w>, <w n="6.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.11" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="7.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="7.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="7.10" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1" lm="12"><w n="8.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rt</w>, <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="8.6">squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rs</w> <w n="8.8" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="vg">à</seg></w>,</l>
						<l n="9" num="2.2" lm="12"><w n="9.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="9.10" punct="vg:10">s<seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="vg">oi</seg></w>, <w n="9.11">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="9.12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="2.3" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="10.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w>-<w n="10.9" punct="pe:12">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps">e</seg>s</w> !…</l>
						<l n="11" num="2.4" lm="12">—<w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="pi:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pi ps">ou</seg>s</w> ?… <w n="11.3" punct="pi:3">N<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pi ps">ou</seg>s</w> ?…<w n="11.4">Qu</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w>-<w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="11.7">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="11.9">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="11.10" punct="pi:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi ps">i</seg>rs</w> ?…</l>
						<l n="12" num="2.5" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg></w> <w n="12.2" punct="pe:1">h</w> ! <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.10">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</w></l>
						<l n="13" num="2.6" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="13.3" punct="vg:3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>t</w>, <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="13.6" punct="pe:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe">ou</seg>t</w> ! <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="13.11" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="2.7" lm="12"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="14.6" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>, <w n="14.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>t</w>-<w n="14.8" punct="pi:12"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps">e</seg></w> ?…</l>
					</lg>
				</div></body></text></TEI>