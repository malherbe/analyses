<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR256" modus="cm" lm_max="12">
					<head type="main">BERGES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="1.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</w>, <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6" punct="vg:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</w>, <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</w> <w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>x</w> <w n="1.10" punct="vg:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">gu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.8" punct="vg:12">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">H<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="3.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="3.7">fr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="4.6">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t</w> <w n="4.8" punct="pt:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="5.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="5.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ds</w> <w n="5.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12" punct="vg">ein</seg>ts</w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="6.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.9" punct="vg:12">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1" punct="vg:2">Fl<seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>nt</w>, <w n="7.2">l</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="7.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.9">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="7.11" punct="vg:12">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="8.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="8.7" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.5" punct="vg:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="9.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="9.8">d</w>’<w n="9.9"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.10">b<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2" punct="vg:2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="10.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="10.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="7">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="10.6">br<seg phoneme="y" type="vs" value="1" rule="445" place="9">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="10.8" punct="vg:12">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>ls</w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.3" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="11.6">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="11.8" punct="vg:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>ls</w>,</l>
						<l n="12" num="3.4" lm="12"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.8" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rg<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="13.9"><seg phoneme="ø" type="vs" value="1" rule="247" place="11">œu</seg>fs</w> <w n="13.10" punct="vg:12">d<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</w>,</l>
						<l n="14" num="4.2" lm="12"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.9">fr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="14.10" punct="vg:12">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="15" num="4.3" lm="12"><w n="15.1">D</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.3" punct="vg:4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ls</w>, <w n="15.4">d</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="15.7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>ls</w> <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.9">d</w>’<w n="15.10"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.11" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>rs</w>,</l>
						<l n="16" num="4.4" lm="12"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">l</w>’<w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="16.10">l</w>’<w n="16.11"><seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="16.12">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="16.13" punct="pt:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>