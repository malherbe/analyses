<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR242" modus="cp" lm_max="12">
					<head type="main">AURORE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">l</w>’<w n="1.3" punct="pt:4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt">e</seg></w>. <w n="1.4">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>qs</w> <w n="1.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="1.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.9">l</w>’<w n="1.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="2.2">fr<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="2.5">s</w>’<w n="2.6" punct="vg:9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="y" type="vs" value="1" rule="457" place="9" punct="vg">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="3.8">cl<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="3.9" punct="pv:12">n<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ts</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="4.5">s</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="4.10">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.11" punct="vg:12">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="5.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="5.8" punct="pt:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="12"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="6.5">cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="7" num="2.2" lm="12"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="7.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rs</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.9" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</w>,</l>
						<l n="8" num="2.3" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="8.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.8">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="9" num="2.4" lm="12"><w n="9.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6" punct="dp:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="dp">e</seg>l</w> :</l>
						<l n="10" num="2.5" lm="12"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="10.7" punct="pe:12">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pe">e</seg>l</w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="12"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="11.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="11.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.7">n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="11.9" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</w>,</l>
						<l n="12" num="3.2" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.3" punct="vg:6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w></l>
						<l n="13" num="3.3" lm="12"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="13.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>xi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="13.6" punct="vg:12">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="14" num="3.4" lm="12"><w n="14.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>x</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.7">l</w>’<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.10">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="14.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="15" num="3.5" lm="12"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w>-<w n="15.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="15.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="15.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>il</w></l>
						<l n="16" num="3.6" lm="12"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="16.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="16.7">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="16.8" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>