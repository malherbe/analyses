<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR721" modus="sm" lm_max="8">
				<head type="main">Le Loup</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>p</w>, <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">gr<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="1.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p</w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="2.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>ps</w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="4.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l</w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="4.6">s</w>’<w n="4.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="7">em</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p</w> <w n="5.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="5.4">c</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</w></l>
					<l n="6" num="2.2" lm="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="6.6" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="7" num="2.3" lm="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.7" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="8" num="2.4" lm="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.4" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.7" punct="ps:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
					<l n="10" num="3.2" lm="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.5">gr<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="10.6" punct="vg:6">l<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>ps</w>, <w n="10.7">c</w>’<w n="10.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.9" punct="vg:8">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></w>,</l>
					<l n="11" num="3.3" lm="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="11.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="11.7" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="vg">ê</seg>t</w>,</l>
					<l n="12" num="3.4" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.5" punct="pt:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>