<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">MÉDITATIONS</head><div type="poem" key="DLR700" modus="cp" lm_max="12">
					<head type="main">AU-DELÀ</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg></w>-<w n="1.3" punct="vg:3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="1.4">l</w>’<w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w>-<w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="1.11" punct="vg:12">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="vg:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>s</w>, <w n="2.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.5" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="3.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="3.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg></w>, <w n="3.5">c</w>’<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ci<seg phoneme="ɛ" type="vs" value="1" rule="366" place="9">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.9" punct="vg:12">j<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="5.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="5.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="5.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="5.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>rs</w> <w n="5.8">d</w>’<w n="5.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="6.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2" punct="vg:3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="6.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4" punct="vg:5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" punct="vg">oin</seg>s</w>, <w n="6.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ds</w> <w n="6.7" punct="pt:8">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>xqu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="7.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="8.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="8.8">d<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>il</w> <w n="8.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="9.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.7" punct="vg:9">j<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" punct="vg">in</seg>s</w>, <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.9" punct="vg:12">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="8"><space unit="char" quantity="8"></space><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7" punct="vg:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="11.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="11.3">l</w>’<w n="11.4" punct="pt:6">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pt">e</seg>r</w>. <w n="11.5">C</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="11.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</w>,</l>
						<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="12.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="13.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="13.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.7">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="13.8">f<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg>s</w></l>
						<l n="14" num="4.2" lm="8"><space unit="char" quantity="8"></space><w n="14.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="14.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="14.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="15" num="4.3" lm="12"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="15.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="15.8" punct="vg:12">d<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="12"><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="16.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="16.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="16.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="16.9">m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg>s</w></l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="17.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="17.4">c</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="17.6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="17.7" punct="pt:9">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt">e</seg></w>. <w n="17.8">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="17.9">n</w>’<w n="17.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="17.11">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</w></l>
						<l n="18" num="5.2" lm="8"><space unit="char" quantity="8"></space><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2" punct="vg:2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rt</w>, <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4">n<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="18.5">qu</w>’<w n="18.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="18.7" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="19" num="5.3" lm="12"><w n="19.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="19.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="19.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg></w>, <w n="19.5">c</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="19.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="19.8">qu</w>’<w n="19.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="19.10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="20" num="5.4" lm="12"><w n="20.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="20.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="20.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="20.4">c</w>’<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="20.6" punct="pt:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pt">oi</seg></w>. <w n="20.7">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.8">n</w>’<w n="20.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="20.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="20.11">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="20.12" punct="pt:12">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>