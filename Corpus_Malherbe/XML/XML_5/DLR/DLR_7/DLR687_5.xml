<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A MES SŒURS</head><div type="poem" key="DLR687" modus="cp" lm_max="12">
					<head type="main">A MA SŒUR L’AÎNÉE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="249" place="2" punct="vg">œu</seg>r</w>, <w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="1.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ts</w>-<w n="1.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ</w>’<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="2.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="2.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="2.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="2.7">ch<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3" punct="vg:3">qu<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.5" punct="vg:8">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>ts</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="?" type="va" value="1" rule="162" place="2">en</seg>t</w> <w n="4.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="4.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.9">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>ts</w>, <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w>-<w n="5.6" punct="vg:10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" punct="vg">e</seg>s</w>, <w n="5.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.8" punct="pe:12">s<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pe">œu</seg>r</w> !</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.7" punct="pt:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="7" num="2.3" lm="8"><space unit="char" quantity="8"></space><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4">om</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="7.8" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="8.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w>-<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="8.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="8.9">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r</w> <w n="8.10" punct="vg:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>r</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">qu</w>’<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.4" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="9.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="9.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="9.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.9">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="9.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.11" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="10.7" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="11" num="3.3" lm="8"><space unit="char" quantity="8"></space><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="11.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="11.7" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="12.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="12.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="12.9" punct="pt:12"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>