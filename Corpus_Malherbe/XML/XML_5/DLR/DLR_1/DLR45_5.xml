<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES I</head><div type="poem" key="DLR45" modus="cm" lm_max="12">
					<head type="main">LA PROMISE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>th<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.3">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="1.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="1.6" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>ts</w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="415" place="11">ô</seg></w> <w n="1.8" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg>s</w> !</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="œ" type="vs" value="1" rule="249" place="1">Œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="4">ue</seg>il</w> <w n="2.4">qu</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="2.8" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="3.8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="3.9" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="4.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ts</w> <w n="4.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="4.9" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="6.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.9"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="6.10" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.4">g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.5" punct="vg:6">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg">ain</seg></w>, <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.9">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="8.6">v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w> <w n="8.7">s</w>’<w n="8.8"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="8.9" punct="pt:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="9.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="9.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="9.9">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="9.10">b<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="10.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="10.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="10.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="10.8" punct="pv:12">fi<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>vr<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="11.6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="11.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="12" num="3.4" lm="12"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>s</w> <w n="12.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="12.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.9" punct="pv:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg>s</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6">n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.7" punct="pv:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="14" num="4.2" lm="12"><w n="14.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>ts</w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rps</w> <w n="14.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="14.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="14.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.10" punct="vg:12">d<seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="vg">eu</seg>il</w>,</l>
						<l n="15" num="4.3" lm="12"><w n="15.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="15.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="15.7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="15.8" punct="vg:10">br<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</w>, <w n="15.9">g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="16" num="4.4" lm="12"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">t<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="16.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="16.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="16.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.9" punct="pe:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="12" punct="pe">ue</seg>il</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="12"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="17.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="17.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="17.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="18" num="5.2" lm="12"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t</w> <w n="18.7">qu</w>’<w n="18.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="18.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="18.10" punct="vg:12">d<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>t</w>,</l>
						<l n="19" num="5.3" lm="12"><w n="19.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="19.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="19.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="5">en</seg>s</w> <w n="19.6" punct="vg:6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg">oin</seg>t</w>, <w n="19.7"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="19.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="19.9" punct="pe:12">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="20" num="5.4" lm="12"><w n="20.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="20.3">t</w>’<w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="20.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="20.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="20.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r</w> <w n="20.9" punct="vg:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="12"><w n="21.1" punct="vg:1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>r</w>, <w n="21.2">d</w>’<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="21.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.6">si<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="21.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="22" num="6.2" lm="12"><w n="22.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="22.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="22.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="22.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg></w>,</l>
						<l n="23" num="6.3" lm="12"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="23.3">sp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="23.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="23.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="24" num="6.4" lm="12"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="24.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="24.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="24.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="24.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w> <w n="24.8" punct="pe:12">n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>t</w> !</l>
					</lg>
				</div></body></text></TEI>