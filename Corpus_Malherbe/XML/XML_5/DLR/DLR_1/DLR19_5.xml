<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR19" modus="cm" lm_max="12">
					<head type="main">PROMENADE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:3">L<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="1.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t</w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="3.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="3.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="3.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="3.7" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>t</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="4.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="4.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="4.7" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1" punct="vg:3">L<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="5.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="5.8" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="12"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.7"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="6.9" punct="pt:12">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="7" num="2.2" lm="12"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2" punct="vg:3">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="7.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6">cl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="7.8" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>s</w>,</l>
						<l n="8" num="2.3" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w>-<w n="8.6" punct="vg:12">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>s</w>,</l>
						<l n="9" num="2.4" lm="12"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="9.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="9.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="9.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="9.8" punct="pt:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="10.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>cs</w> <w n="10.7" punct="vg:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rts</w>,</l>
						<l n="11" num="3.2" lm="12"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="11.4" punct="vg:6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>ill<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="11.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="11.8" punct="vg:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rts</w>,</l>
						<l n="12" num="3.3" lm="12"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="12.7">f<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="13" num="3.4" lm="12"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="13.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ts</w> <w n="13.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</w></l>
						<l n="14" num="3.5" lm="12"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>il</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="14.7" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>f<seg phoneme="i" type="vs" value="1" rule="483" place="11">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pt">en</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1" lm="12"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="15.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.8" punct="pv:9">fr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9" punct="pv">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="15.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="15.11">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="16" num="4.2" lm="12"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>ds</w> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="16.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="16.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="16.9" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg>s</w> !</l>
						<l n="17" num="4.3" lm="12"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">l</w>’<w n="17.3" punct="vg:3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="17.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="17.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="17.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="17.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="17.8"><seg phoneme="i" type="vs" value="1" rule="497" place="9">y</seg></w> <w n="17.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</w></l>
						<l n="18" num="4.4" lm="12"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="18.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="18.4" punct="vg:4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="18.7" punct="vg:8">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>c</w>, <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="18.9">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="18.10" punct="pt:12">gr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="19" num="5.1" lm="12"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.6" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="19.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="19.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="19.10">t<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="19.11" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="5.2" lm="12"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="20.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="20.4">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="20.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="20.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="21" num="5.3" lm="12"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="21.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="21.4">d</w>’<w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="21.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="21.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="8">e</seg></w> <w n="21.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="21.9" punct="pv:12">r<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="22" num="5.4" lm="12"><w n="22.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.5">l</w>’<w n="22.6" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" punct="vg">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="22.7"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="22.8" punct="pe:8">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="22.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="22.10">t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="22.11">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="23" num="5.5" lm="12"><w n="23.1">D<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w>-<w n="23.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="23.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="23.7" punct="ps:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
					</lg>
				</div></body></text></TEI>