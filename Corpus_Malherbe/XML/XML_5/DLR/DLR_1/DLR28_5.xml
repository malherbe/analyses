<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME ET LA MER</head><div type="poem" key="DLR28" modus="cm" lm_max="10">
					<head type="main">JE SUIS LA HANTEUSE…</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="1.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="2" num="1.2" lm="10"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">s</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="2.6" punct="vg:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>ts</w>,</l>
						<l n="3" num="1.3" lm="10"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="3.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="3.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="3.6"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="4" num="1.4" lm="10"><w n="4.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">cr<seg phoneme="i" type="vs" value="1" rule="469" place="4">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="4.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="4.9" punct="pt:10">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>cs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="497" place="9">y</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="6" num="2.2" lm="10"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w></l>
						<l n="7" num="2.3" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2" punct="vg:2"><seg phoneme="u" type="vs" value="1" rule="426" place="2" punct="vg">où</seg></w>, <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="7.4">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">d</w>’<w n="7.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg">e</seg></w>, <w n="7.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8">h<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="8" num="2.4" lm="10"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.8">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="8.9" punct="ps:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="ps">a</seg>s</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><w n="9.1" punct="vg:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="vg">O</seg>h</w>, <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3" punct="pe:3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="pe">e</seg>r</w> ! <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5" punct="pe:5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="pe">e</seg>r</w> ! <w n="9.6">T<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="9.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="8">e</seg>s</w> <w n="9.9"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="10"><w n="10.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="10.2">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="10.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="10.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="10.8" punct="vg:10">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r</w>,</l>
						<l n="11" num="3.3" lm="10"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.7">s</w>’<w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.10" punct="vg:10">cl<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.4" lm="10"><w n="12.1">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rc<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.6" punct="pe:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pe">oi</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>