<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR72" modus="cp" lm_max="12">
					<head type="main">BRUME</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3" punct="vg:4">sc<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>lpt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w>-<w n="1.8" punct="vg:12">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>tt<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="2.5">gr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="3" num="1.3" lm="3"><space unit="char" quantity="18"></space><w n="3.1" punct="pt:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="12"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3" punct="vg:3">br<seg phoneme="y" type="vs" value="1" rule="453" place="3" punct="vg">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.5">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>fl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="4.7">f<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="4.8">r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="5" num="2.2" lm="12"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:6">B<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.7">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="11">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="6" num="2.3" lm="3"><space unit="char" quantity="18"></space><w n="6.1" punct="pt:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="12"><w n="7.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.8" punct="vg:10">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>t</w>, <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="7.10" punct="vg:12">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="8" num="3.2" lm="12"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="8.5">j<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="8.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="8.9">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">aim</seg></w></l>
						<l n="9" num="3.3" lm="3"><space unit="char" quantity="18"></space><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3" punct="pe:3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="12"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>x</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4" punct="vg:5">br<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="10.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="10.9">n</w>’<w n="10.10"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="10.11" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
						<l n="11" num="4.2" lm="12"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.5" punct="vg:10">qu<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg">e</seg>s</w>, <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w>-<w n="11.7" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
						<l n="12" num="4.3" lm="3"><space unit="char" quantity="18"></space><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2" punct="pt:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>