<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR538" modus="cm" lm_max="12">
					<head type="main">RONDEL D’OCTOBRE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="1.5">qu</w>’<w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="1.9" punct="pv:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="2.9" punct="pv:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pv">œu</seg>rs</w> ;</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3" punct="vg:4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" punct="vg">e</seg></w>, <w n="3.4">bl<seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="3.6">l</w>’<w n="3.7" punct="vg:9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="3.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="3.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.10" punct="vg:12">m<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="4.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.4" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>, <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.6" punct="pe:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>r</w>, <w n="5.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="5.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="5.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="5.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="5.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.7" punct="ps:12">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="ps">eu</seg>rs</w>…</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="7.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.5">qu</w>’<w n="7.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="7.9" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="8.9" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>rs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rps</w> <w n="9.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="9.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="9.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="9.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="10.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="10.10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
						<l n="11" num="3.3" lm="12"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.4">h<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.7">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="11.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
						<l n="12" num="3.4" lm="12"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="12.7" punct="pe:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="13" num="3.5" lm="12"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="13.9" punct="pt:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>