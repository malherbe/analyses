<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR492" modus="cp" lm_max="12">
					<head type="main">IN MEMORIAM</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="1.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="1.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="1.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.8" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.7">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.8">s</w>’<w n="2.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">qu</w>’<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="3.6" punct="vg:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="u" type="vs" value="1" rule="d-2" place="8">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="3.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.10" punct="vg:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="4.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="4.4" punct="pt:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="8" punct="pt">ô</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rt</w> <w n="5.5">ch<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="5.7" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>ts</w>. <w n="5.8">L<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.9" punct="pi:12">v<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ps</w> <w n="6.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="6.4" punct="vg:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>t</w>, <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.7" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="11">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></w>.</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2" punct="pi:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pi">i</seg>s</w> ? <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="7.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="7.7" punct="pe:8">v<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg></w> ! <w n="7.8">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="7.9">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rps</w> <w n="7.10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg></w></l>
						<l n="8" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7" punct="ps:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12">« <w n="9.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="9.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="9.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">n</w>’<w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="9.6" punct="vg:6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" punct="vg">en</seg></w>, <w n="9.7">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="9.9" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="10.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="10.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="10.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.7" punct="pi:12">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="11.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="11.7"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="11.8">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="11.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
						<l n="12" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.6" punct="pi:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ? »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1" punct="vg:2">P<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="13.2" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="13.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu</w>’<w n="13.4" punct="vg:6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" punct="vg">un</seg></w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="13.6">l</w>’<w n="13.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.8"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="13.10">nu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>t</w> <w n="13.11">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="12">en</seg>t</w></l>
						<l n="14" num="4.2" lm="12"><w n="14.1">T</w>’<w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="14.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.7" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="15" num="4.3" lm="12"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="15.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.4" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</w>, <w n="15.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="15.6" punct="dp:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="16" num="4.4" lm="8"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2" punct="vg:4">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="16.3" punct="vg:5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg></w>, <w n="16.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8" punct="pt">en</seg>t</w>.</l>
					</lg>
				</div></body></text></TEI>