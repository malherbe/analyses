<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">BALLADES</head><div type="poem" key="DHL45" modus="cm" lm_max="10">
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">AN</seg>S</w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">h<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rts</w></l>
						<l n="2" num="1.2" lm="10"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="2.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6" punct="dp:10"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
						<l n="3" num="1.3" lm="10"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="3.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rs</w></l>
						<l n="4" num="1.4" lm="10"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="4.2">C<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.5" punct="pv:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="5" num="1.5" lm="10"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="3">œ</seg>il</w> <w n="5.5" punct="vg:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>x</w>, <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ts</w> <w n="5.10" punct="pv:10">m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>rds</w> ;</l>
						<l n="6" num="1.6" lm="10"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="6.3" punct="vg:4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="6.4">l</w>’<w n="6.5">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="6.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t</w> <w n="6.7" punct="pv:10"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="7" num="1.7" lm="10"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="7.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="7.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.7" punct="dp:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="dp">ai</seg>ts</w> :</l>
						<l n="8" num="1.8" lm="10"><w n="8.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="8.2">n</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.4">d</w>’<w n="8.5" punct="pt:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pt">u</seg>s</w>. <w n="8.6">P<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="8.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="8.10">s<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="8.11" punct="vg:10">n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="9" num="1.9" lm="10"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="9.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.5">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="9.7" punct="pt:10">C<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">è</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1" lm="10"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="10.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="10.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rds</w></l>
						<l n="11" num="2.2" lm="10"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="11.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.6">l</w>’<w n="11.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
						<l n="12" num="2.3" lm="10"><w n="12.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="6">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>nt</w> <w n="12.4">t<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t</w> <w n="12.5" punct="vg:10">vi<seg phoneme="e" type="vs" value="1" rule="383" place="9">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rds</w>,</l>
						<l n="13" num="2.4" lm="10"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="13.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>vi<seg phoneme="?" type="va" value="1" rule="162" place="4">en</seg>t</w> <w n="13.4">j<seg phoneme="ø" type="vs" value="1" rule="390" place="5">eû</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.6" punct="vg:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="14" num="2.5" lm="10"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="14.2">pr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="14.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="14.5" punct="pt:10">g<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>rds</w>.</l>
						<l n="15" num="2.6" lm="10"><w n="15.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="15.5">br<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>f</w> <w n="15.6" punct="dp:10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
						<l n="16" num="2.7" lm="10"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.4" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">aî</seg>t</w>, <w n="16.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">n</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.8">qu</w>’<hi rend="ital"><w n="16.9" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d</w> <w n="16.10">h<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="DHL45_1" place="10" punct="pt">e</seg>s</w></hi>.</l>
						<l n="17" num="2.8" lm="10"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>ts</w> <w n="17.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="17.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rcs</w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="354" place="7">e</seg>x<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="18" num="2.9" lm="10"><w n="18.1">L</w>’<w n="18.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="18.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="18.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="18.5">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="18.7" punct="pt:10">C<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">è</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1" lm="10"><w n="19.1">L</w>’<w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="19.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="19.7">d</w>’<w n="19.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="19.9" punct="vg:10">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rds</w>,</l>
						<l n="20" num="3.2" lm="10"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">cr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="21" num="3.3" lm="10"><w n="21.1">D</w>’<w n="21.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="21.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="21.4">h<seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="4">en</seg></w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="21.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rds</w></l>
						<l n="22" num="3.4" lm="10"><w n="22.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.4">d</w>’<w n="22.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="22.6" punct="vg:10">d<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="23" num="3.5" lm="10"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="23.3">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="23.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s</w> <w n="23.6" punct="pt:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>rds</w>.</l>
						<l n="24" num="3.6" lm="10"><w n="24.1">M<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="24.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="24.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="24.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="24.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="25" num="3.7" lm="10"><w n="25.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r</w> <w n="25.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="25.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="25.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="25.6" punct="pt:10">c<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">è</seg>s</w>.</l>
						<l n="26" num="3.8" lm="10"><w n="26.1">D</w>’<w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="26.3">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="26.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="26.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t</w> <w n="26.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="26.8" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="27" num="3.9" lm="10"><w n="27.1">L</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="27.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="27.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="27.5">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="27.7" punct="pt:10">C<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">è</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<head type="main">ENVOI</head>
						<l n="28" num="4.1" lm="10"><w n="28.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="28.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="28.3">d</w>’<w n="28.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="28.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="28.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rds</w>,</l>
						<l n="29" num="4.2" lm="10"><w n="29.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="29.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.3">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="29.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="29.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rds</w></l>
						<l n="30" num="4.3" lm="10"><w n="30.1" punct="dp:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="dp">an</seg>t</w> : <w n="30.2">B<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="30.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="30.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="30.5">l</w>’<w n="30.6" punct="pt:10">h<seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
						<l n="31" num="4.4" lm="10"><w n="31.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="31.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="31.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="31.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="31.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="31.7" punct="vg:10">fl<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="vg">è</seg>s</w>,</l>
						<l n="32" num="4.5" lm="10"><w n="32.1">Qu</w>’<w n="32.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="32.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="32.4">gr<seg phoneme="o" type="vs" value="1" rule="434" place="5">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.5">d<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>t</w> <w n="32.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w> <w n="32.7" punct="dp:10">d<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
						<l n="33" num="4.6" lm="10"><w n="33.1">L</w>’<w n="33.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="33.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="33.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="33.5">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="33.7" punct="pt:10">C<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">è</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>