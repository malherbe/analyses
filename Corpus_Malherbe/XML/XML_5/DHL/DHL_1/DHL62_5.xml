<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="DHL62" modus="cp" lm_max="12">
				<head type="main">TATA,</head>
				<head type="sub_1">CHAT DE MADAME LA MARQUISE DE MONGLAS,</head>
				<opener>
					<salute>À GRISETTE, <lb></lb>CHATTE DE MADAME DESHOULIÈRES.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><space unit="char" quantity="8"></space><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">AI</seg></w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="2.4" punct="vg:8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="3.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="3.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>tti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="5" num="1.5" lm="8"><space unit="char" quantity="8"></space><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="5.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">d</w>’<w n="5.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="pe">en</seg>s</w> !</l>
					<l n="6" num="1.6" lm="8"><space unit="char" quantity="8"></space><w n="6.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="6.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="6.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="6.6" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="8"><space unit="char" quantity="8"></space><w n="7.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t</w> <w n="7.6" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
					<l n="8" num="1.8" lm="8"><space unit="char" quantity="8"></space><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="9" num="1.9" lm="7"><space unit="char" quantity="10"></space><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">j</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.4" punct="pt:7"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pt">en</seg>t</w>.</l>
					<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3">m</w>’<w n="10.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="10.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="11" num="1.11" lm="8"><space unit="char" quantity="8"></space><w n="11.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6" punct="pi:8">f<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pi">oi</seg></w> ?</l>
					<l n="12" num="1.12" lm="8"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.7">s</w>’<w n="12.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="13" num="1.13" lm="8"><space unit="char" quantity="8"></space><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.5" punct="pv:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pv">oi</seg></w> ;</l>
					<l n="14" num="1.14" lm="8"><space unit="char" quantity="8"></space><w n="14.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w>-<w n="14.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="14.6" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="15" num="1.15" lm="8"><space unit="char" quantity="8"></space><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="15.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</w> !</l>
					<l n="16" num="1.16" lm="8"><space unit="char" quantity="8"></space><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.6" punct="vg:8">ch<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="17" num="1.17" lm="8"><space unit="char" quantity="8"></space><w n="17.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w>-<w n="17.2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="17.5">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w>-<w n="17.6">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.8" punct="pi:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pi">eu</seg>x</w> ?</l>
					<l n="18" num="1.18" lm="8"><space unit="char" quantity="8"></space><w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="18.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="18.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4">e</seg></w> <w n="18.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6" punct="dp:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
					<l n="19" num="1.19" lm="8"><space unit="char" quantity="8"></space><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="19.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="19.5">l</w>’<w n="19.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="20" num="1.20" lm="8"><space unit="char" quantity="8"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="20.2">qu</w>’<w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="20.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="20.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="20.6">p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
					<l n="21" num="1.21" lm="8"><space unit="char" quantity="8"></space><w n="21.1">M</w>’<w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="21.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.6">qu</w>’<w n="21.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="21.8" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="22" num="1.22" lm="10"><space unit="char" quantity="4"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="22.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="22.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w></l>
					<l n="23" num="1.23" lm="8"><space unit="char" quantity="8"></space><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.3">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="23.7" punct="vg:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="24" num="1.24" lm="8"><space unit="char" quantity="8"></space><w n="24.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="24.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="24.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="24.4" punct="pv:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</w> ;</l>
					<l n="25" num="1.25" lm="8"><space unit="char" quantity="8"></space><w n="25.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="25.4">d</w>’<w n="25.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="25.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="26" num="1.26" lm="8"><space unit="char" quantity="8"></space><w n="26.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="26.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="26.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="26.4">qu</w>’<w n="26.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="26.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="26.7" punct="pt:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="27" num="1.27" lm="8"><space unit="char" quantity="8"></space><w n="27.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="27.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.3">n</w>’<w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="27.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.7" punct="dp:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>rs</w> :</l>
					<l n="28" num="1.28" lm="12"><w n="28.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.2">d</w>’<w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="28.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="28.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="28.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="28.9" punct="vg:12">Gr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="29" num="1.29" lm="12"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="29.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="29.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="29.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="29.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="29.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="29.9">j</w>’<w n="29.10"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="11">ai</seg></w> <w n="29.11">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="30" num="1.30" lm="6"><space unit="char" quantity="12"></space><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="30.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="30.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="30.4" punct="pv:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>rs</w> ;</l>
					<l n="31" num="1.31" lm="8"><space unit="char" quantity="8"></space><w n="31.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="31.2">d</w>’<w n="31.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="31.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="31.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="32" num="1.32" lm="6"><space unit="char" quantity="12"></space><w n="32.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="32.3" punct="pt:6"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>