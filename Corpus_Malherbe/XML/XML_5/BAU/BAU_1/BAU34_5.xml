<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU34" modus="cm" lm_max="10">
					<head type="number">XXXIII</head>
					<head type="main">Le Léthé</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6">cr<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.8" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10"><w n="2.1">T<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>rs</w> <w n="2.6" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="pv">en</seg>ts</w> ;</l>
						<l n="3" num="1.3" lm="10"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="3.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="3.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>gts</w> <w n="3.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w></l>
						<l n="4" num="1.4" lm="10"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.6">cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.7" punct="pv:10">l<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="5.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="10">um</seg></w></l>
						<l n="6" num="2.2" lm="10"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.3" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2" punct="vg:4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="7.6" punct="vg:10">fl<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="10"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="8.7" punct="pt:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" punct="pt">un</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3" punct="pe:4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe">i</seg>r</w> ! <w n="9.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t</w> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.7" punct="pe:10">v<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
						<l n="10" num="3.2" lm="10"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="10.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="10.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="10.8" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rt</w>,</l>
						<l n="11" num="3.3" lm="10"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="11.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rd</w></l>
						<l n="12" num="3.4" lm="10"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rps</w> <w n="12.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.8" punct="pt:10">cu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>ts</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w></l>
						<l n="14" num="4.2" lm="10"><w n="14.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="6">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="14.9" punct="pv:10">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="15" num="4.3" lm="10"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="15.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="15.7" punct="vg:10">b<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="10"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">L<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="16.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="16.7" punct="pt:10">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.3" punct="vg:4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="17.6" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="10"><w n="18.1">J</w>’<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="18.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="18.5" punct="pv:10">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></w> ;</l>
						<l n="19" num="5.3" lm="10"><w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>r</w> <w n="19.2" punct="vg:4">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.3"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w> <w n="19.4" punct="vg:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>,</l>
						<l n="20" num="5.4" lm="10"><w n="20.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="20.6" punct="vg:10">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="10"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2" punct="vg:4">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="21.4">n<seg phoneme="wa" type="vs" value="1" rule="440" place="6">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="21.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="21.6" punct="vg:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="vg">œu</seg>r</w>,</l>
						<l n="22" num="6.2" lm="10"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="22.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="22.6">c<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">ë</seg></w></l>
						<l n="23" num="6.3" lm="10"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="23.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ts</w> <w n="23.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.6">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">ë</seg></w></l>
						<l n="24" num="6.4" lm="10"><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="24.2">n</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="24.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="24.7" punct="pt:10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="pt">œu</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>