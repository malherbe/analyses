<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUPPLÉMENT AUX FLEURS DU MAL</head><div type="poem" key="BAU157" modus="cm" lm_max="12">
					<head type="number">XXI</head>
					<head type="main">La Lune offensée</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="1.2">L<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">qu</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="1.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="1.7" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="5">y</seg>s</w> <w n="2.5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s</w> <w n="2.6" punct="vg:7"><seg phoneme="u" type="vs" value="1" rule="426" place="7" punct="vg">où</seg></w>, <w n="2.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="2.8" punct="vg:12">s<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="vg">a</seg>il</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">im</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="3.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="vg">a</seg>il</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3" punct="vg:6">C<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="4">yn</seg>th<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="4.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="4.7" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="5.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ts</w> <w n="5.8" punct="vg:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="6.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="6.9" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="pi">a</seg>il</w> ?</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="7.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="7.8" punct="pi:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="pi">a</seg>il</w> ?</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>cs</w> <w n="8.6">s</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>pl<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="8.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="8.9" punct="pi:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg></w> <w n="9.4" punct="vg:6">j<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">d</w>’<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="9.8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="9">e</seg>d</w> <w n="9.9" punct="vg:12">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w>-<w n="10.2" punct="vg:2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg></w>, <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="10.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="10.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>squ</w>’<w n="10.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="10.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">En</seg>d<seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg>m<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="11.6" punct="pi:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12">« — <w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.8">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pp<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="13.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.7">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rd</w> <w n="13.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="13.9">d</w>’<w n="13.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg></w> <w n="14.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="14.7">t</w>’<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.9" punct="pe:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg></w> ! »</l>
					</lg>
				</div></body></text></TEI>