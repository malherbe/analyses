<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU78" modus="cm" lm_max="12">
					<head type="number">LXXIV</head>
					<head type="main">Une gravure fantastique</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="1.4">n</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.8" punct="vg:12">t<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Gr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="2.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.7" punct="vg:12">squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.7" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>l</w>.</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.4" punct="vg:6">fou<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>t</w>, <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="4.8" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">F<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.3" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="5.4">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="i" type="vs" value="1" rule="493" place="11">y</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="6.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="7.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="7.7">s</w>’<w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="7.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w> <w n="7.10" punct="vg:12">d<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>t</w> <w n="8.8" punct="pt:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rd<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="9.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="1.10" lm="12"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6">om</seg></w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="10.9" punct="vg:12">br<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rt</w>, <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ct<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="11.8" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="12" num="1.12" lm="12"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">c<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.5" punct="vg:8">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>d</w>, <w n="12.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="12.7" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
						<l n="13" num="1.13" lm="12"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="13.2" punct="vg:3">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>nt</w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="13.4">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="13.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c</w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="13.10" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="1.14" lm="12"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">l</w>’<w n="14.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366" place="9">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8" punct="pt:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>