<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU17" modus="cm" lm_max="12">
					<head type="number">XVI</head>
					<head type="main">Châtiment de l’orgueil</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.7">Th<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8">d</w>’<w n="2.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rg<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="3.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="3.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="3.10" punct="vg:12">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ds</w>,</l>
						<l n="4" num="1.4" lm="12">— <w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="4.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="4.6" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>ts</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="5.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="5.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="5.7" punct="pv:12">n<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="6.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="6.6">c<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="6.7">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="7" num="1.7" lm="12"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="7.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="7.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w>-<w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rs</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ts</w> <w n="8.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>ls</w> <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w>-<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.9" punct="vg:12">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</w>,</l>
						<l n="9" num="1.9" lm="12">— <w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="9.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="9.6" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>t</w>, <w n="9.7">pr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.9" punct="tc:12">p<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg ti">e</seg></w>, —</l>
						<l n="10" num="1.10" lm="12"><w n="10.1">S</w>’<w n="10.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="10.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="10.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="9">ue</seg>il</w> <w n="10.7" punct="dp:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="11" num="1.11" lm="12">« <w n="11.1" punct="vg:2">J<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="11.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="11.3" punct="pe:6">J<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe">u</seg>s</w> ! <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5">t</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="11.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="11.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11">en</seg></w> <w n="11.9" punct="pe:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="pe">au</seg>t</w> !</l>
						<l n="12" num="1.12" lm="12"><w n="12.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="12.3">j</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="12.6">t</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="12.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>t</w></l>
						<l n="13" num="1.13" lm="12"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l</w>’<w n="13.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="13.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="13.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="13.8" punct="vg:12">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="1.14" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.8">f<seg phoneme="e" type="vs" value="1" rule="250" place="8">œ</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="14.9" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> ! »</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1" lm="12"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="15.6" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></w>.</l>
						<l n="16" num="2.2" lm="12"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="16.6">d</w>’<w n="16.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="16.8">cr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="16.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="16.10" punct="pv:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pv">à</seg></w> ;</l>
						<l n="17" num="2.3" lm="12"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">ch<seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg><seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="17.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="17.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="17.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="2.4" lm="12"><w n="18.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="18.3" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="18.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="18.8">d</w>’<w n="18.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="19" num="2.5" lm="12"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">pl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ds</w> <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="19.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="19.7">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="19.9" punct="pt:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></w>.</l>
						<l n="20" num="2.6" lm="12"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="20.6">s</w>’<w n="20.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="20.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="20.9" punct="vg:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg></w>,</l>
						<l n="21" num="2.7" lm="12"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="21.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="21.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="21.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="21.7">cl<seg phoneme="e" type="vs" value="1" rule="148" place="9">e</seg>f</w> <w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="21.9" punct="pt:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="22" num="3.1" lm="12"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="22.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="22.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="22.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="22.7">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="22.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="22.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="22.10" punct="vg:12">r<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="23" num="3.2" lm="12"><w n="23.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="23.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="23.4">s</w>’<w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="23.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="23.8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="23.9" punct="vg:9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>r</w>, <w n="23.10"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="23.11">tr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>rs</w></l>
						<l n="24" num="3.3" lm="12"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2" punct="vg:2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">am</seg>ps</w>, <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="24.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>gu<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="24.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="24.8" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</w>,</l>
						<l n="25" num="3.4" lm="12"><w n="25.1" punct="vg:1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="25.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>d</w> <w n="25.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="25.7">ch<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.8" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="26" num="3.5" lm="12"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="26.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="26.6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="26.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="26.9" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>