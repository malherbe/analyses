<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU107" modus="cm" lm_max="12">
					<head type="number">CIII</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="1.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="1.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="1.9" punct="vg:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="2.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="2.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.8">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="9">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.9" punct="vg:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="3.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="3.7" punct="pt:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2" punct="vg:2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rts</w>, <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.5" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rts</w>, <w n="4.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.9" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="5.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">O</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="5.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="6.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="6.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1" punct="vg:1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" punct="vg">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="7.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="7.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="7.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="7.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>ts</w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="vg:3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>r</w>, <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="8.5" punct="vg:6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>t</w>, <w n="8.6">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="8.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="8.9" punct="vg:12">dr<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>ps</w>,</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="9.2" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="9.6" punct="vg:12">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="1.10" lm="12"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="10.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="10.6">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="10.7" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="11" num="1.11" lm="12"><w n="11.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="11.2">squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="11.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.7" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
						<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="12.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.8">l</w>’<w n="12.9">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</w></l>
						<l n="13" num="1.13" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="13.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="13.9">f<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="14" num="1.14" lm="12"><w n="14.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="14.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r</w> <w n="14.8" punct="pt:12">gr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1" lm="12"><w n="15.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.3">b<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.6" punct="vg:9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="15.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="15.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="15.9" punct="vg:12">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</w>,</l>
						<l n="16" num="2.2" lm="12"><w n="16.1" punct="vg:2">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w> <w n="16.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="16.7">v<seg phoneme="wa" type="vs" value="1" rule="440" place="9">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="16.8">s</w>’<w n="16.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="12" punct="vg">eoi</seg>r</w>,</l>
						<l n="17" num="2.3" lm="12"><w n="17.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="17.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="17.5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="17.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="17.9" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="2.4" lm="12"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="18.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="18.7">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg></w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="18.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="18.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="19" num="2.5" lm="12"><w n="19.1" punct="vg:1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="19.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="19.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="19.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="19.9"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12">e</seg>l</w></l>
						<l n="20" num="2.6" lm="12"><w n="20.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="20.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="20.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="9">œ</seg>il</w> <w n="20.8" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="vg">e</seg>l</w>,</l>
						<l n="21" num="2.7" lm="12"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w>-<w n="21.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4">e</seg></w> <w n="21.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="21.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="21.8" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="22" num="2.8" lm="12"><w n="22.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="22.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="22.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="22.7">p<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="22.8" punct="pi:12">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>