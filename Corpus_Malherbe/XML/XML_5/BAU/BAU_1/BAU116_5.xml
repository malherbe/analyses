<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">FLEURS DU MAL</head><div type="poem" key="BAU116" modus="cm" lm_max="12">
					<head type="number">CXII</head>
					<head type="main">La Destruction</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.5">c<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.9">D<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="2.9" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>lp<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6">en</seg>s</w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.8">br<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="3.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="3.10">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.9" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="5.3" punct="vg:4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d</w>, <w n="5.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.9">l</w>’<w n="5.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">A</seg>rt</w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="6.6">s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="6.8" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">sp<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="7.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>xt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>rd</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="8.6">ph<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="8.7" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="9.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="9.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="9.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="9.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.9" punct="vg:12">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5" punct="vg:9">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="10.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg></w></l>
						<l n="11" num="3.3" lm="12"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">l</w>’<w n="11.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="5">En</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="11.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="12.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg>s</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
						<l n="13" num="4.2" lm="12"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="13.3" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="428" place="5">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.5">bl<seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="13.6" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.7" punct="pe:12">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>str<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></w> !</l>
					</lg>
				</div></body></text></TEI>