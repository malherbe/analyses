<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN465" modus="sm" lm_max="8">
				<head type="main">Celle qui reste</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg>s</w> ! <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="1.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="1.4" punct="pt:8">dr<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="2.2" punct="vg:3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>s</w>, <w n="2.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="2.6" punct="vg:8">t<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4">fr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="3.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="3.6" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="6" num="2.2" lm="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3" punct="vg:4">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l</w> <w n="6.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.3" lm="8"><w n="7.1">L</w>’<w n="7.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.4" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.6" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="9.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="9.5" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="10" num="3.2" lm="8"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="10.5" punct="vg:8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="11" num="3.3" lm="8"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="12" num="3.4" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="12.4" punct="vg:5">gl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>, <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7" punct="pt:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="vg:3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="13.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="14" num="4.2" lm="8"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="14.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="14.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="15" num="4.3" lm="8"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="4.4" lm="8"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="16.5" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="dp">ai</seg>t</w> : <w n="17.4">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="17.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="17.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="18" num="5.2" lm="8"><w n="18.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="18.4" punct="dp:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="dp">e</seg>rt</w> : <w n="18.5">l</w>’<w n="18.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="19" num="5.3" lm="8"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="19.2" punct="vg:2">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2" punct="vg">aim</seg></w>, <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4" punct="pv:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oi</seg>d</w> ; <w n="19.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="19.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="19.8" punct="pv:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
					<l n="20" num="5.4" lm="8"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="20.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="20.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.7" punct="pe:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="21.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="21.4">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="21.6" punct="vg:8">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="22" num="6.2" lm="8"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="22.5" punct="vg:8">l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="23" num="6.3" lm="8"><w n="23.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="23.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="24" num="6.4" lm="8"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="24.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="24.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="24.5" punct="dp:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">l</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="25.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="25.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="26" num="7.2" lm="8"><w n="26.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="27" num="7.3" lm="8"><w n="27.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w>-<w n="27.3" punct="vg:3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="27.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="27.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="27.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="28" num="7.4" lm="8"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="28.3" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="28.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="28.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="28.6" punct="pt:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="8"><w n="29.1" punct="pe:4"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg>s</w> ! <w n="29.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="29.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="29.4" punct="pe:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="30" num="8.2" lm="8"><w n="30.1">L</w>’<w n="30.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="30.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="30.5" punct="dp:6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="dp">à</seg></w> : <w n="30.6">qu</w>’<w n="30.7" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="31" num="8.3" lm="8"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="31.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="31.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="31.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="31.6" punct="dp:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
					<l n="32" num="8.4" lm="8"><w n="32.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="32.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="32.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="32.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="32.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="8"><w n="33.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="33.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l</w> <w n="33.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="33.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="33.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.6" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="34" num="9.2" lm="8"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="34.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="34.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="34.5">tr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="35" num="9.3" lm="8"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="35.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="35.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="35.4" punct="ps:8">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					<l n="36" num="9.4" lm="8"><w n="36.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="36.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="36.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="36.4">n</w>’<w n="36.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="36.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.7" punct="pt:8">bl<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>