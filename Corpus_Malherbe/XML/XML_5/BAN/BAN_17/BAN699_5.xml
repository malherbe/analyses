<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN699" modus="cm" lm_max="12">
				<head type="main">Massacre</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pv">an</seg>s</w> ; <w n="1.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="1.9">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="2.8">d</w>’<w n="2.9"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.10">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="2.11" punct="pt:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="3.4">r<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="3.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="426" place="12" punct="vg">ou</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="4.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="4.8">j<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>squ</w>’<w n="4.9" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="426" place="12" punct="pt">où</seg></w>.</l>
					<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ff<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="5.7">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="vg:6">n<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="6.9">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="7" num="1.7" lm="12"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="7.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>, <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="7.7" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg></w>.</l>
					<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.7">br<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s</w> <w n="8.8">n<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg></w></l>
					<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="9.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rgn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="9.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="9.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.6">b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>j<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.8" punct="pt:12">cu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="10.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.9">su<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ffr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="11.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="11.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
					<l n="12" num="1.12" lm="12"><w n="12.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.5">d<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="12.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="12.9" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</w>.</l>
					<l n="13" num="1.13" lm="12"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="13.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="13.7">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="13.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="14" num="1.14" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ff<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="14.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="14.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.7" punct="pv:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="15" num="1.15" lm="12"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.5">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="15.6" punct="vg:12">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="16" num="1.16" lm="12"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="16.8">l</w>’<w n="16.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
					<l n="17" num="1.17" lm="12"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="17.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>s</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="17.9" punct="pt:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="18" num="1.18" lm="12"><w n="18.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="18.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="18.3" punct="vg:3">d<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>il</w>, <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.5">n<seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="6">ï</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="19" num="1.19" lm="12"><w n="19.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="19.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="19.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="19.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="19.7" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>rd</w>,</l>
					<l n="20" num="1.20" lm="12"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="20.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="20.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="20.8" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>rt</w>,</l>
					<l n="21" num="1.21" lm="12"><w n="21.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="21.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rps</w> <w n="21.4">fr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="21.8" punct="vg:12">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="22" num="1.22" lm="12"><w n="22.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2" punct="vg:4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="22.3" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</w> ! <w n="22.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="22.5" punct="pt:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mercredi, 12 janvier 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>