<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN714" modus="sm" lm_max="7">
				<head type="number">Le Guitariste</head>
				<head type="form">Pantoum</head>
				<opener>
					<salute>A Georges Rochegrosse</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="7"><w n="1.1">J<seg phoneme="u" type="vs" value="1" rule="426" place="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>r</w>, <w n="1.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.4" punct="vg:7">gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="7"><w n="2.1">J<seg phoneme="u" type="vs" value="1" rule="426" place="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg></w> <w n="2.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="2.5" punct="pt:7">f<seg phoneme="u" type="vs" value="1" rule="426" place="7" punct="pt">ou</seg></w>.</l>
					<l n="3" num="1.3" lm="7"><w n="3.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="3.5" punct="vg:6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="3.6" punct="vg:7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="7"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="4.6" punct="pt:7">tr<seg phoneme="u" type="vs" value="1" rule="426" place="7" punct="pt">ou</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="7"><w n="5.1">J<seg phoneme="u" type="vs" value="1" rule="426" place="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg></w> <w n="5.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="5.5" punct="vg:7">f<seg phoneme="u" type="vs" value="1" rule="426" place="7" punct="vg">ou</seg></w>,</l>
					<l n="6" num="2.2" lm="7"><w n="6.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>t</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.4" punct="pt:7">pl<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="7" num="2.3" lm="7"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="7.6" punct="vg:7">tr<seg phoneme="u" type="vs" value="1" rule="426" place="7" punct="vg">ou</seg></w>,</l>
					<l n="8" num="2.4" lm="7"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="8.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4" punct="pt:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="7"><w n="9.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>t</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4" punct="vg:7">pl<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="7"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>ds</w> <w n="10.6" punct="pt:7">l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg>s</w>.</l>
					<l n="11" num="3.3" lm="7"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="11.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="12" num="3.4" lm="7"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="12.6" punct="pt:7">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="7"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>ds</w> <w n="13.6">l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w></l>
					<l n="14" num="4.2" lm="7"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.4" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
					<l n="15" num="4.3" lm="7"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="15.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="15.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w></l>
					<l n="16" num="4.4" lm="7"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.5" punct="pt:7">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="7"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="17.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
					<l n="18" num="5.2" lm="7"><w n="18.1">C</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="18.6" punct="pe:7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pe">eu</seg>r</w> !</l>
					<l n="19" num="5.3" lm="7"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="19.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="19.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
					<l n="20" num="5.4" lm="7"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="20.4">n<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="20.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="20.6" punct="pt:7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="7"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="21.6" punct="vg:7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</w>,</l>
					<l n="22" num="6.2" lm="7"><w n="22.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="22.2">j<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="22.4">s</w>’<w n="22.5" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="23" num="6.3" lm="7"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="23.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="23.4">n<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="23.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w></l>
					<l n="24" num="6.4" lm="7"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.6" punct="pt:7">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="7"><w n="25.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="25.2">j<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.4">s</w>’<w n="25.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="26" num="7.2" lm="7"><w n="26.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="26.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="26.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="26.5" punct="pt:7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pt">an</seg>t</w>.</l>
					<l n="27" num="7.3" lm="7"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="28" num="7.4" lm="7"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pt">en</seg>t</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="7"><w n="29.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="29.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="29.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="29.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="29.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w></l>
					<l n="30" num="8.2" lm="7"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="30.6" punct="pt:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="31" num="8.3" lm="7"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7" punct="pt">en</seg>t</w>.</l>
					<l n="32" num="8.4" lm="7"><w n="32.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="32.4" punct="pe:7">br<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="7"><w n="33.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="33.2">l</w>’<w n="33.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="33.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="33.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="33.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="34" num="9.2" lm="7"><w n="34.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="34.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="34.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="34.4">f<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="34.6" punct="pt:7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></w>.</l>
					<l n="35" num="9.3" lm="7"><w n="35.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="35.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="35.3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="35.4" punct="pe:7">br<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
					<l n="36" num="9.4" lm="7"><w n="36.1">C</w>’<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="36.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="36.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="36.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="36.7" punct="pt:7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1" lm="7"><w n="37.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="37.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="37.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="37.4">f<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="37.6" punct="pt:7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></w>.</l>
					<l n="38" num="10.2" lm="7"><w n="38.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="38.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="38.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="38.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="38.5" punct="pi:7">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="6">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></w> ?</l>
					<l n="39" num="10.3" lm="7"><w n="39.1">C</w>’<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="39.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="39.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="39.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="39.7" punct="pt:7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></w>.</l>
					<l n="40" num="10.4" lm="7"><w n="40.1">J<seg phoneme="u" type="vs" value="1" rule="426" place="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="40.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>r</w>, <w n="40.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="40.4" punct="pt:7">gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>