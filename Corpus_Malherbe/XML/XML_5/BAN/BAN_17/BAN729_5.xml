<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN729" modus="cm" lm_max="12">
				<head type="main">Églé</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd</w> <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="1.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.10" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.9" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">d</w>’<w n="3.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">É</seg>gl<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="3.7">l</w>’<w n="3.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">aî</seg>t</w>, <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="4.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="10">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">pr<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="5.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="5.9" punct="vg:12">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="6.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.9">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l</w> <w n="6.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.11" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">l<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>s</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.8">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="7.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="7.10">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="10">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">gl<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.6" punct="pt:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pt">en</seg>d</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.7" punct="vg:12">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>gl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="10.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="10.8">d</w>’<w n="10.9" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="11.5" punct="vg:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r</w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.9" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pt">e</seg>il</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2">fr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.6">gl<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" punct="vg">ein</seg>s</w>, <w n="13.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="13.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="13.7">r<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.9" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</w>,</l>
					<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.5">cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="14.10" punct="pt:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">Villa de Banville, 29 octobre 1884</date>
					</dateline>
				</closer>
			</div></body></text></TEI>