<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN730" modus="sm" lm_max="8">
				<head type="main">Ballade <lb></lb>Pour Mademoiselle Edmée Daudet</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="1.3" punct="vg:3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>rs</w> <w n="2.4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="3" num="1.3" lm="8"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="3.5">c<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.8" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="5" num="1.5" lm="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="5.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="5.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="6" num="1.6" lm="8"><w n="6.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.5" punct="vg:8">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.5" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg>s</w>.</l>
					<l n="8" num="1.8" lm="8"><w n="8.1" punct="vg:2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>dm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="8"><w n="9.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="9.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="2.2" lm="8"><w n="10.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="10.2" punct="vg:3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="11" num="2.3" lm="8"><w n="11.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5" punct="pt:8">ch<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="12" num="2.4" lm="8"><w n="12.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="12.2">pr<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="12.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="12.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="13" num="2.5" lm="8"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg>y<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="14" num="2.6" lm="8"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="15" num="2.7" lm="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="15.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="15.3" punct="pt:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pt">en</seg>ts</w>.</l>
					<l n="16" num="2.8" lm="8"><w n="16.1" punct="vg:2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>dm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="8"><w n="17.1" punct="vg:1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="17.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="18" num="3.2" lm="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2" punct="vg:3">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3" punct="vg">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="18.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="18.6" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="19" num="3.3" lm="8"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="19.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.6" punct="vg:8">gr<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="20" num="3.4" lm="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="20.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.5" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>ts</w>,</l>
					<l n="21" num="3.5" lm="8"><w n="21.1">L</w>’<w n="21.2" punct="vg:2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="2" punct="vg">ue</seg>il</w>, <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ts</w></l>
					<l n="22" num="3.6" lm="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="22.3" punct="vg:4">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="22.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="23" num="3.7" lm="8"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gs</w> <w n="23.4" punct="pe:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pe">en</seg>ts</w> !</l>
					<l n="24" num="3.8" lm="8"><w n="24.1" punct="vg:2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="24.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>dm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<head type="form">Envoi</head>
					<l n="25" num="4.1" lm="8"><w n="25.1" punct="vg:3">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="25.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="25.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rds</w> <w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w></l>
					<l n="26" num="4.2" lm="8"><w n="26.1">F<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="26.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="27" num="4.3" lm="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="27.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="27.4" punct="pt:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pt">en</seg>ts</w>.</l>
					<l n="28" num="4.4" lm="8"><w n="28.1" punct="vg:2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="28.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>dm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1886">Villa de Banville, mercredi 21 juillet 1886.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>