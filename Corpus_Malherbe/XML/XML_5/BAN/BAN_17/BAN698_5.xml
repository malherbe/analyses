<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN698" modus="cm" lm_max="12">
				<head type="main">Mourir, dormir</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg">en</seg>t</w>, <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="1.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="1.8" punct="pt:12">fi<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rts</w> <w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="2.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="2.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.10" punct="pt:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="3.5" punct="pv:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>r</w> ; <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="3.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>t</w> <w n="3.9" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</w>.</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="4.4">gl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="4.6">b<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="4.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="4.9" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</w>.</l>
					<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rts</w> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="5.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="5.11" punct="pv:12">cl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="12">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="6.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="6.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rts</w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.10" punct="pv:12">pl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="12">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.7">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="7.9" punct="pt:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>s</w>.</l>
					<l n="8" num="1.8" lm="12"><w n="8.1" punct="vg:2">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="8.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="8.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="8.9" punct="vg:12">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>ts</w>,</l>
					<l n="9" num="1.9" lm="12"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.9">fou<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="11" num="1.11" lm="12"><w n="11.1">Gr<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="11.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3" punct="vg:4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.8" punct="pt:12">n<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
					<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.3">h<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="12.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w> <w n="12.7">l</w>’<w n="12.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>qu<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</w> ;</l>
					<l n="13" num="1.13" lm="12"><w n="13.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="13.4" punct="vg:6">tr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="13.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="13.8" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="1.14" lm="12"><w n="14.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="14.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ps</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">fou<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="14.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.10" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="15" num="1.15" lm="12"><w n="15.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="15.2" punct="pe:3">h<seg phoneme="y" type="vs" value="1" rule="457" place="3" punct="pe">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ! <w n="15.3" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="133" place="4" punct="pe">E</seg>h</w> ! <w n="15.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.5" punct="vg:6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>c</w>, <w n="15.6" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg></w> ! <w n="15.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="15.8" punct="vg:10">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>c</w>, <w n="15.9" punct="pe:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>h<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg>t</w> !</l>
					<l n="16" num="1.16" lm="12"><w n="16.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="16.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3" punct="vg:3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>c</w>, <w n="16.4" punct="pe:6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="?" type="va" value="1" rule="162" place="6" punct="pe">en</seg>t</w> ! <w n="16.5" punct="pe:9">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pe">e</seg></w> ! <w n="16.6" punct="pe:12">G<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>h<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg>t</w> !</l>
					<l n="17" num="1.17" lm="12"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3" punct="vg:4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>l</w>, <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="17.5">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="17.8" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="18" num="1.18" lm="12"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2" punct="vg:2">fou<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="vg">e</seg>t</w>, <w n="18.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="18.5" punct="vg:6">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="18.8" punct="pt:12">l<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="19" num="1.19" lm="12"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rcl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="19.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>, <w n="19.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="10">y</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
					<l n="20" num="1.20" lm="12"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="20.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="20.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="20.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="20.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="21" num="1.21" lm="12"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ff<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="21.4">n</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="21.7"><seg phoneme="y" type="vs" value="1" rule="391" place="8">eu</seg></w> <w n="21.8">d</w>’<w n="21.9" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="421" place="10" punct="vg">oi</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="21.11">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="22" num="1.22" lm="12"><w n="22.1">N</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="22.3">qu</w>’<w n="22.4"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.5" punct="pt:4">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pt">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">I</seg>l</w> <w n="22.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="22.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="22.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="22.11" punct="pt:12">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mercredi, 12 janvier 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>