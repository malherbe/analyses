<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">AMOURS D’ÉLISE</head><div type="poem" key="BAN10" modus="sm" lm_max="8">
					<head type="number">V</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">z<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr</w>’<w n="2.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6" punct="vg:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ts</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">f<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6" punct="pt:8">f<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="2.2" lm="8"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="6.3">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.6" punct="vg:8">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>gts</w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="2">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="2.4" lm="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3">tr<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.6" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.6">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="10" num="3.2" lm="8"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5" punct="vg:6">fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="10.6" punct="vg:8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="3.3" lm="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.6" punct="pt:8">m<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="8"><w n="12.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="13" num="4.2" lm="8"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="14" num="4.3" lm="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">b<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="14.6" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>