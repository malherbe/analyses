<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="main_subpart">EN HABIT ZINZOLIN</head><div type="poem" key="BAN65" modus="cp" lm_max="10">
						<head type="number">III</head>
						<head type="main">RONDEAU, À ISMÈNE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="10"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4" punct="vg:4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="vg">oin</seg>s</w>, <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w>-<w n="1.6" punct="vg:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="1.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="468" place="9">I</seg>sm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="10"><w n="2.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.3" punct="pv:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pv">a</seg>s</w> ; <w n="2.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="2.5" punct="vg:7">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</w>, <w n="2.6" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="10"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>ts</w></l>
							<l n="4" num="1.4" lm="10"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.7">v<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="4.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.9" punct="vg:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="10" punct="vg">en</seg>s</w>,</l>
							<l n="5" num="1.5" lm="10"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2" punct="vg:4">g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="5.3" punct="vg:7">cr<seg phoneme="y" type="vs" value="1" rule="454" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg">e</seg></w>, <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="5.6" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1" lm="10"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="6.4">l</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="6.7" punct="vg:10">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="7" num="2.2" lm="10"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="7.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-27" place="9">e</seg></w> <w n="7.6" punct="vg:10">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="8" num="2.3" lm="10"><w n="8.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>rs</w> <w n="8.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="8.6" punct="vg:10">pr<seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>ts</w>,</l>
							<l n="9" num="2.4" lm="4"><space quantity="12" unit="char"></space><w n="9.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4" punct="pt:4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="pt">oin</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="10" num="3.1" lm="10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.2" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rs</w>, <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.5" punct="vg:10">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="11" num="3.2" lm="10"><w n="11.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="11.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">l<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>s</w> <w n="11.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.8" punct="vg:10">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="12" num="3.3" lm="10"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="12.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</w></l>
							<l n="13" num="3.4" lm="10"><w n="13.1">M</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="1">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="13.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="13.6" punct="pv:10">pu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pv">an</seg>ts</w> ;</l>
							<l n="14" num="3.5" lm="10"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="14.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="14.7" punct="vg:10">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="15" num="3.6" lm="4"><space quantity="12" unit="char"></space><w n="15.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4" punct="pt:4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="pt">oin</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>