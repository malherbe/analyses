<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">SONGE D’HIVER</head><div type="poem" key="BAN24" modus="cm" lm_max="12">
						<head type="number">XI</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="1.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.6" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.3">Ju<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5" punct="vg:6">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lt<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.8" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1" lm="12"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="3.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg>s</w> <w n="3.9">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="11">ei</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
							<l n="4" num="2.2" lm="12"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4" punct="vg:6">v<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="4.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="4.8" punct="pt:12">j<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
						</lg>
						<lg n="3">
							<l n="5" num="3.1" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</w>, <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="5.7">f<seg phoneme="a" type="vs" value="1" rule="193" place="10">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="5.8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="6" num="3.2" lm="12"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.9" punct="pt:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="7" num="4.1" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2" punct="vg:2"><seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="7.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.8">r<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rts</w>,</l>
							<l n="8" num="4.2" lm="12"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.3">s</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="6">eoi</seg>r</w> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="8.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="8.9" punct="pt:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rts</w>.</l>
						</lg>
						<lg n="5">
							<l n="9" num="5.1" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="9.9">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="10" num="5.2" lm="12"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">gl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="10.10" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="11" num="6.1" lm="12"><w n="11.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="11.6">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>ds</w> <w n="11.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
							<l n="12" num="6.2" lm="12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="12.3" punct="vg:6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="12.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="12.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rbr<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</w>,</l>
						</lg>
						<lg n="7">
							<l n="13" num="7.1" lm="12"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="13.2" punct="pv:3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="pv">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> ; <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="14" num="7.2" lm="12"><w n="14.1">S</w>’<w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383" place="2">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="14.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="14.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>ts</w> <w n="14.9">d</w>’<w n="14.10" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>