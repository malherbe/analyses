<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="main_subpart">EN HABIT ZINZOLIN</head><div type="poem" key="BAN70" modus="sm" lm_max="8">
						<head type="number">VIII</head>
						<head type="main">MADRIGAL, À GLYCÈRE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3">m</w>’<w n="1.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
							<l n="2" num="1.2" lm="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="2.8" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="3.3">C</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="3.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.8" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
							<l n="4" num="1.4" lm="8"><w n="4.1" punct="vg:2">Gl<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">n</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="4.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.8" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="5" num="1.5" lm="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w></l>
							<l n="6" num="1.6" lm="8"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6" punct="pt:8">ch<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="7" num="1.7" lm="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="7.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.5">l</w>’<w n="7.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>r</w> ?</l>
							<l n="8" num="1.8" lm="8"><w n="8.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="8.2">V<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.5" punct="pe:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1842">juin 1842</date>.
							</dateline>
						</closer>
					</div></body></text></TEI>