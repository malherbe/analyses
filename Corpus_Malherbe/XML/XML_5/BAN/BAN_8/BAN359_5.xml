<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN359" modus="cm" lm_max="12">
				<head type="main">A la Font-Georges</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w>-<w n="1.2" punct="vg:3">ge<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4" punct="pe:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="1.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="1.7" punct="pe:10"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pe">eau</seg>x</w> ! <w n="1.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">z<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>r</w> <w n="2.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="2.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="2.8" punct="pe:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">N<seg phoneme="a" type="vs" value="1" rule="343" place="1">a</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs</w> <w n="3.6" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pe">en</seg>s</w> !</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="vg">ai</seg></w>-<w n="4.3" punct="vg:3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w>, <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.5" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="vg">en</seg>t</w>, <w n="4.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="4.9" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg>s</w>,</l>
					<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="5.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">J<seg phoneme="u" type="vs" value="1" rule="d-2" place="1">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="6.8" punct="vg:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">j</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="7.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="7.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.10" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</w>,</l>
					<l n="8" num="1.8" lm="12"><w n="8.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="8.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="8.9"><seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg>x</w></l>
					<l n="9" num="1.9" lm="12"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.5">n<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.7">d</w>’<w n="9.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r</w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.11" punct="vg:12">m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.5" punct="vg:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>t</w>, <w n="10.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.7">f<seg phoneme="œ" type="vs" value="1" rule="304" place="10">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="10.8" punct="vg:12">b<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="11.8">fl<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>ts</w> <w n="11.9">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s</w></l>
					<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="12.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.8" punct="pe:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1849">Août 1849.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>