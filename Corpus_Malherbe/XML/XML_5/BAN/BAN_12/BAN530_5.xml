<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PRINCESSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>294 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvillelesprincesses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1994">1994</date>
								</imprint>
								<biblScope unit="tome">IV</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1891">1889-1891</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VII</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN530" modus="cm" lm_max="12">
				<head type="number">XVIII</head>
				<head type="main">La Princesse de Lamballe</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Pendant la vogue des traîneaux, la Reine en reçut <lb></lb>
								un bleu et or, attelé de chevaux blancs aux harnais <lb></lb>
								de velours bleu ; elle le partageait souvent avec la <lb></lb>
								princesse de Lamballe…
							</quote>
							<bibl>
								<name>James de Chambrier</name>,<hi rend="ital">Marie-Antoinette, Reine de France</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3" punct="vg:4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="1.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="1.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="1.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.9">d</w>’<w n="2.10" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</w>, <w n="2.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="2.12">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="2.13">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r</w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:2">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.8" punct="vg:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2" punct="vg:4">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.5">c<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="4.8" punct="pt:12">R<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="5.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.8" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="6.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.8" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="7.2" punct="vg:4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>nt</w>, <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.7">l</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.9" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="8.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>x</w> <w n="8.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>rs</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.8">l</w>’<w n="8.9" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11">U</seg>kr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.3" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="4" punct="vg">ue</seg>il</w>, <w n="9.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>, <w n="9.5" punct="vg:7">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7" punct="vg">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="9">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</w></l>
					<l n="10" num="3.2" lm="12"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">gr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">s<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="10.6">gl<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.8" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="11.4">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>di<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.8">Fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.8" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="493" place="12" punct="vg">y</seg>s</w>,</l>
					<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rds</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="13.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rd</w> <w n="13.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="13.10" punct="pt:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="14" num="4.3" lm="12">« <w n="14.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="14.2">t<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3" punct="pe:5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="pe">e</seg>s</w> ! » <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.7" punct="pt:12">G<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>