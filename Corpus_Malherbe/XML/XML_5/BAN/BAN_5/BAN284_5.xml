<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN284" modus="sm" lm_max="8">
				<head type="number">XCII</head>
				<head type="main">Les Tristes</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="a" type="vs" value="1" rule="365" place="7">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="2.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="2.6" punct="vg:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="4.5">s</w>’<w n="4.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.2">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></w>,</l>
					<l n="6" num="2.2" lm="8"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t</w> <w n="6.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="6.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="7" num="2.3" lm="8"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.4" punct="vg:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
					<l n="8" num="2.4" lm="8"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>il</w> <w n="8.5" punct="pt:8">st<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8"><w n="9.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="9.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="10" num="3.2" lm="8"><w n="10.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.3" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="11" num="3.3" lm="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ls</w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="12" num="3.4" lm="8"><w n="12.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>nt</w>, <w n="12.2">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="12.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="12.6" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8"><w n="13.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w>-<w n="13.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ls</w>, <w n="13.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
					<l n="14" num="4.2" lm="8"><w n="14.1">D<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="14.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="14.4" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
					<l n="15" num="4.3" lm="8"><w n="15.1">T<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="15.3" punct="pe:3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pe">an</seg>g</w> ! <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="15.6">t</w>-<w n="15.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
					<l n="16" num="4.4" lm="8"><w n="16.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4" punct="pi:8">cr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</w> ?</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8"><w n="17.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="17.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="17.4"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="18" num="5.2" lm="8"><w n="18.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>nt</w>, <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="18.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>t</w> <w n="18.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="19" num="5.3" lm="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="19.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
					<l n="20" num="5.4" lm="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="20.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="20.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w> <w n="20.6" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="21.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4">t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w></l>
					<l n="22" num="6.2" lm="8"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="22.3">d</w>’<w n="22.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="22.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.6" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="23" num="6.3" lm="8"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="23.2" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>nt</w>, <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.5" punct="vg:8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg></w>,</l>
					<l n="24" num="6.4" lm="8"><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">s</w>’<w n="24.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="24.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.6" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="8"><w n="25.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="25.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="25.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="25.6" punct="pe:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</w> !</l>
					<l n="26" num="7.2" lm="8"><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="26.2">n</w>’<w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="26.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="26.7">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="27" num="7.3" lm="8"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="27.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="27.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="27.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="27.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
					<l n="28" num="7.4" lm="8"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="28.2">b<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="28.3">qu</w>’<w n="28.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="28.5" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="8"><w n="29.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="29.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="29.4" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="30" num="8.2" lm="8"><w n="30.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="30.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="30.3" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="30.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="30.5" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
					<l n="31" num="8.3" lm="8"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="31.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="31.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="31.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.6">p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
					<l n="32" num="8.4" lm="8"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="32.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="32.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="32.6" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="8"><w n="33.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="33.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="33.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="33.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>fs</w></l>
					<l n="34" num="9.2" lm="8"><w n="34.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="34.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rs</w> <w n="34.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="34.4">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="34.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="34.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="34.7" punct="vg:8">br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="35" num="9.3" lm="8"><w n="35.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="35.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="35.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="35.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="35.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>fs</w></l>
					<l n="36" num="9.4" lm="8"><w n="36.1">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="36.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="36.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="36.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</w> <w n="36.5" punct="pv:8">l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1" lm="8"><w n="37.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="37.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="37.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="37.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="37.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w></l>
					<l n="38" num="10.2" lm="8"><w n="38.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="38.2">c<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="38.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="38.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="38.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="38.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="39" num="10.3" lm="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="39.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="39.4">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="39.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="39.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="39.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w></l>
					<l n="40" num="10.4" lm="8"><w n="40.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="40.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="40.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="40.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="40.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="40.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">12 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>