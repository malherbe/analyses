<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN265" modus="sm" lm_max="7">
				<head type="number">LXXIII</head>
				<head type="main">Ave</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">fl<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w></l>
					<l n="2" num="1.2" lm="7"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">l</w>’<w n="2.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6" punct="vg:7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="vg">em</seg>ps</w>,</l>
					<l n="3" num="1.3" lm="7"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="3.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.7" punct="pv:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</w> ;</l>
					<l n="4" num="1.4" lm="7"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="4.6" punct="dp:6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" punct="dp">un</seg></w> : <w n="4.7" punct="vg:7">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="7" punct="vg">i</seg></w>,</l>
					<l n="5" num="1.5" lm="7"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rd</w>’<w n="5.4">hu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w></l>
					<l n="6" num="1.6" lm="7"><w n="6.1">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.3">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.4" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="7"><w n="7.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">fr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w></l>
					<l n="8" num="2.2" lm="7"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="vg:7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>x</w>,</l>
					<l n="9" num="2.3" lm="7"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="9.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="10" num="2.4" lm="7"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>ts</w></l>
					<l n="11" num="2.5" lm="7"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w></l>
					<l n="12" num="2.6" lm="7"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="12.5" punct="pt:7">C<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="7"><w n="13.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">P<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w></l>
					<l n="14" num="3.2" lm="7"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="14.5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="6">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w></l>
					<l n="15" num="3.3" lm="7"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7" punct="vg:7">dr<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="16" num="3.4" lm="7"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="16.5">l</w>’<w n="16.6" punct="vg:7">h<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</w>,</l>
					<l n="17" num="3.5" lm="7"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="17.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w></l>
					<l n="18" num="3.6" lm="7"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.6" punct="pt:7">f<seg phoneme="a" type="vs" value="1" rule="193" place="7">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1" lm="7"><w n="19.1">L</w>’<w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.4">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
					<l n="20" num="4.2" lm="7"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="20.2">c<seg phoneme="œ" type="vs" value="1" rule="345" place="2">ue</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="20.4">fl<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
					<l n="21" num="4.3" lm="7"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="21.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4">om</seg></w> <w n="21.4" punct="vg:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="22" num="4.4" lm="7"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="22.4">s</w>’<w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w></l>
					<l n="23" num="4.5" lm="7"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="23.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="23.5">d</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w></l>
					<l n="24" num="4.6" lm="7"><w n="24.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">pr<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="24.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="24.6" punct="pt:7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1" lm="7"><w n="25.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="25.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="25.5">M<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.6" punct="vg:7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="vg">e</seg>rt</w>,</l>
					<l n="26" num="5.2" lm="7"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3" punct="vg:4">v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="26.4" punct="vg:5">v<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg></w>, <w n="26.5" punct="pv:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="pv">e</seg>rt</w> ;</l>
					<l n="27" num="5.3" lm="7"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="27.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="27.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="27.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="28" num="5.4" lm="7"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2" punct="vg:2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>t</w>, <w n="28.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="28.5">m<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>ts</w> <w n="28.6" punct="vg:7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>rs</w>,</l>
					<l n="29" num="5.5" lm="7"><w n="29.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>br<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="29.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="29.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="29.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>rs</w></l>
					<l n="30" num="5.6" lm="7"><w n="30.1">L</w>’<w n="30.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="30.3" punct="pt:7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1" lm="7"><w n="31.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="31.2">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.4" punct="vg:7">L<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="vg">e</seg>ps</w>,</l>
					<l n="32" num="6.2" lm="7"><w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.4" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="32.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="32.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="32.8" punct="vg:7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="vg">e</seg>ps</w>,</l>
					<l n="33" num="6.3" lm="7"><w n="33.1">Qu</w>’<w n="33.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="33.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="33.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="33.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="33.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="33.7" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					<l n="34" num="6.4" lm="7"><w n="34.1">L</w>’<w n="34.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="34.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="34.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="34.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="34.6" punct="vg:7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg></w>,</l>
					<l n="35" num="6.5" lm="7"><w n="35.1" punct="vg:2">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="35.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="35.3"><seg phoneme="y" type="vs" value="1" rule="251" place="4">eû</seg>t</w> <w n="35.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="35.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="35.6" punct="vg:7">di<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg></w>,</l>
					<l n="36" num="6.6" lm="7"><w n="36.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="36.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="36.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="36.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="36.5">d</w>’<w n="36.6" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="355" place="6">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1" lm="7"><w n="37.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="37.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="37.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="37.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>sthm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="37.5"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rts</w></l>
					<l n="38" num="7.2" lm="7"><w n="38.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="38.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="38.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="38.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="38.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="38.6" punct="pv:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="pv">e</seg>rts</w> ;</l>
					<l n="39" num="7.3" lm="7"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="39.3">T<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="39.4" punct="vg:7">ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="40" num="7.4" lm="7"><w n="40.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="40.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="40.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="40.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="40.5" punct="vg:7">t<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>ts</w>,</l>
					<l n="41" num="7.5" lm="7"><w n="41.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="41.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="41.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>ts</w></l>
					<l n="42" num="7.6" lm="7"><w n="42.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="42.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="42.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.5" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1" lm="7"><w n="43.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="43.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="43.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="43.4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="43.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="43.6" punct="pv:7">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="318" place="7" punct="pv">au</seg>x</w> ;</l>
					<l n="44" num="8.2" lm="7"><w n="44.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="44.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="44.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="44.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="44.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="44.6">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w></l>
					<l n="45" num="8.3" lm="7"><w n="45.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="45.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="45.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="45.4" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="46" num="8.4" lm="7"><w n="46.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="46.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="46.3">R<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
					<l n="47" num="8.5" lm="7"><w n="47.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="47.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="47.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="47.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="47.5">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w></l>
					<l n="48" num="8.6" lm="7"><w n="48.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="48.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="48.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="48.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="48.5" punct="pt:7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">22 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>