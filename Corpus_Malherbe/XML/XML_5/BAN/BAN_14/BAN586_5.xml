<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN586" modus="cm" lm_max="12">
				<head type="main">A Gérard Piogey</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2" punct="vg:3">G<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd</w>, <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="1.8">d</w>’<w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.10">l<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.4">c<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.6" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="3.5">c<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="3.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="4.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="4.7" punct="pt:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="5.4" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>l</w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="5.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</w></l>
					<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3" punct="vg:3">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg>s</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>xqu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="6.8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="6.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="6.10" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="7.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="7.7">m</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="7.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="7.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.11" punct="pi:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					<l n="8" num="2.4" lm="12"><w n="8.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4" punct="vg:6"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7">m</w>’<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="8.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="11">en</seg>s</w> <w n="8.10" punct="pt:12">pl<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4" punct="vg:6">b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.8">l<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.5">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="11.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>dv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</w>, <w n="12.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="12.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="13.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.4">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="13.5" punct="vg:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="13.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>, <w n="13.7" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="213" place="9">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="14" num="4.3" lm="12"><w n="14.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">Ge<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.3" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="14.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="14.8" punct="pe:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1875">Lundi 22 mars 1875.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>