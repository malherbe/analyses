<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN607" modus="cm" lm_max="12">
				<head type="main">Querelle</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.5" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.8">f<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="10">ê</seg>ts</w> <w n="1.9" punct="vg:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="2.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>ds</w> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="2.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="307" place="11">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="3.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="3.9" punct="vg:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="4.6" punct="pt:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="5.5">d</w>’<w n="5.6" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="5.7">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="5.10" punct="vg:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.3">ru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="6.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rs</w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="6.7">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="6.8">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="6.10" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="7.4">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="7.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="7.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.9" punct="vg:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="8.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="8.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="8.7" punct="pt:12">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="9.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="9.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="9.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>sti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="9.8" punct="vg:12">bl<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1" punct="vg:2">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="10.5" punct="dp:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="dp">oi</seg>s</w> : <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="444" place="9">O</seg></w> <w n="10.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="10.8" punct="pe:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>ts</w> !</l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="11.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="11.5" punct="vg:6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>ds</w>, <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="11.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="11.9">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="11">ai</seg></w> <w n="11.10" punct="pe:12">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="12.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>s</w>, <w n="12.6">d</w>’<w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.8">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l</w> <w n="12.9" punct="vg:12">m<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
					<l n="13" num="4.2" lm="12"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="14" num="4.3" lm="12"><w n="14.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="14.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="14.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="14.9" punct="pt:12">y<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845"> 16 février 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>