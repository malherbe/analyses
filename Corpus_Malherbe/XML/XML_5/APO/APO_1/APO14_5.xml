<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Bestiaire</title>
				<title type="medium">Édition électronique</title>
				<author key="APO">
					<name>
						<forename>Guillaume</forename>
						<surname>APOLLINAIRE</surname>
					</name>
					<date from="1880" to="1918">1880-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>127 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">APO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bestiaire</title>
						<author>Guillaume Apollinaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>florilege.free.fr</publisher>
						<idno type="URL">http://www.florilege.free.fr/recueil/apollinaire-le_bestiaire.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="APO14" modus="sm" lm_max="8">
				<head type="main">La Chenille</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>il</w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="2.2" punct="vg:5">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="2.3" punct="pe:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</w> !</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="3.6">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
				</lg>
			</div></body></text></TEI>