<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI109" modus="cm" lm_max="10">
					<head type="main">Les Dissonances</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">UN</seg></w> <w n="1.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d</w> <w n="1.5">br<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.7" punct="vg:10">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg></w> <w n="2.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="2.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="2.8" punct="vg:10">v<seg phoneme="wa" type="vs" value="1" rule="440" place="8">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
						<l n="3" num="1.3" lm="10"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c</w> <w n="3.5">R<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.8">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="3.9">l</w>’<w n="3.10" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="4" num="1.4" lm="10"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="4.5">br<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="5" num="1.5" lm="10"><w n="5.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.4" punct="dp:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="dp in">an</seg>t</w> : « <w n="5.5" punct="vg:7">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</w>, <w n="5.6" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></w> ! »</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="10"><w n="6.1">P<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.4" punct="po:5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> (<w n="6.5">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.7" punct="pf:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>),</l>
						<l n="7" num="2.2" lm="10"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.4">st<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="7.8" punct="dp:10">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="dp">an</seg></w> :</l>
						<l n="8" num="2.3" lm="10"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3">pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>f</w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="8.7" punct="pv:10">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="9">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg>s</w> ;</l>
						<l n="9" num="2.4" lm="10"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.7" punct="ps:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg>s</w>…</l>
						<l n="10" num="2.5" lm="10"><w n="10.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="10.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.8">Gu<seg phoneme="i" type="vs" value="1" rule="491" place="9">î</seg>cl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg></w><ref type="noteAnchor">*</ref></l>
					</lg>
					<closer>
						<note id="*" type="footnote">Barde du Ve siècle.</note>
					</closer>
				</div></body></text></TEI>