<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="sub_part">AU BORD DE LA MÉDITERRANÉE</head><div type="poem" key="BRI76" modus="cm" lm_max="12">
					<head type="main">Le Rêve</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>TT<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">E</seg></w> <w n="1.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4" punct="pt:6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="pt">ai</seg>s</w>. <w n="1.5">S<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.4" punct="po:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> (<w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="2.8" punct="pf:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="12" punct="vg">oi</seg></w>),</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.4" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>ts</w>, <w n="3.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="3.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.2" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="vg">en</seg>t</w>, <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.6" punct="pv:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pv">oi</seg></w> ;</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">m</w>’<w n="5.5" punct="dp:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="dp in">ai</seg></w> : « <w n="5.6">S<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="5.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.10">m</w>’<w n="5.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.4" punct="pv:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pv">er</seg></w> ; <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.8">c</w>’<w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="6.10" punct="pe:12">tr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> ! »</l>
						<l n="7" num="1.7" lm="12"><w n="7.1" punct="vg:3">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="7.2">j</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="7.7" punct="dp:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="dp">ou</seg>ps</w> :</l>
						<l n="8" num="1.8" lm="12">« <w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="8.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="8.5">qu</w>’<w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="8.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="6">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.9">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.10"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="8.11">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="8.12" punct="pt:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>. »</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">m</w>’<w n="9.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="9.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.8" punct="vg:9">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="9.10" punct="vg:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
						<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="10.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.8" punct="pt:12">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>