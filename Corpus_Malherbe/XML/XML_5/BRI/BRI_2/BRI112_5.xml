<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI112" modus="cp" lm_max="12">
					<head type="main">Frutti di Mare</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">E</seg>RS</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.8">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg></w></l>
						<l n="2" num="1.2" lm="10"><space unit="char" quantity="4"></space><w n="2.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t</w> <w n="2.2">l</w>’<w n="2.3" punct="pv:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pv">er</seg></w> ; <w n="2.4" punct="vg:5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>s</w>, <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="2.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.7" punct="vg:10">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.4" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg">ain</seg></w>, <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="3.6" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="472" place="9">i</seg>i<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">ez</seg></w>, <w n="3.7" punct="ps:12">N<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="ps">a</seg></w>…</l>
						<l n="4" num="1.4" lm="6"><space unit="char" quantity="12"></space><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in">i</seg>t</w> : « <w n="4.5">P<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w>-<w n="4.6" punct="pi:6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></w> ? »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="vg">en</seg>t</w>, <w n="5.3">l</w>’<w n="5.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="5.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.7">fru<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>ts</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.9" punct="pv:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pv">e</seg>r</w> ;</l>
						<l n="6" num="2.2" lm="10"><space unit="char" quantity="4"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="6.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gts</w> <w n="6.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.7" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="7.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="7.4" punct="vg:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="7.8" punct="dp:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="dp">ai</seg>r</w> :</l>
						<l n="8" num="2.4" lm="6"><space unit="char" quantity="12"></space><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd</w> <w n="8.5">s</w>’<w n="8.6" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="9.5" punct="vg:6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="9.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.8">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="9.9" punct="pv:12">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</w> ;</l>
						<l n="10" num="3.2" lm="10"><space unit="char" quantity="4"></space><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="10.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.7">l</w>’<w n="10.8">hu<seg phoneme="i" type="vs" value="1" rule="491" place="8">î</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.9" punct="ps:10">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg></w>…</l>
						<l n="11" num="3.3" lm="12"><w n="11.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="3">œ</seg>il</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="11.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="11.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9"><seg phoneme="œ" type="vs" value="1" rule="286" place="10">œ</seg>il</w> <w n="11.10" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
						<l n="12" num="3.4" lm="6"><space unit="char" quantity="12"></space><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="12.3" punct="dp:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>s</w> : <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5" punct="pe:6">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w>-<w n="13.2" punct="vg:2">d<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="13.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="13.4">fru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ts</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg">e</seg>r</w>, <w n="13.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g</w> <w n="13.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd</w> <w n="13.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l</w></l>
						<l n="14" num="4.2" lm="10"><space unit="char" quantity="4"></space><w n="14.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="14.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="14.3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="14.4">l</w>’<w n="14.5" punct="dp:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="dp">er</seg></w> : <w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="14.8" punct="ps:10">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg></w>…</l>
						<l n="15" num="4.3" lm="12"><w n="15.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="15.2" punct="vg:6">N<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg">ain</seg>s</w>, <w n="15.3" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>, <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="15.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="15.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="16" num="4.4" lm="6"><space unit="char" quantity="12"></space><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">d<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="16.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t</w> <w n="16.5" punct="pt:6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>