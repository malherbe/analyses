<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI108" modus="cp" lm_max="12">
					<head type="main">Hymne au Fils</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">t</w>’<w n="1.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="1.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="1.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8" punct="pe:12"><seg phoneme="y" type="vs" value="1" rule="453" place="10">U</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></w> !</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="2.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="4">ym</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="2.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="2.7" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2">fr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ts</w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>ds</w> <w n="3.6">l</w>’<w n="3.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="3.10" punct="dp:12">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="dp">é</seg></w> :</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="4.5" punct="vg:8">V<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="5" num="1.5" lm="8"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.5" punct="pt:8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="12"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="6.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3" punct="vg:6">P<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.6">t</w>’<w n="6.7" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>t</w> !</l>
						<l n="7" num="2.2" lm="12"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rbi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>cts</w> <w n="7.5">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="7.7" punct="pv:12">cr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="8" num="2.3" lm="12"><w n="8.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>pp<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.5">pl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.8" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</w>,</l>
						<l n="9" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="9.1">P<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="9.2" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="9.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w></l>
						<l n="10" num="2.5" lm="8"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="10.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="10.5" punct="pt:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">t</w>’<w n="11.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="11.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="11.7" punct="pe:12">Cr<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></w> !</l>
						<l n="12" num="3.2" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>gn<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="12.2">d</w>’<w n="12.3" punct="pe:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pe">on</seg></w> ! <w n="12.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.5" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="13" num="3.3" lm="12"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="13.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="14" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="14.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
						<l n="15" num="3.5" lm="8"><space unit="char" quantity="8"></space><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>rs</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.6" punct="pt:8">cr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>