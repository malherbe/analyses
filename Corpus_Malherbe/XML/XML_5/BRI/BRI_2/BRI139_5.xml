<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI139" modus="cm" lm_max="12">
					<head type="main">À la Fontaine féerique</head>
					<head type="sub_1">de Baranton</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="1.5">m</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="8">eoi</seg>r</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="1.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="1.9" punct="pv:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1" punct="vg:1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>f</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">aî</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="2.8" punct="pt:12">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="12"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ct<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.8" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="2.2" lm="12"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="4.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.6" punct="pt:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="12"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="5.5">j</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="5.7">c<seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="5.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="5.10">t<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
						<l n="6" num="3.2" lm="12"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">Sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="6">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>x</w> <w n="6.9">fru<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="6.10">d</w>’<w n="6.11" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>r</w> ;</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="12"><w n="7.1">M</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="7.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="6">ue</seg>il</w> <w n="7.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.7">S<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="11">ain</seg></w> <w n="7.8" punct="vg:12">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="4.2" lm="12"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2" punct="vg:2">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.5" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>t</w>, <w n="8.6">j</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="8.8">m<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="8.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="8.10" punct="pv:12"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="12"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="9.5">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>st<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="9.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
						<l n="10" num="5.2" lm="12"><w n="10.1">L</w>’<w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rd</w> <w n="10.7" punct="dp:12">pr<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="dp">a</seg>l</w> :</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="12"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l</w>’<w n="11.3" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rt</w>, <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.6" punct="vg:6">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.8" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>, <w n="11.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.10" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11">I</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="12" num="6.2" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="12.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t</w> <w n="12.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ct</w> <w n="12.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="12.7">t</w>’<w n="12.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="12.9" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1" lm="12"><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.4" punct="pt:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg></w>. <w n="13.5">Vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="7">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="13.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="13.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="13.9" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</w>,</l>
						<l n="14" num="7.2" lm="12"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="14.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="14.5" punct="vg:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="14.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="14.10" punct="pt:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</w>.</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1" lm="12"><w n="15.1">J</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="15.9">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>t</w> <w n="15.10" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="16" num="8.2" lm="12"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="16.3">m</w>’<w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="16.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="16.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="16.10">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg></w>-<w n="16.11" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1" lm="12"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="17.6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="17.7">d</w>’<w n="17.8" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">A</seg>rv<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>r</w> !</l>
						<l n="18" num="9.2" lm="12"><w n="18.1" punct="vg:3">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="18.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="18.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="18.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="18.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg></w> <w n="18.8">d</w>’<w n="18.9" punct="pe:12"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>