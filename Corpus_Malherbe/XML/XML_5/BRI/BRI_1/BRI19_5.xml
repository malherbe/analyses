<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI19" modus="cm" lm_max="12">
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="1.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="1.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ts</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.9" punct="vg:12">d<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.7" punct="pv:12">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.4">j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rps</w> <w n="4.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.10">s</w>’<w n="4.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="5.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="5.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.6" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="7.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="7.10" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="8" num="1.8" lm="12"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="8.7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="8.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.9" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="9" num="1.9" lm="12"><w n="9.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="9.2">C<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="9.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="9.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="9.8">qu</w>’<w n="9.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.11" punct="dp:12">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="dp">é</seg></w> :</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="10.7" punct="vg:6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rd</w> <w n="10.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.12" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>