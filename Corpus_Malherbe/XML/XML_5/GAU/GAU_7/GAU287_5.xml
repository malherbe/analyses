<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU287" modus="cm" lm_max="12">
					<head type="main">ADIEUX A LA POÉSIE</head>
					<head type="form">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.7" punct="pv:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="2.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="2.8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>s</w> <w n="2.9">d</w>’<w n="2.10" punct="pv:12"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pv">o</seg>r</w> ;</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2" punct="vg:2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>t</w>, <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="3.4">h<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.6">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="3.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="3.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="3.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="3.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="4.9" punct="pt:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="5.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>d</w> <w n="5.9">d</w>’<w n="5.10"><seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="5.11">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.12" punct="pt:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="6.3">li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5" punct="dp:6">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="dp">er</seg></w> : <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="6.7">n</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="6.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="6.10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="6.11" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pv">o</seg>r</w> ;</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="7.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="7.5">l</w>’<w n="7.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="7.7" punct="pv:12">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pv">o</seg>r</w> ;</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="8.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.9" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="9.5" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="9.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="9.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="9.9" punct="dp:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="dp">ain</seg></w> :</l>
						<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.7" punct="pv:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pv">in</seg></w> ;</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="11.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rds</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="11.9" punct="pe:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.6">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="12.9">l</w>’<w n="12.10"><seg phoneme="œ" type="vs" value="1" rule="286" place="11">œ</seg>il</w> <w n="12.11" punct="vg:12">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt</w> <w n="13.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="13.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w>-<w n="13.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="14.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="14.9">d</w>’<w n="14.10" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1844">1844</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>