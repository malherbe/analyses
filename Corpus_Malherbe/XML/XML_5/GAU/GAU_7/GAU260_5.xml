<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU260" modus="cm" lm_max="12">
					<head type="main">L’ESCURIAL</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">P<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="1.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.9" punct="vg:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">E</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>l</w>, <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="3.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="3.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>ts</w> <w n="3.7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="10">e</seg>ds</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="3.9" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>l</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2" punct="vg:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454" place="5">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.5" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3">gr<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.5">T<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pt">o</seg>l</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="12"><w n="7.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="7.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="7.3" punct="vg:6">ph<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="7.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cs</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="7.9">d</w>’<w n="7.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">É</seg>g<seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.2" lm="12"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="8.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="8.8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="8.9" punct="pv:12">cr<seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="9" num="2.3" lm="12"><w n="9.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="9.2">sph<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>x</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt</w> <w n="9.5">n</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="9.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="9.9">d</w>’<w n="9.10" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg></w> ;</l>
						<l n="10" num="2.4" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="10.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.8" punct="pv:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="11" num="2.5" lm="12"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="11.2">l</w>’<w n="11.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="11.7" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="12" num="2.6" lm="12"><w n="12.1" punct="vg:2">M<seg phoneme="wa" type="vs" value="1" rule="421" place="1">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="12.2" punct="vg:4">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="12.3" punct="vg:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>ts</w>, <w n="12.4" punct="vg:9">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" punct="vg">an</seg>s</w>, <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.7" punct="pe:12">fu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pe">i</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="13.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="13.4" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rt</w>, <w n="13.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="13.7">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rd</w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="13.9" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="3.2" lm="12"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.4">r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="14.5" punct="vg:6">sc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="14.10">n<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="15" num="3.3" lm="12"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="15.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="15.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="15.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="15.8" punct="vg:12">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="16" num="3.4" lm="12"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">s</w>’<w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="16.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">aim</seg>s</w> <w n="16.8">d</w>’<w n="16.9" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="17" num="3.5" lm="12"><w n="17.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="17.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ps</w> <w n="17.8">d</w>’<w n="17.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="18" num="3.6" lm="12"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="18.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="18.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe ps">é</seg></w> !…</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Escurial</placeName>,
							<date when="1840">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>