<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU71" modus="cm" lm_max="12">
				<head type="main">LE POT DE FLEURS</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="1.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.7" punct="vg:12">gr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="2.3">d</w>’<w n="2.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rd</w>, <w n="2.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="2.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="2.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="3.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.7">p<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.9" punct="vg:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="4.4">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.7">b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="4.8" punct="pt:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.4" punct="pt:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt">a</seg></w>. <w n="5.5">L<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.9">s</w>’<w n="5.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="6.4">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="6.7" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rbr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pv">eau</seg></w> ;</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="7.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="7.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>d</w> <w n="7.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="7.8">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.4" lm="12"><w n="8.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="8.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="8.9" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.3" punct="pv:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="pv">en</seg>t</w> ; <w n="9.4" punct="vg:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="9.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.8">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="9.9" punct="vg:12">gr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="10.5">p<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="10.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rts</w> <w n="10.9" punct="vg:12">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>rds</w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="11.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.7">t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="11.9" punct="pv:12">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">s</w>’<w n="12.3" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>gts</w> <w n="12.7">s</w>’<w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="12.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>x</w> <w n="12.10" punct="pt:12">d<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>rds</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12"><w n="13.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="13.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.8" punct="pv:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="14" num="4.2" lm="12"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="14.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.9" punct="dp:12">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="dp">em</seg>ps</w> :</l>
					<l n="15" num="4.3" lm="12"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="6">ë</seg>s</w> <w n="15.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="15.8">r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="15.9">br<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="16" num="4.4" lm="12"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">p<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="16.6">d<seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="16.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>ts</w>.</l>
				</lg>
			</div></body></text></TEI>