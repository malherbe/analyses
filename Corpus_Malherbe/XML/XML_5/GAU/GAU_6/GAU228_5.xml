<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU228" modus="cm" lm_max="12">
			<head type="main">OUI, FORSTER, J’ADMIRAIS…</head>
			<lg n="1">
				<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2" punct="vg:3">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rst<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="1.3">j</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.7" punct="pv:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
				<l n="2" num="1.2" lm="12"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">m</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="2.5" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="2.6">l</w>’<w n="2.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.9" punct="dp:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
				<l n="3" num="1.3" lm="12"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="3.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="3.9">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="3.10">m<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
				<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="4.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="4.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="4.7" punct="pe:12">d<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg>s</w> !</l>
				<l n="5" num="1.5" lm="12"><w n="5.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="5.2" punct="vg:6">B<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg">o</seg></w>, <w n="5.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="5.6" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				<l n="6" num="1.6" lm="12"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="6.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="6.8">f<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.9" punct="vg:12">n<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				<l n="7" num="1.7" lm="12"><w n="7.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.9">d</w>’<w n="7.10"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</w></l>
				<l n="8" num="1.8" lm="12"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="8.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">d</w>’<w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="8.9">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>t</w> <w n="8.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="8.11" punct="pe:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>t</w> !</l>
				<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="9.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.7" punct="vg:12">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2" punct="vg:3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="10.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="10.8">d</w>’<w n="10.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.8" punct="vg:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
				<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg></w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.9" punct="pe:12">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pe">e</seg>r</w> !</l>
				<l n="13" num="1.13" lm="12"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5" punct="vg:6">gr<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>c</w>, <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="13.8">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.10" punct="vg:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				<l n="14" num="1.14" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="14.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="14.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.9">d</w>’<w n="14.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">A</seg>phr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				<l n="15" num="1.15" lm="12"><w n="15.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg>h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>j<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="15.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="15.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</w></l>
				<l n="16" num="1.16" lm="12"><w n="16.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.3">l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4" punct="tc:6">r<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg ti">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, — <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="16.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</w></l>
				<l n="17" num="1.17" lm="12"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="17.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="17.4" punct="vg:6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="17.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="17.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="17.8" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
				<l n="18" num="1.18" lm="12"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="18.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="18.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="18.8" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				<l n="19" num="1.19" lm="12"><w n="19.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">d</w>’<w n="19.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="19.4" punct="vg:9">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>t</w>, <w n="19.5" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg></w>,</l>
				<l n="20" num="1.20" lm="12"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="20.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="20.3" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="20.4">d</w>’<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="20.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg></w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1841">1841</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>