<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU118" modus="cm" lm_max="12">
				<head type="main">LE SENTIER</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								En une sente me vins rendre <lb></lb>
								Longue et estroite, où l’herbe tendre <lb></lb>
								Croissait très-drue. <lb></lb>
							</quote>
							<bibl>
								<hi rend="ital">Le livre des quatre Dames</hi>.
							</bibl>
						</cit>
						<cit>
							<quote>
								Un petit sentier vert, je le pris…
							</quote>
							<bibl>
							 <name>Alfred de Musset.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="1.5">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="2.5">s</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="2.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="2.9">g<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.10"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="2.11">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="2.12"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="2.13" punct="pt:12">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="12">— <w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="3.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="3.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.8">M<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="3.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="3.11" punct="vg:12">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rds</w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="4.7" punct="pv:12">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pv">o</seg>rs</w> ;</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3" punct="pv:6">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>t</w> ; <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="5.6" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4" punct="vg:6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">em</seg>ps</w>, <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="6.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="6.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="6.8" punct="pt:12">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="7.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="7.7" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</w>,</l>
					<l n="8" num="1.8" lm="12"><w n="8.1" punct="vg:2">T<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="8.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="8.8">d</w>’<w n="8.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</w>,</l>
					<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="9.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.5">d</w>’<w n="9.6" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>r</w>, <w n="9.7">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">P<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.7">j<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="11" num="1.11" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:3">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="11.4" punct="vg:6">j<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="11.7" punct="vg:12">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</w>,</l>
					<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="12.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>ts</w> <w n="12.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="12.9" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pv">o</seg>s</w> ;</l>
					<l n="13" num="1.13" lm="12"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">f<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="13.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="13.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>gt</w> <w n="13.9" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="14" num="1.14" lm="12"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="14.5">mi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="14.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="14.8">p<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.9">d</w>’<w n="14.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="15" num="1.15" lm="12"><w n="15.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">h<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="15.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="15.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="11">e</seg>t</w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
					<l n="16" num="1.16" lm="12"><w n="16.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="16.4">fr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rd<seg phoneme="wa" type="vs" value="1" rule="440" place="9">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="16.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
					<l n="17" num="1.17" lm="12"><w n="17.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="17.2" punct="pt:3">cr<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pt ti">on</seg></w>. — <w n="17.3">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="17.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="17.6">qu</w>’<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="17.8" punct="vg:9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9" punct="vg">e</seg>st</w>, <w n="17.9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg></w> <w n="17.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="17.11">l</w>’<w n="17.12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="18" num="1.18" lm="12"><w n="18.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="18.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="18.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="18.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="18.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="18.8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="18.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="19" num="1.19" lm="12"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="5">e</seg></w> <w n="19.4">h<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="19.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="19.7" punct="pv:9">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="pv">eu</seg>rs</w> ; <w n="19.8">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="19.9">c</w>’<w n="19.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="19.11" punct="vg:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg></w>,</l>
					<l n="20" num="1.20" lm="12"><w n="20.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="20.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu</w>’<w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="20.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.7">l<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="20.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="20.10" punct="vg:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg></w>,</l>
					<l n="21" num="1.21" lm="12"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="21.3">br<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="21.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="21.5" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>r</w>, <w n="21.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w>-<w n="21.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="21.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="22" num="1.22" lm="12"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="22.4">s</w>’<w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="22.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="22.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="22.9" punct="vg:12">m<seg phoneme="i" type="vs" value="1" rule="493" place="11">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="23" num="1.23" lm="12"><w n="23.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="23.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="23.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="23.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ts</w> <w n="23.7" punct="vg:12">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>fs</w>,</l>
					<l n="24" num="1.24" lm="12"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="24.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="24.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="24.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="24.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="24.8" punct="pt:12">f<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>fs</w>.</l>
				</lg>
			</div></body></text></TEI>