<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU159" modus="cm" lm_max="12">
				<head type="main">SONNET V</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								C’est mon plaisir ; chacun querre le sien.
							</quote>
							<bibl>
							 <name>P. L. Jacob</name>, <hi rend="ital">bibliophile</hi>.
							</bibl>
						</cit>
						<cit>
							<quote>
								Heureusement que, pour nous consoler de tout <lb></lb>
								cela, il nous reste l’adultère, le tabac de Maryland, <lb></lb>
								et le papel español por cigaritos
							</quote>
							<bibl>
								<name>Petrus Borel</name>, <hi rend="ital">le lycanthrope</hi>.
							</bibl>
						</cit>
						<cit>
							<quote>
								Où trouver le bonheur ?
							</quote>
							<bibl>
								<name>Méry et Barthélemy</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="1.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="1.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.9" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="pi ti">e</seg></w> ? — <w n="1.10">L</w>’<w n="1.11"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="2.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="2.9" punct="vg:12">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>ts</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2" punct="vg:3">pi<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.4" punct="vg:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>s</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="3.7">d</w>’<w n="3.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r</w> <w n="3.9">qu</w>’<w n="3.10"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="3.11"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">In</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g</w> <w n="4.5">n</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="4.8">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="9">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.9" punct="pt:12">P<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="5.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6" punct="pt:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt ti">u</seg>s</w>. — <w n="5.7">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r</w>-<w n="5.9" punct="vg:9">ni<seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="9" punct="vg">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="5.11" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="6.3">l</w>’<w n="6.4" punct="pt:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pt ti">en</seg>t</w>. — <w n="6.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="6.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="6.9">c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</w></l>
					<l n="7" num="2.3" lm="12"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="7.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="7.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="7.10" punct="vg:12">fr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.6">l<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="8.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="8.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.10" punct="pt:12">b<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="9.7">t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="9.10" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rt</w>, <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="10.9">l</w>’<w n="10.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</w>,</l>
					<l n="11" num="3.3" lm="12">— <w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.4" punct="vg:6">p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="11.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.9" punct="pt:12">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>xcl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>fs</w>, <w n="12.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8">en</seg>s</w>-<w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="12.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>nt</w> <w n="12.8" punct="pv:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>l</w> ;</l>
					<l n="13" num="4.2" lm="12"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="13.8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="13.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r</w> <w n="13.10" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.3" lm="12"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2" punct="tc:2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="dp ti">on</seg>t</w> : — <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">Un</seg></w> <w n="14.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="14.5" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg">e</seg>il</w>, <w n="14.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="14.7" punct="vg:9">f<seg phoneme="a" type="vs" value="1" rule="193" place="9" punct="vg">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="14.9" punct="pe:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>l</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1831">1831</date>
					</dateline>
				</closer>
			</div></body></text></TEI>