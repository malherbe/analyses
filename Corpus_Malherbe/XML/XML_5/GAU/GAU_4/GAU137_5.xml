<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU137" modus="cm" lm_max="12">
				<head type="main">LE COIN DU FEU</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Blow, blow, winter’s wind.
							</quote>
							<bibl>
								<name>Shakspeare.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Vente, gelle, gresle, j’ay mon pain cuict.
							</quote>
							<bibl>
								<name>Villon.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Around in sympathetic mirth, <lb></lb>
								Its tricks the kitten tries ; <lb></lb>
								The cricket chirrups in the hearth, <lb></lb>
								The crackling faggot flies.
							</quote>
							<bibl>
								<name>Goldsmith.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Quam juvat immites ventos audire cubantem.
							</quote>
							<bibl>
								<name>Tibulle.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">plu<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="1.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.9">t<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>ts</w> <w n="1.10" punct="pe:12">ru<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="2.6" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg">e</seg></w>, <w n="2.7">cr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2">gr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>t</w> <w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.9" punct="pe:12">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>c</w> !</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5">gl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.8">s</w>’<w n="4.9" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cr<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="5.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="5.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="5.8" punct="vg:10">g<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="5.10">r<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="6.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rds</w> <w n="6.7">qu<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rti<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.9" punct="pe:12">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>c</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="12"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="7.3" punct="pe:2">g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="pe">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.5">qu</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="7.8" punct="vg:6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="7.10" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="7.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.12">gr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.2" lm="12"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="8.4">fou<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="8.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="8.7" punct="pe:12">fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="9" num="2.3" lm="12"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">d</w>’<w n="9.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="9.9" punct="pe:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>r</w> !</l>
					<l n="10" num="2.4" lm="12"><w n="10.1">Qu</w>’<w n="10.2" punct="pi:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pi">e</seg></w> ? <w n="10.3">n</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="10.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="10.8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="10.9">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="10.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="10.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="10.12" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="11" num="2.5" lm="12"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.8">j<seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.10" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="2.6" lm="12"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.4" punct="vg:6">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.6">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>il</w> <w n="12.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="12.8" punct="pi:12">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>r</w> ?</l>
				</lg>
			</div></body></text></TEI>