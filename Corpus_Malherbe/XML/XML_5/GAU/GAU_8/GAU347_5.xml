<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU347" modus="cm" lm_max="10">
				<head type="main">A MAXIME DU CAMP</head>
				<head type="form">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="1.3" punct="pe:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="pe">eau</seg></w> ! <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="1.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></w>,</l>
					<l n="2" num="1.2" lm="10"><w n="2.1">C<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3" punct="vg:5">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>, <w n="2.4" punct="vg:7">c<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>f</w>, <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="2.6">d</w>’<w n="2.7" punct="vg:10"><seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="10"><w n="3.1">Pl<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg></w> <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.7" punct="pe:10">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
					<l n="4" num="1.4" lm="10"><w n="4.1" punct="vg:2">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2" punct="vg">en</seg>t</w>, <w n="4.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="4.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>, <w n="4.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="pe:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="5.6">gr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>b<seg phoneme="u" type="vs" value="1" rule="428" place="9">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w></l>
					<l n="6" num="2.2" lm="10"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="6.4">qu</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="6.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.8">pl<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="7" num="2.3" lm="10"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg">o</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.7">t</w>’<w n="7.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="8" num="2.4" lm="10"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>z<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="8.5" punct="pe:10">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rti<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="9.4" punct="vg:5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="i" type="vs" value="1" rule="493" place="5" punct="vg">y</seg></w>, <w n="9.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="9.8" punct="vg:10">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="10"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">v<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="10.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>s</w>, <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="10.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
					<l n="11" num="3.3" lm="10"><w n="11.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="11.4">gl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="11.6" punct="vg:10">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>t</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="10"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.8">p<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="10">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="13" num="4.2" lm="10"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <hi rend="ital"><w n="13.8" punct="vg:10">cr<seg phoneme="i" type="vs" value="1" rule="111" place="9">ea</seg>m</w> <w n="13.9">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>d</w></hi>,</l>
					<l n="14" num="4.3" lm="10"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>pr<seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="14.6" punct="pe:10">G<seg phoneme="e" type="vs" value="1" rule="250" place="10">œ</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>