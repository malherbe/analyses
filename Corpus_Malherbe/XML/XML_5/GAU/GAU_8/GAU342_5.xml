<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU342" modus="cm" lm_max="12">
				<head type="main">LE SONNET</head>
				<head type="sub_1">A maître Claudius Popelin, émailleur et poëte.</head>
				<head type="sub_1">SONNET III</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.4">S<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="1.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg>s</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">Cr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg>s</w>, <w n="2.4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>str<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="2.5">d</w>’<w n="2.6" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="3.8">g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="4.2" punct="vg:3">v<seg phoneme="e" type="vs" value="1" rule="383" place="2">e</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="4.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="4.5" punct="vg:6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg">oin</seg>g</w>, <w n="4.6">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ts</w> <w n="4.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="4.9" punct="pt:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">d<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="5.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.8" punct="pv:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg>s</w> ;</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4" punct="vg:6">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" punct="vg">un</seg></w>, <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.8" punct="vg:12">f<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="7.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="7.6" punct="vg:12">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="8.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="8.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.7" punct="pt:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="11">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="9.4" punct="vg:6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>c</w>, <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.7">h<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.8" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2" punct="vg:3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>ts</w>, <w n="10.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="10.4" punct="vg:6">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg>s</w>, <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="10.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r</w> <w n="10.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="11.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="11.6">R<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="11.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="11.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.9" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">aî</seg>t</w> <w n="12.6">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <hi rend="ital"><w n="13.2" punct="vg:3">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w></hi>, <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <hi rend="ital"><w n="13.4" punct="vg:6">D<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w></hi>, <w n="13.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="13.8"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
					<l n="14" num="4.3" lm="12"><w n="14.1" punct="vg:3">Cl<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2">i</seg><seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s</w>, <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="14.3">l</w>’<w n="14.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="vg">a</seg>il</w>, <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.8" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">14 juillet 1870</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>