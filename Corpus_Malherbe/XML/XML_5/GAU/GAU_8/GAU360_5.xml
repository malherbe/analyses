<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">APPENDICE</head><div type="poem" key="GAU360" modus="cp" lm_max="11">
					<head type="number">IV</head>
					<head type="main">AVE MARIA</head>
					<head type="form">CHANT</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><space quantity="10" unit="char"></space><hi rend="ital"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="e" type="vs" value="1" rule="GAU360_1" place="2">e</seg></w> <w n="1.2" punct="pe:4">M<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ri<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg></w> !</hi> <w n="1.3">R<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.5" punct="pe:8">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</w> !</l>
						<l n="2" num="1.2" lm="8"><space quantity="10" unit="char"></space><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="2.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="dp:8">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="3" num="1.3" lm="8"><space quantity="10" unit="char"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="3.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7" punct="vg:8">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
						<l n="4" num="1.4" lm="11"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="4.4" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="4.5">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.6" punct="vg:6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="4.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.10">j</w>’<w n="4.11" punct="pe:11"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pe">e</seg></w> !</l>
						<l n="5" num="1.5" lm="8"><space quantity="10" unit="char"></space><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.5" punct="dp:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="6" num="1.6" lm="8"><space quantity="10" unit="char"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.3" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>s</w> ! <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="6.6" punct="pe:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
						<l n="7" num="1.7" lm="8"><space quantity="10" unit="char"></space><w n="7.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.3" punct="vg:4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</w>, <w n="7.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="7.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="7.7" punct="pv:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="8" num="1.8" lm="8"><space quantity="10" unit="char"></space><w n="8.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w>-<w n="8.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="8.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w>-<w n="8.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="8"><space quantity="10" unit="char"></space><hi rend="ital"><w n="9.1" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="e" type="vs" value="1" rule="GAU360_1" place="2">e</seg></w> <w n="9.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ri<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w></hi>, <w n="9.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ls</w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w></l>
						<l n="10" num="2.2" lm="8"><space quantity="10" unit="char"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="10.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.7" punct="pv:8">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="11" num="2.3" lm="8"><space quantity="10" unit="char"></space><w n="11.1">B<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.4" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></w>.</l>
						<l n="12" num="2.4" lm="11"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.4" punct="vg:3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3" punct="vg">en</seg></w>, <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="12.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="12.10" punct="pe:11">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pe">e</seg></w> !</l>
						<l n="13" num="2.5" lm="8"><space quantity="10" unit="char"></space><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="13.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">fr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.7">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="14" num="2.6" lm="8"><space quantity="10" unit="char"></space><w n="14.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="14.4">l</w>’<w n="14.5" punct="pe:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pe">en</seg>t</w> !</l>
						<l n="15" num="2.7" lm="8"><space quantity="10" unit="char"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="354" place="1">E</seg>x<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="15.2" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="15.3">c</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="15.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="2.8" lm="8"><space quantity="10" unit="char"></space><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="16.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="16.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="16.6" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="8"><space quantity="10" unit="char"></space><hi rend="ital"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="e" type="vs" value="1" rule="GAU360_1" place="2">e</seg></w> <w n="17.2" punct="pe:4">M<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ri<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg></w> !</hi> <w n="17.3" punct="dp:5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="dp">ai</seg>s</w> : <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="17.5" punct="pe:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</w> !</l>
						<l n="18" num="3.2" lm="8"><space quantity="10" unit="char"></space><w n="18.1">L</w>’<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="18.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.6">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="19" num="3.3" lm="8"><space quantity="10" unit="char"></space><w n="19.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.5" punct="pe:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</w> !</l>
						<l n="20" num="3.4" lm="11"><w n="20.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>x</w> <w n="20.2" punct="pe:3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pe ti">ai</seg>t</w> ! — <w n="20.3">T<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.4" punct="pt:8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt ti">é</seg></w>. — <w n="20.5">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>t</w> <w n="20.6" punct="pe:11">m<seg phoneme="i" type="vs" value="1" rule="493" place="10">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pe">e</seg></w> !</l>
						<l n="21" num="3.5" lm="8"><space quantity="10" unit="char"></space>«<w n="21.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="21.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="21.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.5">j</w>’<w n="21.6" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="22" num="3.6" lm="8"><space quantity="10" unit="char"></space><w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2" punct="pe:2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe">i</seg>ls</w> ! <w n="22.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="22.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="22.6" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !»</l>
						<l n="23" num="3.7" lm="8"><space quantity="10" unit="char"></space><w n="23.1" punct="vg:2">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="23.2" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.4" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="24" num="3.8" lm="8"><space quantity="10" unit="char"></space><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="24.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="24.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="24.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="24.7" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
					</lg>
				</div></body></text></TEI>