<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
				<title type="sub">Poèmes absents des précédentes éditions</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>504 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">GAU_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Th._Gautier_qui_ne_figureront_pas_dans_ses_%C5%93uvres</idno>
						<p>Exporté de Wikisource le 26 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
								<author>Théophile Gautier</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k72036w</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Impr. particulière</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
				<p>Cette édition ne contient que les poèmes de Th. Gautier non inclus dans les précédentes éditions.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU373" modus="cm" lm_max="12">
				<head type="main">DISTIQUE <lb></lb>POUR UN DESSIN DU PEINTRE P. D. C.</head>
				<head type="sub_2"><hi rend="ital">Un monsieur à lunettes faisant le bonheur d’une femme</hi></head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="1.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">n<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="2.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.8">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="2.11" punct="pt:12">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<closer>
					<note type="footnote" id="">
						Le peintre P. D. C. aime à se dégourdir de son allégorisme officiel par des charges d’une bouffonnerie violente. Ce n’est pas la seule de sa façon que Gautier ait illustrée d’une légende congruante.<lb></lb>
						<lb></lb>
						Quelques-unes des pièces précédentes ont paru en 1863, avec la signature A (anonyme), dans le <hi rend="ital">Parnasse satyrique du XIXe siècle</hi>, publié à l’étranger par M. P.-M. Nous tenons de l’éditeur que Gautier lui avait écrit pour désavouer à l’avance tout ce qui porterait son nom dans ce recueil clandestin.<lb></lb>
						Sans mettre en doute la véracité de M. P.-M., on peut s’étonner de la susceptibilité de Gautier. Il disait volontiers ses priapées, si singulièrement solennelles, et n’était pas sans y attacher du prix. Un petit homme de lettres qu’il en avait régalé mal à propos, par une nuit de gelée, lui en avait même fait, en 1853, des reproches rimés et publics.<lb></lb>
						Voici ce qu’on avait pu lire dans <hi rend="ital">le Cœur et l’Estomac</hi>, de M. Alfred Asseline, poète déplorable, mais illustre débordé (Michel Lévy, in-18):
						<lb></lb>
						<lb></lb>
						SUR DES VERS INÉDITS DE THÉOPHILE GAUTIER <lb></lb>
						<lb></lb>
						I <lb></lb>
						<lb></lb>
							Après le ballet, arpentant l’asphalte,<lb></lb>
							Gautier nous a dit des vers indécents.<lb></lb>
							Le ciel était pur comme un ciel de Malte,<lb></lb>
							Et le vent du nord a glacé nos sens.<lb></lb>
						<lb></lb>
							Gautier nous a dit, sous ce vent d’automne,<lb></lb>
							Sous le regard froid des astres d’argent,<lb></lb>
							Les vers <hi rend="ital">sur les poix, l’ode à la Colonne</hi>,<lb></lb>
							Dont s’effraîrait même un ancien sergent.<lb></lb>
						<lb></lb>
							J’admirais la forme et l’éclat bizarre<lb></lb>
							De ces vers taillés dans le marbre dur…<lb></lb>
							Mais, dis, que t’ont fait Paros et Carrare,<lb></lb>
							Jadis façonnés au goût le plus pur ?<lb></lb>
						<lb></lb>
							Sculpteur, qu’ont-ils fait, pour qu’aux jours moroses,<lb></lb>
							Où le spleen te suit d’un pas diligent,<lb></lb>
							Tu fasses courir dans leurs veines roses<lb></lb>
							Le poison subtil de ton vif-argent ?<lb></lb>
						<lb></lb>
						<lb></lb>
						II <lb></lb><lb></lb>
							Tu veux donc avoir aussi ton musée<lb></lb>
							Où tu montreras, comme Dupuytren,<lb></lb>
							Plongeant ton scalpel dans la chair blessée,<lb></lb>
							Ce que fait le vice aux os qu’il étreint.<lb></lb>
						<lb></lb>
							Ce savant bourreau porta la lumière<lb></lb>
							Jusqu’au fond des corps qu’il avait meurtris;<lb></lb>
							Mais l’âme amollie a plus d’un ulcère<lb></lb>
							Dont tu fouilleras les contours flétris.<lb></lb>
						<lb></lb>
							Eh bien ! chante donc, chercheur de problèmes,<lb></lb>
							Prêtre de Vénus qu’on voit aux cités,<lb></lb>
							Les plaisirs malsains, groupes aux faces blêmes,<lb></lb>
							Qui vit et se meut dans tes vers sculptés.<lb></lb>
						<lb></lb>
							Moi, cherchant les quais où le soleil brille,<lb></lb>
							J’irai contempler les deux yeux ardents<lb></lb>
							Et l’ovale frais d’une belle fille<lb></lb>
							Qui passe au grand jour, le sourire aux dents.<lb></lb>
						<lb></lb>
					</note>
				</closer>
			</div></body></text></TEI>