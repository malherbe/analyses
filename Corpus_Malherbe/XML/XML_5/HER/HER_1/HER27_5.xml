<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Épigrammes bucoliques</head><div type="poem" key="HER27" modus="cm" lm_max="12">
						<head type="main">Épigramme votive</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="1.2">r<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="pe">è</seg>s</w> ! <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.7" punct="pe:12">D<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
							<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="2.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="2.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="2.5" punct="vg:6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="2.9">p<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="3" num="1.3" lm="12"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="3.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rd</w> <w n="3.7" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>cl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="4.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="4.9" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">J<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>s</w>-<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="5.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="5.4" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt">a</seg>rc</w>. <w n="5.5" punct="vg:5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>s</w>, <w n="5.6" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>vi<seg phoneme="?" type="va" value="1" rule="162" place="8">en</seg>t</w>-<w n="5.8"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="5.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.11">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.5" punct="pi:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pi ti">oi</seg>s</w> ? — <w n="6.6">c</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="6.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="6.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="6.10">n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>fl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="7" num="2.3" lm="12"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="7.5">n</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.9" punct="tc:12">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="ti">er</seg></w> —</l>
							<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="8.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="8.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="8.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="8.11" punct="pi:12">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4" punct="pt:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>s</w>. <w n="9.5">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="8">œ</seg>il</w> <w n="9.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="10.3">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">cu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>r</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.9">l</w>’<w n="10.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
							<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="11.8" punct="pv:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="12.3" punct="pt:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt ti">e</seg></w>. — <w n="12.4">T<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="12.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">j</w>’<w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="12.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="12.10">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="12.11" punct="pi:12">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pi">ai</seg>ts</w> ?</l>
							<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="13.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>th<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="13.7" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>s</w>,</l>
							<l n="14" num="4.3" lm="12"><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="14.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="14.10" punct="pt:12">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>