<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Épigrammes bucoliques</head><div type="poem" key="HER32" modus="cm" lm_max="12">
						<head type="main">Le Laboureur</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="y" type="vs" value="1" rule="457" place="6" punct="vg">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.6" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>g</w>, <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>cs</w> <w n="1.9" punct="vg:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2" punct="vg:3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274" place="5">ui</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>lx</w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.10" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>x</w> <w n="4.9" punct="pv:12">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="11">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pv">an</seg>s</w> ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="5.3" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg>s</w>, <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rd</w>’<w n="5.5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="5.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="5.7" punct="vg:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5">v<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="6.9">Rh<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="7" num="2.3" lm="12"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="7.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.9" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="8" num="2.4" lm="12"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2" punct="vg:2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.6" punct="pv:6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="pv">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="8.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.9">qu<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w>-<w n="8.10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>gts</w> <w n="8.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.4" punct="vg:3">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="9.6" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg">e</seg>il</w>, <w n="9.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="9.9"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="9.11" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="10.7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.10" punct="pv:12">fr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="11.2">v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.4" punct="vg:6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="11.6">vi<seg phoneme="e" type="vs" value="1" rule="383" place="8">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="11.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rds</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="12.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="12.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="12.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.11">gl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w>-<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="13.7" punct="vg:9">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg></w>, <w n="13.8">ch<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="13.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="13.10" punct="vg:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rts</w>,</l>
							<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>ps</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="14.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="14.8">l</w>’<w n="14.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">É</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>