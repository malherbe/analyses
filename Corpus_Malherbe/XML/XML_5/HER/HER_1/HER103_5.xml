<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Nature et le Rêve</head><head type="main_subpart">La Mer de Bretagne</head><div type="poem" key="HER103" modus="cm" lm_max="12">
						<head type="main">Soleil couchant</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>cs</w> <w n="1.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>ts</w>, <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="1.6" punct="vg:12">gr<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>t</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="2.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2" punct="vg:2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg></w>, <w n="3.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="3.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="3.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.8">d</w>’<w n="3.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="4.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="4.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="4.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.9" punct="pt:12">f<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>t</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="5.4">c</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.7" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.9" punct="pt:10">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="pt">e</seg></w>. <w n="5.10">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.11">n<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d</w></l>
							<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:2">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>t</w>, <w n="6.3">l</w>’<w n="6.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="6.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.9">ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="6.11" punct="pv:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="7" num="2.3" lm="12"><w n="7.1" punct="vg:1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>l</w>, <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">An</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.9" punct="vg:12">br<seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">r<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg></w> <w n="8.8">s</w>’<w n="8.9" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="9.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="9.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="9">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="9.9" punct="vg:12">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2" punct="vg:3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4" punct="vg:6">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg>s</w>, <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>x</w> <w n="10.8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
							<l n="11" num="3.3" lm="12"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="11.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.6" punct="pt:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="pt">a</seg>il</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1">L</w>’<w n="12.2">h<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="12.5">s</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="12.8">l</w>’<w n="12.9" punct="vg:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="13.4" punct="vg:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="13.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="13.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="13.10" punct="vg:12">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="14" num="4.3" lm="12"><w n="14.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="14.8">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="pt">a</seg>il</w>.</l>
						</lg>
					</div></body></text></TEI>