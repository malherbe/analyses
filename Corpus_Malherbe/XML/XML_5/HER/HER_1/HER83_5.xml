<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><head type="main_subpart">Les conquérants</head><div type="poem" key="HER83" modus="cm" lm_max="12">
						<head type="main">L’Ancêtre</head>
						<opener>
							<salute>À Claudius Popelin.</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.7"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="1.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
							<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-27" place="4">e</seg></w> <w n="2.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="2.7">C<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l</w> <w n="3.8">n</w>’<w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="3.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="3.11">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="4.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ils</w> <w n="4.9" punct="pt:12">t<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="5.3" punct="vg:3">li<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="5.4">C<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="5.5" punct="vg:6">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" punct="vg">e</seg>s</w>, <w n="5.7">si<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="5.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.5" punct="vg:6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>x</w>, <w n="6.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="6.8">l</w>’<w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="7" num="2.3" lm="12"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">An</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="7.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.5">p<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="7.6">f<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="8" num="2.4" lm="12"><w n="8.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="8.3">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>lf<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="8.8" punct="pt:12">Fl<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="9.4" punct="vg:6">n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="9.5" punct="vg:9">Cl<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="8">i</seg><seg phoneme="y" type="vs" value="1" rule="450" place="9" punct="vg">u</seg>s</w>, <w n="9.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.7" punct="vg:12">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="10.7">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="10.8">r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg>x</w></l>
							<l n="11" num="3.3" lm="12"><w n="11.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg>ï<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="11.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="7">e</seg>r</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="11.7" punct="pv:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="12.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</w></l>
							<l n="13" num="4.2" lm="12"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>il</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="13.9">m<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.5">C<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="14.6">d</w>’<w n="14.7" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pt">o</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>