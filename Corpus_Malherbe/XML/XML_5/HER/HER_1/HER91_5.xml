<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’Orient et les Tropiques</head><div type="poem" key="HER91" modus="cm" lm_max="12">
					<head type="main">Le Samouraï</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">D</w>’<w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="1.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>gt</w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="1.5">fr<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.8" punct="vg:12">b<seg phoneme="i" type="vs" value="1" rule="468" place="11">î</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="2.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.4">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="2.5">tr<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="2.7">f<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="2.8" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3" punct="vg:3">v<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.6">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="8">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="3.9" punct="vg:12">pl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="4.8" punct="pt:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3" punct="pt:2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pt">i</seg></w>. <w n="5.4">S<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.6" punct="vg:6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>c</w>, <w n="5.7">l</w>’<w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>il</w> <w n="5.9" punct="vg:10">h<seg phoneme="o" type="vs" value="1" rule="318" place="10" punct="vg">au</seg>t</w>, <w n="5.10"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l</w> <w n="5.11" punct="pt:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></w>.</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.6">gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="7" num="2.3" lm="12"><w n="7.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4" punct="vg:6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="7.7">l</w>’<w n="7.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="10" punct="vg">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="2.4" lm="12"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">bl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">H<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>z<seg phoneme="e" type="vs" value="1" rule="HER89_1" place="6">e</seg>n</w> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7" punct="pt:12">T<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>k<seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg>g<seg phoneme="a" type="vs" value="1" rule="343" place="11">a</seg>w<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="9.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.9" punct="vg:12">pl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3" punct="vg:4">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5">s<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="10.8">br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="10.9" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.3">cr<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="11.4" punct="vg:6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="11.5">g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>il</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.4" punct="pt:3">v<seg phoneme="y" type="vs" value="1" rule="457" place="3" punct="pt">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>. <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">I</seg>l</w> <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.9">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.10">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="12.11" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="13.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>f</w> <w n="13.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="13.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="13.9">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>il</w></l>
						<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="366" place="4">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="14.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="14.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="14.10" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>