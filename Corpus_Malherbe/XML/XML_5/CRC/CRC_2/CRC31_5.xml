<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC31" modus="sp" lm_max="8">
		<head type="main">Toulouse-Lautrec</head>
			<opener>
				<salute><hi rend="smallcap">À JEAN PELLERIN</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.4">gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="1.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cs</w></l>
			<l n="2" num="1.2" lm="6"><space unit="char" quantity="4"></space><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>z</w> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
			<l n="3" num="1.3" lm="8">— <w n="3.1">C</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="3.3" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>r</w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="3.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6" punct="pe:8">m<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ti">e</seg></w> ! —</l>
			<l n="4" num="1.4" lm="8"><w n="4.1">T</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.5" punct="pt:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>g</w>.</l>
		</lg>
		<lg n="2">
			<l n="5" num="2.1" lm="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="5.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
			<l n="6" num="2.2" lm="6"><space unit="char" quantity="4"></space><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="6.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w></l>
			<l n="7" num="2.3" lm="8">— <w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="7.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.5" punct="tc:8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ti">a</seg>l</w> —</l>
			<l n="8" num="2.4" lm="8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.7" punct="pt:8">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
		</lg>
		<lg n="3">
			<l n="9" num="3.1" lm="8"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3" punct="vg:3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>t</w>, <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.6">cl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
			<l n="10" num="3.2" lm="6"><space unit="char" quantity="4"></space><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="10.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
			<l n="11" num="3.3" lm="8"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="11.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="12" num="3.4" lm="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt</w> <w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="12.6" punct="pt:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pt">œu</seg>rs</w>.</l>
		</lg>
		<lg n="4">
			<l n="13" num="4.1" lm="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="13.4">r<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
			<l n="14" num="4.2" lm="6"><space unit="char" quantity="4"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w></l>
			<l n="15" num="4.3" lm="8"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="15.6" punct="vg:8">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>gts</w>,</l>
			<l n="16" num="4.4" lm="8"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="16.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="16.4">d</w>’<w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="16.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.7" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
		</lg>
	</div></body></text></TEI>