<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA18" modus="cm" lm_max="10">
				<head type="number">VIII</head>
				<head type="main">Le Cid</head>
				<head type="form">Romance</head>
				<head type="tune">Air des <hi rend="ital">Folies d’Espagne</hi></head>
				<lg n="1">
					<l n="1" num="1.1" lm="10"><w n="1.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>t</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">C<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d</w> <w n="2.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="2.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.7" punct="vg:10">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>r</w>,</l>
					<l n="3" num="1.3" lm="10"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3" punct="vg:4">gu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="3.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>ds</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="3.8" punct="vg:10">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="10"><w n="4.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="4.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ct<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="4.7">l</w>’<w n="4.8" punct="dp:10">h<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="dp">eu</seg>r</w> :</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10"><w n="5.1">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg>t</w> : <w n="5.4">V<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.7" punct="pv:10">M<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
					<l n="6" num="2.2" lm="10"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>s</w> <w n="6.6" punct="pt:10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
					<l n="7" num="2.3" lm="10"><w n="7.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.6">m</w>’<w n="7.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.4" lm="10"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.4">c<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="8.8">l</w>’<w n="8.9" punct="pt:10">h<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10">‒  <w n="9.1" punct="vg:2">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="9.2">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.8" punct="pe:10">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
					<l n="10" num="3.2" lm="10"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="10.8" punct="dp:10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="dp">œu</seg>r</w> :</l>
					<l n="11" num="3.3" lm="10"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ts</w> <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.6" punct="vg:10">v<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="12" num="3.4" lm="10"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="12.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.6">d<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="12.8">l</w>’<w n="12.9" punct="pt:10">h<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="10"><w n="13.1">M<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="13.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.5" punct="vg:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="10"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="14.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w></l>
					<l n="15" num="4.3" lm="10"><w n="15.1">D</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="15.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="15.7" punct="vg:10">f<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="16" num="4.4" lm="10"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="16.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="16.7">l</w>’<w n="16.8" punct="pt:10">h<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="10"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">An</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="18" num="5.2" lm="10"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="18.3">chr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="18.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="18.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="18.6" punct="dp:10">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="dp">eu</seg>r</w> :</l>
					<l n="19" num="5.3" lm="10"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2" punct="vg:4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="19.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w>-<w n="19.4" punct="vg:7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>ls</w>, <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="19.7">v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="20" num="5.4" lm="10"><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="20.2" punct="vg:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="20.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="20.4" punct="vg:4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.6">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="20.8">l</w>’<w n="20.9" punct="pt:10">h<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>