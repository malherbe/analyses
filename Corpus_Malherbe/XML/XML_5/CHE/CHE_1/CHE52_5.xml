<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE52" modus="cm" lm_max="12">
					<head type="number">XVIII</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.6" punct="vg:6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6" punct="vg">om</seg></w>, <w n="1.7">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="1.8" punct="vg:9">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="vg">er</seg></w>, <w n="1.9">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>t</w> <w n="1.10" punct="vg:12">Ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">M<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.4" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="2.5">m<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.7">l</w>’<w n="2.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Cr<seg phoneme="y" type="vs" value="1" rule="454" place="1">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="3.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="3.9" punct="pe:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>x</w> !</l>
						<l n="4" num="1.4" lm="12"><w n="4.1" punct="vg:3">Ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.3" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="4.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="4.7">br<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="4.8" punct="vg:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="5.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r</w> <w n="5.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="5.6" punct="vg:9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg">e</seg>s</w>, <w n="5.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.5" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>t</w>, <w n="6.6" punct="vg:8">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>, <w n="6.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.9" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.5">l</w>’<w n="7.6">h<seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="6">en</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="7.9">d<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="7.10">pr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="8" num="1.8" lm="12"><w n="8.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.3">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.6">d</w>’<w n="8.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
						<l n="9" num="1.9" lm="12"><w n="9.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="9.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.6" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>x</w>, <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.9">Ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="10" num="1.10" lm="12"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="10.4">t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.8">l</w>’<w n="10.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="11" num="1.11" lm="12">— <w n="11.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg></w>, <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="11.6">s</w>’<w n="11.7" punct="vg:11"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="vg">e</seg></w>, <w n="11.8" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</w>,</l>
						<l n="12" num="1.12" lm="12"><w n="12.1">R<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="12.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="12.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
					</lg>
				</div></body></text></TEI>