<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS</head><div type="poem" key="CHE70" modus="cm" lm_max="12">
					<head type="number">V</head>
					<head type="main">Traduction de Bion</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">B<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4" punct="vg:6">V<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="1.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>t</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w> <w n="2.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>t</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4" punct="vg:6">t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>l</w>, <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>d</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="3.9" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7">l<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.8" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.8" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg>s</w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.6">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="6.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg>s</w>.</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">J</w>’<w n="7.2" punct="pv:2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pv">e</seg></w> ; <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="7.8" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="3">ym</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="8.8" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="9" num="1.9" lm="12"><w n="9.1" punct="vg:2">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.7" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>t</w>,</l>
						<l n="10" num="1.10" lm="12"><w n="10.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="10.5" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>rs</w>, <w n="10.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.9" punct="pt:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg>t</w>.</l>
					</lg>
				</div></body></text></TEI>