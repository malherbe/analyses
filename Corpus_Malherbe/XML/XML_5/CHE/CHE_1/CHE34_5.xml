<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="CHE34" modus="cm" lm_max="12">
					<head type="number">VIII</head>
					<head type="main">Les Navigateurs</head>
					<head type="form">(Idylle maritime).</head>
					<p><hi rend="ital">
					A. — Enfin nous avons passé dans la nuit le cap de
					Malea. Les dieux soient loués… J’ai fait un bien long voyage.
					Avant que nous nous embarquions tous ensemble à Syracuse,
					j’avais parcouru la côte de Marseille et Tyrrhénie, etc. Certes
					le monde est grand. Mais voici notre Grèce chérie. Et vous,
					compagnons, d’où veniez-vous quand nous nous sommes embarqués
					ensemble sur ce vaisseau ?
					B. — Moi, j’ai été ici…
					Γ. — Moi, là…
					Δ. — Moi, j’ai été jusqu’à Tartessus, au delà des colonnes
					d’Alcide, aux embouchures du Betis… là… là… Ah ! vous
					n’avez rien vu, vous tous… je brûle de me revoir à Lesbos,
					ma patrie.
					E. — Pour moi, je n’ai été qu’à… et je brûle de me revoir
					à Lesbos… O belle mer Egée !… les îles éparses sur tes flots
					azurés sont comme les étoiles dans la nuit… et toi, Lesbos, la
					plus belle de toutes…
					Z. — Et les sommets de Naxos bruyants de bacchanales.
					H. —Et Samos, et Junon ?… etc., et quoi ! ma Délos
					sera-t-elle la dernière ?… où il y a ceci… cela. </hi>
					</p>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="1.4" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="1.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="1.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>g<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="2.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.4">C<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="5">yn</seg>th<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6">r<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="2.7">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">s</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="3.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="3.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>vr<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>ils</w> <w n="3.9">s<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="4.2">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">fr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="4.7" punct="pt:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<p><hi rend="ital">
					Mais je ne sais quel vent froid nous vient de l’est et
					semble annoncer une tempête… Voilà un grain qui se
					forme.
					A. —Oh ! non… non…
					K. — Pour moi, je ne peux point vanter ma patrie. Les
					dieux ont peu fait pour elle… Mycone n’a que des figues et
					des raisins… C’est un rocher aride… Mais c’est ma patrie…
					C’est là que j’ai ouvert les yeux pour la première fois… Là
					sont mes parents, ma famille… mes premiers amis… Je m’y
					retrouverai avec joie, je n’en sortirai plus, et je la préférerai
					à toutes les autres que j’ai vues, quoique plus belles. Mais
					voyez, la mer devient houleuse… je crains bien un orage…
					A, B, Γ, Δ,</hi>(ensemble). — <hi rend="ital">Ma patrie est la plus belle, etc. </hi>
					Le pilote. — <hi rend="ital">Paix  ! quel bruit  ! on ne s’entend pas. Est-ce
					le temps de discuter ? Voici une tempête terrible…
					— Baisse la voile… prends ce câble… Je crois que tous les
					démons sont à cheval sur cette vague… Quel vent !… Voilà
					la voile en pièces… </hi>
					Les voyageurs pleurent et gémissent. — <hi rend="ital">Ah  ! pourquoi ai-je
					quitté ma famille, etc. Ah ! qu’avais-je à faire en tel lieu…
					Ah ! ne pouvais-je me passer des richesses de telle ou telle
					contrée, etc. O Jupiter de tel lieu ! Neptune Ténien, Apollon
					Délien, Junon Samienne (chacun le dieu de son pays). </hi>
					Le pilote. — <hi rend="ital">Paix donc !… </hi>
					Les voyageurs. — <hi rend="ital">Cent moutons… Mille brebis… Cent
					taureaux…
					O dieux ! sauvez-nous !… </hi>
					Le pilote. — <hi rend="ital">O quels cris ! vous nous rendez sourds, et les
					dieux aussi… Simon, tire ce câble… Au lieu de crier, tra-
					vaillez et aidez-nous… Voyez-les un peu qui disputent et
					crient entre eux ; et, dans le danger, ils ne savent que pleu-
					rer et se mettre à genoux et nommer tous les dieux par leurs
					noms et surnoms. Travaillez… cela vaudra mieux. Matelot,
					tiens ferme, etc. Oh ! cette vague me cassera le gouvernail…
					Dieux ! nous sommes engloutis… Non, ce n’est rien… Eh
					bien, que fais-tu là ? toi, Siphniote imbécile ?… que ne vas-tu
					aider ?
					— Je suis un homme libre.
					— Homme libre, travaille, de peur que dans peu… ta li-
					berté ne soit esclave de Pluton… Ah ! c’est fini…
					Voilà tout le peuple accouru sur la côte… ils sont bonnes
					gens. Ils venaient nous voir noyer, et ils nous auraient fait
					de beaux cénotaphes de marbre du Ténare, avec des épita-
					phes où ils auraient cité notre exemple à ceux qui s’embar-
					quent. Ils sont, par Jupiter, humains et secourables. Il vaut
					mieux toutefois leur épargner ces soins.
					— Allons, nous allons relâcher sur la côte… Eh bien ! vous
					qui faisiez des voeux ?… Vos cent brebis, cent boeufs, cent
					moutons ? Voyons, donnez-nous-en un ou deux à compte sur
					le rivage, ça nous refera un peu. </hi>
					A. — <hi rend="ital">Moi, je n’ai rien promis… je ne suis pas riche. </hi>
					Le pilote. — <hi rend="ital">Comment, tu n’es pas riche ? et ces belles
					étoffes, et ces belles marchandises que tu as apportées
					de Tartessus, de Bétis, etc. </hi>(Il lui répète ses mêmes paroles.)
					Le Myconien. — <hi rend="ital">Moi, je suis pauvre comme ma patrie,
					mais pas assez pour ne pas pouvoir tous nous régaler d’un
					mouton, etc. </hi>
					B. — <hi rend="ital">Moi, j’ai promis, mais je tiendrai mon voeu quand
					je serai sur le rivage même de mon île. </hi>
					T. —<hi rend="ital">Mais, patron, tu as interrompu nos voeux… les dieux
					n’ont pas pu les entendre :</hi>
					</p>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="5.4">t<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="5.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.9" punct="ps:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
					</lg>
					<p>
					Ils nous exauçaient d’avance ; nous ne sommes tenus à
					rien. Pour une autre fois nous gardons nos offrandes.
					Le pilote :
					</p>
					<lg n="3">
						<l n="6" num="3.1" lm="12"><w n="6.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.6">di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="6.8" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</w>.</l>
						<l n="7" num="3.2" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pv">in</seg></w> ; <w n="7.6" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="9">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="pv">e</seg></w> ; <w n="7.7" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">ez</seg></w>.</l>
						<l n="8" num="3.3" lm="12"><w n="8.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="8.6" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="9" num="3.4" lm="12"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="9.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ls</w> <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w> <w n="9.8" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="10" num="3.5" lm="12"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="10.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ttr<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="10.4" punct="pv:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv">ou</seg>t</w> ; <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="10.7">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="10.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="10.9" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rds</w>.</l>
						<l n="11" num="3.6" lm="12"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="11.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3" punct="vg:6">p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="11.9" punct="pe:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>rs</w> !</l>
						<l n="12" num="3.7" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="12.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="12.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>x</w> <w n="12.8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="12.9">p<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="13" num="3.8" lm="12"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="13.5">St<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>x</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="13.8" punct="pt:12">C<seg phoneme="i" type="vs" value="1" rule="493" place="11">y</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>