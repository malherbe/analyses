<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE57" modus="cm" lm_max="12">
					<head type="number">XXIII</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="1.2" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="1.7" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12" punct="vg">ê</seg>ts</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="5">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="2.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.7" punct="vg:12">C<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12" punct="vg">è</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="3.8"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.9" punct="pt:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="4.2" punct="vg:2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w>, <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="4.5">c<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="4.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="4.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="4.9">f<seg phoneme="œ" type="vs" value="1" rule="406" place="11">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="5" num="1.5" lm="12"><w n="5.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>ts</w> <w n="5.6" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>x</w> <w n="6.2">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.4">v<seg phoneme="ø" type="vs" value="1" rule="248" place="6">œu</seg>x</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="6.6">C<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="6.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="354" place="10">e</seg>x<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</w>.</l>
						<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.6">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="7.7">b<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="8" num="1.8" lm="12"><w n="8.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="8.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="8.8" punct="pv:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="9.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="9.8" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</w>,</l>
						<l n="10" num="1.10" lm="12"><w n="10.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="10.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="10.7">l</w>’<w n="10.8" punct="dp:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="dp">ou</seg>r</w> :</l>
						<l n="11" num="1.11" lm="12"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="11.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>gt</w> <w n="11.6">Dr<seg phoneme="i" type="vs" value="1" rule="d-4" place="8">y</seg><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="11.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="12" num="1.12" lm="12"><w n="12.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="?" type="va" value="1" rule="162" place="2">en</seg>t</w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="12.7">C<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="12.9" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>