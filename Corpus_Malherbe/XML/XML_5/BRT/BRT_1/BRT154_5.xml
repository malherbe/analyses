<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT154" modus="sm" lm_max="8">
				<head type="number">IV</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="1.5" punct="vg:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">C<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.3" punct="pv:8">p<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>r<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>l</w> ;</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="3.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.6" punct="vg:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8"><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg>s</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="pt:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</w>.</l>
					<l n="5" num="1.5" lm="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">S<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="pv:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="6" num="1.6" lm="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">L<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="6.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="7" num="1.7" lm="8"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="7.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.7" punct="pt:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></w>.</l>
					<l n="8" num="1.8" lm="8"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="8.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.6" punct="ps:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					<l n="9" num="1.9" lm="8"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="9.3">J<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="1.10" lm="8"><w n="10.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="ps:4">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="ps">a</seg></w>… <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.5" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></w>.</l>
					<l n="11" num="1.11" lm="8"><w n="11.1">L</w>’<w n="11.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.4">d</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="1.12" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="13" num="1.13" lm="8"><w n="13.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="14" num="1.14" lm="8"><w n="14.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">l</w>’<hi rend="ital"><w n="14.4" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w></hi>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Cèdre.</note>
					</closer>
			</div></body></text></TEI>