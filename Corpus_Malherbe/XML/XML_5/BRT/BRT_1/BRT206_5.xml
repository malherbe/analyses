<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT206" modus="cm" lm_max="12">
				<head type="number">XXXI</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="1.3" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="1.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="1.6" punct="vg:9">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9" punct="vg">ai</seg></w>, <w n="1.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="1.8">m</w>’<w n="1.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="2.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.8">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>t</w> <w n="2.9"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="2.10">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.11" punct="pt:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="3.8">b<seg phoneme="ø" type="vs" value="1" rule="247" place="9">œu</seg>fs</w> <w n="3.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="3.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="3.11">pr<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
					<l n="4" num="2.2" lm="12"><w n="4.1" punct="vg:1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>t</w>, <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="4.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="4.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="170" place="4">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="4.8" punct="pt:12">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="5.6">s</w>’<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.8" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.9"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="5.10">m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="6" num="3.2" lm="12"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="6.4">bl<seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="6.8">s</w>’<w n="6.9" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="7" num="4.1" lm="12"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="7.8">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>gs</w>,</l>
					<l n="8" num="4.2" lm="12"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="8.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>ds</w> <w n="8.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>ts</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="9" num="5.1" lm="12"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.4">br<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>f</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>dj<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>f</w> <w n="9.6">d</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.8">f<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="10" num="5.2" lm="12"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="10.10">s<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="10.11"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="10.12" punct="pt:12">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="11" num="6.1" lm="12"><w n="11.1">Qu</w>’<w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.4" punct="dp:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="dp">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.6">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>t</w> <w n="11.8">l</w>’<w n="11.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="11.10">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg></w></l>
					<l n="12" num="6.2" lm="12"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="12.3" punct="vg:3">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="12.4" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="12.5" punct="vg:7">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg">e</seg></w>, <w n="12.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.8" punct="pe:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="7">
					<l n="13" num="7.1" lm="12"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.7" punct="vg:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="7.2" lm="12"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="14.4" punct="pt:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="pt">ain</seg></w>. <w n="14.5">Fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="8">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w>-<w n="14.6" punct="vg:10">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="10" punct="vg">e</seg></w>, <w n="14.7" punct="pe:12">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Ton, taon, ton, thon, ton, tonton, ton.</note>
					</closer>
			</div></body></text></TEI>