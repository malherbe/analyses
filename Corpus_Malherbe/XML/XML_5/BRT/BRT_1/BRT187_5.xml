<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT187" modus="cm" lm_max="12">
				<head type="number">XII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>gt</w> <w n="1.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="1.9">l</w>’<w n="1.10" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rph<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1" punct="vg:2">J<seg phoneme="a" type="vs" value="1" rule="310" place="1">ea</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.7" punct="pt:12">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.4">s</w>’<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="3.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.8">z<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="2.2" lm="12"><w n="4.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="4.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>s</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5" punct="vg:6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="4.8" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="5.7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>d</w> <w n="5.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="5.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</w></l>
					<l n="6" num="3.2" lm="12"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.8">l</w>’<w n="6.9" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="7" num="4.1" lm="12"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.4" punct="vg:4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="7.5">d</w>’<w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.7" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>p</w>, <w n="7.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>t</w> <w n="7.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="7.10" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="8" num="4.2" lm="12"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="8.8" punct="pt:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="9" num="5.1" lm="12"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="9.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="9.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.8" punct="vg:9">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9" punct="vg">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.9"><seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg></w> <w n="9.10" punct="vg:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="10" num="5.2" lm="12"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="10.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="10.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.6" punct="ps:9">l<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="ps">i</seg>s</w>… <w n="10.7">l</w>’<w n="10.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>lph<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Dé, dey, dais, dé, d.</note>
					</closer>
			</div></body></text></TEI>