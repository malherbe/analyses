<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT82" modus="cp" lm_max="12">
				<head type="number">LXXXII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="1.6">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.8">p<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="6"><space unit="char" quantity="12"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="2.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.6" punct="pt:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="pt">ain</seg></w>. »</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ts</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.6">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.9">b<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>il</w> <w n="3.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="3.11">b<seg phoneme="ø" type="vs" value="1" rule="400" place="12">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="2.2" lm="12"><w n="4.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4" punct="pt:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>x</w>. <w n="4.5">T<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="4.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="4.8" punct="pt:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">c<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="5.9" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="3.2" lm="6"><space unit="char" quantity="12"></space><w n="6.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3" punct="pv:6">vi<seg phoneme="e" type="vs" value="1" rule="383" place="5">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pv">a</seg>rd</w> ;</l>
					<l n="7" num="3.3" lm="12"><w n="7.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t</w> <w n="7.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="6">ai</seg>t</w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="7.6" punct="pv:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="8" num="3.4" lm="12"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w>-<w n="8.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>m<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="8.8" punct="pi:12">l<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg>rd</w> ? »</l>
				</lg>
				<lg n="4">
					<l n="9" num="4.1" lm="12"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.9" punct="dp:12">tr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="10" num="4.2" lm="6"><space unit="char" quantity="12"></space><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.4" punct="pt:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
					<l n="11" num="4.3" lm="12"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.6">vi<seg phoneme="e" type="vs" value="1" rule="383" place="5">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.8">p<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.9" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="4.4" lm="12"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5" punct="vg:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="12.8" punct="vg:9">g<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">î</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>s</w> <w n="12.11" punct="pi:12">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pi">eu</seg>x</w> ?</l>
					<l n="13" num="4.5" lm="12"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="13.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="13.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="13.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg">en</seg>t</w>, <w n="13.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="13.7">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="13.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="14" num="4.6" lm="6"><space unit="char" quantity="12"></space><w n="14.1">Pr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="14.4" punct="ps:6">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" punct="ps">en</seg></w>…</l>
					<l n="15" num="4.7" lm="12"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="15.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="6">ue</seg>il</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.5">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="15.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="15.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="15.8" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="16" num="4.8" lm="6"><space unit="char" quantity="12"></space><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="16.2" punct="ps:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="ps">i</seg></w>… <w n="16.3">c</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.6" punct="pt:6">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" punct="pt">en</seg></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Qui aime Martin aime son chien.</note>
					</closer>
			</div></body></text></TEI>