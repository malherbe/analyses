<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT103" modus="cp" lm_max="12">
				<head type="number">III</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>lph<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.2">K<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="1.7">n</w>’<w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="1.9">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>t</w> <w n="1.10">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="2.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="2.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="2.8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="2.10" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="3.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="3.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="3.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rds</w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.9" punct="pv:12">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pv">o</seg>l</w> ;</l>
					<l n="4" num="2.2" lm="12"><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w>-<w n="4.7" punct="vg:10">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>s</w>, <w n="4.8" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">E</seg>lv<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="5.6" punct="vg:6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="5.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.8" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>s</w>, <w n="5.9" punct="vg:12">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</w>,</l>
					<l n="6" num="3.2" lm="6"><space unit="char" quantity="12"></space><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6" punct="pt:6">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="7" num="3.3" lm="6"><space unit="char" quantity="12"></space><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">Ai</seg></w>-<w n="7.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="7.4" punct="pi:6"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="pi ps">en</seg>t</w> ? …</l>
					<l n="8" num="3.4" lm="12"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2" punct="pe:2">f<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="pe vg in">oi</seg></w> !, « <w n="8.3">s</w>’<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="8.5">m</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.7" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="vg">en</seg>t</w>, <w n="8.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="8.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.10">m</w>’<w n="8.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="8.12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="11">en</seg>t</w> <w n="8.13" punct="pe:12">gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Fa-mine.</note>
					</closer>
			</div></body></text></TEI>