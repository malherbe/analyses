<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT37" modus="cp" lm_max="12">
				<head type="number">XXXVII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.8">M<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="6"><space unit="char" quantity="12"></space><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>ts</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="3.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="1.4" lm="6"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="pi:6">br<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pi">i</seg>s</w> ?</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.8" punct="pv:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="6.7" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2" punct="vg:2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>gts</w>, <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.5" punct="vg:6">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="7.9">d</w>’<w n="7.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="7.11" punct="ps:12">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="ps">ain</seg></w>…</l>
					<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="8.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="8.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.10" punct="pe:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pe ps">in</seg></w> ! …</l>
					<l n="9" num="1.9" lm="12"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2" punct="vg:3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="9.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="9.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="9.5" punct="vg:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>, <w n="9.6">s</w>’<w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10">a</seg>y<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="9.9" punct="pv:12">r<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">br<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="10.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="10.8" punct="pt:12">br<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Où la chèvre est attachée, il faut qu’elle broute.</note>
					</closer>
			</div></body></text></TEI>