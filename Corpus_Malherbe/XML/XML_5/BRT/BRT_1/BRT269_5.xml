<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT269" modus="cp" lm_max="12">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="1.8" punct="pv:12">t<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>s</w></add></subst> ;</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>l</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <subst hand="ML" reason="analysis" type="completion"><del>....</del><add rend="hidden"><w n="2.10" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>s</w></add></subst> ;</l>
					<l n="3" num="1.3" lm="6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4" punct="pe:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg>s</w> !</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.7">d<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="4.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.9" punct="pe:12">fr<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg></w> !</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <subst hand="ML" reason="analysis" type="completion"><del>...</del><add rend="hidden"><w n="5.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w></add></subst> <w n="5.7">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8" punct="vg:8">cr<seg phoneme="i" type="vs" value="1" rule="469" place="8" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="5.10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="5.11" punct="dp:12">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg></w> :</l>
					<l n="6" num="1.6" lm="6"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="6.2" punct="vg:3">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="6.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</w>.</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w>-<w n="7.5" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w>-<w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="7.8">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>p</w> <w n="7.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="7.10" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="445" place="12" punct="vg">û</seg>r</w>,</l>
					<l n="8" num="1.8" lm="12"><w n="8.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.5" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="8.8" punct="vg:9">fl<seg phoneme="o" type="vs" value="1" rule="438" place="9" punct="vg">o</seg>ts</w>, <w n="8.9">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="8.10">m<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r</w></l>
					<l n="9" num="1.9" lm="6"><w n="9.1">D</w>’<subst hand="ML" reason="analysis" type="completion"><del>..</del><add rend="hidden"><w n="9.2" punct="vg:1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">I</seg>s</w></add></subst>, <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5" punct="pi:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></w> ?</l>
					<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>r</w>, <w n="10.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>.</del><add rend="hidden"><w n="10.6" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="6" punct="vg">è</seg>s</w></add></subst>, <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>t</del><add rend="hidden"><w n="10.9">t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w></add></subst> <w n="10.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="10.11">l</w>’<subst hand="ML" reason="analysis" type="completion"><del>r</del><add rend="hidden"><w n="10.12"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r</w></add></subst></l>
					<l n="11" num="1.11" lm="12"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="11.5">b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg></w> <w n="11.7" punct="vg:9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9" punct="vg">en</seg>t</w>, <w n="11.8">n<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="11.10">l</w>’<w n="11.11" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
					<l n="12" num="1.12" lm="6"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="426" place="1">ou</seg></w> <w n="12.2" punct="vg:2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>g</w>, <w n="12.3">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4" punct="pe:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Tamis<lb></lb>▪ amis<lb></lb>▪ mis<lb></lb>▪ is<lb></lb>▪ s</note>
					</closer>
			</div></body></text></TEI>