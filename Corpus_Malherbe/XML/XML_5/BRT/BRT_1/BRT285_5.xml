<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT285" modus="cm" lm_max="12">
				<head type="number">X</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="1.2" punct="dp:3">ph<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="1.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">Œ</seg>il</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.5" punct="vg:6">bi<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="1.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg>t</w> <w n="1.7" punct="vg:9">j<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="1.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.9" punct="vg:12">pl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">Ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">qu<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="2.9">b<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="2.10" punct="pv:12">fl<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</w> ;</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.2">t<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.9" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">V<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ts</w> <w n="4.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="4.3">d</w>’<w n="4.4" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>r</w>, <w n="4.5">d</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">d</w>’<w n="4.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="5.2" punct="dp:3">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="dp">a</seg>l</w> : <w n="5.3">V<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="5.7">h<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>t</w> <w n="5.8" punct="vg:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>rr<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.4">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="10">en</seg>s</w> <w n="6.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="6.7" punct="pv:12">fl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="7.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2">c<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>t</w> <w n="8.6" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">r<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">L<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.7" punct="vg:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3" punct="vg">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="10.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>xh<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="10.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="10.8" punct="pv:12">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.6">dr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="11.8" punct="pe:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pe">e</seg>l</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="12.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="12.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.6">qu</w>’<w n="12.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.9">c<seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="12.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.11" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="13.5">ch<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="13.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="13.7">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r</w> <w n="13.9" punct="dp:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="14" num="4.3" lm="12"><w n="14.1">Sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="2">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="14.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="14.5">l</w>’<w n="14.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.7" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pe">e</seg>l</w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Confucius.</note>
					</closer>
			</div></body></text></TEI>