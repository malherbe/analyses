<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT197" modus="cp" lm_max="12">
				<head type="number">XXII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">f<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>t</w> <w n="1.5">d</w>’<w n="1.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="1.7">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="1.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="11">e</seg></w> <w n="1.10" punct="vg:12">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="2.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">p<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd</w> <w n="2.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="2.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="3" num="1.3" lm="6"><space unit="char" quantity="12"></space><w n="3.1">Tr<seg phoneme="u" type="vs" value="1" rule="427" place="1">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="3.4" punct="pt:6">t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="4" num="2.1" lm="12"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">br<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt</w> <w n="4.8"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.10" punct="vg:12">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="5" num="2.2" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="5.8" punct="vg:10">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg">e</seg>s</w>, <w n="5.9" punct="vg:12">H<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.3" lm="6"><space unit="char" quantity="12"></space><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="6.5" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="7" num="3.1" lm="12"><w n="7.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">m</w>’<w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.6" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="7.7">qu</w>’<w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.9">s<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="7.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="3.2" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="8.2" punct="pe:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg></w> ! <w n="8.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="8">e</seg></w> <w n="8.7">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="9" num="3.3" lm="6"><space unit="char" quantity="12"></space><w n="9.1" punct="vg:2">Gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="9.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="9.4" punct="pe:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>p<seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="pe">ô</seg>ts</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="10" num="4.1" lm="12"><w n="10.1">L</w>’<w n="10.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="10.3">l</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="10.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="10.9">d</w>’<w n="10.10" punct="pv:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="11" num="4.2" lm="12"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="11.3" punct="vg:5">gr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.4" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.6">n</w>’<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="11.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="11.9">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="11.10" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="12" num="4.3" lm="6"><space unit="char" quantity="12"></space><w n="12.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="12.4" punct="pe:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="pe">o</seg>s</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="13" num="5.1" lm="12"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>br<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ct</w> <w n="13.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="14" num="5.2" lm="12"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="14.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="14.3">t<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="14.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="14.7">f<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.9" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="15" num="5.3" lm="6"><space unit="char" quantity="12"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.4">n</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.7" punct="pe:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe">u</seg>s</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="16" num="6.1" lm="12"><w n="16.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="16.4" punct="vg:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="16.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="16.8" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="17" num="6.2" lm="12"><w n="17.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="17.2">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>s</w> <w n="17.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="17.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="18" num="6.3" lm="6"><space unit="char" quantity="12"></space><w n="18.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="18.3" punct="pt:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg>s</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="7">
					<l n="19" num="7.1" lm="12"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="19.5" punct="pt:6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" punct="pt">e</seg>d</w>. <w n="19.6">S<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="19.8" punct="vg:9">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" punct="vg">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="19.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="20" num="7.2" lm="12"><w n="20.1">S</w>’<w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="20.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="20.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="20.6" punct="vg:12">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="21" num="7.3" lm="6"><space unit="char" quantity="12"></space><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="21.2">n</w>’<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="21.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.6" punct="pt:6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="8">
					<l n="22" num="8.1" lm="12"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="22.5">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="22.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="22.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="22.8">g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="22.9" punct="pv:12">gr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="23" num="8.2" lm="12"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="23.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="23.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="23.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r</w> <w n="23.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="23.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="24" num="8.3" lm="6"><space unit="char" quantity="12"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="24.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l</w> <w n="24.5" punct="pt:6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="9">
					<l n="25" num="9.1" lm="12"><w n="25.1">J</w>’<w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="25.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="25.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.5"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="25.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="25.7">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="25.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="25.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="25.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="25.11" punct="pv:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="26" num="9.2" lm="12"><w n="26.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t</w> <w n="26.5">d</w>’<w n="26.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="26.7">fl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="27" num="9.3" lm="6"><space unit="char" quantity="12"></space><w n="27.1">L</w>’<w n="27.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="27.5">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c</w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.7" punct="pt:6">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pt">o</seg>cs</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="10">
					<l n="28" num="10.1" lm="12"><w n="28.1">J</w>’<w n="28.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="28.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="28.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.5">d</w>’<w n="28.6" punct="pt:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pt">o</seg>r</w>. <w n="28.7">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="28.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="28.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="28.11" punct="pv:12">ru<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>nt</w> ;</l>
					<l n="29" num="10.2" lm="12"><w n="29.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="29.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="29.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="29.4">pr<seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>rs</w> <w n="29.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="29.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="29.7">s</w>’<w n="29.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>nt</w> ;</l>
					<l n="30" num="10.3" lm="6"><space unit="char" quantity="12"></space><w n="30.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="30.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="30.4" punct="pe:6">br<seg phoneme="o" type="vs" value="1" rule="437" place="6" punct="pe">o</seg>cs</w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Cotte, cotte, cote, cote, cote, côte, côte, côte, côte, côte.</note>
					</closer>
			</div></body></text></TEI>