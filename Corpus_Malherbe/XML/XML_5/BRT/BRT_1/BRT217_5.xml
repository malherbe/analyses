<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT217" modus="cm" lm_max="12">
				<head type="number">XLII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4" punct="pe:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pe">oi</seg>s</w> ! <w n="1.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="1.6">G<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="2.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.8" punct="pv:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>sc<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pv">eau</seg>x</w> ;</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="3.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w></add></subst> <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="3.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.9" punct="pe:12">Th<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> ! »</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">G<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>str<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.8" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</w>.</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="5.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="5.4">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="4">ym</seg></w></add></subst> <w n="5.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="5.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="5.8">p<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="6" num="1.6" lm="12"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">s<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="6.9" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">oû</seg>ts</w>.</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">G<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="7.4">pr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs</w> <w n="7.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="7.10" punct="pv:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
					<l n="8" num="1.8" lm="12"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt</w> <w n="8.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="8.8">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>t</w> <w n="8.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="8.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="8.11" punct="pt:12">g<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">oû</seg>ts</w>.</l>
					<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="9.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t</w></add></subst> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.5" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="9.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="9.8">ju<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>f</w> <w n="9.9">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="9.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="9.11" punct="vg:12">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="10.4" punct="pv:6">ch<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t</w></l>
					<l n="11" num="1.11" lm="12"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w>-<w n="11.7" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="1.12" lm="12"><w n="12.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>gn<seg phoneme="ø" type="vs" value="1" rule="403" place="2">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>.....</del><add rend="hidden"><w n="12.4">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>t</w></add></subst> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="12.8">l</w>’<w n="12.9" punct="dp:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="dp">e</seg>t</w> :</l>
					<l n="13" num="1.13" lm="12"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="13.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="13.9">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="14" num="1.14" lm="12"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3" punct="dp:6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="dp">ai</seg></w> : <w n="14.4">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="14.8" punct="pt:12">T<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></w></add></subst>.</l>
					<l n="15" num="1.15" lm="12"><w n="15.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.3">n<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="15.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="15.7">d</w>’<w n="15.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="16" num="1.16" lm="12"><w n="16.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="16.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.4" punct="vg:4">m<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>rs</w>, <w n="16.5">n</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="16.8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="16.9">gl<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="16.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="16.11" punct="pt:12">t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></w></add></subst>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Tin, thym, tint, teint, thain, Tain.</note>
					</closer>
			</div></body></text></TEI>