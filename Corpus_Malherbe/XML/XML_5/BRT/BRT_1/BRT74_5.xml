<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT74" modus="sm" lm_max="8">
				<head type="number">LXXIV</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">J<seg phoneme="a" type="vs" value="1" rule="310" place="1">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.2">tr<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.5">cu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.4" punct="pv:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>t</w> ;</l>
					<l n="4" num="1.4" lm="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="4.3" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="192" place="4">oe</seg>ll<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="4.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6" punct="pv:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>g</w> ;</l>
					<l n="5" num="1.5" lm="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>z</w>, <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="6.7">l</w>’<w n="6.8" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="7" num="1.7" lm="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="7.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rt</w>-<w n="7.7" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
					<l n="8" num="1.8" lm="8"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="307" place="4" punct="vg">a</seg>il</w>, <w n="8.4" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>l</w>, <w n="8.5" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="227" place="7">o</seg>ign<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></w> ;</l>
					<l n="9" num="1.9" lm="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2" punct="vg:4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="8"><w n="10.1">Ch<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4" punct="pt:8">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="11" num="1.11" lm="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.6" punct="vg:8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
					<l n="12" num="1.12" lm="8"><w n="12.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg></w> <w n="12.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="12.5" punct="vg:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
					<l n="13" num="1.13" lm="8"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="13.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rn<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="14" num="1.14" lm="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="14.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="14.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="15" num="1.15" lm="8"><w n="15.1" punct="vg:1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" punct="vg">e</seg></w>, <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="15.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>t</w>,</l>
					<l n="16" num="1.16" lm="8"><w n="16.1" punct="ps:3">J<seg phoneme="a" type="vs" value="1" rule="310" place="1">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="ps">on</seg></w>… <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.3">br<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="16.5" punct="pe:8">r<seg phoneme="o" type="vs" value="1" rule="415" place="8" punct="pe">ô</seg>t</w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Chat échaudé craint l’eau froide.</note>
					</closer>
			</div></body></text></TEI>