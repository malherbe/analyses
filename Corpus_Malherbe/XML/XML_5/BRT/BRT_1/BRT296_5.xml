<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT296" modus="cm" lm_max="12">
				<head type="number">XXI</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.5" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="6" punct="pt">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>. <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">I</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="1.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.9" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.4">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="2.9" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="3.5">c</w>’<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="3.8" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="4.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="4.9" punct="pt:12">br<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.3" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.6" punct="vg:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="5.7">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="5.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.10" punct="ps:12">cl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="ps">é</seg></w>…</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="6.2" punct="ps:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="ps">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>d</w>, <w n="6.5">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="6.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="6.8" punct="ps:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">C<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="7.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.9" punct="pv:12">gr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="8" num="2.4" lm="12"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">cl<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="8.4">m<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.6">fru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>ts</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.8" punct="pt:12">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2" punct="vg:3">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.4" punct="vg:6">st<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>ts</w>, <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6" punct="vg:9">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg>s</w>, <w n="9.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.8" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="10.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="10.9" punct="pt:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="11.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="11.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>ts</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.10" punct="pe:12">f<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="12.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>sc<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.3" lm="12"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="14.5" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="14.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="14.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="14.10" punct="pe:12">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe ps">eu</seg></w> ! …</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Sainte Thérèse.</note>
					</closer>
			</div></body></text></TEI>