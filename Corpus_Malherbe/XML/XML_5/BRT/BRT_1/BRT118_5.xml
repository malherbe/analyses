<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT118" modus="cm" lm_max="12">
				<head type="number">XVIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>z<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="1.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="1.7" punct="vg:12">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="2.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>gt</w>-<w n="2.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>q</w> <w n="2.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>ts</w>, <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.7" punct="dp:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg></w> :</l>
					<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="3.2" punct="vg:5">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="3.3">th<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="3.6" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12">— <w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3" punct="vg:4">t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="4.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.7" punct="vg:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="5.7">ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="5.8" punct="pt:12">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					<l n="6" num="1.6" lm="12"><w n="6.1">C</w>’<w n="6.2" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>t</w>, <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="6.4" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rb<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="7" num="1.7" lm="12">— <w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="7.7" punct="pe:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe ps">er</seg></w> ! …</l>
					<l n="8" num="1.8" lm="12"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.4" punct="vg:6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>x</w> <w n="8.7">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="8.8" punct="vg:12">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="9" num="1.9" lm="12"><w n="9.1">N</w>’<w n="9.2" punct="vg:1"><seg phoneme="y" type="vs" value="1" rule="391" place="1" punct="vg">eu</seg>t</w>, <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="9.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="9.6" punct="vg:6">tr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rs</w>, <w n="9.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="9.8" punct="vg:9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>rds</w>, <w n="9.9">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="9.10" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>t</w> <w n="10.8" punct="pt:12">gr<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
					<l n="11" num="1.11" lm="12"><w n="11.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.5" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg">ain</seg>s</w>, <w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.8">gu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="12.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.9" punct="vg:12">f<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
					<l n="13" num="1.13" lm="12"><w n="13.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="13.3" punct="vg:6">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>x</w>, <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="13.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="13.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="14" num="1.14" lm="12"><w n="14.1">P<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.4">F<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.6">s</w>’<w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.9" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="15" num="1.15" lm="12">— <w n="15.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">m<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="15.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="15.9">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg></w> <w n="15.10" punct="pt:12">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Cha-pelle.</note>
					</closer>
			</div></body></text></TEI>