<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT38" modus="cp" lm_max="12">
				<head type="number">XXXVIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4" punct="pe:6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" punct="pe">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w>-<w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="1.8" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
					<l n="2" num="1.2" lm="6"><space unit="char" quantity="12"></space><w n="2.1">M<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.3" punct="pe:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>sp<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe">é</seg></w> !</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="3.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="3.8" punct="vg:12">p<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
					<l n="4" num="1.4" lm="6"><space unit="char" quantity="12"></space><w n="4.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.3">l</w>’<w n="4.4" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>x</w>, <w n="4.5" punct="pe:6">h<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe">é</seg></w> !</l>
					<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l</w> <w n="5.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>dvi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>t</w>, <w n="5.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="5.8" punct="pv:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="6" num="1.6" lm="12"><w n="6.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="6.2" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="6.3" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" punct="vg">un</seg></w>, <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="6.6" punct="vg:9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9" punct="vg">oin</seg>s</w>, <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.8" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="7.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="7.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="7.6" punct="pt:6">g<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="7.7" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">E</seg>t</w>, <w n="7.8" punct="vg:10">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="vg">en</seg>t</w>, <w n="7.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="12">oi</seg></w></l>
					<l n="8" num="1.8" lm="12"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3" punct="pi:6">g<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi ps">a</seg>l</w> ? … <w n="8.4">C</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.7" punct="vg:10">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="8.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="8.9" punct="dp:12">f<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="dp">oi</seg></w> :</l>
					<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="9.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="9.5" punct="vg:6">fl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="9.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="9.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.10">qu</w>’<w n="9.11"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l</w> <w n="9.12" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="10.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="10.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="10.7" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Honni soit qui mal y pense.</note>
					</closer>
			</div></body></text></TEI>