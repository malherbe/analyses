<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT85" modus="cp" lm_max="12">
				<head type="number">LXXXV</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="1.2">f<seg phoneme="u" type="vs" value="1" rule="428" place="2">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ct<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="2.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="2.8" punct="pi:12">t<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>s</w> ?</l>
					<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="3.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>lgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ts</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>s</w> <w n="4.7" punct="pi:12">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					<l n="5" num="1.5" lm="5"><space unit="char" quantity="14"></space><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="5.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="5.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1" lm="12"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">fl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="6">ue</seg>il</w> <w n="6.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="6.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="6.8" punct="vg:12">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="11">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="7" num="2.2" lm="12"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="7.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="7.10" punct="pi:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>s</w> ?</l>
					<l n="8" num="2.3" lm="12"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="8.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="8.8">m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="9" num="2.4" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="9.6">d<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ils</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="9.10">d</w>’<w n="9.11" punct="pi:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg>s</w> ?</l>
					<l n="10" num="2.5" lm="5"><space unit="char" quantity="14"></space><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="10.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1" lm="12"><w n="11.1">Qu</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.8">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.11" punct="pi:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					<l n="12" num="3.2" lm="12"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3" punct="vg:4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="12.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.8">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="12.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="12.10" punct="pi:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>s</w> ?</l>
					<l n="13" num="3.3" lm="12"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="13.5">l<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="13.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9" punct="pi:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
					<l n="14" num="3.4" lm="12"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="14.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="14.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="14.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.9">b<seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>il</w> <w n="14.10" punct="pi:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps">e</seg></w> ? …</l>
					<l n="15" num="3.5" lm="5"><space unit="char" quantity="14"></space><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="15.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="15.4" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>s</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Les amis de nos amis sont nos amis.</note>
					</closer>
			</div></body></text></TEI>