<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FANTAISIES TRAGIQUES</head><div type="poem" key="CRO143" modus="sm" lm_max="8">
					<head type="main">Ballade des Souris</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="1.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">c<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1" punct="vg:2">Gr<seg phoneme="we" type="vs" value="1" rule="CRO143_1" place="1">oë</seg>nl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>d</w>, <w n="2.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.3" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">I</seg>sl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="3.4">m</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="3.7" punct="vg:8">fi<seg phoneme="ɛ" type="vs" value="1" rule="CRO143_1" place="8" punct="vg">e</seg>r</w>,</l>
						<l n="4" num="1.4" lm="8"><w n="4.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.8" punct="pi:8">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pi">ai</seg>r</w> ?</l>
						<l n="5" num="1.5" lm="8"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="1.6" lm="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="6.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt</w></l>
						<l n="7" num="1.7" lm="8"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.7">d</w>’<w n="7.8" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>r</w>.</l>
						<l n="8" num="1.8" lm="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6" punct="pt:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="9.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="9.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w></l>
						<l n="10" num="2.2" lm="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd</w> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.6">b<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.7">qu</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.9" punct="vg:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="2.3" lm="8"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="11.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7" punct="vg:8">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>r</w>,</l>
						<l n="12" num="2.4" lm="8"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="12.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="12.6">d</w>’<w n="12.7" punct="dp:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="dp">e</seg>r</w> :</l>
						<l n="13" num="2.5" lm="8">« <w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w>-<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="14" num="2.6" lm="8"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="14.7">l</w>’<w n="14.8" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>r</w>. »</l>
						<l n="15" num="2.7" lm="8"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rs</w>, <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>r</w>,</l>
						<l n="16" num="2.8" lm="8"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.6" punct="pt:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="17.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w></l>
						<l n="18" num="3.2" lm="8"><w n="18.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="18.4" punct="vg:8">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="19" num="3.3" lm="8"><w n="19.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="19.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="19.6" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>r</w>,</l>
						<l n="20" num="3.4" lm="8"><w n="20.1">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="20.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w></l>
					</lg>
					<lg n="4">
						<l n="21" num="4.1" lm="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4" punct="vg:5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>, <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="21.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="22" num="4.2" lm="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="22.3">c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="22.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="22.6" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>r</w>,</l>
						<l n="23" num="4.3" lm="8"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="23.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="23.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="23.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>r</w>,</l>
						<l n="24" num="4.4" lm="8"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.6" punct="pt:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1" lm="8"><w n="25.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="25.4" punct="vg:6">B<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="25.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">Au</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
						<l n="26" num="5.2" lm="8"><w n="26.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2" punct="vg:3">M<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="26.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="26.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="26.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="27" num="5.3" lm="8"><w n="27.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="27.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="27.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="27.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="27.5" punct="pt:8">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>r</w>.</l>
						<l n="28" num="5.4" lm="8"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="28.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="28.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="28.6" punct="pt:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>