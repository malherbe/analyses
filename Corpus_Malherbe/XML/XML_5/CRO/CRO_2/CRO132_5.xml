<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO132" modus="sm" lm_max="8">
					<head type="main">Les Langues</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">r<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.4" punct="vg:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>d</w>, <w n="1.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6" punct="vg:8">cr<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="2.3">chu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.5" punct="pv:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="3" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="3.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.4" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rp<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.8" punct="vg:8">mi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>,</l>
						<l n="6" num="2.2" lm="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">m<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ts</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="7" num="2.3" lm="8"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="7.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6" punct="pv:8">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="8" num="2.4" lm="8"><w n="8.1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2" punct="vg:2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>rt</w>, <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>ps</w> <w n="8.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ct<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3">d</w>’<w n="9.4" punct="pi:6"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pi">en</seg>t</w> ? <w n="9.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w></l>
						<l n="10" num="3.2" lm="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="10.2">gl<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.4" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="pt">ê</seg>t</w>.</l>
						<l n="11" num="3.3" lm="8"><w n="11.1" punct="vg:2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="11.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="11.4">m</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="11.6" punct="vg:8">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w></l>
						<l n="13" num="4.2" lm="8"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="4.3" lm="8"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.4" punct="vg:4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="14.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.7" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</w>.</l>
					</lg>
				</div></body></text></TEI>