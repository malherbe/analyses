<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DOULEURS ET COLÈRES</head><head type="sub_part">Vers trouvés sur la berge</head><div type="poem" key="CRO144" modus="sm" lm_max="8">
					<head type="main">Banalité</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="2.4" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>cr<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>gs</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.9" punct="pt:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="pt:3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="pt">en</seg>t</w>. <w n="5.4">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="5.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w></l>
						<l n="6" num="2.2" lm="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l</w>’<w n="6.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="2">I</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>l</w>, <w n="6.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="6.7" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3" punct="pt:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pt">on</seg>s</w>. <w n="7.4">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w></l>
						<l n="8" num="2.4" lm="8"><w n="8.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.8" punct="pt:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.4">l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rds</w> <w n="9.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>x</w></l>
						<l n="10" num="3.2" lm="8"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">br<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="3.3" lm="8"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.5">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.7" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="8"><w n="12.1">L</w>’<w n="12.2" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">o</seg>r</w>, <w n="12.3">l</w>’<w n="12.4" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="12.5">r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="13" num="4.2" lm="8"><w n="13.1" punct="vg:2">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="13.3" punct="vg:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>ds</w>, <w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt</w></l>
						<l n="14" num="4.3" lm="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.6" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>