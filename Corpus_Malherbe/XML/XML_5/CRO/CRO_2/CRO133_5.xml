<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO133" modus="sm" lm_max="8">
					<head type="main">Ballade de la Ruine</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="8" punct="vg">y</seg>s</w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="2.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="7">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg>s</w> <w n="4.5" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>s</w>.</l>
						<l n="5" num="1.5" lm="8"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="6" num="1.6" lm="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="6.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
						<l n="7" num="1.7" lm="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.6" punct="pt:8">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="8" num="1.8" lm="8"><w n="8.1">L</w>’<w n="8.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w></l>
						<l n="10" num="2.2" lm="8"><w n="10.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">j<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="11" num="2.3" lm="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="11.5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w></l>
						<l n="12" num="2.4" lm="8"><w n="12.1">Cl<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.3">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w>-<w n="12.4" punct="pv:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>s</w> ;</l>
						<l n="13" num="2.5" lm="8"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="13.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="13.7" punct="pt:8">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="14" num="2.6" lm="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="14.3">l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="14.5">cr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
						<l n="15" num="2.7" lm="8"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="15.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="15.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="15.7" punct="pt:8">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="16" num="2.8" lm="8"><w n="16.1">L</w>’<w n="16.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="16.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="8"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="17.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="17.5" punct="pt:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</w>.</l>
						<l n="18" num="3.2" lm="8"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rs</w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="18.5" punct="vg:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="19" num="3.3" lm="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg>ts</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>ts</w></l>
						<l n="20" num="3.4" lm="8"><w n="20.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="1">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.6" punct="pt:8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>ts</w>.</l>
						<l n="21" num="3.5" lm="8"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="21.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
						<l n="22" num="3.6" lm="8"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ls</w> <w n="22.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4">plu<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
						<l n="23" num="3.7" lm="8"><w n="23.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="23.2">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="23.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="23.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="24" num="3.8" lm="8"><w n="24.1">L</w>’<w n="24.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">ENVOI</head>
						<l n="25" num="4.1" lm="8"><w n="25.1" punct="vg:1">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" punct="vg">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="25.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="25.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w>-<w n="25.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="25.6" punct="pv:8">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="26" num="4.2" lm="8"><w n="26.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="26.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="26.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="26.4">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rs</w> <w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
						<l n="27" num="4.3" lm="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="27.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="27.4">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="27.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="27.7" punct="pt:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="28" num="4.4" lm="8"><w n="28.1">L</w>’<w n="28.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="28.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>