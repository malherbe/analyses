<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DOULEURS ET COLÈRES</head><head type="sub_part">Vers trouvés sur la berge</head><div type="poem" key="CRO155" modus="sm" lm_max="8">
					<head type="main">Insoumission</head>
					<opener>
						<salute>A Lionel Nunès</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.5" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="2.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="2.4" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">V<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="3.5" punct="pt:8">p<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="5" num="2.2" lm="8"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="5.3">n</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="5.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>fr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.3" lm="8"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="8"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.9" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rt</w>,</l>
						<l n="8" num="3.2" lm="8"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="8.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6" punct="vg:8">l<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rd</w>,</l>
						<l n="9" num="3.3" lm="8"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.9" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rt</w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="8"><w n="10.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="10.2" punct="vg:2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="10.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>squ</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.8" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="11" num="4.2" lm="8"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="11.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="11.7" punct="vg:8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="12" num="4.3" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="8"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>t</w> <w n="13.3" punct="vg:4">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>t</w> <w n="13.6" punct="pt:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
						<l n="14" num="5.2" lm="8"><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</w>,</l>
						<l n="15" num="5.3" lm="8"><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="15.2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rt</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="15.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="15.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.6" punct="pt:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>