<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><div type="poem" key="CRO66" modus="sp" lm_max="4">
					<head type="main">Bonne fortune</head>
					<opener>
						<salute>A Théodore de Banville</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="4"><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="3"><space quantity="4" unit="char"></space><w n="2.1"><seg phoneme="œ" type="vs" value="1" rule="286" place="1">Œ</seg>il</w>-<w n="2.2" punct="vg:3">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>,</l>
						<l n="3" num="1.3" lm="4"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="4" num="1.4" lm="3"><space quantity="4" unit="char"></space><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="4.3" punct="pi:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="pi">u</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="4"><w n="5.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3" punct="vg:4">tr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="3"><space quantity="4" unit="char"></space><w n="6.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="6.2" punct="vg:3">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>,</l>
						<l n="7" num="2.3" lm="4"><w n="7.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">br<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="8" num="2.4" lm="3"><space quantity="4" unit="char"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="vg:3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="4"><w n="9.1">Fr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">c<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</w></l>
						<l n="10" num="3.2" lm="3"><space quantity="4" unit="char"></space><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3" punct="vg:3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>cs</w>,</l>
						<l n="11" num="3.3" lm="4"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</w></l>
						<l n="12" num="3.4" lm="3"><space quantity="4" unit="char"></space><w n="12.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="12.2" punct="pt:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pt">an</seg>ts</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="4"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="14" num="4.2" lm="3"><space quantity="4" unit="char"></space><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3" punct="pt:3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pt">o</seg>rps</w>.</l>
						<l n="15" num="4.3" lm="4"><w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="16" num="4.4" lm="3"><space quantity="4" unit="char"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="16.2" punct="pt:3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pt">o</seg>rs</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="4"><w n="17.1">Qu</w>’<w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.4" punct="vg:4">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="4">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="3"><space quantity="4" unit="char"></space><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w></l>
						<l n="19" num="5.3" lm="4"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="19.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="20" num="5.4" lm="3"><space quantity="4" unit="char"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3" punct="pt:3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt">a</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="4"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4" punct="vg:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="22" num="6.2" lm="3"><space quantity="4" unit="char"></space><w n="22.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="22.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="22.3" punct="vg:3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="3" punct="vg">e</seg>r</w>,</l>
						<l n="23" num="6.3" lm="4"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="23.4" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="24" num="6.4" lm="3"><space quantity="4" unit="char"></space><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">Â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="24.3" punct="pt:3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pt">ai</seg>r</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="4"><w n="25.1">C</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="25.3" punct="po:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> (<w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="25.5">l</w>’<w n="25.6" punct="pe:4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !)</l>
						<l n="26" num="7.2" lm="3"><space quantity="4" unit="char"></space><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="26.2">t</w>’<w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>s</w></l>
						<l n="27" num="7.3" lm="4"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="27.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.4">v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="28" num="7.4" lm="3"><space quantity="4" unit="char"></space><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="28.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3" punct="pt:3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3" punct="pt">ein</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="4"><w n="29.1" punct="vg:2">C<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>s</w>, <w n="29.2" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="30" num="8.2" lm="3"><space quantity="4" unit="char"></space><w n="30.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="30.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w></l>
						<l n="31" num="8.3" lm="4"><w n="31.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="31.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="31.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="31.4">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="32" num="8.4" lm="3"><space quantity="4" unit="char"></space><w n="32.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="32.2" punct="pt:3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pt">eu</seg>r</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1" lm="4"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="33.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="34" num="9.2" lm="3"><space quantity="4" unit="char"></space><w n="34.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="34.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>,</l>
						<l n="35" num="9.3" lm="4"><w n="35.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="35.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">t</w>’<w n="35.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="36" num="9.4" lm="3"><space quantity="4" unit="char"></space><w n="36.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="36.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="36.3" punct="pt:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pt">i</seg>t</w>.</l>
					</lg>
				</div></body></text></TEI>