<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO100" modus="cm" lm_max="12">
					<head type="main">Vue sur la cour</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">cu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="1.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.8">p<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>t</w>-<w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w>-<w n="1.10">f<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg></w> <w n="1.11">b<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t</w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="pt:4">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pt">eau</seg></w>. <w n="2.4">L<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.5" punct="vg:6">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" punct="vg">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="2.8" punct="vg:12">tr<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.6" punct="vg:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>gts</w> <w n="4.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.5" punct="vg:6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="4.8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="4.10" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>t</w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="5.9" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="6.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="6.6">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="6.7" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4" punct="pt:6">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6" punct="pt">y</seg>s</w>. <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="7.7">gr<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="7.8" punct="vg:12">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="8.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="vg:9">b<seg phoneme="u" type="vs" value="1" rule="428" place="8">ou</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="vg">on</seg></w>, <w n="8.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
						<l n="9" num="1.9" lm="12"><w n="9.1">S</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="9.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="9.7" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>t</w>, <w n="9.8">ch<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>d</w> <w n="9.9" punct="ps:12">cr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
						<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="10.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="10.8" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>