<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO3" modus="sm" lm_max="8">
					<head type="main">Nocturne</head>
					<opener>
						<salute>A Arsène Houssaye</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">B<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="1.2" punct="vg:4">fr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>ts</w>, <w n="1.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="1.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w>-<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="3.4" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="8"><w n="4.1" punct="vg:1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1" punct="vg">en</seg>ts</w>, <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="4.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5" punct="vg:8">r<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>,</l>
						<l n="5" num="2.2" lm="8"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="5.3" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>ts</w>, <w n="5.4">r<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ls</w> <w n="5.5" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>,</l>
						<l n="6" num="2.3" lm="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="307" place="1">A</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="6.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="pe:8">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>rs</w> !</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="7.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t</w> <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w></l>
						<l n="8" num="3.2" lm="8"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></w>.</l>
						<l n="9" num="3.3" lm="8"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">n</w>’<w n="9.5"><seg phoneme="y" type="vs" value="1" rule="391" place="5">eu</seg>s</w> <w n="9.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="9.7" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="8"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rds</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>s</w> <w n="10.5">d</w>’<w n="10.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
						<l n="11" num="4.2" lm="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="11.7">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
						<l n="12" num="4.3" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="8"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="13.3"><seg phoneme="y" type="vs" value="1" rule="391" place="2">eu</seg>s</w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="13.6" punct="pv:8">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pv">en</seg>t</w> ;</l>
						<l n="14" num="5.2" lm="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2" punct="vg:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>s</w>, <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="14.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="14.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w></l>
						<l n="15" num="5.3" lm="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="15.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1" lm="8"><w n="16.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="16.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="16.3">qu</w>’<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="16.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.6">f<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>t</w> <w n="16.7" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>,</l>
						<l n="17" num="6.2" lm="8"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="17.4">pr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="17.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="17.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="17.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="17.8">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w></l>
						<l n="18" num="6.3" lm="8"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="18.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="18.7" punct="pt:8">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1" lm="8"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="19.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp in">ai</seg>s</w> : « <w n="19.4">T<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="19.5">m</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w></l>
						<l n="20" num="7.2" lm="8"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="20.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="20.5" punct="pe:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</w> ! »</l>
						<l n="21" num="7.3" lm="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="21.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="21.5">qu</w>’<w n="21.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="21.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.8" punct="pt:8">br<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1" lm="8"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2" punct="vg:2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="22.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="22.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="22.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>t</w>,</l>
						<l n="23" num="8.2" lm="8"><w n="23.1">S</w>’<w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.7" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
						<l n="24" num="8.3" lm="8"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="24.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6">y</seg>s</w> <w n="24.6" punct="pt:8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1" lm="8"><w n="25.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.3">n</w>’<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="25.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="25.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="25.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
						<l n="26" num="9.2" lm="8"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="26.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="26.4">l</w>’<w n="26.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>g</w>, <w n="26.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w></l>
						<l n="27" num="9.3" lm="8"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="27.2" punct="vg:2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>rs</w>, <w n="27.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="27.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="27.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>t</w> <w n="27.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1" lm="8"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="28.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="28.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="28.4">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="28.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
						<l n="29" num="10.2" lm="8"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="29.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="29.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w></l>
						<l n="30" num="10.3" lm="8"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">j</w>’<w n="30.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="30.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="30.5" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1" lm="8"><w n="31.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="31.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="31.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd</w> <w n="31.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="31.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="31.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w></l>
						<l n="32" num="11.2" lm="8"><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="32.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="32.4" punct="vg:5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="5" punct="vg">om</seg></w>, <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="32.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
						<l n="33" num="11.3" lm="8"><w n="33.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="33.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="33.4">l</w>’<w n="33.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="33.6" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</w>.</l>
					</lg>
					<lg n="12">
						<l n="34" num="12.1" lm="8"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="34.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="34.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="34.6" punct="vg:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="35" num="12.2" lm="8"><w n="35.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="35.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="35.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="35.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>ts</w>, <w n="35.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="35.6">gr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
						<l n="36" num="12.3" lm="8"><w n="36.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="36.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="36.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.4">m</w>’<w n="36.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="pt">ai</seg></w>.</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1" lm="8"><w n="37.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="37.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="37.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="37.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w></l>
						<l n="38" num="13.2" lm="8"><w n="38.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="38.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="38.3">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="38.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="38.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="38.6" punct="pv:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg>t</w> ;</l>
						<l n="39" num="13.3" lm="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="39.3">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>cs</w> <w n="39.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rts</w> <w n="39.5">m</w>’<w n="39.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>t</w>.</l>
					</lg>
					<lg n="14">
						<l n="40" num="14.1" lm="8"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="40.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="40.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="40.4" punct="vg:5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>, <w n="40.5">fr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
						<l n="41" num="14.2" lm="8"><w n="41.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="41.2">l</w>’<w n="41.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="41.4" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="42" num="14.3" lm="8"><w n="42.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="42.2">l</w>’<w n="42.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="42.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="42.5">l</w>’<w n="42.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</w>.</l>
					</lg>
					<lg n="15">
						<l n="43" num="15.1" lm="8"><w n="43.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="43.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="43.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="43.4" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="43.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
						<l n="44" num="15.2" lm="8"><w n="44.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="44.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="44.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg>s</w> <w n="44.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="44.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="44.6">d</w>’<w n="44.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
						<l n="45" num="15.3" lm="8"><w n="45.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="45.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="45.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="45.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="45.5" punct="pe:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="16">
						<l n="46" num="16.1" lm="8"><w n="46.1">Qu</w>’<w n="46.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="46.3" punct="vg:3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="46.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="46.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
						<l n="47" num="16.2" lm="8"><w n="47.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="47.2">l</w>’<w n="47.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="47.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="47.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="47.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="47.7" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></w>,</l>
						<l n="48" num="16.3" lm="8"><w n="48.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="48.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="48.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="48.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="48.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w>-<w n="48.6" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>