<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><div type="poem" key="CRO64" modus="cm" lm_max="12">
					<head type="main">Six tercets</head>
					<opener>
						<salute>A Degas</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.5" punct="vg:8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>ds</w>, <w n="1.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="1.8" punct="vg:12">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="2.3" punct="dp:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lti<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="dp">er</seg>s</w> : <w n="2.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.7">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>qu<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="3.8" punct="pt:12">sp<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1" lm="12"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="4.2" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></w></l>
						<l n="5" num="2.2" lm="12"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2" punct="vg:4">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="5.5">qu</w>’<w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="5.8">fl<seg phoneme="y" type="vs" value="1" rule="d-3" place="9">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="5.9">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="6" num="2.3" lm="12"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>pt<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="6.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.7" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="12"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="dp:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="dp">e</seg></w> : <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="7.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="7.6" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ls</w>, <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rcs</w> <w n="7.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</w>,</l>
						<l n="8" num="3.2" lm="12"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="8.4">fl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="8.5" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.8">p<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>rp<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="9" num="3.3" lm="12"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="9.2">s</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="9.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="u" type="vs" value="1" rule="428" place="8">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.8" punct="vg:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.4" punct="vg:6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="10.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.7">v<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
						<l n="11" num="4.2" lm="12"><w n="11.1" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="11.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="11.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="12" num="4.3" lm="12"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="12.5">n<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="12.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="12.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.11" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="12"><w n="13.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="13.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="13.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7"><seg phoneme="u" type="vs" value="1" rule="426" place="8">où</seg></w> <w n="13.8">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="13.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.10">p<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="14" num="5.2" lm="12"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>gt</w> <w n="14.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.7" punct="vg:12">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>g<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="15" num="5.3" lm="12"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gm<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.3">f<seg phoneme="o" type="vs" value="1" rule="434" place="8">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="15.5" punct="pt:12">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1" lm="12"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2" punct="vg:2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">en</seg>ds</w>, <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="16.8" punct="vg:9">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="16.10">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="17" num="6.2" lm="12"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">fl<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="17.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="17.6">n<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="17.7">f<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="17.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="18" num="6.3" lm="12"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="18.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">cr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="18.6">r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="18.7">d</w>’<w n="18.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="18.9" punct="vg:11">v<seg phoneme="u" type="vs" value="1" rule="425" place="11" punct="vg">ou</seg>s</w>, <w n="18.10" punct="pt:12">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rth<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>