<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA56" modus="sm" lm_max="7">
					<head type="main">TOUJOURS TOI</head>
					<opener>
						<salute>A Marie Désirée.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="7"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="1.2" punct="pe:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">t</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="2" num="1.2" lm="7"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="2.6" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pv">ou</seg>r</w> ;</l>
						<l n="3" num="1.3" lm="7"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="4" num="1.4" lm="7"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.5" punct="pv:7">j<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pv">ou</seg>r</w> ;</l>
						<l n="5" num="1.5" lm="7"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.6" punct="vg:7">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="7"><w n="6.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.6" punct="dp:7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="dp">eu</seg>r</w> :</l>
						<l n="7" num="1.7" lm="7"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="7.5" punct="vg:7">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="8" num="1.8" lm="7"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5" punct="pe:7">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pe">eu</seg>r</w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="7"><w n="9.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="9.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.7" punct="vg:7">c<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="10" num="2.2" lm="7"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5" punct="vg:7">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</w>,</l>
						<l n="11" num="2.3" lm="7"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg></w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.6" punct="vg:7">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="12" num="2.4" lm="7"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="12.6" punct="pt:7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</w>.</l>
						<l n="13" num="2.5" lm="7"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="13.6">s</w>’<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="14" num="2.6" lm="7"><w n="14.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6" punct="pv:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pv">é</seg></w> ;</l>
						<l n="15" num="2.7" lm="7"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="15.3">qu</w>’<w n="15.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="15.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="16" num="2.8" lm="7"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="16.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="16.5" punct="pt:7">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="7"><w n="17.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="18" num="3.2" lm="7"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5" punct="vg:7">T<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>,</l>
						<l n="19" num="3.3" lm="7"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="19.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="20" num="3.4" lm="7"><w n="20.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="20.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg>s</w> <w n="20.4" punct="vg:7">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>,</l>
						<l n="21" num="3.5" lm="7"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="21.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="22" num="3.6" lm="7"><w n="22.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="22.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="22.6" punct="vg:7">h<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>,</l>
						<l n="23" num="3.7" lm="7"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="23.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="24" num="3.8" lm="7"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="24.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.4" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>