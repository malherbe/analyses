<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA76" modus="sm" lm_max="8">
					<head type="main">LE BOSQUET DE ROSES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2" punct="vg">e</seg>st</w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="1.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.7" punct="vg:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="2.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="3.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.7" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w>-<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="5.6" punct="pi:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</w> ?</l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1" lm="8"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c</w>-<w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.5">j</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="6.7">m</w> ’<w n="6.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="8" punct="vg">eoi</seg>r</w>,</l>
						<l n="7" num="3.2" lm="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.3" punct="pe:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="8" num="3.3" lm="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="9" num="3.4" lm="8"><w n="9.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="9.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="9.3">j</w>’<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="9.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="pt:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="8"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="10.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="11" num="5.1" lm="8"><w n="11.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="11.2">j</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w></l>
						<l n="12" num="5.2" lm="8"><w n="12.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="12.3" punct="vg:3"><seg phoneme="u" type="vs" value="1" rule="426" place="3" punct="vg">où</seg></w>, <w n="12.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="13" num="5.3" lm="8"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="5.4" lm="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp in">ai</seg>s</w> : « <w n="14.4">R<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="14.5" punct="pe:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></w> ! »</l>
					</lg>
					<lg n="6">
						<l n="15" num="6.1" lm="8"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w>-<w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="15.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="15.5" punct="pi:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="7">
						<l n="16" num="7.1" lm="8"><w n="16.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="16.2">l</w>’<w n="16.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="16.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="16.6" punct="pv:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>r</w> ;</l>
						<l n="17" num="7.2" lm="8"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2">s</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="17.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.7">s</w>’<w n="17.8" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="18" num="7.3" lm="8"><w n="18.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="18.8">c<seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="19" num="7.4" lm="8"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="19.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</w>.</l>
					</lg>
					<lg n="8">
						<l n="20" num="8.1" lm="8"><w n="20.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="20.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">j</w>’<w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="20.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="20.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.7" punct="pe:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1855">Avril 1855.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>