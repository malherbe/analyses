<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA63" modus="cm" lm_max="10">
					<head type="main">LES ÂMES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="1.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="1.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="1.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.6">cr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="10"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="2.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r</w> <w n="2.8" punct="vg:10">d<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</w>,</l>
						<l n="3" num="1.3" lm="10"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">s</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="3.6" punct="vg:10">fl<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="4" num="1.4" lm="10"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">s</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="4.7">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l</w> <w n="4.8" punct="pt:10">h<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.8" punct="vg:10">g<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="10"><w n="6.1">F<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="6.4">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="6.6" punct="pt:10">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
						<l n="7" num="2.3" lm="10"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="7.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="8" num="2.4" lm="10"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t</w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="8.8" punct="pt:10">s<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="pt">œu</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2" punct="pe:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pe">oi</seg>r</w> ! <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7">en</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.7" punct="pe:10">v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
						<l n="10" num="3.2" lm="10"><w n="10.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.3" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" punct="vg">ain</seg></w>, <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.5">l</w>’<w n="10.6" punct="vg:7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg">e</seg></w>, <w n="10.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="10.9" punct="vg:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>r</w>,</l>
						<l n="11" num="3.3" lm="10"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="11.3" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.7" punct="vg:10">r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.4" lm="10"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="12.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">î</seg>t</w> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="12.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="13.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="13.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="13.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="14" num="4.2" lm="10"><w n="14.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="14.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="14.5" punct="vg:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></w>,</l>
						<l n="15" num="4.3" lm="10"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr</w>’<w n="15.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="7">en</seg></w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="15.8" punct="vg:10">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
						<l n="16" num="4.4" lm="10"><w n="16.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="16.4">n</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="16.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="16.8">d</w>’<w n="16.9" punct="pt:10">h<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pt">ain</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10"><w n="17.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="17.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>rs</w> <w n="17.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="17.7">s</w>’<w n="17.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>nt</w>,</l>
						<l n="18" num="5.2" lm="10"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="18.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="18.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="18.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="18.7">qu</w>’<w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="18.9" punct="pe:10">f<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg></w> !</l>
						<l n="19" num="5.3" lm="10"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ls</w> <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.6">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="19.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="19.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg>nt</w> ;</l>
						<l n="20" num="5.4" lm="10"><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="20.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="20.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="20.9" punct="pe:10">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg></w> !</l>
					</lg>
				</div></body></text></TEI>