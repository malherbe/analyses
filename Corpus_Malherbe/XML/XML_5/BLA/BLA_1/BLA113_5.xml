<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA113" modus="sm" lm_max="6">
					<head type="main">AMITIÉ MORTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="6"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3" punct="pe:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pe">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.6" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="6"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="2.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe">é</seg></w> !</l>
						<l n="3" num="1.3" lm="6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ss<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.3">l</w>’<w n="3.4" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
						<l n="4" num="1.4" lm="6"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.5" punct="pe:6">p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:3">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="5.4" punct="vg:6">f<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="6"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="6.6" punct="pe:6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe">eu</seg>l</w> !</l>
						<l n="7" num="2.3" lm="6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="8" num="2.4" lm="6"><w n="8.1">Fr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d</w> <w n="8.5" punct="pv:6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pv">eu</seg>l</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="10" num="3.2" lm="6"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="10.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="10.6" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>,</l>
						<l n="11" num="3.3" lm="6"><w n="11.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="12" num="3.4" lm="6"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <hi rend="ital"><w n="12.3" punct="pe:6">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="167" place="5">un</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe">i</seg>s</w></hi> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="6"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4" punct="vg:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" punct="vg">en</seg></w>, <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.6" punct="vg:6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.6" punct="pv:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="pv">ai</seg>r</w> ;</l>
						<l n="15" num="4.3" lm="6"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="15.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="15.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="16" num="4.4" lm="6"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="16.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="16.6" punct="pt:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pt">e</seg>r</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="6"><w n="17.1">D</w>’<w n="17.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="17.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="6"><w n="18.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="18.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="18.3">t</w>’<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="3">e</seg>s</w> <w n="18.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="18.7" punct="vg:6">j<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg></w>,</l>
						<l n="19" num="5.3" lm="6"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2" punct="ps:2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="ps">i</seg>s</w>… <w n="19.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5" punct="dp:6">l<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></w> :</l>
						<l n="20" num="5.4" lm="6"><w n="20.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.2">t</w>’<w n="20.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4" punct="pt:6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="6"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.4">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="22" num="6.2" lm="6"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.4" punct="pv:6">ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>r</w> ;</l>
						<l n="23" num="6.3" lm="6"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="23.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="23.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="24" num="6.4" lm="6"><w n="24.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>t</w> <w n="24.4">qu</w>’<w n="24.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="24.6" punct="pt:6">m<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg>r</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="6"><w n="25.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">fr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.5" punct="pv:6">n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
						<l n="26" num="7.2" lm="6"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="26.4">d<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="26.5" punct="pt:6">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
						<l n="27" num="7.3" lm="6"><w n="27.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="27.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="28" num="7.4" lm="6"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="28.3">t</w>’<w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="28.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.6" punct="pt:6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="6"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="29.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="30" num="8.2" lm="6"><w n="30.1">N</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="30.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="30.4">qu</w>’<w n="30.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="30.6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d</w> <w n="30.7" punct="pv:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pv">eau</seg></w> ;</l>
						<l n="31" num="8.3" lm="6"><w n="31.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="31.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="31.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="31.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="31.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>d</w> <w n="31.6">qu</w>’<w n="31.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="32" num="8.4" lm="6"><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="32.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="32.6" punct="pt:6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1" lm="6"><w n="33.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg></w> ! <w n="33.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="33.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="33.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="34" num="9.2" lm="6"><w n="34.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="34.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="34.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="34.4" punct="pt:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg>s</w>.</l>
						<l n="35" num="9.3" lm="6"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="35.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="35.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="35.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="35.6" punct="dp:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></w> :</l>
						<l n="36" num="9.4" lm="6"><w n="36.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="36.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rts</w> <w n="36.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="36.5" punct="pe:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe">u</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>