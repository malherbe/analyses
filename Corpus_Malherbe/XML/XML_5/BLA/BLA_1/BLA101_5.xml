<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA101" modus="cm" lm_max="10">
					<head type="main">POURQUOI PARTIR</head>
					<opener>
						<salute>A Madame Amélie Rivière.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w>-<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="2" num="1.2" lm="10"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="2.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="2.7" punct="pi:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pi">an</seg>ts</w> ?</l>
						<l n="3" num="1.3" lm="10"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w>-<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s</w> <w n="3.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="8">o</seg>p</w> <w n="3.8">cr<seg phoneme="y" type="vs" value="1" rule="454" place="9">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="4" num="1.4" lm="10"><w n="4.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="4.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5">l</w>’<w n="4.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="4.8" punct="pi:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pi">am</seg>ps</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="5.7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="6" num="2.2" lm="10"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="6.2">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="6.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7">en</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="6.5" punct="pv:10">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>s</w> ;</l>
						<l n="7" num="2.3" lm="10"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3" punct="pe:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pe">ez</seg></w> ! <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="7.5">n</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="7.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="8" num="2.4" lm="10"><w n="8.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="8.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="8.4">v<seg phoneme="ø" type="vs" value="1" rule="248" place="4">œu</seg>x</w> <w n="8.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="8.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="8.8">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>s</w> <w n="8.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="8.10" punct="pe:10">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="9.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="9.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="10" num="3.2" lm="10"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.3" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>il</w>, <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="10.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="10.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="10.8" punct="pe:10">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>rs</w> !</l>
						<l n="11" num="3.3" lm="10"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3" punct="vg:4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="487" place="7">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="11.7" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.4" lm="10"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="12.9" punct="pe:10">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">n<seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.6" punct="vg:10">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="10"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="14.7" punct="vg:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="vg">en</seg>ts</w>,</l>
						<l n="15" num="4.3" lm="10"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="15.4">pl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="15.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="15.7" punct="dp:10">h<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
						<l n="16" num="4.4" lm="10"><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="16.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="16.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="16.8" punct="pt:10">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="pt">en</seg>ts</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="17.3" punct="pt:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="pt">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="17.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">A</seg>h</w> ! <w n="17.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="vg:10">f<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="18" num="5.2" lm="10"><w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.4">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="18.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="18.7" punct="dp:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="dp">ou</seg>rs</w> :</l>
						<l n="19" num="5.3" lm="10"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="19.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="19.7" punct="vg:10">r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="20" num="5.4" lm="10"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2" punct="vg:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>t</w>, <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="20.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="20.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="20.7" punct="pt:10">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>rs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="10"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="21.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="21.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="21.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="22" num="6.2" lm="10"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">Cr<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="22.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="22.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="22.8" punct="vg:10">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r</w>,</l>
						<l n="23" num="6.3" lm="10"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="23.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="23.6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="23.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
						<l n="24" num="6.4" lm="10"><w n="24.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="24.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="24.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="24.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="24.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="24.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="9">œ</seg>il</w> <w n="24.8" punct="pt:10">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>r</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="10"><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="25.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="25.7" punct="pv:10">tr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="26" num="7.2" lm="10"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="26.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="26.5">l</w>’<w n="26.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="26.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="26.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="26.9" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</w> ;</l>
						<l n="27" num="7.3" lm="10"><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="27.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="27.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>rs</w>, <w n="27.6">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="7">oi</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7" punct="dp:10"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp">e</seg></w> :</l>
						<l n="28" num="7.4" lm="10"><w n="28.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="28.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="28.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="28.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.7">l</w>’<w n="28.8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="8">î</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="28.9" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="10"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">gl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="29.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="29.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="29.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="29.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="29.7" punct="vg:10">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="30" num="8.2" lm="10"><w n="30.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="30.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="30.4">gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="30.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="30.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg>s</w> ;</l>
						<l n="31" num="8.3" lm="10"><w n="31.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="31.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="31.3">l</w>’<w n="31.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg></w> <w n="31.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="31.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="31.8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="32" num="8.4" lm="10"><w n="32.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="32.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="32.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="32.6" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg>s</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1" lm="10"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="33.3">ch<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="33.4" punct="vg:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="34" num="9.2" lm="10"><w n="34.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="34.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="34.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="34.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="34.6" punct="vg:10">r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="vg">u</seg>s</w>,</l>
						<l n="35" num="9.3" lm="10"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="35.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rds</w> <w n="35.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="35.5" punct="vg:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg>t</w>, <w n="35.6">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="8">oi</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
						<l n="36" num="9.4" lm="10"><w n="36.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="36.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="36.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="4">y</seg>s</w> <w n="36.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="36.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="36.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="36.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="36.8" punct="pt:10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pt">u</seg>s</w>.</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Longefont</placeName>,
							<date when="1856">18 août 1856.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>