<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN20" modus="cp" lm_max="12">
		<head type="main">ALLEGRO DES COALTARS</head>
			<div type="section" n="1">
			<head type="number">I</head>
			<lg n="1">
				<l n="1" num="1.1" lm="10"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="smallcap"><w n="1.2">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="1.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8">É</seg></w> <w n="1.4">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">E</seg>B<seg phoneme="œ" type="vs" value="1" rule="407" place="10">EU</seg>F</w></hi></l>
				<l n="2" num="1.2" lm="12"><w n="2.1">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l</w> <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="2.7">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="2.8">d</w>’<w n="2.9" punct="dp:12"><seg phoneme="œ" type="vs" value="1" rule="389" place="12" punct="dp">oeu</seg>f</w> :</l>
				<l n="3" num="1.3" lm="8"><w n="3.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="3.2">c</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.4">j<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="3.5">r<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
				<l n="4" num="1.4" lm="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="smallcap"><w n="4.2" punct="pe:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="4.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">É</seg></w></hi> !</l>
				<l n="5" num="1.5" lm="5"><w n="5.1" punct="vg:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.2" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
				<l n="6" num="1.6" lm="4"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="6.2" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></w> !</l>
				<l n="7" num="1.7" lm="8"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>xt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.5" punct="dp:8">n<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="dp">é</seg></w> :</l>
				<l n="8" num="1.8" lm="1"><w n="8.1">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2">e</seg></w></l>
				<l n="9" num="1.9" lm="3"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
				<l n="10" num="1.10" lm="8"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <hi rend="smallcap"><w n="10.2" punct="pe:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="10.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">É</seg></w></hi> !</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="number">II</head>
			<lg n="1">
				<l n="11" num="1.1" lm="10"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="smallcap"><w n="11.2">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="11.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8">É</seg></w> <w n="11.4">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">E</seg>B<seg phoneme="œ" type="vs" value="1" rule="407" place="10">EU</seg>F</w></hi></l>
				<l n="12" num="1.2" lm="12"><w n="12.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="12.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="12.9" punct="dp:12">n<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="dp">eu</seg>f</w> :</l>
				<l n="13" num="1.3" lm="8"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
				<l n="14" num="1.4" lm="8"><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <hi rend="smallcap"><w n="14.2" punct="pe:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="14.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">É</seg></w></hi> !</l>
				<l n="15" num="1.5" lm="5"><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="15.4" punct="vg:5">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
				<l n="16" num="1.6" lm="4"><w n="16.1" punct="vg:2">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2" punct="vg">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="16.3" punct="dp:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg>s</w> :</l>
				<l n="17" num="1.7" lm="8"><w n="17.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="17.4" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="dp">é</seg></w> :</l>
				<l n="18" num="1.8" lm="1"><w n="18.1">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2">e</seg></w></l>
				<l n="19" num="1.9" lm="3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
				<l n="20" num="1.10" lm="8"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="20.2">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="20.3" punct="pe:8">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">É</seg></w> !</l>
			</lg>
			</div>
			<div type="section" n="3">
			<head type="number">III</head>
			<lg n="1">
				<l n="21" num="1.1" lm="10"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="smallcap"><w n="21.2">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="21.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8">É</seg></w> <w n="21.4">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">E</seg>B<seg phoneme="œ" type="vs" value="1" rule="407" place="10">EU</seg>F</w></hi></l>
				<l n="22" num="1.2" lm="12"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="22.2">d</w>’<w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="22.5">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="22.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="22.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="22.8">qu<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rti<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="22.9" punct="dp:12">M<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rb<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="dp">eu</seg>f</w> :</l>
				<l n="23" num="1.3" lm="8"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="23.2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="23.3">n</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w>-<w n="23.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="23.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="23.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
				<l n="24" num="1.4" lm="8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="smallcap"><w n="24.2" punct="pi:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="24.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">É</seg></w></hi> ?</l>
				<l n="25" num="1.5" lm="5"><w n="25.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="25.2" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</w>,</l>
				<l n="26" num="1.6" lm="4"><w n="26.1" punct="vg:2">F<seg phoneme="ɥi" type="vs" value="1" rule="462" place="1">u</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="26.2" punct="dp:4">M<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg>s</w> :</l>
				<l n="27" num="1.7" lm="8"><w n="27.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="27.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.3">gl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="27.5" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
				<l n="28" num="1.8" lm="1"><w n="28.1">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="2">e</seg></w></l>
				<l n="29" num="1.9" lm="3"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
				<l n="30" num="1.10" lm="8"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <hi rend="smallcap"><w n="30.2" punct="pe:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="2">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>R</w> <w n="30.3">S<seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>P<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>N<seg phoneme="i" type="vs" value="1" rule="467" place="7">I</seg>N<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">É</seg></w></hi> !</l>
			</lg>
			</div>
			<div type="section" n="4">
			<head type="sub">INCANTATION</head>
			<lg n="1">
				<l n="31" num="1.1" lm="10"><w n="31.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="31.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="31.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="31.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="31.8" punct="vg:10">S<seg phoneme="a" type="vs" value="1" rule="342" place="10" punct="vg">à</seg>r</w>,</l>
				<l n="32" num="1.2" lm="10"><w n="32.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="32.3" punct="vg:4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="32.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>ts</w> <w n="32.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="32.8" punct="vg:10">S<seg phoneme="a" type="vs" value="1" rule="342" place="10" punct="vg">à</seg>r</w>,</l>
				<l n="33" num="1.3" lm="10"><w n="33.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="33.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="33.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ts</w>, <w n="33.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="33.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="33.6">dr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ps</w> <w n="33.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="33.8" punct="vg:10">S<seg phoneme="a" type="vs" value="1" rule="342" place="10" punct="vg">à</seg>r</w>,</l>
				<l n="34" num="1.4" lm="8"><w n="34.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="34.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="34.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <hi rend="smallcap"><w n="34.5" punct="pt:8">C<seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>LT<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">A</seg>R</w></hi>.</l>
			</lg>
			</div>
	</div></body></text></TEI>