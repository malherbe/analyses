<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN10" modus="cp" lm_max="16">
		<head type="main">LA CHANSON DU PORC-ÉPIC</head>
			<lg n="1">
				<l n="1" num="1.1" lm="8"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="1.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rc</w>-<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c</w></l>
				<l n="2" num="1.2" lm="14"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.8" punct="vg:11">p<seg phoneme="a" type="vs" value="1" rule="307" place="9">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" punct="vg">on</seg></w>, <w n="2.9">r<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="2.10" punct="pt:14">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="13">e</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="14" punct="pt">i</seg>c</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1" lm="13"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w> <w n="3.8" punct="vg:13">c<seg phoneme="u" type="vs" value="1" rule="426" place="13" punct="vg">ou</seg></w>,</l>
				<l n="4" num="2.2" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="4.6">s<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="4.8" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>t</w>.</l>
			</lg>
			<lg n="3">
				<l n="5" num="3.1" lm="12"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="5.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="5.6">m</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="5.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>t</w>,</l>
				<l n="6" num="3.2" lm="14"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="6.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="6.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="6.6" punct="vg:9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="vg">ez</seg></w>, <w n="6.7">d</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="6.10">p<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg></w> <w n="6.11" punct="pt:14">s<seg phoneme="y" type="vs" value="1" rule="450" place="13">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="14" punct="pt">i</seg>s</w>.</l>
			</lg>
			<lg n="4">
				<l n="7" num="4.1" lm="15"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg">ai</seg></w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12">en</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="13">e</seg></w> <w n="7.7" punct="vg:15">d<seg phoneme="u" type="vs" value="1" rule="425" place="14">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="15" punct="vg">eu</seg>r</w>,</l>
				<l n="8" num="4.2" lm="10"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" punct="vg">en</seg></w>, <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="8.6">f<seg phoneme="œ" type="vs" value="1" rule="304" place="7">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="8.7" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</w>.</l>
			</lg>
			<lg n="5">
				<l n="9" num="5.1" lm="10"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="9.7" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</w>,</l>
				<l n="10" num="5.2" lm="12"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3" punct="vg:5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>, <w n="10.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></w>, <w n="10.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.6">qu</w>’<w n="10.7"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="10.8">f<seg phoneme="œ" type="vs" value="1" rule="304" place="10">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="10.9" punct="pt:12">l<seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pt">à</seg></w>.</l>
			</lg>
			<lg n="6">
				<l n="11" num="6.1" lm="13"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">m</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rç<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="11.8">qu</w>’<w n="11.9"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="11.11" punct="dp:13">cr<seg phoneme="ə" type="em" value="1" rule="e-19" place="12">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="13" punct="dp">é</seg></w> :</l>
				<l n="12" num="6.2" lm="16"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="12.7" punct="vg:9"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="12.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="12.9" punct="vg:13">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="12">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="13" punct="vg">ez</seg></w>, <w n="12.10">d</w>’<w n="12.11" punct="pt:16"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="14">in</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="15">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="16" punct="pt">er</seg></w>.</l>
			</lg>
	</div></body></text></TEI>