<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN5" modus="sp" lm_max="6">
		<head type="main">DU PAYS TOURANGEAU</head>
			<div type="section" n="1">
			<head type="number">I</head>
			<lg n="1">
				<l n="1" num="1.1" lm="6"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ge<seg phoneme="o" type="vs" value="1" rule="314" place="6">au</seg></w></l>
				<l n="2" num="1.2" lm="6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="3" num="1.3" lm="6"><w n="3.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="4" num="1.4" lm="6"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3" punct="pt:6">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></w>.</l>
				<l n="5" num="1.5" lm="6"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">Y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.6">l</w>’<w n="5.7" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
				<l n="6" num="1.6" lm="6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">Y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="6.6" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
				<l n="7" num="1.7" lm="6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2" punct="vg:3">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>,</l>
				<l n="8" num="1.8" lm="6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.5" punct="pt:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>r</w>.</l>
				<l n="9" num="1.9" lm="6"><w n="9.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="10" num="1.10" lm="6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="11" num="1.11" lm="6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="11.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="11.4" punct="tc:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg ti">ou</seg>rs</w>, —</l>
				<l n="12" num="1.12" lm="6"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4" punct="dp:6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></w> :</l>
				<l n="13" num="1.13" lm="2"><space unit="char" quantity="8"></space><w n="13.1" punct="vg:1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s</w>, <w n="13.2" punct="vg:2">Y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
				<l n="14" num="1.14" lm="5"><w n="14.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w> <w n="14.2">Y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4" punct="pe:5">T<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pe">ou</seg>rs</w> !</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="number">II</head>
			<lg n="1">
				<l n="15" num="1.1" lm="6"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="15.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w></l>
				<l n="16" num="1.2" lm="6"><w n="16.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="16.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.4" punct="vg:6">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
				<l n="17" num="1.3" lm="6"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2" punct="vg:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>s</w>, <w n="17.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="18" num="1.4" lm="6"><w n="18.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="18.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="18.5" punct="pt:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</w>.</l>
				<l n="19" num="1.5" lm="6"><w n="19.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="19.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="19.4" punct="dp:6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></w> :</l>
				<l n="20" num="1.6" lm="6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.2" punct="dp:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></w> :</l>
				<l n="21" num="1.7" lm="6">— <w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="21.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">m</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="21.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>,</l>
				<l n="22" num="1.8" lm="6"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="22.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="22.3">c</w>’<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.6" punct="pt:6">tr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt ti">a</seg>s</w>. —</l>
				<l n="23" num="1.9" lm="6"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>t</w> <w n="23.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">s</w>’<w n="23.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="24" num="1.10" lm="6"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">cr<seg phoneme="y" type="vs" value="1" rule="454" place="2">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="25" num="1.11" lm="6"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="25.5" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="dp">ou</seg>rs</w> :</l>
				<l n="26" num="1.12" lm="6"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="26.2">s</w>’<w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="26.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="26.6" punct="tc:6">t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ti">e</seg></w> —</l>
				<l n="27" num="1.13" lm="2"><space unit="char" quantity="8"></space><w n="27.1" punct="vg:1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s</w>, <w n="27.2" punct="vg:2">Y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
				<l n="28" num="1.14" lm="5"><w n="28.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w> <w n="28.2">Y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.4" punct="pe:5">T<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pe">ou</seg>rs</w> !</l>
			</lg>
			</div>
	</div></body></text></TEI>