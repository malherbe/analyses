<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN12" modus="cp" lm_max="16">
		<head type="main">CE QUE L’ON ENTENDAIT LE SOIR <lb></lb>DANS LES RUES DE GÊNES</head>
			<lg n="1">
				<l n="1" num="1.1" lm="13"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="6">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="12">e</seg></w> <w n="1.7" punct="vg:13">G<seg phoneme="ɛ" type="vs" value="1" rule="411" place="13">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="vg">e</seg>s</w>,</l>
				<l n="2" num="1.2" lm="16"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">fr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="y" type="vs" value="1" rule="d-3" place="9">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="2.5" punct="tc:12">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="12" punct="ti">e</seg>s</w> — <w n="2.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="13">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="14">ai</seg></w>-<w n="2.7" punct="tc:15">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="15" punct="ti">e</seg></w> — <w n="2.8" punct="vg:16">b<seg phoneme="u" type="vs" value="1" rule="425" place="16">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="vg">e</seg>s</w>,</l>
				<l n="3" num="1.3" lm="15"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.5" punct="vg:11">l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>x<seg phoneme="y" type="vs" value="1" rule="d-3" place="8">u</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11" punct="vg">en</seg>t</w>, <w n="3.6">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="457" place="13">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="14">e</seg></w> <w n="3.8" punct="vg:15">r<seg phoneme="u" type="vs" value="1" rule="425" place="15">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="16" punct="vg">e</seg></w>,</l>
				<l n="4" num="1.4" lm="16"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="4.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>str<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="13">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="14">ô</seg>t</w> <w n="4.7" punct="dp:16"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="15">o</seg>bsc<seg phoneme="ɛ" type="vs" value="1" rule="410" place="16">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="dp">e</seg>s</w> :</l>
				<l n="5" num="1.5" lm="16"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>il</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="5.7">c<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="12">e</seg>s</w> <w n="5.9">m<seg phoneme="œ" type="vs" value="1" rule="389" place="13">oeu</seg>rs</w> <w n="5.10" punct="pt:16"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="14">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="15">i</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="16">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt">e</seg>s</w>.</l>
			</lg>
			<lg n="2">
				<l n="6" num="2.1" lm="13"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="6.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">j</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="6.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="6.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>cts</w> <w n="6.11" punct="dp:13">m<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="13" punct="dp">ai</seg>s</w> :</l>
				<l n="7" num="2.2" lm="14"><w n="7.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="7.6">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="8">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w> <w n="7.8" punct="vg:14">v<seg phoneme="wa" type="vs" value="1" rule="420" place="13">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="14">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="vg">e</seg>s</w>,</l>
				<l n="8" num="2.3" lm="11"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="8.3">l</w>’<w n="8.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="8.5">d</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="8.7" punct="vg:11">l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg">e</seg></w>,</l>
				<l n="9" num="2.4" lm="14"><w n="9.1">D</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="9.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="12">e</seg></w> <w n="9.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="13">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="14">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15">e</seg></w></l>
				<l n="10" num="2.5" lm="16"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ls</w> <w n="10.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.6">f<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w> <w n="10.7">d</w>’<w n="10.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="13">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="14">e</seg></w> <w n="10.9" punct="vg:16">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="15">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="16" punct="vg">ai</seg>s</w>,</l>
				<l n="11" num="2.6" lm="13"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="11.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="11.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></w>, <w n="11.5">d<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg></w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l</w> <w n="11.8" punct="pt:13">G<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="13" punct="pt">ai</seg>s</w>.</l>
			</lg>
			<lg n="3">
				<l n="12" num="3.1" lm="14"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="12.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="12">e</seg>s</w> <w n="12.11" punct="vg:14">m<seg phoneme="a" type="vs" value="1" rule="340" place="13">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="491" place="14">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="vg">e</seg>s</w>,</l>
				<l n="13" num="3.2" lm="14"><w n="13.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lli<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="13.3" punct="vg:5">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5" punct="vg">en</seg>t</w>, <w n="13.4">fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="12">e</seg>s</w> <w n="13.7" punct="dp:14"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="13">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491" place="14">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="dp">e</seg>s</w> :</l>
			</lg>
			<lg n="4">
				<l n="14" num="4.1" lm="7"><space unit="char" quantity="12"></space><w n="14.1" punct="vg:4">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="14.2" punct="vg:7">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
				<l n="15" num="4.2" lm="6"><space unit="char" quantity="12"></space><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">t</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="15.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.7" punct="vg:6">G<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
				<l n="16" num="4.3" lm="6"><space unit="char" quantity="12"></space><w n="16.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="16.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
				<l n="17" num="4.4" lm="6"><space unit="char" quantity="12"></space><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="17.4" punct="pv:6"><seg phoneme="ø" type="vs" value="1" rule="405" place="5">Eu</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
				<l n="18" num="4.5" lm="6"><space unit="char" quantity="12"></space><w n="18.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">t</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="18.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.7" punct="vg:6">G<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
				<l n="19" num="4.6" lm="6"><space unit="char" quantity="12"></space><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="19.3" punct="vg:6">h<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
				<l n="20" num="4.7" lm="7"><space unit="char" quantity="12"></space><w n="20.1" punct="vg:4">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="20.2" punct="pe:7">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
			</lg>
			<lg n="5">
				<l n="21" num="5.1" lm="16"><w n="21.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="21.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p</w> <w n="21.5">d</w>’<w n="21.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="21.7">ch<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="12">e</seg>s</w> <w n="21.8" punct="pv:16">sp<seg phoneme="i" type="vs" value="1" rule="468" place="13">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="14">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="15">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="16">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pv">e</seg>s</w> ;</l>
				<l n="22" num="5.2" lm="16"><w n="22.1" punct="vg:5">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" punct="vg">en</seg>t</w>, <w n="22.2">c</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="22.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="22.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="22.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="22.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="22.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="12">e</seg></w> <w n="22.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="13">e</seg></w> <w n="22.11">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="14">e</seg></w> <w n="22.12" punct="pt:16">r<seg phoneme="a" type="vs" value="1" rule="340" place="15">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="16">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="17" punct="pt">e</seg></w>.</l>
			</lg>
	</div></body></text></TEI>