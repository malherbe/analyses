<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR155" modus="cm" lm_max="12">
						<head type="main">TEMPS DES FÉES</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="1.3" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="1.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="1.6" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>x</w> <w n="1.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="1.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="1.10" punct="vg:12">F<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="2.6" punct="vg:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>, <w n="2.7">ch<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="2.9" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>ts</w>,</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3" punct="vg:4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.6">r<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="3.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1" lm="12"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="4.3" punct="vg:4">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="4.7" punct="vg:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg></w>, <w n="4.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="4.10" punct="vg:12">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>s</w>,</l>
							<l n="5" num="2.2" lm="12"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>rs</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">b<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="5.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.9" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="6" num="2.3" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="6.6" punct="vg:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>, <w n="6.7">ch<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.9" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>ts</w>,</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1" lm="12"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="7.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="7.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="7.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.8" punct="vg:12">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="8" num="3.2" lm="12"><w n="8.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="8.9" punct="vg:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</w>,</l>
							<l n="9" num="3.3" lm="12"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>rs</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">b<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.9" punct="pt:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1" lm="12">— <w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.4" punct="vg:4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>x</w>, <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="10.7" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg">e</seg></w>, <w n="10.8">l</w>’<w n="10.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt</w> <w n="10.10" punct="ps:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="ps">an</seg>t</w>…</l>
							<l n="11" num="4.2" lm="12"><w n="11.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="11.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="11.8">d</w>’<w n="11.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="12" num="4.3" lm="12"><w n="12.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.9" punct="pe:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pe">en</seg>t</w> !</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1" lm="12"><w n="13.1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="13.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="13.8" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps">e</seg></w> !…</l>
							<l n="14" num="5.2" lm="12"><w n="14.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">M<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="14.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="14.7"><seg phoneme="y" type="vs" value="1" rule="251" place="8">eû</seg>t</w> <w n="14.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="14.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="14.10" punct="pv:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>rs</w> ;</l>
							<l n="15" num="5.3" lm="12"><w n="15.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="15.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="15.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="15.8">d</w>’<w n="15.9" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1" lm="12"><w n="16.1">C</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="16.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="16.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="16.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.8">j<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="16.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="16.11" punct="pe:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>rs</w> !</l>
							<l n="17" num="6.2" lm="12"><w n="17.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="17.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="17.7" punct="ps:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg>s</w>…</l>
							<l n="18" num="6.3" lm="12"><w n="18.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">M<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b</w> <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="18.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="18.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="18.7"><seg phoneme="y" type="vs" value="1" rule="251" place="8">eû</seg>t</w> <w n="18.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="18.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="18.10" punct="vg:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</w>,</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1" lm="12"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="19.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="19.3" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="19.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="19.6" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>, <w n="19.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>x</w> <w n="19.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="19.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="19.10" punct="pt:12">F<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>