<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR118" modus="cm" lm_max="12">
						<head type="main">PARISIENNE</head>
						<opener>
							<salute>À ACHILLE MÉLANDRI</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>dr<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.6">b<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.7" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1" punct="vg:2">Dr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.8" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2" punct="dp:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="dp">a</seg></w> : <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8">b<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="4.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>t</w>, <w n="4.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="4.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="4.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="4.11" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2" punct="pt:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt">a</seg></w>. <w n="5.3">S<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.8">l</w>’<w n="5.9" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>d</w> <w n="6.3" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>f</w>, <w n="6.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg></w> <w n="6.7">d</w>’<w n="6.8" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>t</w>, <w n="6.9" punct="vg:12">tr<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l</w>’<w n="7.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>sph<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.5" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>, <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="7.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="7.10" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="8" num="2.4" lm="12"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="8.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="8.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="8.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="8.10" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="9.3" punct="vg:4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="9.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="9.9" punct="vg:12">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>il</w> <w n="10.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="10.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="10.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.8">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">r<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="11.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="11.9" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pt">en</seg>t</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.5" punct="vg:8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</w>, <w n="12.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="12.8" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="13" num="4.2" lm="12"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="2">um</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3" punct="ps:4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="ps">eu</seg>rs</w>… <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">E</seg>t</w> <w n="13.5" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.7" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg">e</seg></w>, <w n="13.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
							<l n="14" num="4.3" lm="12"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rs</w> <w n="14.5">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ds</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="14.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ls</w> <w n="14.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg>s</w> <w n="14.9">l</w>’<w n="14.10" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>