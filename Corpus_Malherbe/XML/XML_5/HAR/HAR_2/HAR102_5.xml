<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">L’AUBE</head><div type="poem" key="HAR102" modus="cp" lm_max="12">
						<head type="main">À LA DÉSOLÉE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="1.5" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="vg">à</seg></w>, <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.10">t<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="1.11" punct="vg:12">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">M<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="2.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.8" punct="pv:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pv">e</seg>il</w> ;</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="3.9">h<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
							<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ts</w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.6" punct="pe:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pe">e</seg>il</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="5.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="5.8">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l</w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.10" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="6.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="7.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="8" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="8.4">n</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="8.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.8" punct="ps:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="ps">eu</seg>r</w>…</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="9.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">Pl<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="10.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.8" punct="vg:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</w>,</l>
							<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.7" punct="vg:6">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>t</w>, <w n="11.8"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="11.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.10">p<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="12" num="3.4" lm="8"><space unit="char" quantity="8"></space><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="12.7" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>r</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1" lm="12"><w n="13.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="13.5" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="vg">à</seg></w>, <w n="13.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="13.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="13.9" punct="vg:12">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="14" num="4.2" lm="12"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="14.7">p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.9" punct="vg:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
							<l n="15" num="4.3" lm="12"><w n="15.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="15.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="15.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="16" num="4.4" lm="8"><space unit="char" quantity="8"></space><w n="16.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.7" punct="pt:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1" lm="12"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="17.5">n</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="17.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="17.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="17.10" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="18" num="5.2" lm="12"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3" punct="vg:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="18.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="18.6">m</w>’<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="18.8">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="8">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="18.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="18.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="18.11" punct="vg:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>r</w>,</l>
							<l n="19" num="5.3" lm="12"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="19.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="19.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>l</w> <w n="19.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="19.8">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="20" num="5.4" lm="8"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="20.2">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xcl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>f</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="20.5" punct="pe:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1" lm="12">— <w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">m</w>’<w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="21.5" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="21.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="21.7">l</w>’<w n="21.8"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="21.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.10"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="21.11" punct="vg:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="22" num="6.2" lm="12"><w n="22.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="22.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="22.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="22.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="22.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="22.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="22.10" punct="vg:12">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></w>,</l>
							<l n="23" num="6.3" lm="12"><w n="23.1">Cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="23.3" punct="vg:4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="23.4">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="23.7">p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="23.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="23.9" punct="dp:12">ch<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg>s</w> :</l>
							<l n="24" num="6.4" lm="8"><space unit="char" quantity="8"></space>« <w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="24.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="24.7" punct="pe:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pe">en</seg></w> ! »</l>
						</lg>
					</div></body></text></TEI>