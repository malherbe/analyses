<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR124" modus="cm" lm_max="12">
						<head type="main">ADULTÈRE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="1.6" punct="vg:8">st<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.9">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>t</w></l>
							<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="2.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="2.5">qu</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.7">l</w>’<w n="2.8" punct="dp:12">h<seg phoneme="i" type="vs" value="1" rule="497" place="10">y</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="3.3">t</w>’<w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="3.8">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l</w> <w n="3.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.10">t</w>’<w n="3.11"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="3.12" punct="vg:12">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="4.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.5">n</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="4.8">qu</w>’<w n="4.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.10">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="4.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.12" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g</w> <w n="5.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.8" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rt</w>, <w n="5.9" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>d</w>, <w n="5.10" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r</w> <w n="6.7" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="7.7">c<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="7.9">t</w>’<w n="7.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="7.11">pr<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="8" num="2.4" lm="12"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">br<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="8.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="8.8">s</w>’<w n="8.9" punct="ps:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="10">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="ps">ai</seg>t</w>…</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="9.2">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="9.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="9.9" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="pe:6">d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="pe">ai</seg></w> ! <w n="10.5">T<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="10.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s</w> <w n="10.8" punct="pv:12">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="11" num="3.3" lm="12"><w n="11.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="11.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="11.4">d<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="11.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="11.7">l</w>’<w n="11.8" punct="dp:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="dp">i</seg></w> :</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3" punct="vg:3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="12.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.8" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="13" num="4.2" lm="12"><w n="13.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="13.8" punct="vg:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
							<l n="14" num="4.3" lm="12"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="14.4">qu</w>’<w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.8" punct="pe:12">cr<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>