<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR147" modus="cm" lm_max="12">
						<head type="main">CHIENS ERRANTS</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1" punct="dp:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="dp">e</seg></w> : <w n="1.2" punct="pi:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="4" punct="pi">oi</seg></w> ? <w n="1.3" punct="dp:6">M<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp">i</seg>r</w> : <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="1.5" punct="pi:8">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="8" punct="pi">oi</seg></w> ? <w n="1.6">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="1.9" punct="pv:12">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pv">en</seg></w> ;</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.6">b<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ts</w> <w n="2.7" punct="vg:9"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg>s</w>, <w n="2.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="2.10">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12">en</seg></w></l>
							<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="3.8" punct="dp:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>rs</w>, <w n="4.3" punct="vg:3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="4.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.8" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</w>,</l>
							<l n="5" num="1.5" lm="12"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="2">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="5.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.12" punct="vg:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</w>,</l>
							<l n="6" num="1.6" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="6.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>t</w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="6.11" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>