<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR128" modus="cm" lm_max="12">
						<head type="main">VERS À CIRCÉ</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>x</w> <w n="1.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bh<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.7" punct="vg:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rd</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="2.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="2.5">t<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="2.8">m<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="2.9" punct="vg:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="3.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="4.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="4.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="4.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="4.11" punct="pe:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pe">o</seg>rt</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">H<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3" punct="pe:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="5.4">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="5.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="5.10" punct="vg:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="3">oû</seg>t</w> <w n="6.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>t</w> <w n="6.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="6.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.9" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="7.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>p<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="8" num="2.4" lm="12"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428" place="3">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.4">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rd</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12">— <w n="9.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="9.2">l</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.6" punct="vg:9">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>r</w>, <w n="9.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="9.9" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">t</w>’<w n="10.5"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="9">ue</seg>il</w> <w n="10.9">d</w>’<w n="10.10"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="10.11" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="11.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.8" punct="pe:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>s</w> !</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="12.7" punct="dp:12">n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>vr<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg>s</w> :</l>
							<l n="13" num="4.2" lm="12"><w n="13.1" punct="vg:2">J<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="13.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>rs</w> <w n="13.6">m<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ts</w></l>
							<l n="14" num="4.3" lm="12"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="14.5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="14.8" punct="pt:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>