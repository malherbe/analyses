<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">CHANSON NOMADE</head><div type="poem" key="DAN22">
						<head type="main">Le désert</head>
						<lg n="1">
							<l ana="unanalyzable" n="1" num="1.1">J’suis Arab’ sans en avoir l’air</l>
							<l ana="unanalyzable" n="2" num="1.2">Et ma vie s’coul’ dans un désert,</l>
							<l ana="unanalyzable" n="3" num="1.3">Une désolation à pein’ pénétrée</l>
							<l ana="unanalyzable" n="4" num="1.4">Au fond de l’Arabie Pétrée,</l>
							<l ana="unanalyzable" n="5" num="1.5">Ousqu’il n’y a rien qu’ du sable gris,</l>
							<l ana="unanalyzable" n="6" num="1.6">Larrabi,</l>
							<l ana="unanalyzable" n="7" num="1.7">Dans la plain’, les butt’s et les creux,</l>
							<l ana="unanalyzable" n="8" num="1.8">Dans les fent’s des galets ocreux,</l>
							<l ana="unanalyzable" n="9" num="1.9">Dans les sandal’s et dans les yeux ;</l>
							<l ana="unanalyzable" n="10" num="1.10">Tant de sable, mon Dieu ! tant d’sable !</l>
							<l ana="unanalyzable" n="11" num="1.11">Comm’ si l’Pacifique, obsédé du diable,</l>
							<l ana="unanalyzable" n="12" num="1.12">Avait filtré à travers tout son sable !</l>
							<l ana="unanalyzable" n="13" num="1.13">Sur ce parterr’ de sable gris,</l>
							<l ana="unanalyzable" n="14" num="1.14">Larrabi,</l>
							<l ana="unanalyzable" n="15" num="1.15">C’qui pouss’, c’est les moignons tordus</l>
							<l ana="unanalyzable" n="16" num="1.16">Et pointus d’quéq’ maigres cactus</l>
							<l ana="unanalyzable" n="17" num="1.17">Qu’ont l’air accroupis sur la dune,</l>
							<l ana="unanalyzable" n="18" num="1.18">La nuit, pour fair’ peur à la lune,</l>
							<l ana="unanalyzable" n="19" num="1.19">Et l’ j our, qu’ont l’air de sing’s méchants</l>
							<l ana="unanalyzable" n="20" num="1.20">Prêts à vous griffer en passant,</l>
							<l ana="unanalyzable" n="21" num="1.21">Et des mouss’s en papier mâché</l>
							<l ana="unanalyzable" n="22" num="1.22">(Ou s’rait-ce d’la cendre ayant cru végéter ?)</l>
							<l ana="unanalyzable" n="23" num="1.23">Qui craqu’nt et s’défont sous votr’ pied.</l>
						</lg>
						<lg n="2">
							<l ana="unanalyzable" n="24" num="2.1">Pas de trèfle, pas de bruyère,</l>
							<l ana="unanalyzable" n="25" num="2.2">Pas d’foin d’odeur ni d’fougères,</l>
							<l ana="unanalyzable" n="26" num="2.3">Pas de merles dans les buissons,</l>
							<l ana="unanalyzable" n="27" num="2.4">Pas de buissons, et pas d’chansons.</l>
							<l ana="unanalyzable" n="28" num="2.5">Pas d’pacages où le bétail broute,</l>
							<l ana="unanalyzable" n="29" num="2.6">Pas de haies vives et pas d’routes.</l>
							<l ana="unanalyzable" n="30" num="2.7">Ni homm’ ni femm’, bien entendu,</l>
							<l ana="unanalyzable" n="31" num="2.8">Pas plus qu’dans l’paradis perdu.</l>
							<l ana="unanalyzable" n="32" num="2.9">La richess’ de ce royaum’-ci,</l>
							<l ana="unanalyzable" n="33" num="2.10">Larrabi,</l>
							<l ana="unanalyzable" n="34" num="2.11">Est tout’ dans sa superficie ;</l>
							<l ana="unanalyzable" n="35" num="2.12">I’ s’y empil’, sans qu ça renverse,</l>
							<l ana="unanalyzable" n="36" num="2.13">Des mill’s, des kilomètr’s et des verstes.</l>
							<l ana="unanalyzable" n="37" num="2.14">Les patrimoin’s, les parcs Lenôtre,</l>
							<l ana="unanalyzable" n="38" num="2.15">S’mesur’nt d’un horizon à l’autre</l>
							<l ana="unanalyzable" n="39" num="2.16">Et s’étal’nt sous le dur soleil</l>
							<l ana="unanalyzable" n="40" num="2.17">Tous plats, tous chauv’s et tous pareils.</l>
							<l ana="unanalyzable" n="41" num="2.18">L’âm’ se sent mince et comm’ fondue</l>
							<l ana="unanalyzable" n="42" num="2.19">D’vant tant et tant d’étendue</l>
							<l ana="unanalyzable" n="43" num="2.20">Si nue ! et quant aux oasis,</l>
							<l ana="unanalyzable" n="44" num="2.21">C’est des cont’s de cerveaux moisis ;</l>
							<l ana="unanalyzable" n="45" num="2.22">Et la mann’, pour s’faire un festin</l>
							<l ana="unanalyzable" n="46" num="2.23">Faudrait se l’ver d’trop grand matin.</l>
						</lg>
						<lg n="3">
							<l ana="unanalyzable" n="47" num="3.1">C’est dans c’vaste et large pays</l>
							<l ana="unanalyzable" n="48" num="3.2">Qu’en bon Arab’ j’ai mon gourbi,</l>
							<l ana="unanalyzable" n="49" num="3.3">Et là, depuis vingt ans entiers,</l>
							<l ana="unanalyzable" n="50" num="3.4">J’fais l’commerce des briqu’s et mortiers,</l>
							<l ana="unanalyzable" n="51" num="3.5">Que pour du biscuit, d’loin en loin,</l>
							<l ana="unanalyzable" n="52" num="3.6">J’trafique à mes frèr’s les Bédouins ;</l>
							<l ana="unanalyzable" n="53" num="3.7">Et Preste du temps, je m’pavane</l>
							<l ana="unanalyzable" n="54" num="3.8">Loin du sentier des caravanes.</l>
							<l ana="unanalyzable" n="55" num="3.9">J’suis seul, mais je suis libre aussi,</l>
							<l ana="unanalyzable" n="56" num="3.10">Larrabi,</l>
							<l ana="unanalyzable" n="57" num="3.11">Dans c’te capital’ du Gobi</l>
							<l ana="unanalyzable" n="58" num="3.12">Y a pas d’règlements qui m’embêtent ;</l>
							<l ana="unanalyzable" n="59" num="3.13">Avec moi-mêm’ j’peux fair’ la fête,</l>
							<l ana="unanalyzable" n="60" num="3.14">J’peux être bolchévik, si j ’veux,</l>
							<l ana="unanalyzable" n="61" num="3.15">Sans qu’ça rend’ les banquiers nerveux ;</l>
							<l ana="unanalyzable" n="62" num="3.16">Et j’suis pas bousculé quand j’passe :</l>
							<l ana="unanalyzable" n="63" num="3.17">La rue occupe tout l’espace.</l>
						</lg>
						<lg n="4">
							<l ana="unanalyzable" n="64" num="4.1">Mais c’est vrai que l’domaine est chaud</l>
							<l ana="unanalyzable" n="65" num="4.2">À rendr’ piteux les hauts fourneaux ;</l>
							<l ana="unanalyzable" n="66" num="4.3">À r’gretter la r’traite bucolique</l>
							<l ana="unanalyzable" n="67" num="4.4">D’la sous-cal’ des transatlantiques ;</l>
							<l ana="unanalyzable" n="68" num="4.5">Si chaud, que la plupart des êtres</l>
							<l ana="unanalyzable" n="69" num="4.6">Sont empaillés avant de naître,</l>
							<l ana="unanalyzable" n="70" num="4.7">C’qui fait qu’ils ne naiss’nt pas du tout.</l>
							<l ana="unanalyzable" n="71" num="4.8">L’air s’révolte et fuse en grisou,</l>
							<l ana="unanalyzable" n="72" num="4.9">Le soleil ouvre un’ gueul’ de braise ;</l>
							<l ana="unanalyzable" n="73" num="4.10">Pour parasols à c’te fournaise</l>
							<l ana="unanalyzable" n="74" num="4.11">Y a qu’des palmiers aux feuill’s d’enseigne</l>
							<l ana="unanalyzable" n="75" num="4.12">Qui laiss’nt passer l’jour comme des peignes.</l>
							<l ana="unanalyzable" n="76" num="4.13">Au fait, sur l’sol que c’feu surplombe,</l>
							<l ana="unanalyzable" n="77" num="4.14">J’suis l’seul écran à fair’ de l’ombre</l>
							<l ana="unanalyzable" n="78" num="4.15">Et j’me r’présente, au coup d’midi,</l>
							<l ana="unanalyzable" n="79" num="4.16">Un copeau dans un incendie,</l>
							<l ana="unanalyzable" n="80" num="4.17">Larrabi</l>
							<l ana="unanalyzable" n="81" num="4.18">Puis c’qu’achèv’ de m’rendre stupide,</l>
							<l ana="unanalyzable" n="82" num="4.19">C’est tant d’ flamme et si peu d’ liquide !</l>
							<l ana="unanalyzable" n="83" num="4.20">Ce que j’donn’rais pour une rivière,</l>
							<l ana="unanalyzable" n="84" num="4.21">Pour une mare, pour une gouttière !</l>
							<l ana="unanalyzable" n="85" num="4.22">Oh ! l’eau qui fredonne et qui rit !</l>
							<l ana="unanalyzable" n="86" num="4.23">Mais, au trou des fossés croupis,</l>
							<l ana="unanalyzable" n="87" num="4.24">Larrabi,</l>
							<l ana="unanalyzable" n="88" num="4.25">I’ n’en rest’ pas, d’puis l’temps qu’ell’ bouille,</l>
							<l ana="unanalyzable" n="89" num="4.26">Assez pour flotter une grenouille,</l>
							<l ana="unanalyzable" n="90" num="4.27">Et j’suis des jours, quoiqu’maladif,</l>
							<l ana="unanalyzable" n="91" num="4.28">Sans l’plus minime apéritif,</l>
							<l ana="unanalyzable" n="92" num="4.29">Forcé, comm’ les chameaux, d’bercer</l>
							<l ana="unanalyzable" n="93" num="4.30">D’espéranc’s mon gosier gercé.</l>
						</lg>
						<lg n="5">
							<l ana="unanalyzable" n="94" num="5.1">Y a les mirages : c’est drôl’ comm’ tout !</l>
							<l ana="unanalyzable" n="95" num="5.2">C’est des rêv’s qu’on fait tout d’bout ;</l>
							<l ana="unanalyzable" n="96" num="5.3">On voit des tours, des esplanades,</l>
							<l ana="unanalyzable" n="97" num="5.4">Des bois, des fontain’s qui cascadent,</l>
							<l ana="unanalyzable" n="98" num="5.5">Des icoglans et des houris,</l>
							<l ana="unanalyzable" n="99" num="5.6">Larrabi.</l>
							<l ana="unanalyzable" n="100" num="5.7">Mais l’plus rasant de c’phénomène,</l>
							<l ana="unanalyzable" n="101" num="5.8">C’est qu’tout’s les chos’s qui s’y promènent,</l>
							<l ana="unanalyzable" n="102" num="5.9">Les homm’s, les forêts vierg’s, tout ça</l>
							<l ana="unanalyzable" n="103" num="5.10">S’tient et circul’ la tête en bas</l>
							<l ana="unanalyzable" n="104" num="5.11">C’qui vous donn’ la sensation bête</l>
							<l ana="unanalyzable" n="105" num="5.12">D’avoir les antipod’s sur la tête…</l>
							<l ana="unanalyzable" n="106" num="5.13">Et puis c’n’est qu’un nuage farceur</l>
							<l ana="unanalyzable" n="107" num="5.14">Qui s’était payé votr’ bon coeur.</l>
						</lg>
						<lg n="6">
							<l ana="unanalyzable" n="108" num="6.1">Non, c’n’est pas l’pays d’Rarahu !</l>
							<l ana="unanalyzable" n="109" num="6.2">La nuit, les chacals font l’chahut,</l>
							<l ana="unanalyzable" n="110" num="6.3">Quéq’ lion ou quéq’ tigre s’amène,</l>
							<l ana="unanalyzable" n="111" num="6.4">Ou bien de dégoûtant’s hyènes</l>
							<l ana="unanalyzable" n="112" num="6.5">Qui dans les môl’s de sable gris,</l>
							<l ana="unanalyzable" n="113" num="6.6">Larrabi,</l>
							<l ana="unanalyzable" n="114" num="6.7">Déterr’nt les macchabées pourris</l>
							<l ana="unanalyzable" n="115" num="6.8">Et rigol’nt dans leurs faces de fouines</l>
							<l ana="unanalyzable" n="116" num="6.9">En s’pourléchant leurs sal’s babines.</l>
							<l ana="unanalyzable" n="117" num="6.10">Vous n’sauriez croir’ comm’ ces animaux-là</l>
							<l ana="unanalyzable" n="118" num="6.11">Hurl’nt faux et triste : on dirait un glas !</l>
							<l ana="unanalyzable" n="119" num="6.12">Quant i’ s’mett’nt tous à faire : Hou ! hou !</l>
							<l ana="unanalyzable" n="120" num="6.13">J’ai l’cauch’mar des topapahous.</l>
						</lg>
						<lg n="7">
							<l ana="unanalyzable" n="121" num="7.1">Et l’simoun, c’est ça qu’est bassinant !</l>
							<l ana="unanalyzable" n="122" num="7.2">Figurez-vous la ros’ des vents</l>
							<l ana="unanalyzable" n="123" num="7.3">Qui de sa tige s’rait secouée</l>
							<l ana="unanalyzable" n="124" num="7.4">Et s’effeuill’rait sur la contrée ;</l>
							<l ana="unanalyzable" n="125" num="7.5">Ou tous les tuyaux à soupirs</l>
							<l ana="unanalyzable" n="126" num="7.6">Crevés dans l’usin’ des zéphyrs.</l>
							<l ana="unanalyzable" n="127" num="7.7">Comme une caval’rie d’uhlans ivres</l>
							<l ana="unanalyzable" n="128" num="7.8">Du fond d’l’horizon ça dérive</l>
							<l ana="unanalyzable" n="129" num="7.9">Dans une charge qu’emporte en l’air</l>
							<l ana="unanalyzable" n="130" num="7.10">L’sol et la toitu’ du désert.</l>
							<l ana="unanalyzable" n="131" num="7.11">C’est tous les tonnerr’s, tout’s les trombes,</l>
							<l ana="unanalyzable" n="132" num="7.12">Tous les cyclones et tout’s les bombes.</l>
							<l ana="unanalyzable" n="133" num="7.13">Pis qu’des goul’s de Mille et une Nuits</l>
							<l ana="unanalyzable" n="134" num="7.14">Ca braill’, ça miaule et ça gémit,</l>
							<l ana="unanalyzable" n="135" num="7.15">Larrabi,</l>
							<l ana="unanalyzable" n="136" num="7.16">Et ça souffle à travers votr’ porte</l>
							<l ana="unanalyzable" n="137" num="7.17">Tout l’sel et l’soufr’ de la Mer Morte.</l>
						</lg>
						<lg n="8">
							<l ana="unanalyzable" n="138" num="8.1">Ah ! mais vous’ n’m’avez pas compris !</l>
							<l ana="unanalyzable" n="139" num="8.2">Ou p’t’êr’ vous croyez que j’faribole ?</l>
							<l ana="unanalyzable" n="140" num="8.3">Tout ça, c’est des symboles,</l>
							<l ana="unanalyzable" n="141" num="8.4">Et j’en grimaç’ plus que j’n’en ris,</l>
							<l ana="unanalyzable" n="142" num="8.5">Larrabi,</l>
							<l ana="unanalyzable" n="143" num="8.6">L’désert qui sèch’ dans sa torpeur,</l>
							<l ana="unanalyzable" n="144" num="8.7">C’est la grande solitude de mon coeur ;</l>
							<l ana="unanalyzable" n="145" num="8.8">Et les milliards de grains de sable,</l>
							<l ana="unanalyzable" n="146" num="8.9">C’est tout les atôm’s lamentables</l>
							<l ana="unanalyzable" n="147" num="8.10">De mes pensées et de mes rêves</l>
							<l ana="unanalyzable" n="148" num="8.11">Que mon âm’ retourne et soulève ;</l>
							<l ana="unanalyzable" n="149" num="8.12">Et ’soleil qui flambe et qui cuit,</l>
							<l ana="unanalyzable" n="150" num="8.13">C’est ma fièvre et c’est mon ennui.</l>
							<l ana="unanalyzable" n="151" num="8.14">La soif qui m’ronge comme un vautour,</l>
							<l ana="unanalyzable" n="152" num="8.15">C’est l’tourment qu’ j’ai d’un grand amour ;</l>
							<l ana="unanalyzable" n="153" num="8.16">Et Pépin’ des cactus, hélas !</l>
							<l ana="unanalyzable" n="154" num="8.17">C’est le coeur de cell’ qui n’m’aim’ pas.</l>
							<l ana="unanalyzable" n="155" num="8.18">Et mon commerce, ah ! mon commerce,</l>
							<l ana="unanalyzable" n="156" num="8.19">C’est les métiers vils que j’exerce</l>
							<l ana="unanalyzable" n="157" num="8.20">D’puis que l’mond’ chic m’a fichu orphelin</l>
							<l ana="unanalyzable" n="158" num="8.21">Et que j’turbine hors du droit ch’min,</l>
							<l ana="unanalyzable" n="159" num="8.22">Nomade, et sevré d’sympathie,</l>
							<l ana="unanalyzable" n="160" num="8.23">A’ caus’ d’mon manq’ d’orthodoxie.</l>
							<l ana="unanalyzable" n="161" num="8.24">Les bêt’s rongeant les cadavr’s désossés</l>
							<l ana="unanalyzable" n="162" num="8.25">C’est les souv’nirs qui dévor’nt mon passé ;</l>
							<l ana="unanalyzable" n="163" num="8.26">Et les mirages foux qui s’renversent</l>
							<l ana="unanalyzable" n="164" num="8.27">C’est mes espoirs que l’sort boul’verse ;</l>
							<l ana="unanalyzable" n="165" num="8.28">Et l’espace vide, illimité,</l>
							<l ana="unanalyzable" n="166" num="8.29">C’est l’fantôm’ de ma liberté ;</l>
							<l ana="unanalyzable" n="167" num="8.30">Et l’simoun qui siffle et qui mord</l>
							<l ana="unanalyzable" n="168" num="8.31">C’est la vie qui, d’tout son effort,</l>
							<l ana="unanalyzable" n="169" num="8.32">M’pouss’ vers le Grand Sahara d’la mort.</l>
						</lg>
					</div></body></text></TEI>