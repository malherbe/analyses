<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM131" modus="cm" lm_max="12">
				<head type="main">LE VILLAGE OU L’ÉGLISE ÉTAIT BLANCHE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="1.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>p</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="421" place="12">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ds</w> <w n="2.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.7" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>c<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="3.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c</w> <w n="3.5">qu</w>’<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rtu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.7" punct="pt:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="5.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>ssi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="5.7">c<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="6" num="2.2" lm="12"><w n="6.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>si<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="6.3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="6.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>, <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="6.9" punct="vg:12">ru<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2" punct="vg:3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.4">br<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.7" punct="vg:10">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="9">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w>, <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w>-<w n="7.9" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="vg">au</seg>t</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1" punct="vg:2">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="8.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>g</w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="8.8" punct="pt:12">pl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="9.6" punct="vg:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="8" punct="vg">oi</seg></w>, <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="9.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="10.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="10.8" punct="vg:9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>c</w>, <w n="10.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.10" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="11.8" punct="vg:12">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="12.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="10">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg></w> <w n="12.7" punct="pt:12">n<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>