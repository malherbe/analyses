<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM118" modus="cp" lm_max="12">
				<head type="main">DE GRANDS NUAGES NOIRS…</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="1.3">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>rs</w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="1.8" punct="pt:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="2" num="1.2" lm="10"><space unit="char" quantity="4"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="2.6">s</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="2.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="2.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="2.10">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
					<l n="3" num="1.3" lm="10"><space unit="char" quantity="4"></space><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.4">s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="3.5">br<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7">en</seg>t</w> <w n="3.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7" punct="vg:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.6" punct="pt:12">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="11">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2" punct="vg:2">b<seg phoneme="ø" type="vs" value="1" rule="247" place="2" punct="vg">œu</seg>fs</w>, <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="5.4">s</w>’<w n="5.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg></w> <w n="5.9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t</w></l>
					<l n="6" num="2.2" lm="10"><space unit="char" quantity="4"></space><w n="6.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>dr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="6.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.7" punct="vg:10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="7" num="2.3" lm="10"><space unit="char" quantity="4"></space><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="7.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
					<l n="8" num="2.4" lm="12"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="8.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="6">oû</seg>t</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>th<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.11" punct="pt:12">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="374" place="11">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="9.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.7" punct="pt:12">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>il</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="10.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="5">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="10.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t</w> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="10.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</w>,</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="11.3" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>t</w>, <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.7" punct="vg:12">tr<seg phoneme="u" type="vs" value="1" rule="427" place="11">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="12.4">l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rd</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w>-<w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="3">an</seg>t</w> <w n="13.3">pr<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="a" type="vs" value="1" rule="365" place="5">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="421" place="9">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="13.6" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="14" num="4.3" lm="12"><w n="14.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>ps</w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w> <w n="14.8" punct="pt:12">d<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>