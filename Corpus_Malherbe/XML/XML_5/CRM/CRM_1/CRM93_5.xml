<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM93" modus="cm" lm_max="12">
				<head type="main">LE VIEUX CURE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3">c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w>-<w n="1.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg></w> <w n="1.8">c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="2.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="2.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="2.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="2.9" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.2" punct="vg:2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2" punct="vg">en</seg></w>, <w n="3.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="3.8" punct="vg:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</w>,</l>
					<l n="4" num="2.2" lm="12"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="4.7" punct="pt:12">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>ts</w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="5.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="5.7" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="6" num="3.2" lm="12"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.3">D<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.4" punct="vg:6">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="323" place="6" punct="vg">ay</seg></w>, <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="6.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rpr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="7.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
					<l n="8" num="4.2" lm="12"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="8.8">d</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="8.10">gu<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1" lm="12"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>t</w>, <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="9.7" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</w>.</l>
					<l n="10" num="5.2" lm="12"><w n="10.1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="10.2" punct="ps:6">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="451" place="6" punct="ps">u</seg>m</w>… <w n="10.3">Qu</w>’<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="10.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="10.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1" lm="12"><w n="11.1">S</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="11.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="11.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="12">e</seg>c</w></l>
					<l n="12" num="6.2" lm="12"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4" punct="pi:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi">a</seg>s</w> ? <w n="12.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="12.6" punct="vg:8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></w>, <w n="12.7" punct="vg:9">lu<seg phoneme="i" type="vs" value="1" rule="491" place="9" punct="vg">i</seg></w>, <w n="12.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t</w></l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1" lm="12"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="13.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="13.4">c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8" punct="vg">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg></w> <w n="13.8">m<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="13.10" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="12" punct="vg">ai</seg></w>,</l>
					<l n="14" num="7.2" lm="12"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="14.7">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="14.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="14.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.11">pl<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1" lm="12"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="15.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rc</w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="15.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="15.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
			</div></body></text></TEI>