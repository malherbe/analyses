<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM25" modus="cm" lm_max="12">
				<head type="main">PAYSAGE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="1.10" punct="pt:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">Br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="2.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="2.7" punct="pt:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5" punct="pt:6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="pt">au</seg>t</w>. <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.9" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rbi<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg>s</w>,</l>
					<l n="4" num="2.2" lm="12"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>il</w>, <w n="4.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="4.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></w> <w n="4.9" punct="pt:12">g<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">bl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt</w> <w n="5.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="5.9">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="11">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
					<l n="6" num="3.2" lm="12"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="6.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="6.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="6.10" punct="pt:12">di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1" lm="12"><w n="7.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="7.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>f</w> <w n="7.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.8" punct="vg:12">f<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>ts</w>,</l>
					<l n="8" num="4.2" lm="12"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">tr<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="8.8" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12" punct="vg">ê</seg>t</w>,</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1" lm="12"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>t</w>, <w n="9.6" punct="vg:9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg">e</seg>s</w>, <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="9.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="9.9" punct="vg:12">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="5.2" lm="12"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="10.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="10.8" punct="pt:12">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1" lm="12"><w n="11.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="11.5">dr<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="11.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="12" num="6.2" lm="12"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.8" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="13.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="13.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.7">j</w>’<w n="13.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="13.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.10" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
					<l n="14" num="7.2" lm="12"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="14.2" punct="vg:4">sc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.5"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="14.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="14.7" punct="pt:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>