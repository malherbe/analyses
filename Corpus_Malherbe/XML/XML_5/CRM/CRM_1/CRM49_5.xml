<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM49" modus="cm" lm_max="12">
				<head type="main">LES ARBRES</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.10">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="2.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="2.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="2.8"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="2.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="12">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>ts</w> <w n="3.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>ts</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.8">l<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.5">f<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t</w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>g</w> <w n="4.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="4.10" punct="pt:12">qu<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ds</w> <w n="5.9" punct="vg:12">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="6.8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="9">e</seg>ds</w> <w n="6.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="6.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="6.11" punct="vg:12">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>cs</w>,</l>
					<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2" punct="vg:3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.8" punct="vg:12">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>s</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="8.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rd</w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="8.9">f<seg phoneme="œ" type="vs" value="1" rule="406" place="11">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="9.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="9.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="10" num="3.2" lm="12"><w n="10.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="10.8">r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
					<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>th<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.9" punct="pt:12">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="12.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6">an</seg>t</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="12.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="12.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ils</w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="13.5">gl<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg></w></l>
					<l n="14" num="4.3" lm="12"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w>-<w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="14.3">l</w>’<w n="14.4" punct="vg:6">h<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="14.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="14.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.9" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pt">e</seg>il</w>.</l>
				</lg>
			</div></body></text></TEI>