<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM53" modus="cp" lm_max="12">
				<head type="main">LE PETIT CIMETIÈRE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rp<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="1.7" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
					<l n="2" num="1.2" lm="6"><space unit="char" quantity="12"></space><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="2.3">c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="3.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg>x</w></l>
					<l n="4" num="1.4" lm="6"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5" punct="pt:6">l<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> <w n="5.6">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="5.8">l</w>’<w n="5.9">h<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></w></l>
					<l n="6" num="2.2" lm="6"><space unit="char" quantity="12"></space><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</w></l>
					<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="7.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.7" punct="vg:12">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>t</w>,</l>
					<l n="8" num="2.4" lm="6"><space unit="char" quantity="12"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="8.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.5" punct="pt:6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="9.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="9.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="9.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.9">qu</w>’<w n="9.10"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ls</w> <w n="9.11" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>nt</w>,</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="10.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="10.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="10.8">m<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="10.9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="11" num="3.3" lm="6"><space unit="char" quantity="12"></space><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="11.4" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pt">e</seg>il</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="12.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg">ain</seg>s</w>, <w n="12.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="12.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="12.8"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l</w></l>
					<l n="13" num="4.2" lm="12"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="290" place="6">aon</seg></w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="13.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="13.7">p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.8">l</w>’<w n="13.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.3" lm="6"><space unit="char" quantity="12"></space><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>h<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="14.3" punct="pt:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>