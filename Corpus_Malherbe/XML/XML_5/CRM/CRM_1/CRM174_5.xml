<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM174" rhyme="none" modus="cm" lm_max="12">
				<head type="main">DE HAUTS NUAGES BLANCS…</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ts</w> <w n="1.3">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.6" punct="pt:12">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="pt">en</seg>t</w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">L</w>’<w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">cr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="3.6" punct="vg:10">d<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w>, <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="3.8" punct="pt:12">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1" lm="12"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.4">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="4.6" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="4.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="5" num="2.2" lm="12"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="6" num="2.3" lm="12"><w n="6.1">Br<seg phoneme="y" type="vs" value="1" rule="445" place="1">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3" punct="pt:6">gr<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w>. <w n="6.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="6.8">j<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1" lm="12"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="7.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="7.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.8" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="8" num="3.2" lm="12"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rpr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="8.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p</w></l>
					<l n="9" num="3.3" lm="12"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="9.5" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.9" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>