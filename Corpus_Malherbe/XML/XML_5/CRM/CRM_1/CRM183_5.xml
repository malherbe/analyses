<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM183" modus="cm" lm_max="12">
				<head type="main">LE CARABE DORE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="1.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="1.8" punct="pt:12">bl<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="2.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="2.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="2.10" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>r</w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">t</w>’<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5" punct="vg:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>ps</w> <w n="3.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="3.10" punct="vg:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="4.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="4.9" punct="pt:12">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9">im</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="5.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>t</w>, <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ti<seg phoneme="a" type="vs" value="1" rule="365" place="8">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="5.6" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="6" num="2.2" lm="12"><w n="6.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="6.6">m<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="6.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="7.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.9"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">s</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="8.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="8.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>ts</w> <w n="8.8" punct="pt:12">gl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.3" punct="vg:6">f<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg">au</seg>x</w>, <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="9.6" punct="pt:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="pt">a</seg>il</w>.</l>
					<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ssi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w>-<w n="10.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="10.10">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="11" num="3.3" lm="12"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oî</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="vg">où</seg></w>, <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="11.6">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>j<seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.10">d</w>’<w n="11.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="vg">a</seg>il</w>,</l>
					<l n="12" num="3.4" lm="12"><w n="12.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="12.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="3">oi</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w>-<w n="13.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="13.6" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pi">en</seg>t</w> ?</l>
					<l n="14" num="4.2" lm="12"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">br<seg phoneme="u" type="vs" value="1" rule="427" place="2">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="14.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="14.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="14.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="14.9" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
					<l n="15" num="4.3" lm="12"><w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w>-<w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="15.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="15.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="15.9">d</w>’<w n="15.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="15.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
					<l n="16" num="4.4" lm="12"><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">tr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="16.3" punct="dp:6"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="16.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="16.6" punct="pt:12">d<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>