<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM41" modus="cm" lm_max="12">
				<head type="main">BALSAMINES</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">D<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="1.8" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ls<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="2" num="1.2" lm="12"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="2.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="2.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="2.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="2.10" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1" lm="12"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="3.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.8">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
					<l n="4" num="2.2" lm="12"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="4.5">m<seg phoneme="u" type="vs" value="1" rule="428" place="5">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="4.9" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1" lm="12"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="5.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="5.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="6" num="3.2" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="6.6">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="6.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="6.9">v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="7.4">l</w>’<w n="7.5">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="7">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="7.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="7.9" punct="vg:12">f<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</w>,</l>
					<l n="8" num="4.2" lm="12"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rd</w> <w n="8.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="8.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</w></l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1" lm="12"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="10" num="5.2" lm="12"><w n="10.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="10.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="10.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.9">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>rs</w> <w n="10.10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
				</lg>
			</div></body></text></TEI>