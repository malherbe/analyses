<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM110" modus="cp" lm_max="12">
				<head type="main">HAUTS MANOIRS</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">H<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ts</w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>rs</w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="1.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="1.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="1.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>gs</w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="2.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rds</w> <w n="2.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="2.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="2.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>nt</w></l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.3" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="3.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ</w>’<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg></w> <w n="3.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.8">l</w>’<w n="3.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="6"><space unit="char" quantity="12"></space><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.4" punct="pi:6">Br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pi">an</seg>t</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="5.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="5.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ts</w>-<w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w>-<w n="5.6"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ls</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
					<l n="6" num="2.2" lm="8"><space unit="char" quantity="8"></space><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.6" punct="pi:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="7.7">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.4" lm="6"><space unit="char" quantity="12"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="8.3" punct="pi:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pi">é</seg>s</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">r<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="9.9" punct="vg:12">g<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="8"><space unit="char" quantity="8"></space><w n="10.1">L</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="10.3">r<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.6" punct="pt:8">l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					<l n="11" num="3.3" lm="12"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="11.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.8">r<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="11.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="11.10">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="11.11" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="3.4" lm="6"><space unit="char" quantity="12"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>d</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="12.4">l</w>’<w n="12.5" punct="pi:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pi">er</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12"><w n="13.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="13.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w>-<w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="13.7" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>gs</w> !</l>
					<l n="14" num="4.2" lm="12"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rds</w> <w n="14.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="14.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="14.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>nt</w></l>
					<l n="15" num="4.3" lm="12"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="15.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3" punct="vg:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="15.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ</w>’<w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg></w> <w n="15.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="15.8">l</w>’<w n="15.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="16" num="4.4" lm="6"><space unit="char" quantity="12"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="16.4" punct="pi:6">Br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pi">an</seg>t</w> ?</l>
				</lg>
			</div></body></text></TEI>