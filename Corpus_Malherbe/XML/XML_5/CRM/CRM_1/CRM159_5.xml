<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM159" rhyme="none" modus="cp" lm_max="12">
				<head type="main">SOLEIL ARDENT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="4">em</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="2.8">n<seg phoneme="ø" type="vs" value="1" rule="248" place="9">œu</seg>d</w> <w n="2.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.10" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b</w> <w n="3.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="3.8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="11">e</seg>d</w> <w n="3.9">d</w>’<w n="3.10"><seg phoneme="œ" type="vs" value="1" rule="249" place="12">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.9" punct="pt:12">fr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="pt">en</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pt">e</seg>rt</w>. <w n="5.5">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="5.7">v<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>ts</w></l>
					<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="6.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="7" num="2.3" lm="12"><w n="7.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="7.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">r<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="8.9" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>qui<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>ts</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.6" punct="pt:6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pt">i</seg>t</w>. <w n="9.7">S<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="9.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="9.9" punct="vg:10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rch<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r</w>, <w n="9.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="9.11">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>qs</w></l>
					<l n="10" num="3.2" lm="10"><space unit="char" quantity="4"></space><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.7" punct="pt:10">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pt">o</seg>c</w>.</l>
					<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="11.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</w></l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="12.3">cr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="12.6">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
					<l n="13" num="4.2" lm="10"><space unit="char" quantity="4"></space><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bs<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r</w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="14" num="4.3" lm="12"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rd</w>’<w n="14.4" punct="vg:6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="14.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.7">j<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="14.8" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
				</lg>
			</div></body></text></TEI>