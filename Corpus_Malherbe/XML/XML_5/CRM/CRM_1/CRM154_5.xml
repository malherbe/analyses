<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM154" modus="sm" lm_max="5">
				<head type="main">QUAND J’ÉTAIS ENFANT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="5"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w>-<w n="1.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w></l>
					<l n="2" num="1.2" lm="5"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.4" punct="pe:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pe">an</seg>t</w> !</l>
					<l n="3" num="1.3" lm="5"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4" punct="vg:5">r<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>,</l>
					<l n="4" num="1.4" lm="5"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>ts</w> <w n="4.3">d</w>’<w n="4.4" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pt">an</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="5"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w></l>
					<l n="6" num="2.2" lm="5"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w></l>
					<l n="7" num="2.3" lm="5"><w n="7.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>ts</w></l>
					<l n="8" num="2.4" lm="5"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">j</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="8.5" punct="pt:5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="pt">oi</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="5"><w n="9.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4" punct="dp:5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="dp">oi</seg>s</w> :</l>
					<l n="10" num="3.2" lm="5"><w n="10.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.3" punct="pe:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
					<l n="11" num="3.3" lm="5"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="11.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.4" punct="dp:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></w> :</l>
					<l n="12" num="3.4" lm="5"><w n="12.1">V<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w>-<w n="12.4" punct="pe:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">â</seg>ts</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="5"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="13.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
					<l n="14" num="4.2" lm="5"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>dr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="14.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="e" type="vs" value="1" rule="383" place="4">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w></l>
					<l n="15" num="4.3" lm="5"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w></l>
					<l n="16" num="4.4" lm="5"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="16.4" punct="pt:5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="5"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">m</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="17.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="17.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="17.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w></l>
					<l n="18" num="5.2" lm="5"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="18.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="18.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="18.5" punct="vg:5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="19" num="5.3" lm="5"><w n="19.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="19.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">oû</seg>l</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">b<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w></l>
					<l n="20" num="5.4" lm="5"><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="20.3">l<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="20.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="20.6" punct="pt:5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="pt">in</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="5"><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
					<l n="22" num="6.2" lm="5"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="22.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="22.4">d</w>’<w n="22.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="23" num="6.3" lm="5"><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="23.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="23.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
					<l n="24" num="6.4" lm="5"><w n="24.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>d</w> <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
				</lg>
			</div></body></text></TEI>