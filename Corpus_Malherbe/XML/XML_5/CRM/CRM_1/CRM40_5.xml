<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM40" modus="sp" lm_max="8">
				<head type="main">AUTOMNE EN VILLE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="3.2">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="3.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="3.4">f<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="4" num="1.4" lm="4"><space unit="char" quantity="8"></space><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3" punct="pt:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pt">an</seg>ts</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.7">r<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="6" num="2.2" lm="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.8" punct="vg:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</w>,</l>
					<l n="7" num="2.3" lm="6"><space unit="char" quantity="4"></space><w n="7.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="7.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="8" num="2.4" lm="8"><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="8.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="8.3">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.5">l</w>’<w n="8.6" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">tr<seg phoneme="a" type="vs" value="1" rule="343" place="2">a</seg>mw<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg>s</w> <w n="9.3" punct="vg:4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>t</w>, <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="9.6" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>nt</w>,</l>
					<l n="10" num="3.2" lm="8"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ss<seg phoneme="?" type="va" value="1" rule="162" place="5">en</seg>t</w> <w n="10.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="10.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="10.7" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
					<l n="11" num="3.3" lm="8"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="3.4" lm="8"><w n="12.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="12.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="12.6" punct="pt:8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ck<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w></l>
					<l n="14" num="4.2" lm="8"><w n="14.1" punct="vg:2">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="14.2" punct="vg:4">gl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="14.4" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="15" num="4.3" lm="8"><w n="15.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">s</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="4.4" lm="8"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ts</w> <w n="16.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="16.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="16.6" punct="pt:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="17.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="18" num="5.2" lm="8"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="18.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="18.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="18.5" punct="vg:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>c</w>,</l>
					<l n="19" num="5.3" lm="8"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w></l>
					<l n="20" num="5.4" lm="8"><w n="20.1">S</w>’<w n="20.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="20.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>ts</w>.</l>
				</lg>
			</div></body></text></TEI>