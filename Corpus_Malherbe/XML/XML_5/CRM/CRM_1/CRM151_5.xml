<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM151" modus="cm" lm_max="12">
				<head type="main">LE PETIT SENTIER MONTE…</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.7">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg>s</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">d<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="2.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c</w> <w n="2.8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="2.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.10" punct="pt:12">bl<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:4">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="2">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.3">b<seg phoneme="ø" type="vs" value="1" rule="247" place="6">œu</seg>fs</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="3.6" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="4.8" punct="pt:12">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
					<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="5.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.7" punct="vg:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
					<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="6.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="6.9" punct="pt:12">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
					<l n="8" num="2.2" lm="12"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="8.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.5" punct="vg:6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg">au</seg>t</w>, <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w>-<w n="8.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="8.8"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="8.10">cl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
					<l n="9" num="2.3" lm="12"><w n="9.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="9.8">d</w>’<w n="9.9"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="u" type="vs" value="1" rule="d-2" place="11">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="2.4" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="10.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="10.6">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="10.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="10.9">h<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11">e</seg>s</w> <w n="10.10" punct="vg:12">gu<seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="11" num="2.5" lm="12"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="11.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.4">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.7">li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="12" num="2.6" lm="12"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="12.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="12.7" punct="vg:6">g<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">î</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="12.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>l</w> <w n="12.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="12.11" punct="pt:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>