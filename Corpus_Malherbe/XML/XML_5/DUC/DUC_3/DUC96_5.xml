<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Deuxième Partie</head><head type="sub_part">(1870-1871)</head><div type="poem" key="DUC96" modus="cp" lm_max="12">
					<head type="main">Quinze Août</head>
					<head type="sub">Fête de l’Empereur</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w>-<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="1.4" punct="pi:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pi">e</seg>rt</w> ? <w n="1.5">L<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w>-<w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="1.9" punct="pi:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space><w n="2.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="2.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="2.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.4" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>t</w> !</l>
						<l n="3" num="1.3" lm="12"><w n="3.1" punct="vg:2">N<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">cl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.4">s</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="3.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="3.8" punct="vg:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="4.4" punct="tc:6">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="pv ti in">ai</seg>t</w> ; — « <w n="4.5">Qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6" punct="pe:8"><seg phoneme="u" type="vs" value="1" rule="292" place="8" punct="pe in">Aoû</seg>t</w> ! «</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="5.2">fl<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="5.5" punct="vg:9">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" punct="vg">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.6" punct="pv:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="6" num="1.6" lm="8"><space unit="char" quantity="8"></space><w n="6.1" punct="vg:2">Cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="6.2" punct="pv:4">c<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pv">on</seg>s</w> ; <w n="6.3">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="6.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>spl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="7.8">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w>-<w n="7.9" punct="vg:12">D<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="1.8" lm="8"><space unit="char" quantity="8"></space><w n="8.1" punct="dp:2">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="dp">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> : <foreign lang="LAT"><w n="8.2" punct="pe:8">T<seg phoneme="e" type="vs" value="1" rule="160" place="3">e</seg></w> <w n="8.3">D<seg phoneme="e" type="vs" value="1" rule="393" place="4">e</seg><seg phoneme="ɔ" type="vs" value="1" rule="451" place="5">u</seg>m</w> <w n="8.4">L<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>s</w></foreign> !</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="9.3">c<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.9" punct="vg:12">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="10.3">l</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</w>.</l>
						<l n="11" num="1.11" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>r</w>, <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="11.5">s</w>’<w n="11.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="11.8" punct="pv:12">l<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="12" num="1.12" lm="8"><space unit="char" quantity="8"></space><w n="12.1">R<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ggi<seg phoneme="e" type="vs" value="1" rule="DUC96_1" place="2">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="4">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="12.3">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="12.5" punct="vg:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>, <ref type="noteAnchor" target="(1)">(1)</ref></l>
						<l n="13" num="1.13" lm="12"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="13.3">l</w>’<w n="13.4" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">Em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt ti">eu</seg>r</w>. — <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rd</w>’<w n="13.6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="13.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="13.8" punct="pe:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="14" num="1.14" lm="8"><space unit="char" quantity="8"></space><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="14.4" punct="vg:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.7" punct="pt:8">cr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></w>.</l>
						<l n="15" num="1.15" lm="12"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="15.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="3">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="15.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="15.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="15.8" punct="pv:12">n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="16" num="1.16" lm="8"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="16.4">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="16.6" punct="pe:8">R<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ggi<seg phoneme="e" type="vs" value="1" rule="DUC96_1" place="7">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
						<l n="17" num="1.17" lm="12"><w n="17.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rs<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="17.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="17.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="17.7" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="18" num="1.18" lm="8"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="18.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="18.6" punct="vg:8">p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rds</w>,</l>
						<l n="19" num="1.19" lm="12"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="19.7" punct="vg:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="20" num="1.20" lm="8"><space unit="char" quantity="8"></space><w n="20.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="20.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="20.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="20.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="20.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rds</w>.</l>
						<l n="21" num="1.21" lm="12"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2">c<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="21.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="21.5" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.7">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="21.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="21.9" punct="pv:12">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="11">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
						<l n="22" num="1.22" lm="8"><space unit="char" quantity="8"></space><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3" punct="vg:5">ch<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>,</l>
						<l n="23" num="1.23" lm="12"><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="23.2">d</w>’<w n="23.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="23.4" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.6">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="23.8">d</w>’<w n="23.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="24" num="1.24" lm="8"><space unit="char" quantity="8"></space><w n="24.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="24.5" punct="pt:8">C<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</w>.</l>
						<l n="25" num="1.25" lm="12"><w n="25.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="25.3">cl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="25.5" punct="vg:6">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>ts</w>, <w n="25.6">ch<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="25.7">pr<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="25.9" punct="vg:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="26" num="1.26" lm="8"><space unit="char" quantity="8"></space><w n="26.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="26.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="26.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="26.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
						<l n="27" num="1.27" lm="12"><w n="27.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="27.2" punct="vg:4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>t</w>, <w n="27.3" punct="tc:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="dp ti in">an</seg>t</w> : — « <w n="27.4">L</w>’<w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">Em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="27.6" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>. »</l>
						<l n="28" num="1.28" lm="8"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="28.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="28.4" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pe">eau</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">15 Août 1870</date>.— 10 heures du soir.
						</dateline>
						<note type="footnote" id="1">Ruggieri, artificier des fêtes impériales.</note>
					</closer>
				</div></body></text></TEI>