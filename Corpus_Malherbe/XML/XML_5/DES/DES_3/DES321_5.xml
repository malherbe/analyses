<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES321" modus="cm" lm_max="12">
				<head type="main">LOUISE DE LA VALLIÈRE</head>
				<head type="sub">À GENOUX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w>-<w n="1.6" punct="pe:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</w> ! <w n="1.7">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.8" punct="vg:9">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="1.9">qu</w>’<w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="1.12" punct="pe:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="2" num="1.2" lm="12"><w n="2.1" punct="ps:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="ps">on</seg></w>… <w n="2.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.9" punct="dp:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="3.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="3.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="3.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">gu<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="3.8"><seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="4.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6" punct="vg:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="4.9" punct="vg:9">v<seg phoneme="i" type="vs" value="1" rule="482" place="9" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="4.10"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.12" punct="pv:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>r</w> ;</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="5.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="5.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>ts</w> <w n="5.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg>s</w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.10">l<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="6" num="1.6" lm="12"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="6.3">m</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>r</w> <w n="6.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="6.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.8" punct="pv:12">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
					<l n="7" num="1.7" lm="12"><w n="7.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="7.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="7.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.6">Chr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st</w> <w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.10" punct="vg:12">f<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</w>,</l>
					<l n="8" num="1.8" lm="12"><w n="8.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="8.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="8.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>rds</w>, <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.6" punct="vg:9">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="8.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="8.9" punct="pt:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>s</w>.</l>
					<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2" punct="vg:3">cl<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="9.3">s</w>’<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="9.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="9.6" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">aî</seg>t</w>, <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="9.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="9.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="9.10" punct="pe:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg>s</w> !</l>
					<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="10.2">cl<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>ts</w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="10.9" punct="vg:12">t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="11" num="1.11" lm="12"><w n="11.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="11.2">tr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ts</w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.7">j</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="11.9" punct="vg:9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>t</w>, <w n="11.10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="11.11" punct="pe:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pe">e</seg>rt</w> !</l>
					<l n="12" num="1.12" lm="12"><w n="12.1" punct="vg:2">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="12.6" punct="vg:6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="12.7">j</w>’<w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="12.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="12.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="12.11">l</w>’<w n="12.12" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</w>.</l>
				</lg>
					<p>SON FIANCÉ,<hi rend="ital">qu’elle ne reconnaît pas sous l’habit religieux</hi></p>
				<lg n="2">
					<l n="13" num="2.1" lm="12"><w n="13.1" punct="pe:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="pe">eu</seg></w> ! <w n="13.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="13.5">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="13.8" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="14" num="2.2" lm="12"><w n="14.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="14.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.8" punct="dp:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="15" num="2.3" lm="12"><w n="15.1">L</w>’<w n="15.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.3">qu</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t</w> <w n="15.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="15.11" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pv">en</seg>d</w> ;</l>
					<l n="16" num="2.4" lm="12"><w n="16.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="16.2">c</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="16.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.5">f<seg phoneme="a" type="vs" value="1" rule="193" place="6">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="16.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="16.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="16.9">d</w>’<w n="16.10" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>t</w> !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				</lg>
			</div></body></text></TEI>