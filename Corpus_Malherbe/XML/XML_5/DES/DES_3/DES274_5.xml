<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES274" modus="sm" lm_max="5">
				<head type="main">MARGUERITE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="5"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.5" punct="vg:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">am</seg>ps</w>,</l>
					<l n="2" num="1.2" lm="5"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg></w> <w n="2.3">m</w>’<w n="2.4" punct="pv:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="4">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
					<l n="3" num="1.3" lm="5"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="3.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="4" num="1.4" lm="5"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5" punct="dp:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="dp">an</seg>ts</w> :</l>
					<l n="5" num="1.5" lm="5"><w n="5.1">J</w>’<w n="5.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1" punct="vg">ai</seg></w>, <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5" punct="vg:5">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="5"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="6.4" punct="pv:5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pv">on</seg>t</w> ;</l>
					<l n="7" num="1.7" lm="5"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="7.4">l</w>’<w n="7.5" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="8" num="1.8" lm="5"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2">d<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>il</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.5" punct="pe:5">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pe">om</seg>pt</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="5"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w></l>
					<l n="10" num="2.2" lm="5"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.4">m</w>’<w n="10.5" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="11" num="2.3" lm="5"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="11.5" punct="pv:5">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
					<l n="12" num="2.4" lm="5"><w n="12.1" punct="vg:2">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.3" punct="pe:5">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="pe">in</seg></w> !</l>
					<l n="13" num="2.5" lm="5"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="14" num="2.6" lm="5"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">l</w>’<w n="14.4" punct="dp:5">h<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="dp">e</seg>r</w> :</l>
					<l n="15" num="2.7" lm="5"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="15.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="15.5">l</w>’<w n="15.6" punct="vg:5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="16" num="2.8" lm="5"><w n="16.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>d</w> <w n="16.4" punct="pe:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="pe">e</seg>rt</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="5"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="17.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="17.5">li<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w></l>
					<l n="18" num="3.2" lm="5"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="18.2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rt</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.4" punct="pv:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
					<l n="19" num="3.3" lm="5"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="19.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="19.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="19.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="20" num="3.4" lm="5"><w n="20.1">L</w>’<w n="20.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="20.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.5" punct="pt:5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="pt">eu</seg></w>.</l>
					<l n="21" num="3.5" lm="5"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="22" num="3.6" lm="5"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">ru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="22.4" punct="vg:5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>r</w>,</l>
					<l n="23" num="3.7" lm="5"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="23.3" punct="vg:5">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="24" num="3.8" lm="5"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">l</w>’<w n="24.4" punct="pe:5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pe">ai</seg>r</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1" lm="5"><w n="25.1">J</w>’<w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="25.4" punct="pt:5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="pt">oi</seg>s</w>.</l>
					<l n="26" num="4.2" lm="5"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="26.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ts</w> <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.5" punct="vg:5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="27" num="4.3" lm="5"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="27.4">m<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="28" num="4.4" lm="5"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.4" punct="pv:5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="pv">oi</seg>x</w> ;</l>
					<l n="29" num="4.5" lm="5"><w n="29.1">Qu</w>’<w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w>-<w n="29.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="29.4" punct="pi:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></w> ?</l>
					<l n="30" num="4.6" lm="5"><w n="30.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="30.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="30.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="30.4" punct="dp:5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="dp">eu</seg>r</w> :</l>
					<l n="31" num="4.7" lm="5"><w n="31.1">J</w>’<w n="31.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="31.4">j</w>’<w n="31.5" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="32" num="4.8" lm="5"><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4" punct="pe:5">b<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pe">eu</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>