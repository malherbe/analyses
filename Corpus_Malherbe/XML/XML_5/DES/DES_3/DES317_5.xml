<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES317" modus="cm" lm_max="12">
				<head type="main">À UNE BELLE MARIE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.9" punct="vg:12">M<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12"><w n="2.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp in">i</seg>t</w> : « <w n="2.2">T<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.8" punct="pv:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</w> ;</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8" punct="vg:12">gu<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12"><w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.5">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="4.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.8" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="5.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w>-<w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="5.9">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="6" num="2.2" lm="12"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="6.7">j</w>’<w n="6.8" punct="dp:9"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="dp">i</seg>s</w> : <w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">Ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="6.11" punct="pe:12">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>s</w> !</l>
					<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="7.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.8" punct="vg:12">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="8.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="8.10" punct="pt:12">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="9.5">br<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="9.6" punct="pv:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>rg<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
					<l n="10" num="3.2" lm="12"><w n="10.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ds</w> <w n="10.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="10.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="10.5">ru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="10.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
					<l n="11" num="3.3" lm="12"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gs</w> <w n="11.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="11.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t</w> <w n="11.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="3.4" lm="12"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="12.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="12.6">j</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="12.9" punct="pt:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12"><w n="13.1" punct="dp:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="dp">a</seg></w> : <w n="13.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="13.3">n</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="13.8" punct="vg:12">r<seg phoneme="wa" type="vs" value="1" rule="440" place="11">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="12"><w n="14.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="14.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4" punct="pv:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pv">a</seg>s</w> ;<w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="14.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="14.9" punct="pv:12">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
					<l n="15" num="4.3" lm="12"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="15.5">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">l</w>’<w n="15.8"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="16" num="4.4" lm="12"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="16.5">s</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="16.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="16.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="16.9" punct="pt:12">y<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="12"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">m</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.5" punct="dp:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp">a</seg>s</w> : <w n="17.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="17.8">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>s</w> <w n="17.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="17.10" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="18" num="5.2" lm="12"><w n="18.1" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>p<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.4" punct="pe:6">s<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="pe">œu</seg>r</w> ! <w n="18.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="18.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>ds</w> <w n="18.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>cs</w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="18.9" punct="vg:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>ls</w>,</l>
					<l n="19" num="5.3" lm="12"><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="19.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="19.3">m</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="19.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="19.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="20" num="5.4" lm="12"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">t</w>’<w n="20.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="20.4" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="20.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="20.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="20.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="20.8" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pt">e</seg>ls</w>. »</l>
				</lg>
			</div></body></text></TEI>