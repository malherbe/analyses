<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES311" modus="sp" lm_max="8">
				<head type="main">JEUNE FILLE</head>
				<head type="sub_1">MADEMOISELLE ZOÉ DESSAIX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.7">l</w>’<w n="1.8" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="2" num="1.2" lm="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="2.4" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="3.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="3.5">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
					<l n="4" num="1.4" lm="2"><space quantity="20" unit="char"></space><w n="4.1" punct="vg:2">N<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="5" num="1.5" lm="8"><w n="5.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3">f<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="5.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</w>,</l>
					<l n="6" num="1.6" lm="2"><space quantity="20" unit="char"></space><w n="6.1" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="pe">an</seg>t</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="8"><w n="7.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.2" lm="8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="8.4">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="9" num="2.3" lm="8"><w n="9.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="9.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.6" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="10" num="2.4" lm="2"><space quantity="20" unit="char"></space><w n="10.1" punct="pv:2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pv">e</seg></w> ;</l>
					<l n="11" num="2.5" lm="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="11.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="11.5">s<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="11.7" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</w>,</l>
					<l n="12" num="2.6" lm="2"><space quantity="20" unit="char"></space><w n="12.1" punct="pe:2">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="pe">an</seg>t</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="8"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.5">l</w>’<w n="13.6" punct="pv:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="14" num="3.2" lm="8"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="14.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="14.6">l</w>’<w n="14.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.8" punct="pv:8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="15" num="3.3" lm="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="15.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="15.7" punct="vg:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</w>,</l>
					<l n="16" num="3.4" lm="2"><space quantity="20" unit="char"></space><w n="16.1">T</w>’<w n="16.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></w>,</l>
					<l n="17" num="3.5" lm="8"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="17.4">t</w>’<w n="17.5"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w></l>
					<l n="18" num="3.6" lm="2"><space quantity="20" unit="char"></space><w n="18.1">L</w>’<w n="18.2" punct="pe:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="pe">oi</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>