<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES335" modus="cp" lm_max="12">
				<head type="main">AU REVOIR</head>
				<head type="sub_1">À MA FILLE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gs</w> <w n="1.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="1.5">d</w>’<w n="1.6" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>r</w>, <w n="1.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="1.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="1.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="1.10">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="1.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="1.12">gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="2" num="1.2" lm="2"><space quantity="20" unit="char"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2" punct="vg:2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" punct="vg">en</seg>t</w>,</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="3.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>pt</w> <w n="3.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="3.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="3.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					<l n="4" num="1.4" lm="2"><space quantity="20" unit="char"></space><w n="4.1" punct="vg:2">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" punct="vg">en</seg>t</w>,</l>
					<l n="5" num="1.5" lm="12"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">d</w>’<w n="5.5" punct="vg:6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg></w>, <w n="5.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ds</w> <w n="5.8" punct="vg:9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>s</w>, <w n="5.9"><seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg></w> <w n="5.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="5.11" punct="pe:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					<l n="6" num="1.6" lm="2"><space quantity="20" unit="char"></space><w n="6.1">D</w>’<w n="6.2" punct="dp:2"><seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="dp">oi</seg></w> :</l>
					<l n="7" num="1.7" lm="12"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="7.5">c</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="7.10" punct="dp:12">br<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="8" num="1.8" lm="2"><space quantity="20" unit="char"></space><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3" punct="pe:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="pe">oi</seg></w> !</l>
					<l n="9" num="1.9" lm="12"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="9.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="9.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.9" punct="vg:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="10" num="1.10" lm="2"><space quantity="20" unit="char"></space><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>r</w>,</l>
					<l n="11" num="1.11" lm="12"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="11.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="11.5" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.9" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="12" num="1.12" lm="2"><space quantity="20" unit="char"></space><w n="12.1">T</w>’<w n="12.2" punct="pv:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="2" punct="pv">eoi</seg>r</w> ;</l>
					<l n="13" num="1.13" lm="12"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">t</w>’<w n="13.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w> <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="13.8">l</w>’<w n="13.9" punct="vg:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
					<l n="14" num="1.14" lm="2"><space quantity="20" unit="char"></space><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>,</l>
					<l n="15" num="1.15" lm="12"><w n="15.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="15.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="15.8"><seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="15.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="15.10" punct="dp:12">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
					<l n="16" num="1.16" lm="2"><space quantity="20" unit="char"></space><w n="16.1">C</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="16.3" punct="pe:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="pe">oi</seg></w> !</l>
				</lg>
			</div></body></text></TEI>