<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES284" modus="sm" lm_max="8">
				<head type="main">L’HORLOGE ARRÊTÉE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.4">s</w>’<w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="1.6">l</w>’<w n="1.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8"><w n="2.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="2.7" punct="vg:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</w>,</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.5" punct="vg:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="3.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d</w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="4.8" punct="dp:8">s<seg phoneme="y" type="vs" value="1" rule="445" place="8" punct="dp">û</seg>r</w> :</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.5" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d</w> <w n="6.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>dr<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg></w> <w n="6.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w> <w n="6.7" punct="pv:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg>s</w> ;</l>
					<l n="7" num="2.3" lm="8"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t</w>-<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.4" lm="8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="8.8">j</w>’<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="8.10" punct="pe:8">l<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>