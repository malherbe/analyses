<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES189" modus="sm" lm_max="7">
						<head type="main">LA JEUNE CHATELAINE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="7">« <w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>ds</w>, <w n="1.4" punct="vg:7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="7"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="2.6" punct="pt:7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pt">oi</seg>s</w>. »</l>
							<l n="3" num="1.3" lm="7"><w n="3.1">M</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="3.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="3.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="3.6">d</w>’<w n="3.7" punct="vg:7">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="7"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pt:7">f<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pt">oi</seg>s</w>.</l>
							<l n="5" num="1.5" lm="7"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="5.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="6" num="1.6" lm="7"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="6.5" punct="pv:7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pv">u</seg></w> ;</l>
							<l n="7" num="1.7" lm="7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">j</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.6">l</w>’<w n="7.7" punct="pe:7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
							<l n="8" num="1.8" lm="7"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1" lm="7">« <w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>ds</w>, <w n="9.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5" punct="vg:7">m<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="10" num="2.2" lm="7"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>f</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5" punct="pt:7">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pt">eu</seg>r</w>. »</l>
							<l n="11" num="2.3" lm="7"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="12" num="2.4" lm="7"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="12.6" punct="pt:7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</w>.</l>
							<l n="13" num="2.5" lm="7"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6" punct="pv:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
							<l n="14" num="2.6" lm="7"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="14.4">n</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.6" punct="dp:7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="dp">u</seg></w> :</l>
							<l n="15" num="2.7" lm="7"><w n="15.1" punct="pe:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="pe">eu</seg></w> ! <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="15.5">m</w>’<w n="15.6" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
							<l n="16" num="2.8" lm="7"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="16.3">l</w>’<w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.5" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1" lm="7">« <w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ds</w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="18" num="3.2" lm="7"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.5" punct="pt:7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg>x</w>. »</l>
							<l n="19" num="3.3" lm="7"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="20" num="3.4" lm="7"><w n="20.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2" punct="vg:2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="20.3">m</w>’<w n="20.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="20.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="20.7" punct="pt:7">li<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg>x</w>.</l>
							<l n="21" num="3.5" lm="7"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="21.4" punct="vg:4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="21.6" punct="vg:7">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="22" num="3.6" lm="7"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="22.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="22.3">s</w>’<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="22.6" punct="pt:7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg></w>.</l>
							<l n="23" num="3.7" lm="7"><w n="23.1">D</w>’<w n="23.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="23.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.7" punct="pi:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></w> ?</l>
							<l n="24" num="3.8" lm="7"><w n="24.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="24.3">l</w>’<w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="24.5" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1" lm="7"><w n="25.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="25.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="25.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="26" num="4.2" lm="7"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="26.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.5" punct="pv:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pv">er</seg></w> ;</l>
							<l n="27" num="4.3" lm="7"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="27.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="27.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="28" num="4.4" lm="7"><w n="28.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="28.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="28.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="28.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="28.6" punct="pt:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></w>.</l>
							<l n="29" num="4.5" lm="7"><w n="29.1">Qu</w>’<w n="29.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="29.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="29.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="29.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.7" punct="pe:7">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
							<l n="30" num="4.6" lm="7"><w n="30.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="30.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="30.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="30.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>ps</w> <w n="30.5" punct="pe:7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pe">u</seg></w> !</l>
							<l n="31" num="4.7" lm="7"><w n="31.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="31.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="31.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="31.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="31.5" punct="pi:7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></w> ?</l>
							<l n="32" num="4.8" lm="7"><w n="32.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="32.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="32.3">l</w>’<w n="32.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="32.5" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg></w>.</l>
						</lg>
					</div></body></text></TEI>