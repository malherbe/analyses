<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES24" modus="cp" lm_max="12">
						<head type="main">L’IMPRUDENCE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="10"><space quantity="2" unit="char"></space><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3" punct="vg:4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="1.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>t</w>, <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.7">br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
							<l n="3" num="1.3" lm="10"><space quantity="2" unit="char"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7" punct="dp:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="dp">eu</seg>r</w> :</l>
							<l n="4" num="1.4" lm="8"><space quantity="6" unit="char"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.4">m</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="4.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="5" num="1.5" lm="10"><space quantity="2" unit="char"></space><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="5.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.8" punct="pe:10">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>rs</w> !</l>
							<l n="6" num="1.6" lm="12"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="6.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w>-<w n="6.8">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="9">e</seg></w> <w n="6.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="6.10" punct="pi:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
							<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">m</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">aî</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="7.8">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg>s</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.10" punct="pv:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>rs</w> ;</l>
							<l n="8" num="1.8" lm="8"><space quantity="6" unit="char"></space><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7" punct="pt:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
							<l n="9" num="1.9" lm="12"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="9.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="9.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="9.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.8">l</w>’<w n="9.9" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>r</w> ;</l>
							<l n="10" num="1.10" lm="12"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="10.5">l</w>’<w n="10.6" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="11" num="1.11" lm="12"><w n="11.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="11.2">s</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="11.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="11.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="12" num="1.12" lm="12"><w n="12.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ge<seg phoneme="a" type="vs" value="1" rule="316" place="6" punct="vg">a</seg>s</w>, <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w>-<w n="12.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="12.7">m</w>’<w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="12.9" punct="pi:12">p<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>r</w> ?</l>
							<l n="13" num="1.13" lm="12"><w n="13.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="13.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="13.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>x</w> <w n="13.9" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="14" num="1.14" lm="12"><w n="14.1" punct="pe:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg></w> ! <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="14.6">j</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="14.8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="14.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="14.10" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
							<l n="15" num="1.15" lm="12"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="15.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="15.4">t</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="15.6" punct="vg:6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="15.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="15.8">m</w>’<w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="15.10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="15.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="15.12" punct="vg:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>r</w>,</l>
							<l n="16" num="1.16" lm="12"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="16.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="16.7">br<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="16.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="16.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="16.10" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="17" num="1.17" lm="10"><space quantity="2" unit="char"></space><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ds</w> <w n="17.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="17.6">bl<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="17.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r</w> <w n="17.8" punct="pv:10">t<seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="pv">oi</seg></w> ;</l>
							<l n="18" num="1.18" lm="8"><space quantity="6" unit="char"></space><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="18.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="18.4" punct="dp:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
							<l n="19" num="1.19" lm="8"><space quantity="6" unit="char"></space><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="19.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="19.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.5" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="20" num="1.20" lm="12"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w>-<w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="20.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="20.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="20.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="20.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="20.10" punct="pi:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pi">oi</seg></w> ?</l>
						</lg>
					</div></body></text></TEI>