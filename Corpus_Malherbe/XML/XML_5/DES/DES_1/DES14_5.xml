<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES14" modus="cp" lm_max="12">
						<head type="main">LA FONTAINE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="1.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">n</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.9">d</w>’<w n="1.10"><seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="1.11" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="2.5">m</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.9" punct="pv:12">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>r</w> ;</l>
							<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="3.6" punct="vg:9">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="3.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="3.9" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="6"><space quantity="10" unit="char"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="4.5">m</w>’<w n="4.6" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="6" punct="pt">eoi</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w>-<w n="5.6">t</w>-<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.7">r<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> <w n="6.9" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="7.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
							<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">l</w>’<w n="8.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="9">om</seg></w> <w n="8.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.9">j</w>’<w n="8.10" punct="pe:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
							<l n="9" num="2.5" lm="12"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="9.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.7" punct="vg:12">z<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="12" punct="vg">y</seg>rs</w>,</l>
							<l n="10" num="2.6" lm="12"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="10.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="10.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.7">s</w>’<w n="10.8"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="10.9">v<seg phoneme="wa" type="vs" value="1" rule="440" place="9">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="10.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="11" num="2.7" lm="12"><w n="11.1">Qu</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="11.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="11.5">s</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="11.9"><seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="11.10">qu</w>’<w n="11.11"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="11.12" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="12" num="2.8" lm="12"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="12.7">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.10" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>rs</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1" lm="12"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="13.7">j</w>’<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="13.9" punct="pt:9">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="pt">ou</seg>t</w>. <w n="13.10">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.11"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="13.12" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="14" num="3.2" lm="12"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="14.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="14.6">l</w>’<w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="14.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="14.10" punct="dp:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="dp">œu</seg>r</w> :</l>
							<l n="15" num="3.3" lm="12"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="15.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="15.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">j</w>’<w n="15.8"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="15.9">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="15.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="16" num="3.4" lm="12"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="16.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="16.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="16.8" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
							<l n="17" num="3.5" lm="8"><space quantity="6" unit="char"></space><w n="17.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.3" punct="vg:8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="18" num="3.6" lm="12"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="18.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="18.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="18.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d</w> <w n="18.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</w>,</l>
							<l n="19" num="3.7" lm="8"><space quantity="6" unit="char"></space><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="19.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="19.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.5" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="20" num="3.8" lm="8"><space quantity="6" unit="char"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="20.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="20.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="21" num="4.1" lm="12"><w n="21.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="21.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="21.5">l</w>’<w n="21.6" punct="dp:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="dp">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> : <w n="21.7" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="pe">o</seg>h</w> ! <w n="21.8">c</w>’<w n="21.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="21.10">qu</w>’<w n="21.11"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.12"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="21.13" punct="pt:12">fl<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>tt<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="22" num="4.2" lm="12"><w n="22.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="22.4"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="22.5">gl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="22.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="22.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="22.8" punct="pt:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
							<l n="23" num="4.3" lm="12"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2" punct="ps:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="ps">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="23.5" punct="ps:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="ps">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>… <w n="23.6" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</w> ! <w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="23.9" punct="pv:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="24" num="4.4" lm="12"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="24.2" punct="vg:2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="24.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="24.7">s</w>’<w n="24.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="24.9" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1" lm="12"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="25.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">n</w>’<w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="25.9">d</w>’<w n="25.10"><seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="25.11" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="26" num="5.2" lm="12"><w n="26.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="26.5">m</w>’<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="26.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>rs</w> <w n="26.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="26.9" punct="vg:12">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</w>,</l>
							<l n="27" num="5.3" lm="12"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="27.3">l</w>’<w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="27.6" punct="vg:9">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg">e</seg></w>, <w n="27.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="27.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="27.9" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="28" num="5.4" lm="6"><space quantity="10" unit="char"></space><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="28.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="28.5">m</w>’<w n="28.6" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="6" punct="pt">eoi</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>