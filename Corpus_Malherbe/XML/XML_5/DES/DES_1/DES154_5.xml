<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES154" modus="cm" lm_max="12">
						<head type="main">L’ABSENCE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="1.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="1.7">p<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>ds</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.10" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="2.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="2.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="2.9" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.7" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="4.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="4.11" punct="pe:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pe">œu</seg>r</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.4" punct="vg:6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="5.6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="5.9" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="6.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ts</w> <w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="6.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="6.10" punct="pv:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">J</w>’<w n="7.2" punct="pe:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ps">e</seg></w> !… <w n="7.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="7.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>x</w>, <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="7.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="7.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="7.10" punct="vg:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</w>,</l>
							<l n="8" num="2.4" lm="12"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="8.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ds</w> <w n="8.5">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="8.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3" punct="pv:3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pv">ai</seg>s</w> ; <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="9.7">qu</w>’<w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="9.10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>ts</w> <w n="9.11" punct="vg:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>ls</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>s</w> <w n="10.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="10.6">m</w>’<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="10.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="10.9" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="11" num="3.3" lm="12"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="11.4" punct="dp:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="dp in">a</seg>s</w> : « <w n="11.5">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6" punct="pe:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pe">oi</seg>r</w> ! » <w n="11.7">T<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="11.9">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="11.10" punct="dp:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="dp in">a</seg>s</w> : « <w n="11.11" punct="pe:12">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pe">ain</seg></w> ! »</l>
							<l n="12" num="3.4" lm="12"><w n="12.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="12.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in">i</seg>s</w> : « <w n="12.5" punct="pe:6">T<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe">ou</seg>rs</w> ! » <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.7">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="12.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.10" punct="pt:12">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></w>.</l>
						</lg>
					</div></body></text></TEI>