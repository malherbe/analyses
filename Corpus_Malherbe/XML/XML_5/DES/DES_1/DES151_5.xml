<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES151" modus="cp" lm_max="12">
						<head type="main">LES CLOCHES DU SOIR</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="1.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="1.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="1.9" punct="vg:12">v<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.3">l</w>’<w n="2.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="2.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="2.9" punct="pv:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.3">n</w>’<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="3.5">d</w>’<w n="3.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="3.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="3.10">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="3.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="3.12" punct="pv:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pv">oi</seg></w> ;</l>
							<l n="4" num="1.4" lm="6"><space quantity="10" unit="char"></space><w n="4.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.3" punct="pe:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="4.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.6" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe">oi</seg></w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="5.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="5.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>x</w> <w n="5.9" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="6.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="6.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.7" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="7.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>br<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7">m<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.10" punct="dp:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="dp">oi</seg></w> :</l>
							<l n="8" num="2.4" lm="6"><space quantity="10" unit="char"></space><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="8.2" punct="pe:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="8.4" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe">oi</seg></w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="9.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="9.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="10.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="10.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="10.9" punct="pt:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
							<l n="11" num="3.3" lm="12"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="11.7">n</w>’<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.9">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="11.10">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="11.11" punct="vg:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
							<l n="12" num="3.4" lm="6"><space quantity="10" unit="char"></space><w n="12.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3" punct="pe:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="12.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe">oi</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1" lm="12"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="13.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="13.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="13.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="13.9">l</w>’<w n="13.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="14" num="4.2" lm="12"><w n="14.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="14.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.8" punct="pv:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="15" num="4.3" lm="12"><w n="15.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="15.2">c</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="15.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="15.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.9">s<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="15.10">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r</w> <w n="15.11" punct="vg:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
							<l n="16" num="4.4" lm="6"><space quantity="10" unit="char"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="16.3" punct="pe:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="16.6" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe">oi</seg></w> !</l>
						</lg>
					</div></body></text></TEI>