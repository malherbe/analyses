<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES96" modus="sm" lm_max="6">
						<head type="main">GARAT À BORDEAUX</head>
						<lg n="1">
							<l n="1" num="1.1" lm="6">« <w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w>-<w n="2.3" punct="vg:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="2.4" punct="pi:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pi">ou</seg>r</w> ? »</l>
							<l n="3" num="1.3" lm="6">« — <w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="4" num="1.4" lm="6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="4.4" punct="pt:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="6"><w n="5.1" punct="vg:2">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>t</w>, <w n="5.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.2" lm="6"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.5" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pv">an</seg>s</w> ;</l>
							<l n="7" num="2.3" lm="6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="8" num="2.4" lm="6"><w n="8.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.3" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pt">en</seg>ts</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="6"><w n="9.1">J<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="10" num="3.2" lm="6"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="10.4">m</w>’<w n="10.5" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pv">a</seg></w> ;</l>
							<l n="11" num="3.3" lm="6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="11.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="12" num="3.4" lm="6"><w n="12.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.3" punct="pt:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1" lm="6"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="13.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="14" num="4.2" lm="6"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="14.3" punct="pv:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pv">é</seg></w> ;</l>
							<l n="15" num="4.3" lm="6"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="15.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="15.4" punct="pv:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
							<l n="16" num="4.4" lm="6"><w n="16.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.3" punct="pt:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1" lm="6"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="3">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="18" num="5.2" lm="6"><w n="18.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="18.3" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>rs</w>,</l>
							<l n="19" num="5.3" lm="6"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="19.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="20" num="5.4" lm="6"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="20.2">p<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="20.4" punct="pt:6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>rs</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1" lm="6"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="21.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="21.4" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="22" num="6.2" lm="6"><w n="22.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="2">ue</seg>il</w> <w n="22.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="22.3" punct="vg:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>,</l>
							<l n="23" num="6.3" lm="6"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="23.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="23.4" punct="vg:6">B<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="24" num="6.4" lm="6"><w n="24.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="24.2">Qu<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="24.4">l</w>’<w n="24.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1" lm="6"><w n="25.1">N</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="25.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="25.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="26" num="7.2" lm="6"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="26.5">l</w>’<w n="26.6" punct="pv:6">h<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pv">eu</seg>r</w> ;</l>
							<l n="27" num="7.3" lm="6"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">l</w>’<w n="27.3" punct="vg:2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="vg">o</seg>r</w>, <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="27.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="27.6" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="28" num="7.4" lm="6"><w n="28.1">N</w>’<w n="28.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">aî</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="28.5" punct="pt:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="pt">œu</seg>r</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1" lm="6"><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>st<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.2">n<seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="6">ï</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="30" num="8.2" lm="6"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="30.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="30.3" punct="pv:6">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pv">on</seg>s</w> ;</l>
							<l n="31" num="8.3" lm="6"><w n="31.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="31.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="31.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="31.5" punct="vg:6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="32" num="8.4" lm="6"><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="32.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="32.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="32.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="32.5" punct="pt:6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pt">on</seg>s</w>.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1" lm="6"><w n="33.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="33.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="34" num="9.2" lm="6"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.2">d</w>’<w n="34.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>,</l>
							<l n="35" num="9.3" lm="6"><w n="35.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t</w> <w n="35.2">d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="35.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="36" num="9.4" lm="6"><w n="36.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="36.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="36.3" punct="pt:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1" lm="6"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="37.2">li<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="37.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="37.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="37.5" punct="vg:6">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="38" num="10.2" lm="6"><w n="38.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="38.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="38.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w>-<w n="38.4" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>,</l>
							<l n="39" num="10.3" lm="6"><w n="39.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="39.2">m</w>’<w n="39.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="39.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="39.5" punct="vg:6">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="40" num="10.4" lm="6"><w n="40.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="40.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="40.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="40.4" punct="pt:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pt">oi</seg></w>.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1" lm="6">« — <w n="41.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="41.2" punct="vg:3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>c</w>, <w n="41.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="41.4" punct="vg:6">m<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
							<l n="42" num="11.2" lm="6"><w n="42.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="42.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="42.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="42.4" punct="pv:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv">ou</seg>r</w> ;</l>
							<l n="43" num="11.3" lm="6"><w n="43.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="43.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="43.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="43.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="44" num="11.4" lm="6"><w n="44.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="44.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="44.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="44.4" punct="pt:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</w>.</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1" lm="6"><w n="45.1" punct="pe:2">L<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe">e</seg></w> ! <w n="45.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="45.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="45.4" punct="pe:6">l<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
							<l n="46" num="12.2" lm="6"><w n="46.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="46.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="46.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="46.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w></l>
							<l n="47" num="12.3" lm="6"><w n="47.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="47.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="47.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="47.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="47.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
							<l n="48" num="12.4" lm="6"><w n="48.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="48.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="48.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="48.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="48.5" punct="pt:6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>r</w>. »</l>
						</lg>
					</div></body></text></TEI>