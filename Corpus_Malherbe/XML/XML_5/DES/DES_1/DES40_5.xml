<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES40" modus="cp" lm_max="12">
						<head type="main">LES REGRETS</head>
						<lg n="1">
							<l n="1" num="1.1" lm="10"><space quantity="2" unit="char"></space><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="1.4" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pe">u</seg></w> ! <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r</w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="1.9" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rt</w>,</l>
							<l n="2" num="1.2" lm="10"><space quantity="2" unit="char"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="2.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="2.4" punct="pe:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="pe">em</seg>ps</w> ! <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r</w> <w n="2.8">l</w>’<w n="2.9" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="10"><space quantity="2" unit="char"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.4" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="3.5" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</w> ! <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="3.7">l</w>’<w n="3.8" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
							<l n="4" num="1.4" lm="12"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l</w> <w n="4.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="4.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.8">m</w>’<w n="4.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="4.10">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="4.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="4.12" punct="pt:12">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="10"><space quantity="2" unit="char"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="5.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="5.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="7">ue</seg>il</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="5.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.2" lm="12"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="6.6">qu</w>’<w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="6.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="6.10" punct="pv:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pv">e</seg>il</w> ;</l>
							<l n="7" num="2.3" lm="10"><space quantity="2" unit="char"></space><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="7.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="7.5">j</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="7.7">v<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="7.8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="7.10" punct="vg:10">fl<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="8" num="2.4" lm="12"><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.6">n</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.8">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>t</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.10" punct="pt:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pt">e</seg>il</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="9.4" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.9" punct="pv:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="10.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="10.11" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>ts</w>.</l>
							<l n="11" num="3.3" lm="12"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="11.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="12" num="3.4" lm="12"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="12.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="12.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="12.8" punct="pe:12">j<seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pe">ai</seg>s</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1" lm="10"><space quantity="2" unit="char"></space><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">t</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="13.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="13.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="13.6">tr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="13.7">d</w>’<w n="13.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="13.9" punct="vg:10">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="14" num="4.2" lm="10"><space quantity="2" unit="char"></space><w n="14.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="14.6" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>rs</w> ;</l>
							<l n="15" num="4.3" lm="12"><w n="15.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="15.3" punct="vg:3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="15.5">s</w>’<w n="15.6"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="15.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="15.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="15.11" punct="vg:12">l<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="16" num="4.4" lm="12"><w n="16.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="16.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.4">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg>s</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="16.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="16.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="16.8" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1" lm="10"><space quantity="2" unit="char"></space><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="17.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="17.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="17.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="17.7" punct="pv:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
							<l n="18" num="5.2" lm="10"><space quantity="2" unit="char"></space><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="18.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="18.6" punct="pv:10">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pv">eau</seg></w> ;</l>
							<l n="19" num="5.3" lm="12"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="19.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="19.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="19.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="19.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="19.8" punct="ps:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
							<l n="20" num="5.4" lm="8"><space quantity="6" unit="char"></space><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">m</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383" place="3">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="20.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="20.6" punct="pt:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1" lm="10"><space quantity="2" unit="char"></space><w n="21.1">M<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="21.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="21.6" punct="vg:10"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>bs<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
							<l n="22" num="6.2" lm="10"><space quantity="2" unit="char"></space><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="22.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5">n</w>’<w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="22.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="22.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="22.9" punct="pt:10">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>x</w>.</l>
							<l n="23" num="6.3" lm="10"><space quantity="2" unit="char"></space><w n="23.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w>-<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="23.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="23.7" punct="vg:7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="23.9" punct="pe:10"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe">e</seg></w> !</l>
							<l n="24" num="6.4" lm="12"><w n="24.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="24.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg></w> ! <w n="24.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="24.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="24.6">c</w>’<w n="24.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="24.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="24.9">d</w>’<w n="24.10"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="24.11" punct="pe:12">f<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>s</w> !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1" lm="12"><w n="25.1">C</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="25.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="25.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="25.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="25.6" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>rs</w>, <w n="25.7">qu</w>’<w n="25.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="25.9">m</w>’<w n="25.10" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="vg">en</seg>d</w>, <w n="25.11">qu</w>’<w n="25.12"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="25.13">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
							<l n="26" num="7.2" lm="12"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="26.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="26.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="26.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="26.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="26.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="26.10" punct="pt:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></w>.</l>
							<l n="27" num="7.3" lm="12"><w n="27.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="27.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w>-<w n="27.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="27.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="27.5">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="27.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="27.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="27.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.9" punct="pi:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>xp<seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi">e</seg></w> ?</l>
							<l n="28" num="7.4" lm="12"><w n="28.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="28.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="28.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="28.4" punct="vg:4">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>s</w>, <w n="28.5" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>t</w>, <w n="28.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="28.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="28.8">b<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="28.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="28.10" punct="pt:12">fu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></w>.</l>
						</lg>
					</div></body></text></TEI>