<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES183" modus="cm" lm_max="12">
						<head type="main">LES DEUX PEUPLIERS</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.4" punct="vg:6">z<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="6" punct="vg">y</seg>rs</w>, <w n="1.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="1.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="2.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.4" punct="vg:6"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="2.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="2.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="2.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="2.8" punct="pt:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
							<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="3.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="3.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
							<l n="4" num="1.4" lm="12"><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="4.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="4.7" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
							<l n="5" num="1.5" lm="12"><w n="5.1">L</w>’<w n="5.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="5.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gs</w> <w n="5.5" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>ts</w>, <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">fr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="5.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="5.9" punct="pv:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="6.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="6.8" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
							<l n="7" num="1.7" lm="12"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="7.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="7.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="7.8" punct="vg:12">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="12">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="8" num="1.8" lm="12"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="8.5">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="8.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="8.9">v<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="8.10" punct="pt:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>rs</w>.</l>
							<l n="9" num="1.9" lm="12"><w n="9.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="9.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="9.3">fr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="9.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="9.8" punct="pv:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="10" num="1.10" lm="12"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.4" punct="vg:6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</w> <w n="10.7">n<seg phoneme="ø" type="vs" value="1" rule="248" place="9">œu</seg>ds</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</w></l>
							<l n="11" num="1.11" lm="12"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="11.6">l</w>’<w n="11.7" punct="pv:6"><seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="pv">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="11.11">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.12" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="12" num="1.12" lm="12"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2" punct="vg:2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="12.7" punct="vg:6">fr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="12.9">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="12.10" punct="pe:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">ez</seg></w> !</l>
						</lg>
						<lg n="2">
							<l n="13" num="2.1" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="13.3">j</w>’<w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="13.5" punct="ps:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="ps">u</seg></w>… <w n="13.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="13.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="14" num="2.2" lm="12"><w n="14.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="14.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.7">v<seg phoneme="ø" type="vs" value="1" rule="248" place="8">œu</seg>x</w> <w n="14.8">qu</w>’<w n="14.9"><seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="14.10">n</w>’<w n="14.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d</w> <w n="14.12" punct="dp:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="dp">a</seg>s</w> :</l>
							<l n="15" num="2.3" lm="12"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="15.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="15.6">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.8" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
							<l n="16" num="2.4" lm="12"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="16.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="16.7">n</w>’<w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="16.9">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>t</w> <w n="16.10">n<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>s</w> <w n="16.11" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>