<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES181" modus="sm" lm_max="5">
						<head type="main">L’EXIL</head>
						<lg n="1">
							<l n="1" num="1.1" lm="5"><w n="1.1" punct="vg:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="vg">en</seg>s</w>, <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>x</w>,</l>
							<l n="2" num="1.2" lm="5"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3" punct="pv:5">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
							<l n="3" num="1.3" lm="5"><w n="3.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="3.5" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="5"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="4.5" punct="pt:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg>x</w>.</l>
							<l n="5" num="1.5" lm="5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="5.2" punct="vg:2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">em</seg>ps</w>, <w n="5.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="5.5" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
							<l n="6" num="1.6" lm="5"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="6.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="6.3" punct="dp:5">m<seg phoneme="o" type="vs" value="1" rule="318" place="5" punct="dp">au</seg>x</w> :</l>
							<l n="7" num="1.7" lm="5"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="8" num="1.8" lm="5"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="8.4" punct="pe:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="5" punct="pe">o</seg>s</w> !</l>
							<l n="9" num="1.9" lm="5"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="10" num="1.10" lm="5"><w n="10.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="10.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w>-<w n="10.4" punct="pv:5">n<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pv">é</seg></w> ;</l>
							<l n="11" num="1.11" lm="5"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="11.2" punct="vg:3">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="11.3">j</w>’<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="12" num="1.12" lm="5"><w n="12.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.4" punct="pt:5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pt">é</seg></w>.</l>
							<l n="13" num="1.13" lm="5"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">l</w>’<w n="13.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="13.6">pr<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="14" num="1.14" lm="5"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="14.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.4" punct="vg:5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg">o</seg>rt</w>,</l>
							<l n="15" num="1.15" lm="5"><w n="15.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="15.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="16" num="1.16" lm="5"><w n="16.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="16.3" punct="pe:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pe">o</seg>r</w> !</l>
							<l n="17" num="1.17" lm="5"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="17.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.4" punct="vg:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</w>,</l>
							<l n="18" num="1.18" lm="5"><w n="18.1">Pr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>x</w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="18.4" punct="vg:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg">eu</seg>rs</w>,</l>
							<l n="19" num="1.19" lm="5"><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="2">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="19.3">f<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
							<l n="20" num="1.20" lm="5"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.4" punct="pt:5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pt">eu</seg>rs</w>.</l>
							<l n="21" num="1.21" lm="5"><w n="21.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="21.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
							<l n="22" num="1.22" lm="5"><w n="22.1">L</w>’<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="22.5" punct="vg:5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>,</l>
							<l n="23" num="1.23" lm="5"><w n="23.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w>-<w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="23.4" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></w> :</l>
							<l n="24" num="1.24" lm="5">« <w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="24.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="24.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="24.4" punct="pv:5">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="pv">eu</seg>x</w> ;</l>
							<l n="25" num="1.25" lm="5"><w n="25.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="25.2" punct="vg:3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="25.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="25.4" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
							<l n="26" num="1.26" lm="5"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="26.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="26.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w></l>
							<l n="27" num="1.27" lm="5"><w n="27.1">D</w>’<w n="27.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="28" num="1.28" lm="5"><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="28.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="28.4" punct="pt:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">er</seg></w>.</l>
							<l n="29" num="1.29" lm="5"><w n="29.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="29.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="30" num="1.30" lm="5"><w n="30.1">C<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="30.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>r</w>,</l>
							<l n="31" num="1.31" lm="5"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="31.2">n<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="31.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
							<l n="32" num="1.32" lm="5"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="32.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="32.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>r</w>,</l>
							<l n="33" num="1.33" lm="5"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="33.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="33.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="33.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
							<l n="34" num="1.34" lm="5"><w n="34.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="34.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="34.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="34.4" punct="vg:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rs</w>,</l>
							<l n="35" num="1.35" lm="5"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="35.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="35.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
							<l n="36" num="1.36" lm="5"><w n="36.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="36.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="36.3" punct="pe:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pe">ou</seg>rs</w> !</l>
							<l n="37" num="1.37" lm="5"><w n="37.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="37.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="37.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
							<l n="38" num="1.38" lm="5"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="38.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="38.3" punct="vg:5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="vg">en</seg>t</w>,</l>
							<l n="39" num="1.39" lm="5"><w n="39.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="39.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="39.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="39.4">d</w>’<w n="39.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</w></l>
							<l n="40" num="1.40" lm="5"><w n="40.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="40.2" punct="ps:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="ps">en</seg>t</w>…</l>
							<l n="41" num="1.41" lm="5"><w n="41.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="41.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="41.3" punct="pv:5">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
							<l n="42" num="1.42" lm="5"><w n="42.1" punct="vg:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="vg">en</seg>s</w>, <w n="42.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="42.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.4" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg>x</w>.</l>
							<l n="43" num="1.43" lm="5"><w n="43.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="43.2">j</w>’<w n="43.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="43.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="43.5" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
							<l n="44" num="1.44" lm="5"><w n="44.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="44.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="44.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="44.5" punct="pt:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg>x</w>.</l>
						</lg>
					</div></body></text></TEI>