<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES95" modus="cp" lm_max="12">
						<head type="main">L’EXILÉ</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8"><space quantity="6" unit="char"></space><w n="1.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.7" punct="vg:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2" punct="vg:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.4" punct="vg:6">ru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg>x</w>, <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="2.9" punct="dp:12">f<seg phoneme="œ" type="vs" value="1" rule="406" place="11">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg>s</w> :</l>
							<l n="3" num="1.3" lm="12"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="3.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="3.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rts</w> <w n="3.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
							<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.3" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="4.8" punct="pt:12">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="8"><space quantity="6" unit="char"></space>« <w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="5.7">z<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>rs</w></l>
							<l n="6" num="2.2" lm="12"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.5">n<seg phoneme="a" type="vs" value="1" rule="343" place="8">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="9">ï</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="6.6" punct="pv:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg>s</w> ;</l>
							<l n="7" num="2.3" lm="12"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.4">z<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>r</w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="7.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="7.9">l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
							<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="8.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="8.9" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>rs</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="8"><space quantity="6" unit="char"></space>« <w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" punct="vg">en</seg>s</w>, <w n="9.5">c</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.9" punct="vg:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</w>,</l>
							<l n="10" num="3.2" lm="12"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7" punct="vg:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>, <w n="10.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="10.9" punct="vg:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="11" num="3.3" lm="12"><w n="11.1" punct="vg:2">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.3" punct="vg:6">g<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="11.8" punct="dp:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
							<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="12.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.6" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="12.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.9">n</w>’<w n="12.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="12.11">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="12.12" punct="pt:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>. »</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1" lm="8"><space quantity="6" unit="char"></space><w n="13.1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="13.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="13.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.5" punct="pv:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pv">o</seg>rt</w> ;</l>
							<l n="14" num="4.2" lm="12"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg></w> <w n="14.6">m</w>’<w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.8">r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="14.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="14.10" punct="pt:12">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
							<l n="15" num="4.3" lm="12"><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="15.4" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>f</w>, <w n="15.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="15.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="15.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="15.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="15.9" punct="pv:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="16" num="4.4" lm="12"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="16.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="16.4" punct="vg:6">bl<seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="16.5">j</w>’<w n="16.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="16.7">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>s</w> <w n="16.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="16.9">cr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="16.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="16.11" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1" lm="8"><space quantity="6" unit="char"></space><w n="17.1">G<seg phoneme="u" type="vs" value="1" rule="425" place="1">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="17.4" punct="pe:8">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</w> !</l>
							<l n="18" num="5.2" lm="12"><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="18.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l</w> <w n="18.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="18.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="18.9" punct="pv:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="19" num="5.3" lm="12"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="19.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="19.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="19.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="19.8" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
							<l n="20" num="5.4" lm="12"><w n="20.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5" punct="ps:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="ps">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="20.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="20.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="20.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>x</w> <w n="20.10" punct="pt:12">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
						</lg>
					</div></body></text></TEI>