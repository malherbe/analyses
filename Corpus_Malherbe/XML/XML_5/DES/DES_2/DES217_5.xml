<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES217" modus="cm" lm_max="12">
					<head type="main">LA JALOUSE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Pour la dernière fois je veux tromper mon cœur, <lb></lb>
									L’enivrer d’espérance, hélas ! et de mensonges !
								</quote>
								 <bibl><hi rend="smallcap">Charles Nodier.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4" punct="vg:6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.6" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg></w> <w n="1.8">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l</w> <w n="1.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="1.10">j</w>’<w n="1.11"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.4" punct="dp:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="dp in">e</seg>t</w> : « <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6" punct="dp:9"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="dp">en</seg>d</w> : <w n="2.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w>-<w n="2.8" punct="pe:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg></w> !</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.3">t</w>’<w n="3.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6" punct="dp:9">r<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="dp in">on</seg>ds</w> : « <w n="3.7">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.8" punct="pe:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pe">à</seg></w> ! »</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>t</w>, <w n="4.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="4.7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="4.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg></w>-<w n="4.9" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">cr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.8">b<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="5.9">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></w> <w n="5.10">d</w>’<w n="5.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
						<l n="6" num="1.6" lm="12"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="6.5" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in">i</seg>t</w> : « <w n="6.6">C</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="6.8" punct="pe:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pe">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! » <w n="6.9"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="6.10">n</w>’<w n="6.11"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.12">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="6.13" punct="dp:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="dp in">i</seg>t</w> : « <w n="6.14">C</w>’<w n="6.15"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="6.16" punct="pe:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> ! »</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="12"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:4">n<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="7.4">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="7.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="7.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="2.2" lm="12"><w n="8.1">J</w>’<w n="8.2" punct="dp:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp in">i</seg>s</w> : « <w n="8.3">J</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="8.5" punct="pt:6">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></w>. <w n="8.6">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7" punct="ps:9">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="ps">e</seg></w>… <w n="8.8">C</w>’<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="8.10">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r</w> <w n="8.11" punct="pe:12">v<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>s</w> !</l>
						<l n="9" num="2.3" lm="12"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="9.9" punct="pe:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>s</w> ! »</l>
						<l n="10" num="2.4" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="10.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="10.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="10.10" punct="vg:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="11" num="2.5" lm="12"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.7">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="11.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="11.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="11.10" punct="vg:12">f<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
						<l n="12" num="2.6" lm="12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="12.5" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in">i</seg>t</w> : « <w n="12.6">C</w>’<w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.8" punct="pe:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pe">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! » <w n="12.9"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="12.10">n</w>’<w n="12.11"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.12">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="12.13" punct="dp:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="dp in">i</seg>t</w> : « <w n="12.14">C</w>’<w n="12.15"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="12.16" punct="pe:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> ! »</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="12"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="dp in">e</seg></w> : « <w n="13.3">C</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="13.5" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe">oi</seg></w> ! » <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">m</w>’<w n="13.8" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="9" punct="vg">i</seg>s</w>, <w n="13.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.10" punct="pv:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="14" num="3.2" lm="12"><w n="14.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="14.9" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
						<l n="15" num="3.3" lm="12"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="15.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6">om</seg></w> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>t</w> <w n="15.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="15.10" punct="vg:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
						<l n="16" num="3.4" lm="12"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w>-<w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="16.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="16.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="16.8" punct="pv:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="17" num="3.5" lm="12"><w n="17.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="17.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="17.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></w>,</l>
						<l n="18" num="3.6" lm="12"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="18.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="18.5" punct="dp:4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="dp in">oin</seg>s</w> : « <w n="18.6">C</w>’<w n="18.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="18.8" punct="pe:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pe">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="18.9"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="18.10" punct="pe:9">p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="pe">é</seg></w> ! <w n="18.11">c</w>’<w n="18.12"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="18.13" punct="pe:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> ! »</l>
					</lg>
				</div></body></text></TEI>