<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES237" modus="sm" lm_max="5">
					<head type="main">LA SINCÈRE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Ah ! c’est vous que je vois <lb></lb>
									Enfin ! et cette voix qui parle est votre voix ! <lb></lb>
									Pourquoi le sort mit-il mes jours si loin des vôtres ? <lb></lb>
									J’ai tant besoin de vous pour oublier les autres !
								</quote>
								 <bibl><hi rend="smallcap">Victor Hugo.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="5"><w n="1.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="1.3">l</w>’<w n="1.4" punct="pi:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pi">er</seg></w> ?</l>
						<l n="2" num="1.2" lm="5"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.5" punct="pt:5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="5"><w n="3.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w>-<w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.3">l</w>’<w n="3.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>,</l>
						<l n="4" num="1.4" lm="5"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.3" punct="pi:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pi">er</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="5"><w n="5.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.5">d</w>’<w n="5.6" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>,</l>
						<l n="6" num="2.2" lm="5"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="6.4" punct="pv:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="7" num="2.3" lm="5"><w n="7.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w></l>
						<l n="8" num="2.4" lm="5"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="8.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pe">an</seg>t</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="5"><w n="9.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="9.2">j</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6" punct="pv:5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pv">i</seg>x</w> ;</l>
						<l n="10" num="3.2" lm="5"><w n="10.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w>-<w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="pi:5">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pi">e</seg></w> ?</l>
						<l n="11" num="3.3" lm="5"><w n="11.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.6" punct="pv:5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pv">i</seg>x</w> ;</l>
						<l n="12" num="3.4" lm="5"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="12.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="12.5" punct="pt:5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="5"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5" punct="pi:5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="pi">en</seg></w> ?</l>
						<l n="14" num="4.2" lm="5"><w n="14.1" punct="pe:1">D<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1" punct="pe">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="14.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.5" punct="pt:5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="15" num="4.3" lm="5"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5" punct="vg:5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="vg">en</seg></w>,</l>
						<l n="16" num="4.4" lm="5"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4" punct="pi:5">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="pi">en</seg></w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="5"><w n="17.1">S</w>’<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="17.7" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>,</l>
						<l n="18" num="5.2" lm="5"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">n</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6" punct="pv:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="19" num="5.3" lm="5"><w n="19.1">S</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="19.3">n</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="19.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="19.7" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>,</l>
						<l n="20" num="5.4" lm="5"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="20.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="20.5" punct="pt:5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pt">oi</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="5"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="21.3" punct="vg:5">gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>,</l>
						<l n="22" num="6.2" lm="5"><w n="22.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4" punct="pv:5">v<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="23" num="6.3" lm="5"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="23.3" punct="vg:5">gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>,</l>
						<l n="24" num="6.4" lm="5"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="24.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l</w> <w n="24.4">l</w>’<w n="24.5" punct="pe:5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg></w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="5"><w n="25.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="25.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="25.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="25.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rs</w>,</l>
						<l n="26" num="7.2" lm="5"><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="26.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="26.4" punct="pv:5">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="27" num="7.3" lm="5"><w n="27.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="27.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="27.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rs</w>,</l>
						<l n="28" num="7.4" lm="5"><w n="28.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="28.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.5" punct="pt:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg>rs</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="5"><w n="29.1">L</w>’<w n="29.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="29.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="30" num="8.2" lm="5"><w n="30.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="30.4" punct="pv:5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="31" num="8.3" lm="5"><w n="31.1">L</w>’<w n="31.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="31.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="31.4" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>r</w>,</l>
						<l n="32" num="8.4" lm="5"><w n="32.1" punct="pe:2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">er</seg></w> ! <w n="32.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="32.3" punct="pt:5">m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pt">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>