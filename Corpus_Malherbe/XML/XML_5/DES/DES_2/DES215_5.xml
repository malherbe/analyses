<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES215" modus="cm" lm_max="12">
					<head type="main">ADIEU !</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Ce soleil un instant voilera son visage, <lb></lb>
									Et sans la rallumer laissera son image <lb></lb>
									S’éteindre au fond de l’eau.
								</quote>
								 <bibl><hi rend="smallcap">Joseph de Lorme.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe">i</seg>r</w> ! <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="1.4" punct="pe:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe">i</seg>r</w> ! <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</w> <w n="1.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.9" punct="vg:12">cr<seg phoneme="y" type="vs" value="1" rule="454" place="11">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">m</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</w>, <w n="2.7">m</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.9" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>t</w> : <w n="2.10">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.11">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="2.12" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>r</w> !</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="3.2" punct="ps:2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="ps">u</seg></w>… <w n="3.3" punct="pt:3">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pt">on</seg></w>. <w n="3.4">P<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="3.8">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="3.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.10" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg">en</seg>t</w>, <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="4.7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l</w> <w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="10">e</seg>s</w> <w n="4.9" punct="pv:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="5" num="1.5" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">t</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.6" punct="pe:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>s</w> ! <w n="5.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.9">s<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="5.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="5.11" punct="dp:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="dp">ou</seg>r</w> :</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>s</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="6.5" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pv">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="6.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="12"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2" punct="pe:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="pe">ou</seg>r</w> ! <w n="7.3">T<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rs</w> <w n="7.5" punct="pi:6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pi">on</seg>c</w> ? <w n="7.6" punct="vg:7">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="7" punct="vg">i</seg></w>, <w n="7.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="7.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="7.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="7.11" punct="pe:12">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="8" num="2.2" lm="12"><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="8.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="8.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>s</w> <w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="8.9">j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="8.10" punct="pt:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="9" num="2.3" lm="12"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.5" punct="pv:6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv">i</seg>x</w> ; <w n="9.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w>-<w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w>-<w n="9.8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="9.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>r</w> <w n="9.10" punct="pv:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pv">oi</seg></w> ;</l>
						<l n="10" num="2.4" lm="12"><w n="10.1" punct="vg:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="10.2">j</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5" punct="ps:6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="ps">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>… <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="10.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="10.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="10.10">c</w>’<w n="10.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="10.12" punct="pe:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="12"><w n="11.1" punct="vg:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.9">qu</w>’<w n="11.10"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="11.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.2" lm="12"><w n="12.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">l</w>’<w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.8">qu</w>’<w n="12.9"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="12.10" punct="pv:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="13" num="3.3" lm="12"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>ts</w> <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="13.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.6" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</w>, <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.8" punct="vg:9">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" punct="vg">e</seg>ct</w>, <w n="13.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="13.10" punct="dp:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="dp">ou</seg>r</w> :</l>
						<l n="14" num="3.4" lm="12"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="14.7">t</w>’<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.9">d<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="14.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.11" punct="pe:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1" lm="12"><w n="15.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe ps">i</seg>r</w> !… <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="15.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.7" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.2" lm="12"><w n="16.1">M</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="2">eoi</seg>r</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="16.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.5" punct="vg:6">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>x</w>, <w n="16.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="16.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="16.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="16.10" punct="pe:12">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>cs</w> !</l>
						<l n="17" num="4.3" lm="12"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.5" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg">ain</seg>s</w>, <w n="17.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="17.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="17.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="17.9">br<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="17.10" punct="vg:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</w>,</l>
						<l n="18" num="4.4" lm="12"><w n="18.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="18.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="18.7">d<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="18.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="18.9" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="19" num="5.1" lm="12"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">si<seg phoneme="ɛ" type="vs" value="1" rule="366" place="3">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="19.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="19.6">s</w>’<w n="19.7" punct="ps:9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="ps">e</seg></w>… <w n="19.8">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>s</w>-<w n="19.9" punct="pe:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>s</w> !</l>
						<l n="20" num="5.2" lm="12"><w n="20.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg>s</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="20.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="20.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="20.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="21" num="5.3" lm="12"><w n="21.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="21.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="21.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="21.6">qu</w>’<w n="21.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="21.8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="21.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="21.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="21.11" punct="pe:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="22" num="5.4" lm="12"><w n="22.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4" punct="vg:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="22.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="22.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="22.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="22.9" punct="pv:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>x</w> ;</l>
						<l n="23" num="5.5" lm="12"><w n="23.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="23.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="23.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="23.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="23.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="23.9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="23.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="23.11" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="24" num="5.6" lm="12"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="24.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="24.5" punct="ps:6">s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ps">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>… <w n="24.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="24.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="24.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="24.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="24.10" punct="pe:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>