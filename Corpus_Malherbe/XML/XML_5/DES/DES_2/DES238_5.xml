<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES238" modus="cm" lm_max="12">
					<head type="main">UNE FLEUR</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									L’odeur d’une simple violette suffit pour rappeler le <lb></lb>souvenir des jouissances de plusieurs printemps.
								</quote>
								 <bibl><hi rend="smallcap">Ramond.</hi></bibl>
							</cit>
							<cit>
								<quote>
									Que ne parlais-tu, quand il était temps de me sauver ?
								</quote>
								 <bibl><hi rend="smallcap">Clément XIV et Carlo.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pe">o</seg>r</w> ! <w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="1.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="1.9" punct="pv:12">d<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">m</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.4" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in">i</seg>t</w> : « <w n="2.5">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>s</w>-<w n="2.6" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="2.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.8" punct="vg:9">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9" punct="vg">i</seg>t</w>, <w n="2.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="2.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="2.11" punct="pe:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pe">œu</seg>r</w> ! »</l>
						<l n="3" num="1.3" lm="12"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4" punct="vg:4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.6" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg">ain</seg></w>, <w n="3.7">r<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="3.9">l</w>’<w n="3.10">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="10">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="3.11" punct="vg:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>rs</w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.7" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="11">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="5.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg></w></l>
						<l n="6" num="2.2" lm="12"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>ds</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>x</w> <w n="6.8" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">en</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="7.7">br<seg phoneme="y" type="vs" value="1" rule="445" place="9">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.8" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="1">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="8.7" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="9.4" punct="pt:6">cr<seg phoneme="y" type="vs" value="1" rule="454" place="5">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="pt">e</seg>l</w>. <w n="9.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="9.9">y<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
						<l n="10" num="3.2" lm="12"><w n="10.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="10.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg>s</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="10.8" punct="pv:12">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.7">m</w>’<w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="11.9" punct="dp:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="dp in">i</seg>t</w> : « <w n="11.10"><seg phoneme="i" type="vs" value="1" rule="468" place="11">I</seg>l</w> <w n="11.11">t</w>’<w n="11.12" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> ! »</l>
						<l n="12" num="3.4" lm="12"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="12.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="12.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="12.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.10">r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">m</w>’<w n="13.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6" punct="pt:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>r</w>. <w n="13.7">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="13.9">d<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>t</w> <w n="13.10" punct="pt:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</w>.</l>
						<l n="14" num="4.2" lm="12"><w n="14.1" punct="vg:2">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="14.5">j</w>’<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="14.8" punct="pv:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="15" num="4.3" lm="12"><w n="15.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="193" place="3" punct="vg">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</w>, <w n="15.5">s</w>’<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>nt</w> <w n="15.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg>s</w> <w n="15.8" punct="dp:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="16" num="4.4" lm="12"><w n="16.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="16.2">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="16.3">d</w>’<w n="16.4" punct="ps:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="ps">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="16.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="16.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="16.9">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="16.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="16.11">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="16.12" punct="pe:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>