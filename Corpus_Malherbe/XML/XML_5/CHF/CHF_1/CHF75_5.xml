<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF75" modus="cp" lm_max="12">
					<head type="main">A UNE FEMME</head>
					<head type="sub_2">Qui prétendait que ses amis ne s’occupaient pas d’elle</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10"><space unit="char" quantity="4"></space><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="1.6" punct="vg:8">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>s</w>, <w n="1.7" punct="pv:10">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg></w> ;</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w>-<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="2.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>s</w> <w n="2.10" punct="pv:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pv">en</seg>t</w> ;</l>
						<l n="3" num="1.3" lm="10"><space unit="char" quantity="4"></space><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="3.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="vg:6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</w>, <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
						<l n="4" num="1.4" lm="10"><space unit="char" quantity="4"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w>-<w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.7"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="4.8" punct="pt:10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="10" punct="pt">an</seg>t</w>.</l>
					</lg>
				</div></body></text></TEI>