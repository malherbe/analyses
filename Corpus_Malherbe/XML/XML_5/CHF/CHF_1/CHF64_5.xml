<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF64" modus="cp" lm_max="12">
					<head type="main">AUTRE DU MÊME</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.4" punct="vg:6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>ds</w>, <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="1.8" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w>-<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="2.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9">o</seg>p</w> <w n="2.8">l</w>’<w n="2.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="3.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.4">qu</w>’<w n="3.5" punct="vg:4"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.7">v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="3.8">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="3.9">qu</w>’<w n="3.10" punct="dp:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</w> :</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="4.4" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>s</w>, <w n="4.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="4.8" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
						<l n="5" num="1.5" lm="12"><w n="5.1" punct="dp:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="dp">eu</seg></w> : <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="5.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="5.7" punct="pv:12">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4" punct="pv:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pv">on</seg>s</w> ; <w n="6.5" punct="vg:10">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">ez</seg></w>, <w n="6.6" punct="vg:12">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></w>,</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ccr<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="8" num="1.8" lm="12"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5" punct="vg:6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg">oin</seg>t</w>, <w n="8.6" punct="dp:8">D<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>s</w> : <w n="8.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="8.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">ez</seg></w>.</l>
					</lg>
				</div></body></text></TEI>