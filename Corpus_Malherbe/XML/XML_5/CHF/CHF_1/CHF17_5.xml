<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF17" modus="sm" lm_max="8">
					<head type="main">CALCUL PATRIOTIQUE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t</w> <w n="1.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6" punct="pe:8">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="2.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>ts</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg></w> !</l>
						<l n="3" num="1.3" lm="8"><w n="3.1" punct="vg:2">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="3.2" punct="vg:4">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rp<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="4.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pt:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="5" num="1.5" lm="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.4" punct="vg:6">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="5.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w>-<w n="5.6" punct="vg:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
						<l n="6" num="1.6" lm="8"><w n="6.1">Qu</w>’<w n="6.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="7" num="1.7" lm="8"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
						<l n="8" num="1.8" lm="8"><w n="8.1">D</w>’<w n="8.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ppr<seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
						<l n="9" num="1.9" lm="8"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="10" num="1.10" lm="8"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2" punct="dp:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="dp">on</seg></w> :</l>
						<l n="11" num="1.11" lm="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="vg:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3" punct="vg">en</seg>t</w>, <w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="11.5" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>,</l>
						<l n="12" num="1.12" lm="8"><w n="12.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="12.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.6"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="12.7" punct="pe:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="13" num="1.13" lm="8"><w n="13.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="13.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="13.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="13.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>p<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>ts</w></l>
						<l n="14" num="1.14" lm="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="14.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="14.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="14.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w>-<w n="14.7" punct="pv:8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="15" num="1.15" lm="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="15.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3">en</seg>s</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="15.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>x</w></l>
						<l n="16" num="1.16" lm="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">B<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5" punct="pv:8">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>pt<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="17" num="1.17" lm="8"><w n="17.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="17.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="17.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="17.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="17.6">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>s</w></l>
						<l n="18" num="1.18" lm="8"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="19" num="1.19" lm="8"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3" punct="vg:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="19.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="20" num="1.20" lm="8"><w n="20.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>sf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="20.5" punct="pt:8">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg>s</w>.</l>
						<l n="21" num="1.21" lm="8"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="21.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="21.5">ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="22" num="1.22" lm="8"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="22.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu</w>’<w n="22.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="23" num="1.23" lm="8"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="23.5" punct="pt:8">chr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
						<l n="24" num="1.24" lm="8"><w n="24.1">M<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.3">c<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="24.4">l</w>’<w n="24.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="24.6" punct="dp:8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="25" num="1.25" lm="8"><w n="25.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">qu</w>’<w n="25.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="25.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd</w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="25.6" punct="vg:8">th<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="26" num="1.26" lm="8"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="26.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="26.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="26.6" punct="pt:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="pt">en</seg></w>.</l>
						<l n="27" num="1.27" lm="8"><w n="27.1">L</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.3">pr<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d</w>’<w n="27.4">h<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="28" num="1.28" lm="8"><w n="28.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="28.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w>-<w n="28.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="28.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="28.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="28.7" punct="pt:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
						<l n="29" num="1.29" lm="8"><w n="29.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w>-<w n="29.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.6" punct="pi:8">m<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363" place="8" punct="pi">en</seg></w> ?</l>
						<l n="30" num="1.30" lm="8"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="30.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="30.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="30.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="30.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="30.7" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="31" num="1.31" lm="8"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="31.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="31.4" punct="vg:5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>, <w n="31.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="32" num="1.32" lm="8"><w n="32.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="32.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="32.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="32.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="32.6" punct="vg:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></w>,</l>
						<l n="33" num="1.33" lm="8"><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="33.3" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="33.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="33.5" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363" place="8" punct="vg">en</seg></w>,</l>
						<l n="34" num="1.34" lm="8"><w n="34.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="34.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="34.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="34.5" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="35" num="1.35" lm="8"><w n="35.1" punct="pe:2">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="35.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="35.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="35.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="35.5" punct="vg:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</w>,</l>
						<l n="36" num="1.36" lm="8"><w n="36.1">M<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="36.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="36.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="36.4" punct="dp:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="dp">è</seg>s</w> :</l>
						<l n="37" num="1.37" lm="8"><w n="37.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="37.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="37.4" punct="dp:8">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="38" num="1.38" lm="8"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="38.2">l</w>’<w n="38.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="38.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="38.5" punct="pv:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="pv">è</seg>s</w> ;</l>
						<l n="39" num="1.39" lm="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">m<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="39.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="39.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="39.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w></l>
						<l n="40" num="1.40" lm="8"><w n="40.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="ital"><w n="40.2">m<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>rs</w></hi> <w n="40.3">l</w>’<w n="40.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="40.5" punct="dp:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>str<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="41" num="1.41" lm="8"><w n="41.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="41.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="41.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="41.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w></l>
						<l n="42" num="1.42" lm="8"><w n="42.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="42.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="42.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="42.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="43" num="1.43" lm="8"><w n="43.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="43.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="43.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="43.4">gu<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="43.5" punct="pt:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>