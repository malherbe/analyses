<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">PAYSAGES</head><div type="poem" key="ELS8" modus="sm" lm_max="8">
					<head type="number">VII</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="1.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="1.4" punct="vg:4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.6">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.8" punct="vg:8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</w>,</l>
						<l n="2" num="1.2" lm="8">— <w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="2.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.3" punct="vg:5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>r</w>, <w n="2.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.5" punct="tc:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg ti">oi</seg>r</w>, —</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="8"><w n="3.1">f<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>ts</w> <w n="3.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="189" place="5" punct="vg">e</seg>t</w>, <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="4" num="2.2" lm="8"><w n="4.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="vg">e</seg>ts</w>, <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="4.4" punct="vg:5">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="4.5" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1" lm="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="5.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="5.4" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="5.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="3.2" lm="8"><w n="6.1">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="6.2" punct="vg:4">d<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="6.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1" lm="8"><w n="7.1">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2" punct="pe:3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="pe">i</seg>t</w> ! <w n="7.3" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>r</w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>ts</w> <w n="7.6" punct="vg:8">ch<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>,</l>
						<l n="8" num="4.2" lm="8"><w n="8.1">v<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.5">n</w>’<w n="8.6"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="8.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="8.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w></l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1" lm="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="9.2" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="9.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>gts</w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="9.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.8" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="10" num="5.2" lm="8"><w n="10.1">c</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1" lm="8"><w n="11.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="11.2">b<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="11.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s</w> <w n="11.7" punct="vg:8">p<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="12" num="6.2" lm="8"><w n="12.1">c<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">c<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="12.7" punct="vg:8">pr<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>nt</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1" lm="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="13.4" punct="vg:5">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="13.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="13.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8">en</seg>s</w></l>
						<l n="14" num="7.2" lm="8"><w n="14.1">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1" lm="8"><w n="15.1">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2" punct="pe:3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="pe">i</seg>t</w> ! <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.4" punct="vg:6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.6" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="16" num="8.2" lm="8"><w n="16.1">br<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="16.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="16.8">l</w>’<w n="16.9" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="17.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="17.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>gts</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="17.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="17.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c</w></l>
						<l n="18" num="9.2" lm="8"><w n="18.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.5" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>ts</w> ;</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1" lm="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="19.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>x</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="19.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.6" punct="dp:8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="20" num="10.2" lm="8"><w n="20.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="20.2" punct="vg:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg>s</w>, <w n="20.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ts</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="20.5" punct="vg:8">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1" lm="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="21.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="21.4">c<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="21.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="21.6">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>nt</w></l>
						<l n="22" num="11.2" lm="8"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg></w> <w n="22.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="22.4" punct="vg:4">dr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>ps</w>, <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="22.6">ch<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>d</w> <w n="22.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.8" punct="vg:8">l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1" lm="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="23.2">Chr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>sts</w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="23.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>d</w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.6">l</w>’<w n="23.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.8" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="24" num="12.2" lm="8"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="24.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="24.3" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1" lm="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="25.2">Ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="25.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w></l>
						<l n="26" num="13.2" lm="8"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="26.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.5" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>