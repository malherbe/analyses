<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">PAYSAGES</head><div type="poem" key="ELS7" modus="sp" lm_max="8">
					<head type="number">VI</head>
					<lg n="1">
						<l n="1" num="1.1" lm="4"><space unit="char" quantity="8"></space><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="2.2" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg>x</w>, <w n="2.3" punct="vg:4">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>fs</w>, <w n="2.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="2.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
						<l n="3" num="1.3" lm="3"><space unit="char" quantity="10"></space><w n="3.1" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="pe">é</seg></w> ! <w n="3.2" punct="pe:3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg></w> !</l>
						<l n="4" num="1.4" lm="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>ts</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.4" punct="vg:5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.6" punct="vg:8">dr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
						<l n="5" num="1.5" lm="4"><space unit="char" quantity="8"></space><w n="5.1">c<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="5.4" punct="pv:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></w> ;</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1" lm="4"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="6.2">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3" punct="vg:4">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.2" lm="8"><w n="7.1" punct="vg:2">h<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="7.2" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg>s</w>, <w n="7.3" punct="vg:6">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg>s</w>, <w n="7.4" punct="vg:8">p<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="2.3" lm="3"><space unit="char" quantity="10"></space><w n="8.1" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="pe">é</seg></w> ! <w n="8.2" punct="pe:3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg></w> !</l>
						<l n="9" num="2.4" lm="8"><w n="9.1">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
						<l n="10" num="2.5" lm="4"><space unit="char" quantity="8"></space><w n="10.1">p<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.3" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1" lm="4"><space unit="char" quantity="8"></space><w n="11.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="11.4" punct="vg:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="12" num="3.2" lm="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="12.6" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
						<l n="13" num="3.3" lm="3"><space unit="char" quantity="10"></space><w n="13.1" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="pe">é</seg></w> ! <w n="13.2" punct="pe:3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg></w> !</l>
						<l n="14" num="3.4" lm="8"><w n="14.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="14.4" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="8" punct="vg">ô</seg>t</w>,</l>
						<l n="15" num="3.5" lm="4"><space unit="char" quantity="8"></space><w n="15.1" punct="pe:1">f<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="pe">eu</seg></w> ! <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="15.3" punct="pv:4">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pv">eau</seg>x</w> ;</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1" lm="4"><space unit="char" quantity="8"></space><w n="16.1">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="16.4" punct="vg:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
						<l n="17" num="4.2" lm="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="17.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="17.4" punct="vg:5">qu<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>, <w n="17.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>cks</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="17.7">l</w>’<w n="17.8" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
						<l n="18" num="4.3" lm="3"><space unit="char" quantity="10"></space><w n="18.1" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="pe">é</seg></w> ! <w n="18.2" punct="pe:3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg></w> !</l>
						<l n="19" num="4.4" lm="8"><w n="19.1">t<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="19.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="19.5" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>t</w>,</l>
						<l n="20" num="4.5" lm="4"><space unit="char" quantity="8"></space><w n="20.1">c<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="20.4" punct="pt:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>