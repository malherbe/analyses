<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO60" modus="cp" lm_max="12">
					<head type="main">DIALOGUE ENTRE DEUX SERVANTES</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><space unit="char" quantity="8"></space>« <w n="1.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="1.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="1.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="1.5" punct="pi:8">C<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></w> ?</l>
						<l n="2" num="1.2" lm="8"><space unit="char" quantity="8"></space>» ‒ <w n="2.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="2.2" punct="pe:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ls<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pe">é</seg></w> ! <w n="2.3">c</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="2.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.7" punct="pt:8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="12">» <w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="3.4" punct="vg:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="3.8">qu</w>’<w n="3.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="3.10">m</w>’<w n="3.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10"><space unit="char" quantity="4"></space>» <w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="4.4">n</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="4.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="4.9" punct="pt:10">gr<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></w>.</l>
						<l n="5" num="1.5" lm="12">« ‒ <w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="5.2" punct="pi:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pi in">a</seg></w> ? ‒ <w n="5.3" punct="pi:6">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="pi">en</seg>t</w> ? <w n="5.4" punct="vg:7">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7" punct="vg">en</seg>s</w>, <w n="5.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w>-<w n="5.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10" punct="vg">en</seg></w>, <w n="5.7" punct="dp:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="6" num="1.6" lm="12">» <w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.6">qu<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="6.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>s</w>,</l>
						<l n="7" num="1.7" lm="8"><space unit="char" quantity="8"></space>» <w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.5" punct="pv:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg>s</w> ;</l>
						<l n="8" num="1.8" lm="12">» <w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.4">cl<seg phoneme="e" type="vs" value="1" rule="148" place="3">e</seg>f</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="8.10">n</w>’<w n="8.11"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="8.12">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg></w> <w n="8.13"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="8.14" punct="pt:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="9" num="1.9" lm="12">» ‒ <w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3" punct="pi:3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="ps pi in">i</seg>t</w>… ? ‒ <w n="9.4" punct="pe:4"><seg phoneme="o" type="vs" value="1" rule="444" place="4" punct="pe">O</seg>h</w> ! <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="9.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="9.8">f<seg phoneme="œ" type="vs" value="1" rule="304" place="8">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="9.9">l<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="342" place="11">à</seg></w> <w n="9.11" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>rt</w> ;</l>
						<l n="10" num="1.10" lm="8"><space unit="char" quantity="8"></space>» <w n="10.1">M<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>rl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>t</w> <w n="10.6" punct="vg:8">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="1.11" lm="12">» <w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="11.7">m<seg phoneme="e" type="vs" value="1" rule="353" place="8">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="11.8" punct="pt:12">Ch<seg phoneme="u" type="vs" value="1" rule="d-2" place="11">ou</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>rt</w>.</l>
						<l n="12" num="1.12" lm="8"><space unit="char" quantity="8"></space>» ‒ <w n="12.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>il</w> <w n="12.7" punct="pe:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="13" num="1.13" lm="8"><space unit="char" quantity="8"></space>» <w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="13.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="13.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="13.5">d</w>’<w n="13.6" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="7">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="dp">i</seg></w> :</l>
						<l n="14" num="1.14" lm="12">» <w n="14.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">j</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="14.6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="14.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="14.8">ch<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="14.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="14.10" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="15" num="1.15" lm="12">» <w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="15.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="15.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="15.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="15.11" punct="vg:12">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="16" num="1.16" lm="8"><space unit="char" quantity="8"></space>» <w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="16.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="16.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="16.7" punct="pt:8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg></w>. »</l>
					</lg>
				</div></body></text></TEI>