<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL15" modus="cm" lm_max="12">
					<head type="main">Patrie</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="2" punct="pe">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> ! <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="1.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4">om</seg></w> <w n="1.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="1.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w>-<w n="1.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w> <w n="1.8" punct="pi:12">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="12" punct="pi">en</seg></w> ?</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="2.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="2.6">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="2.9">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="3" num="1.3" lm="12"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="3.4">br<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="3.8" punct="vg:12">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="4.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8">en</seg>s</w> <w n="4.8" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pt">en</seg></w>.</l>
						<l n="5" num="1.5" lm="12"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="5.6" punct="vg:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="5.7" punct="vg:9">C<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>di<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9" punct="vg">en</seg>s</w>, <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="6" num="1.6" lm="12"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="6.5">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t</w>-<w n="6.6" punct="vg:6">L<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg">en</seg>t</w>, <w n="6.7">c</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="6.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.10">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="6.11" punct="pv:12">S<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="323" place="12" punct="pv">ay</seg></w> ;</l>
						<l n="7" num="1.7" lm="12"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="7.8">p<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="8" num="1.8" lm="12"><w n="8.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2" punct="vg:3">f<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.7" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="9" num="1.9" lm="12"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ct</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="9.5">l</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="9.8">l</w>’<w n="9.9" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>r</w> ;</l>
						<l n="10" num="1.10" lm="12"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">l</w>’<w n="10.4">h<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.9">l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.10" punct="ps:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
						<l n="11" num="1.11" lm="12"><w n="11.1" punct="vg:2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>pl<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2" punct="vg">ain</seg></w>, <w n="11.2" punct="vg:4">Br<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>f</w>, <w n="11.3" punct="vg:6">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tc<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>lm</w>, <w n="11.4">Fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.6" punct="pe:12">L<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="12" num="1.12" lm="12"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="3" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="12.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ds</w> <w n="12.5" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg">o</seg>rts</w>, <w n="12.6">c</w>’<w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="12.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="12.9" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</w>.</l>
					</lg>
					<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				</div></body></text></TEI>