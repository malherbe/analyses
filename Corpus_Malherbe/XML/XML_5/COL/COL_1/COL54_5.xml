<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL54" modus="sp" lm_max="6">
				<head type="main">CHANSON DE COMÉDIE</head>
				<head type="sub_1">EN PERSONNES NATURELLES.</head>
				<head type="tune">Air : O gué, lanla, lanlere, etc.</head>
				<head type="main">Noté, N°. 33.</head>
				<lg n="1">
					<l n="1" num="1.1" lm="6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="1.3">M<seg phoneme="œ" type="vs" value="1" rule="151" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="4">eu</seg>r</w> <w n="1.4">L<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="2" num="1.2" lm="4"><space unit="char" quantity="4"></space><w n="2.1">V<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w> <w n="2.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="vg">ou</seg></w>,</l>
					<l n="3" num="1.3" lm="6"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="3.6" punct="vg:6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="4"><space unit="char" quantity="4"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">sç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="4.4" punct="pt:4"><seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="pt">où</seg></w>.</l>
					<l n="5" num="1.5" lm="4"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.3">F<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</w></l>
					<l n="6" num="1.6" lm="3"><space unit="char" quantity="6"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t</w>-<w n="6.3" punct="vg:3">Cl<seg phoneme="u" type="vs" value="1" rule="426" place="3" punct="vg">ou</seg></w>,</l>
					<l n="7" num="1.7" lm="5"><space unit="char" quantity="2"></space><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">Gr<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w>-<w n="7.4" punct="pt:5">c<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="426" place="5" punct="pt">ou</seg></w>.</l>
					<l n="8" num="1.8" lm="5"><space unit="char" quantity="2"></space><w n="8.1">S</w>’<w n="8.2">t<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="8.3" punct="vg:5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="u" type="vs" value="1" rule="426" place="5" punct="vg">ou</seg></w>,</l>
					<l n="9" num="1.9" lm="6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="9.4" punct="vg:6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="1.10" lm="4"><space unit="char" quantity="4"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.4" punct="pt:4">f<seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="pt">ou</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="11" num="2.1" lm="6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3" punct="vg:3">V<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.5" punct="vg:6">C<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="12" num="2.2" lm="4"><space unit="char" quantity="4"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="12.3" punct="pv:4">g<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pv">a</seg>rd</w> ;</l>
					<l n="13" num="2.3" lm="6"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="14" num="2.4" lm="4"><space unit="char" quantity="4"></space><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w>-<w n="14.3" punct="vg:3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>t</w>, <w n="14.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w></l>
					<l rhyme="none" n="15" num="2.5" lm="6"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.3" punct="vg:5">F<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="15.4" punct="pt:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pt">e</seg>tc</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="16" num="3.1" lm="6"><w n="16.1" punct="vg:2">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">ON</seg>T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">ON</seg>S</w>, <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">M<seg phoneme="œ" type="vs" value="1" rule="151" place="4">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>r</w> <w n="16.4">G<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="17" num="3.2" lm="4"><space unit="char" quantity="4"></space><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="17.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="17.4" punct="dp:4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp">ai</seg>ts</w> :</l>
					<l n="18" num="3.3" lm="6"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="18.2">F<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.5">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="19" num="3.4" lm="4"><space unit="char" quantity="4"></space><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="19.3" punct="pv:3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pv">eu</seg>t</w> ; <w n="19.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w></l>
					<l rhyme="none" n="20" num="3.5" lm="6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3" punct="vg:5">F<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="20.4" punct="pt:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pt">e</seg>tc</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="21" num="4.1" lm="6"><w n="21.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">OU</seg>V<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">EN</seg>T</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="22" num="4.2" lm="4"><space unit="char" quantity="4"></space><w n="22.1">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="22.3" punct="pv:4">Tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv">eu</seg>r</w> ;</l>
					<l n="23" num="4.3" lm="6"><w n="23.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="23.2">c</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="23.6">Gu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="24" num="4.4" lm="4"><space unit="char" quantity="4"></space><w n="24.1">Qu</w>’<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="24.3">bl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="24.5" punct="dp:4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="dp">œu</seg>r</w> :</l>
					<l rhyme="none" n="25" num="4.5" lm="6"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3" punct="vg:5">F<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="25.4" punct="pt:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="pt">e</seg>tc</w>.</l>
				</lg>
			</div></body></text></TEI>