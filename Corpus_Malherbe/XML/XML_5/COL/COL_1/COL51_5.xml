<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL51" modus="sp" lm_max="8">
				<head type="main">RONDE.</head>
				<head type="tune">Air : Noté, N°. 30.</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">ON</seg>F<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">E</seg>SS<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">E</seg></w> <w n="1.3">m</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
					<l n="2" num="1.2" lm="6"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4" punct="dp:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></w> :</l>
					<l n="3" num="1.3" lm="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="3.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="3.4">p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">j</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="3.8" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>t</w>,</l>
					<l n="4" num="1.4" lm="6"><space unit="char" quantity="4"></space><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.6" punct="pt:6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="5" num="1.5" lm="6"><space unit="char" quantity="4"></space><w n="5.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>,</l>
					<l n="6" num="1.6" lm="3"><space unit="char" quantity="10"></space><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="6.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="6.3" punct="vg:3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>,</l>
					<l n="7" num="1.7" lm="6"><space unit="char" quantity="4"></space><w n="7.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="7.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4" punct="pt:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="8" num="2.1" lm="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="8.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="8.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="8.4">p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">j</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="8.8" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>t</w>,</l>
					<l n="9" num="2.2" lm="6"><space unit="char" quantity="4"></space><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">d</w>’<w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.6" punct="pt:6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="10" num="2.3" lm="8"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2" punct="vg:3">F<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="10.6" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></w>,</l>
					<l n="11" num="2.4" lm="6"><space unit="char" quantity="4"></space><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.5" punct="pt:6">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="6">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="12" num="2.5" lm="6"><space unit="char" quantity="4"></space><w n="12.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="12.5" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>,</l>
					<l n="13" num="2.6" lm="3"><space unit="char" quantity="10"></space><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="13.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="13.3" punct="vg:3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>,</l>
					<l n="14" num="2.7" lm="6"><space unit="char" quantity="4"></space><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="14.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4" punct="pt:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="15" num="3.1" lm="8"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="15.2" punct="vg:3">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="15.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="15.6" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></w>,</l>
					<l n="16" num="3.2" lm="6"><space unit="char" quantity="4"></space><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="16.5" punct="pt:6">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="6">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="17" num="3.3" lm="8"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="17.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="17.3">M<seg phoneme="œ" type="vs" value="1" rule="151" place="4">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>r</w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5" punct="vg:8">C<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="18" num="3.4" lm="6"><space unit="char" quantity="4"></space><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="18.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="18.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="18.4">l</w>’<w n="18.5" punct="pi:6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></w> ?</l>
					<l n="19" num="3.5" lm="6"><space unit="char" quantity="4"></space><w n="19.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="19.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>,</l>
					<l n="20" num="3.6" lm="3"><space unit="char" quantity="10"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="20.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="20.3" punct="vg:3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>,</l>
					<l n="21" num="3.7" lm="6"><space unit="char" quantity="4"></space><w n="21.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="21.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4" punct="pt:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="22" num="4.1" lm="8"><w n="22.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>T<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">E</seg>S</w>-<w n="22.2" punct="vg:3">M<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">OI</seg></w>, <w n="22.3">M<seg phoneme="œ" type="vs" value="1" rule="151" place="4">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>r</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5" punct="vg:8">C<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="23" num="4.2" lm="6"><space unit="char" quantity="4"></space><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="23.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="23.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5">e</seg></w> <w n="23.4">l</w>’<w n="23.5" punct="pi:6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></w> ?</l>
					<l n="24" num="4.3" lm="8"><w n="24.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="24.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="24.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">oû</seg>t</w> <w n="24.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="24.6" punct="ps:8">p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="ps">é</seg></w>…</l>
					<l n="25" num="4.4" lm="6"><space unit="char" quantity="4"></space><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="25.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d</w>, <w n="25.4" punct="pt:6">fr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="26" num="4.5" lm="6"><space unit="char" quantity="4"></space><w n="26.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="26.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>,</l>
					<l n="27" num="4.6" lm="3"><space unit="char" quantity="10"></space><w n="27.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="27.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="27.3" punct="vg:3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>,</l>
					<l n="28" num="4.7" lm="6"><space unit="char" quantity="4"></space><w n="28.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="28.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.4" punct="pt:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="29" num="5.1" lm="8"><w n="29.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>H</w> ! <w n="29.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="29.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="29.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">oû</seg>t</w> <w n="29.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="29.6" punct="ps:8">p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="ps">é</seg></w>…</l>
					<l n="30" num="5.2" lm="6"><space unit="char" quantity="4"></space><w n="30.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="30.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d</w>, <w n="30.4" punct="pt:6">fr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="31" num="5.3" lm="8"><w n="31.1" punct="vg:1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1" punct="vg">en</seg></w>, <w n="31.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="31.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="31.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>q</w> <w n="31.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="31.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>x</w> <w n="31.7" punct="vg:8">f<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>,</l>
					<l n="32" num="5.4" lm="6"><space unit="char" quantity="4"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="32.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.5" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="33" num="5.5" lm="6"><space unit="char" quantity="4"></space><w n="33.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="33.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="33.3" punct="vg:3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add><w n="33.4" punct="pt:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg></w></add></subst>.</l>
					<l n="34" num="5.6" lm="3"><space unit="char" quantity="10"></space><subst hand="RR" reason="analysis" type="repetition"><del></del><add><w n="34.1" punct="pt:3">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="34.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="34.3" punct="vg:3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt">a</seg></w>,</add></subst>.</l>
					<l n="35" num="5.7" lm="6"><space unit="char" quantity="4"></space><subst hand="RR" reason="analysis" type="repetition"><del></del><add><w n="35.1" punct="pt:6">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="35.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="35.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.4" punct="pt:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</add></subst>.</l>

				</lg>
				<ab type="star">❉</ab>
				<lg n="6">
					<l n="36" num="6.1" lm="8"><w n="36.1" punct="vg:1">BI<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1" punct="vg">EN</seg></w>, <w n="36.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="36.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="36.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>q</w> <w n="36.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="36.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>x</w> <w n="36.7">f<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w></l>
					<l n="37" num="6.2" lm="6"><space unit="char" quantity="4"></space><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="37.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="37.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="37.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="37.5" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="38" num="6.3" lm="8"><w n="38.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="38.2" punct="vg:3">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="38.3">M<seg phoneme="œ" type="vs" value="1" rule="151" place="4">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="5">eu</seg>r</w> <w n="38.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="38.5" punct="vg:8">C<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="39" num="6.4" lm="6"><space unit="char" quantity="4"></space><w n="39.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="39.2">P<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="39.4" punct="pt:6">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					<l n="40" num="6.5" lm="6"><space unit="char" quantity="4"></space><w n="40.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="40.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="40.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="40.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>,</l>
					<l n="41" num="6.6" lm="3"><space unit="char" quantity="10"></space><w n="41.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="41.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="41.3" punct="vg:3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>,</l>
					<l n="42" num="6.7" lm="6"><space unit="char" quantity="4"></space><w n="42.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="42.2">C<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="42.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="42.4" punct="pt:6">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>