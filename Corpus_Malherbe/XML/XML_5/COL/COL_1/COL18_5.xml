<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL18" modus="sp" lm_max="8">
				<head type="main">LA PRÉCAUTION,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Où allez-vous, Monsieur l’Abbé ?</head>
				<head type="tune">Noté, N°. 16.</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>T<seg phoneme="i" type="vs" value="1" rule="468" place="2">I</seg>SF<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">AI</seg>T<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">E</seg>S</w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="1.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="1.4" punct="dp:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>rs</w> :</l>
					<l n="2" num="1.2" lm="8"><w n="2.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="2.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.6" punct="vg:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>rs</w>,</l>
					<l n="3" num="1.3" lm="6"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4" punct="vg:4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="3.5" punct="vg:6">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="2"><space unit="char" quantity="12"></space><w n="4.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="4.2" punct="pi:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pi">en</seg></w> ?</l>
					<l n="5" num="1.5" lm="6"><space unit="char" quantity="4"></space><w n="5.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="5.2" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4" punct="ps:6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></w>…</l>
					<l n="6" num="1.6" lm="5"><space unit="char" quantity="6"></space><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">m</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="6.4" punct="pt:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" punct="pt">en</seg></w>.</l>
				</lg>
			</div></body></text></TEI>