<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR20" modus="cp" lm_max="12">
						<head type="main">Odelette</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12"><w n="1.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="1.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4">e</seg></w> <w n="1.6">v<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="1.10">m<seg phoneme="wa" type="vs" value="1" rule="440" place="10">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363" place="11">en</seg></w> <w n="1.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.4" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="vg">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="2.7" punct="pe:12">tr<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</w> !</l>
							<l n="3" num="1.3" lm="8"><space unit="char" quantity="8"></space><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="3.3">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="3.5">l</w>’<w n="3.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="8"><space unit="char" quantity="8"></space><w n="4.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="5" num="1.5" lm="8"><space unit="char" quantity="8"></space><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="5.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1" lm="12"><w n="6.1" punct="vg:1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>c</w>, <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="6.7" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
							<l n="7" num="2.2" lm="12"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="7.6">l</w>’<w n="7.7" punct="pv:12">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
							<l n="8" num="2.3" lm="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>str<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="9" num="2.4" lm="8"><space unit="char" quantity="8"></space><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w>-<w n="9.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="9.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="10" num="2.5" lm="8"><space unit="char" quantity="8"></space><w n="10.1">Br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.4" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
						</lg>
					</div></body></text></TEI>