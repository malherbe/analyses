<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN8" modus="sm" lm_max="8">
					<head type="main">Sur la Beauté.</head>
					<head type="form">SONNET VII.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">EIN</seg>T</w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5" punct="vg:8">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="2.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>j<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w></l>
						<l n="3" num="1.3" lm="8"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="dc-2" place="6">ou</seg><seg phoneme="i" type="vs" value="1" rule="477" place="7">ï</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="8"><w n="4.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">d</w>’<w n="4.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="5.4" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>j<seg phoneme="u" type="vs" value="1" rule="BEN__4" place="6">oü</seg><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8"><w n="6.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="6.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>f</w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="6.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</w>,</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="8"><w n="8.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="8.7" punct="pv:8">fr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>ts</w> <w n="9.6" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rp<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="8"><w n="10.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="10.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="10.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="3.3" lm="8"><w n="11.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d</w> <w n="11.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="11.5" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>s</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="8"><w n="12.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="12.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.7">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="13" num="4.2" lm="8"><w n="13.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.7" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="8" punct="vg">y</seg>s</w>,</l>
						<l n="14" num="4.3" lm="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="14.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="14.6" punct="pt:8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>