<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN82" modus="cm" lm_max="12">
					<head type="form">SONNET.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="1.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="1.7" punct="vg:6">p<seg phoneme="wɛ" type="vs" value="1" rule="BEN82_1" place="6" punct="vg">oë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="1.8">H<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="1.10">l</w>’<w n="1.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>t</w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="2.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.8" punct="pv:12">p<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>t</w> <w n="3.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>su<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="3.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>s</w> <w n="3.7">n</w>’<w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="3.9" punct="vg:12">sç<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>t</w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6">en</seg>s</w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="9">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="4.9" punct="pt:12">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="5.8" punct="vg:12">pr<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>t</w>,</l>
						<l n="6" num="2.2" lm="12"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="6.6">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>s</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.8" punct="vg:12">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.2" punct="vg:4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>t</w>, <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="7.5">qu</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.7" punct="vg:12">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>t</w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="8.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="8.7">f<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="8.8" punct="pt:12">dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">qu</w>’<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="9.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.7" punct="vg:6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="vg">ein</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="9.10">f<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t</w> <w n="9.11" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">cr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t</w> <w n="10.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.8" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>r</w> ;</l>
						<l n="11" num="3.3" lm="12"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10">e</seg>nt</w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="11.8" punct="pt:12">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">qu</w>’<w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="12.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.7" punct="vg:6">r<seg phoneme="wa" type="vs" value="1" rule="424" place="6" punct="vg">oy</seg></w>, <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.9">n<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="12.10">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="12.11">l</w>’<w n="12.12" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="vg">en</seg>d</w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="13.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="13.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t</w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="13.10">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="14" num="4.3" lm="12"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.4">sç<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="14.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="14.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="14.10" punct="pt:12">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>d</w>.</l>
					</lg>
				</div></body></text></TEI>