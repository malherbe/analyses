<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN15" modus="sm" lm_max="8">
					<head type="main">Sur la Laideur.</head>
					<head type="form">SONNET XIV.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">AIN</seg>S</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="1.5">s</w>’<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w></l>
						<l n="2" num="1.2" lm="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="2.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="2.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="2.5" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
						<l n="4" num="1.4" lm="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="4.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ls</w> <w n="5.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w></l>
						<l n="6" num="2.2" lm="8"><w n="6.1">J<seg phoneme="u" type="vs" value="1" rule="BEN__2" place="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">ë</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="6.5" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="7" num="2.3" lm="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="424" place="2" punct="vg">oy</seg></w>, <w n="7.3">j</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w></l>
						<l n="8" num="2.4" lm="8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="8.7" punct="pt:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8"><w n="9.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>s</w> <w n="9.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</w>,</l>
						<l n="10" num="3.2" lm="8"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="10.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="10.6" punct="vg:8">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>gt</w>,</l>
						<l n="11" num="3.3" lm="8"><w n="11.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ls</w> <w n="11.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ts</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="11.6" punct="pv:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="8"><w n="12.1">Br<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="12.2" punct="vg:3">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s</w>, <w n="12.3">fr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.4" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>z<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
						<l n="13" num="4.2" lm="8"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="13.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6" punct="vg:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="14" num="4.3" lm="8"><w n="14.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Ou</seg></w>, <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="14.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="14.4" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="14.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="14.7" punct="pt:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>