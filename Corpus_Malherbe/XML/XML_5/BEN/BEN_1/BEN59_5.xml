<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN59" modus="cm" lm_max="12">
					<head type="main">Sur l’Amour.</head>
					<head type="form">BOUTS-RIMEZ.</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:1">OU<seg phoneme="i" type="vs" value="1" rule="497" place="1" punct="vg">Y</seg></w>, <w n="1.2">l</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> ── <w n="1.11">ch<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="2.4" punct="pv:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pv">er</seg></w> ; <w n="2.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t</w> <w n="2.6">c</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="2.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> ── <w n="2.9" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.4">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> ── <w n="3.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="4" num="1.4" lm="12"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.4">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> ── <w n="4.6" punct="pt:12">C<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">s</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="5.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.8">l</w>’── <w n="5.9" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10">A</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv">e</seg></w> ;</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="6.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> ── <w n="6.8" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4" punct="vg:6">M<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></w>, <w n="7.5">l</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> ── <w n="7.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">Em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="8.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>squ</w>’<w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg></w> ── <w n="8.8" punct="pt:12">R<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="9.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="9.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ── <w n="9.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="3.2" lm="12"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">s</w>’<w n="10.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.6">m<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="10.9">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> ── <w n="10.10" punct="pt:12">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
						<l n="11" num="3.3" lm="12"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="11.4" punct="vg:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>x</w>, <w n="11.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> ── <w n="11.8" punct="vg:12">L<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12"><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.7">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="12.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="12.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> ── <w n="12.10" punct="vg:12">g<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="?" type="va" value="1" rule="162" place="6">en</seg>t</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="13.8">d</w>’<w n="13.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> ── <w n="13.10" punct="vg:12">d<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.3" lm="12"><w n="14.1">S</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="14.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> ── <w n="14.9" punct="pt:12">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>