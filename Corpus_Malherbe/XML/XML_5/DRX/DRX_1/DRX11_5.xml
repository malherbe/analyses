<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX11" modus="cp" lm_max="10">
				<head type="main">L’œil</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.4">tr<seg phoneme="e" type="vs" value="1" rule="383" place="4">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="1.7" punct="vg:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="2" num="1.2" lm="10"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="2.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.7">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="2.8" punct="pv:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="pv">en</seg>d</w> ;</l>
					<l n="3" num="1.3" lm="10"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>cs</w> <w n="3.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="3.6">c<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>s</w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="3.8" punct="vg:10">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="10"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="4.3">fl<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.5">ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>sph<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w></l>
					<l n="5" num="1.5" lm="10"><w n="5.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="5.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="5.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rds</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="5.7">cl<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
					<l n="6" num="1.6" lm="5"><space quantity="12" unit="char"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4" punct="pt:5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="10"><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l</w>’<w n="7.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="7.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.8" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="vg">e</seg>il</w>,</l>
					<l n="8" num="2.2" lm="10"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="8.7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="8.8" punct="pv:10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv">e</seg>s</w> ;</l>
					<l n="9" num="2.3" lm="10"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="9.4">v<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>fs</w> <w n="9.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="9.6">br<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w></l>
					<l n="10" num="2.4" lm="10"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8">e</seg>nt</w> <w n="10.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="10.7" punct="vg:10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="11" num="2.5" lm="10"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="11.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="11.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="11.9" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="353" place="9">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rt</w>,</l>
					<l n="12" num="2.6" lm="5"><space quantity="12" unit="char"></space><w n="12.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">s</w>’<w n="12.4" punct="pe:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="pe">o</seg>rt</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="10"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="13.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9">e</seg>s</w> <w n="13.8">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg>s</w></l>
					<l n="14" num="3.2" lm="10"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ts</w> <w n="14.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="14.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="14.7" punct="pt:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>fs</w>.</l>
					<l n="15" num="3.3" lm="10"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="15.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ts</w> <w n="15.4" punct="vg:5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg>s</w>, <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="7">e</seg>ds</w> <w n="15.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="15.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="15.9" punct="vg:10">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="3.4" lm="10"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg">eau</seg>x</w>, <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">aim</seg>s</w> <w n="16.7" punct="vg:10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>fs</w>,</l>
					<l n="17" num="3.5" lm="10"><w n="17.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="17.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="17.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>cs</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="17.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="17.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="17.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg>s</w>,</l>
					<l n="18" num="3.6" lm="5"><space quantity="12" unit="char"></space><w n="18.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="18.2" punct="pt:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1" lm="10"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="19.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="19.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="19.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="19.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w></l>
					<l n="20" num="4.2" lm="10"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="20.5">d</w>’<w n="20.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="20.7">br<seg phoneme="u" type="vs" value="1" rule="427" place="7">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</w> <w n="20.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11">e</seg></w></l>
					<l n="21" num="4.3" lm="10"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="21.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="21.5">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="21.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</w> <w n="21.8">d</w>’<w n="21.9" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pv">ain</seg></w> ;</l>
					<l n="22" num="4.4" lm="10"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="22.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>g</w>, <w n="22.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="22.6" punct="vg:10">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>lg<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg">e</seg></w>,</l>
					<l n="23" num="4.5" lm="10"><w n="23.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="23.4" punct="vg:5"><seg phoneme="œ" type="vs" value="1" rule="286" place="5" punct="vg">œ</seg>il</w>, <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="23.6" punct="pe:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="23.7" punct="pe:10">H<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</w> !</l>
					<l n="24" num="4.6" lm="5"><space quantity="12" unit="char"></space><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="24.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="24.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="24.5" punct="pt:5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt">a</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>