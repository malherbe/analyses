<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB19" modus="cp" lm_max="12">
					<head type="main">PUDENTIANE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="1.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.3" punct="pt:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></w>. <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">On</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="1.6" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><space quantity="8" unit="char"></space><hi rend="ital"><w n="2.1" punct="pt:8">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sci<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</w></hi>.</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">On</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">d</w>’<w n="3.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.6">d</w>’<w n="3.7" punct="dp:8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="dp">e</seg></w> : <hi rend="ital"><w n="3.8">l<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>x<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></hi></l>
						<l n="4" num="1.4" lm="8"><space quantity="8" unit="char"></space><hi rend="ital"><w n="4.1" punct="pe:8">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pe ps">en</seg>t</w></hi> !…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><hi rend="ital"><w n="5.1" punct="ps:3"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="ps">ai</seg>r</w></hi> … <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">On</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="5.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="5.10" punct="pt:12">c<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg></w>.</l>
						<l n="6" num="2.2" lm="8"><space quantity="8" unit="char"></space><hi rend="ital"><w n="6.1" punct="dp:8">S<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>f</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="tc:5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="ti">i</seg></w> — <w n="6.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="dp">en</seg>t</w></hi> :</l>
						<l n="7" num="2.3" lm="12"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.4" punct="ps:6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="ps">ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> … <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="7.8">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="11">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
						<l n="8" num="2.4" lm="8"><space quantity="8" unit="char"></space><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2" punct="dp:2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="dp">ai</seg>t</w> : <hi rend="ital"><w n="8.3" punct="pt:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</w></hi>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10"><space quantity="4" unit="char"></space>… <w n="9.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4" punct="pt:5">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5" punct="pt ti">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. — <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">On</seg></w> <w n="9.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.9" punct="tc:10">d<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ti">e</seg></w> —</l>
						<l n="10" num="3.2" lm="10"><space quantity="4" unit="char"></space><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="10.4" punct="tc:5">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="ti">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> — <w n="10.5" punct="tc:7"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="ti">e</seg>rt</w> — <w n="10.6">qu</w>’<w n="10.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="10.8" punct="pt:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt">e</seg></w>.</l>
						<l n="11" num="3.3" lm="10"><space quantity="4" unit="char"></space><w n="11.1">B<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="11.6" punct="pe:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe">é</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="10"><space quantity="4" unit="char"></space><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l</w>’<w n="12.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="12.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg">eu</seg>rs</w>, <w n="12.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.7">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>q</w> <w n="12.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.9" punct="ps:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg></w>…</l>
						<l n="13" num="4.2" lm="10"><space quantity="4" unit="char"></space><w n="13.1">C<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg></w>-<w n="13.2" punct="pe:2">G<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe">I</seg>T</w> ! <w n="13.3">L<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <hi rend="ital"><w n="13.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w>-<w n="13.5">d</w>’-<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t</w></hi> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="13.8" punct="ps:10">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps">e</seg></w>…</l>
						<l n="14" num="4.3" lm="10"><space quantity="4" unit="char"></space><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.4" punct="po:4">P<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> (<w n="14.5" punct="pf:5">cu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>) <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="14.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.9" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Rome</placeName> — 40 ans. — 16 août.
						</dateline>
					</closer>
				</div></body></text></TEI>