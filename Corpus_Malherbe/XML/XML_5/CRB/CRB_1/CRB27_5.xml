<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB27" modus="sm" lm_max="8">
					<head type="main">LA PIPE AU POÈTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.4">P<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="1.7" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3" punct="dp:4"><seg phoneme="e" type="vs" value="1" rule="189" place="4" punct="dp">e</seg>t</w> : <w n="2.4">j</w>’<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <hi rend="ital"><w n="2.6" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.7">B<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w></hi>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="8"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">ch<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rgn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="4" num="2.2" lm="8"><w n="4.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="4.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.6" punct="vg:8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>t</w>,</l>
						<l n="5" num="2.3" lm="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="ps:2">f<seg phoneme="y" type="vs" value="1" rule="453" place="2" punct="ps">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">E</seg>t</w> <w n="5.4" punct="vg:4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="5.7" punct="vg:8">pl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>d</w>,</l>
						<l n="6" num="2.4" lm="8"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="8">… <w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="7.5" punct="vg:5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="7.7" punct="vg:8">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="8" num="3.2" lm="8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>r</w>, <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg">e</seg>rt</w>, <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.6" punct="pv:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="9" num="3.3" lm="8">— <w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="7">œ</seg>il</w> <w n="9.7" punct="ps:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="ps">o</seg>rt</w>…</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1" lm="8"><w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="10.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.6" punct="vg:8">n<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="11" num="4.2" lm="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="12" num="4.3" lm="8">— <w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3">en</seg>s</w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.5">t<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5">u</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="12.6">qu</w>’<w n="12.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="12.8" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rd</w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="8">— <w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="14" num="5.2" lm="8"><w n="14.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="14.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="14.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.4" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg></w>, <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="pe:8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="15" num="5.3" lm="8">… <w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="15.5">m</w>’<w n="15.6" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="pt ti">ein</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. — <w n="15.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">I</seg>l</w> <w n="15.8" punct="tc:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="ti">o</seg>rt</w> —</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="6">
						<l n="16" num="6.1" lm="8">— <w n="16.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="16.2" punct="dp:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="dp">o</seg>r</w> : <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <hi rend="ital"><w n="16.4">B<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w></hi> <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.6" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="17" num="6.2" lm="8"><w n="17.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="17.6" punct="ps:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="ps">ou</seg>t</w>…</l>
						<l n="18" num="6.3" lm="8"><w n="18.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2" punct="pe:3">P<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ps">e</seg></w> !… <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4">f<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="18.6" punct="pt:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>t</w>.</l>
						<l n="19" num="6.4" lm="8">— <w n="19.1">S</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="19.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="19.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="19.8" punct="ps:8">f<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName> — Janvier.
						</dateline>
					</closer>
				</div></body></text></TEI>