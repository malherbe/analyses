<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRB1" modus="sp" lm_max="7">
				<head type="main">À MARCELLE</head>
				<head type="sub">LE POÈTE ET LA CIGALE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7"><hi rend="ital"><w n="1.1" punct="vg:7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="1.4">r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w></hi>,</l>
					<l n="2" num="1.2" lm="3"><hi rend="ital"><w n="2.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">IM</seg>PR<seg phoneme="i" type="vs" value="1" rule="467" place="2">I</seg>M<seg phoneme="e" type="vs" value="1" rule="409" place="3">É</seg></w></hi></l>
					<l n="3" num="1.3" lm="7"><hi rend="ital"><w n="3.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></hi></l>
					<l n="4" num="1.4" lm="7"><hi rend="ital"><w n="4.1" punct="dp:7">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:3">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" punct="vg">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5">n<seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></w></hi> :</l>
					<l n="5" num="1.5" lm="7"><hi rend="ital"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="5.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="5.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w></hi></l>
					<l n="6" num="1.6" lm="7"><hi rend="ital"><w n="6.1" punct="pt:7">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="ps:2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="ps">e</seg>rs</w> … <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="pt">eau</seg></w></hi>.</l>
					<l n="7" num="1.7" lm="7"><hi rend="ital"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.3">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="7.4">f<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></hi></l>
					<l n="8" num="1.8" lm="7"><hi rend="ital"><w n="8.1" punct="vg:7">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w></hi>,</l>
					<l n="9" num="1.9" lm="7"><hi rend="ital"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="9.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w></hi></l>
					<l n="10" num="1.10" lm="7"><hi rend="ital"><w n="10.1" punct="pt:7">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="10.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4">om</seg></w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="10.5">r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></w></hi>.</l>
					<l n="11" num="1.11" lm="7">(<hi rend="ital"><w n="11.1" punct="pf:7">C</w>’<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></hi>)</l>
					<l n="12" num="1.12" lm="7">— <hi rend="ital"><w n="12.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="12.4" punct="vg:5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>, <w n="12.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w></hi>,</l>
					<l n="13" num="1.13" lm="7"><hi rend="ital"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="13.2">l</w>’<w n="13.3" punct="vg:3"><seg phoneme="u" type="vs" value="1" rule="292" place="3" punct="vg">aoû</seg>t</w>, <w n="13.4">f<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="13.5">d</w>’<w n="13.6" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg>l</w> !</hi></l>
					<l n="14" num="1.14" lm="7"><hi rend="ital"><w n="14.1" punct="pt:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt ti">a</seg>l</w></hi>. —</l>
					<l n="15" num="1.15" lm="7"><hi rend="ital"><w n="15.1" punct="vg:7">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="15.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="15.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w></hi>,</l>
					<l n="16" num="1.16" lm="7"><hi rend="ital"><w n="16.1" punct="dp:7">C</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="16.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="16.5">j<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="16.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="7" punct="dp">au</seg>t</w></hi> :</l>
					<l n="17" num="1.17" lm="7">— <hi rend="ital"><w n="17.1" punct="dp:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="dp">oi</seg></w> : <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="17.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.6">qu</w>’<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="17.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.9" punct="pi:7">f<seg phoneme="o" type="vs" value="1" rule="318" place="7" punct="pi">au</seg>t</w> ?</hi></l>
					<l n="18" num="1.18" lm="7"><hi rend="ital"><w n="18.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">M<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="18.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="18.5" punct="ps:7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg></w>…</hi></l>
					<l n="19" num="1.19" lm="7"><hi rend="ital"><w n="19.1" punct="vg:7">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="19.3" punct="vg:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="19.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="19.6">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</w></hi>,</l>
					<l n="20" num="1.20" lm="7"><hi rend="ital"><w n="20.1">R<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="20.3" punct="ps:4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4" punct="ps">om</seg></w>… <w n="20.4">Qu</w>’<w n="20.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="20.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="20.7" punct="pe:7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</hi></l>
					<l n="21" num="1.21" lm="7"><hi rend="ital"><w n="21.1" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="21.3">j</w>’<w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="21.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="21.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w></hi>.</l>
				</lg>
				<lg n="2">
					<l n="22" num="2.1" lm="7"><hi rend="ital"><w n="22.1" punct="dp:2">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="dp">on</seg>s</w> : <w n="22.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="22.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pt">an</seg>t</w></hi>.</l>
				</lg>
			</div></body></text></TEI>