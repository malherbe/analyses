<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB23" modus="cm" lm_max="12">
					<head type="main">BONNE FORTUNE ET FORTUNE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Odor della feminita</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12"><w n="1.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.5" punct="vg:6">tr<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>tt<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>, <w n="1.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="1.8">n<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="1.10" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="vg:6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg></w>, <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="2.9" punct="vg:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
						<l n="3" num="1.3" lm="12"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="3.3" punct="vg:6">cr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="3.8" punct="vg:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="4.2">cl<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5">pr<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="4.8">p<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="4.11" punct="ps:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="ps">œu</seg>r</w>…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="5.5" punct="tc:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="?" type="va" value="1" rule="162" place="6" punct="ti">en</seg>t</w> — <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="5.7" punct="pe:8">tr<seg phoneme="o" type="vs" value="1" rule="433" place="8" punct="pe ti">o</seg>p</w> ! — <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="5.9"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="5.10">f<seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>t</w> <w n="5.11" punct="dp:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp">e</seg></w> :</l>
						<l n="6" num="2.2" lm="12"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="6.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.6" punct="vg:8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">aim</seg></w>, <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="6.8">gu<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="6.9">s</w>’<w n="6.10" punct="ps:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="11">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps">e</seg></w>…</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="12"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="7.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="7.3" punct="tc:3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="ti">ou</seg>r</w> — <w n="7.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="7.5" punct="pe:6">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe ti">er</seg></w> ! — <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7" punct="vg:9">f<seg phoneme="œ" type="vs" value="1" rule="304" place="8">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>s</w>, <w n="7.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11">e</seg></w> <w n="7.9" punct="vg:12">ç<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></w>,</l>
						<l n="8" num="3.2" lm="12"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2" punct="pt:4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt ti">e</seg></w>. — <w n="8.3" punct="pe:6">M<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe ps ti">er</seg></w> !… — <w n="8.4" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>, <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10">e</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg></w></l>
						<l n="9" num="3.3" lm="12">— <w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2" punct="pi:3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="pi ti">i</seg></w> ? — <w n="9.3">L<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4" punct="pe:6">P<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pe">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="9.5" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="vg">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="9.8" punct="pe:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">om</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe">e</seg></w> !</l>
						<l n="10" num="3.4" lm="12"><w n="10.1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg></w>, <w n="10.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="10.7" punct="ps:10">fr<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10" punct="ps ti">ai</seg></w> … — <w n="10.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="10.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg></w></l>
					</lg>
					<lg n="4">
						<l n="11" num="4.1" lm="12"><w n="11.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="11.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="8">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="11.7" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
						<l part="I" n="12" num="4.2" lm="12"><w n="12.1" punct="ps:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="ps">E</seg>t</w> … <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.5" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg">ain</seg></w>, <w n="12.6" punct="ps:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="ps">e</seg>t</w> … </l>
						<l part="F" n="12" num="4.2" lm="12"><w n="12.7">m</w>’<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="12.9">d<seg phoneme="o" type="vs" value="1" rule="435" place="9">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="12.10">d<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="12.11" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</w>.</l>
					</lg>
					<closer>
						<placeName>Rue des Martyrs</placeName>.
					</closer>
				</div></body></text></TEI>