<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS17" modus="cm" lm_max="12">
				<head type="main">MALADIES SECRÈTES</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>RQU<seg phoneme="i" type="vs" value="1" rule="491" place="2">I</seg>S</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3" punct="vg:6">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg></w>, <w n="1.4">j</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="1.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>b<seg phoneme="i" type="vs" value="1" rule="493" place="11">y</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="2" num="1.2" lm="12"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="2.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>s</w> <w n="2.8" punct="pt:12">tr<seg phoneme="o" type="vs" value="1" rule="435" place="11">o</seg>tt<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>rs</w>.</l>
					<l n="3" num="1.3" lm="12"><w n="3.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="3.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="3.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg></w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="3.8">br<seg phoneme="u" type="vs" value="1" rule="427" place="10">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>rds</w> <w n="3.9">n<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>rs</w></l>
					<l n="4" num="1.4" lm="12"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.4">B<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8">e</seg>s</w> <w n="4.7" punct="pt:12"><seg phoneme="ø" type="vs" value="1" rule="405" place="9">Eu</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="i" type="vs" value="1" rule="493" place="11">y</seg>ps<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12"><w n="5.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="5.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rs</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9">e</seg></w> <w n="5.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>squ</w>’<w n="5.8"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>x</w> <w n="5.9">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13">e</seg>s</w></l>
					<l n="6" num="2.2" lm="12"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="6.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rts</w> <w n="6.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="6.8" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>rs</w> ;</l>
					<l n="7" num="2.3" lm="12"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">c</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="4">oi</seg></w> <w n="7.5">j</w>’<w n="7.6" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">en</seg>ds</w>, <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="7.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="7.10" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rv<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>rs</w>,</l>
					<l n="8" num="2.4" lm="12"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>z<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>, <w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11">e</seg>s</w> <w n="8.8" punct="pt:12">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>x<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="9.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="9.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg></w></l>
					<l n="10" num="3.2" lm="12"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.5" punct="vg:6">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>il</w>, <w n="10.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="10.8" punct="pe:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>h<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg></w> !</l>
					<l n="11" num="3.3" lm="12"><w n="11.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="11.5" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="11.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="11.7">j</w>’<w n="11.8"><seg phoneme="y" type="vs" value="1" rule="391" place="8">eu</seg>s</w> <w n="11.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9">e</seg>s</w> <w n="11.10">m<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>rs</w> <w n="11.11" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="12.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rs</w> <w n="12.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8">e</seg></w> <w n="12.6">c<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10">e</seg>s</w> <w n="12.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>gn<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</w>.</l>
					<l n="13" num="4.2" lm="12"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="13.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w>-<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="13.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>s</w> <w n="13.10" punct="pt:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="pt">au</seg>x</w>.</l>
					<l n="14" num="4.3" lm="12"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="14.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="8">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="14.6" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt">e</seg>s</w>.</l>
				</lg>
				<closer>
					<note type="footnote" id="">Les <hi rend="ital">Bodegas</hi> sont des établissements de mastroquets exotiques où les vins de Cette se vendent en espagnol (<hi rend="ital">aqui se habla</hi>), — Et l’Eucalypsinthe ! Rêve d’un liquoriste marseillais qui prétendait avec l’essence d’eucalyptus imiter l’absinthe !</note>
				</closer>
			</div></body></text></TEI>