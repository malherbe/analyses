<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS15" modus="sm" lm_max="8">
				<head type="main">AUSCULTATION</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8"><w n="1.1" punct="pe:2">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">O</seg>MM<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2" punct="pe">EN</seg>T</w> ! <w n="1.2">C</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.4" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="1.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6" punct="pi:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pi">o</seg>t</w> ?</l>
					<l n="2" num="1.2" lm="8">— « <w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2" punct="vg:2">ou<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="2.3">mʼsi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="2.4" punct="vg:4">P<seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>l</w>, <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.6">j</w>’<w n="2.7">m</w>’<w n="2.8" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="3" num="1.3" lm="8">« <w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w>’ <w n="3.5">p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v</w>’ <w n="3.6" punct="pe:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="4" num="1.4" lm="8">« <w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="4.3">qu</w>’<w n="4.4">j</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="4.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="4.7">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="5">en</seg></w> <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.9">l</w>’<w n="4.10" punct="pi:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pi">o</seg>t</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8">« <w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="5.2">qu</w>’<w n="5.3">ç<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">sʼr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="5.6">qu<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>qu</w>’ <w n="5.7" punct="pe:8">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pe">o</seg>t</w> !</l>
					<l n="6" num="2.2" lm="8">« <w n="6.1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">m</w>’<w n="6.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="6.5" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="6.6">ç<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.7">m</w>’<w n="6.8">gr<seg phoneme="u" type="vs" value="1" rule="427" place="6">ou</seg>il</w>’ <w n="6.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="6.10">l</w>’<w n="6.11" punct="pe:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>tʼ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="7" num="2.3" lm="8">« <w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="7.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm</w>’ <w n="7.3" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="7.4" punct="pv:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pv">oi</seg></w> ; <w n="7.5">j</w>’<w n="7.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="7.8" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="8" num="2.4" lm="8">«<w n="8.1">Pʼt</w>-<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>t</w> <w n="8.3">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="2">en</seg></w> <w n="8.4">qu</w>’<w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="8.6">m</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="8.9">l</w>’<w n="8.10">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w> <w n="8.11" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>t</w>, »</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8">— « … <w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="9.2" punct="pe:2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg>c</w> ! <w n="9.3">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="9.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.6" punct="pe:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></w> !… »</l>
					<l n="10" num="3.2" lm="8"><w n="10.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="11" num="3.3" lm="8"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="11.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>s</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rs</w> <w n="11.6">qu</w>’<w n="11.7" punct="pv:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pv">ain</seg>s</w> ;</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="8"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="12.4">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.5" punct="vg:8">t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="13" num="4.2" lm="8"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w></l>
					<l n="14" num="4.3" lm="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="14.4" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>