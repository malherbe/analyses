<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV38" modus="cm" lm_max="12" metProfile="6=6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="0.0" er_max="0" er_min="0" er_mode="0(7/7)" er_moy_et="0.0" qr_moy="1.43" qr_max="N5" qr_mode="0(5/7)" qr_moy_et="2.26">
				<head type="main">FÉVRIER</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="1.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="1.8">F<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" mp6="C" met="6−6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="2.3" punct="ps:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="ps">ou</seg>r</w>… <w n="2.4">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="P">a</seg>r</w> <w n="2.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="2.6">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="2.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="2.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="2.11">ti<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="3.4" punct="ps:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="ps" caesura="1">u</seg>s</w>…<caesura></caesura> <w n="3.5">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.7">v<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg></w> <w n="3.8">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" mp6="M" mp8="F" met="4+8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="4.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="4.7" punct="pe:12">m<seg phoneme="u" type="vs" value="1" rule="428" place="11" mp="M">ou</seg>ill<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.3">d</w>’<w n="5.4" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="dc-2" place="3" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>st</w>, <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l</w><caesura></caesura> <w n="5.7" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</w>, <w n="5.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.10">p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ti<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="12" mp6="Lp" met="6−6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d</w>’<w n="6.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" mp="Lp" caesura="1">en</seg>s</w><caesura></caesura>-<w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.7">m</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.10"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="7" num="2.3" lm="12" mp6="M" met="4+4+4"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lgu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.7">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" caesura="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w><caesura></caesura> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.9">t</w>’<w n="7.10" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>bs<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</w>, <w n="8.4">t</w>’<w n="8.5" punct="ps:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="5" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="ps" caesura="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>…<caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="8.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="8.10" punct="pe:12">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" qr="N5"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">er</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="9.2">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="9.4" punct="pe:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe" caesura="1">u</seg>s</w> !<caesura></caesura> <w n="9.5">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="9.10">g<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="10.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.6" punct="ps:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="ps">oi</seg></w>… <w n="10.7" punct="vg:9">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>s</w>, <w n="10.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="Lp">i</seg>s</w>-<w n="10.9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="11">oi</seg></w> « <w n="10.10" punct="pe:12">R<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !»</l>
						<l n="11" num="3.3" lm="12" mp6="M" mp8="M" met="4+8">…<w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="11.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.9" punct="vg:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt</w> <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="12.8">f<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="12.9" punct="ps:12">di<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="ps:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="ps">i</seg>s</w>… <w n="13.2" punct="ps:2">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="ps">o</seg>l</w>… <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="13.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="13.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="13.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="13.11" punct="ps:12">y<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="ps">eu</seg>x</rhyme></pgtc></w>…</l>
						<l n="14" num="4.3" lm="12" mp6="M" mp4="M" met="8+4"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="14.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" caesura="1">on</seg></w><caesura></caesura> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="14.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="14.6" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date to="1922" from="1921">Mars 1921 ‒ Février 1922</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>