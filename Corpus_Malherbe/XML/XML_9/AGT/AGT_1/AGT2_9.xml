<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="AGT">
					<name>
						<forename>Marie</forename>
						<nameLink>d’</nameLink>
						<surname>AGOULT</surname>
						<addName type="pen_name">Daniel STERN</addName>
					</name>
					<date from="1805" to="1876">1805-1876</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>185 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AGT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
						<author>Daniel STERN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://catalogue.bnf.fr/ark:/12148/cb30005117k</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
							<author>Daniel STERN</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>CALMANN LÉVY, ÉDITEUR</publisher>
									<date when=" 1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1880">1880</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne porte que sur le chapitre consacrée aux poésies.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES</head><div type="poem" key="AGT2" modus="cp" lm_max="12" metProfile="6, 6+6" form="suite périodique" schema="3(abab)" er_moy="1.33" er_max="6" er_min="0" er_mode="0(4/6)" er_moy_et="2.21" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
					<head type="number">II</head>
					<head type="main">L’ADIEU</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="1.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.9">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <w n="1.10" punct="vg:12">fi<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="2.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.6" punct="vg:9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" punct="vg">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="2.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>gr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l</w> <w n="3.2" punct="vg:3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="3.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l</w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rds</w><caesura></caesura> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8">l<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="4.4" punct="pt:6">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="5.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w> <w n="5.9">bru<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="5.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="10" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">vr<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">d</w>’<w n="6.5">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.7" punct="vg:12">d<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="C">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="7.6">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" mp="M">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="7.7">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.9">f<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg></w> <w n="7.10">j<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.4" punct="pt:6">ch<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.9" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>d<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt</w> <w n="10.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.8" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>r</rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.4">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.9">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">l</w>’<w n="12.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>