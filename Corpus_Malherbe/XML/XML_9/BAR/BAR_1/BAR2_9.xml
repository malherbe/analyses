<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR2" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="5[abab]" er_moy="1.5" er_max="8" er_min="0" er_mode="0(6/10)" er_moy_et="2.42" qr_moy="0.0" qr_max="C0" qr_mode="0(10/10)" qr_moy_et="0.0">
				<lg n="1" type="regexp" rhyme="abababababababab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M/mp">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M/mp">ou</seg>cl<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="1.2" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" punct="vg" caesura="1">e</seg>s</w>,<caesura></caesura> <w n="1.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="1.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>gs</w> <w n="1.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.7" punct="vg:10">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="2.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg>s</w><caesura></caesura> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="2.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>rs</w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="2.7">d</w>’<w n="2.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>nn<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="3.2" punct="vg:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="3.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="3.7">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="4.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gs</w> <w n="4.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="4.4">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" caesura="1">un</seg>s</w><caesura></caesura> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="4.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="4.8">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="4.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="4.10" punct="pe:10">b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pe">eau</seg>x</rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M/mp">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.3" punct="vg:4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="5.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>g<seg phoneme="a" type="vs" value="1" rule="365" place="3" mp="M">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="6.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6" mp="C">o</seg>s</w> <w n="7.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="7.6" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="8.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="8.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>f<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>s</w><caesura></caesura> <w n="9.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg><pgtc id="5" weight="2" schema="CR">tt<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</w> <w n="10.9">s</w>’<w n="10.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="10.11" punct="vg:10">v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>t</rhyme></pgtc></w>,</l>
					<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="11.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>s<pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="10" met="4+6"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="12.3"><seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="12.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.8" punct="pe:10">fr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pe">on</seg>t</rhyme></pgtc></w> !</l>
					<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="13.2">c</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rs</w><caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="13.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="13.8"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>g<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="14.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.7">l</w>’<w n="14.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg><pgtc id="8" weight="8" schema="CVCR">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="b" id="8" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="15" num="1.15" lm="10" met="4+6"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="15.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="15.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="15.9" punct="ps:10">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>tr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
					<l n="16" num="1.16" lm="10" met="4+6"><w n="16.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="16.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="16.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">î</seg>t</w><caesura></caesura> <w n="16.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="16.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="16.7" punct="pe:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg><pgtc id="8" weight="8" schema="CVCR">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="b" id="8" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="regexp" rhyme="abab">
					<l n="17" num="2.1" lm="10" met="4+6"><w n="17.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="17.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">em</seg>ps</w><caesura></caesura> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M/mp">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="6" mp="Lp">ez</seg></w>-<w n="17.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="17.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.8">ch<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="2.2" lm="10" met="4+6"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="18.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="18.3">c<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="18.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs</w> <w n="18.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="18.7" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="10" weight="3" schema="GR">ti<rhyme label="b" id="10" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pv">ez</seg></rhyme></pgtc></w> ;</l>
					<l n="19" num="2.3" lm="10" met="4+6"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="19.4">v<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="19.5" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></w>, <w n="19.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="19.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>p<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="2.4" lm="10" met="4+6"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="20.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="20.3">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>s</w><caesura></caesura> <w n="20.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="20.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="20.8">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="20.9" punct="ps:10">m<seg phoneme="u" type="vs" value="1" rule="428" place="9" mp="M">ou</seg><pgtc id="10" weight="3" schema="GR">ill<rhyme label="b" id="10" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="ps">é</seg>s</rhyme></pgtc></w>…</l>
				</lg>
			</div></body></text></TEI>