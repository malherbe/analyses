<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU62" modus="cm" lm_max="12" metProfile="6+6" form="sonnet non classique" schema="abab cdcd efe fgg" er_moy="2.29" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="2.49" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="number">LVIII</head>
					<head type="main">Causerie</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="1.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="1.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="1.6">d</w>’<w n="1.7" punct="vg:9"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="1.8">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.10" punct="pe:12"><pgtc id="1" weight="2" schema="[CR">r<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="2.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="2.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8">l<pgtc id="2" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="2.9" punct="vg:12"><pgtc id="2" weight="6" schema="V[CR" part="2">m<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2" punct="vg:2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>fl<seg phoneme="y" type="vs" value="1" rule="454" place="5" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="3.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR" part="1">r<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="4.3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.6">l<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="4.7" punct="pt:12"><pgtc id="2" weight="6" schema="[VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="cdcd">
						<l n="5" num="2.1" lm="12" met="6+6">— <w n="5.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">gl<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="5.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.9">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="5.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="5.11">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="5.12" punct="pv:12">p<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="6.4" punct="vg:4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="vg" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="6.8">li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="6.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="4" weight="2" schema="CR" part="1">g<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="7.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.7">f<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.10" punct="pt:12">f<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="8.5" punct="pv:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="pv" caesura="1">œu</seg>r</w> ;<caesura></caesura> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.7">b<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.8">l</w>’<w n="8.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="8.10" punct="pt:12">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg><pgtc id="4" weight="2" schema="CR" part="1">g<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="efe">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="9.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.6">fl<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="P">a</seg>r</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9" punct="pv:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>h<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="e" id="5" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="10.4" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">oû</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="10.6">s</w>’<w n="10.7"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="10.8" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="457" place="6" punct="vg" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>,<caesura></caesura> <w n="10.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="10.10">s</w>’<w n="10.11"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="10.12">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="10.13"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="10.14" punct="pe:12">ch<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="f" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
						<l n="11" num="3.3" lm="12" met="6+6">— <w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg></w> <w n="11.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="11.7">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.8" punct="pe:12">n<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="e" id="5" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></pgtc></w> !…</l>
					</lg>
					<lg n="4" rhyme="fgg">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="12.2" punct="vg:3">B<seg phoneme="o" type="vs" value="1" rule="315" place="2" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="12.4">fl<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg></w><caesura></caesura> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.6" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="12.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="12.8">l<pgtc id="6" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></pgtc></w> <w n="12.9" punct="pe:12"><pgtc id="6" weight="6" schema="V[CR" part="2">v<rhyme label="f" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="13.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="13.5" punct="vg:6">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="13.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w> <w n="13.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="13.9" punct="vg:12">f<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="g" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>lc<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="14.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="14.4">qu</w>’<w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rgn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.8" punct="pe:12">b<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="g" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>