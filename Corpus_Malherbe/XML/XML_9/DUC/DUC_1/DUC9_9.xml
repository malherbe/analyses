<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC9" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="1(ababccd) 1(abab)" er_moy="0.33" er_max="2" er_min="0" er_mode="0(5/6)" er_moy_et="0.75" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="main">A MADAME HÉBERT-MASSY</head>
				<head type="sub_1">de l’Opéra-Comique</head>
				<lg n="1" type="septain" rhyme="ababccd">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.5" punct="vg:4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" punct="vg">e</seg></w>, <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w> <w n="1.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="1.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="1.11" punct="vg:12">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="2.2" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="2.3" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="vg" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="2.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="2.9">pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6">(<w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3" punct="pf:6">r<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>ls</w><caesura></caesura>) <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="3.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="3.6" punct="vg:12">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>x</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="4.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="5.5" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="6.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="6.5" punct="vg:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="6.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="6.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>r<pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="7.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="7.4">t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7" punct="pt:8">gl<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="8" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2" punct="vg:3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">cr<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="9" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="9.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="9.8" punct="vg:8">d<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="10" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="10.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="10.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>s</w> <w n="10.5">l</w>’<w n="10.6">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="11" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">D</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="11.3">r<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nîmes</placeName>,
						<date when="1855">novembre 1855</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>