<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC5" modus="sp" lm_max="6" metProfile="6, 4" form="suite périodique" schema="3(ababcdcdee)" er_moy="1.07" er_max="8" er_min="0" er_mode="0(10/15)" er_moy_et="2.05" qr_moy="0.0" qr_max="C0" qr_mode="0(15/15)" qr_moy_et="0.0">
				<head type="main">MON RÊVE D’OR</head>
				<head type="form">ROMANCE</head>
				<opener>
					<salute>A Mademoiselle E. H.</salute>
				</opener>
				<lg n="1" type="dizain" rhyme="ababcdcdee">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="1.3">l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rm<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">fl<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="4.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.6" punct="vg:6">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="6" met="6"><w n="5.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="5.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>r</w>, <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="5.5" punct="pv:6"><pgtc id="3" weight="2" schema="[CR">r<rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="6" num="1.6" lm="6" met="6"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5" punct="vg:6"><pgtc id="4" weight="2" schema="[CR">fl<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="6" met="6"><w n="7.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="7.4"><pgtc id="3" weight="2" schema="[CR">tr<rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="6" met="6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.4" punct="pt:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="4" met="4"><space unit="char" quantity="4"></space><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="9.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="4" met="4"><space unit="char" quantity="4"></space><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d</w>’<w n="10.4" punct="pe:4"><pgtc id="5" weight="0" schema="[R"><rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="dizain" rhyme="ababcdcdee">
					<l n="11" num="2.1" lm="6" met="6"><w n="11.1" punct="vg:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="vg">en</seg>s</w>, <w n="11.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4" punct="vg:6">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="2.2" lm="6" met="6"><w n="12.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.4" punct="vg:6">Di<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="2.3" lm="6" met="6"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="13.6" punct="vg:6"><pgtc id="6" weight="0" schema="[R"><rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="2.4" lm="6" met="6"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5" punct="pt:6">f<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
					<l n="15" num="2.5" lm="6" met="6"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">bl<seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg><pgtc id="8" weight="2" schema="CR">ss<rhyme label="c" id="8" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="2.6" lm="6" met="6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="16.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="16.5" punct="vg:6">j<pgtc id="9" weight="0" schema="R"><rhyme label="d" id="9" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="17" num="2.7" lm="6" met="6"><w n="17.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="17.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg><pgtc id="8" weight="2" schema="CR">s<rhyme label="c" id="8" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="2.8" lm="6" met="6"><w n="18.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="18.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="18.4">d</w>’<w n="18.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="9" weight="0" schema="R"><rhyme label="d" id="9" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					<l n="19" num="2.9" lm="4" met="4"><space unit="char" quantity="4"></space><w n="19.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="19.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="19.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<pgtc id="10" weight="0" schema="R"><rhyme label="e" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
					<l n="20" num="2.10" lm="4" met="4"><space unit="char" quantity="4"></space><w n="20.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="20.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">d</w>’<w n="20.4" punct="pe:4"><pgtc id="10" weight="0" schema="[R"><rhyme label="e" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="dizain" rhyme="ababcdcdee">
					<l n="21" num="3.1" lm="6" met="6"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="21.4" punct="vg:6">b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="22" num="3.2" lm="6" met="6"><w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="22.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="22.4" punct="vg:6"><pgtc id="12" weight="8" schema="[CVCR">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="3.3" lm="6" met="6"><w n="23.1" punct="vg:1">Gu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.4" punct="vg:6">v<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="3.4" lm="6" met="6"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="24.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="24.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg><pgtc id="12" weight="8" schema="CVCR">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe">é</seg></rhyme></pgtc></w> !</l>
					<l n="25" num="3.5" lm="6" met="6"><w n="25.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="25.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="25.5" punct="vg:6"><pgtc id="13" weight="2" schema="[CR">ch<rhyme label="c" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="3.6" lm="6" met="6"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="26.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.4" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<pgtc id="14" weight="0" schema="R"><rhyme label="d" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pt">e</seg>il</rhyme></pgtc></w>.</l>
					<l n="27" num="3.7" lm="6" met="6"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="27.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="27.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="27.4">m</w>’<w n="27.5" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg><pgtc id="13" weight="2" schema="CR">ch<rhyme label="c" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="28" num="3.8" lm="6" met="6"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="28.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r</w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="28.4" punct="pi:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<pgtc id="14" weight="0" schema="R"><rhyme label="d" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pi ps">e</seg>il</rhyme></pgtc></w> ?…</l>
					<l n="29" num="3.9" lm="4" met="4"><space unit="char" quantity="4"></space><w n="29.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="29.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="29.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<pgtc id="15" weight="0" schema="R"><rhyme label="e" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
					<l n="30" num="3.10" lm="4" met="4"><space unit="char" quantity="4"></space><w n="30.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="30.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">d</w>’<w n="30.4" punct="pe:4"><pgtc id="15" weight="0" schema="[R"><rhyme label="e" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Annecy (Savoie)</placeName>,
						<date when="1852">juillet 1852</date>.
						</dateline>
				</closer>
			</div></body></text></TEI>