<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV8" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="6(aa)" er_moy="3.17" er_max="8" er_min="0" er_mode="0(2/6)" er_moy_et="2.97" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="main">Madrigal</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="1.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<pgtc id="1" weight="8" schema="CVCR">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>t<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6" punct="pt:7"><pgtc id="1" weight="8" schema="CVCR">c<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="distique" rhyme="aa">
					<l n="3" num="2.1" lm="7" met="7"><w n="3.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ffl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.5">b<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="2.2" lm="7" met="7"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2" punct="pt:3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="pt">oi</seg>s</w>. <w n="4.3">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w>-<w n="4.5" punct="pi:6">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6" punct="pi">e</seg></w> ? <w n="4.6" punct="pt:7">Qu<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="distique" rhyme="aa">
					<l n="5" num="3.1" lm="7" met="7"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.8" punct="vg:7"><pgtc id="3" weight="3" schema="[CGR">ri<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="3.2" lm="7" met="7"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ct<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.3" punct="vg:7">v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg><pgtc id="3" weight="3" schema="CGR">ri<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" type="distique" rhyme="aa">
					<l n="7" num="4.1" lm="7" met="7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="4.2" lm="7" met="7"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="8.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="8.5" punct="pt:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="distique" rhyme="aa">
					<l n="9" num="5.1" lm="7" met="7"><w n="9.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="9.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="9.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="5.2" lm="7" met="7"><w n="10.1" punct="vg:2">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="10.4" punct="vg:7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="6" type="distique" rhyme="aa">
					<l n="11" num="6.1" lm="7" met="7"><w n="11.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="11.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="11.6">t</w>’<w n="11.7" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>l<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="6.2" lm="7" met="7"><w n="12.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.3" punct="pt:7">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>