<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP67" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="4(abab)" er_moy="0.88" er_max="2" er_min="0" er_mode="0(4/8)" er_moy_et="0.93" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="main">Aubade</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.7" punct="pt:7"><pgtc id="1" weight="2" schema="[CR">n<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5" punct="pv:7">n<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="7" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2">f<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="4.4">m</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.6" punct="pt:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="7" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="7" met="7"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>d</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6" punct="pv:7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>gu<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">fr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="7.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>th</w> <w n="7.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="7" met="7"><w n="8.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.5" punct="pt:7">c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="9.5" punct="vg:7"><pgtc id="5" weight="2" schema="[CR">p<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="7" met="7"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="10.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="10.6">s<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>r</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg><pgtc id="5" weight="2" schema="CR">p<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="7" met="7"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6" punct="pv:7">m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pv">u</seg>r</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5"><pgtc id="7" weight="2" schema="CR">d<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="7" met="7"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2">m</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.4" punct="vg:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>d<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5" punct="vg:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>r<pgtc id="7" weight="2" schema="CR">d<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="7" met="7"><w n="16.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="16.4" punct="pt:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>