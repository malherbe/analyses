<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP21" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="1.57" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.99" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="main">Décembre</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">H<seg phoneme="y" type="vs" value="1" rule="450" place="1" punct="vg">u</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="2.3">D<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.5" punct="pv:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>r</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="4.4">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.7" punct="pt:8"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.8" punct="vg:8">n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="6.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.5" punct="pi:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg>r</rhyme></pgtc></w> ?</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="7.2" punct="vg:4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>t</w>, <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.4">l</w>’<w n="7.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.3" punct="pv:4">p<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pv">u</seg>rs</w> ; <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></w>, <w n="8.6" punct="pi:8">s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.5">s</w>’<w n="9.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.6" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6" punct="pe:8">r<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="12.4">s</w>’<w n="12.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="12.7" punct="pv:8">b<pgtc id="7" weight="1" schema="GR">i<rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="13.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.6" punct="vg:8">r<pgtc id="7" weight="1" schema="GR">i<rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="14.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="14.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7" punct="pt:8">ch<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>