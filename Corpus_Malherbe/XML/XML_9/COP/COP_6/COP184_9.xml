<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP184" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)" er_moy="0.36" er_max="2" er_min="0" er_mode="0(11/14)" er_moy_et="0.72" qr_moy="0.0" qr_max="C0" qr_mode="0(14/14)" qr_moy_et="0.0">
				<head type="main">En écoutant l’Orgue</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:4">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="1.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="1.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.6" punct="vg:8">c<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>br<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4" punct="pt:5">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pt">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>l</w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.4">t<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p</w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.5">s</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.8" punct="vg:8"><pgtc id="3" weight="2" schema="[CR">pr<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg></w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.6" punct="pt:8">ci<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.4">t<seg phoneme="ɥi" type="vs" value="1" rule="462" place="4">u</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.5">tr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.5">fr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="12.6" punct="pt:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>str<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="13.2" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>xtr<seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="7" weight="2" schema="CR">n<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="14.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="14.7" punct="vg:8">br<pgtc id="8" weight="1" schema="GR">u<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>ts</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w> <w n="15.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="15.5">t<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">nn<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="16.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="16.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.7" punct="pt:8">n<pgtc id="8" weight="1" schema="GR">u<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>ts</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="17.5">d</w>’<w n="17.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">S</w>’<w n="18.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="18.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w> <w n="18.7" punct="vg:8">v<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>x</rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.5">V<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ct<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.6" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>tb<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2" punct="pe:2">ch<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="pe ps">u</seg>t</w> !… <w n="21.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5">ps<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.4">d</w>’<w n="22.5" punct="vg:8"><pgtc id="12" weight="0" schema="[R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="23.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3" punct="ps:3">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="ps">ai</seg>t</w>… <w n="23.4">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>ts</w> <w n="23.6">d</w>’<w n="23.7">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L</w>’<w n="24.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rgu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">l</w>’<w n="24.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="25.6">n<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>f</w> <w n="25.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="26.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3" punct="vg:4">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="26.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="26.6" punct="dp:8">p<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="dp">eu</seg></rhyme></pgtc></w> :</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">C</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.4">V<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="27.6">c</w>’<w n="27.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="27.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="27.9">N<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="28.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="28.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="28.5" punct="pe:8">Di<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>