<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU28" modus="sm" lm_max="5" metProfile="5" form="suite périodique avec vers clausules" schema="2(ababc) 2(c) 1(abab) 1(aa)" er_moy="1.44" er_max="6" er_min="0" er_mode="0(4/9)" er_moy_et="1.83" qr_moy="0.0" qr_max="C0" qr_mode="0(9/9)" qr_moy_et="0.0">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Endymion</head>
						<lg n="1" type="quintil" rhyme="ababc">
							<l n="1" num="1.1" lm="5" met="5"><w n="1.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="1.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.4" punct="vg:5">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="5" met="5"><w n="2.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.2" punct="ps:5"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="2" weight="6" schema="CVR">bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="ps">e</seg></rhyme></pgtc></w>…</l>
							<l n="3" num="1.3" lm="5" met="5"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="5" met="5"><w n="4.1" punct="vg:2">M<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.3"><pgtc id="2" weight="6" schema="[C[VR" part="1">l</pgtc></w>’<w n="4.4" punct="vg:5"><pgtc id="2" weight="6" schema="[C[VR" part="2"><seg phoneme="i" type="vs" value="1" rule="d-4" place="4">y</seg><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="5" num="1.5" lm="5" met="5"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg><pgtc id="3" weight="2" schema="C[R" part="1">s</pgtc></w> <w n="5.4" punct="ps:5"><pgtc id="3" weight="2" schema="C[R" part="2"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="ps">eau</seg>x</rhyme></pgtc></w>…</l>
						</lg>
						<lg n="2" type="vers clausule" rhyme="c">
							<l n="6" num="2.1" lm="5" met="5"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4" punct="ps:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="3" weight="2" schema="CR" part="1">s<rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="ps">eau</seg>x</rhyme></pgtc></w>…</l>
						</lg>
						<lg n="3" type="quintil" rhyme="ababc">
							<l n="7" num="3.1" lm="5" met="5"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.3">c<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
							<l n="8" num="3.2" lm="5" met="5"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3" punct="ps:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="ps">e</seg>s</rhyme></pgtc></w>…</l>
							<l n="9" num="3.3" lm="5" met="5"><w n="9.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">b<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
							<l n="10" num="3.4" lm="5" met="5"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">pl<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></pgtc></w></l>
							<l n="11" num="3.5" lm="5" met="5"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3" punct="pe:5"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="c" id="6" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="pe">eau</seg>x</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="4" type="vers clausule" rhyme="c">
							<l n="12" num="4.1" lm="5" met="5"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="12.4" punct="ps:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="c" id="6" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="ps">eau</seg>x</rhyme></pgtc></w>…</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abab">
							<l n="13" num="5.1" lm="5" met="5"><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l</w>’<w n="13.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="5.2" lm="5" met="5"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4" punct="vg:5">n<pgtc id="8" weight="1" schema="GR" part="1">u<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
							<l n="15" num="5.3" lm="5" met="5"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
							<l n="16" num="5.4" lm="5" met="5"><w n="16.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="16.4" punct="ps:5">l<pgtc id="8" weight="1" schema="GR" part="1">u<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="ps">i</seg>t</rhyme></pgtc></w>…</l>
						</lg>
						<lg n="6" type="distique" rhyme="aa">
							<l n="17" num="6.1" lm="5" met="5"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg><pgtc id="9" weight="2" schema="C[R" part="1">s</pgtc></w> <w n="17.5"><pgtc id="9" weight="2" schema="C[R" part="2"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</rhyme></pgtc></w></l>
							<l n="18" num="6.2" lm="5" met="5"><w n="18.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="18.3" punct="pt:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="9" weight="2" schema="CR" part="1">s<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>