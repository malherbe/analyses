<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU30" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abba)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(6/8)" er_moy_et="0.87" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Sirènes</head>
						<lg n="1" type="quatrain" rhyme="abba">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="œ" type="vs" value="1" rule="249" place="1">œu</seg>rs</w> <w n="1.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="1.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="1.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.3">pr<seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="2.5" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="4.6" punct="pt:8"><pgtc id="1" weight="2" schema="[CR">fl<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>cs</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abba">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6" punct="vg:8">f<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3">l</w>’<w n="6.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.7">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.8">d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</rhyme></pgtc></w></l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>m<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="7.2">d</w>’<w n="7.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="7.5">l</w>’<w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.6" punct="pe:8">l<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abba">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.4">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="9.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.8" punct="ps:8">ch<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="ps">ai</seg>r</rhyme></pgtc></w>…</l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="10.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6">l</w>’<w n="10.7" punct="pe:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">On</seg>d<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="11.6" punct="vg:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="12" num="3.4" lm="8" met="8"><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="ps:8">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="ps">e</seg>r</rhyme></pgtc></w>…</l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abba">
							<l n="13" num="4.1" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6" punct="ps:8">v<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</rhyme></pgtc></w>…</l>
							<l n="14" num="4.2" lm="8" met="8"><w n="14.1" punct="vg:2">V<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="14.2">B<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lg<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="14.4" punct="vg:8">R<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
							<l n="15" num="4.3" lm="8" met="8"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="15.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="15.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="15.6" punct="vg:8">ci<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
							<l n="16" num="4.4" lm="8" met="8"><w n="16.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.6" punct="pe:8">b<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
						</lg>
					</div></body></text></TEI>