<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI38" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(abbaa)" er_moy="0.33" er_max="1" er_min="0" er_mode="0(2/3)" er_moy_et="0.47" qr_moy="0.0" qr_max="C0" qr_mode="0(3/3)" qr_moy_et="0.0">
				<head type="main">ÉPIGRAMME</head>
				<head type="sub">Le Débiteur reconnaissant</head>
				<opener>
					<dateline>
						<date when="1681">(1681)</date>
					</dateline>
				</opener>
				<lg n="1" type="quintil" rhyme="abbaa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="1.5">l</w>’<w n="1.6" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="2.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="2.6" punct="pv:8">r<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="3.2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg>qu</w>’<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="3.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>t</w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.8" punct="vg:8">b<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.6" punct="ps:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4" punct="pe:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>