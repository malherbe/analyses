<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI94" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababa)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(3/3)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(3/3)" qr_moy_et="0.0">
				<head type="number">IX</head>
				<head type="main">Parodie de cinq vers de Chapelle</head>
				<lg n="1" type="quintil" rhyme="ababa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="1.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="2.9" punct="pt:8">gu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="3.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="3.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="3.7" punct="pv:8">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>ts</rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.7">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="4.8" punct="vg:8">f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="5.6" punct="pt:8">m<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>