<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’ÉTERNELLE CHANSON</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>192 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/henribeauclairleshorizontales.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>L’Éternelle chanson</title>
						<author>Henri Beauclair</author>
						<imprint>
							<publisher>Léon Vanier,Paris</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA2" modus="sm" lm_max="8" metProfile="8" form="suite de triolets" schema="4(ABaAabAB)" er_moy="1.3" er_max="2" er_min="0" er_mode="2(13/20)" er_moy_et="0.95" qr_moy="0.0" qr_max="C0" qr_mode="0(20/20)" qr_moy_et="0.0">
				<head type="number">II</head>
				<head type="main">Jamais !</head>
				<lg n="1" rhyme="ABaAabAB">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="1.3" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp in">e</seg></w> : «<w n="1.4" punct="pe:8">J<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="A" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></pgtc></w> !»</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="2.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5" punct="pi:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="B" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">t</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="3.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.8">t</w>’<w n="3.9" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="4.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="4.3" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp">e</seg></w> : <w n="4.4" punct="pt:8">J<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="A" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.4" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="438" place="3" punct="vg">o</seg>t</w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.8" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">dr<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="7.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="7.3" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp in">e</seg></w> : «<w n="7.4" punct="pe:8">J<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="A" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></pgtc></w> !»</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="8.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5" punct="pi:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="B" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="2" rhyme="ABaAabAB">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1" punct="pt:2">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="pt">ai</seg>s</w>. <w n="9.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="9.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="9.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="A" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="10.6" punct="pt:8"><pgtc id="5" weight="2" schema="[CR">L<rhyme label="B" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="11.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="11.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="11.6" punct="vg:7">c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>l</w>, <w n="11.7" punct="pi:8">h<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="pi">ein</seg></rhyme></pgtc></w> ?</l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1" punct="pt:2">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="pt">ai</seg>s</w>. <w n="12.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="12.6" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="A" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="13.5" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t</w>, <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="13.7" punct="pt:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="a" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="14.2">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b</w> <w n="14.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5" punct="pt:8">sc<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR">l<rhyme label="b" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1" punct="pt:2">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="pt">ai</seg>s</w>. <w n="15.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="15.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="15.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="A" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="16.6" punct="pe:8"><pgtc id="5" weight="2" schema="[CR">L<rhyme label="B" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" rhyme="ABaAabAB">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="17.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="17.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.5">l</w>’<w n="17.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<pgtc id="7" weight="0" schema="R"><rhyme label="A" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</rhyme></pgtc></w>,</l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.5" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp">e</seg></w> : <w n="18.6" punct="vg:8">T<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rl<pgtc id="8" weight="0" schema="R"><rhyme label="B" id="8" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="19.6" punct="pt:8">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="20.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="20.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="20.5">l</w>’<w n="20.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<pgtc id="9" weight="0" schema="R"><rhyme label="A" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</rhyme></pgtc></w>,</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1" punct="vg:1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1" punct="vg">e</seg>l</w>, <w n="21.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="21.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1">B<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">l</w>’<w n="22.4"><seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="22.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="22.7">n</w>’<w n="22.8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="22.9" punct="pt:8">c<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="23.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="23.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="23.5">l</w>’<w n="23.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<pgtc id="9" weight="0" schema="R"><rhyme label="A" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</rhyme></pgtc></w>,</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="24.5" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp">e</seg></w> : <w n="24.6" punct="pt:8">T<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rl<pgtc id="8" weight="0" schema="R"><rhyme label="B" id="8" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="ABaAabAB">
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="25.5" punct="pv:8">m<pgtc id="10" weight="0" schema="R"><rhyme label="A" id="10" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pv">œu</seg>rs</rhyme></pgtc></w> ;</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1" punct="vg:1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="26.4" punct="vg:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="11" weight="2" schema="CR">n<rhyme label="B" id="11" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="27.3">n</w>’<w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="27.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="27.7" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.5" punct="vg:8"><pgtc id="12" weight="2" schema="[CR">m<rhyme label="A" id="12" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="29" num="4.5" lm="8" met="8"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="29.3" punct="dp:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="dp in">er</seg></w> : «<w n="29.4" punct="ps:5"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="ps">Ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>… <w n="29.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="29.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="29.7"><pgtc id="12" weight="2" schema="[CR">m<rhyme label="a" id="12" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</rhyme></pgtc></w>»</l>
					<l n="30" num="4.6" lm="8" met="8">— <w n="30.1">Tr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="30.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="11" weight="2" schema="CR">n<rhyme label="b" id="11" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="31" num="4.7" lm="8" met="8"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="31.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="31.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="31.5" punct="pv:8"><pgtc id="12" weight="2" schema="[CR">m<rhyme label="A" id="12" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pv">œu</seg>rs</rhyme></pgtc></w> ;</l>
					<l n="32" num="4.8" lm="8" met="8"><w n="32.1" punct="vg:1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="32.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="32.4" punct="pt:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="11" weight="2" schema="CR">n<rhyme label="B" id="11" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>