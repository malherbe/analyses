<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA66" modus="cm" lm_max="10" metProfile="4+6" form="sonnet classique" schema="abab abab cdc dcd" er_moy="1.25" er_max="2" er_min="0" er_mode="2(5/8)" er_moy_et="0.97" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
					<head type="main">LA CHANSON DES BOIS</head>
					<head type="form">SONNET</head>
					<opener>
						<salute>A Marie Désirée</salute>
					</opener>
					<lg n="1" rhyme="ababababcdcdcd">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M/mp">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="Lp">ai</seg>s</w>-<w n="1.3" punct="vg:4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="1.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="1.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9" mp="M">ain</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="2.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="2.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="2.9" punct="pi:10">ci<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pi">eu</seg>x</rhyme></pgtc></w> ?</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">A</seg>s</w>-<w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.3" punct="vg:4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">B<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>rs</w><caesura></caesura> <w n="4.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ts</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6" punct="pi:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pi">eu</seg>x</rhyme></pgtc></w> ?</l>
						<l n="5" num="1.5" lm="10" met="4+6"><space quantity="4" unit="char"></space><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">A</seg>s</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="Lc">à</seg></w>-<w n="6.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>t</w><caesura></caesura> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="7">um</seg>s</w> <w n="6.6" punct="vg:10">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>gr<seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">p<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>t</w> <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="8.8" punct="pi:10"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pi">eu</seg>x</rhyme></pgtc></w> ?</l>
						<l n="9" num="1.9" lm="10" met="4+6"><space quantity="4" unit="char"></space><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="9.5" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="9.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.11" punct="vg:10">m<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>s<pgtc id="5" weight="2" schema="CR">t<rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="10.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</rhyme></pgtc></w></l>
						<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="11.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="11.6" punct="pv:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="12" num="1.12" lm="10" met="4+6"><space quantity="4" unit="char"></space><w n="12.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="12.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="12.4" punct="vg:4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="12.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="12.9"><pgtc id="6" weight="2" schema="[CR">t<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</rhyme></pgtc></w></l>
						<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="13.2">gl<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="13.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>rs</w> <w n="13.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.7" punct="vg:10"><pgtc id="5" weight="2" schema="[CR">t<rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" caesura="1">e</seg>rs</w><caesura></caesura> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="6" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="14.6">d</w>’<w n="14.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>