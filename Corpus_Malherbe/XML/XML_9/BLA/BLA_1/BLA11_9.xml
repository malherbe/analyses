<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA11" modus="cm" lm_max="10" metProfile="4+6" form="sonnet classique" schema="abba abba cdc dcd" er_moy="1.0" er_max="4" er_min="0" er_mode="0(5/8)" er_moy_et="1.41" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
					<head type="main">MÉLANCOLIE</head>
					<head type="form">SONNET</head>
					<lg n="1" rhyme="abbaabbacdcdcd">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="1.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.4" punct="vg:4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg" caesura="1">oi</seg>rs</w>,<caesura></caesura> <w n="1.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>fs</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7" punct="vg:10">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="2.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg>s</w><caesura></caesura> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="2.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="2.6" punct="pv:10">fl<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="3.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>t</w> <w n="3.8" punct="vg:10">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="4.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="4.5" punct="pt:10">cl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6"><space quantity="4" unit="char"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="5.6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="5.7" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="6.2">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="6.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="6.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="6.6" punct="pv:10">t<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg></w><caesura></caesura> <w n="7.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="7.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rps</w> <w n="7.9">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.3">d<seg phoneme="œ" type="vs" value="1" rule="406" place="4" caesura="1">eu</seg>il</w><caesura></caesura> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.5">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="8.6">qu</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.9" punct="pt:10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">tt<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
						<l n="9" num="1.9" lm="10" met="4+6"><space quantity="4" unit="char"></space><w n="9.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="9.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w> <w n="9.7" punct="dp:10">m<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>c<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="3" mp="Fc">e</seg></w> <w n="10.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>rp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="10.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="10.7" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<pgtc id="6" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="11" num="1.11" lm="10" met="4+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>f</w> <w n="11.7">s</w>’<w n="11.8" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xh<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="12" num="1.12" lm="10" met="4+6"><space quantity="4" unit="char"></space><w n="12.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="12.2">d<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>il</w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="12.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="12.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="12.7" punct="pi:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>c<pgtc id="6" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pi">eu</seg>x</rhyme></pgtc></w> ?</l>
						<l n="13" num="1.13" lm="10" met="4+6"><w n="13.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="13.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="13.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">aî</seg>t</w><caesura></caesura> <w n="13.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.6" punct="dp:10">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="14" num="1.14" lm="10" met="4+6"><w n="14.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="14.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>l</w><caesura></caesura> <w n="14.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="14.7">gu<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="14.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="14.9" punct="pe:10">ci<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1851">9 Décembre 1851.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>