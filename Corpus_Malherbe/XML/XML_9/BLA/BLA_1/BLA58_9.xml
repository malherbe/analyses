<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA58" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="6(abab)" er_moy="1.17" er_max="6" er_min="0" er_mode="0(7/12)" er_moy_et="1.72" qr_moy="0.0" qr_max="C0" qr_mode="0(12/12)" qr_moy_et="0.0">
					<head type="main">MYSTÈRES</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="1.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>rs</w> <w n="1.9">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">à</seg></w> <w n="2.3">l</w>’<w n="2.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.10" punct="pv:10">ci<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="3.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>rs</w> <w n="3.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>ls</w><caesura></caesura> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="4.5">d<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w> <w n="4.6" punct="pt:10">m<seg phoneme="i" type="vs" value="1" rule="493" place="7" mp="M">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="5.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="5.8" punct="vg:10">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>ct<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="6.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.8" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="7.9"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg>s</w> <w n="8.3" punct="vg:4">p<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>rs</w>,<caesura></caesura> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">c<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="8.9" punct="pt:10">j<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>r<pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="10.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>rs</w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>rs</w> <w n="10.6" punct="pv:10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>r<pgtc id="6" weight="2" schema="CR">d<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pv">u</seg>s</rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8" mp="M">ym</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">th<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">M<seg phoneme="u" type="vs" value="1" rule="428" place="1" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="12.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="12.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="12.6" punct="pt:10"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="6" weight="2" schema="CR">d<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pt">u</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="13.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="13.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="13.7" punct="pe:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
						<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.6" punct="pv:10">p<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</rhyme></pgtc></w> ;</l>
						<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="15.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="15.7" punct="pe:10">v<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
						<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="16.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="16.3">s</w>’<w n="16.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="16.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="16.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="16.8" punct="dp:10">b<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="dp">a</seg>s</rhyme></pgtc></w> :</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="10" met="4+6">« <w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="17.3">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="17.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="17.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="17.8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>ts</w> <w n="17.9">s<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="18" num="5.2" lm="10" met="4+6"><w n="18.1">D</w>’<w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="18.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="18.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="18.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="18.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8" punct="vg:10"><pgtc id="10" weight="6" schema="[VCR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="19.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="19.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="19.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="19.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="19.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="19.8"><pgtc id="9" weight="0" schema="[R" part="1"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="10" met="4+6"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="20.6" punct="dp:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="dp">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M/mp">Ai</seg>m<pgtc id="10" weight="6" schema="V[CR" part="1"><seg phoneme="e" type="vs" value="1" rule="347" place="9" mp="Lp">ez</seg></pgtc></w>-<w n="20.8" punct="pe:10"><pgtc id="10" weight="6" schema="V[CR" part="2">m<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="10" met="4+6">« <w n="21.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="21.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="21.4">n</w>’<w n="21.5"><seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="21.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="21.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="11" weight="2" schema="CR" part="1">l<rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="22" num="6.2" lm="10" met="4+6"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">à</seg></w> <w n="22.3">l</w>’<w n="22.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.5"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="22.6">l</w>’<w n="22.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="22.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="22.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="22.10" punct="vg:10">ci<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="10" met="4+6"><w n="23.1">T<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="23.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>rs</w><caesura></caesura> <w n="23.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="23.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="23.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="23.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="11" weight="2" schema="CR" part="1">l<rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="10" met="4+6"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="24.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="24.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>ls</w><caesura></caesura> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="24.5">d<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w> <w n="24.6" punct="pe:10">m<seg phoneme="i" type="vs" value="1" rule="493" place="7" mp="M">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg>x</rhyme></pgtc></w> ! »</l>
					</lg>
				</div></body></text></TEI>