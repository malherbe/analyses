<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA122" modus="cp" lm_max="12" metProfile="3, 6+6" form="suite périodique" schema="5(abab)" er_moy="0.4" er_max="2" er_min="0" er_mode="0(8/10)" er_moy_et="0.8" qr_moy="0.0" qr_max="C0" qr_mode="0(10/10)" qr_moy_et="0.0">
					<head type="main">RÉSIGNATION</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="1.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">nn<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5">j</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="2.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="2.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="2.9" punct="vg:12">t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M/mp">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="Lp">û</seg>t</w>-<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="3.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">aî</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="3" met="3"><space unit="char" quantity="18"></space><w n="4.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3" punct="pe:3">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="5.3">l</w>’<w n="5.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>t</w>, <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.6" punct="pv:6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pv" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> ;<caesura></caesura> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="5.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.10">d<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>il</w> <w n="5.11"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="5.12">l</w>’<w n="5.13" punct="pe:12"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="6.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>rs</w><caesura></caesura> <w n="6.6">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.9" punct="vg:12">t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="7.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="7.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="7.9">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11">oin</seg></w> <w n="7.10" punct="vg:12">s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="3" met="3"><space unit="char" quantity="18"></space><w n="8.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="8.3" punct="pe:3">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.4">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.5">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="7" mp="M">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="9.7">t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fm">e</seg></w>-<w n="9.8"><pgtc id="5" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="9.9" punct="vg:12"><pgtc id="5" weight="2" schema="[C[R" part="2"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.8" punct="vg:12">t<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" caesura="1">un</seg></w><caesura></caesura> <w n="11.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="11.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="11.11" punct="dp:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="5" weight="2" schema="CR" part="1">d<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="12" num="3.4" lm="3" met="3"><space unit="char" quantity="18"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="12.2" punct="pe:3">m<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="13.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5" punct="tc:6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp ti" caesura="1">i</seg>t</w> :<caesura></caesura> — <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.7" punct="vg:9">b<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="9" punct="vg">eu</seg>r</w>, <w n="13.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.10" punct="vg:12"><pgtc id="7" weight="0" schema="[R" part="1"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="14.2" punct="vg:2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>r</w>, <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="14.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="14.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="14.8">br<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="14.10" punct="pe:12">t<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe ti">oi</seg></rhyme></pgtc></w> ! —</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="15.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="15.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="15.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="15.8">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="15.10" punct="vg:12">m<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="3" met="3"><space unit="char" quantity="18"></space><w n="16.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="16.3" punct="pe:3">m<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2" punct="pe:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg></w> ! <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="17.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="17.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="18.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="18.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7" mp="M">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="18.7" punct="pe:12">t<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l</w>’<w n="19.3" punct="vg:3"><seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="19.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="19.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="19.9">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="3" met="3"><space unit="char" quantity="18"></space><w n="20.1">S<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="20.3" punct="pe:3">m<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>