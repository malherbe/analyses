<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA44" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)" er_moy="0.61" er_max="2" er_min="0" er_mode="0(12/18)" er_moy_et="0.89" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
					<head type="main">RETRAITE</head>
					<opener>
						<salute>A Marie Désirée.</salute>
						<epigraph>
							<cit>
								<quote>
										Ille mihi terrarum prêter omnes <lb></lb>
										Angulus ridet.
								</quote>
								<bibl>
									<name>HORACE</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="1.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>g<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w>-<w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4" punct="vg:5">f<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="2.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="2.7" punct="vg:8">br<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="3.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">tr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="4.7" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">l<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="4">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="6.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.8" punct="pv:8"><pgtc id="4" weight="2" schema="[CR">gr<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>d</rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.3" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rç<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rs</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="10.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.7" punct="pv:8">vi<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">tu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="12.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6">d<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="14.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="14.6" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>rd</rhyme></pgtc></w> ;</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="16.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rt</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="17.5">qu</w>’<w n="17.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.7" punct="vg:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="9" weight="2" schema="CR">b<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="18.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.7" punct="pv:8">fl<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>rs</rhyme></pgtc></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t</w> <w n="19.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="19.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR">b<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2">mi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="20.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="20.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="20.6" punct="pv:8">pl<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>rs</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="11" weight="2" schema="CR">t<rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="22.3">l</w>’<w n="22.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="22.5" punct="vg:8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ge<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>sm<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.5">cl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="11" weight="2" schema="CR">t<rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="24.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="24.4">d</w>’<w n="24.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="25.2" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.5" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rd<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="26.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="26.5" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg><pgtc id="14" weight="2" schema="CR">n<rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="27.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="27.3">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="27.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="27.6" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="28.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="28.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="28.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="28.5" punct="pt:8"><pgtc id="14" weight="2" schema="[CR">n<rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>ds</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="29.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="29.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.6" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="354" place="7">e</seg>x<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="30.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="30.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="30.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="30.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="30.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="31.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="31.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="31.4"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="31.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="31.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="31.7" punct="vg:8">p<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="32.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="32.3"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="32.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="32.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="32.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="32.7">d</w>’<w n="32.8" punct="pt:8"><pgtc id="16" weight="0" schema="[R"><rhyme label="b" id="16" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="abab">
						<l n="33" num="9.1" lm="8" met="8"><w n="33.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="33.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="33.4">s</w>’<w n="33.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="33.7"><pgtc id="17" weight="2" schema="CR">p<rhyme label="a" id="17" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="34" num="9.2" lm="8" met="8"><w n="34.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="34.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="34.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="34.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="34.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="34.6" punct="pt:8">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>gn<pgtc id="18" weight="0" schema="R"><rhyme label="b" id="18" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
						<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="35.2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="35.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="35.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="35.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="35.6">d</w>’<w n="35.7" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>s<pgtc id="17" weight="2" schema="CR">p<rhyme label="a" id="17" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="36" num="9.4" lm="8" met="8"><w n="36.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="36.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w>-<w n="36.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="36.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="36.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="36.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.7" punct="pi:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<pgtc id="18" weight="0" schema="R"><rhyme label="b" id="18" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pi">eu</seg>r</rhyme></pgtc></w> ?</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Sceaux</placeName>,
							<date when="1849">août 1849.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>