<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER108" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(abab)" er_moy="1.67" er_max="6" er_min="0" er_mode="0(3/6)" er_moy_et="2.13" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="number">CVIII</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3" punct="pt:4">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pt">on</seg>s</w>. <w n="1.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="1.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="2.5">l</w>’<w n="2.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>br<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="4.7" punct="pt:8">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="pt:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pt">eu</seg></w>. <w n="5.2">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="5.4" punct="pt:7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="pt">e</seg>nt</w>. <w n="5.5"><pgtc id="3" weight="0" schema="[R">L<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="pt:4">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt">e</seg></w>. <w n="6.2">D<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="6.3" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="4" weight="2" schema="CR">n<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></pgtc></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:4">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="7.2" punct="pt:7">cr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg><pgtc id="3" weight="0" schema="[R" part="1">l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w>. <w n="7.3"><pgtc id="3" weight="0" schema="[R" part="2"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="8.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="8.5" punct="pt:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="4" weight="2" schema="CR" part="1">n<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="10.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="6" gender="m" type="a" qr="E0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">bu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="12.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="6" gender="m" type="e" qr="E0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>