<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM90" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2(abba) 3(abab)" er_moy="0.6" er_max="2" er_min="0" er_mode="0(6/10)" er_moy_et="0.8" qr_moy="2.0" qr_max="N5" qr_mode="0(6/10)" qr_moy_et="2.45">
				<head type="main">JE VOUS INVITE…</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6" punct="pt:8">k<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.8">gr<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="N5"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w> <w n="3.6" punct="vg:8">ch<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="N5"><seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>d</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="4.3">ç<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="4.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.6">l</w>’<w n="4.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="5.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="5.7">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">d</w>’<w n="6.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="7.3">n</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>f</w> <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.7">l</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="pt:8">b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">V<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="10.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="6" weight="1" schema="GR">i<rhyme label="b" id="6" gender="m" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="12.6">d</w>’<w n="12.7" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>b<pgtc id="6" weight="1" schema="GR">i<rhyme label="b" id="6" gender="m" type="e" qr="N5"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="13.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.5" punct="vg:8"><pgtc id="8" weight="2" schema="[CR">c<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="15.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>gl<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pt:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">ez</seg></w> ! <w n="17.2" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pe">ez</seg></w> ! <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="9" weight="2" schema="CR">d<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="18.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="18.6" punct="pt:8">r<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></rhyme></pgtc></w>.</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="19.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="19.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6">en</seg>s</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6" punct="vg:8">b<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.6" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR">d<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>s</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>