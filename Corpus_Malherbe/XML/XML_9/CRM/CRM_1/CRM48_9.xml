<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM48" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="5[aa] 1[abab]" er_moy="0.0" er_max="0" er_min="0" er_mode="0(7/7)" er_moy_et="0.0" qr_moy="1.43" qr_max="N5" qr_mode="0(5/7)" qr_moy_et="2.26">
				<head type="main">AU MILIEU DES LABOURS</head>
				<lg n="1" type="regexp" rhyme="aa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w>-<w n="1.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="1.7">pr<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:5">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.5" punct="pi:8">Br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pi">an</seg>t</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="2" type="regexp" rhyme="aa">
					<l n="3" num="2.1" lm="8" met="8"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="3.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="3.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="3.6">d</w>’<w n="3.7" punct="vg:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
					<l n="4" num="2.2" lm="8" met="8"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="4.6" punct="pv:8">m<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pv">o</seg>ts</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="3" type="regexp" rhyme="aa">
					<l n="5" num="3.1" lm="8" met="8"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="5.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w> <w n="5.7">bl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="3" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</rhyme></pgtc></w></l>
					<l n="6" num="3.2" lm="8" met="8"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.6" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="3" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" type="regexp" rhyme="aa">
					<l n="7" num="4.1" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="u" type="vs" value="1" rule="292" place="6">aoû</seg>t</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.7" punct="vg:8">g<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="4" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="4.2" lm="8" met="8"><w n="8.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="8.6">d</w>’<w n="8.7">h<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="4" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				</lg>
				<lg n="5" type="regexp" rhyme="aa">
					<l n="9" num="5.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="5.2" lm="8" met="8"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.6" punct="pt:8">p<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="regexp" rhyme="ab">
					<l n="11" num="6.1" lm="8" met="8"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="11.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</rhyme></pgtc></w>,</l>
					<l n="12" num="6.2" lm="8" met="8"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="12.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="12.6" punct="vg:8">Br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="7" type="regexp" rhyme="ab">
					<l n="13" num="7.1" lm="8" met="8"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="13.5">g<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.7">s<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rds</rhyme></pgtc></w></l>
					<l n="14" num="7.2" lm="8" met="8"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="14.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="14.5">d</w>’<w n="14.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>