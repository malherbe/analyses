<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM80" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2(abba) 3(abab)" er_moy="0.9" er_max="6" er_min="0" er_mode="0(7/10)" er_moy_et="1.81" qr_moy="2.0" qr_max="M15" qr_mode="0(8/10)" qr_moy_et="4.58">
				<head type="main">IL PLEUT SUR LES SENTIERS RIEURS</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="1.6" punct="pt:8">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2" punct="vg:2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>t</w>, <w n="2.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w>-<w n="2.4" punct="vg:5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>, <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="2.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.3">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="3.4">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.7">tr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">h<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">plu<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="1" weight="2" schema="CR">c<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3">um</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="pt:8">m<pgtc id="2" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<rhyme label="b" id="2" gender="f" type="a" qr="M15"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="7.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6"><pgtc id="1" weight="2" schema="[CR">c<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">f<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t</w> <w n="8.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<pgtc id="2" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<rhyme label="b" id="2" gender="f" type="e" qr="M15"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6" punct="pt:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6">d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg></w> <w n="12.4" punct="pt:8">t<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="13.2" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="13.3">c</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="13.9">s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="14.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">plu<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="15.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5">dr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="15.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.7">t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s</w> <w n="16.6" punct="pt:8">n<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">S<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt</w>-<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.7" punct="vg:8">c<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">j<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5">J<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="19.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="19.4">Br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="19.5" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="20.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.6" punct="pi:8">Fr<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>