<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM160" modus="cm" lm_max="10" metProfile="4÷6" form="sonnet non classique" schema="abba cddc eef ggf" er_moy="0.29" er_max="2" er_min="0" er_mode="0(6/7)" er_moy_et="0.7" qr_moy="3.57" qr_max="M15" qr_mode="0(4/7)" qr_moy_et="5.15">
				<head type="main">NIVELLES</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="1.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="4" caesura="1">om</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.8" punct="vg:10">D<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ts</w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.5">bi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="2.9" punct="vg:10">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" mp4="F" met="6+4"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="2" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rt</w><caesura></caesura> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="3.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="3.8">chi<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="4.2">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="2">ean</seg></w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.7" punct="pi:10">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="2" rhyme="cddc">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="5.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>rc</w><caesura></caesura> <w n="5.5" punct="vg:5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg></w>, <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>il</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="5.8">l</w>’<w n="5.9">h<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="10" mp4="F" mp6="M" met="10"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.3" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="6.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="6.5">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="6" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="6.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="6.8"><pgtc id="4" weight="2" schema="[CR">m<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3" punct="vg:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.5" punct="vg:7">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.7" punct="vg:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="10" met="6+4" mp5="Fc"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="8.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="8.7" punct="pi:10">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>rpr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg>nt</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="3" rhyme="eef">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>St</w>-<w n="9.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>l</w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="9.7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="9.8">h<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>d<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="10.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="307" place="3" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="10.7">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="10.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="10.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="10.10" punct="vg:10">ci<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="e" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1" mp="C">e</seg>t</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="11.6">f<pgtc id="5" weight="0" schema="R"><rhyme label="f" id="5" gender="m" type="a" qr="M15"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></rhyme></pgtc></w></l>
				</lg>
				<lg n="4" rhyme="ggf">
					<l n="12" num="4.1" lm="10" met="4+6"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.7">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><pgtc id="6" weight="0" schema="R"><rhyme label="g" id="6" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="13" num="4.2" lm="10" met="4+6"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="13.4">w<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ll<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="13.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>g<pgtc id="6" weight="0" schema="R"><rhyme label="g" id="6" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="10" met="4+6"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.4" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="14.5" punct="vg:7">N<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="14.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.7" punct="pi:10">j<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<pgtc id="5" weight="0" schema="R"><rhyme label="f" id="5" gender="m" type="e" qr="M15"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pi">in</seg></rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>