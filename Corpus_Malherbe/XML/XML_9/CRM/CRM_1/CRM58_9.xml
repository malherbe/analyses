<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM58" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="1[abaab] 1[aa] 1[abba]" er_moy="1.0" er_max="6" er_min="0" er_mode="0(5/6)" er_moy_et="2.24" qr_moy="6.67" qr_max="A20" qr_mode="0(3/6)" qr_moy_et="7.99">
				<head type="main">ON VOYAIT LES CHEMINS</head>
				<lg n="1" type="regexp" rhyme="abaabaaabba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="1.5">s</w>’<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="1.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" mp="P">e</seg>rs</w> <w n="1.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.10" punct="pt:12">ci<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" stanza="1" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="421" place="10">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.7" punct="pt:12">v<pgtc id="1" weight="0" schema="R"><rhyme label="b" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="3.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="3.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.8">bl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="1" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rd</w> <w n="4.6">d</w>’<w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="4.8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="4.9" punct="vg:12">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" stanza="1" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="5.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="5.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.4">h<seg phoneme="e" type="vs" value="1" rule="169" place="7" mp="M">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="5.7" punct="pt:12">t<pgtc id="1" weight="0" schema="R"><rhyme label="b" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rtr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="6.3">j<seg phoneme="ə" type="ee" value="0" rule="e-18">e</seg></w> <w n="6.4" punct="vg:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="8">oi</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w>-<w n="6.7">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="6.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tt<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<rhyme label="a" id="2" gender="m" type="a" stanza="2" qr="A20"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="7.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="7.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="7.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>f<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>d<rhyme label="a" id="2" gender="m" type="e" stanza="2" qr="A20"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="2" mp="M">eu</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.8" punct="pi:12">m<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>ss<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="3" qr="M15"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pi">on</seg>s</rhyme></pgtc></w> ?</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="9.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="9.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.10">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t</rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="10.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="10.7">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>t</rhyme></pgtc></w></l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="11.8" punct="pi:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>z<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="3" qr="M15"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pi">on</seg></rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>