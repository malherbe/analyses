<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM6" modus="sp" lm_max="8" metProfile="8, 2" form="suite périodique" schema="4(aabccb)" er_moy="0.58" er_max="2" er_min="0" er_mode="0(8/12)" er_moy_et="0.86" qr_moy="2.5" qr_max="N5" qr_mode="0(6/12)" qr_moy_et="2.5">
				<head type="main">DU BLEU ?</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2" punct="pi:2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pi">eu</seg></w> ? <w n="1.3">J</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="1.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>ps</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>mm<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="2" met="2"><space unit="char" quantity="12"></space><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="pt:2">l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="4.2" punct="pi:2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pi">eu</seg></w> ? <w n="4.3">J</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<pgtc id="3" weight="1" schema="GR">i<rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<pgtc id="3" weight="1" schema="GR">i<rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="2" met="2"><space unit="char" quantity="12"></space><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2" punct="pt:2">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2" punct="pt">ain</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="7.5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7">bl<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</rhyme></pgtc></w></l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">M<seg phoneme="y" type="vs" value="1" rule="445" place="1">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="8.3">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.6">ju<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ll<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</rhyme></pgtc></w></l>
					<l n="9" num="2.3" lm="2" met="2"><space unit="char" quantity="12"></space><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2" punct="vg:2">l<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">f<seg phoneme="œ" type="vs" value="1" rule="304" place="7">ai</seg>s<pgtc id="6" weight="0" schema="R"><rhyme label="c" id="6" gender="m" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></pgtc></w></l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="11.6">c<seg phoneme="œ" type="vs" value="1" rule="345" place="6">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="11.7">s<pgtc id="6" weight="0" schema="R"><rhyme label="c" id="6" gender="m" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg>s</rhyme></pgtc></w></l>
					<l n="12" num="2.6" lm="2" met="2"><space unit="char" quantity="12"></space><w n="12.1" punct="pt:2">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.6">d</w>’<w n="13.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg><pgtc id="7" weight="2" schema="CR">pl<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="15" num="3.3" lm="2" met="2"><space unit="char" quantity="12"></space><w n="15.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg><pgtc id="8" weight="2" schema="CR">ss<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="16.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.6" punct="vg:8">c<pgtc id="9" weight="0" schema="R"><rhyme label="c" id="9" gender="f" type="a" qr="N5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="17.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="17.5">b<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="17.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.7">j</w>’<w n="17.8"><pgtc id="9" weight="0" schema="[R"><rhyme label="c" id="9" gender="f" type="e" qr="N5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="2" met="2"><space unit="char" quantity="12"></space><w n="18.1" punct="pt:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="19.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="19.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>c<pgtc id="10" weight="2" schema="CR">t<rhyme label="a" id="10" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="20.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="20.4">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.7" punct="vg:8"><pgtc id="10" weight="2" schema="CR">t<rhyme label="a" id="10" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="21" num="4.3" lm="2" met="2"><space unit="char" quantity="12"></space><w n="21.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="a" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</rhyme></pgtc></w></l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1" punct="vg:2">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="22.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="22.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="5">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="22.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<pgtc id="12" weight="0" schema="R"><rhyme label="c" id="12" gender="f" type="a" qr="N5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="23.5">n<pgtc id="12" weight="0" schema="R"><rhyme label="c" id="12" gender="f" type="e" qr="N5"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="24" num="4.6" lm="2" met="2"><space unit="char" quantity="12"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2" punct="pt:2">m<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="e" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="pt">ai</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>