<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM162" modus="cm" lm_max="12" metProfile="6+6" form="sonnet peu classique" schema="abab abba ccd eed" er_moy="2.86" er_max="8" er_min="0" er_mode="2(3/7)" er_moy_et="2.8" qr_moy="2.14" qr_max="N5" qr_mode="0(4/7)" qr_moy_et="2.47">
				<head type="main">DIGITALES</head>
				<lg n="1" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="1.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="1.3">c<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M/mp">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M/mp">é</seg>br<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="1.4" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" qr="N5"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.5">ju<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="2.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="2.8">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>b</w> <w n="2.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg><pgtc id="2" weight="2" schema="CR">d<rhyme label="b" id="2" gender="m" type="a" qr="N5"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.5">f<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6" caesura="1">ê</seg>ts</w><caesura></caesura> <w n="3.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.8">li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.9">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e" qr="N5"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>nt</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w> <w n="4.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" mp="M">è</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.5">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="4.6" punct="pi:12">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="2" weight="2" schema="CR">d<rhyme label="b" id="2" gender="m" type="e" qr="N5"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pi">u</seg>s</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="5.9">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="5.10" punct="vg:12">p<pgtc id="3" weight="6" schema="VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="10">y</seg>s</w> <w n="6.6" punct="pi:12"><pgtc id="4" weight="8" schema="[CVCR">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pi">u</seg></rhyme></pgtc></w> ?</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l</w> <w n="7.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="354" place="5" mp="M">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="7.8">m<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><pgtc id="4" weight="8" schema="CVCR">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="8.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>gts</w><caesura></caesura> <w n="8.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="8.8" punct="pt:12"><pgtc id="3" weight="6" schema="[VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="9.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="9.9" punct="vg:12">ch<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="10.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="10.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rs</w><caesura></caesura> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" mp="C">e</seg>t</w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="12.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="12.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>pr<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="13.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="13.3">br<seg phoneme="y" type="vs" value="1" rule="445" place="5" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="13.8">s<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="14.3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="14.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="14.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="14.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>