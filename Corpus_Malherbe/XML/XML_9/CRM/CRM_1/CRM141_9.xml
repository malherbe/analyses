<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM141" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="1(abba) 4(abab)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(10/10)" er_moy_et="0.0" qr_moy="4.0" qr_max="S30" qr_mode="0(7/10)" qr_moy_et="8.89">
				<head type="main">LES POMMES DE PIN</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.3">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5" punct="pt:8">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="3.3">t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">th<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8">ym</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.6" punct="pt:8">li<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="S30"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="S30"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>rt<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">p<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.6">vr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="12.6" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="13.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rdi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6" punct="pt:8">p<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3" punct="vg:5">m<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>t</w>, <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="15.6" punct="vg:8">tr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="16.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="16.7" punct="pt:8">m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2" punct="vg:3">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="17.6" punct="vg:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="18.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ni<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="18.3" punct="vg:8">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="19.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="20.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="20.6" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>