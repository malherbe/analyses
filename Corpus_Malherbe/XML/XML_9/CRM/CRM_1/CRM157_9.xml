<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM157" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(abbaccdcd)" er_moy="0.4" er_max="2" er_min="0" er_mode="0(4/5)" er_moy_et="0.8" qr_moy="8.0" qr_max="A20" qr_mode="0(2/5)" qr_moy_et="8.12">
				<head type="main">UN CHEVAL ARRÊTE…</head>
				<lg n="1" type="neuvain" rhyme="abbaccdcd">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="1.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="1.9">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="A20"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.3"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="2.6">f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="3.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="3.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="3.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="3.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>l</w> <w n="3.9" punct="pt:12">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2" punct="vg:3">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg>s</w>, <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>rs</w> <w n="4.5" punct="vg:6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" punct="vg" caesura="1">e</seg>ds</w>,<caesura></caesura> <w n="4.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="4.7">r<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="4.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="4.9" punct="pt:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e" qr="A20"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">bl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="5.5" punct="pt:6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="pt" caesura="1">au</seg>t</w>.<caesura></caesura> <w n="5.6">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="5.9" punct="pt:12">d<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">fl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="6.8" punct="pt:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ll<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="7.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>rs</w> <w n="7.5" punct="pv:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pv" caesura="1">eu</seg>rs</w> ;<caesura></caesura> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.7">r<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="7.8">s</w>’<w n="7.9"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="M15"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>nt</rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="307" place="6" caesura="1">a</seg>il</w><caesura></caesura> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.8" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="c" id="3" gender="f" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="9.2" punct="vg:2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290" place="2" punct="vg">aon</seg></w>, <w n="9.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="4" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="9.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="9.7" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e" qr="M15"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pt">e</seg>il</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>