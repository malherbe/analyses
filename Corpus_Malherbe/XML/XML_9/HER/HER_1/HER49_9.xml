<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Rome et les Barbares</head><div type="poem" key="HER49" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba ccd dcd" er_moy="1.75" er_max="6" er_min="0" er_mode="2(4/8)" er_moy_et="1.85" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
					<head type="main">Tranquillus</head>
					<opener>
						<epigraph>
							<cit>
								<quote>C. Plinii Secundi Epist. Lib. I, Ep. XXIV</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="1.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6" caesura="1">y</seg>s</w><caesura></caesura> <w n="1.7">qu</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.9">v<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="1.10" punct="pv:12">S<seg phoneme="y" type="vs" value="1" rule="dc-3" place="10" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="2.3">l</w>’<w n="2.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="3">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="2.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.8" punct="vg:12">T<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="3.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="3.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10">an</seg></w> <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.10" punct="vg:12">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="4.3">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="4" mp="M">u</seg><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">am</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.7" punct="pt:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>s<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="5.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.9" punct="vg:9">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>r</w>, <w n="5.10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.11" punct="vg:12"><pgtc id="3" weight="6" schema="[VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="6.3" punct="vg:3">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="3" punct="vg">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="6.8">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ls</w> <w n="6.9">d</w>’<w n="6.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>z<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="7.7">c<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>p</w> <w n="7.8" punct="pt:12">m<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="445" place="12" punct="pt">û</seg>r</rhyme></pgtc></w>.</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="8">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.8" punct="pt:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<pgtc id="3" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="9.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>x</w><caesura></caesura> <w n="9.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="9.7">c</w>’<w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="9.9"><pgtc id="5" weight="2" schema="[CR" part="1">l<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="12">à</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="10.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.5" punct="vg:6">N<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="10.6" punct="vg:8">Cl<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="10.7" punct="vg:12">C<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="11" mp="M">u</seg><pgtc id="5" weight="2" schema="CR" part="1">l<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="e" type="vs" value="1" rule="353" place="1" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="415" place="5" mp="M">ô</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.5">st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.6" punct="pv:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rpr<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" rhyme="dcd">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.7">st<seg phoneme="i" type="vs" value="1" rule="493" place="6" caesura="1">y</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.10">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.11"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="13.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="13.4" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="C">i</seg><pgtc id="5" weight="2" schema="C[R" part="1">l</pgtc></w> <w n="13.6"><pgtc id="5" weight="2" schema="C[R" part="2"><rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="14.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>rs</w> <w n="14.4">l<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>rs</w><caesura></caesura> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="14.6">vi<seg phoneme="e" type="vs" value="1" rule="383" place="8" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.8" punct="pt:12">C<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR" part="1">pr<rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>