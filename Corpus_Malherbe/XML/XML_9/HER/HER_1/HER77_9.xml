<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER77" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="1.71" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.98" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="main">Émail</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="1.3" punct="pv:4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pv">i</seg>t</w> ; <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.7" punct="pt:9">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt" mp="F">e</seg></w>. <w n="1.8">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ds</w> <w n="1.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.10" punct="pt:12">l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="2.5">s</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="365" place="11" mp="M">e</seg><pgtc id="2" weight="2" schema="CR">mm<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9">p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>g<pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12">en</seg>t</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="4.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.8" punct="pt:12">tr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s</w>, <w n="5.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2" mp="M/mp">ein</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="Lp">a</seg>s</w>-<w n="5.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">m<seg phoneme="i" type="vs" value="1" rule="493" place="6" caesura="1">y</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.8">l<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.10">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="6.2" punct="vg:3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>s<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="6.4" punct="vg:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="6.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.7"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.9">l</w>’<w n="6.10" punct="pi:12"><pgtc id="4" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pi">an</seg>t</rhyme></pgtc></w> ?</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="7.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="7.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mp">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="Lp">a</seg>s</w>-<w n="7.5" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="7.8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="7.9" punct="vg:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>rm<pgtc id="4" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>br<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2">l</w>’<w n="8.3">h<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.7">gl<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.8" punct="pi:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>pp<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="pt:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pt">on</seg></w>. <w n="9.2" punct="vg:3">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="3" punct="vg">ô</seg>t</w>, <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="9.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">ph<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>scr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="10.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-6" place="4">e</seg>r</w> <w n="10.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l</w><caesura></caesura> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.7">d</w>’<w n="10.8" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">O</seg><pgtc id="5" weight="2" schema="CR">ph<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:3">Th<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>str<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="11.2" punct="pt:6">Br<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>.<caesura></caesura> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="7">Au</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="11.5">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="12.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="12.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="12.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="12.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="13.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ds</w> <w n="13.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.7">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="14.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="14.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg></w><caesura></caesura> <w n="14.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="14.9">d</w>’<w n="14.10" punct="pt:12"><pgtc id="7" weight="0" schema="[R"><rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>