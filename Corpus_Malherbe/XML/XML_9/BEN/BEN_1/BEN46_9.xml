<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN46" modus="sm" lm_max="8" metProfile="8" form="sonnet peu classique" schema="abab baba ccd ede" er_moy="3.0" er_max="6" er_min="0" er_mode="2(3/7)" er_moy_et="2.07" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="form">SONNET.</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">O</seg>RS</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<pgtc id="1" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2" punct="vg:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>t</w>, <w n="2.3">c</w>’<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="2.6" punct="vg:5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5" punct="vg">o</seg>p</w>, <w n="2.7" punct="dp:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="dp">on</seg>s</rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rs</w>, <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.6">br<pgtc id="1" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.6" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rr<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="baba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.4" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="3" weight="2" schema="CR">ç<rhyme label="b" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg></w> <w n="6.5" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg><pgtc id="4" weight="2" schema="CR">gu<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="7.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="b" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="8.3">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt</w> <w n="8.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">gu<rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="9.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="9.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5" punct="pe:8">p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="5" weight="3" schema="CGR">ti<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>b<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="5" weight="3" schema="CGR">ti<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="11.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7" punct="vg:8">v<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="12.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.4">d</w>’<w n="12.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="7" weight="2" schema="CR">pp<rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="13.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="13.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="13.6">l</w>’<w n="13.7" punct="vg:8"><pgtc id="6" weight="0" schema="[R"><rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">s</w>’<w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="14.6" punct="pt:8"><pgtc id="7" weight="2" schema="[CR">p<rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>