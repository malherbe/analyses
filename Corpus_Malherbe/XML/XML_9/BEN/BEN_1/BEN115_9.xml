<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN115" modus="sm" lm_max="7" metProfile="7" form="suite de distiques" schema="5((aa))" er_moy="1.6" er_max="4" er_min="0" er_mode="0(2/5)" er_moy_et="1.5" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
					<head type="main">Épitaphe d’un Comédien.</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg></w> <w n="1.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.4" punct="vg:7">C<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="6">i</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">s</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="2.6" punct="dp:7">bi<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7" punct="dp">en</seg></rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">br<seg phoneme="u" type="vs" value="1" rule="BEN__4" place="2">oü</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4" punct="vg:7">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="4.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="vg:7">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg><pgtc id="2" weight="2" schema="CR">nn<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">T<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="3" weight="2" schema="CR">f<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.3">qu</w>’<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="6.5" punct="pt:7"><pgtc id="3" weight="2" schema="[CR">f<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.7" punct="vg:7">h<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="7" met="7"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="8.8" punct="pv:7">pl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="9" num="1.9" lm="7" met="7"><w n="9.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="9.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<pgtc id="5" weight="4" schema="VR"><seg phoneme="u" type="vs" value="1" rule="BEN__4" place="6">oü</seg><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="1.10" lm="7" met="7"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="10.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="10.5">r<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="10.7" punct="pt:7">j<pgtc id="5" weight="4" schema="VR"><seg phoneme="u" type="vs" value="1" rule="BEN__4" place="6">oü</seg><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>