<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><head type="main_subpart">XVIII</head><head type="main_subpart">A FIGARO</head><div type="poem" key="BUS20" modus="cm" lm_max="12" metProfile="6+6" form="sonnet peu classique" schema="abab baab ccd dee" er_moy="1.43" er_max="6" er_min="0" er_mode="0(4/7)" er_moy_et="2.06" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
						<head type="number">II</head>
						<lg n="1" rhyme="abab">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="1" punct="pe">ai</seg></w> ! <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="1.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>pt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="1.6" punct="pt:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt" caesura="1">u</seg>s</w>.<caesura></caesura> <w n="1.7">P<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="1.8">j</w>’<w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="1.10">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>r<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405" place="5" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="Lc">e</seg>lqu</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.9" punct="ps:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="3.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="3.5" punct="ps:6">b<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ps in" caesura="1">a</seg>ts</w>…<caesura></caesura> (<w n="3.6">c</w>’<w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="3.9" punct="pf:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></pgtc></w>)</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.6" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp" caesura="1">i</seg>t</w> :<caesura></caesura> <w n="4.7">N</w>’<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7" mp="Lp">e</seg>st</w>-<w n="4.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="4.11" punct="vg:12">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						</lg>
						<lg n="2" rhyme="baab">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="pi:2">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="pi">eu</seg>r</w> ? <w n="5.2">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="5.3" punct="pe:4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4" punct="pe">e</seg>d</w> ! <w n="5.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="5.5" punct="pe:6">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe" caesura="1">eu</seg></w> !<caesura></caesura> <w n="5.6">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="6.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="6.7" punct="pe:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pe">ain</seg></rhyme></pgtc></w> !</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="7.5">n</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462" place="8" mp="M">u</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="7.8" punct="ps:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>d<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="ps">ain</seg></rhyme></pgtc></w>…</l>
							<l n="8" num="2.4" lm="12" met="6+6">— <w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="8.4" punct="pi:6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pi ti" caesura="1">on</seg>c</w> ?<caesura></caesura> — <w n="8.5" punct="vg:9">G<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">am</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg></w>, <w n="8.6">c</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="8.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.9" punct="pe:12">f<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="9.2">l<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.5" punct="ps:6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" punct="ps" caesura="1">è</seg>s</w>…<caesura></caesura> <hi rend="ital"> <w n="9.6" punct="vg:12">C<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <choice reason="analysis" type="false_rhyme" hand="RR"><sic>Gallegas</sic><corr source="none"><w n="9.8">G<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">g<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>s</rhyme></pgtc></w></corr></choice></hi>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="10.2" punct="pe:3">l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="10.3">ç<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.4">n</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>t</w><caesura></caesura> <w n="10.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="10.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.9">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.10" punct="vg:12">M<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="5" weight="2" schema="CR">g<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</rhyme></pgtc></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">G<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="11.5" punct="tc:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="ti">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="11.9">m</w>’<w n="11.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="4" rhyme="dee">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="12.8" punct="pt:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rb<pgtc id="7" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ll<rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</rhyme></pgtc></w></l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="14.5" punct="vg:6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>s<seg phoneme="ø" type="vs" value="1" rule="403" place="6" punct="vg" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="14.8" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>p<pgtc id="7" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ll<rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
						</lg>
					</div></body></text></TEI>