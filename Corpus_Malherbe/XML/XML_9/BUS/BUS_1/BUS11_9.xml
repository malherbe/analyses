<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS11" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="3(abba) 3(abab)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(7/12)" er_moy_et="0.92" qr_moy="0.0" qr_max="C0" qr_mode="0(12/12)" qr_moy_et="0.0">
					<head type="number">X</head>
					<head type="main">DON SÉBASTIEN</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">Om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2" punct="vg:5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="1.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="2.6" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg">e</seg>r</w>, <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.8" punct="pt:8"><pgtc id="2" weight="2" schema="[CR">s<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="3.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>s</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.5">t</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="257" place="8">eoi</seg>r</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.6" punct="pt:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="o" type="vs" value="1" rule="444" place="4">O</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg></w> <w n="6.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>mm<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="7.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>s</w>-<w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="8.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="8.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="5" weight="2" schema="CR">ll<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>s</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR">ll<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w>-<w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6" punct="pi:8">v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8">— <w n="13.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="13.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="13.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="pt:8">m<pgtc id="7" weight="1" schema="GR">i<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.6" punct="pt:8"><pgtc id="8" weight="0" schema="[R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="15.3" punct="pt:4">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>r</w>. <w n="15.4">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.5">S<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<pgtc id="7" weight="1" schema="GR">i<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">N</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="16.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="16.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.7" punct="pt:8">d<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="17.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>q</w> <w n="17.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="17.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nni<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="18.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="18.5" punct="vg:8">m<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg">en</seg>d</w>, <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="19.4" punct="vg:6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="19.5">P<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abba">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="21.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">l</w>’<w n="21.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">en</seg>ds</w>, <w n="21.6">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="21.7" punct="vg:8">bl<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3" punct="vg:5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="22.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="22.6">j<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="23.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="23.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2" punct="vg:4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.4">R<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="24.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6">j</w>’<w n="24.7" punct="pe:8"><pgtc id="11" weight="0" schema="[R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<placeName>Bélem</placeName>.
					</closer>
				</div></body></text></TEI>