<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS DE TOUS LES PAYS</head><div type="poem" key="BUS33" modus="sp" lm_max="8" metProfile="8, 4" form="suite de strophes" schema="6(abab) 3(abba)" er_moy="1.11" er_max="6" er_min="0" er_mode="0(10/18)" er_moy_et="1.52" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
					<head type="main">CHANSON NORMANDE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Je vois la fille au capitaine <lb></lb>
									A sa fenêtre, à s’y peigner.
								</quote>
								<bibl>(<hi rend="ital">Ballade populaire</hi>).</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8">— <w n="1.1" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="3" punct="vg">o</seg>t</w>, <w n="1.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.5" punct="vg:8">h<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w>-<w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="2.7" punct="pi:8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pi">ain</seg></rhyme></pgtc></w> ?</l>
						<l n="3" num="1.3" lm="4" met="4"><space unit="char" quantity="8"></space>— <w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4">l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">F<seg phoneme="œ" type="vs" value="1" rule="304" place="1">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="4.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8">— <w n="5.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="5.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w>-<w n="5.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.7" punct="vg:8">ch<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.2" punct="pi:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pi">o</seg>t</rhyme></pgtc></w> ?</l>
						<l n="7" num="2.3" lm="8" met="8">— <w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.3">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="7.4">l</w>’<w n="7.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.6">r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="8.6" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8">— <w n="9.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="vg">o</seg>t</w>, <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w>-<w n="9.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6">t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="10.6" punct="pi:8">v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="248" place="8" punct="pi">œu</seg>x</rhyme></pgtc></w> ?</l>
						<l n="11" num="3.3" lm="4" met="4"><space unit="char" quantity="8"></space>— <w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="11.5" punct="pt:4">y<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="12.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8">— <w n="13.1">N</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w>-<w n="13.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="13.4" punct="vg:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg">en</seg></w>, <w n="13.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.6" punct="pi:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pi">o</seg>t</rhyme></pgtc></w> ?</l>
						<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space>— <w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="14.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">J</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="15.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="7" weight="2" schema="CR">gl<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="16.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="16.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="5">y</seg>s</w> <w n="16.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5">j</w>’<w n="16.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="17.4" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="17.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.7" punct="pv:8"><pgtc id="9" weight="2" schema="[CR">s<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pv">œu</seg>r</rhyme></pgtc></w> ;</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="18.4">c</w>’<w n="18.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="18.7">j<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="19" num="5.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3" punct="pt:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="9" weight="2" schema="CR">c<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w>-<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="20.3">qu</w>’<w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="20.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="20.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.7">l</w>’<w n="20.8" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="21.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="11" weight="2" schema="CR">ss<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg></rhyme></pgtc></w> ;</l>
						<l n="22" num="6.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="22.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2" punct="pe:4">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="23.2" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>s</w>, <w n="23.3">j</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="23.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="11" weight="2" schema="CR">c<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.6" punct="pt:8">t<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abba">
						<l n="25" num="7.1" lm="8" met="8">— <w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="25.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="25.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="25.4" punct="pv:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="13" weight="2" schema="CR">l<rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pv">o</seg>t</rhyme></pgtc></w> ;</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3" punct="vg:4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rt</w>, <w n="26.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="26.6">t<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="27" num="7.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="27.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="27.4">m<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="28.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="28.6" punct="pt:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="13" weight="2" schema="CR">gl<rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="29.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="29.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="29.4" punct="pt:8">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><pgtc id="15" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="15" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="30" num="8.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="30.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="30.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="249" place="2" punct="vg">œu</seg>r</w>, <w n="30.3" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="31.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="31.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="31.6">p<pgtc id="15" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<rhyme label="a" id="15" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="32.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="32.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="32.6" punct="pt:8">cl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>ts</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="abba">
						<l n="33" num="9.1" lm="8" met="8"><w n="33.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="33.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="33.3">cl<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="33.5" punct="vg:8">v<pgtc id="17" weight="0" schema="R"><rhyme label="a" id="17" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="34" num="9.2" lm="8" met="8"><w n="34.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="34.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="34.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="34.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="34.6" punct="pe:8"><pgtc id="18" weight="2" schema="[CR">t<rhyme label="b" id="18" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
						<l n="35" num="9.3" lm="4" met="4"><space unit="char" quantity="8"></space>— <w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">l</w>’<w n="35.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="35.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg><pgtc id="18" weight="2" schema="CR">t<rhyme label="b" id="18" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</rhyme></pgtc></w></l>
						<l n="36" num="9.4" lm="8" met="8"><w n="36.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="36.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="36.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="36.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="36.5" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="17" weight="0" schema="R"><rhyme label="a" id="17" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Côtes normandes</placeName>,
							<date when="1880">1880</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>