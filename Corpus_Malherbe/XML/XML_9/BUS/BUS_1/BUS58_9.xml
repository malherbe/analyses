<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRISTESSES ET JOIES</head><div type="poem" key="BUS58" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)" er_moy="0.57" er_max="2" er_min="0" er_mode="0(10/14)" er_moy_et="0.9" qr_moy="0.0" qr_max="C0" qr_mode="0(14/14)" qr_moy_et="0.0">
					<head type="main">LES TROIS CHEVEUX BLANCS</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="1.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6" punct="vg:8">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="2.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="2.4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="2.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="2.6">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cs</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="3.5">j</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="3.8" punct="dp:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="8" met="8">— <w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="4.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6" punct="pe:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="5.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="5.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7" punct="pe:8"><pgtc id="3" weight="2" schema="[CR">tr<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg pe">e</seg></rhyme></pgtc></w>, !</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.5" punct="pe:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg pe">an</seg>s</rhyme></pgtc></w>, !</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="8" num="2.4" lm="8" met="8">— <w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="8.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.6" punct="pe:8"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="9.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.4" punct="vg:5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>t</w>, <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="pt:8">g<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rs</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="10.5">r<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="10.6">bl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cs</rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5" punct="dp:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="12" num="3.4" lm="8" met="8">— <w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="12.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="12.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.6" punct="pe:8"><pgtc id="6" weight="0" schema="[R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.6">l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="14.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg></w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6" punct="pe:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rm<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="15.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="15.4">ph<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.6">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="15.7" punct="dp:8">ch<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></pgtc></w> :</l>
						<l n="16" num="4.4" lm="8" met="8">— <w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="16.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="16.6" punct="pe:8"><pgtc id="8" weight="0" schema="[R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="17.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="17.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="6">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR">g<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="18.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ts</w> <w n="19.5" punct="dp:8">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg><pgtc id="9" weight="2" schema="CR">g<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></pgtc></w> :</l>
						<l n="20" num="5.4" lm="8" met="8">— <w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="20.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="20.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="20.6" punct="pe:8"><pgtc id="10" weight="0" schema="[R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="21.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rds</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="21.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.7" punct="vg:8"><pgtc id="11" weight="0" schema="[R" part="1"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="22.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="22.6" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="23.2">j</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="23.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="23.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="23.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="23.7" punct="dp:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="24" num="6.4" lm="8" met="8">— <w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="24.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="24.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="24.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="24.6" punct="pe:8"><pgtc id="12" weight="0" schema="[R" part="1"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="25.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="dp">e</seg>nt</w> : <w n="25.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="25.5">l</w>’<w n="25.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="13" weight="2" schema="CR" part="1">r<rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="26.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="26.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="26.6" punct="pv:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="14" weight="2" schema="CR" part="1">s<rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="pv">en</seg>s</rhyme></pgtc></w> ;</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="27.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="27.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="27.5" punct="pi:8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="13" weight="2" schema="CR" part="1">r<rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="28" num="7.4" lm="8" met="8">— <w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2" punct="ps:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="ps">i</seg>s</w>… <w n="28.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="28.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="28.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg><pgtc id="14" weight="2" schema="C[R" part="1">s</pgtc></w> <w n="28.6" punct="pe:8"><pgtc id="14" weight="2" schema="C[R" part="2"><rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1856">1856</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>