<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO17" modus="sp" lm_max="7" metProfile="7, 5" form="strophe unique" schema="1(ababaab)" er_moy="2.0" er_max="3" er_min="1" er_mode="2(2/4)" er_moy_et="0.71" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
					<head type="main">IN-PROMPTU</head>
					<head type="sub_1"><hi rend="ital">Chanté dans la maison de M. le Marquis de L***, <lb></lb>à V***, le jour qu’on y pendit la Crémaillère.</hi></head>
					<head type="tune">Sur l’air : <hi rend="ital">La bonne aventure, ô gué.</hi></head>
					<lg n="1" type="septain" rhyme="ababaab">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <hi rend="ital"><w n="1.4" punct="vg:7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="1.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="1" weight="2" schema="CR">c<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></pgtc></w></hi>,</l>
						<l n="2" num="1.2" lm="5" met="5"><space unit="char" quantity="4"></space><w n="2.1">D<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.3" punct="pv:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">D<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="vg:7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg><pgtc id="1" weight="2" schema="CR">c<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="4"></space><w n="4.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="4.2" punct="pe:5">pl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="5.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.5" punct="vg:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="6.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="6.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.6" punct="vg:7"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg><pgtc id="3" weight="2" schema="CR">c<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></pgtc></w>,</l>
						<l rhyme="none" n="7" num="1.7" lm="7" met="7"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3" punct="vg:5">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="7.5" punct="vg:7">gu<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
						<l n="8" num="1.8" lm="5" met="5"><space unit="char" quantity="4"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3" punct="pe:5">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg><pgtc id="2" weight="3" schema="GR">ill<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>