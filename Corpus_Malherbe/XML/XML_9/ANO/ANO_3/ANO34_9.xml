<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO34" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababccdeed)" er_moy="2.0" er_max="8" er_min="0" er_mode="0(3/5)" er_moy_et="3.1" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
					<head type="main">ON FAIT CE QU’ON PEUT</head>
					<lg n="1" type="dizain" rhyme="ababccdeed">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">Bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">cr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="2.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="2.6" punct="vg:8">G<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.8" punct="pt:8"><pgtc id="1" weight="2" schema="[CR">d<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2">s</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="4.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="4.8" punct="pv:8">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2" punct="vg:3">C<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="5.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">qu<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="281" place="8">oi</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="6.5" punct="vg:8">s<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7" punct="dp:8">t<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3" punct="pe:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>r<pgtc id="5" weight="8" schema="CVCR">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4" punct="vg:8">n<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg><pgtc id="5" weight="8" schema="CVCR">ss<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">Bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pr<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" punct="vg">un</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5">f<seg phoneme="a" type="vs" value="1" rule="193" place="6">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.6" punct="pt:8">pr<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>