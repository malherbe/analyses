<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO64" modus="sm" lm_max="7" metProfile="7" form="strophe unique" schema="1(ababccddede)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(6/6)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
					<head type="main">LE FIN MENTEUR</head>
					<lg n="1" type="onzain" rhyme="ababccddede">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2" punct="vg:3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t</w> <w n="2.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.4" punct="dp:7">ph<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="7" met="7">« <w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>f</w> <w n="3.2" punct="vg:3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" punct="vg">e</seg>ct</w>, <w n="3.3" punct="ps:4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" punct="ps">e</seg></w>… <w n="3.4" punct="ps:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="ps in">oi</seg>s</w>… ‒ <w n="3.5" punct="pi:7">Qu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="281" place="7" punct="pi">oi</seg></rhyme></pgtc></w> ?</l>
						<l n="4" num="1.4" lm="7" met="7">» ‒ <w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <hi rend="ital"><w n="4.6" punct="pt:7">v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w></hi>.</l>
						<l n="5" num="1.5" lm="7" met="7">» ‒ <w n="5.1" punct="pi:2">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2" punct="pi in">en</seg></w> ? ‒ <w n="5.2">D<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="5.3" punct="vg:5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5" punct="pt:7">cr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pt">oi</seg></rhyme></pgtc></w>. »</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="6.4">d</w>’<w n="6.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ffr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1">P<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rg<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="7.3" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp in in">i</seg>t</w> : ‒ « <w n="7.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">A</seg>h</w> ! <w n="7.5" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="7" met="7">» <w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.4" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.6">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="8.8" punct="vg:7">cl<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="9" num="1.9" lm="7" met="7">» <w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="9.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="9.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="9.6" punct="pt:7">s<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="7" punct="pt">e</seg>c</rhyme></pgtc></w>.</l>
						<l n="10" num="1.10" lm="7" met="7">» ‒ <w n="10.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="10.2" punct="dp:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="dp">on</seg></w> : <w n="10.3">c</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="10.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="10.8">p<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="1.11" lm="7" met="7">» <w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="11.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">fr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="11.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="7" punct="pt">e</seg>c</rhyme></pgtc></w>. »</l>
					</lg>
				</div></body></text></TEI>