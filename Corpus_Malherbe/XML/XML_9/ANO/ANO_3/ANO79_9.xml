<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO79" modus="cp" lm_max="12" metProfile="8, 6+6" form="strophe unique" schema="1(abaab)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(3/3)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(3/3)" qr_moy_et="0.0">
					<head type="main">ÉPIGRAMME</head>
					<lg n="1" type="quintil" rhyme="abaab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="1.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6" caesura="1">om</seg></w><caesura></caesura> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="1.10" punct="vg:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.5" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>ls</w>, <w n="2.6" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="dp">oi</seg>t</rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space>« <w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="3.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="3.5" punct="pt:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="4" num="1.4" lm="12" met="6+6">» ‒ <w n="4.1" punct="vg:2">M<seg phoneme="œ" type="vs" value="1" rule="151" place="1" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>r</w>, » <w n="4.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="4.3" punct="vg:6">D<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="4.4">c<seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.5" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space>« <w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>r</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="5.8" punct="pi:8">f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pi">ai</seg>t</rhyme></pgtc></w> ? »</l>
					</lg>
				</div></body></text></TEI>