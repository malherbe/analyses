<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">L’ANGOISSE</head><div type="poem" key="DLR850" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2(abba) 3(abab)" er_moy="0.8" er_max="2" er_min="0" er_mode="0(6/10)" er_moy_et="0.98" qr_moy="0.0" qr_max="C0" qr_mode="0(10/10)" qr_moy_et="0.0">
					<head type="main">MON DIEU…</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="1.4">f<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">J<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="2.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="2.6" punct="vg:8"><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>s<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="4.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">cr<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> <w n="4.7" punct="vg:8">pl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.6">h<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w>-<w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="6.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="10" weight="2" schema="CR">ss<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">R<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="441" place="8">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><hi rend="ital"><w n="8.1" punct="vg:2">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w></hi>, <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <hi rend="ital"><w n="8.6" punct="pt:8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7"><pgtc id="10" weight="2" schema="[CR">s<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></pgtc></w></hi>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>r</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4" punct="vg:5">s<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="9.5">c</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="9.7" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" qr="E0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>r</w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="10.5" punct="vg:4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.7">qu</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.9" punct="pi:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pi">oi</seg>t</rhyme></pgtc></w> ?</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="11.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="11.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" qr="E0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Th<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.5">cr<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="12.6">qu</w>’<w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="12.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.9" punct="pi:8">d<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pi">oi</seg>gt</rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="13.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="13.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">In</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.4" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="7" weight="2" schema="CR">n<rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="15.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="15.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="15.8"><pgtc id="7" weight="2" schema="[CR">n<rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="16.3">n</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="16.6" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="17.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="17.3" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="a" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="18.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="18.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="18.5" punct="pt:8">r<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1" punct="tc:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg ti">ai</seg>s</w>, — <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="19.3" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>r</w>, <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="19.5" punct="pe:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="8" weight="2" schema="CR">m<rhyme label="a" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pe ti">en</seg>t</rhyme></pgtc></w> ! —</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="20.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="20.6" punct="pt:8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>