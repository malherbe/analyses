<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR679" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="2(abab) 2(abba)" er_moy="2.25" er_max="8" er_min="0" er_mode="0(4/8)" er_moy_et="2.9" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
					<head type="main">ANNIVERSAIRE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="1.3">d</w>’<w n="1.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="pt:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="2.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="2.5" punct="vg:6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="9">ue</seg>il</w> <w n="2.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="2" weight="2" schema="CR">ff<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.8" punct="pt:8">t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="4.2" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="pe">an</seg></w> ! <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="2" weight="2" schema="CR">f<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg><pgtc id="3" weight="8" schema="CVCR">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="6.4" punct="vg:5">vi<seg phoneme="e" type="vs" value="1" rule="383" place="4" mp="M">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>r</w>, <w n="6.5" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="6.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="6.8" punct="vg:9">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>t</w>, <w n="6.9">j<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="Lc">u</seg>squ</w>’<w n="6.10"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="6.11">b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t</rhyme></pgtc></w></l>
						<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg><pgtc id="3" weight="8" schema="CVCR">tt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.7">d</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="DLR679_1" place="7">a</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">oû</seg>t</rhyme></pgtc></w></l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.4" punct="vg:4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8" punct="pe:8">f<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>ls</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="10.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.6" punct="vg:6">bl<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.10">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="10.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="10.12" punct="pe:12">bl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg></rhyme></pgtc></w> !</l>
						<l n="11" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.6" punct="pt:8">f<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l</w>’<w n="12.4" punct="ps:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="ps">é</seg></w>… <w n="12.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="12.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="7">e</seg>s</w> <w n="12.9">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="13.2" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="pe ps">an</seg></w> !… <w n="13.3">T<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="13.6" punct="pt:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="7" weight="2" schema="CR">n<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="14.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r<pgtc id="8" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></pgtc></w> <w n="14.9" punct="pt:12"><pgtc id="8" weight="6" schema="V[CR" part="2">l<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pt">à</seg></rhyme></pgtc></w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space>— <w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="15.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w>-<w n="15.6" punct="pt:8">d<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="pt">à</seg></rhyme></pgtc></w>.</l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="16.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR" part="1">n<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>. »</l>
					</lg>
				</div></body></text></TEI>