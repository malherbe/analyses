<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">MÉDITATIONS</head><div type="poem" key="DLR698" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="3(abba) 2(abab)" er_moy="0.4" er_max="2" er_min="0" er_mode="0(8/10)" er_moy_et="0.8" qr_moy="0.5" qr_max="N5" qr_mode="0(9/10)" qr_moy_et="1.5">
					<head type="main">FANTÔME</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>t</w> : « <w n="1.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7" punct="pe:8">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></pgtc></w> ! »</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="2.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>t</w> : « <w n="2.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6">en</seg>s</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="2.8" punct="pe:8">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> ! »</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="3.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.8" punct="vg:8">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:3">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3" punct="vg">en</seg>t</w>, <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="4.5">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></pgtc></w></l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="5.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="5.3" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="5.4" punct="vg:5">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5" punct="vg">en</seg>t</w>, <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6">m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.5" punct="pi:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg></rhyme></pgtc></w> ?</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="7.7">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="4" weight="2" schema="CR">c<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.5" punct="vg:6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.7" punct="vg:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="10.2" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.3" punct="vg:4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="11.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.7"><pgtc id="5" weight="0" schema="[R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4" punct="dp:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="dp in">oi</seg></w> : « <w n="12.5" punct="vg:8">M<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></rhyme></pgtc></w> »,</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w>-<w n="13.4" punct="dp:6"><seg phoneme="i" type="vs" value="1" rule="497" place="6" punct="dp">y</seg></w> : <w n="13.5" punct="pe:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="14.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="14.6" punct="vg:8">b<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6" punct="vg:8">p<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="16.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.6" punct="vg:8">r<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="17.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="17.6" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" punct="vg">en</seg></w>, <w n="17.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="17.8" punct="vg:8">c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="18.4" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="18.5" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="18.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="18.7" punct="pt:8">m<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="19.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.6" punct="vg:8">p<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">h<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.5" punct="pe:8">p<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>