<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR680" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="3[aaa] 3[abbba]" er_moy="2.53" er_max="6" er_min="0" er_mode="0(6/15)" er_moy_et="2.58" qr_moy="0.0" qr_max="C0" qr_mode="0(15/15)" qr_moy_et="0.0">
					<head type="main">J’AI RETROUVÉ L’AUTOMNE</head>
					<lg n="1" type="regexp" rhyme="aaaa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.9"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="7">y</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.6">v<pgtc id="1" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.5">p<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="a" id="2" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="2" type="regexp" rhyme="bbba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.8" punct="vg:12"><pgtc id="3" weight="2" schema="[CR">v<rhyme label="b" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="6.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4" punct="vg:5">r<seg phoneme="y" type="vs" value="1" rule="457" place="5" punct="vg">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr</w>’<w n="6.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="b" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3" punct="vg:3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>r</w>, <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6" punct="vg:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7" punct="vg">en</seg>t</w>, <w n="7.7" punct="vg:8">c<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="8.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ff<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<rhyme label="a" id="2" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="3" type="regexp" rhyme="aaaa">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>t</w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.7">li<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="9.8" punct="vg:10">ch<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>s</w>, <w n="9.9">qu</w>’<w n="9.10" punct="pi:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">im</seg>p<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
						<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="10.4">t</w>-<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="10.7" punct="pi:8">m<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="11" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.6">p<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.5">j<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="5" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
					</lg>
					<lg n="4" type="regexp" rhyme="bbba">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.2">f<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.4">j</w>’<w n="14.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg></w>, <w n="15.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="15.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="16.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="5" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
					</lg>
					<lg n="5" type="regexp" rhyme="aaaa">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="17.3">qu</w>’<w n="17.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="17.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="17.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="17.7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.8">m</w>’<w n="17.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="17.10" punct="vg:12">r<pgtc id="7" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="7" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.4">m<pgtc id="7" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></pgtc></w> <w n="18.5" punct="vg:8"><pgtc id="7" weight="6" schema="V[CR" part="2">v<rhyme label="a" id="7" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="19.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="19.3">j</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="19.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="19.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="7" weight="2" schema="CR" part="1">v<rhyme label="a" id="7" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2">n</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="20.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="20.5" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="20.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="20.7">n</w>’<w n="20.8"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="6">e</seg>s</w> <w n="20.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="20.10"><pgtc id="8" weight="2" schema="[CR" part="1">l<rhyme label="a" id="8" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="6" type="regexp" rhyme="bbba">
						<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="21.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w>-<w n="21.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="21.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="21.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="21.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.9" punct="vg:12"><pgtc id="9" weight="6" schema="[VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="b" id="9" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1" punct="vg:2">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="22.2" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>, <w n="22.3" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<pgtc id="9" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>t<rhyme label="b" id="9" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="23.5">s</w>’<w n="23.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="9" weight="2" schema="CR" part="1">t<rhyme label="b" id="9" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="24.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="24.6">l</w>’<w n="24.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w>-<w n="24.8" punct="pi:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="8" weight="2" schema="CR" part="1">l<rhyme label="a" id="8" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="pi">à</seg></rhyme></pgtc></w> ?</l>
					</lg>
				</div></body></text></TEI>