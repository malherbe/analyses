<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A MES SŒURS</head><div type="poem" key="DLR689" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="2(abab) 1(abba)" er_moy="0.67" er_max="2" er_min="0" er_mode="0(4/6)" er_moy_et="0.94" qr_moy="1.67" qr_max="N5" qr_mode="0(4/6)" qr_moy_et="2.36">
					<head type="main">A MA SŒUR SUZANNE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="1.10">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.8">L</w>’<w n="2.9" punct="vg:12"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>f</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.4" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.6" punct="vg:8">s<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</w>, <w n="3.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="3.8">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.9" punct="vg:12">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="4.6">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>f</rhyme></pgtc></w></l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="5.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="5.3">c</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="5.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.9" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="6.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rl<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="6.5" punct="vg:9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9" punct="vg">en</seg>t</w>, <w n="6.6" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="7.3">n</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="7.8">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.11">d<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="7.12">f<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">fr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">N</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="9.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="9.4" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="9.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="9.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="9.9">l</w>’<w n="9.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="9.11" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="10" mp="M">em</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="10.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="10.9" punct="vg:12">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rc<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="345" place="12" punct="vg">ue</seg>il</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="11.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="11.7" punct="vg:6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="11.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="11.10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="11.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="11.12">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="11.13" punct="vg:12">d<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="12" punct="vg">eu</seg>il</rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="12.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					</lg>
				</div></body></text></TEI>