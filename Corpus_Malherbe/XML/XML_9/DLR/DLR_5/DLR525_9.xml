<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR525" modus="cm" lm_max="12" metProfile="6=6" form="suite périodique" schema="3(abbaa)" er_moy="2.22" er_max="6" er_min="0" er_mode="2(4/9)" er_moy_et="2.2" qr_moy="2.78" qr_max="M15" qr_mode="0(6/9)" qr_moy_et="4.78">
					<head type="main">SOUPIR PRINTANIER</head>
					<lg n="1" type="quintil" rhyme="abbaa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="1.2">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>fl<seg phoneme="ø" type="vs" value="1" rule="405" place="8" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="1.5" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">j<rhyme label="a" id="1" gender="m" type="a" qr="N5"><seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pe">à</seg></rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">aî</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="2.4" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290" place="10">aon</seg>s</w> <w n="2.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>rs</w> <w n="2.9" punct="vg:12">qu<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="3.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rds</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.7">l</w>’<w n="3.8">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.9">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="3.11">bl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="4.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="4.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="4.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.8" punct="ps:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg><pgtc id="1" weight="2" schema="CR">ge<rhyme label="a" id="1" gender="m" type="e" qr="N5"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="ps">â</seg>t</rhyme></pgtc></w>…</l>
						<l n="5" num="1.5" lm="12" met="6+6">— <w n="5.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="5.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>fl<seg phoneme="ø" type="vs" value="1" rule="405" place="8" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="5.5" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">j<rhyme label="a" id="1" gender="m" type="a" qr="N5"><seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pt">à</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="abbaa">
						<l n="6" num="2.1" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4" punct="vg:6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="C">y</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.9">l</w>’<w n="6.10" punct="pt:12"><pgtc id="2" weight="6" schema="[VCR"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="a" id="2" gender="f" type="a" qr="M15"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="2.2" lm="12" met="6+6"><w n="7.1" punct="vg:3">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="7.3" punct="vg:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="7.4">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7" mp="M">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.6">l</w>’<w n="7.7" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="3" weight="2" schema="CR">f<rhyme label="b" id="3" gender="m" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
						<l n="8" num="2.3" lm="12" mp6="F" met="4+4+4"><w n="8.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2" punct="vg:4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="8.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="8.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="8" caesura="2">eu</seg>r</w><caesura></caesura> <w n="8.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="8.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="8.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="3" weight="2" schema="CR">f<rhyme label="b" id="3" gender="m" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>d</rhyme></pgtc></w></l>
						<l n="9" num="2.4" lm="12" met="6+6"><w n="9.1">D</w>’<w n="9.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="9.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="9.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.7">l<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="9.8" punct="ps:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<pgtc id="2" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<rhyme label="a" id="2" gender="f" type="e" qr="M15"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
						<l n="10" num="2.5" lm="12" met="6+6">— <w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.4" punct="vg:6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="C">y</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.9">l</w>’<w n="10.10" punct="pt:12"><pgtc id="2" weight="6" schema="[VCR"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="a" id="2" gender="f" type="a" qr="M15"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quintil" rhyme="abbaa">
						<l n="11" num="3.1" lm="12" met="6+6"><w n="11.1" punct="vg:2">N<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="11.2" punct="vg:4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="11.3" punct="vg:6">vi<seg phoneme="e" type="vs" value="1" rule="383" place="5" mp="M">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="11.4" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</w>, <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.7">fl<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></pgtc></w></l>
						<l n="12" num="3.2" lm="12" met="6+6"><w n="12.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="12.3">l</w>’<w n="12.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="12.6" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">ai</seg></w>,<caesura></caesura> <w n="12.7" punct="vg:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" punct="vg" mp="F">e</seg>nt</w>, <w n="12.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="12.9" punct="pv:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="b" id="5" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></pgtc></w> ;</l>
						<l n="13" num="3.3" lm="12" met="6+6"><w n="13.1" punct="vg:1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="13.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="13.3">d</w>’<w n="13.4" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.7" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">aî</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="b" id="5" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="14" num="3.4" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="14.8" punct="pe:12">c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pe">œu</seg>rs</rhyme></pgtc></w> !</l>
						<l n="15" num="3.5" lm="12" met="6+6">— <w n="15.1" punct="vg:2">N<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="15.2" punct="vg:4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="15.3" punct="vg:6">vi<seg phoneme="e" type="vs" value="1" rule="383" place="5" mp="M">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="15.4" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</w>, <w n="15.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="15.7" punct="pt:12">fl<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>