<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Six Poèmes d’Emily Brontë</head><div type="poem" key="DLR1117" modus="sp" lm_max="8" metProfile="8, 6" form="suite périodique" schema="2(abab) 1(abba)" er_moy="1.67" er_max="8" er_min="0" er_mode="0(4/6)" er_moy_et="2.92" qr_moy="4.17" qr_max="M15" qr_mode="0(3/6)" qr_moy_et="5.34">
						<head type="main">Le Vieux Stoïque</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’<w n="1.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" punct="vg">en</seg>t</w>, <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.7" punct="vg:8">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="N5"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg>t</rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="2.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="2.5" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L</w>’<w n="3.2" punct="pi:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pi">on</seg></w> ? <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">Un</seg></w> <w n="3.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.6">l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="N5"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.4">l</w>’<w n="4.5" punct="pt:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abba">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="5.7">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a" qr="M15"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</rhyme></pgtc></w></l>
							<l n="6" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>ls</w> <w n="6.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>ts</w> <w n="6.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5" punct="dp:6">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rm<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="a" qr="N5"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
							<l n="7" num="2.3" lm="8" met="8">« <w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="7.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.8" punct="vg:8">d<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="e" qr="N5"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2" punct="pe:4">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pe">é</seg></w> ! <w n="8.3">C</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.5" punct="pt:6">t<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e" qr="M15"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>t</rhyme></pgtc></w>. »</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="9.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">cl<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="10" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="10.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="10.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="10.5" punct="pt:6"><pgtc id="5" weight="8" schema="[CVCR">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="11.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="11.4" punct="pe:5">l<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pe">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="11.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="189" place="6" punct="vg">e</seg>t</w>, <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="11.7"><pgtc id="4" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="11.8" punct="vg:8"><pgtc id="4" weight="2" schema="[C[R" part="2"><rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d</w>’<w n="12.4" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg><pgtc id="5" weight="8" schema="CVCR" part="1">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>