<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Poèmes de Anna Wickham</head><head type="sub_subpart">traduits en vers libres et réguliers</head><div type="poem" key="DLR1128" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(abbacc)" er_moy="0.67" er_max="2" er_min="0" er_mode="0(2/3)" er_moy_et="0.94" qr_moy="1.67" qr_max="N5" qr_mode="0(2/3)" qr_moy_et="2.36">
						<head type="main">Envoi</head>
						<lg n="1" type="sizain" rhyme="abbacc">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg></w> ! <w n="1.3" punct="vg:3">T<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="1" weight="2" schema="CR">tr<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.6" punct="vg:8">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="N5"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="3.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.9">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="N5"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.6" punct="pi:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="5.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>