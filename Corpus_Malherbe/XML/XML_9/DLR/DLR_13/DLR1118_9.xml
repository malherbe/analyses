<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Six Poèmes d’Emily Brontë</head><div type="poem" key="DLR1118" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(abab) 1(abba)" er_moy="0.67" er_max="2" er_min="0" er_mode="0(4/6)" er_moy_et="0.94" qr_moy="0.83" qr_max="N5" qr_mode="0(5/6)" qr_moy_et="1.86">
						<head type="main">L’Amour et l’Amitié</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="2.6" punct="pt:8">h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">h<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.4" punct="vg:4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="3.5">l</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="4.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="4.6" punct="pi:8">b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>t</rhyme></pgtc></w> ?</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="5.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.6">fr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="6.5">l</w>’<w n="6.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="7.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="7.4">l</w>’<w n="7.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="7.8" punct="vg:8">s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.5" punct="pi:8">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></pgtc></w> ?</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abba">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ff<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="10.4">h<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="10.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.7" punct="pt:8"><pgtc id="6" weight="2" schema="[CR">fr<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>t</rhyme></pgtc></w>.</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="11.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</rhyme></pgtc></w></l>
							<l n="12" num="3.4" lm="8" met="8"><w n="12.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.4" punct="pt:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>