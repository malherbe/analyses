<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE II</head><head type="main_part">Honfleur</head><div type="poem" key="DLR1058" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababcdcdc)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(5/5)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
					<head type="main">Départ</head>
					<lg n="1" type="neuvain" rhyme="ababcdcdc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="2.5" punct="pt:8">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="3.4" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.6">c</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.9" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="4.4">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7" punct="pt:8">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rs</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="5.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="6.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:2">C<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="7.2" punct="ps:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="ps">e</seg>s</w>… <w n="7.3">S<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="7.4">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w> <w n="7.5">m</w>’<w n="7.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="257" place="8" punct="pi">eoi</seg>r</rhyme></pgtc></w> ?</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.6" punct="vg:6">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>t</w>, <w n="8.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.8">gu<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="9.2">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="9.3">d</w>’<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="9.5">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="9.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>