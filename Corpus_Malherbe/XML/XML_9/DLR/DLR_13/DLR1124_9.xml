<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Poèmes de Edna Saint-Vincent Millay</head><head type="sub_subpart">1892-195O</head><div type="poem" key="DLR1124" modus="cm" lm_max="12" metProfile="6−6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="0.29" er_max="2" er_min="0" er_mode="0(6/7)" er_moy_et="0.7" qr_moy="0.71" qr_max="N5" qr_mode="0(6/7)" qr_moy_et="1.75">
						<head type="main">Sonnet</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.9">n</w>’<w n="1.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="1.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="1.12">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="1.13">l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="2.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4" punct="vg:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" mp="M">è</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg" caesura="1">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="2.7">n</w>’<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="9">e</seg>s</w> <w n="2.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="2.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="2.11">b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="3.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="3.8" punct="pt:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="4.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="4.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.7">m</w>’<w n="4.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="4.10" punct="vg:12">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2" punct="vg:3">b<seg phoneme="o" type="vs" value="1" rule="315" place="2" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">pu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.6" punct="pt:9">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="pt">er</seg></w>. <w n="5.7">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.9">p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s</rhyme></pgtc></w></l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="6.2" punct="vg:2">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="6.4" punct="vg:5">g<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="6.5" punct="vg:6">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>t</w>,<caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="6.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="6.9" punct="vg:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="P">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9" punct="pt:12"><pgtc id="4" weight="0" schema="[R" part="1"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="6" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="8.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="11">œu</seg>r</w> <w n="8.9" punct="pt:12">l<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="9.5" punct="vg:7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7" punct="vg">i</seg></w>, <w n="9.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="9.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="9.8" punct="vg:10">c<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>j<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="10.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="10.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="10.7" punct="vg:12">g<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="Lc">u</seg>squ</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="11.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>x</w> <w n="11.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="11.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>p<seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="6" weight="2" schema="CR" part="1">m<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" mp6="C" met="6−6"><w n="12.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="12.4" punct="vg:8">b<seg phoneme="o" type="vs" value="1" rule="315" place="7" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>, <w n="12.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>s<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="e" id="7" gender="f" type="a" qr="N5"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="13.7" punct="vg:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="6" weight="2" schema="CR" part="1">m<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="14.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="14.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="14.8">t<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.10" punct="pt:12">h<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="e" id="7" gender="f" type="e" qr="N5"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>