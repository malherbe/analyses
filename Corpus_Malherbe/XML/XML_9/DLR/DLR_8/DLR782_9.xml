<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR782" modus="sp" lm_max="8" metProfile="8, 6, 3" form="suite de strophes" schema="9(abab) 2(abba)" er_moy="0.64" er_max="2" er_min="0" er_mode="0(15/22)" er_moy_et="0.93" qr_moy="0.23" qr_max="N5" qr_mode="0(21/22)" qr_moy_et="1.04">
				<head type="main">L’Ange Gardien</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="1.6">d</w>’<w n="1.7" punct="vg:8"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="vg:6">m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="3.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="3.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.4">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3" punct="pt:3"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.5">v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.3" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="7.6">bru<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3" punct="pt:3"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ts</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.3" punct="vg:6"><pgtc id="6" weight="2" schema="[CR">c<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="11.7" punct="vg:8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ls</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="12.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.3" punct="pt:3"><pgtc id="6" weight="2" schema="[CR">s<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="13.2" punct="dp:3">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3" punct="dp">e</seg></w> : <w n="13.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="5">e</seg>s</w> <w n="13.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="13.7" punct="pe:8">s<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>l</rhyme></pgtc></w> !</l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="14.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="14.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6" punct="pt:6">p<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.3" punct="vg:3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.6">gr<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="15.7" punct="vg:8">t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>l</rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="16.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">Om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2" punct="pt:3">bl<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="17.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg><pgtc id="9" weight="2" schema="CR">ill<rhyme label="a" id="9" gender="m" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="18.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="18.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="18.5" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>p<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="19.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="19.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.7">r<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="20.1" punct="pt:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg><pgtc id="9" weight="2" schema="CR">ill<rhyme label="a" id="9" gender="m" type="e" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abba">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="21.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="21.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="21.7" punct="vg:8">b<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="6" met="6"><w n="22.1" punct="dp:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="dp in">an</seg>t</w> : « <w n="22.2">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="22.3">m</w>’<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="22.5" punct="pe:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>l</rhyme></pgtc></w> ! »</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="23.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="23.8">m<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3" punct="pi:3"><pgtc id="11" weight="0" schema="[R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="25.3">t</w>’<w n="25.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="25.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="25.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="25.7" punct="vg:8"><pgtc id="13" weight="2" schema="CR">m<rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="vg">en</seg>s</rhyme></pgtc></w>,</l>
					<l n="26" num="7.2" lm="6" met="6"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="26.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="26.4" punct="vg:6">h<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="27.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="27.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="27.5" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="13" weight="2" schema="CR">m<rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>ts</rhyme></pgtc></w>,</l>
					<l n="28" num="7.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="28.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="28.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="28.3" punct="pt:3">pl<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="29.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="29.5">s<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></rhyme></pgtc></w></l>
					<l n="30" num="8.2" lm="6" met="6"><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="30.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="30.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="30.5" punct="pi:6">f<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="31.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="31.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t</w>, <w n="31.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.5">t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></rhyme></pgtc></w></l>
					<l n="32" num="8.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="32.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2" punct="pt:3">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>mi<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="33.2">J<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="33.3" punct="pe:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe">eu</seg>l</w> ! <w n="33.4">C<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="33.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="17" weight="2" schema="CR">l<rhyme label="a" id="17" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></rhyme></pgtc></w></l>
					<l n="34" num="9.2" lm="6" met="6"><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="34.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="34.3" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="34.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="34.5" punct="vg:6"><pgtc id="18" weight="2" schema="[CR">j<rhyme label="b" id="18" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="35.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="35.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="35.4" punct="vg:5">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="35.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="35.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="35.7" punct="vg:8"><pgtc id="17" weight="2" schema="[CR">l<rhyme label="a" id="17" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></pgtc></w>,</l>
					<l n="36" num="9.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="36.1">J<seg phoneme="u" type="vs" value="1" rule="426" place="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="36.3" punct="pt:3"><pgtc id="18" weight="2" schema="[CR">j<rhyme label="b" id="18" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="10" type="quatrain" rhyme="abab">
					<l n="37" num="10.1" lm="8" met="8"><w n="37.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="37.2" punct="vg:3">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3" punct="vg">e</seg></w>, <w n="37.3">c</w>’<w n="37.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="37.5">l</w>’<w n="37.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="19" weight="0" schema="R"><rhyme label="a" id="19" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="38" num="10.2" lm="6" met="6"><w n="38.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="38.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="38.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.4" punct="vg:6"><pgtc id="20" weight="0" schema="[R" part="1"><rhyme label="b" id="20" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="39" num="10.3" lm="8" met="8"><w n="39.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="39.2" punct="vg:3">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3" punct="vg">e</seg></w>, <w n="39.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="39.4">j<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="39.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="39.6" punct="vg:8">ci<pgtc id="19" weight="0" schema="R" part="1"><rhyme label="a" id="19" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="40" num="10.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="40.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="40.2" punct="pt:3">fl<pgtc id="20" weight="0" schema="R" part="1"><rhyme label="b" id="20" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="11" type="quatrain" rhyme="abab">
					<l n="41" num="11.1" lm="8" met="8"><w n="41.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="41.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="41.3" punct="vg:3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd</w>, <w n="41.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ls</w> <w n="41.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="41.6">s<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="41.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="41.8" punct="vg:8"><pgtc id="21" weight="2" schema="[CR" part="1">j<rhyme label="a" id="21" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</rhyme></pgtc></w>,</l>
					<l n="42" num="11.2" lm="6" met="6"><w n="42.1" punct="vg:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="42.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="42.3">n</w>’<w n="42.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="42.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="42.6" punct="ps:6">dr<pgtc id="22" weight="0" schema="R" part="1"><rhyme label="b" id="22" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
					<l n="43" num="11.3" lm="8" met="8"><w n="43.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="43.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="43.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="43.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="21" weight="2" schema="CR" part="1">j<rhyme label="a" id="21" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="44" num="11.4" lm="3" met="3"><space unit="char" quantity="10"></space><w n="44.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="44.2" punct="pt:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<pgtc id="22" weight="0" schema="R" part="1"><rhyme label="b" id="22" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>