<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR743" modus="sm" lm_max="4" metProfile="4" form="suite périodique" schema="2(abab) 1(abba) 3(aabb)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(6/12)" er_moy_et="1.0" qr_moy="0.42" qr_max="N5" qr_mode="0(11/12)" qr_moy_et="1.38">
				<head type="main">Petit Garçon et petite Fille</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="4" met="4"><w n="1.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="1.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<pgtc id="1" weight="2" schema="CR">ç<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="4" met="4"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.2" punct="vg:4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ld<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>t</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="4" met="4"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg><pgtc id="1" weight="2" schema="CR">ç<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="4" met="4"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">É</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</rhyme></pgtc></w></l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="4" met="4"><w n="5.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="5.2">f<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="4" met="4"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="4" met="4"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="4" met="4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3" punct="pt:4">f<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="aabb">
					<l n="9" num="3.1" lm="4" met="4"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3" punct="pe:4">b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pe">oi</seg>s</rhyme></pgtc></w> !</l>
					<l n="10" num="3.2" lm="4" met="4"><w n="10.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3" punct="vg:4">l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="4" met="4"><w n="11.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.2" punct="vg:4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<pgtc id="6" weight="2" schema="CR">ç<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="4" met="4"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.4" punct="pe:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg><pgtc id="6" weight="2" schema="CR">ç<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="aabb">
					<l n="13" num="4.1" lm="4" met="4"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">Â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg><pgtc id="7" weight="2" schema="CR">p<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="4" met="4"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="7" weight="2" schema="CR">p<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="4" met="4"><w n="15.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.2">f<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="4" met="4"><w n="16.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3" punct="pe:4">f<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="aabb">
					<l n="17" num="5.1" lm="4" met="4"><w n="17.1">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="17.2" punct="vg:4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ld<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>t</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="4" met="4"><w n="18.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">É</seg>t<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="4" met="4"><w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="19.2" punct="vg:4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg><pgtc id="10" weight="2" schema="CR">ch<rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="4" met="4"><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="20.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.4" punct="pe:4"><pgtc id="10" weight="2" schema="[CR">ch<rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="4" met="4">‒ <w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="21.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg><pgtc id="11" weight="2" schema="CR">s<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="4" met="4"><w n="22.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="22.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4" punct="vg:4"><pgtc id="12" weight="0" schema="[R" part="1"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="4" met="4"><w n="23.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="23.2" punct="vg:4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<pgtc id="11" weight="2" schema="CR" part="1">ç<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="6.4" lm="4" met="4"><w n="24.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.2" punct="pt:4">f<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>