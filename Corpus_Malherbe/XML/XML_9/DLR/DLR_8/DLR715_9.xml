<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR715" modus="sm" lm_max="8" metProfile="8" form="suite de distiques" schema="6((aa))" er_moy="1.0" er_max="2" er_min="0" er_mode="0(3/6)" er_moy_et="1.0" qr_moy="0.83" qr_max="N5" qr_mode="0(5/6)" qr_moy_et="1.86">
				<head type="main">LE Chat Bleu</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="1.4">M<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="1" weight="2" schema="CR">ch<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.6" punct="pt:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="1" weight="2" schema="CR">ch<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.5" punct="vg:8">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="4.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7" punct="pt:8">qu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="5.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ds</w> <w n="5.7" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="vg">e</seg>rts</rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t</w> <w n="6.4" punct="vg:5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>rs</w>, <w n="6.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>t</w> <w n="6.6" punct="pt:8"><pgtc id="3" weight="2" schema="[CR">v<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rts</rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3">l</w>’<w n="7.4" punct="dp:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="dp">er</seg></w> : <w n="7.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="vg:8">r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="vg:8">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="9.4" punct="vg:4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>g</w>, <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.8" punct="vg:8"><pgtc id="5" weight="2" schema="[CR">r<rhyme label="a" id="5" gender="m" type="a" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>d</rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="10.2" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.4" punct="vg:8">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="m" type="e" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7">en</seg>t</w> <w n="11.6">l</w>’<w n="11.7" punct="vg:8"><pgtc id="6" weight="0" schema="[R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="12.3" punct="vg:3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="12.4" punct="vg:5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>, <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.7" punct="pe:8">cr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>