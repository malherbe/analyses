<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR761" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1[abab] 1[abbab] 1[ababa] 1[abba] 1[aa]" er_moy="6.73" er_max="44" er_min="0" er_mode="2(5/11)" er_moy_et="12.4" qr_moy="1.36" qr_max="N5" qr_mode="0(8/11)" qr_moy_et="2.23">
				<head type="main">Le Cerceau</head>
				<lg n="1" type="regexp" rhyme="ababab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="1" weight="2" schema="CR">c<rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="N5"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">l</w>’<w n="2.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">ll<rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.7">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="3.8" punct="pt:8"><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="N5"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="4.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4"><pgtc id="3" weight="14" schema="[CV[CVCR" part="1">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></pgtc></w> <w n="5.5" punct="vg:8"><pgtc id="3" weight="14" schema="[CV[CVCR" part="2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="vg:1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="6.2">c</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.8" punct="pt:8">r<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="bababa">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="7.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">s</w>’<w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="8.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="8.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="8.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w>-<w n="8.5">t</w>-<w n="8.6" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>l</w>, <w n="8.7"><pgtc id="3" weight="14" schema="[CV[CVCR" part="1">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></pgtc></w> <w n="8.8" punct="vg:8"><pgtc id="3" weight="14" schema="[CV[CVCR" part="2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.6" punct="pi:8">r<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.6" punct="vg:8">b<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>t<rhyme label="a" id="5" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.2" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.3" punct="vg:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="2.6" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w>-<w n="12.3" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>r<pgtc id="5" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></pgtc></w>-<w n="12.6" punct="pi:8"><pgtc id="5" weight="6" schema="V[CR" part="2">t<rhyme label="a" id="5" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pi">on</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="3" type="regexp" rhyme="baabba">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">r<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="13.4" punct="vg:5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="vg">en</seg>t</w>, <w n="13.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="13.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>c</w> <w n="13.7">v<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="e" stanza="3" part="I" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></pgtc></w>-<w n="13.8" punct="pi:8"><pgtc id="6" weight="0" schema="R" part="2"><rhyme label="b" id="6" gender="f" type="e" stanza="3" part="F" qr="C0">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></pgtc></w> <w n="14.2"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></pgtc></w>-<w n="14.3"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="3">t</pgtc></w>-<w n="14.4" punct="pi:4"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pi">on</seg></pgtc></w> ? <w n="14.5"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></pgtc></w> <w n="14.6"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="6">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></pgtc></w>-<w n="14.7"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="7">t</pgtc></w>-<w n="14.8" punct="pi:8"><pgtc id="5" weight="44" schema="CV[CVCV[C[V[CV[CVCV[C[R" part="8"><rhyme label="a" id="5" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pi">on</seg></rhyme></pgtc></w> ?</l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6">l</w>’<w n="15.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="7" weight="2" schema="CR" part="1">ll<rhyme label="a" id="7" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="16.5" punct="vg:8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="8" weight="2" schema="CR" part="1">c<rhyme label="b" id="8" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.7">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="17.8" punct="vg:8"><pgtc id="8" weight="2" schema="CR" part="1">s<rhyme label="b" id="8" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>t</rhyme></pgtc></w>,</l>
					<l n="18" num="3.6" lm="8" met="8"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="18.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="7" weight="2" schema="CR" part="1">l<rhyme label="a" id="7" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="4" type="regexp" rhyme="aa">
					<l n="19" num="4.1" lm="8" met="8">‒ <w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="19.3">c</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="19.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="19.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="19.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR" part="1">d<rhyme label="a" id="9" gender="m" type="a" stanza="5" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="20.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="20.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.6" punct="vg:7">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>s</w>, <w n="20.7" punct="pi:8"><pgtc id="9" weight="2" schema="[CR" part="1">d<rhyme label="a" id="9" gender="m" type="e" stanza="5" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg>s</rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>