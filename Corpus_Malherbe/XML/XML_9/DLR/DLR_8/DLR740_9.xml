<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR740" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="6[abab] 3[aa] 1[aabba]" er_moy="0.22" er_max="2" er_min="0" er_mode="0(16/18)" er_moy_et="0.63" qr_moy="0.56" qr_max="N5" qr_mode="0(16/18)" qr_moy_et="1.57">
				<head type="main">Le Ciel</head>
				<lg n="1" type="regexp" rhyme="ababaaaba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="1.4">c</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.8" punct="dp:8">ci<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="dp">e</seg>l</rhyme></pgtc></w> :</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.7">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5" punct="pt:8">N<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="175" place="8" punct="pt">ë</seg>l</rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.5" punct="vg:8">br<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="5.5" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="2" qr="N5"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>t</w>, <w n="6.4" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="2" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>t</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.6">d</w>’<w n="7.7" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg">en</seg>t</w>, <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="7.9">g<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.7" punct="vg:8">d<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>gt</rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="9.6">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="9.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="9.8" punct="pt:8">v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="baaaa">
					<l n="10" num="2.1" lm="8" met="8"><w n="10.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rç<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></pgtc></w></l>
					<l n="11" num="2.2" lm="8" met="8"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5"><seg phoneme="ø" type="vs" value="1" rule="247" place="6">œu</seg>fs</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7" punct="vg:8">P<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="2.3" lm="8" met="8"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.4">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.7" punct="vg:8">fl<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="13" num="2.4" lm="8" met="8"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="13.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.7" punct="vg:8">f<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>l</rhyme></pgtc></w>,</l>
					<l n="14" num="2.5" lm="8" met="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="14.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="14.4">d</w>’<w n="14.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>vr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="regexp" rhyme="ababab">
					<l n="15" num="3.1" lm="8" met="8"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="15.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="3.2" lm="8" met="8"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="16.2">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="16.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5">qu</w>’<w n="16.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="16.7" punct="pt:8">v<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="a" stanza="6" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>t</rhyme></pgtc></w>.</l>
					<l n="17" num="3.3" lm="8" met="8"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="vg:8">pl<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="3.4" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">c</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w>§ <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="18.5">l</w>’<w n="18.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="18.8" punct="pt:8">j<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="e" stanza="6" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
					<l n="19" num="3.5" lm="8" met="8"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="19.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="19.6" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="a" stanza="7" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="20" num="3.6" lm="8" met="8"><w n="20.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">c</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="20.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7" punct="pt:8">m<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="a" stanza="7" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="regexp" rhyme="ababababab">
					<l n="21" num="4.1" lm="8" met="8"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="21.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="21.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="21.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>mm<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="e" stanza="7" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="22" num="4.2" lm="8" met="8"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">d<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7" punct="pt:8">b<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="e" stanza="7" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
					<l n="23" num="4.3" lm="8" met="8"><w n="23.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="23.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="23.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="23.4">qu</w>’<w n="23.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="23.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="23.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="23.8" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="f" type="a" stanza="8" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="24" num="4.4" lm="8" met="8"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1">e</seg>s</w> <w n="24.3">qu</w>’<w n="24.4"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="24.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="24.6">gr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="24.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.8" punct="pt:8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>b<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="m" type="a" stanza="8" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</rhyme></pgtc></w>.</l>
					<l n="25" num="4.5" lm="8" met="8"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="25.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="25.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="25.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="25.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="25.7">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="f" type="e" stanza="8" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="26" num="4.6" lm="8" met="8"><w n="26.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="26.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="26.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="26.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>d<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="m" type="e" stanza="8" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
					<l n="27" num="4.7" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="27.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="27.4">s</w>’<w n="27.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="27.7">l</w>’<w n="27.8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="a" stanza="9" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="28" num="4.8" lm="8" met="8"><w n="28.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="28.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="28.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="28.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="28.7" punct="dp:8"><pgtc id="15" weight="2" schema="[CR">ch<rhyme label="b" id="15" gender="m" type="a" stanza="9" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="dp">œu</seg>r</rhyme></pgtc></w> :</l>
					<l n="29" num="4.9" lm="8" met="8">« <w n="29.1" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="pe">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="29.2" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pe">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="29.3" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pe">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="29.4" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">En</seg>c<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="e" stanza="9" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> ! »</l>
					<l n="30" num="4.10" lm="8" met="8"><w n="30.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="30.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="30.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="30.7" punct="pe:8"><pgtc id="15" weight="2" schema="[CR">c<rhyme label="b" id="15" gender="m" type="e" stanza="9" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pe">œu</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="regexp" rhyme="aabb">
					<l n="31" num="5.1" lm="8" met="8"><w n="31.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="31.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="31.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="31.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="31.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="31.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w>-<w n="31.8" punct="vg:8">p<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="f" type="a" stanza="10" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="32" num="5.2" lm="8" met="8"><w n="32.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="32.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="32.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="32.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="32.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="32.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="32.7" punct="vg:8">f<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="f" type="e" stanza="10" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="33" num="5.3" lm="8" met="8"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="33.2">l</w>’<w n="33.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="33.4">s</w>’<w n="33.5"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="17" weight="2" schema="CR">m<rhyme label="b" id="17" gender="m" type="a" stanza="10" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					<l n="34" num="5.4" lm="8" met="8"><w n="34.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="34.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="34.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="34.5" punct="pt:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="17" weight="2" schema="CR">m<rhyme label="b" id="17" gender="m" type="e" stanza="10" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="regexp" rhyme="a">
					<l n="35" num="6.1" lm="8" met="8">‒ <w n="35.1" punct="vg:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="35.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="35.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.4" punct="vg:6">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="35.5">j</w>’<w n="35.6" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="f" type="a" stanza="10" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>