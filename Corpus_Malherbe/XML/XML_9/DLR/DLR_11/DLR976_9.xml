<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR976" modus="sm" lm_max="8" metProfile="8" form="ballade non classique" schema="3(ababbcbc) bcbc" er_moy="0.43" er_max="2" er_min="0" er_mode="0(11/14)" er_moy_et="0.82" qr_moy="0.0" qr_max="C0" qr_mode="0(14/14)" qr_moy_et="0.0">
				<head type="main">BALLADE DU FISC</head>
				<lg n="1" type="huitain" rhyme="ababbcbc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>f</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.3" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="2.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5" punct="vg:8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>sc</w> <w n="4.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="4.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="4.7" punct="vg:8">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="5.6" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.3" punct="pe:4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pe">i</seg></w> ! <w n="6.4" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>r</w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="6.6" punct="vg:7">j<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</w>, <w n="6.7" punct="pe:8">t<pgtc id="4" weight="0" schema="R"><rhyme label="c" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>c</rhyme></pgtc></w> !</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="7.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="7.5">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>d<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pt:8">M<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="c" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>ch</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababbcbc">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="9.4" punct="vg:5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="9.5" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>s</rhyme></pgtc></w> !</l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="10.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="10.4">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="10.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="11.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rge<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</rhyme></pgtc></w></l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3" punct="vg:4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="12.4" punct="vg:5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg></w>, <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="13.3">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>x</w> <w n="13.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6" punct="vg:8"><pgtc id="7" weight="2" schema="[CR">l<rhyme label="b" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="354" place="4">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <hi rend="ital"><w n="14.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d</w> <w n="14.5">h<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>c</rhyme></pgtc></w></hi>,</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.5"><pgtc id="7" weight="2" schema="[CR">cl<rhyme label="b" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pi:8">M<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pi">o</seg>ch</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababbcbc">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="17.2">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="17.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>ts</w>-<w n="17.6"><pgtc id="9" weight="2" schema="[CR">l<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</rhyme></pgtc></w></l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="18.5" punct="vg:8">gu<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="19.5">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="19.6" punct="vg:8">g<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="9" weight="2" schema="CR">l<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.6" punct="pe:8">v<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1">L</w>’<w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="21.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="21.4" punct="vg:5">pr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.5" punct="ps:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
					<l n="22" num="3.6" lm="8" met="8">— <w n="22.1">T<seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="22.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="22.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7" punct="vg:8">r<pgtc id="12" weight="0" schema="R"><rhyme label="c" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>c</rhyme></pgtc></w>,</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="23.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="24.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="pe:8">M<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="12" weight="0" schema="R"><rhyme label="c" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pe">o</seg>ch</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="bcbc">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="25.3">qu</w>’<w n="25.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="25.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="25.6">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="25.7" punct="vg:8">gu<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="26.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="26.5" punct="vg:8">st<pgtc id="14" weight="0" schema="R"><rhyme label="c" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>ck</rhyme></pgtc></w>,</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="25" place="2" punct="pe">e</seg>n</w> ! <w n="27.2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="27.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="27.6">c<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.5" punct="pe:8">M<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="14" weight="0" schema="R"><rhyme label="c" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pe">o</seg>ch</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>