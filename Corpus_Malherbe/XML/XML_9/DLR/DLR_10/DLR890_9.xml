<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR890" modus="sp" lm_max="8" metProfile="4, 7, (8), (6)" form="suite périodique" schema="1(abba) 4(abab)" er_moy="1.6" er_max="8" er_min="0" er_mode="0(7/10)" er_moy_et="2.8" qr_moy="0.5" qr_max="N5" qr_mode="0(9/10)" qr_moy_et="1.5">
				<head type="main">LE CRI</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rts</w> <w n="1.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rts</w> <w n="1.6" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>rs</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.5" punct="pt:4">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="3.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5">l</w>’<w n="3.6" punct="dp:7">h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="4" num="1.4" lm="6"><w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="432" place="2" punct="vg">o</seg>s</w>, <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="4.6">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rs</rhyme></pgtc></w></l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="5.4" punct="vg:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg">o</seg>rts</w>, <w n="5.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="5.6" punct="ps:7">m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg>s</rhyme></pgtc></w>…</l>
					<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2">v<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<pgtc id="4" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="7.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>h<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="7" met="7"><w n="8.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="pi:7"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<pgtc id="4" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pi">é</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3" punct="pt:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" punct="pt">un</seg>ts</rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="11.2" punct="pi:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pi">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ? <w n="11.3" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="4">É</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="7" met="7"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="6">ue</seg>ils</w> <w n="12.4" punct="pt:7">br<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" punct="pt">un</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w>-<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.5">p<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="pi:4">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="b" id="8" gender="m" type="a" qr="N5"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="15.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7">h<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="7" met="7"><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="16.6" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg><pgtc id="8" weight="2" schema="CR">ss<rhyme label="b" id="8" gender="m" type="e" qr="N5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="17.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>tr<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">l</w>’<w n="18.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg><pgtc id="10" weight="8" schema="CVCR">f<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="19.3">cr<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.6" punct="dp:7">cr<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="20" num="5.4" lm="7" met="7">— <w n="20.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg></w> ! <w n="20.2">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">n</w>’<w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="20.6" punct="pe:7"><pgtc id="10" weight="8" schema="[CVCR">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>