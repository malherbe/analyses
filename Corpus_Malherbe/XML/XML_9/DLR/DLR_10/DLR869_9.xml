<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MORT ET PRINTEMPS</head><div type="poem" key="DLR869" modus="sp" lm_max="8" metProfile="8, 4" form="suite de strophes" schema="3(abba) 4(abab)" er_moy="1.86" er_max="6" er_min="0" er_mode="0(6/14)" er_moy_et="2.07" qr_moy="0.36" qr_max="N5" qr_mode="0(13/14)" qr_moy_et="1.29">
					<head type="main">LA JEUNESSE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="1.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="1.6">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="1" weight="2" schema="CR">fl<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<pgtc id="2" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></pgtc></w> <w n="2.3" punct="vg:4"><pgtc id="2" weight="6" schema="V[CR" part="2">r<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<pgtc id="2" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></pgtc></w> <w n="3.5" punct="vg:8"><pgtc id="2" weight="6" schema="V[CR" part="2">r<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.6" punct="pt:8">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR" part="1">l<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.6"><pgtc id="3" weight="0" schema="[R" part="1"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="6.5" punct="pt:4"><pgtc id="4" weight="2" schema="[CR" part="1">pl<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pt">u</seg>s</rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="8.2">j</w>’<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR" part="1">l<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D</w>’<w n="9.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="9.8" punct="vg:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="d-2" place="1">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="10.3" punct="vg:4">f<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="11.3" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="11.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="11.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="11.6" punct="vg:8">p<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="12.3">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.7" punct="pe:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bl<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.3" punct="vg:3">m<seg phoneme="y" type="vs" value="1" rule="445" place="3" punct="vg">û</seg>r</w>, <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w>-<w n="13.6" punct="vg:8">d<pgtc id="7" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2" punct="vg:4">t<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="15.5" punct="vg:8">c<pgtc id="7" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="16.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="16.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7" punct="pt:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt</w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="17.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="vg:8"><pgtc id="9" weight="2" schema="[CR" part="1">s<rhyme label="a" id="9" gender="m" type="a" qr="N5"><seg phoneme="u" type="vs" value="1" rule="292" place="8" punct="vg">aoû</seg>l</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1" punct="vg:2">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="18.2" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>, <w n="18.3" punct="pt:4"><pgtc id="10" weight="0" schema="[R" part="1"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">P<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.5">m<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">n</w>’<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="20.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="20.8" punct="pt:8"><pgtc id="9" weight="2" schema="[CR" part="1">s<rhyme label="a" id="9" gender="m" type="e" qr="N5"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">ou</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="21.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4" punct="vg:8">n<seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg><pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="477" place="8">ï</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="22.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<pgtc id="12" weight="4" schema="VR" part="1"><seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dd<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pe">e</seg></w> ! <w n="23.2">L</w>’<w n="23.3">H<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>str<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ct<pgtc id="12" weight="4" schema="VR" part="1"><seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1" punct="vg:1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" punct="vg">in</seg>gt</w>, <w n="25.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>gt</w>-<w n="25.3" punct="vg:3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg>q</w>, <w n="25.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="25.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6" punct="ps:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="ps">an</seg>s</w>… <w n="25.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">On</seg></w> <w n="25.8"><pgtc id="13" weight="2" schema="[CR" part="1">j<rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="26" num="7.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3" punct="pt:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="14" weight="2" schema="CR" part="1">l<rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="27.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="27.3" punct="pt:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt ti">i</seg>t</w>. — <w n="27.4">D<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="27.6"><pgtc id="13" weight="2" schema="[CR" part="1">j<rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="28.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="28.7" punct="pt:8"><pgtc id="14" weight="2" schema="[CR" part="1">fl<rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>