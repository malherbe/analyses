<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR909" modus="sp" lm_max="8" metProfile="8, 5" form="suite périodique avec alternance de type 2" schema="2{1(abba) 1(abab)} 1(abba)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(5/10)" er_moy_et="1.0" qr_moy="1.0" qr_max="N5" qr_mode="0(8/10)" qr_moy_et="2.0">
				<head type="main">DÉPRIMÉE</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">j</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="1.7" punct="vg:8">t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</w>, <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="2.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg><pgtc id="2" weight="2" schema="CR">c<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" punct="vg">en</seg>s</w>, <w n="3.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.8" punct="vg:8"><pgtc id="2" weight="2" schema="[CR">s<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="4.1">M<seg phoneme="y" type="vs" value="1" rule="445" place="1">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4" punct="pe:5">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="pe">o</seg>rt</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="5.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7" punct="pe:8"><pgtc id="3" weight="2" schema="[CR">v<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="6.7" punct="vg:8"><pgtc id="4" weight="2" schema="[CR">b<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="7.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="8.5" punct="pt:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg><pgtc id="4" weight="2" schema="CR">b<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5" punct="vg:8">ph<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">P<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="10.5" punct="pi:8"><pgtc id="6" weight="2" schema="[CR">vr<rhyme label="b" id="6" gender="m" type="a" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="pi">ai</seg></rhyme></pgtc></w> ?</l>
					<l n="11" num="3.3" lm="8" met="8">— <w n="11.1" punct="pe:2">d<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>rt</w> ! <w n="11.2" punct="pe:4">D<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>rt</w> ! <w n="11.3">T<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="11.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="b" id="6" gender="m" type="e" qr="N5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3" punct="pt:5">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg></w> ! <w n="13.2">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" punct="vg">en</seg></w>, <w n="13.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="pe:8">m<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>l</rhyme></pgtc></w> !</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="vg:3">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="14.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="14.4" punct="tc:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="ti">e</seg></w> — <w n="14.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w>-<w n="14.6" punct="ps:8"><pgtc id="8" weight="0" schema="[R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="16.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="16.5" punct="pe:5"><pgtc id="8" weight="0" schema="[R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="ps:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="ps">i</seg>r</w>… <w n="17.2">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="17.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5" punct="vg:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="18.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4">d</w>’<w n="18.5" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="19" num="5.3" lm="8" met="8">— <w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="19.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.4" punct="pe:5">m<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="N5"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pe">eu</seg>rt</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>