<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR888" modus="sp" lm_max="8" metProfile="8, 4, (3)" form="suite de strophes" schema="8(aabb) 2(aa)" er_moy="1.39" er_max="6" er_min="0" er_mode="2(8/18)" er_moy_et="1.42" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
				<head type="main">OXFORD</head>
				<opener>
					<salute>A Bryan Houghton.</salute>
				</opener>
				<lg n="1" type="quatrain" rhyme="aabb">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="1.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rb<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.7">d<pgtc id="1" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></pgtc></w> <w n="1.8" punct="vg:8"><pgtc id="1" weight="6" schema="V[CR" part="2">t<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">An</seg>gl<pgtc id="1" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-27" place="3" punct="vg">e</seg></w>, <w n="3.3">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lf<seg phoneme="œ" type="vs" value="1" rule="304" place="7">ai</seg><pgtc id="2" weight="2" schema="CR" part="1">s<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="3"><space unit="char" quantity="8"></space><w n="4.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>ps</w> <w n="4.2" punct="pt:3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><pgtc id="2" weight="2" schema="CR" part="1">s<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">O</seg>xf<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.8" punct="pt:8">r<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="7.5"><pgtc id="4" weight="2" schema="[CR" part="1">l<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="8.4" punct="pt:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="4" weight="2" schema="CR" part="1">l<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="pt">à</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="aabb">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">J</w>’<w n="9.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1" punct="vg">ai</seg></w>, <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="5" weight="2" schema="CR" part="1">c<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="1">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="10.2">d</w>’<w n="10.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<pgtc id="5" weight="2" schema="CR" part="1">c<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="11.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="6" weight="2" schema="CR" part="1">ss<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="12.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="6" weight="2" schema="CR" part="1">ss<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="aabb">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">bl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">p<pgtc id="7" weight="1" schema="GR" part="1">i<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<pgtc id="7" weight="1" schema="GR" part="1">i<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Bl<seg phoneme="ø" type="vs" value="1" rule="403" place="1">eu</seg>s</w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="15.3" punct="vg:4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="15.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>fs</w> <w n="15.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="8" weight="2" schema="CR" part="1">r<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3" punct="vg:4"><pgtc id="8" weight="2" schema="[CR" part="1">gr<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="aabb">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5" punct="vg:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="428" place="8">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="18.4">f<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="10" weight="2" schema="CR" part="1">v<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="20.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4" punct="vg:4"><pgtc id="10" weight="2" schema="[CR" part="1">v<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="aabb">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>pr<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg><pgtc id="11" weight="2" schema="CR" part="1">bl<rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="22.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="22.2" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg><pgtc id="11" weight="2" schema="CR" part="1">l<rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:3">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="23.3" punct="vg:5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg></w>, <w n="23.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="23.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.6" punct="vg:8">l<pgtc id="12" weight="1" schema="GR" part="1">i<rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="6.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="24.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.3" punct="vg:4">D<pgtc id="12" weight="1" schema="GR" part="1">i<rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="aabb">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="25.2" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="dp in">e</seg></w> : « <w n="25.3" punct="pe:6">V<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">ez</seg></w> ! <w n="25.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="25.5" punct="vg:8">r<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="7.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="26.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3" punct="pt:4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="27.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="27.5" punct="vg:7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="vg">e</seg></w>, <w n="27.6" punct="vg:8">m<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
					<l n="28" num="7.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="28.1">L</w>’<w n="28.2" punct="vg:2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="28.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.4" punct="pt:4">f<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="distique" rhyme="aa">
					<l n="29" num="8.1" lm="8" met="8">« <w n="29.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="29.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="29.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="29.5">n<seg phoneme="y" type="vs" value="1" rule="457" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="29.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="29.7" punct="vg:8">f<pgtc id="15" weight="1" schema="GR" part="1">u<rhyme label="a" id="15" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="30" num="8.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="30.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="30.4" punct="pe:4">s<pgtc id="15" weight="1" schema="GR" part="1">u<rhyme label="a" id="15" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="9" type="distique" rhyme="aa">
					<l n="31" num="9.1" lm="8" met="8"><w n="31.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="31.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="31.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="31.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="31.6">ci<pgtc id="16" weight="0" schema="R" part="1"><rhyme label="a" id="16" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</rhyme></pgtc></w></l>
					<l n="32" num="9.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="32.1">D</w>’<w n="32.2" punct="pi:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><pgtc id="16" weight="0" schema="R" part="1"><rhyme label="a" id="16" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="pi">e</seg>l</rhyme></pgtc></w> ? »</l>
				</lg>
				<lg n="10" type="quatrain" rhyme="aabb">
					<l n="33" num="10.1" lm="8" met="8"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="33.2" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="33.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="33.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.5" punct="dp:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="17" weight="0" schema="R" part="1"><rhyme label="a" id="17" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="34" num="10.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="34.2">M<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363" place="3">en</seg></w>-<w n="34.3" punct="vg:4"><pgtc id="17" weight="0" schema="[R" part="1"><rhyme label="a" id="17" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="35" num="10.3" lm="8" met="8"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="35.2" punct="vg:3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="35.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="35.4">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="35.5" punct="vg:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="18" weight="2" schema="CR" part="1">r<rhyme label="b" id="18" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="36" num="10.4" lm="4" met="4"><space unit="char" quantity="8"></space><hi rend="ital"><w n="36.1" punct="ps:4">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="e" type="vs" value="1" rule="DLR888_1" place="2">e</seg>r<seg phoneme="e" type="vs" value="1" rule="DLR888_2" place="3">e</seg><pgtc id="18" weight="2" schema="CR" part="1">r<rhyme label="b" id="18" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="DLR888_3" place="4" punct="ps">e</seg></rhyme></pgtc></w></hi>… »</l>
				</lg>
			</div></body></text></TEI>