<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR881" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="3(abba) 2(abab)" er_moy="0.6" er_max="2" er_min="0" er_mode="0(7/10)" er_moy_et="0.92" qr_moy="0.0" qr_max="C0" qr_mode="0(10/10)" qr_moy_et="0.0">
				<head type="main">CERCUEILS</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>il</w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="1.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="1.6">j<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="2.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" punct="vg">un</seg>ts</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.2" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" punct="pt">un</seg>s</rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.6" punct="pe:8">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.5"><pgtc id="3" weight="2" schema="[CR">pl<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.4" punct="pt:8">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.3">c<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="vg:8">d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w>-<w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="8.5" punct="pt:8">t<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="9.6" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="10.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="10.3" punct="pi:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rc<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pi">eu</seg>x</rhyme></pgtc></w> ?</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="pe:2">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="pe">e</seg>s</w> ! <w n="11.2">N</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <hi rend="ital"><w n="12.2">m<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="451" place="4">u</seg>m</w></hi> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="12.5" punct="pt:8"><pgtc id="6" weight="0" schema="[R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="13.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="13.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.7" punct="pe:8">v<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="14.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="8" weight="2" schema="CR">s<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></pgtc></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="15.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="16.3">squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4" punct="pt:8">gr<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="8" weight="2" schema="CR">ç<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8">— <w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="17.5">l</w>’<w n="17.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>s<pgtc id="9" weight="2" schema="CR">p<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1" punct="vg:1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="18.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>ts</w>, <w n="18.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="18.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="18.5" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>t</rhyme></pgtc></w> !</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="19.2">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="6">ue</seg>ils</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>b<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu</w>’<w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="20.5" punct="pe:8"><pgtc id="9" weight="2" schema="CR">p<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>