<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VINGT SONNETS</head><div type="poem" key="CRO73" modus="cm" lm_max="12" metProfile="6−6" form="sonnet classique" schema="abba abba cdd cee" er_moy="0.57" er_max="2" er_min="0" er_mode="0(5/7)" er_moy_et="0.9" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="main">Révolte</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>bs<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="1.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.8" punct="vg:12">r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5" punct="vg:6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1">Bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>ds</w>, <w n="3.2" punct="vg:3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg>s</w>, <w n="3.3" punct="vg:6">cr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.6">d</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>r</w> <w n="3.8">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2" punct="vg:2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rs</w> <w n="4.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="4.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="4.8" punct="pt:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">em</seg>ps<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>bs<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="5.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="Lc">i</seg>squ</w>’<w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.4">n</w>’<w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="5.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="5.10" punct="vg:12">pr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" mp6="C" met="6−6"><w n="6.1" punct="vg:4">R<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="6.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="Lc">i</seg>squ</w>’<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C" caesura="1">on</seg></w><caesura></caesura> <w n="6.4">n</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="6.7">j<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="6.8">v<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg></w> <w n="6.9" punct="vg:12">d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" mp6="C" met="6−6"><w n="7.1" punct="vg:1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1" punct="vg">au</seg>f</w>, <w n="7.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2" mp="Lc">eu</seg>t</w>-<w n="7.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="7.6">k<seg phoneme="i" type="vs" value="1" rule="125" place="7" mp="M">ee</seg>ps<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>k<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.7" punct="ps:12">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="10" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="ps">eu</seg>x</rhyme></pgtc></w>…</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="8.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>, <w n="8.5">c</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.9" punct="pt:12">p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="cdd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>bs<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="9.6">pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rt</w> <w n="9.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="9.10">b<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="10.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="11" mp="M">ein</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="12" mp6="C" met="6−6"><w n="11.1">S</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="11.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="11.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="11.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.10" punct="pt:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="cee">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="12.5">p<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="12.7">c<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>f<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</rhyme></pgtc></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="13.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rdr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="13.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ct</w><caesura></caesura> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="13.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="13.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="13.10"><pgtc id="7" weight="2" schema="[CR">r<rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Cl<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>dm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="dp">e</seg>t</w> : <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="14.4" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="493" place="6" punct="vg" caesura="1">y</seg>s</w>,<caesura></caesura> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.6" punct="pt:9">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt" mp="F">e</seg></w>. <w n="14.7">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="14.8">m</w>’<w n="14.9" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="7" weight="2" schema="CR">rr<rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>