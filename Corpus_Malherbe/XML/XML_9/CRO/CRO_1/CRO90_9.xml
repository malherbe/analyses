<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO90" modus="cp" lm_max="10" metProfile="7, 4+6" form="suite périodique" schema="5(abab)" er_moy="3.0" er_max="6" er_min="0" er_mode="2(6/10)" er_moy_et="2.05" qr_moy="0.5" qr_max="N5" qr_mode="0(9/10)" qr_moy_et="1.5">
					<head type="main">Chanson de la Côte</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="1.6" punct="vg:10">m<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="7" met="7"><space quantity="8" unit="char"></space><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>rs</w> <w n="2.5" punct="pt:7">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" caesura="1">e</seg>r</w><caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.6">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="3.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.8" punct="vg:10">n<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="C">i</seg>l</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="4.8" punct="pt:10"><pgtc id="2" weight="2" schema="[CR">pr<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.6" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="7" met="7"><space quantity="8" unit="char"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="6.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="6.6">d</w>’<w n="6.7" punct="pt:7"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="7.3" punct="vg:4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="7.5" punct="vg:7">l<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>, <w n="7.6">M<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>th<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="8.6" punct="pt:10">tr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.6" punct="vg:10">m<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="7" met="7"><space quantity="8" unit="char"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cs</w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.9" punct="vg:10">f<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">p<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="12.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rds</w><caesura></caesura> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="12.5">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="12.7" punct="pt:10">f<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rs</w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="13.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.6" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="7" met="7"><space quantity="8" unit="char"></space><w n="14.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">p<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5" punct="vg:7">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="8" weight="2" schema="CR">p<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cs</w> <w n="15.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="15.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="15.6" punct="pt:7">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>t</w>. <w n="15.7">M<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>th<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="16.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" caesura="1">en</seg>s</w><caesura></caesura> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="16.6">t</w>’<w n="16.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="16.8" punct="pt:10">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg><pgtc id="8" weight="2" schema="CR">p<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="10" met="4+6"><w n="17.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rs</w> <w n="17.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="17.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="17.6" punct="vg:10">m<pgtc id="9" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="7" met="7"><space quantity="8" unit="char"></space><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="18.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="18.5" punct="pt:7"><pgtc id="10" weight="2" schema="[CR">l<rhyme label="b" id="10" gender="m" type="a" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg>g</rhyme></pgtc></w>.</l>
						<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="19.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="19.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="19.4" punct="vg:7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>cs</w>, <w n="19.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="19.6">n<pgtc id="9" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="10" met="4+6"><w n="20.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="20.4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" mp="M">oi</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="20.5" punct="pt:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="10" weight="2" schema="CR">l<rhyme label="b" id="10" gender="m" type="e" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>