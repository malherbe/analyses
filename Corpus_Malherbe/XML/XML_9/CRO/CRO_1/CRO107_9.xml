<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO107" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="5((aa))" er_moy="1.8" er_max="3" er_min="0" er_mode="2(3/5)" er_moy_et="0.98" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
					<head type="main">Paysage</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rs<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>t</w><caesura></caesura> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7">s</w>’<w n="1.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="2.6" punct="vg:12">B<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ts</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.5">r<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <hi rend="ital"><w n="3.7" punct="vg:12">M<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="3.10"><pgtc id="2" weight="3" schema="[CGR">Nu<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>ts</rhyme></pgtc></w></hi>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="4.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>ts</w> <w n="4.6" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg><pgtc id="2" weight="3" schema="CGR">nu<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="5.3" punct="vg:4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>t</w>, <w n="5.4">m<seg phoneme="y" type="vs" value="1" rule="445" place="5" mp="M">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="5.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.8" punct="vg:12">tr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="6.7">l</w>’<w n="6.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd</w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg><pgtc id="4" weight="2" schema="CR">ti<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3" punct="vg:3">l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="vg">o</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.4" punct="vg:4"><seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="vg">où</seg></w>, <w n="8.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="8.6">v<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="8.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.8" punct="dp:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>r<pgtc id="4" weight="2" schema="CR">ti<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="dp">er</seg>s</rhyme></pgtc></w> :</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="9.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.6">l</w>’<w n="9.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="9.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.10"><pgtc id="5" weight="2" schema="[CR">b<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="10.5">p<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="10.7">cr<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="10.9" punct="pt:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR">b<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>