<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VINGT SONNETS</head><div type="poem" key="CRO70" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdd cee" er_moy="0.86" er_max="2" er_min="0" er_mode="0(4/7)" er_moy_et="0.99" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="main">Soir éternel</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rc</w>, <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="P">en</seg><pgtc id="1" weight="0" schema="[R" part="1">tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w> <w n="1.9" punct="pt:12"><pgtc id="1" weight="0" schema="[R" part="2"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="2.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR" part="1">ll<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="3.2" punct="pv:2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="pv">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="3.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>x</w> <w n="3.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg><pgtc id="2" weight="2" schema="CR" part="1">l<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">N</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>x</w><caesura></caesura> <w n="4.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rds</w>, <w n="4.7" punct="vg:10">f<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>fs</w>, <w n="4.8" punct="pt:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg><pgtc id="1" weight="0" schema="R" part="1">r<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR" part="1">r<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="6.4">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" mp="M">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.7" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="4" weight="2" schema="CR" part="1">l<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="7.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.7" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="4" weight="2" schema="CR" part="1">l<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg>s</rhyme></pgtc></w> !…</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="2">um</seg>s</w> <w n="8.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rs<seg phoneme="y" type="vs" value="1" rule="d-3" place="4" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="8.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t</w> <w n="8.7" punct="pe:12"><pgtc id="3" weight="2" schema="[CR" part="1">cr<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe ps">eu</seg>x</rhyme></pgtc></w> !…</l>
					</lg>
					<lg n="3" rhyme="cdd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="9.2" punct="vg:3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="vg">en</seg>t</w>, <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.8" punct="vg:9">br<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>s</w>, <w n="9.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.10">fr<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</w> <w n="10.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="194" place="9" mp="M">e</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="10.9">s<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rps</w> <w n="11.4" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="11.7">pl<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="11.9" punct="pt:12">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="11" mp="M">ei</seg>gn<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="cee">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="12.3" punct="pe:4">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3" mp="M">ei</seg>gn<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pe">oi</seg>r</w> ! <w n="12.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="12.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pt">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="415" place="9">Ô</seg></w> <w n="12.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="12.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="12.10">p<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="13.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>s</w><caesura></caesura> <w n="13.5">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.7" punct="pe:12">p<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg><pgtc id="7" weight="0" schema="R" part="1">r<rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe ps">eu</seg>x</rhyme></pgtc></w> !…</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rc</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="14.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="14.8">l</w>’<w n="14.9"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="14.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="P">en</seg><pgtc id="7" weight="0" schema="[R" part="1">tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w> <w n="14.11" punct="pt:12"><pgtc id="7" weight="0" schema="[R" part="2"><rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>