<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÇA</head><head type="main_subpart">PARIS</head><div type="poem" key="CRB10" modus="sm" lm_max="8" metProfile="8" form="sonnet classique" schema="abab abab ccd dee" er_moy="0.29" er_max="2" er_min="0" er_mode="0(6/7)" er_moy_et="0.7" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
						<lg n="1" rhyme="abab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2" punct="pt:2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pt ti">i</seg>s</w>. — <w n="1.3" punct="pe:3">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3" punct="pe ti">en</seg></w> ! — <w n="1.4">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.6">l</w>’<w n="1.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:3">pl<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="2.4">M<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg></w> <w n="2.5" punct="pt:8">bl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l</w>’<w n="3.3" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bs<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg>th<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7" punct="ps:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.7" punct="pt:8">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pt">œu</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" rhyme="abab">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="5.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.5"><seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>sth<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l</w>’<w n="6.3" punct="ps:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="ps">ou</seg>r</w> … <w n="6.4">l</w>’<w n="6.5" punct="tc:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="ti">ou</seg>r</w> — <w n="6.6" punct="pe:8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gu<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="7.4">h<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">mi<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4" punct="vg:5">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="8.6" punct="pe:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>qu<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l part="I" n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w>-<w n="9.4">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4">e</seg></w> <w n="9.5" punct="pi:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pi">a</seg>s</w> ? <w n="9.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w>-<w n="9.7">t</w>’<w n="9.8" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7" punct="pe">en</seg></w> ! </l>
							<l part="F" n="9" num="3.1" lm="8" met="8"><w n="9.9">L<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2" punct="tc:3">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="ti">e</seg></w> — <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.4" punct="tc:8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ti">e</seg></rhyme></pgtc></w> —</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2" punct="tc:4">r<seg phoneme="e" type="vs" value="1" rule="160" place="2">e</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>lv<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="ti">e</seg>r</w> — <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.4" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="ps">i</seg></rhyme></pgtc></w>…</l>
						</lg>
						<lg n="4" rhyme="dee">
							<l n="12" num="4.1" lm="8" met="8"><w n="12.1">Dr<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="12.4" punct="pe:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></rhyme></pgtc></w> !</l>
							<l n="13" num="4.2" lm="8" met="8">… <w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="13.2" punct="vg:2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="13.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="13.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.8" punct="vg:8"><pgtc id="7" weight="2" schema="CR">v<rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="4.3" lm="8" met="8"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="14.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4" punct="ps:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="7" weight="2" schema="CR">v<rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
						</lg>
					</div></body></text></TEI>