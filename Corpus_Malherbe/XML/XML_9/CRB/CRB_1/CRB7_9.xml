<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÇA</head><head type="main_subpart">PARIS</head><div type="poem" key="CRB7" modus="sm" lm_max="8" metProfile="8" form="sonnet classique" schema="abab abab ccd eed" er_moy="1.71" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.98" qr_moy="0.71" qr_max="N5" qr_mode="0(6/7)" qr_moy_et="1.75">
						<lg n="1" rhyme="abab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.4" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" punct="vg">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5" punct="dp:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="dp">an</seg>t</w> : <w n="1.6">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.5">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="2.7" punct="vg:8">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <hi rend="ital"><w n="4.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w></hi> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="4.5" punct="pt:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" rhyme="abab">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.2"><seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="5.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="3" weight="2" schema="CR">n<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2" punct="ps:3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3" punct="ps">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="6.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="133" place="4" punct="vg">E</seg>h</w>, <w n="6.4">c</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="6.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.8" punct="pe:8">j<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe ps">ou</seg>r</rhyme></pgtc></w> !…</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="tc:1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="dp ti">en</seg>s</w> : — <w n="7.2">C</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="7.5" punct="tc:5">n<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="ti">eu</seg>f</w> — <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>m<pgtc id="3" weight="2" schema="CR">n<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3" punct="ps:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="ps">ou</seg>rs</w> … <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.5">l</w>’<w n="8.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pe:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe">é</seg></w> ! <w n="9.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="9.5" punct="pe:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg><pgtc id="5" weight="2" schema="CR">pl<rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="10.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.6" punct="ps:8"><pgtc id="5" weight="2" schema="[CR">l<rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w> …</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2" punct="pt:3">ç<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt ti">a</seg></w>. — <w n="11.3">N<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>l</w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.8" punct="pt:8">t<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="4" rhyme="eed">
							<l n="12" num="4.1" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">qu</w>’<w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.6">m<seg phoneme="œ" type="vs" value="1" rule="151" place="5">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>r</w> <w n="12.7">c<pgtc id="7" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="13" num="4.2" lm="8" met="8"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="13.3" punct="tc:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="ti">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> — <w n="13.4" punct="pe:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">In</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="pe">e</seg>ct</w> ! <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg>h</w> <w n="13.6" punct="pe:8">spl<pgtc id="7" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ti">e</seg></rhyme></pgtc></w> ! —</l>
							<l n="14" num="4.3" lm="8" met="8">… <w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4" punct="pt:5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="pt ti">en</seg></w>. — <w n="14.5">C</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="14.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="14.8" punct="pt:8">c<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="e" qr="N5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rt</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>