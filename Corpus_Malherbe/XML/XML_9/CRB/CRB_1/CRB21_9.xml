<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB21" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="8(aabb) 2(abab) 1(aabccb)" er_moy="1.91" er_max="8" er_min="0" er_mode="0(11/23)" er_moy_et="2.54" qr_moy="0.0" qr_max="C0" qr_mode="0(23/23)" qr_moy_et="0.0">
					<head type="main">À UNE ROSE</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>-<w n="1.3">d</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="1.5" punct="vg:8">v<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>nn<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="2.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="2.2" punct="pt:4">f<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="3.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="4.3" punct="pe:4"><pgtc id="2" weight="2" schema="[CR">fl<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">F<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="5.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w>-<w n="6.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>rs</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w>-<w n="7.2" punct="vg:4">J<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>ph</w>, <w n="7.3">cr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="7.4">d</w>’<w n="7.5" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></pgtc></w> :</l>
						<l n="8" num="2.4" lm="4" met="4"><space quantity="8" unit="char"></space> — <w n="8.1">Ch<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="8.3" punct="tc:4"><pgtc id="4" weight="2" schema="[CR">v<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="ti">e</seg>rs</rhyme></pgtc></w> —</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="aabb">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="œ" type="vs" value="1" rule="249" place="1">œu</seg>r</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4" punct="vg">um</seg></w>, <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3" punct="ps:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="ps">e</seg></rhyme></pgtc></w> …</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="11.5" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>c<pgtc id="6" weight="2" schema="CR">c<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="vg">è</seg>s</rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="12.2" punct="pv:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><pgtc id="6" weight="2" schema="CR">c<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="pv">è</seg>s</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="aabb">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Gr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.6" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="14.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="14.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ls<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w>-<w n="15.5" punct="vg:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="8" punct="vg">a</seg>il</rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="16.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="16.3">l</w>’<w n="16.4" punct="pe:4"><pgtc id="8" weight="0" schema="[R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="4" punct="pe">a</seg>il</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="sizain" rhyme="aabccb">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="17.4" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>st<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="18.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="18.3">f<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">h<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w>-<w n="19.3">d</w>’<w n="19.4" punct="vg:5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>r</w>, <w n="19.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="19.6" punct="ps:8"><pgtc id="10" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="ps">an</seg>t</rhyme></pgtc></w> …</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.2" punct="vg:5"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg><pgtc id="11" weight="8" schema="CVCR">rr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<rhyme label="c" id="11" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="21" num="5.5" lm="8" met="8"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w>-<w n="21.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.5" punct="vg:8"><pgtc id="11" weight="8" schema="[CVCR">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<rhyme label="c" id="11" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="5.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="22.2" punct="pe:4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><pgtc id="10" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="aabb">
						<l n="23" num="6.1" lm="8" met="8"><w n="23.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="23.6">c<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="25" num="6.3" lm="8" met="8"><w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="25.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w>-<w n="25.3" punct="vg:8">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg><pgtc id="13" weight="8" schema="CVCR">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="b" id="13" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>t</rhyme></pgtc></w>,</l>
						<l n="26" num="6.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="26.1">P<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="26.2" punct="pt:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg><pgtc id="13" weight="8" schema="CVCR">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<rhyme label="b" id="13" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="aabb">
						<l n="27" num="7.1" lm="8" met="8"><w n="27.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="27.2" punct="pe:3">th<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe ps ti">é</seg></w> !… — <w n="27.3">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="27.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.5" punct="vg:6">gr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>g</w>, <w n="27.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w>-<w n="27.7" punct="pe:8"><pgtc id="14" weight="0" schema="[R"><rhyme label="a" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ti">e</seg></rhyme></pgtc></w> ! —</l>
						<l n="28" num="7.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="28.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="28.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="28.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="29" num="7.3" lm="8" met="8"><w n="29.1" punct="vg:2">J<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="29.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="29.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="29.6" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg><pgtc id="15" weight="2" schema="CR">p<rhyme label="b" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="30" num="7.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="30.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="30.2" punct="pe:4">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg><pgtc id="15" weight="2" schema="CR">p<rhyme label="b" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="31" num="8.1" lm="8" met="8"><w n="31.1">V<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w>-<w n="31.2" punct="vg:4">C<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="31.3">n<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="31.5" punct="vg:8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="16" weight="2" schema="CR">l<rhyme label="a" id="16" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="32" num="8.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="32.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="32.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w>-<w n="32.3" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="33" num="8.3" lm="8" met="8"><w n="33.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="33.2">l</w>’<w n="33.3" punct="ps:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="ps">e</seg></w> … <w n="33.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="33.5">c<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="16" weight="2" schema="CR">l<rhyme label="a" id="16" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="34" num="8.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="34.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg></w> <w n="34.3" punct="pe:4">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="pe">in</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="aabb">
						<l n="35" num="9.1" lm="8" met="8"><w n="35.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="35.2" punct="vg:5">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="35.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="35.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="35.5">p<pgtc id="18" weight="0" schema="R"><rhyme label="a" id="18" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="36" num="9.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="36.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="36.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="36.3">m<pgtc id="18" weight="0" schema="R"><rhyme label="a" id="18" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="37" num="9.3" lm="8" met="8"><w n="37.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="37.2">l</w>’<w n="37.3" punct="ps:3"><seg phoneme="a" type="vs" value="1" rule="343" place="2">A</seg><seg phoneme="i" type="vs" value="1" rule="476" place="3" punct="ps">ï</seg></w>… <w n="37.4">D<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="37.5">B<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">O</seg>CK</w> <w n="37.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="37.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<pgtc id="19" weight="0" schema="R"><rhyme label="b" id="19" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></pgtc></w></l>
						<l n="38" num="9.4" lm="4" met="4"><space quantity="8" unit="char"></space> — <w n="38.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="38.2">30<add reason="analysis" type="phonemization" rend="hidden" hand="RR">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></add></w> <hi rend="smallcap"><w n="38.3" punct="pt:4">C<pgtc id="19" weight="0" schema="R"><rhyme label="b" id="19" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="pt">en</seg>t</rhyme></pgtc></w></hi>.</l>
					</lg>
					<lg n="10" type="quatrain" rhyme="aabb">
						<l n="39" num="10.1" lm="8" met="8">— <w n="39.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="39.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p</w>-<w n="39.3">d<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w>-<w n="39.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="39.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="39.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="39.7" punct="pe:8"><pgtc id="20" weight="2" schema="[CR">r<rhyme label="a" id="20" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="40" num="10.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="40.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="40.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="40.3" punct="pv:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="20" weight="2" schema="CR">tr<rhyme label="a" id="20" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="41" num="10.3" lm="8" met="8"><w n="41.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="41.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p</w> <w n="41.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="41.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="41.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="41.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="41.8"><pgtc id="21" weight="0" schema="[R"><rhyme label="b" id="21" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</rhyme></pgtc></w></l>
						<l n="42" num="10.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="42.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="42.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w>-<w n="42.3">d<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w>-<w n="42.4" punct="pe:4">f<pgtc id="21" weight="0" schema="R"><rhyme label="b" id="21" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="pe ps">e</seg>r</rhyme></pgtc></w> !…</l>
					</lg>
					<lg n="11" type="quatrain" rhyme="aabb">
						<l n="43" num="11.1" lm="8" met="8"><w n="43.1" punct="vg:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg></w>, <w n="43.2">g<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="43.4" punct="vg:6">g<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="43.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="43.6"><pgtc id="22" weight="2" schema="[CR">r<rhyme label="a" id="22" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="44" num="11.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="44.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="44.2" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="22" weight="2" schema="CR">r<rhyme label="a" id="22" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="45" num="11.3" lm="8" met="8"><w n="45.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="45.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="45.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w>-<w n="45.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ls</w> <w n="45.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="45.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="45.7" punct="vg:8"><pgtc id="23" weight="2" schema="[CR">c<rhyme label="b" id="23" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="46" num="11.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="46.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="46.2" punct="pe:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg><pgtc id="23" weight="2" schema="CR">qu<rhyme label="b" id="23" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pe">eu</seg>rs</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>