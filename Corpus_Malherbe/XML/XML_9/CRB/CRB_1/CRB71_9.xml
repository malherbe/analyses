<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ARMOR</head><div type="poem" key="CRB71" modus="sm" lm_max="8" metProfile="8" form="sonnet non classique" schema="abab cdcd eef ggf" er_moy="1.71" er_max="8" er_min="0" er_mode="0(5/7)" er_moy_et="2.91" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
					<head type="main">PAYSAGE MAUVAIS</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="1.4" punct="tc:5"><seg phoneme="o" type="vs" value="1" rule="432" place="5" punct="ti">o</seg>s</w> — <w n="1.5">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>t</w> <w n="1.7">r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2" punct="dp:2">gl<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="dp">a</seg>s</w> : <w n="2.3">cr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.4">br<del reason="analysis" type="syneresis" hand="RR">u</del><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="2.6" punct="ps:8">br<seg phoneme="y" type="vs" value="1" rule="dc-3" place="7">u</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="ps">i</seg>t</rhyme></pgtc></w>…</l>
						<l n="3" num="1.3" lm="8" met="8">— <w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>d</w> <w n="3.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">gr<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="4.3" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7" punct="pt:8">nu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="cdcd">
						<l n="5" num="2.1" lm="8" met="8">— <w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6">f<pgtc id="3" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="ps:1">Cu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="ps">i</seg>t</w>… <w n="6.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="6.4">d<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.5" punct="pt:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>gu<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="8" met="8">— <w n="7.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">p<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">l<pgtc id="3" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rci<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ltr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.6" punct="ps:8">fu<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="ps">i</seg>t</rhyme></pgtc></w>…</l>
					</lg>
					<lg n="3" rhyme="eef">
						<l n="9" num="3.1" lm="8" met="8">— <w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">L<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>di<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">tr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.5" punct="vg:8">s<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <hi rend="ital"><w n="11.2" punct="ps:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.4">l<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="ps ti">ou</seg>ps</w></hi>… — <w n="11.5">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.6" punct="pt:8">cr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<pgtc id="6" weight="0" schema="R"><rhyme label="f" id="6" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pt">au</seg>ds</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="ggf">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ts</w> <w n="12.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg><pgtc id="7" weight="8" schema="CVCR">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="g" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="13.4" punct="vg:8"><pgtc id="7" weight="8" schema="[CVCR">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="g" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="14.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="14.4" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<pgtc id="6" weight="0" schema="R"><rhyme label="f" id="6" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Marais de Guérande</placeName> — Avril.
						</dateline>
					</closer>
				</div></body></text></TEI>