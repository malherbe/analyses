
	▪ Francis CARCO [CRC]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ Poésies complètes, 1955 [CRC_1]
		▫ Petits airs, 1920 [CRC_2]
		▫ Au vent crispé du matin, 1913 [CRC_3]
