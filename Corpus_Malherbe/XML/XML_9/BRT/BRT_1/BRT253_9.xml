<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT253" modus="cp" lm_max="12" metProfile="8, 6, 4, 6+6, 4+6" form="suite de strophes" schema="3[abab]" er_moy="1.0" er_max="2" er_min="0" er_mode="0(3/6)" er_moy_et="1.0" qr_moy="0.83" qr_max="N5" qr_mode="0(5/6)" qr_moy_et="1.86">
				<head type="number">III</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">v<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="2.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="2.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.8" punct="pt:12">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="10" met="4+6">— <w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="Lc">ou</seg>rd</w>’<w n="3.3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.5">l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>th</w> <w n="3.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="3.8">pl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7" punct="pt:10">G<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8">— <w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.7"><pgtc id="3" weight="2" schema="[CR">p<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="6.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="6.6" punct="pv:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="4" weight="2" schema="CR">bl<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="6" met="6">— <w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5"><pgtc id="3" weight="2" schema="[CR">p<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="6" met="6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4" punct="ps:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg><pgtc id="4" weight="2" schema="CR">bl<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="ps">é</seg></rhyme></pgtc></w>…</l>
					<l n="9" num="1.9" lm="6" met="6">— <w n="9.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="4" met="4"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3" punct="pv:4">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="6" met="6">— <w n="11.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.3" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.4">f<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="4" met="4"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="12.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="12.3" punct="ps:4"><pgtc id="6" weight="2" schema="[CR">l<rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="ps">on</seg>g</rhyme></pgtc></w>…</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Palais<lb></lb>▪ alais<lb></lb>▪ lais<lb></lb>▪ ais<lb></lb>▪ is<lb></lb>▪ s</note>
					</closer>
			</div></body></text></TEI>