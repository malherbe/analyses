<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT299" modus="cm" lm_max="12" metProfile="6+6" form="sonnet peu classique" schema="abba baab ccd eed" er_moy="2.29" er_max="6" er_min="0" er_mode="2(5/7)" er_moy_et="1.67" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="number">XXIV</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="1.9" punct="pv:12"><pgtc id="1" weight="2" schema="CR">p<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>t</w>, <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="2.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="2.7" punct="pv:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.3" punct="vg:4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="426" place="6" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="3.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="3.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="3.9" punct="pv:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2" punct="vg:2">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.5" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="vg" caesura="1">ain</seg>s</w>,<caesura></caesura> <w n="4.6" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</w>, <w n="4.7" punct="vg:9">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>t</w>, <w n="4.8" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10" punct="vg">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.9" punct="ps:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>s<pgtc id="1" weight="2" schema="CR">p<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
				</lg>
				<lg n="2" rhyme="baab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="5.4">n</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="5.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="5.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.9">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="5.10" punct="pt:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="b" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="6.7" punct="pe:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">p<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="7.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="7.9" punct="pv:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>s<pgtc id="4" weight="2" schema="CR">p<rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.3" punct="vg:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.6" punct="vg:9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9" punct="vg">ain</seg>s</w>, <w n="8.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="8.8" punct="pe:12">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="11" mp="M">eu</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="b" id="3" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="9.2">d</w>’<w n="9.3" punct="pe:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pe ps" mp="F">e</seg>s</w> ! … <w n="9.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="Lc">i</seg></w>-<w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>st<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="9.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="9.9" punct="pt:12"><pgtc id="5" weight="2" schema="[CR">pl<rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="10.2">d</w>’<w n="10.3" punct="pe:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rph<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="pe ps">in</seg>s</w> ! … <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.10" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="5" weight="2" schema="CR">l<rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="11.2">d</w>’<w n="11.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ffl<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pe ps">é</seg>s</w> ! … <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="11.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>t</w> <w n="11.7">l</w>’<w n="11.8" punct="pe:12">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<pgtc id="6" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe ps">é</seg></rhyme></pgtc></w> ! …</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2" punct="vg:3">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="12.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="12.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="12.6">s</w>’<w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="12.8">d<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="12.9">lu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="Lc">i</seg></w>-<w n="12.10" punct="pv:12">m<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="13.6">l</w>’<w n="13.7">h<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>pr<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="14.3" punct="ps:6">tr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="ps vg" caesura="1">o</seg>r</w>…<caesura></caesura>, <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.5">s<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="14.7" punct="pe:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<pgtc id="6" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe ps">é</seg></rhyme></pgtc></w> ! …</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Saint Vincent de Paul.</note>
					</closer>
			</div></body></text></TEI>