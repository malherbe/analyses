<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT199" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="4(abab)" er_moy="2.0" er_max="6" er_min="0" er_mode="0(4/8)" er_moy_et="2.45" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="number">XXIV</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>t</w> <w n="1.6" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>d<pgtc id="1" weight="6" schema="VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="2.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="2.6" punct="dp:12"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="dp">é</seg>s</rhyme></pgtc></w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="3.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>dj<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg>t</w>,<caesura></caesura> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="3.7">c<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.8" punct="vg:12">v<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<pgtc id="1" weight="6" schema="VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2" punct="pv:4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>pt<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv">eu</seg>r</w> ; <w n="4.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.5" punct="pt:12">n<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>l<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.6" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ppr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.5">j</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="6.7">mi<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="7.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4" punct="pt:8">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ch<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="9.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="9.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="9.9">f<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.10" punct="vg:12">ch<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="10.7">t<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="10.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="6" weight="2" schema="CR">rch<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.5" punct="vg:6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ls</w>,<caesura></caesura> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">t<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.9" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>p<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" mp="C">e</seg>t</w> <w n="12.3">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="3">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="12.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="12.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="12.9" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">rch<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">s</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7">g<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.6" punct="pt:8">Br<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">ou</seg></rhyme></pgtc></w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="15.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="15.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.5" punct="pt:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">L</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="16.3" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pi ti">ou</seg>s</w> ? — <w n="16.4">N<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="16.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="16.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="16.7" punct="pt:8">pr<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">ou</seg></rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Bourg, bourre, bourre, Bourg.</note>
					</closer>
			</div></body></text></TEI>