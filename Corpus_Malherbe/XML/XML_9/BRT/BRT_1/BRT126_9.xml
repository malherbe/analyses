<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT126" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abab)" er_moy="0.83" er_max="2" er_min="0" er_mode="0(3/6)" er_moy_et="0.9" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="number">XXVI</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="1.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="1.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="1.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="1.8" punct="dp:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="dp in" mp="F">e</seg>s</w> : « <w n="1.9" punct="pe:12">M<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> ! »</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="dp in">on</seg>ds</w> : « <w n="2.4" punct="pe:6">M<seg phoneme="œ" type="vs" value="1" rule="151" place="5" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="pe" caesura="1">eu</seg>r</w> !<caesura></caesura> » <w n="2.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.9" punct="pt:12">l<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="3.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9" punct="pv:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">H<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="4.3">s</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="4.7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="Lc">ou</seg>rd</w>’<w n="4.10" punct="pt:12">h<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" caesura="1">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.7" punct="pe:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>ppl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></pgtc></w> ! …</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1" mp="C">Y</seg></w> <w n="6.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M/mp">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="6.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="6.6">p<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="6.7" punct="pi:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">n<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi ps">é</seg></rhyme></pgtc></w> ? …</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M/mp">i</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="7.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="7.3" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="7.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="7.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="7.6">v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="8.2">l</w>’<w n="8.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="8.7">n</w>’<w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="8.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg><pgtc id="4" weight="2" schema="CR">nn<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></rhyme></pgtc></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2" punct="pe:3">c<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe ps">a</seg></w> ! … <w n="9.3">N<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.7" punct="pt:12">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>bl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>x</w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="10.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.8" punct="vg:10">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="10.9" punct="pe:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe ps">an</seg>t</rhyme></pgtc></w> ! …</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="11.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="C">i</seg></w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>g</w> <w n="11.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="11.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="11.10" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ppr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="12.3" punct="vg:4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="12.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="12.5" punct="ps:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="ps" caesura="1">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>…<caesura></caesura> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="12.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="12.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ A-dam.</note>
					</closer>
			</div></body></text></TEI>