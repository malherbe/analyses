<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT146" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abab)" er_moy="1.17" er_max="2" er_min="0" er_mode="2(3/6)" er_moy_et="0.9" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="number">XLVI</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="1.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.6" punct="pv:6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pv" caesura="1">on</seg>t</w> ;<caesura></caesura> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="2.8">c</w>’<w n="2.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="2.10">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.12" punct="pt:12">m<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="C">i</seg></w> <w n="3.3" punct="vg:5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="3.4" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.8" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rd</w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="4.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d</w> <w n="4.8" punct="pt:12">s<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg><pgtc id="2" weight="1" schema="GR">y<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="6" caesura="1">e</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.5">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.7" punct="pe:12">f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="448" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="6.3" punct="vg:6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="6.6" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">d<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>st<seg phoneme="y" type="vs" value="1" rule="453" place="6" caesura="1">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.8">s<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.11" punct="pv:12">b<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.6" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ts</w>,<caesura></caesura> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="8.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="8.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="8.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="8.11" punct="pt:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="4" weight="2" schema="CR">d<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="2" mp="M/mp">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="9.4" punct="dp:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp" caesura="1">a</seg>s</w> :<caesura></caesura> <w n="9.5">c</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="10.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>g</w> <w n="10.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="10.9" punct="pt:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg><pgtc id="6" weight="2" schema="CR">qu<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="11.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.9" punct="pv:12">l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="12.9" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">c<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Stras-bourg.</note>
					</closer>
			</div></body></text></TEI>