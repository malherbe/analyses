<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT267" modus="cp" lm_max="12" metProfile="8, 6, 4, 2, 6+6, 4+6" form="suite périodique" schema="6(aa)" er_moy="1.33" er_max="6" er_min="0" er_mode="0(4/6)" er_moy_et="2.21" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="number">XVII</head>
					<div n="1" type="section">
					<head type="number">1.</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g</w> <w n="1.5">g<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="1.10" punct="vg:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg><pgtc id="1" weight="2" schema="CR">pl<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.4" punct="vg:6">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="2.5" punct="vg:9">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="2.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="2.7" punct="pt:12">j<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="2" type="section">
					<head type="number">2.</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="3" num="1.1" lm="10" met="4+6"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="3.8" punct="vg:10">v<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.2" lm="10" met="4+6"><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="4.2" punct="vg:2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2" punct="vg">e</seg>d</w>, <w n="4.3" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="4.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.8" punct="pt:10">c<seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="3" type="section">
					<head type="number">3.</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="5" num="1.1" lm="8" met="8"><w n="5.1">S</w>’<w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ss<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="5.6" punct="vg:8">m<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.2" lm="8" met="8"><w n="6.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.4" punct="pt:8">s<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="4" type="section">
					<head type="number">4.</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="7" num="1.1" lm="6" met="6"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="7.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.6" punct="vg:6">c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
					<l n="8" num="1.2" lm="6" met="6"><w n="8.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.4">d</w>’<w n="8.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="5" type="section">
					<head type="number">5.</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="9" num="1.1" lm="4" met="4"><w n="9.1">S</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.4" punct="vg:4">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="1.2" lm="4" met="4"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3" punct="pt:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="6" type="section">
					<head type="number">6.</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="11" num="1.1" lm="2" met="2"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2" punct="pi:2">f<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="pi ps">ai</seg>t</rhyme></pgtc></w> ?…</l>
					<l n="12" num="1.2" lm="2" met="2"><w n="12.1" punct="pt:2">M<seg phoneme="y" type="vs" value="1" rule="d-3" place="1">u</seg><pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
				</lg>
					</div>
						<closer>
						<note id="none" type="footnote">▪ Artère<lb></lb>▪ route<lb></lb>▪ tube<lb></lb>▪ été<lb></lb>▪ re<lb></lb>▪ e</note>
					</closer>
			</div></body></text></TEI>