<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT14" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="3[abab]" er_moy="2.0" er_max="6" er_min="0" er_mode="2(3/6)" er_moy_et="2.0" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="number">XIV</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6">« <w n="1.1" punct="vg:2">J<seg phoneme="a" type="vs" value="1" rule="310" place="1" mp="M">ea</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="1.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="Lp">a</seg>s</w>-<w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.5" punct="pi:6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pi ps" caesura="1">on</seg>c</w> ?<caesura></caesura> … <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">I</seg>l</w> <w n="1.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.8">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t</w> <w n="1.9" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</w>, <w n="1.10" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" caesura="1">e</seg>ds</w><caesura></caesura> <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="2.10">s<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>l</w> <w n="2.11" punct="pt:12"><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6">— <w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="3.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fm">e</seg></w>-<w n="3.4" punct="pe:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe" caesura="1">oi</seg></w> !<caesura></caesura> <w n="3.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">m</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="4.4">S<seg phoneme="i" type="vs" value="1" rule="493" place="4" mp="M">y</seg>lv<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.6">cr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.8" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6">— <w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">t</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.5" punct="pv:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pv" caesura="1">u</seg>s</w> ;<caesura></caesura> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="5.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="5.8" punct="vg:9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9" punct="vg">oin</seg>s</w>, <w n="5.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="5.10"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="5.11">l</w>’<w n="5.12" punct="vg:12">h<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="6.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="6.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="6.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="6.8" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">â</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="12" punct="pt">ai</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">t</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="445" place="6" caesura="1">û</seg>r</w><caesura></caesura> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="7.7">s<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>il</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="7.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="8.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="8.4">cr<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" caesura="1">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="8.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="8.8" punct="pt:12">b<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="12" punct="pt">ai</seg></rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6">— <w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2" punct="ps:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="ps">en</seg>ds</w>… <w n="9.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="9.5" punct="pe:6">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="pe" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !<caesura></caesura> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">I</seg>l</w> <w n="9.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.8">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t</w> <w n="9.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.10">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.4" punct="ps:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="ps" caesura="1">in</seg></w>…<caesura></caesura> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="10.8" punct="pe:12">t<pgtc id="6" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>nn<rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="11.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="11.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="11.6">gr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.7">t</w>’<w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.9">r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="11.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="11.11">b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>p</w><caesura></caesura> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg>s</w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.10">m<pgtc id="6" weight="6" schema="V(c)[R" part="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></pgtc></w> <w n="12.11" punct="pt:12"><pgtc id="6" weight="6" schema="V(c)[R" part="2"><rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></rhyme></pgtc></w>. »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Il ne faut pas dire: Fontaine, je ne boirai pas de ton eau.</note>
					</closer>
			</div></body></text></TEI>