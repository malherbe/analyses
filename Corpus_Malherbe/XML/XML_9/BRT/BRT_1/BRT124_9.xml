<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT124" modus="cp" lm_max="12" metProfile="6, 6+6" form="suite de strophes" schema="4[abab]" er_moy="1.88" er_max="6" er_min="0" er_mode="0(3/8)" er_moy_et="1.9" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="number">XXIV</head>
				<lg n="1" type="regexp" rhyme="abababababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:2">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="pe">eu</seg>r</w> ! <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2" punct="vg:3">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4" punct="pe:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="3.4">d</w>’<w n="3.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="3.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M/mp">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="3.7">lu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="3.8">l</w>’<w n="3.9" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="4.2" punct="vg:3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mp">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="Lp">a</seg></w>-<w n="4.4">t</w>-<w n="4.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>l</w>,<caesura></caesura> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="4.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.10" punct="pt:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="5.4">b<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>f</w> <w n="5.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="5.8">l<pgtc id="3" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="5.9" punct="vg:12"><pgtc id="3" weight="6" schema="V[CR" part="2">pl<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="6.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>b<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>ts</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>gn<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="7.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c</w> <w n="7.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="7.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.8">s<pgtc id="3" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="7.9" punct="dp:12"><pgtc id="3" weight="6" schema="V[CR" part="2">l<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <subst type="completion" reason="analysis" hand="ML"><del>....</del><add rend="hidden"><w n="8.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">oû</seg>t</w></add><caesura></caesura></subst> <w n="8.6">n</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="8.10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="8.12" punct="pe:12">ch<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>ts</rhyme></pgtc></w> !</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>p</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <subst type="completion" reason="analysis" hand="ML"><del>...</del><add rend="hidden"><w n="9.6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w></add><caesura></caesura></subst> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="9.10" punct="vg:12">br<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="405" place="2" mp="M">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.7">d</w>’<w n="10.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>z<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lc<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="11.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="6" caesura="1">a</seg>il</w><caesura></caesura> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.6">t<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="12.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="12.8" punct="pt:12">s<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="y" type="vs" value="1" rule="445" place="12" punct="pt">û</seg>r</rhyme></pgtc></w>.</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">S</w>’<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>cl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="7" weight="2" schema="CR" part="1">s<rhyme label="a" id="7" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="14.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <subst type="completion" reason="analysis" hand="ML"><del>......</del><add rend="hidden"><w n="14.6" punct="vg:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w></add><caesura></caesura></subst>, <w n="14.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.8" punct="dp:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="8" weight="3" schema="CGR" part="1">du<rhyme label="b" id="8" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="dp">i</seg>t</rhyme></pgtc></w> :</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="15.2" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">im</seg>p<seg phoneme="o" type="vs" value="1" rule="415" place="3" punct="vg">ô</seg>t</w>, <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mp">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="Lp">a</seg></w>-<w n="15.4">t</w>-<w n="15.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>l</w>,<caesura></caesura> <w n="15.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="15.7">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="15.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9" punct="pi:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg><pgtc id="7" weight="2" schema="CR" part="1">s<rhyme label="a" id="7" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					<l n="16" num="1.16" lm="6" met="6"><space unit="char" quantity="12"></space><w n="16.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5" punct="pi:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="8" weight="3" schema="CGR" part="1">du<rhyme label="b" id="8" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pi">i</seg>t</rhyme></pgtc></w> ? »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Mou-lin.</note>
					</closer>
			</div></body></text></TEI>