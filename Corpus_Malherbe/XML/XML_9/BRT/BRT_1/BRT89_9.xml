<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT89" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="8[abab]" er_moy="1.12" er_max="6" er_min="0" er_mode="0(9/16)" er_moy_et="1.58" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
				<head type="number">LXXXIX</head>
				<lg n="1" type="regexp" rhyme="abababababababababababababababab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>lp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="1.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>, <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="2.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w>-<w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd</w> <w n="4.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7" punct="pt:8"><pgtc id="2" weight="2" schema="[CR">m<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pt">e</seg></w>. <w n="5.3">L<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><pgtc id="3" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="œ" type="vs" value="1" rule="286" place="1">œ</seg>il</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">s</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<pgtc id="3" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.3" punct="dp:8">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>r</rhyme></pgtc></w> :</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="10.3" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="10.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="10.6" punct="pv:8">f<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5" punct="vg:8">br<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ch<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="12.6" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="14.5" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ge<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="15" num="1.15" lm="8" met="8"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.4" punct="vg:5">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.6">tr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="1.16" lm="8" met="8"><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="16.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					<l n="17" num="1.17" lm="8" met="8"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3" punct="vg:4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>t</w>, <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5">l</w>’<w n="17.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" stanza="5" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="1.18" lm="8" met="8"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="18.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="18.4" punct="pv:4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pv">eau</seg></w> ; <w n="18.5">c</w>’<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="18.7" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="10" weight="2" schema="CR">d<rhyme label="b" id="10" gender="m" type="a" stanza="5" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
					<l n="19" num="1.19" lm="8" met="8"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="19.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.4">m<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" stanza="5" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="1.20" lm="8" met="8"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="10" weight="2" schema="CR">d<rhyme label="b" id="10" gender="m" type="e" stanza="5" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					<l n="21" num="1.21" lm="8" met="8"><w n="21.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="21.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>mm<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="21.5">b<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" stanza="6" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="1.22" lm="8" met="8"><w n="22.1" punct="vg:1">B<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1" punct="vg">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="22.3" punct="vg:3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>s</w>, <w n="22.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="22.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="22.7" punct="pe:8"><pgtc id="12" weight="2" schema="[CR">g<rhyme label="b" id="12" gender="m" type="a" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
					<l n="23" num="1.23" lm="8" met="8"><w n="23.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="23.2" punct="vg:3">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="23.6" punct="pe:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" stanza="6" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="24" num="1.24" lm="8" met="8"><w n="24.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">oû</seg>ts</w> <w n="24.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="24.4">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="24.5">d</w>’<w n="24.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="354" place="6">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="12" weight="2" schema="CR">ge<rhyme label="b" id="12" gender="m" type="e" stanza="6" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>ts</rhyme></pgtc></w>.</l>
					<l n="25" num="1.25" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a" stanza="7" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="1.26" lm="8" met="8"><w n="26.1">S</w>’<w n="26.2" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="26.4">l</w>’<w n="26.5">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="4">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.6" punct="vg:8">p<seg phoneme="i" type="vs" value="1" rule="dc-1" place="6">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="14" weight="2" schema="CR">t<rhyme label="b" id="14" gender="m" type="a" stanza="7" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="1.27" lm="8" met="8"><w n="27.1">Pr<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="27.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="27.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">aim</seg></w> <w n="27.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="27.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e" stanza="7" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="28" num="1.28" lm="8" met="8"><w n="28.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="28.3" punct="vg:4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>x</w>, <w n="28.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="28.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="28.6" punct="pt:8"><pgtc id="14" weight="2" schema="[CR">t<rhyme label="b" id="14" gender="m" type="e" stanza="7" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="29" num="1.29" lm="8" met="8"><w n="29.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="29.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="29.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="29.4" punct="vg:5">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="29.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="29.6" punct="dp:8">f<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="f" type="a" stanza="8" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="30" num="1.30" lm="8" met="8"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="30.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="30.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="a" stanza="8" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="31" num="1.31" lm="8" met="8"><w n="31.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="31.2">c</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="31.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="31.6">d</w>’<w n="31.7"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="31.8">f<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="f" type="e" stanza="8" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="32" num="1.32" lm="8" met="8"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.3">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="32.4">d</w>’<w n="32.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="32.6" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="e" stanza="8" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Fin contre fin ne fait pas bonne doublure.</note>
					</closer>
			</div></body></text></TEI>