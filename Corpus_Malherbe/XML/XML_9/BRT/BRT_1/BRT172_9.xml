<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT172" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab] 1[aa] 1[abba]" er_moy="0.86" er_max="2" er_min="0" er_mode="0(4/7)" er_moy_et="0.99" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="number">XXIII</head>
				<lg n="1" type="regexp" rhyme="ababaaabbaabab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="2.5" punct="dp:8"><pgtc id="2" weight="2" schema="[CR">f<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="dp">in</seg></rhyme></pgtc></w> :</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p</w> <w n="4.4">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6" punct="pt:8">B<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">ff<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="5.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="5.5">l</w>’<w n="5.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rts</w> <w n="6.5">d</w>’<w n="6.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg><pgtc id="3" weight="2" schema="CR">fr<rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>bs<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3" punct="vg:5">J<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>, <w n="7.4" punct="pt:8">B<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8" punct="pt">o</seg></rhyme></pgtc></w>.</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="vg:8">m<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>s</w>, <w n="9.7">c</w>’<w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.9">l</w>’<w n="9.10" punct="dp:8"><pgtc id="5" weight="0" schema="[R"><rhyme label="b" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="10.4">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="10.6">l</w>’<w n="10.7" punct="pt:8"><pgtc id="4" weight="0" schema="[R"><rhyme label="a" id="4" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ts</w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="11.7" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="12.6" punct="pv:8">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a" stanza="4" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="a" type="vs" value="1" rule="194" place="4">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="13.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" stanza="4" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="14.4">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.6" punct="pt:8">br<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg>y<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e" stanza="4" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Équateur.</note>
					</closer>
			</div></body></text></TEI>