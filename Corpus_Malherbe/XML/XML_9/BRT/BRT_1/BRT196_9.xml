<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT196" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="3[abab]" er_moy="1.17" er_max="2" er_min="0" er_mode="2(3/6)" er_moy_et="0.9" qr_moy="0.0" qr_max="C0" qr_mode="0(6/6)" qr_moy_et="0.0">
				<head type="number">XXI</head>
				<lg n="1" type="regexp" rhyme="ab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="1.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.8" punct="vg:12">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="2.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="pi:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg>s</rhyme></pgtc></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="regexp" rhyme="ab">
					<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">st<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="3.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.7" punct="vg:12">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="4.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="4.5" punct="pt:8"><pgtc id="2" weight="2" schema="[CR">r<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="regexp" rhyme="ab">
					<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="5.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.9" punct="vg:12">ch<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="4">u</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="6.5" punct="pt:8">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>c<pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="regexp" rhyme="ab">
					<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2" punct="vg:2">pr<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.7">p<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="7.10" punct="vg:12">p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">M<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rps</w> <w n="8.5" punct="pt:8"><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5" type="regexp" rhyme="ab">
					<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="9.4" punct="vg:6">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="9.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg><pgtc id="5" weight="2" schema="CR">nn<rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="5.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="10.4">n<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>fs</w> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="10.6" punct="pt:8">v<pgtc id="6" weight="1" schema="GR">i<rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6" type="regexp" rhyme="ab">
					<l n="11" num="6.1" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">S<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.4">S<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="11.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="11.8" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="12.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.7"><pgtc id="6" weight="1" schema="GR">y<rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Sceaux, sot, seau, saut, sceau, sceau-de-Salomon.</note>
					</closer>
			</div></body></text></TEI>