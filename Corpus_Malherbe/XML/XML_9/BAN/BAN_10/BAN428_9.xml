<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN428" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)" er_moy="2.5" er_max="6" er_min="0" er_mode="2(8/16)" er_moy_et="2.18" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
				<head type="main">Réplique</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="1.4" punct="dp:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="dp">en</seg>t</rhyme></pgtc></w> :</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="vg:8">Th<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.8" punct="pe:8">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="5.5">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR">n<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.5" punct="vg:8">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">c</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.5">j<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="7.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="7.9"><pgtc id="3" weight="2" schema="[CR">n<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="8.5">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>c</w> <w n="8.6">d</w>’<w n="8.7" punct="pt:8"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="9.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="9.5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR">ph<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="10.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="10.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.6" punct="vg:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="6" weight="2" schema="CR">rr<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5" punct="vg:8">z<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="5" weight="2" schema="CR">ph<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="8" punct="vg">y</seg>r</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="6" weight="2" schema="CR">rr<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="1">en</seg>s</w> <w n="13.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ts</w>, <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="13.4">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="7" weight="6" schema="CVR">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1" punct="vg:2">M<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.2" punct="tc:3"><seg phoneme="e" type="vs" value="1" rule="189" place="3" punct="ti">e</seg>t</w> — <w n="14.3">c</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.8"><pgtc id="8" weight="2" schema="[CR">br<rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="15.2" punct="tc:4">s<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="ti">e</seg></w> — <w n="15.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>s<pgtc id="7" weight="6" schema="CVR">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.4" punct="pt:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="8" weight="2" schema="CR">r<rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="17.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR">pp<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.5" punct="vg:8">c<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="19.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="19.5" punct="dp:8"><pgtc id="9" weight="2" schema="[CR">p<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="dp">a</seg>s</rhyme></pgtc></w> :</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>ph<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.5" punct="pe:8"><pgtc id="10" weight="2" schema="[CR">r<rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2" punct="vg:4">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg">en</seg>s</w>, <w n="21.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="21.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="21.6" punct="vg:8">pl<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">aî</seg>t</rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">C<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="22.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="22.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="22.4" punct="pt:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>gr<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="23.4" punct="dp:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp">i</seg></w> : <w n="23.5">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><pgtc id="11" weight="0" schema="[R" part="1"><rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">cr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="24.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6">om</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6">f<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="25.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="25.6" punct="pi:8">g<pgtc id="13" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>x</rhyme></pgtc></w> ?</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="26.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.5">d<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<pgtc id="14" weight="6" schema="V[CR" part="1"><seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></pgtc></w> <w n="26.7"><pgtc id="14" weight="6" schema="V[CR" part="2">f<rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="27.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="27.3">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="27.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="27.6">d<pgtc id="13" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></pgtc></w> <w n="27.7" punct="pi:8"><pgtc id="13" weight="6" schema="V[CR" part="2">n<rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pi">ou</seg>s</rhyme></pgtc></w> ?</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1" punct="pe:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="pe">oi</seg></w> ! <w n="28.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="28.3">d</w>’<w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="28.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="28.6" punct="pt:8">J<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ph<rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="29.3">l</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="29.5">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="29.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="29.7" punct="vg:8"><pgtc id="15" weight="2" schema="[CR" part="1">n<rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="8" punct="vg">om</seg></rhyme></pgtc></w>,</l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>lt<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="30.3" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>b<pgtc id="16" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="16" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="31.2">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="31.3" punct="tc:5">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="ti">e</seg></w> — <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.5">c<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="15" weight="2" schema="CR" part="1">n<rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="32.2">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="32.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="32.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="32.6" punct="pt:8">t<pgtc id="16" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<rhyme label="b" id="16" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>