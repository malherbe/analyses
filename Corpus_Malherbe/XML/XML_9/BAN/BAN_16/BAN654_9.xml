<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN654" modus="sp" lm_max="7" metProfile="7, 3" form="suite périodique" schema="13(abab)" er_moy="1.42" er_max="6" er_min="0" er_mode="2(13/26)" er_moy_et="1.42" qr_moy="0.38" qr_max="N5" qr_mode="0(24/26)" qr_moy_et="1.33">
				<head type="number">XXI</head>
				<head type="main">L’Écho de Paris</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="1.6" punct="vg:5">p<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg>r</w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="1.8"><pgtc id="1" weight="2" schema="[CR">s<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="2.1">Cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="2.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="2.3" punct="vg:3"><pgtc id="2" weight="2" schema="[CR">f<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="3.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.5"><pgtc id="1" weight="2" schema="[CR">s<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="4.1">Bru<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="4.2">d</w>’<w n="4.3" punct="pt:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg><pgtc id="2" weight="2" schema="CR">f<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="pt">e</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="5.2" punct="vg:2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="5.6" punct="vg:7">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="6">eu</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1" punct="pv:1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="pv">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.5">p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="3" weight="2" schema="CR">tr<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="8.1">L</w>’<w n="8.2" punct="pt:3"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pt">a</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3" punct="vg:4">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">li<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="10.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="11.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="11.6">bl<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3" punct="vg:3">Di<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="N5"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="13.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.5" punct="vg:7"><pgtc id="7" weight="2" schema="[CR">d<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" punct="vg">en</seg>ts</rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="14.2">t<pgtc id="8" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></rhyme></pgtc></w></l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1" punct="vg:2">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.4">l<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>s</w> <w n="15.5"><pgtc id="7" weight="2" schema="[CR">d<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2" punct="pv:3">v<pgtc id="8" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5"><pgtc id="9" weight="2" schema="[CR">v<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="18.1">M<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="19.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg><pgtc id="9" weight="2" schema="CR">v<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="20.2" punct="vg:3">l<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ri<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="7" met="7"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="21.4" punct="vg:7">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="dc-1" place="6">i</seg><pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="22.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg></w> <w n="22.2">d</w>’<w n="22.3" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ffr<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="7" met="7"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="23.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">O</seg>rph<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ci<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="24.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="24.3" punct="pt:3">r<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="7" met="7"><w n="25.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="25.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.6" punct="vg:7">sc<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<pgtc id="13" weight="2" schema="CR">pt<rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="26" num="7.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="26.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">g<pgtc id="14" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</rhyme></pgtc></w></l>
					<l n="27" num="7.3" lm="7" met="7"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="27.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg><pgtc id="13" weight="2" schema="CR">pt<rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</rhyme></pgtc></w></l>
					<l n="28" num="7.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="28.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="28.2" punct="pv:3">n<pgtc id="14" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="7" met="7"><w n="29.1">L</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="29.3">s</w>’<w n="29.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="29.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="29.7">v<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l</rhyme></pgtc></w></l>
					<l n="30" num="8.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="30.1">Sv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="30.3" punct="vg:3"><pgtc id="16" weight="2" schema="[CR">f<rhyme label="b" id="16" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rt</rhyme></pgtc></w>,</l>
					<l n="31" num="8.3" lm="7" met="7"><w n="31.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="31.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="31.4">R<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l</rhyme></pgtc></w></l>
					<l n="32" num="8.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2" punct="pv:3">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg><pgtc id="16" weight="2" schema="CR">f<rhyme label="b" id="16" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pv">o</seg>rt</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="7" met="7"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="33.3" punct="vg:5"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="33.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<pgtc id="17" weight="2" schema="CR">m<rhyme label="a" id="17" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</rhyme></pgtc></w></l>
					<l n="34" num="9.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="34.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="34.2" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">O</seg><pgtc id="18" weight="2" schema="CR">ph<rhyme label="b" id="18" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="35" num="9.3" lm="7" met="7"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>c<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="35.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="35.3">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg><pgtc id="17" weight="2" schema="CR">m<rhyme label="a" id="17" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</rhyme></pgtc></w></l>
					<l n="36" num="9.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="36.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2" punct="vg:3">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg><pgtc id="18" weight="2" schema="CR">ph<rhyme label="b" id="18" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="10" type="quatrain" rhyme="abab">
					<l n="37" num="10.1" lm="7" met="7"><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="37.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="37.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="37.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="37.5" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg><pgtc id="19" weight="2" schema="CR">l<rhyme label="a" id="19" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
					<l n="38" num="10.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="38.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="38.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<pgtc id="20" weight="2" schema="CR">t<rhyme label="b" id="20" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</rhyme></pgtc></w></l>
					<l n="39" num="10.3" lm="7" met="7"><w n="39.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="39.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="39.3">m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="39.4"><pgtc id="19" weight="2" schema="[CR">l<rhyme label="a" id="19" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</rhyme></pgtc></w></l>
					<l n="40" num="10.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="40.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rs</w> <w n="40.2" punct="pt:3">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><pgtc id="20" weight="2" schema="CR">t<rhyme label="b" id="20" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="pt">au</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="11" type="quatrain" rhyme="abab">
					<l n="41" num="11.1" lm="7" met="7"><w n="41.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="41.2" punct="vg:3">P<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="41.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="41.4">d</w>’<w n="41.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<pgtc id="21" weight="0" schema="R"><rhyme label="a" id="21" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</rhyme></pgtc></w></l>
					<l n="42" num="11.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="42.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="42.3" punct="vg:3">br<pgtc id="22" weight="1" schema="GR">u<rhyme label="b" id="22" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="43" num="11.3" lm="7" met="7"><w n="43.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="43.2">s</w>’<w n="43.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="43.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="43.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="43.6">j<pgtc id="21" weight="0" schema="R"><rhyme label="a" id="21" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</rhyme></pgtc></w></l>
					<l n="44" num="11.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="44.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="44.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="44.3" punct="pv:3">n<pgtc id="22" weight="1" schema="GR">u<rhyme label="b" id="22" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="12" type="quatrain" rhyme="abab">
					<l n="45" num="12.1" lm="7" met="7"><w n="45.1">C</w>’<w n="45.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="45.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="3">oi</seg></w> <w n="45.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="45.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="45.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<pgtc id="23" weight="2" schema="CR">m<rhyme label="a" id="23" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</rhyme></pgtc></w></l>
					<l n="46" num="12.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="46.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="46.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><pgtc id="24" weight="0" schema="R"><rhyme label="b" id="24" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</rhyme></pgtc></w></l>
					<l n="47" num="12.3" lm="7" met="7"><w n="47.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="47.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="47.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="47.4">r<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg><pgtc id="23" weight="2" schema="CR">m<rhyme label="a" id="23" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</rhyme></pgtc></w></l>
					<l n="48" num="12.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="48.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="48.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="48.3" punct="pt:3">ci<pgtc id="24" weight="0" schema="R"><rhyme label="b" id="24" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="pt">e</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="13" type="quatrain" rhyme="abab">
					<l n="49" num="13.1" lm="7" met="7"><w n="49.1">Tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="49.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="49.3" punct="vg:7">J<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ch<pgtc id="25" weight="0" schema="R"><rhyme label="a" id="25" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="vg">o</seg></rhyme></pgtc></w>,</l>
					<l n="50" num="13.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="50.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ts</w> <w n="50.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="50.3" punct="vg:3"><pgtc id="26" weight="2" schema="[CR">cr<rhyme label="b" id="26" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
					<l n="51" num="13.3" lm="7" met="7"><w n="51.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ths</w> <w n="51.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="51.3" punct="vg:4">fl<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="51.4">c</w>’<w n="51.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="51.6">l</w>’<w n="51.7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<pgtc id="25" weight="0" schema="R"><rhyme label="a" id="25" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg></rhyme></pgtc></w></l>
					<l n="52" num="13.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="52.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="52.2" punct="pe:3">P<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg><pgtc id="26" weight="2" schema="CR">r<rhyme label="b" id="26" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pe">i</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 31 octobre 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>