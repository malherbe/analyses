<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN643" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)" er_moy="2.89" er_max="8" er_min="0" er_mode="2(9/18)" er_moy_et="2.6" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
				<head type="number">X</head>
				<head type="main">L’Immortelle</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2">D<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.6" punct="pv:8"><pgtc id="1" weight="8" schema="[CVCR">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">n</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.5">qu</w>’<w n="2.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="3.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">fl<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="1" weight="8" schema="CVCR">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="4.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.6" punct="pt:8">Qu<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="5.5">ph<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>x</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">C<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="6.5"><pgtc id="4" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="6.6" punct="pv:8"><pgtc id="4" weight="2" schema="[C[R" part="2"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="7.5">m<seg phoneme="œ" type="vs" value="1" rule="151" place="6">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="7">eu</seg>r</w> <subst reason="analysis" type="phonemization" hand="RR"><del>X</del><add rend="hidden"><w n="7.6" punct="vg:8"><pgtc id="3" weight="0" schema="[R" part="1"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ks</rhyme></pgtc></w></add></subst>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6" punct="pt:8"><pgtc id="4" weight="2" schema="[CR" part="1">L<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>sl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR" part="1">l<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2" punct="vg:5">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="10.3">l</w>’<w n="10.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>p<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>p<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="11.7"><pgtc id="5" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="11.8"><pgtc id="5" weight="2" schema="[C[R" part="2"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="12.2">S<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ll<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg></w> <w n="12.3">Pr<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>dh<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.5" punct="pt:8">C<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>pp<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="13.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="7" weight="2" schema="CR" part="1">r<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="8" weight="2" schema="CR" part="1">n<rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">M<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="15.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rd</w> <w n="15.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="7" weight="2" schema="CR" part="1">r<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="16.3">j<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="16.5">s</w>’<w n="16.6" punct="pt:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="8" weight="2" schema="CR" part="1">n<rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">v<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="17.3" punct="vg:3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="17.5">c</w>’<w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="17.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="17.8" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="9" weight="2" schema="CR" part="1">c<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="18.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>b<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="19.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="19.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="19.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="9" weight="2" schema="CR" part="1">sc<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="20.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="20.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="20.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.6" punct="pt:8">vi<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>s</w> <w n="21.6">d</w>’<w n="21.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="11" weight="2" schema="CR" part="1">pp<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>s</w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="22.4">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="22.5">qu</w>’<w n="22.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="22.7" punct="vg:8">r<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="23.4">n</w>’<w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="23.6">m<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rt</w> <w n="23.7" punct="pt:8"><pgtc id="11" weight="2" schema="[CR" part="1">p<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="24.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="24.5">d</w>’<w n="24.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="25.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="25.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>s</w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.6">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="13" weight="2" schema="CR" part="1">m<rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</rhyme></pgtc></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.6" punct="vg:8">f<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="27.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="13" weight="2" schema="CR" part="1">m<rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ts</rhyme></pgtc></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="28.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="28.5" punct="pt:8">j<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="29.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="29.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="29.4" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg">e</seg>rs</w>, <w n="29.5"><pgtc id="15" weight="8" schema="[CVC[R" part="1">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</pgtc></w> <w n="29.6"><pgtc id="15" weight="8" schema="[CVC[R" part="2"><rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="30.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="30.3">ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="30.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="30.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.6" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="16" weight="2" schema="CR" part="1">p<rhyme label="b" id="16" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="31.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="31.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="31.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="31.7"><pgtc id="15" weight="8" schema="[CVCR" part="1">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="32.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="32.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.7" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="16" weight="2" schema="CR" part="1">p<rhyme label="b" id="16" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="33.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="33.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="17" weight="2" schema="CR" part="1">v<rhyme label="a" id="17" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="34.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="34.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="34.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="34.7" punct="vg:8">si<pgtc id="18" weight="0" schema="R" part="1"><rhyme label="b" id="18" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="35.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="35.4" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="35.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="35.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="35.7" punct="vg:8"><pgtc id="17" weight="2" schema="[CR" part="1">v<rhyme label="a" id="17" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.3" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="18" weight="0" schema="R" part="1"><rhyme label="b" id="18" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 21 juillet 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>