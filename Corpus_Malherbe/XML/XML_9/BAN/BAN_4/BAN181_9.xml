<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN181" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(ababcc)" er_moy="2.89" er_max="8" er_min="0" er_mode="0(3/9)" er_moy_et="2.85" qr_moy="0.0" qr_max="C0" qr_mode="0(9/9)" qr_moy_et="0.0">
				<head type="main">Les Baisers</head>
				<lg n="1" type="sizain" rhyme="ababcc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="1.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:2">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>f</w>, <w n="2.2">j</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="2.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.6" punct="vg:8">pr<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="3.4">n</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.7" punct="pv:8">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>ts</rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>h<pgtc id="2" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.8" punct="pt:8"><pgtc id="3" weight="2" schema="[CR">m<rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="ababcc">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="7.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</w>, <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">l<pgtc id="4" weight="6" schema="V[CR" part="1"><seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</pgtc></w> <w n="7.7" punct="pe:8"><pgtc id="4" weight="6" schema="V[CR" part="2">d<rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>s</rhyme></pgtc></w> !</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="8.2">h<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.5" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="5" weight="2" schema="CR" part="1">rr<rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="9.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="9.6">t<pgtc id="4" weight="6" schema="V[CR" part="1"><seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</pgtc></w> <w n="9.7" punct="pv:8"><pgtc id="4" weight="6" schema="V[CR" part="2">d<rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>gts</rhyme></pgtc></w> ;</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="vg:4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="10.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.7">h<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="5" weight="2" schema="CR" part="1">r<rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>s</rhyme></pgtc></w></l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.6" punct="vg:8">tr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="c" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
					<l n="12" num="2.6" lm="8" met="8"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="12.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="12.7">d</w>’<w n="12.8" punct="pe:8"><pgtc id="6" weight="0" schema="[R" part="1"><rhyme label="c" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="ababcc">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">m</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="13.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="13.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="13.8" punct="pe:8">ci<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pe">e</seg>l</rhyme></pgtc></w> !</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<pgtc id="8" weight="8" schema="CVCR" part="1">d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></pgtc></w></l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="15.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="15.4" punct="vg:8">cr<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="16.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">m</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.7"><pgtc id="8" weight="8" schema="CVCR" part="1">d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></pgtc></w></l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="17.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.6">fl<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="c" id="9" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="8" met="8"><w n="18.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="18.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="18.6" punct="pt:8">pl<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="c" id="9" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nice</placeName>,
						<date when="1861">février 1861.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>