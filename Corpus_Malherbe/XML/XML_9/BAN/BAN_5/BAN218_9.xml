<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN218" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="7(aabccb)" er_moy="2.43" er_max="8" er_min="0" er_mode="0(8/21)" er_moy_et="2.74" qr_moy="0.0" qr_max="C0" qr_mode="0(21/21)" qr_moy_et="0.0">
				<head type="number">XXVI</head>
				<head type="main">A Zola</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4" punct="vg:8"><pgtc id="1" weight="8" schema="[CVCR">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t</w>-<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg><pgtc id="1" weight="8" schema="CVCR">rr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2" punct="vg:4">F<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>fr<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4">v<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">P<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.6">t<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2" punct="pt:4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="8.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.7">y<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="9" num="2.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4" punct="vg:4">j<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="10.2" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="10.5" punct="vg:8"><pgtc id="6" weight="8" schema="[CVCR">Z<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="c" id="6" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="11.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="11.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="6" weight="8" schema="CVCR">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="c" id="6" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
					<l n="12" num="2.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="12.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">l</w>’<w n="12.4" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.4">r<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.6">p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg><pgtc id="7" weight="2" schema="CR">mmi<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="14.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.5" punct="vg:8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="7" weight="2" schema="CR">mi<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="3.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="15.1" punct="vg:1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3" punct="vg:4">r<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="3.4" lm="8" met="8">(<w n="16.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="16.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="16.5" punct="pf:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">A</seg><pgtc id="9" weight="2" schema="CR">b<rhyme label="c" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></pgtc></w>,)</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="17.7"><pgtc id="9" weight="2" schema="[CR">b<rhyme label="c" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="18.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="18.2">d</w>’<w n="18.3" punct="pv:4"><pgtc id="8" weight="0" schema="[R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">Th<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.4">G<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="10" weight="3" schema="CGR">ti<rhyme label="a" id="10" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1" punct="vg:2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="20.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="20.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="20.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="10" weight="3" schema="CGR">ti<rhyme label="a" id="10" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
					<l n="21" num="4.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="21.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="21.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="21.3" punct="pv:4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>bl<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="22.4" punct="vg:8">n<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="12" weight="2" schema="CR">b<rhyme label="c" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>d</rhyme></pgtc></w>,</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="12" weight="2" schema="CR">b<rhyme label="c" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</rhyme></pgtc></w></l>
					<l n="24" num="4.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="24.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="24.3" punct="pv:4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>ppr<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabccb">
					<l n="25" num="5.1" lm="8" met="8"><w n="25.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="25.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="25.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="25.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="25.7">l<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>s</rhyme></pgtc></w></l>
					<l n="26" num="5.2" lm="8" met="8"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="26.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="26.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.6">C<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>pr<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
					<l n="27" num="5.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="27.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="27.3" punct="vg:4">t<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="28" num="5.4" lm="8" met="8"><w n="28.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="28.3">si<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>d</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="28.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="28.6" punct="vg:8">fl<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="15" weight="8" schema="CVCR">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="c" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="29" num="5.5" lm="8" met="8"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="29.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="29.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="29.5"><pgtc id="15" weight="8" schema="[CVCR">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="c" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="30" num="5.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="30.4" punct="pt:4">r<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="31" num="6.1" lm="8" met="8"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="31.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="31.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="31.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rds</w> <w n="31.6" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="16" weight="2" schema="CR">v<rhyme label="a" id="16" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>ts</rhyme></pgtc></w>,</l>
					<l n="32" num="6.2" lm="8" met="8"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="32.3">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg>s</w> <w n="32.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="16" weight="2" schema="CR">v<rhyme label="a" id="16" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</rhyme></pgtc></w></l>
					<l n="33" num="6.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="33.2">l</w>’<w n="33.3" punct="pe:4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="34" num="6.4" lm="8" met="8"><w n="34.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="34.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="34.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="34.5">G<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<pgtc id="18" weight="4" schema="VR"><seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg><rhyme label="c" id="18" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="476" place="8">ï</seg></rhyme></pgtc></w></l>
					<l n="35" num="6.5" lm="8" met="8"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="35.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="35.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="35.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="35.5">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-27" place="6">e</seg></w> <w n="35.6">h<pgtc id="18" weight="4" schema="VR"><seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg><rhyme label="c" id="18" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="476" place="8">ï</seg></rhyme></pgtc></w></l>
					<l n="36" num="6.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="36.1">F<seg phoneme="y" type="vs" value="1" rule="445" place="1">û</seg>t</w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="36.3" punct="vg:4">n<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="7" type="sizain" rhyme="aabccb">
					<l n="37" num="7.1" lm="8" met="8"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="37.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="37.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="37.4">c</w>’<w n="37.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="37.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="19" weight="2" schema="CR">d<rhyme label="a" id="19" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></pgtc></w>,</l>
					<l n="38" num="7.2" lm="8" met="8"><w n="38.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rs</w>, <w n="38.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="38.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="38.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="38.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w> <w n="38.7"><pgtc id="19" weight="2" schema="[CR">d<rhyme label="a" id="19" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></pgtc></w></l>
					<l n="39" num="7.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="39.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="39.2"><pgtc id="20" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="39.3"><pgtc id="20" weight="2" schema="[C[R" part="2"><rhyme label="b" id="20" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">An</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="40" num="7.4" lm="8" met="8"><w n="40.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="40.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="40.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g</w> <w n="40.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="40.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="21" weight="2" schema="CR" part="1">ss<rhyme label="c" id="21" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
					<l n="41" num="7.5" lm="8" met="8"><w n="41.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="41.2">n</w>’<w n="41.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="41.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="41.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="41.6" punct="dp:8"><pgtc id="21" weight="2" schema="[CR" part="1">ç<rhyme label="c" id="21" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="dp">a</seg></rhyme></pgtc></w> :</l>
					<l n="42" num="7.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="42.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="42.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="42.3"><pgtc id="20" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="42.4" punct="pe:4"><pgtc id="20" weight="2" schema="[C[R" part="2"><rhyme label="b" id="20" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">21 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>