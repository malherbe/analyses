<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN212" modus="sm" lm_max="8" metProfile="8" form="rondel classique" schema="ABba abAB abbaA" er_moy="2.0" er_max="2" er_min="2" er_mode="2(7/7)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="number">XX</head>
				<head type="main">Centième</head>
				<lg n="1" rhyme="ABba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="1.5" punct="dp:8">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="A" id="1" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="dp">a</seg></rhyme></pgtc></w> :</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.6" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="B" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abAB">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe">oi</seg></w> ! <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="5.5">qu</w>’<w n="5.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.7" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="6.5" punct="pe:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="7.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="7.5" punct="dp:8">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="A" id="3" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="dp">a</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.6" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="B" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="abbaA">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="9.3" punct="vg:5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>s</w>, <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.4">fr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="vg:2">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></w>, <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="11.5">s</w>’<w n="11.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">Sh<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ksp<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.4" punct="pe:8">K<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg></rhyme></pgtc></w> !</l>
					<l n="13" num="3.5" lm="8" met="8"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="13.5" punct="pt:8">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="A" id="5" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">14 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>