<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRIOLETS</head><div type="poem" key="BAN114" modus="sm" lm_max="8" metProfile="8" form="triolet classique" schema="ABaAabAB" er_moy="4.4" er_max="8" er_min="2" er_mode="2(3/5)" er_moy_et="2.94" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
					<head type="main">MONSIEUR HOMAIS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Lisez Voltaire, disait l’un…</quote>
								<bibl>
									<name>GUSTAVE FLAUBERT</name>, <hi rend="ital">Madame Bovary.</hi>
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" rhyme="ABaAabAB">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="1.2">H<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.5" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="A" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.4" punct="pt:8">cr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg><pgtc id="2" weight="8" schema="CVCR">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="B" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="3.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>r<seg phoneme="u" type="vs" value="1" rule="dc-2" place="4">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" punct="vg">e</seg>t</w>, <w n="3.4">c</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="3.6" punct="dp:8">H<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="dp">ai</seg>s</rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="4.2">H<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.5" punct="pt:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="A" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5" punct="pv:7">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="pv">e</seg>s</w> ; <w n="5.6"><pgtc id="3" weight="2" schema="[CR">m<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="6.3">qu</w>’<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg><pgtc id="2" weight="8" schema="CV[CR" part="1">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></pgtc></w> <w n="6.8" punct="pt:8"><pgtc id="2" weight="8" schema="CV[CR" part="2">T<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="7.2">H<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.5" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="3" weight="2" schema="CR" part="1">m<rhyme label="A" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></pgtc></w> !</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.4" punct="pt:8">cr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg><pgtc id="2" weight="8" schema="CVCR" part="1">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="B" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1859">janvier 1859</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>