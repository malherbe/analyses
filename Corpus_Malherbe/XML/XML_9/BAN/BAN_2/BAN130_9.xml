<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN130" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="9(abab)" er_moy="2.0" er_max="10" er_min="0" er_mode="2(8/18)" er_moy_et="2.56" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
				<head type="main">À AUGUSTINE BROHAN</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">Th<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="2" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ds</w> <w n="1.5" punct="vg:8"><pgtc id="1" weight="2" schema="[CR">c<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>x</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.4" punct="vg:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.6" punct="dp:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">qu<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="dp">eu</seg>rs</rhyme></pgtc></w> :</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="4.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="5.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6" punct="pe:8">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pe">o</seg>ts</rhyme></pgtc></w> !</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="6.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="6.4" punct="vg:4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>t</w>, <w n="6.5">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="5">ym</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.6" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="7.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ds</w> <w n="7.7"><pgtc id="3" weight="2" schema="[CR">fl<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.3" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="9.6" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.3">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="10.4" punct="vg:8">fr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="11.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.4" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>ts</w> <w n="12.4" punct="pe:8">bl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="vg:3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="13.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="14.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="14.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.8" punct="vg:8">pr<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>ts</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.6">d</w>’<w n="15.7"><pgtc id="7" weight="0" schema="[R"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">M</w>’<w n="16.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="16.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.8" punct="pe:8">j<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="17.5">j</w>’<w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="17.7">m<pgtc id="9" weight="10" schema="VCVR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">L</w>’<w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>th<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.4">l</w>’<w n="18.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="10" weight="2" schema="CR">n<rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">th<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<pgtc id="9" weight="10" schema="VCVR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>rs</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="20.6" punct="pe:8">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="10" weight="2" schema="CR">n<rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="21.3" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="3" punct="vg">oi</seg></w>, <w n="21.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="21.6"><pgtc id="11" weight="2" schema="[CR">p<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.6" punct="vg:8">r<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:2">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="1">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="23.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="23.3">j</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="23.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="23.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="23.7"><pgtc id="11" weight="2" schema="[CR">p<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="24.6" punct="pt:8">c<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="25.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="25.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="25.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="25.5">c<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.7"><pgtc id="13" weight="3" schema="[CGR">lu<rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></rhyme></pgtc></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">L</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="26.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="26.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="27.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="27.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="13" weight="3" schema="CGR">lu<rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></rhyme></pgtc></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="28.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="28.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="28.6" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="29.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="29.4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="29.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="15" weight="2" schema="CR">v<rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="30.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="30.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<pgtc id="16" weight="7" schema="VCGR"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>li<rhyme label="b" id="16" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="31.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="31.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="31.6"><pgtc id="15" weight="2" schema="[CR">v<rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="32.2">s</w>’<w n="32.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="2">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="32.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="32.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="32.6" punct="pe:8">M<pgtc id="16" weight="7" schema="VCGR"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>li<rhyme label="b" id="16" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="9" type="quatrain" rhyme="abab">
					<l n="33" num="9.1" lm="8" met="8"><w n="33.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="33.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="33.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="33.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="33.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="33.6" punct="vg:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg><pgtc id="17" weight="2" schema="CR">b<rhyme label="a" id="17" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="34" num="9.2" lm="8" met="8"><w n="34.1" punct="vg:2">Th<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="2" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="34.2" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="318" place="3">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="4">u</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="5" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="18" weight="2" schema="CR">l<rhyme label="b" id="18" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="35" num="9.3" lm="8" met="8"><w n="35.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="35.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w> <w n="35.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="35.6" punct="vg:8"><pgtc id="17" weight="2" schema="[CR">b<rhyme label="a" id="17" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="36" num="9.4" lm="8" met="8"><w n="36.1">L</w>’<w n="36.2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="36.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="36.5" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="36.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="36.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.8" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="18" weight="2" schema="CR">l<rhyme label="b" id="18" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1858">septembre 1858</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>