<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN592" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="0.86" er_max="2" er_min="0" er_mode="0(4/7)" er_moy_et="0.99" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">Le Musicien</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="1.5">vi<seg phoneme="e" type="vs" value="1" rule="383" place="5" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.8" punct="pt:12">bl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.5" punct="vg:6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>t</w>,<caesura></caesura> <w n="2.6">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7" mp="M">ei</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="2.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.9"><pgtc id="2" weight="2" schema="[CR">fl<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D</w>’<w n="3.2" punct="pv:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pv">i</seg>l</w> ; <w n="3.3" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="189" place="3" punct="vg">e</seg>t</w>, <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="3.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>ds</w><caesura></caesura> <w n="3.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7">c<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9" punct="vg:12"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" mp="M">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="4.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>fs</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="4.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.9" punct="pt:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="5.3">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.4">j<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="5.9" punct="vg:12">p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">im</seg>pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>t</w>, <w n="6.3" punct="vg:6">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-1" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.6" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.6">ru<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="7.8"><pgtc id="4" weight="2" schema="[CR">pl<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="8.4" punct="vg:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="8.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="8.8">d</w>’<w n="8.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.10" punct="pt:12">br<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="P">a</seg>r</w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.5" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.8" punct="vg:10">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>d</w>, <w n="9.9" punct="vg:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="10.6">fr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="10.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg></rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.5">g<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.8" punct="vg:12">n<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="12.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="12.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rts</w>,<caesura></caesura> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>f</w> <w n="12.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="12.8" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></w>, <w n="12.9">l</w>’<w n="12.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="13.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="6" caesura="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="440" place="8" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="13.7">l</w>’<w n="13.8" punct="vg:12">h<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="14.2" punct="pt:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pt">ai</seg>t</w>. <w n="14.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rti<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="14.10" punct="pt:12">c<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Juin 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>