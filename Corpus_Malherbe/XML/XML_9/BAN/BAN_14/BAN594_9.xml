<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN594" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="1.43" er_max="2" er_min="0" er_mode="2(5/7)" er_moy_et="0.9" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">La Blanchisseuse</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3" punct="vg:5">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="4">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>, <w n="1.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="1.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg><pgtc id="1" weight="2" schema="CR">ss<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">aim</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="2.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="2.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="2.4" punct="vg:6"><seg phoneme="ø" type="vs" value="1" rule="405" place="4" mp="M">Eu</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="2.6">gl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.8" punct="vg:12"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:2">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="2" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="251" place="4">eû</seg>t</w> <w n="3.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>, <w n="3.6">n<seg phoneme="y" type="vs" value="1" rule="457" place="9">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="3.9" punct="vg:12"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="4.8" punct="pt:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="1" weight="2" schema="CR">ss<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12" punct="pt">ein</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:2">R<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="5.6">d</w>’<w n="5.7" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>z<seg phoneme="o" type="vs" value="1" rule="442" place="9" punct="vg">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="5.10"><pgtc id="3" weight="2" schema="[CR">s<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">Ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="2" punct="vg">u</seg></w>, <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="6.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="6.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg></w><caesura></caesura> <w n="6.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="353" place="1" mp="M">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.7">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.9"><pgtc id="4" weight="2" schema="[CR">t<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="8.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rps</w><caesura></caesura> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.9">p<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="8.10" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg><pgtc id="3" weight="2" schema="CR">ss<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="9.2">vi<seg phoneme="e" type="vs" value="1" rule="383" place="2" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="9.3" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="9">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="10.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="10.7">l</w>’<w n="10.8" punct="pv:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="11.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">p<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="11.8">L<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg><pgtc id="6" weight="2" schema="CR">d<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</rhyme></pgtc></w></l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.6">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>rs</w> <w n="12.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="12.11" punct="vg:12">s<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="13.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="13.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="13.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.10">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="13.11"><pgtc id="6" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="13.12"><pgtc id="6" weight="2" schema="[C[R" part="2"><rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1" punct="tc:3">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="ti" mp="F">e</seg>s</w> — <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="14.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="14.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="14.8" punct="pt:12">l<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Juin 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>