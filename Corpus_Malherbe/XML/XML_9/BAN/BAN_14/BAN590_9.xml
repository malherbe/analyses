<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN590" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="3.14" er_max="8" er_min="0" er_mode="2(4/7)" er_moy_et="2.59" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">A Jules Claye</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="1.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="1.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6" caesura="1">om</seg></w><caesura></caesura> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="1.6">t<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>gr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8" punct="pv:12">r<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.4" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="3.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.6">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="9">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c</w> <w n="4.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="4.6">h<seg phoneme="e" type="vs" value="1" rule="169" place="8" mp="M">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="4.9" punct="pt:12">pi<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">M<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="5.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="5.8" punct="pt:12"><pgtc id="3" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>gr<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>f<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="6.5" punct="dp:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="dp" caesura="1">ez</seg></w> :<caesura></caesura> <w n="6.6">J<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="6.7" punct="vg:9">Cl<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" punct="vg">a</seg>y<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">Im</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">N</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.8">j<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.9">h<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">c<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="8.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="M">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ls</w><caesura></caesura> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="8.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="8.9" punct="pt:12">p<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="9.2">h<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.3" punct="vg:6">Ph<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>b<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.5">c<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.6" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg><pgtc id="5" weight="8" schema="CVCR">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>st<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="453" place="6" caesura="1">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.7" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">om</seg><pgtc id="5" weight="8" schema="CVCR">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>st<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="11.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="11.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w> <w n="11.9" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">l<rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:2">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="12.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>, <w n="12.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="12.7" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="7" weight="2" schema="CR">v<rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b</w> <w n="13.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="13.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.10"><pgtc id="6" weight="2" schema="[CR">L<rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.4">d</w>’<w n="14.5">H<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="14.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rt</w> <w n="14.9">d</w>’<w n="14.10" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">E</seg>lz<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="7" weight="2" schema="CR">v<rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1875">Mars 1875.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>