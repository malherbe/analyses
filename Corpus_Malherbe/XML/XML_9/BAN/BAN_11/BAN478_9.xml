<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN478" modus="sm" lm_max="8" metProfile="8" form="petite ballade" schema="3(ababbcbc) bcbc" er_moy="2.29" er_max="6" er_min="2" er_mode="2(13/14)" er_moy_et="1.03" qr_moy="0.36" qr_max="N5" qr_mode="0(13/14)" qr_moy_et="1.29">
					<head type="number">IV</head>
					<head type="main">Ballade en l’honneur de sa Mie</head>
					<lg n="1" type="huitain" rhyme="ababbcbc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="2" schema="CR">nn<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.4" punct="pt:8">F<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.6" punct="vg:8">pl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="5.4">fl<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="b" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="6.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">d</w>’<w n="6.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lch<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="c" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="7.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">f<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.7" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>s<pgtc id="3" weight="2" schema="CR">t<rhyme label="b" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.8" punct="pt:8"><pgtc id="4" weight="2" schema="[CR">m<rhyme label="c" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababbcbc">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="9.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="9.3">s</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="5" weight="2" schema="CR">nn<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>bt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.4" punct="tc:8">p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv ti">in</seg></rhyme></pgtc></w> ; —</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="11.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="11.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.6" punct="pv:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="13.3">s</w>’<w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="3">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="13.6" punct="pv:8">f<pgtc id="7" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="c" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="15.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4" punct="pv:8">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<pgtc id="7" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="16.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.8" punct="pt:8"><pgtc id="8" weight="2" schema="[CR">m<rhyme label="c" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababbcbc">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="17.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="17.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="17.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6">n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="9" weight="2" schema="CR">nn<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="18.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="18.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="10" weight="2" schema="CR">t<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.5" punct="pt:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="9" weight="2" schema="CR">n<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="20.2">qu</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="20.7" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="10" weight="2" schema="CR">t<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4">R<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="11" weight="2" schema="CR">t<rhyme label="b" id="11" gender="m" type="a" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="22.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="12" weight="2" schema="CR">m<rhyme label="c" id="12" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.5" punct="dp:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="11" weight="2" schema="CR">t<rhyme label="b" id="11" gender="m" type="e" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="dp">in</seg>s</rhyme></pgtc></w> :</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="24.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="24.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.8" punct="pt:8"><pgtc id="12" weight="2" schema="[CR">m<rhyme label="c" id="12" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="bcbc">
						<head type="form">Envoi</head>
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>thsch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ld</w> <w n="25.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="25.5" punct="vg:8">b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="13" weight="2" schema="CR">t<rhyme label="b" id="13" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.3" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>str<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="14" weight="2" schema="CR">m<rhyme label="c" id="14" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">m<seg phoneme="œ" type="vs" value="1" rule="151" place="2">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="3">eu</seg>r</w> <w n="27.3">N<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="27.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="27.5" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="13" weight="2" schema="CR">t<rhyme label="b" id="13" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="28.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="28.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="28.8" punct="pt:8"><pgtc id="14" weight="2" schema="[CR">m<rhyme label="c" id="14" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1862">Janvier 1862.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>