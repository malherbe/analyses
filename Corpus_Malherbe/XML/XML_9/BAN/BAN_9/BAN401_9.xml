<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN401" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="7((aa))" er_moy="2.86" er_max="8" er_min="0" er_mode="2(3/7)" er_moy_et="2.8" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">L’Étang Mâlo</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Quand le froid de la mort enveloppe cette <lb></lb>
								argile souffrante, où va l’âme immortelle ?
							</quote>
							<bibl>
								<name>Byron</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="1.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>c</w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="1.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="10">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.11">n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="2.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>t</w> <w n="2.7">br<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.9" punct="vg:12">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">b<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rds</rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.8" punct="vg:12"><pgtc id="2" weight="2" schema="[CR">b<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rds</rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.5">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="5.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg>x</w> <w n="5.8" punct="vg:10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="6.3">c<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="6.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.8" punct="pt:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="7.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="M">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.3">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.5">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="7.8" punct="pv:12"><pgtc id="4" weight="2" schema="[CR">v<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pv">en</seg>t</rhyme></pgtc></w> ;</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2" punct="vg:4">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="2" mp="M">ym</seg>ph<seg phoneme="e" type="vs" value="1" rule="300" place="3" mp="M">ae</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="8.3">l</w>’<w n="8.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6">n<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r</w> <w n="8.7" punct="vg:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="9.3">m<seg phoneme="i" type="vs" value="1" rule="d-4" place="3" mp="M">y</seg><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.7">s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="10.2" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="10.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>s</w> <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.7" punct="pt:12"><pgtc id="5" weight="0" schema="[R" part="1"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.5" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.9" punct="vg:12">s<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>r</w> ! <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>d<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></pgtc></w></l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="13.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="13.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="13.7">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="13.8"><pgtc id="7" weight="8" schema="[C[VCR" part="1">l</pgtc></w>’<w n="13.9" punct="vg:12"><pgtc id="7" weight="8" schema="[C[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>cc<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.5">m<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rt</w><caesura></caesura> <w n="14.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="14.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="14.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.9" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg><pgtc id="7" weight="8" schema="CVCR" part="1">pl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Août 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>