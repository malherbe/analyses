<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN375" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(ababccdede)" er_moy="2.4" er_max="8" er_min="0" er_mode="0(2/5)" er_moy_et="2.94" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
				<lg n="1" type="dizain" rhyme="ababccdede">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="1.6" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.8">l<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="1.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="1.10" punct="pt:12"><pgtc id="1" weight="8" schema="[CVCR">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="2.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg>s</w>,<caesura></caesura> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.6">N<seg phoneme="a" type="vs" value="1" rule="343" place="8" mp="M">a</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="2.8">gr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="3.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>st<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>x</w> <w n="3.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><pgtc id="1" weight="8" schema="CVCR">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="4.8" punct="pt:12">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">l<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="5.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rf</w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="3" weight="2" schema="CR">b<rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Tr<seg phoneme="e" type="vs" value="1" rule="353" place="1" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="6.5" punct="pv:6">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pv" caesura="1">o</seg>r</w> ;<caesura></caesura> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="6.7">n</w>’<w n="6.8"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="6.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="6.10"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="6.11" punct="vg:12"><pgtc id="3" weight="2" schema="[CR">b<rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="7.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="7.5">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.8">tr<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rds</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.5">l<seg phoneme="i" type="vs" value="1" rule="493" place="6" caesura="1">y</seg>s</w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="8.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="8.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="8.10" punct="vg:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg><pgtc id="5" weight="2" schema="CR">p<rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">l</w>’<w n="9.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.6">f<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.9">l<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="9.10">qu</w>’<w n="9.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="9.12" punct="pt:12">c<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="10.6" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.8">l<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="10.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="10.10" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR">p<rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Novembre 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>