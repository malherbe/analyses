<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN380" modus="sp" lm_max="6" metProfile="6, 3" form="suite périodique" schema="6(abaab)" er_moy="0.94" er_max="6" er_min="0" er_mode="0(11/18)" er_moy_et="1.51" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
				<head type="main">La Chanson de ma Mie</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Or, voyez qui je suis, ma mie.</quote>
							<bibl>
								<name>Alfred de Musset</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="quintil" rhyme="abaab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cs</w> <w n="1.7">bl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="2.1" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>r<pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.5" punct="dp:6">ci<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="dp">eu</seg>x</rhyme></pgtc></w> :</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.6"><pgtc id="1" weight="1" schema="GR">y<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="3" met="3"><space quantity="8" unit="char"></space><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3" punct="pt:3"><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quintil" rhyme="abaab">
					<l n="6" num="2.1" lm="6" met="6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="2.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.3" lm="6" met="6"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="8.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="8.5" punct="dp:6">b<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="dp">oi</seg>s</rhyme></pgtc></w> :</l>
					<l n="9" num="2.4" lm="6" met="6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">j</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6">v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</rhyme></pgtc></w></l>
					<l n="10" num="2.5" lm="3" met="3"><space quantity="8" unit="char"></space><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3" punct="pt:3">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quintil" rhyme="abaab">
					<l n="11" num="3.1" lm="6" met="6"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5"><pgtc id="5" weight="2" schema="[CR">fl<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</rhyme></pgtc></w></l>
					<l n="12" num="3.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="12.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
					<l n="13" num="3.3" lm="6" met="6"><w n="13.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="13.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4" punct="dp:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg><pgtc id="5" weight="2" schema="CR">l<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="dp">eu</seg>r</rhyme></pgtc></w> :</l>
					<l n="14" num="3.4" lm="6" met="6"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.6"><pgtc id="5" weight="2" schema="[CR">pl<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</rhyme></pgtc></w></l>
					<l n="15" num="3.5" lm="3" met="3"><space quantity="8" unit="char"></space><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3" punct="pt:3">m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quintil" rhyme="abaab">
					<l n="16" num="4.1" lm="6" met="6"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="16.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="16.5" punct="pt:6">br<pgtc id="7" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					<l n="17" num="4.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="17.2">l</w>’<w n="17.3" punct="dp:3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="18" num="4.3" lm="6" met="6"><w n="18.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4" punct="vg:6">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>pr<pgtc id="7" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="4.4" lm="6" met="6"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="19.4">qu</w>’<w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="19.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></rhyme></pgtc></w></l>
					<l n="20" num="4.5" lm="3" met="3"><space quantity="8" unit="char"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.3" punct="pt:3">m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quintil" rhyme="abaab">
					<l n="21" num="5.1" lm="6" met="6"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="21.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">l<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></rhyme></pgtc></w></l>
					<l n="22" num="5.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="22.1">M<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rt</w> <w n="22.2" punct="dp:3">fl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="23" num="5.3" lm="6" met="6"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="23.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="23.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></rhyme></pgtc></w></l>
					<l n="24" num="5.4" lm="6" met="6"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5"><pgtc id="9" weight="2" schema="[CR">s<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></rhyme></pgtc></w></l>
					<l n="25" num="5.5" lm="3" met="3"><space quantity="8" unit="char"></space><w n="25.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="25.3" punct="pt:3">m<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quintil" rhyme="abaab">
					<l n="26" num="6.1" lm="6" met="6"><w n="26.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="26.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="26.5">t<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</rhyme></pgtc></w></l>
					<l n="27" num="6.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="27.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2" punct="dp:3">f<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="28" num="6.3" lm="6" met="6"><w n="28.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="28.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="28.5" punct="vg:6">j<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="29" num="6.4" lm="6" met="6"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">m</w>’<w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="29.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>s</w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="29.6">l</w>’<w n="29.7"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</rhyme></pgtc></w></l>
					<l n="30" num="6.5" lm="3" met="3"><space quantity="8" unit="char"></space><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="30.3" punct="pt:3">m<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Mars 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>