<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CLIO</head><head type="sub_part">Muse de l’Histoire</head><div type="poem" key="FRT18" modus="cp" lm_max="10" metProfile="2, 4+6" form="rondeau non classique" schema="aabba abaC aababC" er_moy="2.25" er_max="8" er_min="0" er_mode="2(5/8)" er_moy_et="2.33" qr_moy="0.62" qr_max="N5" qr_mode="0(7/8)" qr_moy_et="1.65">
					<head type="main">Mademoiselle de la Vigne</head>
					<head type="sub_1">A propos de sa correspondance galante</head>
					<head type="sub_1">avec Fléchier encore abbé</head>
					<head type="form">RONDEAU</head>
					<lg n="1" rhyme="aabba">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="1.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="1.7" punct="pe:10">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">ll<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pe">e</seg>t</rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="2.4">r<seg phoneme="u" type="vs" value="1" rule="426" place="4" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.6" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp in">i</seg>t</w> : « <w n="2.7">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="2.8" punct="pe:10">v<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pe">e</seg>t</rhyme></pgtc></w> ! »</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="3.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="3.8" punct="dp:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>scl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="4.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="4.6">ru<seg phoneme="i" type="vs" value="1" rule="491" place="7" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="4.8" punct="vg:10">T<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="5.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.8" punct="pt:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">ll<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pt">e</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abaC">
						<l n="6" num="2.1" lm="10" met="4+6"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="6.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="6.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="6.8">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.9" punct="vg:10">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>t</rhyme></pgtc></w>,</l>
						<l n="7" num="2.2" lm="10" met="4+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="7.5" punct="vg:7">br<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="7.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">tt<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.3" lm="10" met="4+6"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">à</seg></w> <w n="8.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" mp="C">e</seg>t</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.7">t<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.9">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="a" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</rhyme></pgtc></w></l>
						<l n="9" num="2.4" lm="2" met="2"><space unit="char" quantity="16"></space><w n="9.1"><pgtc id="7" weight="8" schema="CV[CR" part="1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</pgtc></w> <w n="9.2" punct="pt:2"><pgtc id="7" weight="8" schema="CV[CR" part="2">b<rhyme label="C" id="7" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="aababC">
						<l n="10" num="3.1" lm="10" met="4+6"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg>s</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="10.4" punct="pt:4">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="4" punct="pt" caesura="1">eu</seg>s</w>.<caesura></caesura> <w n="10.5">P<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="10.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="10.8">l</w>’<w n="10.9" punct="vg:10"><seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<pgtc id="4" weight="2" schema="CR" part="1">l<rhyme label="a" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>t</rhyme></pgtc></w>,</l>
						<l n="11" num="3.2" lm="10" met="4+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="11.6">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.8"><pgtc id="5" weight="2" schema="[CR" part="1">l<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</rhyme></pgtc></w></l>
						<l n="12" num="3.3" lm="10" met="4+6"><w n="12.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">â</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="12.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rcr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.7" punct="ps:10">C<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="a" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg>s</rhyme></pgtc></w>…</l>
						<l n="13" num="3.4" lm="10" met="4+6"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>bb<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg>s</w>, <w n="13.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="13.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>squ<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="13.7" punct="pv:10">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR" part="1">l<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pv">e</seg>t</rhyme></pgtc></w> ;</l>
						<l n="14" num="3.5" lm="10" met="4+6"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="14.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="14.7">L<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="e" qr="N5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="15" num="3.6" lm="2" met="2"><space unit="char" quantity="16"></space><w n="15.1"><pgtc id="7" weight="8" schema="CV[CR" part="1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</pgtc></w> <w n="15.2" punct="pe:2"><pgtc id="7" weight="8" schema="CV[CR" part="2">b<rhyme label="C" id="7" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>t</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>