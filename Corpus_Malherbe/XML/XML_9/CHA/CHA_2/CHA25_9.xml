<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA25" modus="cp" lm_max="12" metProfile="8, 6+6, (4+6)" form="strophe unique" schema="1(abbacdcd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0" qr_moy="0.0" qr_max="C0" qr_mode="0(4/4)" qr_moy_et="0.0">
				<head type="number">XV</head>
				<head type="main">Vers écrits sur un souvenir<ref target="1" type="noteAnchor">1</ref></head>
				<head type="sub">Donné par la marquise de Grollier à  M. le baron de Humbolt.</head>
				<opener>
					<dateline>
						<placeName>Paris</placeName>,
						<date when="1818">1818</date>.
					</dateline>
				</opener>
				<lg n="1" type="huitain" rhyme="abbacdcd">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="1.4" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w> <w n="1.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M/mp">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="10" mp="Lp">ez</seg></w>-<w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg>s</w> <w n="1.8">cr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="2.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="2.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="2.8">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="2.10" punct="pi:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>r</rhyme></pgtc></w> ?</l>
					<l n="3" num="1.3" lm="12" met="6+6">« <w n="3.1" punct="vg:3">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mp">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="3.3" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="3.7">Z<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>ph<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6" met_alone="True"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="315" place="3" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="4.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="4.9" punct="pt:10">gl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>. »</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="5.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="5.7">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="5.9" punct="pt:12">pr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>x</rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="6.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="6.5" punct="dp:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></pgtc></w> :</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="7.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="7.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ts</rhyme></pgtc></w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="8.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.9" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<note type="footnote" id="1">Ce Souvenir renfermait des pensées de l’illustre voyageur,<lb></lb>et était orné  de fleurs peintes par Mme de Grollier.</note>
				</closer>
			</div></body></text></TEI>