<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU108" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abab)" er_moy="2.5" er_max="6" er_min="0" er_mode="2(4/8)" er_moy_et="2.18" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="main">MÉDITATION</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								… Ce monde où les meilleures choses <lb></lb>
							Ont le pire destin.
							</quote>
							<bibl>
								<name>Malherbe</name>.
							</bibl>
						</cit>
				</epigraph>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="1.3" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="1.4" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</w> ! <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg>t</w> <w n="1.6" punct="pe:12">r<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="2.2" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>ts</w>, <w n="2.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.5">b<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.7">d</w>’<w n="2.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.6">l<pgtc id="1" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="3.7" punct="vg:12"><pgtc id="1" weight="6" schema="V[CR" part="2">v<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="4.8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="4.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="4.10" punct="pi:12">j<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pi">ou</seg>r</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="pi:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pi ps">oi</seg></w> ?… <w n="5.2">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="Lp">oi</seg>t</w>-<w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.8">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.10">r<pgtc id="3" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>s<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.6">n</w>’<w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="6.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="6.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.10" punct="vg:12"><pgtc id="4" weight="2" schema="[CR" part="1">fl<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.4" punct="vg:6">fr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="7.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="7.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d</w> <w n="7.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>xp<pgtc id="3" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>s<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="8.4">n</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="8.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.8">br<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.9" punct="pi:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="4" weight="2" schema="CR" part="1">l<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pi">eu</seg>rs</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="Lp">oi</seg>t</w>-<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="9.5">qu</w>’<w n="9.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7" punct="vg:6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.11" punct="vg:12">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="11" mp="M">im</seg><pgtc id="5" weight="2" schema="CR" part="1">p<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="10.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rd</w> <w n="10.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="10.9" punct="pv:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="6" weight="2" schema="CR" part="1">t<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd</w> <w n="11.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="11.9">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="8" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.10">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR" part="1">p<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>t</w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.7" punct="pi:12">s<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="6" weight="2" schema="CR" part="1">t<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi">é</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="13.5" punct="dp:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp" caesura="1">i</seg></w> :<caesura></caesura> <w n="13.6">l<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.9" punct="pe:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="14.8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="14.10">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="14.11">d</w>’<w n="14.12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>s<pgtc id="8" weight="2" schema="CR" part="1">t<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ts</rhyme></pgtc></w></l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="15.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="15.6" punct="vg:6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="15.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="15.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="15.10">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="15.11" punct="dp:12">r<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="16.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="16.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="16.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="16.8">c<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="16.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg><pgtc id="8" weight="2" schema="C[R" part="1">t</pgtc></w> <w n="16.10" punct="pt:12"><pgtc id="8" weight="2" schema="C[R" part="2"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>