<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU115" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abab)" er_moy="0.8" er_max="6" er_min="0" er_mode="0(8/10)" er_moy_et="1.83" qr_moy="0.5" qr_max="N5" qr_mode="0(9/10)" qr_moy_et="1.5">
				<head type="main">SERMENT</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							L’on ne seust en nule terre <lb></lb>
							Nul plus bel cors de fame querre.
							</quote>
							<bibl>
								<hi rend="ital">Roman de la Rose.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="1.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.8">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="2.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>gs</w> <w n="2.6">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="2.7" punct="vg:8">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>rs</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ils</w> <w n="3.2" punct="vg:4">j<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg>x</w>, <w n="3.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ts</w> <w n="4.6" punct="pv:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>rs</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="5.6">d</w>’<w n="5.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lb<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="6.5" punct="vg:8">br<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" punct="vg">un</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">l</w>’<w n="7.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="7.6">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.3">s<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.6" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="œ̃" type="vs" value="1" rule="268" place="8" punct="pv">um</seg>s</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="9.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>gl<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.3" punct="vg:4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="10.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>il</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="11.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6">l</w>’<w n="12.7" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="8" punct="pv">a</seg>il</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="13.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>f</w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="13.6">s</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="14.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="14.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="8" weight="2" schema="CR">t<rhyme label="b" id="8" gender="m" type="a" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="15.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.6" punct="vg:8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="16.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="16.7" punct="pv:8"><pgtc id="8" weight="2" schema="[CR">t<rhyme label="b" id="8" gender="m" type="e" qr="N5"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="pv">ein</seg>t</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="17.5">d</w>’<w n="17.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gn<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w>-<w n="18.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>pt</w> <w n="18.8" punct="vg:8"><pgtc id="10" weight="0" schema="[R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg>s</rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">t</w>’<w n="19.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="19.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.6" punct="vg:8">f<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="20.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="20.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.5" punct="tc:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg ti">ou</seg>rs</w>, — <w n="20.6" punct="pe:8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gt<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pe">em</seg>ps</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>