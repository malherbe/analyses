<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU219" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdd cee" er_moy="0.86" er_max="2" er_min="0" er_mode="0(4/7)" er_moy_et="0.99" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">A DEUX BEAUX YEUX</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="1.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="8" mp="M">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.7" punct="pv:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="2.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="2.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c</w> <w n="2.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="2.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.10" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>fl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="3.2" punct="vg:4">pr<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="3.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.6">h<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.7" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="307" place="11" mp="M">a</seg>ill<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="4.2">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="4.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.8" punct="pt:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="10" mp="M">i</seg>ss<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">mm<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="5.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.6">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="5.8" punct="pv:12">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="6.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="6.7">qu</w>’<w n="6.8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.10" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rf<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="7.4">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="7.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="8.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="8.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>f</w> <w n="8.8" punct="pt:12">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ts</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="9.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.8">fl<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.5">s</w>’<w n="10.6"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="10.7">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="10.9" punct="vg:12"><pgtc id="6" weight="2" schema="[CR">b<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rs</w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="11.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="11.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="11.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>rs</w> <w n="11.8" punct="pt:12">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg><pgtc id="6" weight="2" schema="CR">b<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="cee">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ls</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="12.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="12.9">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.10" punct="vg:12"><pgtc id="5" weight="0" schema="[R" part="1"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="13.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="13.4">c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="13.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="7" weight="0" schema="R" part="1"><rhyme label="e" id="7" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l</rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="14.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="14.8" punct="pt:12">cr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>st<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="e" id="7" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>l</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>