<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU190" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abab)" er_moy="1.25" er_max="4" er_min="0" er_mode="0(4/8)" er_moy_et="1.48" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="main">CHOC DE CAVALIERS</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">H<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="1.3">m</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.5" punct="po:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> (<w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">j</w>’<w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="1.10" punct="pf:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w>)</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.7">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="2.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.9">ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>c</w> <w n="2.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.11">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="3" schema="CGR">li<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg>s</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="3.2">cu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.4" punct="vg:6">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="3.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">im</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.8" punct="vg:12">cu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ç<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rn<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="4.5" punct="pt:12">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="11" mp="M">u</seg><pgtc id="2" weight="3" schema="CGR">li<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">dr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ccr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.4">gr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>mm<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>rs</w> <w n="5.7" punct="vg:12">c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">M<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="6.7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="6.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rds</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="7.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="7.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>cl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="7.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ts</w> <w n="7.7" punct="vg:12">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">n<seg phoneme="ø" type="vs" value="1" rule="248" place="3">œu</seg>ds</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="307" place="8" mp="M">a</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>rs</w> <w n="8.8" punct="pt:12">br<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>rds</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="9.2" punct="vg:3">m<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3" punct="vg">en</seg>t</w>, <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rd</w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8" punct="vg:12">g<pgtc id="5" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="10.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.3">bl<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="10.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>t</w> <w n="10.7">d</w>’<w n="10.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<pgtc id="6" weight="1" schema="GR">u<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="353" place="4" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="11.8" punct="vg:12">b<pgtc id="5" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Gu<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="12.3">cr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="Lc">en</seg>tr</w>’<w n="12.5"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M/mc">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>s</w> <w n="12.7" punct="pt:12">l<pgtc id="6" weight="1" schema="GR">u<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="13.3" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="13.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="13.5" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>rs</w>,<caesura></caesura> <w n="13.6">c</w>’<w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="13.8" punct="vg:9">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>s</w>, <w n="13.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.10" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rchi<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="14.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="14.8" punct="vg:12">p<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>t</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rps</w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="15.5">m<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="15.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="15.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>rs</w> <w n="15.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="15.9" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="7" weight="2" schema="CR">ss<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="16.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="16.6" punct="pt:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>f<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>d</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>