<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU213" modus="sm" lm_max="6" metProfile="6" form="suite périodique avec alternance de type 1" schema="3{1(abab) 1(aabccb)}" er_moy="0.13" er_max="2" er_min="0" er_mode="0(14/15)" er_moy_et="0.5" qr_moy="0.0" qr_max="C0" qr_mode="0(15/15)" qr_moy_et="0.0">
				<head type="main">BARCAROLLE</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1" punct="vg:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4" punct="vg:6">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.4" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.5" punct="vg:6"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4" punct="pe:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">er</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.4">d</w>’<w n="5.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4" punct="vg:6">m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="6" met="6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>il</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="7.5" punct="pv:6"><pgtc id="4" weight="2" schema="[CR">f<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st</w> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.6" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="2.5" lm="6" met="6"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.5">d</w>’<w n="9.6" punct="vg:6"><pgtc id="5" weight="0" schema="[R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.6" lm="6" met="6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="10.4" punct="pt:6">s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg><pgtc id="4" weight="2" schema="CR">ph<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="11" num="3.1" lm="6" met="6"><w n="11.1" punct="vg:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4" punct="vg:6">b<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.2" lm="6" met="6"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.4" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
					<l n="13" num="3.3" lm="6" met="6"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.5" punct="vg:6"><pgtc id="6" weight="0" schema="[R"><rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="3.4" lm="6" met="6"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4" punct="pe:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffl<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">er</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="15" num="4.1" lm="6" met="6"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="15.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5" punct="vg:6">B<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lt<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="4.2" lm="6" met="6"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="16.4" punct="vg:6">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>f<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="17" num="4.3" lm="6" met="6"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5" punct="pi:6">J<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi">a</seg></rhyme></pgtc></w> ?</l>
					<l n="18" num="4.4" lm="6" met="6"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="18.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="18.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.5" punct="vg:6">N<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>rw<pgtc id="10" weight="0" schema="R"><rhyme label="c" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="4.5" lm="6" met="6"><w n="19.1">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5" punct="vg:6">n<pgtc id="10" weight="0" schema="R"><rhyme label="c" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="4.6" lm="6" met="6"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="20.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="20.4">d</w>’<w n="20.5" punct="pi:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">An</seg>gs<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>k<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi">a</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="21" num="5.1" lm="6" met="6"><w n="21.1" punct="vg:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.4" punct="vg:6">b<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="22" num="5.2" lm="6" met="6"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="22.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="22.4" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
					<l n="23" num="5.3" lm="6" met="6"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="23.5" punct="vg:6"><pgtc id="11" weight="0" schema="[R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="5.4" lm="6" met="6"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="24.4" punct="pe:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffl<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">er</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="25" num="6.1" lm="6" met="6"><w n="25.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="25.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="25.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="25.5" punct="vg:6">b<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="6.2" lm="6" met="6"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="27" num="6.3" lm="6" met="6"><w n="27.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.5" punct="pt:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
					<l n="28" num="6.4" lm="6" met="6">— <w n="28.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.2" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="28.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="28.4" punct="vg:6">ch<pgtc id="15" weight="0" schema="R"><rhyme label="c" id="15" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="29" num="6.5" lm="6" met="6"><w n="29.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="29.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="29.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>t</w> <w n="29.5">gu<pgtc id="15" weight="0" schema="R"><rhyme label="c" id="15" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="30" num="6.6" lm="6" met="6"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="30.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="30.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="30.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>