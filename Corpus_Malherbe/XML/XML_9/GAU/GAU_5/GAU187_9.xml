<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU187" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="4(abab)" er_moy="2.75" er_max="8" er_min="0" er_mode="2(4/8)" er_moy_et="2.63" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="main">LE TROU DU SERPENT</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="1.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.4" punct="vg:4">m<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>rs</w>,<caesura></caesura> <w n="1.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="1.8"><seg phoneme="i" type="vs" value="1" rule="497" place="9" mp="C">y</seg></w> <w n="1.9" punct="vg:10">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="2.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g</w> <w n="2.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<pgtc id="2" weight="2" schema="CR">d<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3" punct="vg:4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="3.6" punct="vg:10">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>zz<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="4.3">m</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="4.6">l</w>’<w n="4.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="4.9" punct="pt:10">m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">d<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="5.8" punct="vg:10"><pgtc id="3" weight="8" schema="[CVCR">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>s<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="6.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.7" punct="pt:10"><pgtc id="4" weight="6" schema="[VCR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pt">u</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="7.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="vg" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="7.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="7.6">qu<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rts</w> <w n="7.7" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg><pgtc id="3" weight="8" schema="CVCR" part="1">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>s<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="8.2">vi<seg phoneme="e" type="vs" value="1" rule="383" place="3" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>rd</w><caesura></caesura> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.4">n</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="6" mp="M">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="8.7" punct="pt:10">v<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pt">u</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.4" punct="vg:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="9.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.9"><pgtc id="5" weight="2" schema="[C[R" part="1">m</pgtc></w>’<w n="9.10" punct="vg:10"><pgtc id="5" weight="2" schema="[C[R" part="2"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="10.6" punct="pv:10">c<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="pv">o</seg>rps</rhyme></pgtc></w> ;</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9" mp="Lc">oi</seg></w>-<w n="11.9" punct="vg:10"><pgtc id="5" weight="2" schema="[CR" part="1">m<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="12.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rt</w><caesura></caesura> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="12.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="12.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="12.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.10" punct="pt:10">m<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="pt">o</seg>rts</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="13.4">s</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="13.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="13.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>s</w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.9" punct="vg:10"><pgtc id="7" weight="2" schema="[CR" part="1">n<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="14.3">tr<seg phoneme="u" type="vs" value="1" rule="426" place="4" caesura="1">ou</seg></w><caesura></caesura> <w n="14.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="14.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="14.8" punct="vg:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg><pgtc id="8" weight="2" schema="CR" part="1">p<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="Lc">u</seg>squ</w>’<w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="15.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>d</w><caesura></caesura> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="15.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="15.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg><pgtc id="7" weight="2" schema="CR" part="1">nn<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="16.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d</w> <w n="16.6">qu</w>’<w n="16.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="16.8" punct="pt:10">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>r<pgtc id="8" weight="2" schema="CR" part="1">p<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1834">1834</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>