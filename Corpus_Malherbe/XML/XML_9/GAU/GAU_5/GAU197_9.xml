<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU197" modus="cp" lm_max="10" metProfile="6, (4+6)" form="suite périodique" schema="3(ababcddcee)" er_moy="2.27" er_max="6" er_min="0" er_mode="0(6/15)" er_moy_et="2.41" qr_moy="0.0" qr_max="C0" qr_mode="0(15/15)" qr_moy_et="0.0">
				<head type="main">LAMENTO</head>
				<head type="sub_1">LA CHANSON DU PÊCHEUR</head>
				<lg n="1" type="dizain" rhyme="ababcddcee">
					<l n="1" num="1.1" lm="6" met="6"><space quantity="4" unit="char"></space><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.5" punct="dp:6">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="2" num="1.2" lm="6" met="6"><space quantity="4" unit="char"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="2.3" punct="pv:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv">ou</seg>rs</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="6" met="6"><space quantity="4" unit="char"></space><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="6" met="6"><space quantity="4" unit="char"></space><w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="6" met="6"><space quantity="4" unit="char"></space><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3" punct="vg:3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="vg">e</seg>l</w>, <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="5.5">m</w>’<w n="5.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="6" met="6"><space quantity="4" unit="char"></space><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">s</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="6.4" punct="pv:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<pgtc id="4" weight="2" schema="CR">n<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pv">a</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="6" met="6"><space quantity="4" unit="char"></space><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="4">em</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="4" weight="2" schema="CR">n<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="6" met="6"><space quantity="4" unit="char"></space><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="8.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5" punct="pt:6">pr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="6" met="6"><space quantity="4" unit="char"></space><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="9.5" punct="pe:6"><pgtc id="5" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pe">e</seg>r</rhyme></pgtc></w> !</l>
					<l n="10" num="1.10" lm="10" met="4+6" met_alone="True"><w n="10.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="10.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="10.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="10.4">s</w>’<w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="10.8">l<pgtc id="5" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></pgtc></w> <w n="10.9" punct="pe:10"><pgtc id="5" weight="6" schema="V[CR" part="2">m<rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" punct="pe">e</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="dizain" rhyme="ababcddcee">
					<l n="11" num="2.1" lm="6" met="6"><space quantity="4" unit="char"></space><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">cr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="2.2" lm="6" met="6"><space quantity="4" unit="char"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="12.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="12.4" punct="pt:6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="345" place="6" punct="pt">ue</seg>il</rhyme></pgtc></w>.</l>
					<l n="13" num="2.3" lm="6" met="6"><space quantity="4" unit="char"></space><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4">n<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="2.4" lm="6" met="6"><space quantity="4" unit="char"></space><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="14.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>t</w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="14.5" punct="pe:6">d<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="pe">eu</seg>il</rhyme></pgtc></w> !</l>
					<l n="15" num="2.5" lm="6" met="6"><space quantity="4" unit="char"></space><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.3"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><pgtc id="8" weight="0" schema="R" part="1"><rhyme label="c" id="8" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="2.6" lm="6" met="6"><space quantity="4" unit="char"></space><w n="16.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="16.5">l</w>’<w n="16.6" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<pgtc id="9" weight="2" schema="CR" part="1">s<rhyme label="d" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pv">en</seg>t</rhyme></pgtc></w> ;</l>
					<l n="17" num="2.7" lm="6" met="6"><space quantity="4" unit="char"></space><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="17.5"><pgtc id="9" weight="2" schema="CR" part="1">s<rhyme label="d" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</rhyme></pgtc></w></l>
					<l n="18" num="2.8" lm="6" met="6"><space quantity="4" unit="char"></space><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="18.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="e" type="vs" value="1" rule="383" place="5">e</seg>ill<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="c" id="8" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="19" num="2.9" lm="6" met="6"><space quantity="4" unit="char"></space><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="19.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="19.5" punct="pe:6"><pgtc id="10" weight="6" schema="[VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="e" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pe">e</seg>r</rhyme></pgtc></w> !</l>
					<l n="20" num="2.10" lm="10" met="4+6" met_alone="True"><w n="20.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="20.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="20.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="20.4">s</w>’<w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="20.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="20.8">l<pgtc id="10" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></pgtc></w> <w n="20.9" punct="pe:10"><pgtc id="10" weight="6" schema="V[CR" part="2">m<rhyme label="e" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" punct="pe">e</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="dizain" rhyme="ababcddcee">
					<l n="21" num="3.1" lm="6" met="6"><space quantity="4" unit="char"></space><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="21.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="21.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg><pgtc id="11" weight="2" schema="CR" part="1">mm<rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="3.2" lm="6" met="6"><space quantity="4" unit="char"></space><w n="22.1">S</w>’<w n="22.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="22.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="22.5" punct="pv:6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg><pgtc id="12" weight="2" schema="CR" part="1">c<rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pv">eu</seg>l</rhyme></pgtc></w> ;</l>
					<l n="23" num="3.3" lm="6" met="6"><space quantity="4" unit="char"></space><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="23.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="11" weight="2" schema="CR" part="1">m<rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="24" num="3.4" lm="6" met="6"><space quantity="4" unit="char"></space><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="24.5" punct="pt:6"><pgtc id="12" weight="2" schema="[CR" part="1">s<rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pt">eu</seg>l</rhyme></pgtc></w>.</l>
					<l n="25" num="3.5" lm="6" met="6"><space quantity="4" unit="char"></space><w n="25.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="25.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="25.5">b<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="c" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="26" num="3.6" lm="6" met="6"><space quantity="4" unit="char"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">l</w>’<w n="26.5" punct="pe:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg><pgtc id="14" weight="2" schema="CR" part="1">m<rhyme label="d" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="pe">ai</seg>s</rhyme></pgtc></w> !</l>
					<l n="27" num="3.7" lm="6" met="6"><space quantity="4" unit="char"></space><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">n</w>’<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="27.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg><pgtc id="14" weight="2" schema="CR" part="1">m<rhyme label="d" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</rhyme></pgtc></w></l>
					<l n="28" num="3.8" lm="6" met="6"><space quantity="4" unit="char"></space><w n="28.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="28.4">qu</w>’<w n="28.5" punct="pt:6"><pgtc id="13" weight="0" schema="[R" part="1"><rhyme label="c" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="29" num="3.9" lm="6" met="6"><space quantity="4" unit="char"></space><w n="29.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="29.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="29.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="29.5" punct="pe:6"><pgtc id="15" weight="6" schema="[VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<rhyme label="e" id="15" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="pe">e</seg>r</rhyme></pgtc></w> !</l>
					<l n="30" num="3.10" lm="10" met="4+6" met_alone="True"><w n="30.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="30.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="30.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="30.4">s</w>’<w n="30.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="30.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="30.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="30.8">l<pgtc id="15" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></pgtc></w> <w n="30.9" punct="pe:10"><pgtc id="15" weight="6" schema="V[CR" part="2">m<rhyme label="e" id="15" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" punct="pe">e</seg>r</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>