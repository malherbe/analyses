<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU305" modus="cp" lm_max="12" metProfile="4, 6, 7, 3, 6+6" form="suite périodique" schema="1(ababcdcd) 1(aaabcccb) 2(aabccbb)" er_moy="1.12" er_max="6" er_min="0" er_mode="0(9/16)" er_moy_et="1.58" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
				<head type="main">FRAGMENTS</head>
				<head type="sub_1">Intercalés dans l’opéra : <hi rend="ital">Maître Wolfram</hi></head>
				<div type="section" n="1">
					<head type="number">I</head>
					<head type="main">WOLFRAM</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="2.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="2.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.7" punct="vg:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="3.9">m</w>’<w n="3.10"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="1" weight="2" schema="CR">bl<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="4.4" punct="dp:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp" caesura="1">a</seg>s</w> :<caesura></caesura> <w n="4.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="4.6">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>nh<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="5.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="5.8">p<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="6.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="6.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="6.9" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg><pgtc id="4" weight="2" schema="CR">d<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="7.2" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.5">W<seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="M">i</seg>lh<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>lm</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.7">d</w>’<w n="7.8" punct="vg:12">H<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="8.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="8.8" punct="pt:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">d<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<p>(Montrant l’orgue.)</p>
					<lg n="2" type="huitain" rhyme="aaabcccb">
						<l n="9" num="2.1" lm="4" met="4"><space quantity="20" unit="char"></space><w n="9.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.2" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="2.2" lm="6" met="6"><space quantity="12" unit="char"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="10.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="10.5" punct="vg:6">b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="2.3" lm="4" met="4"><space quantity="20" unit="char"></space><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.3" punct="vg:4">g<pgtc id="5" weight="6" schema="VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>n<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="2.4" lm="6" met="6"><space quantity="12" unit="char"></space><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4" punct="pv:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pv">en</seg>ts</rhyme></pgtc></w> ;</l>
						<l n="13" num="2.5" lm="6" met="6"><space quantity="12" unit="char"></space><w n="13.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="13.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="2.6" lm="6" met="6"><space quantity="12" unit="char"></space><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="14.4">s</w>’<w n="14.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="2.7" lm="6" met="6"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="353" place="1">E</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.4"><pgtc id="7" weight="2" schema="[CR">v<rhyme label="c" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="2.8" lm="6" met="6"><space quantity="12" unit="char"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="16.3">cr<seg phoneme="y" type="vs" value="1" rule="454" place="3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ls</w> <w n="16.4" punct="pt:6">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pt">en</seg>ts</rhyme></pgtc></w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<head type="main">HÉLÈNE</head>
					<head type="form">COUPLETS</head>
					<div type="subsection" n="1">
						<head type="number">1</head>
						<lg n="1" type="septain" rhyme="aabccbb">
							<l n="17" num="1.1" lm="7" met="7"><space quantity="10" unit="char"></space><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="17.3"><seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="477" place="4">ï</seg>r</w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.6">b<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</rhyme></pgtc></w></l>
							<l n="18" num="1.2" lm="3" met="3"><space quantity="30" unit="char"></space><w n="18.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2" punct="vg:3">v<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>x</rhyme></pgtc></w>,</l>
							<l n="19" num="1.3" lm="7" met="7"><space quantity="10" unit="char"></space><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="19.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="19.6">l</w>’<w n="19.7" punct="vg:7"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="20" num="1.4" lm="7" met="7"><space quantity="10" unit="char"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="20.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="20.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>cr<pgtc id="10" weight="0" schema="R"><rhyme label="c" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>ts</rhyme></pgtc></w></l>
							<l n="21" num="1.5" lm="3" met="3"><space quantity="30" unit="char"></space><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="21.3" punct="vg:3">fr<pgtc id="10" weight="0" schema="R"><rhyme label="c" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
							<l n="22" num="1.6" lm="7" met="7"><space quantity="10" unit="char"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5" punct="pv:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="23" num="1.7" lm="7" met="7"><space quantity="10" unit="char"></space><w n="23.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="23.2">c</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="23.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="23.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="23.7">s</w>’<w n="23.8" punct="pe:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>v<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div>
					<div type="subsection" n="2">
						<head type="number">2</head>
						<lg n="1" type="septain" rhyme="aabccbb">
							<l n="24" num="1.1" lm="7" met="7"><space quantity="10" unit="char"></space><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3">en</seg>s</w> <w n="24.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>gu<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</rhyme></pgtc></w></l>
							<l n="25" num="1.2" lm="3" met="3"><space quantity="30" unit="char"></space><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3" punct="vg:3">c<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
							<l n="26" num="1.3" lm="7" met="7"><space quantity="10" unit="char"></space><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>s</w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="26.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="26.5" punct="vg:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="27" num="1.4" lm="7" met="7"><space quantity="10" unit="char"></space><w n="27.1">G<seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="27.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="27.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="27.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="27.6">j<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</rhyme></pgtc></w></l>
							<l n="28" num="1.5" lm="3" met="3"><space quantity="30" unit="char"></space><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="28.3" punct="vg:3">t<pgtc id="13" weight="0" schema="R"><rhyme label="c" id="13" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
							<l n="29" num="1.6" lm="7" met="7"><space quantity="10" unit="char"></space><w n="29.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="29.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="4">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="29.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="29.5" punct="vg:7"><pgtc id="14" weight="2" schema="[CR">v<rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="30" num="1.7" lm="7" met="7"><space quantity="10" unit="char"></space><w n="30.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="30.2">c</w>’<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="30.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="30.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="30.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="30.7">s</w>’<w n="30.8" punct="pe:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="14" weight="2" schema="CR">v<rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div>
				</div>
				<closer>
					<dateline>
						<date when="1854">1854</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>