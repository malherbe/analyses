<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU341" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdd cee" er_moy="1.14" er_max="6" er_min="0" er_mode="0(5/7)" er_moy_et="2.1" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">SONNET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="1.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="1.8">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.9" punct="vg:12">bl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.5" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg" caesura="1">ain</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="2.9" punct="pt:12">fr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">l</w>’<w n="3.3" punct="vg:4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="3.4" punct="vg:6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>scr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>ts</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="Lc">u</seg>squ</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="4.4" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="4.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ltr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.7">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="4.9" punct="pt:12">br<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.5" punct="vg:6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="vg" caesura="1">ein</seg></w>,<caesura></caesura> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.8" punct="vg:9">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9" punct="vg">o</seg>l</w>, <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.11" punct="pv:12">h<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.4">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6" punct="vg:8">pu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>s</w>, <w n="6.7">l</w>’<w n="6.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="6.9">d</w>’<w n="6.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12" punct="vg">è</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Pl<seg phoneme="ø" type="vs" value="1" rule="405" place="1" mp="M">eu</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="7.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="7.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>s</w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ttr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>ts</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="8.10">s</w>’<w n="8.11" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="9.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="9.4" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="10.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.4">l<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="10.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.9" punct="dp:12">r<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="11" num="3.3" lm="12" met="6+6">— <w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3" punct="vg">en</seg></w>, <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.5" punct="vg:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.6"><seg phoneme="y" type="vs" value="1" rule="251" place="7">eû</seg>t</w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.9" punct="pt:12">ch<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt ti" mp="F">e</seg></rhyme></pgtc></w>. —</l>
				</lg>
				<lg n="4" rhyme="cee">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="12.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>f</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.4">l</w>’<w n="12.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="12.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="12.7">br<seg phoneme="y" type="vs" value="1" rule="445" place="8" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.9" punct="vg:12">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="13.4">D<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>phn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.6">F<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.8">l<pgtc id="7" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="13.9" punct="vg:12"><pgtc id="7" weight="6" schema="V[CR" part="2">Gr<rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="14.2" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="14.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="14.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="14.7" punct="pt:12">c<pgtc id="7" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>