<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">APPENDICE</head><div type="poem" key="GAU359" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="4(abab) 1(abba)" er_moy="2.7" er_max="6" er_min="0" er_mode="2(4/10)" er_moy_et="2.28" qr_moy="0.0" qr_max="C0" qr_mode="0(10/10)" qr_moy_et="0.0">
					<head type="number">III</head>
					<head type="main">QUATRAINS</head>
					<div type="section" n="1">
						<head type="number">1</head>
						<head type="sub">Improvisé sur un portrait</head>
						<head type="sub">DE M<hi rend="sup">lle</hi> SIONA-LÉVY</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="1.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="1.3" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.7" punct="pv:8"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pv">e</seg>rs</rhyme></pgtc></w> ;</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">l<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="4.5" punct="pt:8"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rts</rhyme></pgtc></w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1851">1851</date>.
							</dateline>
						</closer>
					</div>
					<div type="section" n="2">
						<head type="number">2</head>
						<head type="sub">Improvisé sur un portrait</head>
						<head type="sub">DE M<hi rend="sup">me</hi> MADELEINE BROHAN</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="5" num="1.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="5.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="5.9" punct="vg:12"><pgtc id="3" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="1.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="6.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="6.6">cr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.9" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg><pgtc id="4" weight="6" schema="CVR">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></rhyme></pgtc></w>,</l>
							<l n="7" num="1.3" lm="12" met="6+6"><w n="7.1">Scr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.6">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.8" punct="vg:12">N<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="1.4" lm="12" met="6+6"><w n="8.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="8.3" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="vg" caesura="1">a</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.5">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ll<seg phoneme="wa" type="vs" value="1" rule="424" place="9">oy</seg></w> <w n="8.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.7" punct="pt:12"><pgtc id="4" weight="6" schema="[CVR">P<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1857">1857</date>.
							</dateline>
						</closer>
					</div>
					<div type="section" n="3">
						<head type="number">3</head>
						<head type="sub">Improvisé et placé en tête d’un exemplaire</head>
						<head type="sub">de <hi rend="ital">Émaux et Camées</hi></head>
						<head type="sub">A CLAUDIUS POPELIN, MAÎTRE ÉMAILLEUR</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="9" num="1.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="9.4">j</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="9.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <hi rend="ital"><w n="9.8">C<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></hi></l>
							<l n="10" num="1.2" lm="8" met="8"><w n="10.1">Sc<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="10.3">l</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.6" punct="vg:8"><pgtc id="6" weight="2" schema="[CR">m<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="vg">o</seg>ts</rhyme></pgtc></w>,</l>
							<l n="11" num="1.3" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ccl<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
							<l n="12" num="1.4" lm="8" met="8"><w n="12.1"><seg phoneme="y" type="vs" value="1" rule="251" place="1">Eû</seg>t</w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="391" place="2">eu</seg></w> <w n="12.3">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <hi rend="ital"><w n="12.6" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">É</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pe">au</seg>x</rhyme></pgtc></w></hi> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1863">Août 1863</date>.
							</dateline>
						</closer>
					</div>
					<div type="section" n="4">
						<head type="number">4</head>
						<head type="sub">Improvisé</head>
						<head type="sub">SUR UNE ROBE ROSE A POIS NOIRS</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="13" num="1.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.6">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="14" num="1.2" lm="8" met="8"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="14.8" punct="pv:8">s<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>r</rhyme></pgtc></w> ;</l>
							<l n="15" num="1.3" lm="8" met="8"><w n="15.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2" punct="vg:3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="16" num="1.4" lm="8" met="8"><w n="16.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.6" punct="pe:8">n<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></pgtc></w> !</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">5</head>
						<head type="sub">AU VICOMTE DE S. L.</head>
						<lg n="1" type="quatrain" rhyme="abba">
							<l n="17" num="1.1" lm="12" met="6+6"><w n="17.1">M<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="17.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="17.7">bl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="17.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>c<pgtc id="9" weight="1" schema="GR">i<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12">en</seg></rhyme></pgtc></w></l>
							<l n="18" num="1.2" lm="12" met="6+6"><w n="18.1">P<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t</w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="P">a</seg>r</w> <w n="18.3">V<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="323" place="4">Ey</seg>ck</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="18.6">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="18.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="18.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ts</w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="18.10">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="19" num="1.3" lm="12" met="6+6"><w n="19.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" mp="Lc">e</seg>x</w>-<w n="19.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M/mc">o</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg></w><caesura></caesura> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="19.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="19.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>il</w> <w n="19.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="19.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="19.10" punct="vg:12">br<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="20" num="1.4" lm="12" met="6+6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="20.3">h<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="20.5">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="20.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="20.8" punct="pt:12">s<pgtc id="9" weight="1" schema="GR">i<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pt">en</seg></rhyme></pgtc></w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1872">Octobre 1872</date>.
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>