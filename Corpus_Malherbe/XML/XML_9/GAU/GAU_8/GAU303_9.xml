<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU303" modus="cm" lm_max="10" metProfile="5+5" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="0.86" er_max="5" er_min="0" er_mode="0(5/7)" er_moy_et="1.73" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
				<head type="main">MODES ET CHIFFONS</head>
				<head type="form">SONNET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">P<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="1.7" punct="vg:10">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">V<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>r</w><caesura></caesura> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="2.5">l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg>ï<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>s</w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="4" mp="M">e</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="3.5">l</w>’<w n="3.6" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="4.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="4.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="4.7">d</w>’<w n="4.8" punct="pv:10"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>rt</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.4" punct="vg:5">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg" caesura="1">ai</seg>s</w>,<caesura></caesura> <w n="5.5">f<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.7">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="5.8" punct="vg:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="6.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" caesura="1">an</seg></w><caesura></caesura> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="6.10" punct="vg:10">S<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="7.4">g<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>s</w><caesura></caesura> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="7.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>c<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="8.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="8.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="8.6" punct="pv:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>rt</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="9.3">g<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="9.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>cs</w><caesura></caesura> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="9.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="9.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>z<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="442" place="10">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="10.4" punct="vg:5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="10.5" punct="vg:7">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="10.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>rs</w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="10.8" punct="vg:10">j<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.6" punct="vg:10">R<pgtc id="6" weight="5" schema="VGR"><seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="10" met="5+5"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="493" place="5" caesura="1">y</seg></w><caesura></caesura> <w n="12.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="12.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="12.6" punct="vg:10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="13.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="13.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" caesura="1">in</seg>ts</w><caesura></caesura> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="13.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="13.7" punct="pv:10">m<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="14.6">qu</w>’<w n="14.7" punct="tc:5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" punct="ti" caesura="1">un</seg></w> —<caesura></caesura> <w n="14.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="14.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.10">l</w>’<w n="14.11" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>v<pgtc id="6" weight="5" schema="VGR"><seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1851">1851</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>