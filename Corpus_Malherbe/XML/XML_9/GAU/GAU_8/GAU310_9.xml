<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU310" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="4(abab)" er_moy="2.25" er_max="6" er_min="0" er_mode="2(6/8)" er_moy_et="1.56" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="main">LE PROFIL PERDU</head>
				<head type="sub_1">STANCES SUR UNE AQUARELLE DE LA PRINCESSE M***</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4" punct="vg:4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">aî</seg>t</w>,<caesura></caesura> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="1.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>st<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="315" place="3" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="2.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="2.7" punct="vg:10">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="3.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="4.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="4.5">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8" punct="pe:10">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pe">oi</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">g<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="448" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="6.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" caesura="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.6">qu</w>’<w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="6.8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="6.9" punct="pv:10">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>r<pgtc id="4" weight="2" schema="CR">d<rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pv">u</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.3" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="7.5" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>t</w>, <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg><pgtc id="3" weight="2" schema="CR">g<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="448" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">tr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rs</w><caesura></caesura> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="8.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="Lc">ou</seg>s</w>-<w n="8.7" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M/mc">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M/mc">en</seg><pgtc id="4" weight="2" schema="CR">d<rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pe">u</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="9.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w> <w n="9.3">ch<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="9.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.6">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>qu<pgtc id="5" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></pgtc></w> <w n="9.7" punct="vg:10"><pgtc id="5" weight="6" schema="V[CR" part="2">bl<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">Fl<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>ts</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="10.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="10.7">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.9" punct="pv:10">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg><pgtc id="6" weight="2" schema="CR" part="1">m<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pv">an</seg>ts</rhyme></pgtc></w> ;</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">V<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="11.2" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="11.3" punct="vg:5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg>t</w>, <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="11.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="11.6">d<pgtc id="5" weight="6" schema="V[C[R" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></pgtc></w> <w n="11.7"><pgtc id="5" weight="6" schema="V[C[R" part="2">l</pgtc></w>’<w n="11.8" punct="vg:10"><pgtc id="5" weight="6" schema="V[C[R" part="3"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="d-2" place="3" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="12.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="12.6" punct="pt:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg><pgtc id="6" weight="2" schema="CR" part="1">m<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>ts</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt</w> <w n="13.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="13.5">s</w>’<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="13.8" punct="vg:10">m<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>s<pgtc id="7" weight="2" schema="CR" part="1">t<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">Sph<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="2">yn</seg>x</w> <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" mp="M">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.6" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="8" weight="2" schema="CR" part="1">s<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>t</w><caesura></caesura> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="15.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="15.8" punct="pv:10"><pgtc id="7" weight="2" schema="CR" part="1">t<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="16.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.4" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="16.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="16.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>rri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="16.8" punct="pe:10">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg><pgtc id="8" weight="2" schema="CR" part="1">s<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865">22 mai 1865</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>