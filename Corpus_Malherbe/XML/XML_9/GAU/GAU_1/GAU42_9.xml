<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU42" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)" er_moy="1.0" er_max="4" er_min="0" er_mode="0(8/16)" er_moy_et="1.17" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
				<head type="main">LE MERLE</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6">br<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="vg:5">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>, <w n="2.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="2.5">d</w>’<w n="2.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3" punct="vg:4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6" punct="vg:8">bl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3" punct="vg:5">j<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="4.5">fr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c</w> <w n="4.6" punct="pt:8">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.4" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="5.6" punct="vg:8">cr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.3" punct="vg:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<pgtc id="4" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="vg">e</seg>il</w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L</w>’<w n="8.2">h<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.6" punct="pt:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>vr<pgtc id="4" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="9.3" punct="vg:4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="9.5">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.7" punct="pv:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.3">j<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">Rh<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.6" punct="vg:8">bl<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="11.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.6" punct="vg:8">p<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="12.4">h<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="12.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="12.7" punct="pt:8">f<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ts</w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="13.7">l</w>’<w n="13.8" punct="vg:8">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="7" weight="2" schema="CR">m<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>str<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ts</w> <w n="14.4" punct="pv:8">si<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="2" schema="CR">ge<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="15.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="15.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>b<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="354" place="6">e</seg>x<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="7" weight="2" schema="CR">m<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="16.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="16.3">d</w>’<w n="16.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="16.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="8" weight="2" schema="CR">ge<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>str<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="17.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<pgtc id="9" weight="1" schema="GR">u<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">L</w>’<w n="18.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="18.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="18.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.6" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>s<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="19.2" punct="vg:4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="19.3">br<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="19.5" punct="vg:8">pl<pgtc id="9" weight="1" schema="GR">u<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="20.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="20.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.6" punct="pt:8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="21.2">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ss<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="22.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="22.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="22.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="12" weight="2" schema="CR">gt<rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</rhyme></pgtc></w></l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="23.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="23.5" punct="vg:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="pt:8">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg><pgtc id="12" weight="2" schema="CR">t<rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pt">em</seg>ps</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="25.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="25.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="25.6">l</w>’<w n="25.7" punct="pv:8"><pgtc id="13" weight="0" schema="[R"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="26.3" punct="vg:4">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="26.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>t</w> <w n="26.7" punct="vg:8">l<pgtc id="14" weight="1" schema="GR">i<rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">L</w>’<w n="27.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="27.3" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rt</w>, <w n="27.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="27.6">n<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>f</w> <w n="27.7" punct="vg:8">s<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="28.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.3">f<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="28.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="28.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="28.6" punct="pt:8">D<pgtc id="14" weight="1" schema="GR">i<rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="quatrain" rhyme="abab">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="29.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="29.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="29.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="15" weight="2" schema="CR">f<rhyme label="a" id="15" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="30" num="8.2" lm="8" met="8"><w n="30.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="30.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="30.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>ct</w> <w n="30.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ss<seg phoneme="?" type="va" value="1" rule="162" place="6">en</seg>t</w> <w n="30.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="30.6" punct="pt:8">l<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="31.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="31.5" punct="vg:8">ph<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg><pgtc id="15" weight="2" schema="CR">ph<rhyme label="a" id="15" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="32.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="32.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="32.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="32.7" punct="pe:8">t<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>