<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">CHANSON GRAVE</head><div type="poem" key="DAN5" modus="cm" lm_max="12" metProfile="6+6" form="sonnet peu classique" schema="abab abba ccd eed" er_moy="2.0" er_max="6" er_min="0" er_mode="0(4/7)" er_moy_et="2.62" qr_moy="0.0" qr_max="C0" qr_mode="0(7/7)" qr_moy_et="0.0">
						<head type="main">Soleil d’hiver</head>
						<lg n="1" rhyme="abab">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3" punct="dp:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp">i</seg></w> : <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="1.7">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t</rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="426" place="6" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">â</seg>t<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="11" mp="M">a</seg>y<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="3.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="3.4">tr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="3.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="1" weight="2" schema="CR">rr<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rts</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>f</w> <w n="4.7" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" mp="M">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274" place="11" mp="M">ui</seg>ll<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="5.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="5.8">fr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<pgtc id="3" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</rhyme></pgtc></w></l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="6.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="6.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="6.4">gl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l</w> <w n="6.8">s</w>’<w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="7.3" punct="vg:3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3" punct="vg">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="7.6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="7.7" punct="vg:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6" caesura="1">ô</seg>t</w><caesura></caesura> <w n="8.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" mp="P">e</seg>rs</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="10" mp="M">ou</seg><pgtc id="3" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>ss<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.10">gl<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="10.2" punct="vg:2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="10.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lti<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="10.8" punct="vg:12">f<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2" punct="vg:3">pr<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="11.3">s</w>’<w n="11.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5" mp="M">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rps</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="11.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rps</w> <w n="11.8" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>l<pgtc id="6" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="d" id="6" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg>s</rhyme></pgtc></w> ;</l>
						</lg>
						<lg n="4" rhyme="eed">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="12.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="12.8">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>d<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">fl<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.5">cr<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">oû</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="9">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.9" punct="vg:12">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="14.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.7" punct="dp:9">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="dp in" mp="F">e</seg></w> : « <w n="14.8">C</w>’<w n="14.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="14.10" punct="pt:12"><pgtc id="6" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="d" id="6" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">ez</seg></rhyme></pgtc></w> ».</l>
						</lg>
					</div></body></text></TEI>