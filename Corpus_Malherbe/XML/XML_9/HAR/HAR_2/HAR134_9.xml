<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR134" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)" er_moy="1.56" er_max="3" er_min="0" er_mode="2(11/16)" er_moy_et="0.93" qr_moy="0.31" qr_max="N5" qr_mode="0(15/16)" qr_moy_et="1.21">
						<head type="main">IVRE</head>
						<opener>
							<salute>À AMÉDÉE PATTE</salute>
						</opener>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="1.5">j</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="1.7">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.7" punct="pt:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="3.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>f</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5" punct="dp:5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="dp">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> : <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>ls</w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="3.9" punct="vg:8">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="4.5">t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="4.6" punct="pt:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg><pgtc id="2" weight="2" schema="CR">pl<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ts</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.7" punct="vg:8"><pgtc id="4" weight="2" schema="[CR">s<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="7.3" punct="pe:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="pe">in</seg></w> ! <w n="7.4" punct="pe:6">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe">ez</seg></w> ! <w n="7.5" punct="pe:8">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.2">m</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.6" punct="pt:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.4" punct="pe:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pe">ai</seg>t</w> ! <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">E</seg>t</w> <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7" punct="vg:8">b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="5" weight="2" schema="CR">v<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="10.4" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="10.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="10.7" punct="ps:8"><pgtc id="6" weight="0" schema="[R"><rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="11.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7" punct="dp:8"><pgtc id="5" weight="2" schema="[CR">v<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="dp">ai</seg>s</rhyme></pgtc></w> :</l>
							<l n="12" num="3.4" lm="8" met="8"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="12.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7" punct="pt:8">v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abab">
							<l n="13" num="4.1" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="13.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="13.6">f<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="13.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="13.8" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="7" weight="2" schema="CR">m<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rds</rhyme></pgtc></w>,</l>
							<l n="14" num="4.2" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="14.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="14.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="14.7" punct="pv:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="8" weight="2" schema="CR">s<rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="15.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="15.5"><pgtc id="7" weight="2" schema="[CR">m<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rts</rhyme></pgtc></w></l>
							<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.7" punct="pt:8"><pgtc id="8" weight="2" schema="CR">s<rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abab">
							<l n="17" num="5.1" lm="8" met="8"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="17.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="17.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt</w> <w n="17.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.7" punct="vg:8"><pgtc id="9" weight="2" schema="[CR">b<rhyme label="a" id="9" gender="m" type="a" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
							<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">j</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="18.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="18.5">cr<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="18.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="18.7">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="19" num="5.3" lm="8" met="8"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="19.3" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>l</w>, <w n="19.4">j</w>’<w n="19.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="19.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR">b<rhyme label="a" id="9" gender="m" type="e" qr="N5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</rhyme></pgtc></w></l>
							<l n="20" num="5.4" lm="8" met="8"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="20.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6">j</w>’<w n="20.7" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="6" type="quatrain" rhyme="abab">
							<l n="21" num="6.1" lm="8" met="8"><w n="21.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.3" punct="vg:4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4" punct="vg">e</seg>ds</w>, <w n="21.4">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.6" punct="vg:8"><pgtc id="11" weight="2" schema="[CR">r<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>s</rhyme></pgtc></w>,</l>
							<l n="22" num="6.2" lm="8" met="8"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="22.3">n<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="22.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.6" punct="vg:8"><pgtc id="12" weight="2" schema="[CR">r<rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="23" num="6.3" lm="8" met="8"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="23.3">n<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="23.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="23.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="23.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="11" weight="2" schema="CR">gr<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg>s</rhyme></pgtc></w>,</l>
							<l n="24" num="6.4" lm="8" met="8"><w n="24.1">J</w>’<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="24.4" punct="vg:4">b<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>t</w>, <w n="24.5">j</w>’<w n="24.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="24.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="24.8" punct="pt:8"><pgtc id="12" weight="2" schema="[CR">tr<rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="7" type="quatrain" rhyme="abab">
							<l n="25" num="7.1" lm="8" met="8"><w n="25.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rs</w> <w n="25.4" punct="vg:5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="25.7" punct="vg:8">fr<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>t</rhyme></pgtc></w>,</l>
							<l n="26" num="7.2" lm="8" met="8"><w n="26.1">J</w>’<w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="26.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>squ</w>’<w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="26.5">l</w>’<w n="26.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.7" punct="dp:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="14" weight="3" schema="CGR">rni<rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
							<l n="27" num="7.3" lm="8" met="8"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="27.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w> <w n="27.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ds</w> <w n="27.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</rhyme></pgtc></w></l>
							<l n="28" num="7.4" lm="8" met="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="28.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="28.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="28.6">l</w>’<w n="28.7" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg><pgtc id="14" weight="3" schema="CGR">rni<rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="8" type="quatrain" rhyme="abab">
							<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">j</w>’<w n="29.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="29.4" punct="vg:5">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>, <w n="29.5" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="29.6" punct="vg:8">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="15" weight="2" schema="CR">b<rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></pgtc></w>,</l>
							<l n="30" num="8.2" lm="8" met="8"><w n="30.1">L<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rd</w> <w n="30.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="30.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="30.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="30.5">l</w>’<w n="30.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="30.7" punct="ps:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="16" weight="2" schema="CR">bl<rhyme label="b" id="16" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
							<l n="31" num="8.3" lm="8" met="8">— <w n="31.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="31.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="31.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="31.5">j</w>’<w n="31.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="31.7"><pgtc id="15" weight="2" schema="CR">b<rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></pgtc></w></l>
							<l n="32" num="8.4" lm="8" met="8"><w n="32.1">M</w>’<w n="32.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="32.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="32.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="32.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="32.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="32.7" punct="pe:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="16" weight="2" schema="CR">l<rhyme label="b" id="16" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div></body></text></TEI>