<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR156" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="7(abba)" er_moy="1.0" er_max="6" er_min="0" er_mode="0(9/14)" er_moy_et="1.65" qr_moy="0.0" qr_max="C0" qr_mode="0(14/14)" qr_moy_et="0.0">
						<head type="main">RÉSIPISCENCE</head>
						<opener>
							<salute>À MADAME M. GODEBSKA</salute>
						</opener>
						<lg n="1" type="quatrain" rhyme="abba">
							<l n="1" num="1.1" lm="7" met="7"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2" punct="vg:2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" punct="vg">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="1.4" punct="vg:5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.6" punct="pi:7">pl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
							<l n="2" num="1.2" lm="7" met="7"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3"><seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="2.6">t</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="2.8" punct="pt:7">ch<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="7" met="7"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.5" punct="vg:7">p<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="7" met="7"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2" punct="vg:3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5" punct="ps:7">h<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg>s</rhyme></pgtc></w>…</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abba">
							<l n="5" num="2.1" lm="7" met="7"><w n="5.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="5.2" punct="pe:3">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe">é</seg></w> ! <w n="5.3">J</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="5.6">l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg><pgtc id="3" weight="2" schema="CR">tt<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></pgtc></w></l>
							<l n="6" num="2.2" lm="7" met="7"><w n="6.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg><pgtc id="4" weight="0" schema="[R" part="1">tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w> <w n="6.7" punct="vg:7"><pgtc id="4" weight="0" schema="[R" part="2"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="7" num="2.3" lm="7" met="7"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="7.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="7.4" punct="vg:4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.7" punct="vg:7"><pgtc id="4" weight="0" schema="[R" part="1">r<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="2.4" lm="7" met="7"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="8.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="3">ue</seg>il</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4" punct="pt:7">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>l<pgtc id="3" weight="2" schema="CR" part="1">t<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abba">
							<l n="9" num="3.1" lm="7" met="7">— <w n="9.1">J</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.5" punct="pv:7">h<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
							<l n="10" num="3.2" lm="7" met="7"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="10.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.6" punct="vg:7">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
							<l n="11" num="3.3" lm="7" met="7"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="11.7" punct="vg:7">h<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
							<l n="12" num="3.4" lm="7" met="7"><w n="12.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="12.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="12.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="12.6" punct="pt:7">p<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abba">
							<l n="13" num="4.1" lm="7" met="7"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">j</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.7"><pgtc id="7" weight="2" schema="[CR" part="1">ch<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</rhyme></pgtc></w></l>
							<l n="14" num="4.2" lm="7" met="7"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="14.6" punct="pv:7"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg><pgtc id="8" weight="2" schema="CR" part="1">mm<rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="15" num="4.3" lm="7" met="7"><w n="15.1">J</w>’<w n="15.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="15.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6" punct="vg:7"><pgtc id="8" weight="2" schema="[CR" part="1">m<rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="16" num="4.4" lm="7" met="7"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="16.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="16.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>t</w> <w n="16.6">m</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.8" punct="pt:7"><pgtc id="7" weight="2" schema="[CR" part="1">ch<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abba">
							<l n="17" num="5.1" lm="7" met="7"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="17.6">f<seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="17.7">m<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>st<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
							<l n="18" num="5.2" lm="7" met="7"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="18.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="18.6" punct="dp:7">p<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="dp">u</seg>r</rhyme></pgtc></w> :</l>
							<l n="19" num="5.3" lm="7" met="7"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="19.3" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>l</w>, <w n="19.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="19.6">m<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</rhyme></pgtc></w></l>
							<l n="20" num="5.4" lm="7" met="7"><w n="20.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="20.4" punct="pt:7">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="6" type="quatrain" rhyme="abba">
							<l n="21" num="6.1" lm="7" met="7"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="21.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="21.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="21.7"><pgtc id="11" weight="2" schema="[CR" part="1">c<rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</rhyme></pgtc></w></l>
							<l n="22" num="6.2" lm="7" met="7"><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="22.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="22.4">j</w>’<w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="22.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="22.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w>-<w n="22.9" punct="ps:7">m<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg></rhyme></pgtc></w>…</l>
							<l n="23" num="6.3" lm="7" met="7"><w n="23.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="23.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="23.4" punct="vg:7">bl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>sph<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="24" num="6.4" lm="7" met="7"><w n="24.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="24.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4" punct="pe:7">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg><pgtc id="11" weight="2" schema="CR" part="1">qu<rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="7" type="quatrain" rhyme="abba">
							<l n="25" num="7.1" lm="7" met="7"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="25.2">j</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="25.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="25.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.7">h<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
							<l n="26" num="7.2" lm="7" met="7"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="26.5" punct="dp:7">p<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="dp">eu</seg></rhyme></pgtc></w> :</l>
							<l n="27" num="7.3" lm="7" met="7"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="27.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="27.5">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="27.7" punct="pt:7">Di<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
							<l n="28" num="7.4" lm="7" met="7">— <w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="28.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.5" punct="pe:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div></body></text></TEI>