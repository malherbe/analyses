<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES208" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="2(abba) 3(abab)" er_moy="2.0" er_max="8" er_min="0" er_mode="2(6/10)" er_moy_et="2.19" qr_moy="0.0" qr_max="C0" qr_mode="0(10/10)" qr_moy_et="0.0">
					<head type="main">L’ATTENTE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="1.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="1.9">m</w>’<w n="1.10" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.11"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.12">l</w>’<w n="1.13">h<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="2.6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>ds</w><caesura></caesura> <w n="2.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="2.9" punct="pv:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>r<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="3.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>r</w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.5" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="3.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.10" punct="pv:12">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">tt<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">en</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8" punct="vg:9">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.11" punct="pt:12">pl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="5.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>t</w> <w n="5.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="5.7" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg><pgtc id="3" weight="8" schema="CVCR">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2" punct="vg:4">tr<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="6.3">j</w>’<w n="6.4" punct="ps:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="ps" caesura="1">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>…<caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">j</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.8" punct="pv:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="4" weight="2" schema="CR">b<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="7.9">r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="7.10" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">b<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="8.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="8.5" punct="dp:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="dp in" caesura="1">on</seg>d</w> :<caesura></caesura> « <w n="8.6" punct="pe:7">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pe">eu</seg></w> ! <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="Fm">e</seg>s</w>-<w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="10">e</seg></w> <w n="8.9" punct="pe:12"><pgtc id="3" weight="8" schema="[CVCR">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>r</rhyme></pgtc></w> ! »</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="9.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ts</w> <w n="9.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="9.6">j</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ts</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>ts</w><caesura></caesura> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.10" punct="pv:12">b<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>nh<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="11.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>d</w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.6" punct="pv:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pv" caesura="1">eu</seg>x</w> ;<caesura></caesura> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="11.10" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>ppr<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3" punct="vg:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3" punct="vg">om</seg></w>, <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="12.6" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>p</w>,<caesura></caesura> <w n="12.7">s</w>’<w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="12.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="12.11" punct="pt:12">c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pt">œu</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">c</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="13.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" mp="Lc">oi</seg></w>-<w n="13.5" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" punct="vg">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.6" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pe" caesura="1">in</seg></w> !<caesura></caesura> <w n="13.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="13.8">j</w>’<w n="13.9"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="13.10">c<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="13.11">d</w>’<w n="13.12" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="7" weight="2" schema="CR">tt<rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:3">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="14.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.9" punct="vg:12">br<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="15.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.7">j</w>’<w n="15.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="15.9">p<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="15.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="15.11">t</w>’<w n="15.12" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="16.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="16.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="6" punct="vg" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.7">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="16.8">s<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l</w> <w n="16.9">l</w>’<w n="16.10" punct="pe:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>bti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="11" mp="M">en</seg>dr<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w>-<w n="17.2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="17.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="17.4">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="17.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="17.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="17.8">v<seg phoneme="ø" type="vs" value="1" rule="248" place="9">œu</seg>x</w> <w n="17.9" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>r<pgtc id="9" weight="2" schema="CR">d<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1" mp="Lp">E</seg>s</w>-<w n="18.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="18.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="18.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="18.8">t<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="18.9" punct="pi:12"><pgtc id="10" weight="2" schema="[CR">p<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg>s</rhyme></pgtc></w> ?</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="19.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="19.5" punct="pt:6">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt" caesura="1">i</seg>r</w>.<caesura></caesura> <w n="19.6" punct="vg:7">M<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="19.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="19.8" punct="pe:9">s<seg phoneme="œ" type="vs" value="1" rule="249" place="9" punct="pe">œu</seg>r</w> ! <w n="19.9">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.10" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="9" weight="2" schema="CR">d<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="20.3" punct="pe:3">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="pe ps">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !… <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="20.5" punct="pe:6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe" caesura="1">eu</seg>r</w> !<caesura></caesura> <w n="20.6">D<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="20.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="20.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="20.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="20.10">l</w>’<w n="20.11"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="11">e</seg>s</w> <w n="20.12" punct="pe:12"><pgtc id="10" weight="2" schema="[CR">p<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>