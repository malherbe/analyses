<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES100" modus="sp" lm_max="8" metProfile="8, 6" form="suite périodique" schema="4(ababcddc)" er_moy="1.56" er_max="7" er_min="0" er_mode="0(8/16)" er_moy_et="2.09" qr_moy="0.0" qr_max="C0" qr_mode="0(16/16)" qr_moy_et="0.0">
						<head type="main">LA PÈLERINE</head>
						<lg n="1" type="huitain" rhyme="ababcddc">
							<l n="1" num="1.1" lm="8" met="8">« <w n="1.1" punct="vg:3">P<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="1.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w>-<w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="1.6" punct="pi:8">t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>rd</rhyme></pgtc></w> ?</l>
							<l n="2" num="1.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.5">l</w>’<w n="2.6" punct="pt:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w>-<w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="3.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="4.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.5" punct="pi:6"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
							<l n="5" num="1.5" lm="8" met="8">— <w n="5.1" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="5.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="vg:8">p<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s</w> <w n="6.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.8" punct="dp:8">f<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w>-<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="7.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.7">p<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="8.4">br<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.6" punct="pe:8">c<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pe">œu</seg>r</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="2" type="huitain" rhyme="ababcddc">
							<l n="9" num="2.1" lm="8" met="8">— <w n="9.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3" punct="pe:4">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! <w n="9.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.6">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</rhyme></pgtc></w></l>
							<l n="10" num="2.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="10.5">t</w>’<w n="10.6" punct="pv:6"><pgtc id="6" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="11" num="2.3" lm="8" met="8"><w n="11.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></pgtc></w></l>
							<l n="12" num="2.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4" punct="pt:6">ch<pgtc id="6" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="13" num="2.5" lm="8" met="8">— <w n="13.1" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>s<pgtc id="7" weight="7" schema="[V[CGR" part="1">t</pgtc></w> <w n="13.5"><pgtc id="7" weight="7" schema="[V[CGR" part="2"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></pgtc></w> <w n="13.6" punct="pv:8"><pgtc id="7" weight="7" schema="[V[CGR" part="3">Di<rhyme label="c" id="7" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg></rhyme></pgtc></w> ;</l>
							<l n="14" num="2.6" lm="8" met="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4" punct="vg:5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg>t</w>, <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="14.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.7" punct="pv:8">gu<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="d" id="8" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="15" num="2.7" lm="8" met="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="15.2">m</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.6">fu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>r</w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="15.8" punct="dp:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rf<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="d" id="8" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
							<l n="16" num="2.8" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="16.3">l</w>’<w n="16.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="16.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">E</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg><pgtc id="7" weight="7" schema="[VCGR" part="1">t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w>, <w n="16.6" punct="pt:8"><pgtc id="7" weight="7" schema="[VCGR" part="2"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>di<rhyme label="c" id="7" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" type="huitain" rhyme="ababcddc">
							<l n="17" num="3.1" lm="8" met="8">— <w n="17.1" punct="vg:3">P<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3" punct="vg">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="17.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="17.4">l</w>’<w n="17.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">A</seg>m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
							<l n="18" num="3.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="18.4">t</w>’<w n="18.5" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg><pgtc id="10" weight="2" schema="CR" part="1">ch<rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="19" num="3.3" lm="8" met="8"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="19.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="19.5" punct="vg:8">j<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
							<l n="20" num="3.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="20.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="20.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.5" punct="pt:6"><pgtc id="10" weight="2" schema="[CR" part="1">ch<rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="21" num="3.5" lm="8" met="8">— <w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="21.2" punct="pe:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe">eu</seg>x</w> ! <w n="21.3">c</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="21.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="11" weight="2" schema="CR" part="1">m<rhyme label="c" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
							<l n="22" num="3.6" lm="8" met="8"><w n="22.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2" punct="vg:3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="22.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w>-<w n="22.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="22.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="22.6" punct="pe:8">l<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="d" id="12" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
							<l n="23" num="3.7" lm="8" met="8"><w n="23.1">Bl<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="23.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="23.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="23.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="23.6"><pgtc id="12" weight="0" schema="[R" part="1"><rhyme label="d" id="12" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
							<l n="24" num="3.8" lm="8" met="8"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="24.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="24.5" punct="pt:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="11" weight="2" schema="CR" part="1">m<rhyme label="c" id="11" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="4" type="huitain" rhyme="ababcddc">
							<l n="25" num="4.1" lm="8" met="8">— <w n="25.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2" punct="vg:3">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="25.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="25.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>s</w> <w n="25.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="25.7" punct="vg:8">y<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
							<l n="26" num="4.2" lm="6" met="6"><space quantity="2" unit="char"></space><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="26.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="26.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">l</w>’<w n="26.5" punct="pv:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg><pgtc id="14" weight="2" schema="CR" part="1">rd<rhyme label="b" id="14" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="27" num="4.3" lm="8" met="8"><w n="27.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="27.3">n</w>’<w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="27.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="27.6" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
							<l n="28" num="4.4" lm="6" met="6"><space quantity="2" unit="char"></space><w n="28.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="28.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="28.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="28.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="28.5" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg><pgtc id="14" weight="2" schema="CR" part="1">rd<rhyme label="b" id="14" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>. »</l>
							<l n="29" num="4.5" lm="8" met="8"><w n="29.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="29.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.5" punct="pv:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="15" weight="2" schema="CR" part="1">tr<rhyme label="c" id="15" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg></rhyme></pgtc></w> ;</l>
							<l n="30" num="4.6" lm="8" met="8"><w n="30.1">L</w>’<w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="30.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="30.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="30.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.6" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="16" weight="2" schema="CR" part="1">j<rhyme label="d" id="16" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="31" num="4.7" lm="8" met="8"><w n="31.1">L</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ç<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="31.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg><pgtc id="16" weight="2" schema="CR" part="1">j<rhyme label="d" id="16" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="32" num="4.8" lm="8" met="8"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.3" punct="ps:6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="ps">e</seg></w>… <w n="32.4" punct="pt:8">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg><pgtc id="15" weight="2" schema="CR" part="1">r<rhyme label="c" id="15" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>