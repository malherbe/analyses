<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES198" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="8(abab)" er_moy="0.56" er_max="2" er_min="0" er_mode="0(11/16)" er_moy_et="0.86" qr_moy="0.31" qr_max="N5" qr_mode="0(15/16)" qr_moy_et="1.21">
						<head type="main">PÈLERINAGE</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="6" met="6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="1.4">G<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>xp<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>rs</rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="6" met="6"><w n="3.1" punct="vg:2">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2" punct="vg">ain</seg></w>, <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.4" punct="vg:6">c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="6" met="6"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="4.5" punct="pt:6">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="5" num="2.1" lm="6" met="6"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.3" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="6" met="6"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="6.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.4" punct="pv:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pv">eau</seg></rhyme></pgtc></w> ;</l>
							<l n="7" num="2.3" lm="6" met="6"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="8" num="2.4" lm="6" met="6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.4" punct="pt:6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="9" num="3.1" lm="6" met="6"><w n="9.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3" punct="vg:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="10" num="3.2" lm="6" met="6"><w n="10.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">n<seg phoneme="ø" type="vs" value="1" rule="248" place="2">œu</seg>ds</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="10.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="10.5" punct="pv:6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pv">an</seg>ts</rhyme></pgtc></w> ;</l>
							<l n="11" num="3.3" lm="6" met="6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5" punct="vg:6">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="12" num="3.4" lm="6" met="6"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.3" punct="pt:6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pt">en</seg>ts</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abab">
							<l n="13" num="4.1" lm="6" met="6"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">j<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="14" num="4.2" lm="6" met="6"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.4" punct="pv:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pv">o</seg>rd</rhyme></pgtc></w> ;</l>
							<l n="15" num="4.3" lm="6" met="6"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.5" punct="vg:6">v<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="16" num="4.4" lm="6" met="6"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="16.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.6" punct="pt:6"><pgtc id="8" weight="2" schema="[CR">m<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pt">o</seg>rt</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abab">
							<l n="17" num="5.1" lm="6" met="6"><w n="17.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.3">m</w>’<w n="17.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="18" num="5.2" lm="6" met="6"><w n="18.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
							<l n="19" num="5.3" lm="6" met="6"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">m</w>’<w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="19.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="19.6">d</w>’<w n="19.7"><pgtc id="9" weight="0" schema="[R"><rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="20" num="5.4" lm="6" met="6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.3" punct="pt:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="6" type="quatrain" rhyme="abab">
							<l n="21" num="6.1" lm="6" met="6"><w n="21.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="21.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="21.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="21.4" punct="vg:4">n<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</w>, <w n="21.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="21.6" punct="vg:6">d<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="22" num="6.2" lm="6" met="6"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="22.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.4" punct="pv:6">d<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pv">u</seg>r</rhyme></pgtc></w> ;</l>
							<l n="23" num="6.3" lm="6" met="6"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="23.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="23.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.6">r<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="24" num="6.4" lm="6" met="6"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">d</w>’<w n="24.4" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>rth<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg>r</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="7" type="quatrain" rhyme="abab">
							<l n="25" num="7.1" lm="6" met="6"><w n="25.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="25.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="25.3" punct="dp:4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="dp">a</seg></w> : <w n="25.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="13" weight="2" schema="CR">n<rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="26" num="7.2" lm="6" met="6"><w n="26.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w>-<w n="26.2">t</w>-<w n="26.3" punct="dp:3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp in">i</seg>l</w> : « <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="26.6" punct="pe:6">fr<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a" qr="N5"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pe">oi</seg>d</rhyme></pgtc></w> ! »</l>
							<l n="27" num="7.3" lm="6" met="6"><w n="27.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="27.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="27.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg><pgtc id="13" weight="2" schema="CR">nn<rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="28" num="7.4" lm="6" met="6"><w n="28.1">Pr<seg phoneme="i" type="vs" value="1" rule="482" place="1">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w>-<w n="28.2">t</w>-<w n="28.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="28.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="28.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="28.6" punct="pi:6">m<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e" qr="N5"><seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pi">oi</seg></rhyme></pgtc></w> ?</l>
						</lg>
						<lg n="8" type="quatrain" rhyme="abab">
							<l n="29" num="8.1" lm="6" met="6"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="29.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="29.3" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="30" num="8.2" lm="6" met="6"><w n="30.1" punct="vg:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="vg">eu</seg></w>, <w n="30.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="30.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<pgtc id="16" weight="1" schema="GR">u<rhyme label="b" id="16" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pv">i</seg></rhyme></pgtc></w> ;</l>
							<l n="31" num="8.3" lm="6" met="6"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="31.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="31.4">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="32" num="8.4" lm="6" met="6"><w n="32.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="32.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="32.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="32.5" punct="pe:6">l<pgtc id="16" weight="1" schema="GR">u<rhyme label="b" id="16" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pe">i</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div></body></text></TEI>