<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES117" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abbaa)" er_moy="2.0" er_max="6" er_min="0" er_mode="0(6/12)" er_moy_et="2.45" qr_moy="0.0" qr_max="C0" qr_mode="0(12/12)" qr_moy_et="0.0">
						<head type="main">LA RECONNAISSANCE</head>
						<lg n="1" type="quintil" rhyme="abbaa">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="1.7" punct="pe:8">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pe">oin</seg>s</rhyme></pgtc></w> !</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">m</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.4">qu</w>’<w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.7" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rf<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="3.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.6" punct="dp:8">t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.8" punct="ps:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="ps">oin</seg>s</rhyme></pgtc></w>…</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="5.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="5.7" punct="pe:8">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pe">oin</seg>s</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="2" type="quintil" rhyme="abbaa">
							<l n="6" num="2.1" lm="8" met="8"><w n="6.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="6.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.6" punct="vg:8">c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
							<l n="7" num="2.2" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5" punct="pv:8"><pgtc id="4" weight="6" schema="[VCR" part="1"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="8" num="2.3" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pv:8">c<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="9" num="2.4" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="9.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.6" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gu<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
							<l n="10" num="2.5" lm="8" met="8"><w n="10.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="10.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.6" punct="pe:8">c<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pe">œu</seg>r</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="3" type="quintil" rhyme="abbaa">
							<l n="11" num="3.1" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4" punct="pe:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
							<l n="12" num="3.2" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.6" punct="pe:8"><pgtc id="6" weight="2" schema="CR" part="1">v<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
							<l n="13" num="3.3" lm="8" met="8"><w n="13.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="13.4">m</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="13.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="13.7" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="6" weight="2" schema="CR" part="1">v<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="3.4" lm="8" met="8"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="14.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5" punct="pe:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
							<l n="15" num="3.5" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="15.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.4" punct="pe:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
						</lg>
						<lg n="4" type="quintil" rhyme="abbaa">
							<l n="16" num="4.1" lm="8" met="8"><w n="16.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="16.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="vg:8">gu<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="7" weight="2" schema="CR" part="1">r<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
							<l n="17" num="4.2" lm="8" met="8"><w n="17.1" punct="pe:2">Cr<seg phoneme="y" type="vs" value="1" rule="454" place="1">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="pe ps">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="17.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe">A</seg>h</w> ! <w n="17.3" punct="pe:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pe">on</seg></w> ! <w n="17.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5">m</w>’<w n="17.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="18" num="4.3" lm="8" met="8"><w n="18.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="18.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="18.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="18.4">n</w>’<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="18.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="18.7" punct="pv:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rb<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
							<l n="19" num="4.4" lm="8" met="8"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3" punct="vg:3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w>-<w n="19.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="6">e</seg></w> <w n="19.6" punct="ps:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="7" weight="2" schema="CR" part="1">r<rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="ps">i</seg>r</rhyme></pgtc></w>…</l>
							<l n="20" num="4.5" lm="8" met="8"><w n="20.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="20.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="20.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.5" punct="pe:8">gu<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="7" weight="2" schema="CR" part="1">r<rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
						</lg>
					</div></body></text></TEI>