<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES316" modus="cp" lm_max="10" metProfile="5, 5+5" form="suite périodique" schema="3(ababcddcee)" er_moy="1.53" er_max="7" er_min="0" er_mode="2(7/15)" er_moy_et="1.71" qr_moy="0.0" qr_max="C0" qr_mode="0(15/15)" qr_moy_et="0.0">
				<lg n="1" type="dizain" rhyme="ababcddcee">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c</w> <w n="1.7">d</w>’<w n="1.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" caesura="1">o</seg>r</w><caesura></caesura> <w n="2.5">n</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.7">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d</w> <w n="2.8">qu</w>’<w n="2.9"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg></w> <w n="2.10" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="vg">e</seg>il</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>c</w><caesura></caesura> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="4.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.6">f<seg phoneme="wa" type="vs" value="1" rule="440" place="7" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="4.7" punct="pi:10">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rm<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="pi">e</seg>il</rhyme></pgtc></w> ?</l>
					<l n="5" num="1.5" lm="10" met="5+5"><w n="5.1" punct="vg:2">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="5.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="5.6" punct="vg:10">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="10" met="5+5"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="6.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="6.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="6.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="6.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</w> <w n="6.7">d</w>’<w n="6.8" punct="vg:10"><seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="10" met="5+5"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="7.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">ez</seg></w><caesura></caesura> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="7.5">r<seg phoneme="a" type="vs" value="1" rule="307" place="7" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="7.6" punct="pe:10">r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pe">eau</seg>x</rhyme></pgtc></w> !</l>
					<l n="8" num="1.8" lm="10" met="5+5"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="8.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="8.7" punct="dp:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg>s</rhyme></pgtc></w> :</l>
					<l n="9" num="1.9" lm="5" met="5"><space quantity="10" unit="char"></space><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="e" id="5" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="5" met="5"><space quantity="10" unit="char"></space><w n="10.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3" punct="pt:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg><pgtc id="5" weight="2" schema="CR">dr<rhyme label="e" id="5" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">ez</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="dizain" rhyme="ababcddcee">
					<l n="11" num="2.1" lm="10" met="5+5"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="11.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="11.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="11.4">h<seg phoneme="o" type="vs" value="1" rule="318" place="5" caesura="1">au</seg>t</w><caesura></caesura> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="11.6">f<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>lt<pgtc id="6" weight="1" schema="GR">i<rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="2.2" lm="10" met="5+5"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.4">pl<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">î</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>s</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="12.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.8" punct="pt:10">p<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>ds</rhyme></pgtc></w>.</l>
					<l n="13" num="2.3" lm="10" met="5+5"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="13.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="13.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="13.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.5">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="7">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.6" punct="vg:10">p<seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>p<pgtc id="6" weight="1" schema="GR">i<rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="2.4" lm="10" met="5+5"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="14.3">fl<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">ez</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="14.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="14.6">n<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="14.7" punct="pt:10">d<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="pt">oi</seg>gts</rhyme></pgtc></w>.</l>
					<l n="15" num="2.5" lm="10" met="5+5"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="15.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="15.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="15.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>c</w><caesura></caesura> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="15.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="15.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="15.8" punct="pi:10"><pgtc id="8" weight="0" schema="[R"><rhyme label="c" id="8" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg>s</rhyme></pgtc></w> ?</l>
					<l n="16" num="2.6" lm="10" met="5+5"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="16.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" caesura="1">an</seg>s</w><caesura></caesura> <w n="16.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6" mp="C">o</seg>s</w> <w n="16.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="16.7" punct="pe:10">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="9" weight="2" schema="CR">t<rhyme label="d" id="9" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pe">in</seg>s</rhyme></pgtc></w> !</l>
					<l n="17" num="2.7" lm="10" met="5+5"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="17.4" punct="dp:5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="dp" caesura="1">a</seg></w> :<caesura></caesura> <w n="17.5">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="17.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="17.8" punct="vg:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>s<pgtc id="9" weight="2" schema="CR">t<rhyme label="d" id="9" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg>s</rhyme></pgtc></w>,</l>
					<l n="18" num="2.8" lm="10" met="5+5"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="18.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="18.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="18.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="7" mp="M">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p</w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="18.7" punct="dp:10">l<pgtc id="8" weight="0" schema="R"><rhyme label="c" id="8" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg>s</rhyme></pgtc></w> :</l>
					<l n="19" num="2.9" lm="5" met="5"><space quantity="10" unit="char"></space><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="19.3" punct="vg:5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="e" id="10" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="2.10" lm="5" met="5"><space quantity="10" unit="char"></space><w n="20.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="20.3" punct="pe:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg><pgtc id="10" weight="2" schema="CR">dr<rhyme label="e" id="10" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe">ez</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="dizain" rhyme="ababcddcee">
					<l n="21" num="3.1" lm="10" met="5+5"><w n="21.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="21.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="21.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="21.5" punct="vg:5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="21.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="21.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="21.8">l</w>’<w n="21.9" punct="dp:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cr<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="22" num="3.2" lm="10" met="5+5"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" mp="P">è</seg>s</w> <w n="22.5">qu</w>’<w n="22.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="22.8" punct="dp:10">b<pgtc id="12" weight="1" schema="GR">i<rhyme label="b" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10" punct="dp">en</seg></rhyme></pgtc></w> :</l>
					<l n="23" num="3.3" lm="10" met="5+5"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="23.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">ez</seg></w><caesura></caesura> <w n="23.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="23.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="23.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="23.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="23.9" punct="vg:10">l<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="3.4" lm="10" met="5+5"><w n="24.1">L</w>’<w n="24.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="24.4">l</w>’<w n="24.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="24.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="24.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="24.8" punct="pt:10">r<pgtc id="12" weight="1" schema="GR">i<rhyme label="b" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10" punct="pt">en</seg></rhyme></pgtc></w>.</l>
					<l n="25" num="3.5" lm="10" met="5+5"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="25.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="25.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>r</w><caesura></caesura> <w n="25.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="25.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="25.6" punct="vg:10"><pgtc id="13" weight="2" schema="[CR">fl<rhyme label="c" id="13" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="3.6" lm="10" met="5+5"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="26.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="26.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="26.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="26.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="26.6" punct="pv:10"><pgtc id="14" weight="7" schema="[CVGR">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<rhyme label="d" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg>s</rhyme></pgtc></w> ;</l>
					<l n="27" num="3.7" lm="10" met="5+5"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M/mp">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="27.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="27.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="27.4">pl<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="27.6" punct="pv:10"><pgtc id="14" weight="7" schema="[CVGR">cr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<rhyme label="d" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg>s</rhyme></pgtc></w> ;</l>
					<l n="28" num="3.8" lm="10" met="5+5"><w n="28.1">L</w>’<w n="28.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>t</w>, <w n="28.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="28.4" punct="vg:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="28.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="28.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="28.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="28.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="28.9"><pgtc id="13" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="28.10" punct="dp:10"><pgtc id="13" weight="2" schema="[C[R" part="2"><rhyme label="c" id="13" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="29" num="3.9" lm="5" met="5"><space quantity="10" unit="char"></space><w n="29.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="29.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="29.3" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg><pgtc id="15" weight="2" schema="CR" part="1">r<rhyme label="e" id="15" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
					<l n="30" num="3.10" lm="5" met="5"><space quantity="10" unit="char"></space><w n="30.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="30.3" punct="pe:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg><pgtc id="15" weight="2" schema="CR" part="1">dr<rhyme label="e" id="15" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe">ez</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>