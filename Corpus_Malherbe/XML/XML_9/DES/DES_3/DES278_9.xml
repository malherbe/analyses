<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES278" modus="sm" lm_max="5" metProfile="5" form="suite périodique" schema="3(ababcdcd)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(6/12)" er_moy_et="1.0" qr_moy="0.0" qr_max="C0" qr_mode="0(12/12)" qr_moy_et="0.0">
				<head type="main">LA VIE</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="5" met="5"><w n="1.1" punct="vg:3">N<seg phoneme="y" type="vs" value="1" rule="d-3" place="1">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="1.2" punct="vg:5">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="5" met="5"><w n="2.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4"><pgtc id="2" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="2.5" punct="vg:5"><pgtc id="2" weight="2" schema="[C[R" part="2"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="5" met="5"><w n="3.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="3.3">l</w>’<w n="3.4" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><w n="4.1">F<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="4.3">l</w>’<w n="4.4" punct="pv:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><pgtc id="2" weight="2" schema="CR" part="1">cl<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pv">ai</seg>r</rhyme></pgtc></w> ;</l>
					<l n="5" num="1.5" lm="5" met="5"><w n="5.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4">fl<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="5" met="5"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c</w> <w n="6.4" punct="pi:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="pi">e</seg>il</rhyme></pgtc></w> ?</l>
					<l n="7" num="1.7" lm="5" met="5"><w n="7.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.4"><pgtc id="3" weight="0" schema="[R" part="1"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="5" met="5"><w n="8.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.3" punct="pi:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="pi">e</seg>il</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababcdcd">
					<l n="9" num="2.1" lm="5" met="5"><w n="9.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="9.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="9.5"><pgtc id="5" weight="0" schema="[R" part="1"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="2.2" lm="5" met="5"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="10.4" punct="pv:5">t<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pv">oi</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="2.3" lm="5" met="5"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="2.4" lm="5" met="5"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4" punct="pt:5">m<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
					<l n="13" num="2.5" lm="5" met="5"><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">fl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><pgtc id="7" weight="2" schema="CR" part="1">tr<rhyme label="c" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="2.6" lm="5" met="5"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.4" punct="vg:5"><pgtc id="8" weight="2" schema="[CR" part="1">b<rhyme label="d" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
					<l n="15" num="2.7" lm="5" met="5"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t</w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg><pgtc id="7" weight="2" schema="CR" part="1">tr<rhyme label="c" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="469" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="2.8" lm="5" met="5"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w>-<w n="16.3" punct="pe:3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe">a</seg>s</w> ! <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w>-<w n="16.5" punct="pe:5"><pgtc id="8" weight="2" schema="[CR" part="1">b<rhyme label="d" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababcdcd">
					<l n="17" num="3.1" lm="5" met="5"><w n="17.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w>-<w n="17.2" punct="ps:2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="ps">on</seg>c</w>… <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="17.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5"><pgtc id="9" weight="2" schema="[CR" part="1">d<rhyme label="a" id="9" gender="f" type="a" part="I" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</rhyme></pgtc></w>-<w n="17.6" punct="pe:5"><pgtc id="9" weight="2" schema="[CR" part="2"><rhyme label="a" id="9" gender="f" type="a" part="F" qr="C0">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="18" num="3.2" lm="5" met="5"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="18.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4"><pgtc id="10" weight="2" schema="[CR" part="1">v<rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</rhyme></pgtc></w></l>
					<l n="19" num="3.3" lm="5" met="5"><w n="19.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="19.4" punct="pe:5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="9" weight="2" schema="CR" part="1">d<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="20" num="3.4" lm="5" met="5"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="20.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.4"><pgtc id="10" weight="2" schema="[CR" part="1">v<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</rhyme></pgtc></w></l>
					<l n="21" num="3.5" lm="5" met="5"><w n="21.1">R<seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">a</seg>y<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="21.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="21.3"><pgtc id="11" weight="2" schema="[CR" part="1">r<rhyme label="c" id="11" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="3.6" lm="5" met="5"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="22.3" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>f<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="d" id="12" gender="m" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg>s</rhyme></pgtc></w>,</l>
					<l n="23" num="3.7" lm="5" met="5"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="23.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><pgtc id="11" weight="2" schema="CR" part="1">r<rhyme label="c" id="11" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="3.8" lm="5" met="5"><w n="24.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="pe">an</seg>t</w> ! <w n="24.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="24.3">n</w>’<w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="4">e</seg>s</w> <w n="24.5" punct="pe:5">pl<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="d" id="12" gender="m" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="pe">u</seg>s</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>