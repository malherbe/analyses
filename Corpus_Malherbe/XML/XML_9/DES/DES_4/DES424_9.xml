<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES424" modus="sp" lm_max="6" metProfile="6, 4" form="suite périodique" schema="6(ababcdcd)" er_moy="1.25" er_max="6" er_min="0" er_mode="2(12/24)" er_moy_et="1.39" qr_moy="0.0" qr_max="C0" qr_mode="0(24/24)" qr_moy_et="0.0">
					<head type="main">L’ENFANT AU MIROIR</head>
					<opener>
						<salute>À Mlle Émilie Bascans.</salute>
					</opener>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="1.5" punct="vg:6">gr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="4" met="4"><space quantity="2" unit="char"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.3">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">gu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>rl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="4" met="4"><space quantity="2" unit="char"></space><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3" punct="pt:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="6" met="6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5" punct="vg:6">ch<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="4" met="4"><space quantity="2" unit="char"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>’<w n="6.3" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg><pgtc id="4" weight="2" schema="CR">dr<rhyme label="d" id="4" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pv">ai</seg>s</rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="6" met="6"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.6" punct="vg:6"><pgtc id="3" weight="0" schema="[R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="4" met="4"><space quantity="2" unit="char"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="pt:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="d" id="4" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="6" met="6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">d<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="9.4" punct="vg:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="2.2" lm="4" met="4"><space quantity="2" unit="char"></space><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="10.4" punct="vg:4">p<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="11" num="2.3" lm="6" met="6"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.4" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="2.4" lm="4" met="4"><space quantity="2" unit="char"></space><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="12.4" punct="vg:4">b<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="13" num="2.5" lm="6" met="6"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">gl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4" punct="vg:6">cl<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="2.6" lm="4" met="4"><space quantity="2" unit="char"></space><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="14.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
						<l n="15" num="2.7" lm="6" met="6"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">pl<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="2.8" lm="4" met="4"><space quantity="2" unit="char"></space><w n="16.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="16.3" punct="pt:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="6" met="6"><w n="17.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="17.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg><pgtc id="9" weight="2" schema="CR">mm<rhyme label="a" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.2" lm="4" met="4"><space quantity="2" unit="char"></space><w n="18.1">D</w>’<w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="18.3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg><pgtc id="10" weight="2" schema="C[R" part="1">t</pgtc></w> <w n="18.4" punct="pe:4"><pgtc id="10" weight="2" schema="C[R" part="2"><rhyme label="b" id="10" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
						<l n="19" num="3.3" lm="6" met="6"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="19.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="19.5"><pgtc id="9" weight="2" schema="[CR" part="1">m<rhyme label="a" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="3.4" lm="4" met="4"><space quantity="2" unit="char"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="20.4" punct="pe:4"><pgtc id="10" weight="2" schema="[CR" part="1">t<rhyme label="b" id="10" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="pe ps">em</seg>ps</rhyme></pgtc></w> !…</l>
						<l n="21" num="3.5" lm="6" met="6"><w n="21.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="21.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.5">v<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="c" id="11" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="22" num="3.6" lm="4" met="4"><space quantity="2" unit="char"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="22.3" punct="pe:4">s<pgtc id="12" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<rhyme label="d" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
						<l n="23" num="3.7" lm="6" met="6"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="23.2">s</w>’<w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="23.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="23.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="23.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>qu<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="c" id="11" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="485" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="3.8" lm="4" met="4"><space quantity="2" unit="char"></space><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="24.3" punct="pe:4">v<pgtc id="12" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<rhyme label="d" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="huitain" rhyme="ababcdcd">
						<l n="25" num="4.1" lm="6" met="6"><w n="25.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="25.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="25.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="25.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="25.5"><pgtc id="13" weight="2" schema="[C[R" part="1">m</pgtc></w>’<w n="25.6"><pgtc id="13" weight="2" schema="[C[R" part="2"><rhyme label="a" id="13" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="26" num="4.2" lm="4" met="4"><space quantity="2" unit="char"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><pgtc id="14" weight="2" schema="CR" part="1">f<rhyme label="b" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d</rhyme></pgtc></w>,</l>
						<l n="27" num="4.3" lm="6" met="6"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="27.3">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="27.5"><pgtc id="13" weight="2" schema="[CR" part="1">m<rhyme label="a" id="13" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="28" num="4.4" lm="4" met="4"><space quantity="2" unit="char"></space><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="28.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="28.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg><pgtc id="14" weight="2" schema="CR" part="1">f<rhyme label="b" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="29" num="4.5" lm="6" met="6"><w n="29.1">M</w>’<w n="29.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="29.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="29.4">l</w>’<w n="29.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="29.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>s<pgtc id="15" weight="2" schema="CR" part="1">p<rhyme label="c" id="15" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="30" num="4.6" lm="4" met="4"><space quantity="2" unit="char"></space><w n="30.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="30.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="30.4"><pgtc id="16" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="30.5" punct="pt:4"><pgtc id="16" weight="2" schema="[C[R" part="2"><rhyme label="d" id="16" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pt">ai</seg>r</rhyme></pgtc></w>.</l>
						<l n="31" num="4.7" lm="6" met="6"><w n="31.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="31.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="31.4" punct="pt:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg><pgtc id="15" weight="2" schema="CR" part="1">p<rhyme label="c" id="15" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="32" num="4.8" lm="4" met="4"><space quantity="2" unit="char"></space><w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="32.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="32.4" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><pgtc id="16" weight="2" schema="CR" part="1">cl<rhyme label="d" id="16" gender="m" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pe">ai</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="huitain" rhyme="ababcdcd">
						<l n="33" num="5.1" lm="6" met="6"><w n="33.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="33.2">ru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="33.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="33.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg><pgtc id="17" weight="2" schema="CR" part="1">r<rhyme label="a" id="17" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></pgtc></w></l>
						<l n="34" num="5.2" lm="4" met="4"><space quantity="2" unit="char"></space><w n="34.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="34.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="34.3">ps<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg><pgtc id="18" weight="2" schema="CR" part="1">ch<rhyme label="b" id="18" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</rhyme></pgtc></w></l>
						<l n="35" num="5.3" lm="6" met="6"><w n="35.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="35.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="35.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="35.4" punct="vg:6">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="5">eu</seg><pgtc id="17" weight="2" schema="CR" part="1">r<rhyme label="a" id="17" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="36" num="5.4" lm="4" met="4"><space quantity="2" unit="char"></space><w n="36.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="36.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ts</w> <w n="36.3" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg><pgtc id="18" weight="2" schema="CR" part="1">ch<rhyme label="b" id="18" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
						<l n="37" num="5.5" lm="6" met="6"><w n="37.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="37.2">l</w>’<w n="37.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="37.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="37.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="37.6" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg><pgtc id="19" weight="2" schema="CR" part="1">l<rhyme label="c" id="19" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="38" num="5.6" lm="4" met="4"><space quantity="2" unit="char"></space><w n="38.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="38.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="38.3" punct="vg:4">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg><pgtc id="20" weight="2" schema="CR" part="1">ss<rhyme label="d" id="20" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="39" num="5.7" lm="6" met="6"><w n="39.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="39.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="39.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="39.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="19" weight="2" schema="CR" part="1">l<rhyme label="c" id="19" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="40" num="5.8" lm="4" met="4"><space quantity="2" unit="char"></space><w n="40.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="40.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="40.3" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="20" weight="2" schema="CR" part="1">ss<rhyme label="d" id="20" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="huitain" rhyme="ababcdcd">
						<l n="41" num="6.1" lm="6" met="6"><w n="41.1">C</w>’<w n="41.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="41.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="41.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="41.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="41.6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="41.7">s<pgtc id="21" weight="0" schema="R" part="1"><rhyme label="a" id="21" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="42" num="6.2" lm="4" met="4"><space quantity="2" unit="char"></space><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="42.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="42.3">c</w>’<w n="42.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="42.5">b<pgtc id="22" weight="0" schema="R" part="1"><rhyme label="b" id="22" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></rhyme></pgtc></w></l>
						<l n="43" num="6.3" lm="6" met="6"><w n="43.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="43.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="43.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="43.5"><pgtc id="21" weight="0" schema="[R" part="1"><rhyme label="a" id="21" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="44" num="6.4" lm="4" met="4"><space quantity="2" unit="char"></space><w n="44.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="44.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="44.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="44.4">l</w>’<w n="44.5" punct="pe:4"><pgtc id="22" weight="0" schema="[R" part="1"><rhyme label="b" id="22" gender="m" type="e" qr="C0"><seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
						<l n="45" num="6.5" lm="6" met="6"><w n="45.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="45.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="45.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="45.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="45.5" punct="vg:6"><pgtc id="23" weight="0" schema="[R" part="1"><rhyme label="c" id="23" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="46" num="6.6" lm="4" met="4"><space quantity="2" unit="char"></space><w n="46.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="46.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="24" weight="2" schema="CR" part="1">j<rhyme label="d" id="24" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rs</rhyme></pgtc></w>,</l>
						<l n="47" num="6.7" lm="6" met="6"><w n="47.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="47.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="47.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="47.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<pgtc id="23" weight="0" schema="R" part="1"><rhyme label="c" id="23" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="48" num="6.8" lm="4" met="4"><space quantity="2" unit="char"></space><w n="48.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="48.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="48.3" punct="pe:4"><pgtc id="24" weight="2" schema="[CR" part="1">j<rhyme label="d" id="24" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>