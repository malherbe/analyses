<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU62" modus="sp" lm_max="7" metProfile="7, 5" form="suite périodique" schema="6(aabccb)" er_moy="1.39" er_max="6" er_min="0" er_mode="2(9/18)" er_moy_et="1.46" qr_moy="0.0" qr_max="C0" qr_mode="0(18/18)" qr_moy_et="0.0">
				<head type="main">LA DERNIÈRE CHANSON</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="1.6" punct="vg:7">j<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="2.4">d</w>’<w n="2.5" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pv">ou</seg>r</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="3.1">J</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="3.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="3.4" punct="pe:5">n<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>scr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>ts</rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6">pr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="5" met="5"><space unit="char" quantity="4"></space><w n="6.1">M</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="6.4" punct="pt:5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>r<pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2" punct="vg:3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>d</w>, <w n="7.3">j</w>’<w n="7.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7" punct="vg:7"><pgtc id="18" weight="2" schema="CR">qu<rhyme label="a" id="18" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7" punct="vg">ai</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt</w> <w n="8.4" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<pgtc id="18" weight="2" schema="CR">qu<rhyme label="a" id="18" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="2.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="9.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="9.2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">d</w>’<w n="9.4" punct="vg:5">h<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="f" type="a" qr="E0"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.4" lm="7" met="7"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg>ï<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="10.5" punct="vg:7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>r<pgtc id="5" weight="2" schema="CR">d<rhyme label="c" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="11" num="2.5" lm="7" met="7"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.4" punct="vg:7"><pgtc id="5" weight="2" schema="CR">d<rhyme label="c" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
					<l n="12" num="2.6" lm="5" met="5"><space unit="char" quantity="4"></space><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3" punct="pt:5">D<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>c<pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="f" type="e" qr="E0"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="vg:4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="13.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="13.4" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>r<pgtc id="6" weight="2" schema="CR">c<rhyme label="a" id="6" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="14.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="14.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6" punct="vg:7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg><pgtc id="6" weight="2" schema="CR">ss<rhyme label="a" id="6" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="3.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3" punct="vg:5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rf<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="3.4" lm="7" met="7"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">s</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="16.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="8" weight="2" schema="CR">nn<rhyme label="c" id="8" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></rhyme></pgtc></w></l>
					<l n="17" num="3.5" lm="7" met="7"><w n="17.1">L</w>’<w n="17.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="17.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="6">î</seg><pgtc id="8" weight="2" schema="CR">n<rhyme label="c" id="8" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="5" met="5"><space unit="char" quantity="4"></space><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="18.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4" punct="pe:5">v<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2" punct="vg:4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="19.4" punct="pe:7">b<pgtc id="9" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nh<rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
					<l n="20" num="4.2" lm="7" met="7"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="20.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>x</w> <w n="20.6">d</w>’<w n="20.7">h<pgtc id="9" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</rhyme></pgtc></w></l>
					<l n="21" num="4.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="21.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x</w> <w n="21.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>cs</w> <w n="21.4" punct="pe:5">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg><pgtc id="10" weight="2" schema="CR">qu<rhyme label="b" id="10" gender="f" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="22" num="4.4" lm="7" met="7"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="22.3">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.4">d</w>’<w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>f<pgtc id="11" weight="0" schema="R"><rhyme label="c" id="11" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</rhyme></pgtc></w></l>
					<l n="23" num="4.5" lm="7" met="7"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="23.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="23.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="23.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="4">î</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="23.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="23.7" punct="ps:7">tr<pgtc id="11" weight="0" schema="R"><rhyme label="c" id="11" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="ps">oi</seg>s</rhyme></pgtc></w>…</l>
					<l n="24" num="4.6" lm="5" met="5"><space unit="char" quantity="4"></space><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="24.3">d</w>’<w n="24.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg><pgtc id="10" weight="2" schema="CR">c<rhyme label="b" id="10" gender="f" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabccb">
					<l n="25" num="5.1" lm="7" met="7"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="25.2" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="25.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.4">n</w>’<w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="25.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="25.7" punct="vg:7">r<pgtc id="12" weight="1" schema="GR">i<rhyme label="a" id="12" gender="m" type="a" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="5.2" lm="7" met="7"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="26.3" punct="vg:4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="26.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="26.6" punct="vg:7">ch<pgtc id="12" weight="1" schema="GR">i<rhyme label="a" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="5.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="27.4" punct="pt:5">l<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="28" num="5.4" lm="7" met="7"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="28.2" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="28.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="28.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="28.5" punct="vg:7">tr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<pgtc id="14" weight="0" schema="R"><rhyme label="c" id="14" gender="m" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
					<l n="29" num="5.5" lm="7" met="7"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="29.5">d</w>’<w n="29.6"><pgtc id="14" weight="0" schema="[R"><rhyme label="c" id="14" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</rhyme></pgtc></w></l>
					<l n="30" num="5.6" lm="5" met="5"><space unit="char" quantity="4"></space><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">N<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="30.4" punct="pe:5">br<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="31" num="6.1" lm="7" met="7"><w n="31.1">T<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="31.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="31.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="31.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.5" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ff<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg><pgtc id="15" weight="2" schema="CR">m<rhyme label="a" id="15" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="32" num="6.2" lm="7" met="7"><w n="32.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w>-<w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="32.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="32.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="32.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg><pgtc id="15" weight="2" schema="CR">m<rhyme label="a" id="15" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></rhyme></pgtc></w></l>
					<l n="33" num="6.3" lm="5" met="5"><space unit="char" quantity="4"></space><w n="33.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="33.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="33.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="33.4" punct="pv:5"><pgtc id="16" weight="2" schema="[CR">t<rhyme label="b" id="16" gender="f" type="a" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="34" num="6.4" lm="7" met="7"><w n="34.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="34.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="34.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>s</w> <w n="34.5" punct="vg:7">fr<pgtc id="17" weight="0" schema="R"><rhyme label="c" id="17" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>d</rhyme></pgtc></w>,</l>
 					<l n="35" num="6.5" lm="7" met="7"><w n="35.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="35.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3" punct="vg:3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="35.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="35.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="35.7" punct="vg:7">d<pgtc id="17" weight="0" schema="R"><rhyme label="c" id="17" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>gt</rhyme></pgtc></w>,</l>
					<l n="36" num="6.6" lm="5" met="5"><space unit="char" quantity="4"></space><w n="36.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="36.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="36.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="36.4" punct="pe:5"><pgtc id="16" weight="2" schema="[CR">t<rhyme label="b" id="16" gender="f" type="e" qr="C0"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>