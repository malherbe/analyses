<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU105" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abab)" er_moy="2.5" er_max="6" er_min="0" er_mode="0(3/8)" er_moy_et="2.4" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
				<head type="number">XXXVIII</head>
				<head type="main">APPARITION</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3" punct="vg:5">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6" punct="vg:8">t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>ts</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.6" punct="vg:8"><pgtc id="2" weight="2" schema="[CR">r<rhyme label="b" id="2" gender="f" type="a" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">m</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="f" type="e" qr="C0"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="5.3">fl<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.6">c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ds</w> <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.6" punct="pv:8">g<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="7.7"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="426" place="8">où</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="8.5" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="9.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>il</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="9.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<pgtc id="5" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">dr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.4">s<pgtc id="6" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></pgtc></w> <w n="10.5" punct="pv:8"><pgtc id="6" weight="6" schema="V[CR" part="2">t<rhyme label="b" id="6" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="11.4">s<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="11.5" punct="vg:8">r<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.6" punct="dp:8">b<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="6" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8">« <w n="13.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.2" punct="pe:3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pe">i</seg></w> ! <w n="13.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="13.4">c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<pgtc id="7" weight="4" schema="VR" part="1"><seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="vg">en</seg>s</w>, <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="14.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="8" weight="2" schema="CR" part="1">nn<rhyme label="b" id="8" gender="f" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.4">p<pgtc id="7" weight="4" schema="VR" part="1"><seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="16.3" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg><pgtc id="8" weight="2" schema="CR" part="1">nn<rhyme label="b" id="8" gender="f" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> ! »</l>
				</lg>
			</div></body></text></TEI>