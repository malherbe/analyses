<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU109" modus="sm" lm_max="7" metProfile="7" form="suite de strophes" schema="1[aabba] 1[abab]" er_moy="1.2" er_max="2" er_min="0" er_mode="2(3/5)" er_moy_et="0.98" qr_moy="0.0" qr_max="E0" qr_mode="0(5/5)" qr_moy_et="0.0">
				<head type="number">XLII</head>
				<head type="main">LA CHANSON DES RAMES</head>
				<lg n="1" type="regexp" rhyme="aabb">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">B<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="1.2" punct="pe:3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="pe">u</seg>s</w> ! <w n="1.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="1.4">V<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="1.5">d</w>’<w n="1.6" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="E0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="2.3" punct="pe:3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="pe">i</seg>t</w> ! <w n="2.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="2.5">L</w>’<w n="2.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.8" punct="pe:7">j<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="E0"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="pe:3">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3" punct="pe">e</seg>il</w> ! <w n="3.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="3.4">S</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="3.6" punct="pe:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg></rhyme></pgtc></w> !</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.4" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="389" place="3" punct="vg">oeu</seg>r</w>, <w n="4.5" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>h</w> ! <w n="4.6">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="4.7" punct="pe:7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg><pgtc id="2" weight="2" schema="CR">pl<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="regexp" rhyme="aab">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3" punct="vg:3">n<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="vg">e</seg>f</w>, <w n="5.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>h</w> ! <w n="5.5">L</w>’<w n="5.6"><seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="5.7" punct="vg:7">m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="E0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="7" met="7"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2" punct="vg:3">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>d</w>, <w n="6.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>h</w> ! <w n="6.4">M<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="7.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>h</w> ! <w n="7.5">S<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="7.6" punct="pt:7">j<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="4" weight="2" schema="CR">l<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="regexp" rhyme="ab">
					<l n="8" num="3.1" lm="7" met="7"><w n="8.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="8.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>ts</w>, <w n="8.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>h</w> ! <w n="8.4">L</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.6" punct="pe:7">d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="9" num="3.2" lm="7" met="7"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>h</w> ! <w n="9.5">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="9.6">l</w>’<w n="9.7" punct="pe:7"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="4" weight="2" schema="CR">bl<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg></rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<signed>l’empereur Vou-Ti.</signed>
				</closer>
			</div></body></text></TEI>