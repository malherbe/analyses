<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI142" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="13(aa)" er_moy="0.92" er_max="6" er_min="0" er_mode="0(9/13)" er_moy_et="1.69" qr_moy="0.0" qr_max="E0" qr_mode="0(13/13)" qr_moy_et="0.0">
					<head type="main">En passant à Kemper</head>
					<opener>
						<salute>À mon ami J. Béliard</salute>
					</opener>
					<lg n="1" type="distique" rhyme="aa">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">E</seg></w> <w n="1.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>t</w><caesura></caesura> <w n="1.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="1.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.7">cl<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="2.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>fl<seg phoneme="y" type="vs" value="1" rule="454" place="3">u</seg><seg phoneme="?" type="va" value="1" rule="162" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">O</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="2.8" punct="pv:10">St<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="3" num="2.1" lm="10" met="4+6"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="3.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">h<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">ll<rhyme label="a" id="1" gender="f" type="a" qr="E0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="4" num="2.2" lm="10" met="4+6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="6" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="4.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.5" punct="vg:10">v<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" qr="E0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="3" type="distique" rhyme="aa">
						<l n="5" num="3.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">C<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.5">r<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="5.6">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M/mc">â</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="Lc">on</seg></w>-<w n="5.7">M<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="a" qr="C0"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>r</rhyme></pgtc></w></l>
						<l n="6" num="3.2" lm="10" met="4+6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="6.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.7" punct="pv:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pv">o</seg>r</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" type="distique" rhyme="aa">
						<l n="7" num="4.1" lm="10" met="4+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>ts</w><caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="7.6">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>ts</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">A</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="4.2" lm="10" met="4+6"><w n="8.1">M</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="8.5">c<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="8.6" punct="ps:10">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>lt<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
					</lg>
					<lg n="5" type="distique" rhyme="aa">
						<l n="9" num="5.1" lm="10" met="4+6"><w n="9.1">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="1">ean</seg></w> <w n="9.2">L<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="9.3" punct="pe:4">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" punct="pe" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.6">t</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ch<rhyme label="a" id="3" gender="m" type="a" qr="E0"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></rhyme></pgtc></w></l>
						<l n="10" num="5.2" lm="10" met="4+6"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="10.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="10.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3" mp="M">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>t<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ch<rhyme label="a" id="3" gender="m" type="e" qr="E0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></pgtc></w><ref type="noteAnchor">*</ref></l>
					</lg>
					<lg n="6" type="distique" rhyme="aa">
						<l n="11" num="6.1" lm="10" met="4+6">« <w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="11.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="11.3">br<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="11.4" punct="vg:5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg></w>, <w n="11.5">f<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.6" punct="vg:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">am</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">n<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="6.2" lm="10" met="4+6"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s</w>-<w n="12.2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="12.4" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="12.5" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="12.7">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.8" punct="pe:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<pgtc id="4" weight="2" schema="CR">n<rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="7" type="distique" rhyme="aa">
						<l n="13" num="7.1" lm="10" met="4+6">« <w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="13.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rds</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="a" qr="E0"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></pgtc></w></l>
						<l n="14" num="7.2" lm="10" met="4+6"><w n="14.1">D<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3" punct="vg:4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>t</w>,<caesura></caesura> <w n="14.4" punct="vg:6">K<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="14.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.7" punct="dp:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="e" qr="E0"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="dp">é</seg></rhyme></pgtc></w> :</l>
					</lg>
					<lg n="8" type="distique" rhyme="aa">
						<l n="15" num="8.1" lm="10" met="4+6">« <w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="15.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.6">qu</w>’<w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="15.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="15.9">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>g</w> <w n="15.10">s<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="8.2" lm="10" met="4+6"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.2" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="Lc">à</seg></w>-<w n="16.4" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>, <w n="16.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="16.6" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nh<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="9" type="distique" rhyme="aa">
						<l n="17" num="9.1" lm="10" met="4+6">« <w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="17.2">si<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>d</w> <w n="17.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="17.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="17.6">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" mp="M">o</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="17.7">d</w>’<w n="17.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>tru<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></rhyme></pgtc></w></l>
						<l n="18" num="9.2" lm="10" met="4+6"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="18.3">n<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="18.5">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M/mc">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="8" mp="Lc">eau</seg></w>-<w n="18.6" punct="pe:10">Thi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M/mc">e</seg>rr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="10" punct="pe">y</seg></rhyme></pgtc></w> ! »</l>
					</lg>
					<lg n="10" type="distique" rhyme="aa">
						<l n="19" num="10.1" lm="10" met="4+6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="19.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="6" mp="C">o</seg>s</w> <w n="19.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="19.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>r<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="20" num="10.2" lm="10" met="4+6"><w n="20.1">G<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="20.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="20.6" punct="vg:10">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rnou<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="11" type="distique" rhyme="aa">
						<l n="21" num="11.1" lm="10" met="4+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="21.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="21.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>gs</w> <w n="21.6" punct="vg:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="22" num="11.2" lm="10" met="4+6"><w n="22.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="22.3">br<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="22.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="22.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>rs</w> <w n="22.7" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg>ï<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="12" type="distique" rhyme="aa">
						<l n="23" num="12.1" lm="10" met="4+6"><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="23.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="23.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="23.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="23.6" punct="pv:10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="24" num="12.2" lm="10" met="4+6"><w n="24.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="24.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="24.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="24.8" punct="dp:10">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>p<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					</lg>
					<lg n="13" type="distique" rhyme="aa">
						<l n="25" num="13.1" lm="10" met="4+6"><w n="25.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="25.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="25.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>t</w><caesura></caesura> <w n="25.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="25.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="25.7">cl<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="a" qr="E0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</rhyme></pgtc></w></l>
						<l n="26" num="13.2" lm="10" met="4+6"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="26.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>fl<seg phoneme="y" type="vs" value="1" rule="454" place="3">u</seg><seg phoneme="?" type="va" value="1" rule="162" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="26.4">l</w>’<w n="26.5"><seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">O</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w> <w n="26.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="26.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="26.8" punct="pt:10">St<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="e" qr="E0"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<note type="footnote" id="*">Fable du Charretier embourbé.</note>
					</closer>
				</div></body></text></TEI>