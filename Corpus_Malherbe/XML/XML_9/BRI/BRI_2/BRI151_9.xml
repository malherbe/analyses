<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI151" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(aaa)" er_moy="1.25" er_max="2" er_min="0" er_mode="2(5/8)" er_moy_et="0.97" qr_moy="0.0" qr_max="C0" qr_mode="0(8/8)" qr_moy_et="0.0">
					<head type="main">Les trois Douleurs</head>
					<lg n="1" type="tercet" rhyme="aaa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">AN</seg>S</w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="1.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="1.6">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="1.7" punct="vg:8">fl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.4">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="2.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.7" punct="vg:8">pl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="3.6" punct="pt:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="tercet" rhyme="aaa">
						<l n="4" num="2.1" lm="8" met="8">« <w n="4.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="4.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="vg">ai</seg></w>-<w n="4.3" punct="vg:3">j<seg phoneme="ə" type="ee" value="0" rule="e-15">e</seg></w>, <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="4.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.8"><pgtc id="2" weight="2" schema="[C[R" part="1">m</pgtc></w>’<w n="4.9" punct="pe:8"><pgtc id="2" weight="2" schema="[C[R" part="2"><rhyme label="a" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
						<l n="5" num="2.2" lm="8" met="8"><w n="5.1" punct="vg:1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w>-<w n="5.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR" part="1">m<rhyme label="a" id="2" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="6" num="2.3" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rs</w> <w n="6.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="6.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="6.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="6.6">m</w>’<w n="6.7" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bs<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="a" id="2" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="tercet" rhyme="aaa">
						<l n="7" num="3.1" lm="8" met="8">— <w n="7.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="7.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w>-<w n="7.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>l</w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ds</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="7.6" punct="pv:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>r</rhyme></pgtc></w> ;</l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rps</w> <w n="8.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="8.7" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7" punct="pt:8">fl<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="3" weight="2" schema="CR" part="1">tr<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="tercet" rhyme="aaa">
						<l n="10" num="4.1" lm="8" met="8">« <w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6" punct="vg:8"><pgtc id="4" weight="2" schema="[CR" part="1">f<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="4.2" lm="8" met="8"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.8" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="4" weight="2" schema="CR" part="1">f<rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="12" num="4.3" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="12.5" punct="pe:8">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="4" weight="2" schema="CR" part="1">ph<rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> ! »</l>
					</lg>
					<closer>
						<dateline>
						<date when="1841">15 août 1841.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>