<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI140" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="5(aa)" er_moy="0.8" er_max="2" er_min="0" er_mode="0(3/5)" er_moy_et="0.98" qr_moy="0.0" qr_max="C0" qr_mode="0(5/5)" qr_moy_et="0.0">
					<head type="main">Le Catéchisme</head>
					<div type="section" n="1">
						<head type="number">MARIE.</head>
						<lg n="1" type="distique" rhyme="aa">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="1.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="1.4" punct="vg:6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="1.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="1.8" punct="vg:12">n<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>r<pgtc id="1" weight="2" schema="CR">c<rhyme label="a" id="1" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">An</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="2.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc</w> <w n="2.3">d</w>’<w n="2.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">A</seg>rz<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="vg" caesura="1">ô</seg></w>,<caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="2.7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem/mp">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="9" mp="Lp">ez</seg></w>-<w n="2.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="2.9" punct="pi:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg></rhyme></pgtc></w> ?</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">LE VOYAGEUR.</head>
						<lg n="1" type="distique" rhyme="aa">
							<l n="3" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">D</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="3.4" punct="vg:5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="vg">ain</seg></w>, <w n="3.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6" punct="vg:8">f<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="4.6" punct="pt:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="a" id="2" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">MARIE.</head>
						<lg n="1" type="distique" rhyme="aa">
							<l n="5" num="1.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M/mp">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="Lp">en</seg>d</w>-<w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.6" punct="pi:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="pi ti" caesura="1">oin</seg></w> ?<caesura></caesura> — <w n="5.7">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.9">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.10"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.11" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">nn<rhyme label="a" id="3" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="1.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="6.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ls</w> <w n="6.7" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">aî</seg><pgtc id="3" weight="2" schema="CR">n<rhyme label="a" id="3" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">LE VOYAGEUR.</head>
						<lg n="1" type="distique" rhyme="aa">
							<l n="7" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="7.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>s</w>, <w n="7.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="8" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">J</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>gl<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">MARIE.</head>
						<lg n="1" type="distique" rhyme="aa">
							<l n="9" num="1.1" lm="12" met="6+6"><w n="9.1" punct="pe:1">Ch<seg phoneme="y" type="vs" value="1" rule="450" place="1" punct="pe">u</seg>t</w> ! <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.5" punct="vg:6">Cr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg" caesura="1">o</seg></w>,<caesura></caesura> <w n="9.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="7" mp="M">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt</w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="9.9" punct="dp:12">d<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="dp">ou</seg>x</rhyme></pgtc></w> :</l>
							<l n="10" num="1.2" lm="12" met="6+6"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="10.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="10.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>, <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M/mp">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="11" mp="Lp">ez</seg></w>-<w n="10.9" punct="pi:12">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" qr="C0"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pi">ou</seg>s</rhyme></pgtc></w> ?</l>
						</lg>
					</div>
				</div></body></text></TEI>